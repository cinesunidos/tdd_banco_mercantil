﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    [DataContract]
    public class LoadMovies
    {
        [DataMember]
        public string HO { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}
