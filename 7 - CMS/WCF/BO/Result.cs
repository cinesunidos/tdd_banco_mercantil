﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF.BO
{
    public class Result
    {
        public int Code { get; set; }
        public string Message { get; set; }

    }
}