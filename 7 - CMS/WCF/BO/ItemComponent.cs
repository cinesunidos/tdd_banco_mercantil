﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF.BO
{
    /// <summary>
    /// Ramiro Marimon
    /// esta clase representa cad uno de los items de materia prima necesarios para conformar un combo o un item de venta final (bebida, cotufa, etc.)
    /// </summary>
    public class ItemComponent
    {
        /// <summary>
        /// el id del item de materia prima en vista
        /// </summary>
        public string ComponentId { get; set; }
        /// <summary>
        /// la cantidad que se becesita de este item de materia prima para crear un solo item final.
        /// </summary>
        public decimal ComponentOriginalQty { get; set; }
        /// <summary>
        /// la descripcion del item de materia prima
        /// </summary>
        public string ComponentDescription { get; set; }
        /// <summary>
        /// la cantidad disponible de este item en stock
        /// </summary>
        public decimal ComponentStock { get; set; }

        /// <summary>
        /// unidad de medida del componente
        /// </summary>
        public string UOM { get; set; }

        /// <summary>
        /// el id maestro del item en vista
        /// </summary>
        public string MasterID { get; set; }
    }
}