﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF.BO
{
    public class CloudServicesObject
    {
        public String InstanceName { get; set; }
        public String IpInternal { get; set; }
        public String IpPublic { get; set; }
    }
}