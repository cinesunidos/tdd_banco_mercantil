﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    [DataContract]
    public class Permissions
    {
        [DataMember]
        public System.Guid id { get; set; }
        [DataMember]
        public string Name { get; set; }      
    }
}