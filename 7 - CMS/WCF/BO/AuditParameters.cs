﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.BO
{
    [DataContract]
    public class AuditParameters
    {
        [DataMember]
        public string DateBegin { get; set; }
        [DataMember]
        public string DateEnd { get; set; }
        [DataMember]
        public string UserId { get; set; }
        [DataMember]
        public string Department { get; set; }
    }
}