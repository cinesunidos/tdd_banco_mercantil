﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.BO
{
    [DataContract]
    public class CandiesWeb
    {
        [DataMember]
        public string Cinema_strCode { get; set; }
        [DataMember]
        public string Item_strItemId { get; set; }
        [DataMember]
        public decimal Quantity { get; set; }
    }

}