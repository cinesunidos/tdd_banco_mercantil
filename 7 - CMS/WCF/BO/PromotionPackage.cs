﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    [DataContract]
    public class PromotionPackage
    {

        [DataMember]
        public string ListPromotions { get; set; }

        [DataMember]
        public string Token { get; set; }
        
    }
}
