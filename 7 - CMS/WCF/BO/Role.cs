﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    [DataContract]
    public class Role
    {
        [DataMember]
        public System.Guid roleId { get; set; }
        [DataMember]
        public string name { get; set; }
    }
}