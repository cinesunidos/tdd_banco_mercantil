﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    [DataContract]
    public class Message
    {
        [DataMember]
        public Boolean error { get; set; }
        [DataMember]
        public string  message { get; set; }
        [DataMember]
        public int number { get; set; }
    }
}
