﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.BO
{
    [DataContract]
    public class UserWeb
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string IdCard { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string MobilePhone { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public Boolean Active { get; set; }
        [DataMember]
        public bool MassMailSubcription { get; set; }
        [DataMember]
        public string Platform { get; set; }

        //Propiedades para Auditoria
        [DataMember]        
        public Guid userAudit { get; set; }
        [DataMember]
        public string Department { get; set; }
    }
}