﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF.BO
{
    public class ModalCinema
    {
        public int CinemaCode { get; set; }
        public string CinemaName { get; set; }
        public bool ModalEnabled { get; set; }
        public DateTime DateChanged { get; set; }
    }
}