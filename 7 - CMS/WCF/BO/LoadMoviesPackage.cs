﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    public class LoadMoviesPackage
    {
        [DataMember]
        public string ListLoadMovies { get; set; }

        [DataMember]
        public string Token { get; set; }
    }
}
