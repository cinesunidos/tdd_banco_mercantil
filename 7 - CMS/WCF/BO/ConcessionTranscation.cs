﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    [DataContract]
    public class ConcessionTransaction
    {
        [DataMember]
        public string TransactionId { get; set; }
        [DataMember]
        public string TransactionType { get; set; }
        [DataMember]
        public string TransactionStatus { get; set; }
        [DataMember]
        public string ItemId { get; set; }
        [DataMember]
        public string ItemDescription { get; set; }
        [DataMember]
        public int ItemQuantity { get; set; }
        [DataMember]
        public decimal ItemValue { get; set; }
        [DataMember]
        public decimal SubTotal { get; set; }
        [DataMember]
        public string Vendor { get; set; }
        [DataMember]
        public string LocationDescription { get; set; }
        [DataMember]
        public string Workstation { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FilmTitle { get; set; }
        [DataMember]
        public DateTime FilmDate { get; set; }
        [DataMember]
        public DateTime CollectionDate { get; set; }
        [DataMember]
        public String PickupWorkstation { get; set; }
        [DataMember]
        public int PickupUserId { get; set; }
        [DataMember]
        public String PickupUser { get; set; }
    }

    [DataContract]
    public class ConcessionTransactionSummary
    {
        [DataMember]
        public string ItemId { get; set; }
        [DataMember]
        public string ItemDescription { get; set; }
        [DataMember]
        public int ItemQuantity { get; set; }
        [DataMember]
        public decimal ItemValue { get; set; }
        [DataMember]
        public decimal SubTotal { get; set; }
    }

    [DataContract]
    public class ConcessionTransactionResponse
    {
        [DataMember]
        public List<ConcessionTransaction> Transactions;
        [DataMember]
        public List<ConcessionTransactionSummary> Summary;
        [DataMember]
        public decimal TotalSales;
    }

    public class ConcessionTransactionByDateF
    {
        [DataMember]
        public string TransactionId { get; set; }
        [DataMember]
        public string TransactionType { get; set; }
        [DataMember]
        public string TransactionStatus { get; set; }
        [DataMember]
        public string ItemId { get; set; }
        [DataMember]
        public string ItemDescription { get; set; }
        [DataMember]
        public int ItemQuantity { get; set; }
        [DataMember]
        public decimal ItemValue { get; set; }
        [DataMember]
        public decimal SubTotal { get; set; }
        [DataMember]
        public string Vendor { get; set; }
        [DataMember]
        public string LocationDescription { get; set; }
        [DataMember]
        public string Workstation { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FilmTitle { get; set; }
        [DataMember]
        public DateTime FilmDate { get; set; }
        [DataMember]
        public DateTime CollectionDate { get; set; }
        [DataMember]
        public String PickupWorkstation { get; set; }
        [DataMember]
        public int PickupUserId { get; set; }
        [DataMember]
        public String PickupUser { get; set; }
        [DataMember]
        public String Pickup { get; set; }
    }

    public class ConcessionTransactionByDateC
    {
        [DataMember]
        public string ItemId { get; set; }
        [DataMember]
        public string ItemDescription { get; set; }
        [DataMember]
        public int ItemQuantity { get; set; }
        [DataMember]
        public String PickupWorkstation { get; set; }
        [DataMember]
        public String PickupUser { get; set; }
        [DataMember]
        public String Componentes { get; set; }
        [DataMember]
        public Double Cantidad_Componentes { get; set; }
        [DataMember]
        public Double Total_Cantidad_Componentes { get; set; }
    }


    [DataContract]
    public class ConcessionTransactionByDate
    {
        [DataMember]
        public List<ConcessionTransactionByDateF> Transactions;
    }


    [DataContract]
    public class ConcessionTransactionComponentsByDate
    {
        [DataMember]
        public List<ConcessionTransactionByDateC> Transactions;
    }
}