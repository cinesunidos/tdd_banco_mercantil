﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.BO
{
    [DataContract]
    [Serializable]
    public class TransferPack
    {
        [DataMember]
        public string MessageList { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string idCine { get; set; }
    }
}