﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF.BO
{
    /// <summary>
    /// esta clase representa cada uno de los items que se desean transferir junto con sus componetes
    /// </summary>
    public class ItemAndComponets
    {
        /// <summary>
        /// el item peincipal que se desea transferir (combo mediano, por ejemplo)
        /// </summary>
        public TransferItem PrincipalItem { get; set; }

        /// <summary>
        /// si los componentes del item principal estan disponibles en cantidad, retorna verdadero, 
        /// de lo contrario retorna false lo que implica que el item principal no se puede tranferir por falta de materias primas
        /// </summary>
        public bool TransferAble
        {
            get
            {
                bool ok = true;
                foreach (var item in this.ItemComponents)
                {
                    if (PrincipalItem.itemType != null)
                    {
                        if (PrincipalItem.Quantity * item.ComponentOriginalQty > item.ComponentStock)
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        if (PrincipalItem.Quantity > item.ComponentStock)
                        {
                            ok = false;
                        }
                    }
                    
                }
                return ok;
            }
        }

        /// <summary>
        /// cada uno de los componenetes del item principal, con su cantidad disponible en stock
        /// </summary>
        public List<ItemComponent> ItemComponents { get; set; }

        //contructor -.-
        public ItemAndComponets()
        {
            this.ItemComponents = new List<ItemComponent>();
        }

    }

}