﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WCF.BO
{

    [DataContract]
    [Serializable]
    public class MessagesPack
    {
        [DataMember]
        public string MessageList { get; set; }

        [DataMember]
        public string Token { get; set; }

    }
}