﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF.BO
{
    public class TransferItem
    {
        public string itemDescription { get; set; }
        public string itemCode { get; set; }
        public string UnidadDeMedida { get; set; }
        public string itemType { get; set; }
        public int Quantity { get; set; }
    }
}