﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.BO
{
    [DataContract]
    public class BlackList_User
    {
        [DataMember]
        public System.Guid Id { get; set; }
        [DataMember]
        public string IdCard { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string platform { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public System.DateTime create { get; set; }
        //Para uso de Auditoria
        [DataMember]    
        public Guid userAudit { get; set; }
        public string Department { get; set; }
    }
}