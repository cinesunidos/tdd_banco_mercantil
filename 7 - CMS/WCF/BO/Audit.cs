﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.BO
{
    [DataContract]
    public class Audit
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public string Event { get; set; }
        [DataMember]
        public string Department { get; set; }
        [DataMember]
        public DateTime dateTime { get; set; }
    }
}