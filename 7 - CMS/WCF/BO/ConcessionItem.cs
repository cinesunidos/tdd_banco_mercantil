﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF.BO
{
    [DataContract]
    public class ConcessionItem
    {
        [DataMember]
        public string ItemId { get; set; }
        [DataMember]
        public string ItemDescription { get; set; }
        [DataMember]
        public string ItemHOPK { get; set; }
        [DataMember]
        public decimal ItemPrice { get; set; }
    }

    [DataContract]
    public class ConcessionCatalogResponse
    {
        [DataMember]
        public List<ConcessionItem> Products;
    }

}