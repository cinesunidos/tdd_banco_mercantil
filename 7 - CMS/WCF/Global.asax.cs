﻿using Castle.Core;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using WCF.BL;
using WCF.BL.Contract;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF
{
    public class Global : System.Web.HttpApplication
    {
        private IWindsorContainer _container;

        protected void Application_Start(object sender, EventArgs e)
        {
            _container = new WindsorContainer()
         .Register
         (
                 Component.For<IWebDAL>().ImplementedBy<UserWebDAL>(),
                 Component.For<IMobileDAL>().ImplementedBy<UserMobileDAL>(),
                 Component.For<IAdminDAL>().ImplementedBy<AdminDAL>(),
                 Component.For<ILoginUsersDAL>().ImplementedBy<LoginUserDAL>(),
                 Component.For<IManagementDAL>().ImplementedBy<ManagementDAL>(),
                 Component.For<IRedisKeysDAL>().ImplementedBy<RedisKeysDAL>(),
				 Component.For<IMessagesDAL>().ImplementedBy<MessagesDAL>(),
                 Component.For<IPromotionsDAL>().ImplementedBy<PromotionsDAL>(),
                 Component.For<IConcessionSettingsDAL>().ImplementedBy<ConcessionSettingsDAL>(),
                 Component.For<IConcessionTransactionsDAL>().ImplementedBy<ConcessionTransactionsDAL>(),
                 Component.For<ILoadMoviesDAL>().ImplementedBy<LoadMoviesDAL>()
         );

            _container.AddFacility<WcfFacility>()
            .Register
            (
                Component.For<LoggingInterceptor>().ImplementedBy<LoggingInterceptor>(),
                Component.For<IWeb>().ImplementedBy<WebServices>().Named("WebServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IMobile>().ImplementedBy<MobileServices>().Named("MobileServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IAdmin>().ImplementedBy<AdminServices>().Named("AdminServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<ILogin>().ImplementedBy<LoginUsersServices>().Named("LoginUsersServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IManagement>().ImplementedBy<ManagementServices>().Named("ManagementServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IRedisKeys>().ImplementedBy<RedisKeysServices>().Named("RedisKeysServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
				Component.For<IMessage>().ImplementedBy<MessageService>().Named("MessageService").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IPromotions>().ImplementedBy<PromotionsServices>().Named("PromotionsServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IConcession>().ImplementedBy<ConcessionServices>().Named("ConcessionServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<ILoadMovies>().ImplementedBy<LoadMoviesServices>().Named("LoadMoviesServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere

                );
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-VE");

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (_container != null)
                _container.Dispose();
        }
    }
}