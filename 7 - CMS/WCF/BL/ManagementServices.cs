﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class ManagementServices : IManagement
    {
        private IManagementDAL m_IManagementDAL;

        public ManagementServices(IManagementDAL iManagementDAL)
            : base()
        {
            m_IManagementDAL = iManagementDAL;
        }

        public List<BO.Role> GetRole()
        {
            return m_IManagementDAL.GetRole();
        }

        public Message CreateRole(Role role)
        {
            return m_IManagementDAL.CreateRole(role);
        }

        public Message ModifyRole(Role role)
        {
            return m_IManagementDAL.ModifyRole(role);
        }

        public Message RemoveRole(Role role)
        {
            return m_IManagementDAL.RemoveRole(role);
        }

        public List<BO.Permissions> GetPermissions()
        {
            return m_IManagementDAL.GetPermissions();
        }
        public List<BO.Permissions> GetAccessByRole(Role role)
        {
            Guid id =(role.roleId);
            return m_IManagementDAL.GetAccessByRole(id);
        }

        public Message CreatePermission(Permissions perm)
        {
            return m_IManagementDAL.CreatePermissions(perm);
        }

        public Message ModifyPermission(Permissions perm)
        {
            return m_IManagementDAL.ModifyPermission(perm);
        }

        public Message RemovePermission(Permissions perm)
        {
            return m_IManagementDAL.RemovePermissions(perm);
        }
        public Message AddPermissionByRole(string[] perm)
        {
            return m_IManagementDAL.AddPermissionByRole(perm);
        }
        public Message DeletePermissionByRole(string[] perm)
        {
            return m_IManagementDAL.DeletePermissionByRole(perm);
        }
    }
}
