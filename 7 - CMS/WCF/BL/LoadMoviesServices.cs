﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class LoadMoviesServices : ILoadMovies
    {
        private ILoadMoviesDAL m_LoadMoviesDAL;

        public LoadMoviesServices(ILoadMoviesDAL iLoadMoviesDAL)
            : base()
        {
            m_LoadMoviesDAL = iLoadMoviesDAL;
        }

        public List<LoadMovies> GetLoadMovies()
        {
            return m_LoadMoviesDAL.GetLoadMovies();
        }

        public Message FileGetRediCache(string redicache)
        {
            return m_LoadMoviesDAL.FileGetRediCache(redicache);
        }
    }
}
