﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL;

namespace WCF.BL
{
    public class WebServices : IWeb
    {
        private IWebDAL m_IWebDAL;

        public WebServices(IWebDAL iWebDAL)
            : base()
        {
            m_IWebDAL = iWebDAL;
        }

        public UserWeb GetUserIdCard(string idCard)
        {
            return m_IWebDAL.GetUserIdCard(idCard);
        }
        public UserWeb GetUserEmail(string email)
        {
            return m_IWebDAL.GetUserEmail(email);
        }
        public Message ModifyUser(UserWeb userWeb)
        {
            return m_IWebDAL.ModifyUser(userWeb, false);
        }
        public Message RemoveUser(UserWeb userWeb)
        {
            return m_IWebDAL.RemoveUser(userWeb);
        }
    }
}