﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class AdminServices : IAdmin
    {
        private IAdminDAL m_IAdminDAL;

        public AdminServices(IAdminDAL iAdminDAL)
            : base()
        {
            m_IAdminDAL = iAdminDAL;
        }

        public Message CreateBlackList(UserWeb userWeb)
        {
            return m_IAdminDAL.CreateBlackList(userWeb);
        }

        public UserWeb GetBlackList(string email, string platform)
        {
            return m_IAdminDAL.GetBlackList(email, platform);
        }

        public List<BlackList_User> GetAllBlackListUsers()
        {
            return m_IAdminDAL.GetAllBlackListUsers();
        }

        BlackList_User IAdmin.GetBlackListUserByIdCard(string IdCard)
        {
            return m_IAdminDAL.GetBlackListUserByIdCard(IdCard);
        }
        BlackList_User IAdmin.GetBlackListUserByEmail(string Email)
        {
            return m_IAdminDAL.GetBlackListUserByEmail(Email);
        }

        Message IAdmin.Delete_BlackListUser(BlackList_User BlackList_User)
        {
            return m_IAdminDAL.Delete_BlackListUser(BlackList_User);
        }

        public List<BO.Audit> ListAudit(AuditParameters parameters)
        {
            return m_IAdminDAL.ListAudit(parameters);
        }
    }
}