﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class RedisKeysServices : IRedisKeys
    {
        private IRedisKeysDAL m_IRedisKeyDAL;

        public RedisKeysServices(IRedisKeysDAL iRediskeyDAL)
            : base()
        {
            m_IRedisKeyDAL = iRediskeyDAL;
        }

        public Message AddKeyRedis(RedisKey Redis)
        {
            return m_IRedisKeyDAL.AddKeyRedis(Redis);
        }

        public RedisKey GetKeyRedis(RedisKey Redis)
        {
            return m_IRedisKeyDAL.GetKeyRedis(Redis);
        }

        public List<RedisKey> GetListRedisKeys()
        {
            return m_IRedisKeyDAL.GetListRedisKeys();
        }

        public Message ModifyKeyRedis(RedisKey Redis)
        {
            return m_IRedisKeyDAL.ModifyKeyRedis(Redis);
        }

        public Message RemoveKeyRedis(RedisKey Redis)
        {
            return m_IRedisKeyDAL.RemoveKeyRedis(Redis);
        }

        public List<Message> ExecuteKeyRedis(RedisKey Redis)
        {
            return m_IRedisKeyDAL.ExecuteKeyRedis(Redis);
        }
    }
}