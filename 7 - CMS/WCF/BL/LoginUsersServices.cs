﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class LoginUsersServices : ILogin

    {
        private ILoginUsersDAL m_ILoginUsersDAL;

        public LoginUsersServices(ILoginUsersDAL iLoginUsersDAL)
            : base()
        {
            m_ILoginUsersDAL = iLoginUsersDAL;
        }

        public LoginUser GetUserData(LoginUser Usr)
        {
            return m_ILoginUsersDAL.GetUserData(Usr);
        }
        public LoginUser GetUser(LoginUser Usr)
        {
            return m_ILoginUsersDAL.GetUserData(Usr);
        }
        public List<LoginUser> GetUser()
        {
            return m_ILoginUsersDAL.GetUser();
        }
        public Message CreateUser(LoginUser Usr)
        {
            return m_ILoginUsersDAL.CreateUser(Usr);
        }

        public Message ModifyUser(LoginUser User)
        {
            return m_ILoginUsersDAL.ModifyUser(User);
        }
        public Message DeleteUser(LoginUser User)
        {
            return m_ILoginUsersDAL.DeleteUser(User);
        }

    }
}