﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class MessageService : IMessage
    {
        
        private IMessagesDAL m_MessageDAL;

        public MessageService(IMessagesDAL ImessageDAL)
            : base()
        {
            m_MessageDAL = ImessageDAL;            
        }
        //public Message ApplyChanges(List<MessageForCinema> MessagesList)
        public Message ApplyChanges(string MessagesList)
        {            
            return m_MessageDAL.ApplyChange(MessagesList);
        }

        public List<MessageForCinema> GetMessagesFromDB()
        {
            return m_MessageDAL.GetMessagesFromDB();
        }

        public Message SaveMessagesToDB(List<MessageForCinema> messages)
        {
            return m_MessageDAL.SaveMessagesToDB(messages);
        }
    }
}