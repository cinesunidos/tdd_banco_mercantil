﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IWeb
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UserIdCard/{idCard}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserWeb GetUserIdCard(string idCard);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UserEmail/{email}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserWeb GetUserEmail(string email);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ModifyUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyUser(UserWeb userWeb);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/RemoveUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message RemoveUser(UserWeb userWeb);
    }
}
