﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IMessage
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Publish/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
         Message ApplyChanges(string MessagesList);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/SaveToDB/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message SaveMessagesToDB(List<MessageForCinema> messages);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetMessagesFromDB/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<MessageForCinema> GetMessagesFromDB();
    }
}