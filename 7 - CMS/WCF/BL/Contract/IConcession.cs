﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IConcession
    {

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UpdateSetting/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message UpdateSetting(ConcessionSetting setting);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetSetting/{key}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionSetting GetSetting(string key);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetSalesReportByDate/{date}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]        
        ConcessionTransactionResponse GetSalesReportByDate(string date);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetFullSalesReportByDate/{date}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionTransactionResponse GetFullSalesReportByDate(string date);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetConcessionCatalogByTheater/{theaterId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionCatalogResponse GetConcessionCatalogByTheater(string theaterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetConcessionItem/{itemId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionItem GetConcessionItem(string itemId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ModifyModalConcessions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyModalConcessions(string ModalCinemaList);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetModalConcessionsStatus/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ModalCinema GetModalConcessionsStatus(ModalCinema modal);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetSalesReportFilterByDate/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message GetSalesReportFilterByDate(string date);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetSalesReportComponentsFilterByDate/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message GetSalesReportComponentsFilterByDate(string date);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetConcessionsItemsForCinema/{dinamicConnection}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<TransferItem> GetConcessionsItemsForCinema(string dinamicConnection = "");

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/VerifyItemsForTranslate/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ItemAndComponets> VerifyItemsForTranslate(List<TransferItem> items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/VerifyItemsForReverse/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ItemAndComponets> VerifyItemsForReverse(List<TransferItem> items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ConfirmItemsForTranslate/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ItemComponent> ConfirmItemsForTranslate(List<ItemComponent> items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ConfirmItemsForReverse/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ItemComponent> ConfirmItemsForReverse(List<ItemComponent> items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ReverseTranslate/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ItemComponent> ReverseTranslate(List<ItemComponent> items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UpdateCandiesTable/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TransferPack UpdateCandiesTable(PackViewModel items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/DecreaseCandiesTable/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TransferPack DecreaseCandiesTable(PackViewModel items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetConcessionsItemsInCinema/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ItemAndComponets> GetConcessionsItemsInCinema(string CinemaId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetAviableConcessionsItemsInCinema/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<TransferItem> GetAviableConcessionsItemsInCinema(string CinemaId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ModifyCandiesWeb/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ModifyCandiesWeb(string data);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetAllModalConcessionsStatus/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ModalCinema> GetAllModalConcessionsStatus();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetAllConcecion/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<TransferItem> GetAllConcecion(string CinemaId);

    }
}
