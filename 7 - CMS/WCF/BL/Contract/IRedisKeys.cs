﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IRedisKeys
    {

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetRedisKey", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        RedisKey GetKeyRedis(RedisKey Redis);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/All", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<RedisKey> GetListRedisKeys();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Add", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message AddKeyRedis(RedisKey Redis);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Modify", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyKeyRedis(RedisKey Redis);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Remove", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message RemoveKeyRedis(RedisKey Redis);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ExecuteKey", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Message> ExecuteKeyRedis(RedisKey Redis);
    }
}



