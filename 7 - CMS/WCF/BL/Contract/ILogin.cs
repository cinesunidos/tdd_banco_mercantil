﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface ILogin
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/LogIn/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        LoginUser GetUserData(LoginUser Usr);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Users/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<LoginUser> GetUser();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Create/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message CreateUser(LoginUser Usr);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Update/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyUser(LoginUser User);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Delete/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message DeleteUser(LoginUser User);

    }
}
