﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IAdmin
    {

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/CreateBlackList/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message CreateBlackList(UserWeb userWeb);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetBlackList/{email},{platform}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserWeb GetBlackList(string email, string platform);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/All", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<BlackList_User> GetAllBlackListUsers();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetByIdCard/{IdCard}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        BlackList_User GetBlackListUserByIdCard(string IdCard);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetByEmail/{Email}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        BlackList_User GetBlackListUserByEmail(string Email);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/DeleteBlackList/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message Delete_BlackListUser(BlackList_User BlackList_User);      

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ListAudit/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Audit> ListAudit(AuditParameters parameters);
    }            
}
