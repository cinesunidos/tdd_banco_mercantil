﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface ILoadMovies
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetLoadMovies/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<LoadMovies> GetLoadMovies();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/FileGetRediCache/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message FileGetRediCache(string redicache);
    }
}
