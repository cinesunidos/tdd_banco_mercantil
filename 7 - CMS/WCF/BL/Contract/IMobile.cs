﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IMobile
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UserMobileIdCard/{idCard}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserWeb GetUserMobileIdCard(string idCard);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UserMobileEmail/{email}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserWeb GetUserMobileEmail(string email);

        [OperationContract]
        
        [WebInvoke(Method = "POST", UriTemplate = "/ModifyUserMobile/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyUserMobile(UserWeb user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/RemoveUserMobile/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message RemoveUserMobile(UserWeb user);
    }
}