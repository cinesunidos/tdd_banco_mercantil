﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IPromotions
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPromotions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Promotion> GetPromotions();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Update/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyPromotions(Promotion promo);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Create/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message CreatePromotions(Promotion promo);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Delete/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message DeletePromotions(Promotion promo);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ApplyChanges/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ApplyChanges(string promo);
    }
}
