﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.BL.Contract
{
    [ServiceContract]
    public interface IManagement
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetRole/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<BO.Role> GetRole();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/CreateRole/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message CreateRole(BO.Role role);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ModifyRole/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyRole(BO.Role role);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/RemoveRole/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message RemoveRole(BO.Role role);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPermissions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<BO.Permissions> GetPermissions();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetPermissionsByRole/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<BO.Permissions> GetAccessByRole(Role role);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/CreatePermissions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message CreatePermission(BO.Permissions perm);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ModifyPermissions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message ModifyPermission(BO.Permissions perm);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/RemovePermissions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message RemovePermission(BO.Permissions perm);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/AddPermissionByRole/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message AddPermissionByRole(string[] perm);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/DeletePermissionByRole/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Message DeletePermissionByRole(string[] perm);
    }
}
