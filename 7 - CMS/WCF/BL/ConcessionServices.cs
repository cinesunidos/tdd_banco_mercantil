﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class ConcessionServices : IConcession
    {
        private IConcessionSettingsDAL m_IConcessionSettingsDAL;
        private IConcessionTransactionsDAL m_IConcessionTransactionsDAL;

        public ConcessionServices(IConcessionSettingsDAL iConcessionSettingsDAL, IConcessionTransactionsDAL iConcessionTransactionsDAL) : base()
        {
            m_IConcessionSettingsDAL = iConcessionSettingsDAL;
            m_IConcessionTransactionsDAL = iConcessionTransactionsDAL;
        }

        public ConcessionSetting GetSetting(string key)
        {
            return m_IConcessionSettingsDAL.GetSetting(key);
        }

        public Message UpdateSetting(ConcessionSetting setting)
        {
            return m_IConcessionSettingsDAL.UpdateSetting(setting.Key, setting.Value);
        }

        public ConcessionTransactionResponse GetSalesReportByDate(string date)
        {
            return m_IConcessionTransactionsDAL.GetConcessionsSalesReportByDate(date);
        }

        public ConcessionTransactionResponse GetFullSalesReportByDate(string date)
        {
            return m_IConcessionTransactionsDAL.GetFullSalesReportByDate(date);
        }

        public ConcessionCatalogResponse GetConcessionCatalogByTheater(string theaterId)
        {
            return m_IConcessionTransactionsDAL.GetConcessionCatalogByTheater(theaterId);
        }

        public ConcessionItem GetConcessionItem(string itemId)
        {
            return m_IConcessionTransactionsDAL.GetConcessionItem(itemId);
        }

        public Message ModifyModalConcessions(string ModalCinemaList)
        {
            return m_IConcessionTransactionsDAL.ModifyModalConcessions(ModalCinemaList);
        }

        public Message GetSalesReportFilterByDate(string date)
        {
            return m_IConcessionTransactionsDAL.GetSalesReportFilterByDate(date);
        }

        public Message GetSalesReportComponentsFilterByDate(string date)
        {
            return m_IConcessionTransactionsDAL.GetSalesReportComponentsFilterByDate(date);
        }

        public ModalCinema GetModalConcessionsStatus(ModalCinema modal)
        {
            return m_IConcessionTransactionsDAL.GetModalConcessionsStatus(modal);
        }

        public List<TransferItem> GetConcessionsItemsForCinema(string dinamicConnection)
        {
            if (dinamicConnection == "{dinamicConnection}")
            {
                dinamicConnection = null;
            }
            return m_IConcessionTransactionsDAL.GetConcessionsItemsForCinema(dinamicConnection);
        }

        public List<ItemAndComponets> VerifyItemsForTranslate(List<TransferItem> items)
        {
            return m_IConcessionTransactionsDAL.VerifyItemsForTranslate(items);
        }

        public List<ItemComponent> ConfirmItemsForTranslate(List<ItemComponent> items)
        {
            return m_IConcessionTransactionsDAL.ConfirmItemsForTranslate(items);
        }

        public TransferPack UpdateCandiesTable(PackViewModel items)
        {
            return m_IConcessionTransactionsDAL.UpdateCandiesTable(items);
        }

        public List<ItemComponent> ReverseTranslate(List<ItemComponent> items)
        {
            return m_IConcessionTransactionsDAL.ReverseTranslate(items);
        }

        public List<ItemAndComponets> GetConcessionsItemsInCinema(string CinemaId)
        {



            return m_IConcessionTransactionsDAL.GetConcessionsItemsInCinema(CinemaId);
        }

        public List<TransferItem> GetAviableConcessionsItemsInCinema(string CinemaId)
        {
            return m_IConcessionTransactionsDAL.GetAviableConcessionsItemsInCinema(CinemaId);
        }

        public List<ItemComponent> ConfirmItemsForReverse(List<ItemComponent> items)
        {
            return m_IConcessionTransactionsDAL.ConfirmItemsForReverse(items);
        }

        public List<ItemAndComponets> VerifyItemsForReverse(List<TransferItem> items)
        {
            return m_IConcessionTransactionsDAL.VerifyItemsForReverse(items);
        }

        public TransferPack DecreaseCandiesTable(PackViewModel items)
        {
            return m_IConcessionTransactionsDAL.DecreaseCandiesTable(items);
        }

        public string ModifyCandiesWeb(string data)
        {
            return m_IConcessionTransactionsDAL.ModifyCandiesWeb(data);
        }
        public List<ModalCinema> GetAllModalConcessionsStatus()
        {
            return m_IConcessionTransactionsDAL.GetAllModalConcessionsStatus();
        }
        public List<TransferItem> GetAllConcecion(string CinemaID)
        {
            return m_IConcessionTransactionsDAL.GetAllConcecion(CinemaID);
        }

    }
}