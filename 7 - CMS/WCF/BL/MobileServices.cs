﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL;

namespace WCF.BL
{
    public class MobileServices : IMobile
    {
        private IMobileDAL m_IMobileDAL;

        public MobileServices(IMobileDAL iMobileDAL)
            : base()
        {
            m_IMobileDAL = iMobileDAL;
        }
        public UserWeb GetUserMobileIdCard(string idCard)
        {
            return m_IMobileDAL.GetUserMobileIdCard(idCard);
        }
        public UserWeb GetUserMobileEmail(string email)
        {
            return m_IMobileDAL.GetUserMobileEmail(email);
        }
        public Message ModifyUserMobile(UserWeb user)
        {
            return m_IMobileDAL.ModifyUserMobile(user, false);            
        }
        public Message RemoveUserMobile(UserWeb user)
        {
            return m_IMobileDAL.RemoveUserMobile(user);

        }
        
    }
}