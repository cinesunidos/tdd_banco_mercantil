﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;

namespace WCF.BL
{
    public class PromotionsServices : IPromotions
    {
        private IPromotionsDAL m_PromotionsDAL;

        public PromotionsServices(IPromotionsDAL iPromotionsDAL)
            : base()
        {
            m_PromotionsDAL = iPromotionsDAL;
        }

        public List<Promotion> GetPromotions()
        {
            return m_PromotionsDAL.GetPromotions();
        }

        public Message ModifyPromotions(Promotion promo)
        {
            return m_PromotionsDAL.ModifyPromotions(promo);
        }

        public Message CreatePromotions(Promotion promo)
        {
            return m_PromotionsDAL.CreatePromotions(promo);
        }

        public Message DeletePromotions(Promotion promo)
        {
            return m_PromotionsDAL.DeletePromotions(promo);
        }

        public Message ApplyChanges(string promo)
        {
            return m_PromotionsDAL.ApplyChanges(promo);
        }
    }
}
