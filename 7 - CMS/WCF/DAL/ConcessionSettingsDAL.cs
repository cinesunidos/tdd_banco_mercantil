﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF.DAL
{
    public class ConcessionSettingsDAL : IConcessionSettingsDAL
    {
        public Message UpdateSetting(string key, string value)
        {
            Message msg = new BO.Message();

            try
            {
                using (concessionEntities entities = new concessionEntities())
                {
                    

                    var query = (from item in entities.Concessions_Settings
                                 where item.Key == key
                                 select item).FirstOrDefault();

                    if(query != null)
                    {
                        query.Value = value;
                    }
                    else
                    {
                        ConcessionSetting setting = new ConcessionSetting();
                        setting.Key = key;
                        setting.Value = value;
                    }
                    
                    entities.SaveChanges();
                    msg.error = false;
                    msg.message = "Los cambios se realizaron exitosamente";
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }

            return msg;
        }

        public ConcessionSetting GetSetting(string key)
        {
            ConcessionSetting setting = new ConcessionSetting();
            
            using (concessionEntities entities = new concessionEntities())
            {
                var query = (from item in entities.Concessions_Settings
                                where item.Key == key
                                select item).FirstOrDefault();

                if (query != null)
                {
                    setting.Key = query.Key;
                    setting.Value = query.Value;
                    return setting;
                }
            }
            
            return null;
        }
    }
}