﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Security.Cryptography.X509Certificates;
using Microsoft.WindowsAzure.Management.Compute;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using Microsoft.WindowsAzure;
using System.Collections.Specialized;


namespace WCF.DAL
{
    public class LoadMoviesDAL : ILoadMoviesDAL
    {
        public List<BO.LoadMovies> GetLoadMovies()
        {
            List<BO.LoadMovies> ListLoadMovies = new List<BO.LoadMovies>();
            SqlConnection connection = new SqlConnection();
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LoadMoviesConnection"].ToString());
                connection.Open();

                SqlCommand cmd = new SqlCommand("SELECT Film_strHOFilmCode, Film_strTitle, Film_dtmOpeningDate " +
                                                "FROM tblFilm " +
                                                "WHERE YEAR(Film_dtmOpeningDate) BETWEEN(YEAR(GETDATE()) - 1) AND (YEAR(GETDATE()) + 1) " +
                                                "ORDER BY Film_strTitle", connection);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListLoadMovies.Add(new LoadMovies
                    {
                        HO = reader.GetString(0),
                        Title = reader.GetString(1),
                    });
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return ListLoadMovies;
        }

        public Message FileGetRediCache(string redicache)
        {
            Message message = new Message();
            try
            {
                var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
                var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
                var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
                var cert = new X509Certificate2(Convert.FromBase64String(certString));
                var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
                var computeManagementClient = new ComputeManagementClient(credentials);

                var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysCloudServiceMobile"].ToString();

                var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
                var deployments = cloudServiceDetails.Deployments;

                foreach (var deployment in deployments)
                {
                    foreach (var instance in deployment.RoleInstances)
                    {
                        if (deployment.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production)
                        {
                            using (var client = new WebClient())
                            {
                                var values = new NameValueCollection();
                                values["redicache"] = redicache;
                                var response = client.UploadValues("http://" + instance.IPAddress + "/blackberry.asmx/FileGetRedisCache", values);
                                var responseString = Encoding.Default.GetString(response);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message.error = true;
                message.message = ex.Message;
            }

            return message;
        }
    }
}
