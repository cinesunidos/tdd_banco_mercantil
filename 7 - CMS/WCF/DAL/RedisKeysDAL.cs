﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;
using WCF.DAL.Contract;
using System.Net.Http;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Management.Compute;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Threading;

namespace WCF.DAL
{
    public class RedisKeysDAL : IRedisKeysDAL
    {
        public RedisKey GetKeyRedis(RedisKey Redis)
        {
            RedisKey _Redis = new RedisKey();
            using (adminEntities RedisKeysEntities = new adminEntities())
            {
                Redis_RedisKeys _redis = null;
                var query = _redis;
                if (!string.IsNullOrEmpty(Redis.Id))
                {
                    query = (from RedisKeys in RedisKeysEntities.Redis_RedisKeys
                             where RedisKeys.Id == new Guid(Redis.Id)
                             select RedisKeys).FirstOrDefault();

                    if (query != null)
                    {
                        _Redis.Id = query.Id.ToString();
                        _Redis.Value = query.Value;
                        _Redis.Description = query.Description;
                    }
                    else
                    {
                        _Redis = null;
                    }

                    return _Redis;
                }
                if (!string.IsNullOrEmpty(Redis.Value))
                {
                    query = (from rediskey in RedisKeysEntities.Redis_RedisKeys
                             where rediskey.Value == Redis.Value.Trim()
                             select rediskey).FirstOrDefault();
                    if (query != null)
                    {
                        _Redis.Id = query.Id.ToString();
                        _Redis.Value = query.Value;
                        _Redis.Description = query.Description;
                    }
                    else
                    {
                        _Redis = null;
                    }

                    return _Redis;
                }
            }
            return _Redis;
        }

        public List<RedisKey> GetListRedisKeys()
        {
            List<RedisKey> ListRedisKeys = new List<RedisKey>();

            using (adminEntities RedisKeysEntities = new adminEntities())
            {
                var query = (from rediskeys in RedisKeysEntities.Redis_RedisKeys
                             select rediskeys).ToList();
                if (query.Count > 0)
                {
                    foreach (var item in query)
                    {
                        RedisKey RedisKey = new RedisKey();
                        RedisKey.Id = item.Id.ToString();
                        RedisKey.Value = item.Value;
                        RedisKey.Description = item.Description;

                        ListRedisKeys.Add(RedisKey);
                    }
                }
                else
                {
                    ListRedisKeys = null;
                }
            }
            return ListRedisKeys;
        }
        public Message AddKeyRedis(RedisKey Redis)
        {
            Message message = new Message();
            if ((!string.IsNullOrEmpty(Redis.Value)) && (!string.IsNullOrEmpty(Redis.Description)))
            {
                var redisKey = GetKeyRedis(Redis);

                if (redisKey == null)
                {
                    using (adminEntities RedisKeysEntities = new adminEntities())
                    {
                        Redis_RedisKeys _KeyRedis = new Redis_RedisKeys();
                        _KeyRedis.Id = Guid.NewGuid();
                        _KeyRedis.Value = Redis.Value;
                        _KeyRedis.Description = Redis.Description;

                        RedisKeysEntities.Redis_RedisKeys.Add(_KeyRedis);
                        try
                        {
                            RedisKeysEntities.SaveChanges();
                            message.error = false;
                            message.message = "LLave del Redis creada satisfactoriamente";
                        }
                        catch (Exception ex)
                        {
                            message.error = true;
                            message.message = ex.InnerException.Message;
                        }
                    }
                }
                else
                {
                    message.error = false;
                    message.message = "Llave del redis existente";
                }
            }
            else
            {
                message.error = true;
                message.message = "Imposible guardar, se requiere de un Valor y Descripción para crear la llave del Redis";
            }
            return message;
        }
        public Message ModifyKeyRedis(RedisKey Redis)
        {
            Message message = new Message();
            if ((!string.IsNullOrEmpty(Redis.Value)) && (!string.IsNullOrEmpty(Redis.Description)))
            {
                using (adminEntities RedisKeysEntities = new adminEntities())
                {
                    var query = (from rediskey in RedisKeysEntities.Redis_RedisKeys
                                 where rediskey.Id == new Guid(Redis.Id)
                                 select rediskey).FirstOrDefault();
                    if (query != null)
                    {
                        query.Id = new Guid(Redis.Id);
                        query.Value = Redis.Value;
                        query.Description = Redis.Description;
                        try
                        {
                            RedisKeysEntities.SaveChanges();
                            message.error = false;
                            message.message = "Cambios registrados satisfactoriamente";
                        }
                        catch (Exception ex)
                        {
                            message.error = true;
                            message.message = ex.InnerException.Message;
                        }
                    }
                    else
                    {
                        message.error = true;
                        message.message = "Llave no encontrada, no fue posible guardar los cambios";
                    }
                }
            }
            else
            {
                message.error = true;
                message.message = "Imposible guardar, se requiere de un Valor y Descripción para modificar la llave del Redis";
            }
            return message;
        }
        public Message RemoveKeyRedis(RedisKey Redis)
        {
            Message message = new Message();
            if ((!string.IsNullOrEmpty(Redis.Value)) && (!string.IsNullOrEmpty(Redis.Description)))
            {
                using (adminEntities RedisKeysEntities = new adminEntities())
                {
                    var query = (from rediskey in RedisKeysEntities.Redis_RedisKeys
                                 where rediskey.Id == new Guid(Redis.Id)
                                 select rediskey).FirstOrDefault();
                    if (query != null)
                    {
                        RedisKeysEntities.Redis_RedisKeys.Remove(query);
                        try
                        {
                            RedisKeysEntities.SaveChanges();
                            message.error = false;
                            message.message = "Llave del Redis eliminada satisfactoriamente";
                        }
                        catch (Exception ex)
                        {
                            message.error = true;
                            message.message = ex.InnerException.Message;
                        }
                    }
                    else
                    {
                        message.error = true;
                        message.message = "Llave no encontrada, no fue posible eliminar";
                    }
                }
            }
            else
            {
                message.error = true;
                message.message = "Imposible eliminar, se requiere de un Valor y Descripción para modificar la llave del Redis";
            }
            return message;
        }

        public List<Message> ExecuteKeyRedis(RedisKey Redis)
        {
            List<Message> messages = new List<Message>();

            #region establish connection to CloudServices 
            try
            {
                switch (Redis.Value.ToLower())
                {
                    case "deletecachecinesunidosmobile":
                        messages = DeleteMovileCache();
                        break;
                    case "deletemvccache":
                        messages.AddRange(DeleteWebCache());
                        break;
                    default:
                        messages.AddRange(DeleteWsCache(Redis.Value));
                        break;
                }
            }
            catch (Exception ex)
            {
                messages.Add(new Message
                {
                    message = ex.Message,
                    error = true
                });
            }
            #endregion
            return messages;
        }


        /// <summary>
        /// RM - Limpia el cache de las instancias del despliegue de capa 2 en la pagina web
        /// </summary>
        /// <returns></returns>
        private List<Message> DeleteWsCache(string RedisKey)
        {
            List<Message> messages = new List<Message>();
            var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
            var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
            var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
            var cert = new X509Certificate2(Convert.FromBase64String(certString));
            var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
            var computeManagementClient = new ComputeManagementClient(credentials);
            var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysCloudService"].ToString();

            var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
            ///obtener solo los deploy de produccion (es decir, no trae staging)
            var deployments = cloudServiceDetails.Deployments.FirstOrDefault(d => d.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production);

            foreach (var instance in deployments.RoleInstances)
            {
                //llamar a servicio via rest por cada IP interna.
                string url = string.Format("http://{0}/RedisCacheManagement.svc/{1}/{2}", instance.IPAddress.ToString(), RedisKey, token);

                HttpClient client = new HttpClient();
                HttpResponseMessage wcfResponse = client.GetAsync(url).Result;
                HttpContent stream = wcfResponse.Content;
                var data = stream.ReadAsStringAsync();
                messages.Add(new Message
                {
                    message = $"Nodo {deployments.RoleInstances.IndexOf(instance)} de capa 2 :" + String.Join("", data.Result.Split('\\', '"', '-'))
                });
            }
            return messages;
        }

        /// <summary>
        /// RM - limpia la cache de las instancias de despliegue de Capa 1 en la pagina web
        /// </summary>
        /// <returns></returns>
        private List<Message> DeleteWebCache()
        {
            List<Message> messages = new List<Message>();
            var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
            var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
            var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
            var cert = new X509Certificate2(Convert.FromBase64String(certString));
            var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
            var computeManagementClient = new ComputeManagementClient(credentials);
            var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysFront"].ToString();

            var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
            ///obtener solo los deploy de produccion (es decir, no trae staging)
            var deployments = cloudServiceDetails.Deployments.FirstOrDefault(d => d.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production);

            foreach (var instance in deployments.RoleInstances)
            {
                //llamar a servicio via rest por cada IP interna.
                string url = string.Format("http://{0}/security/LimpiarCacheFront/?token={1}", instance.IPAddress.ToString(), token);

                using (var handler = new WebRequestHandler())
                {
                    //Se realiza Bypass de certidicado SSL 
                    handler.ServerCertificateValidationCallback = delegate { return true; };

                    using (var client = new HttpClient(handler))
                    {
                        HttpResponseMessage wcfResponse = client.GetAsync(url).Result;
                        HttpContent stream = wcfResponse.Content;
                        var data = stream.ReadAsStringAsync();
                        messages.Add(new Message
                        {
                            message = $"Nodo {deployments.RoleInstances.IndexOf(instance)} de capa 1 :" + String.Join("", data.Result.Split('\\', '"', '-'))
                        });
                    }
                }
            }
            return messages;
        }

        /// <summary>
        /// RM - limpiar la cache de los moviles.
        /// </summary>
        /// <param name="Redis"></param>
        /// <returns></returns>
        private List<Message> DeleteMovileCache()
        {
            List<Message> messages = new List<Message>();
            var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
            var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
            var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
            var cert = new X509Certificate2(Convert.FromBase64String(certString));
            var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
            var computeManagementClient = new ComputeManagementClient(credentials);
            var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysCloudServiceMobile"].ToString();

            var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
            var deployments = cloudServiceDetails.Deployments.FirstOrDefault(d => d.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production);

            foreach (var instance in deployments.RoleInstances)
            {
                string URL = "http://" + instance.IPAddress + "/blackberry.asmx/DeleteCache";
                var httpClient = new HttpClient();
                var httpResponseMessage = httpClient.GetAsync(URL).Result;
                messages.Add(new Message
                {
                    message = $"nodo {deployments.RoleInstances.IndexOf(instance)} de moviles :" + "Los archivos cache de los mobiles fueron eliminados"
                });
            }
            return messages;
        }
    }
}
