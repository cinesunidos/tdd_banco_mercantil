﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF.DAL
{
    public class Management : IManagementDAL
    {
        public List<Access> GetAccessByRole(Guid _RoleId)
        {
            List<Access> LstAccess = new List<Access>();

            if (_RoleId != null)
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = from Access in adminEntities.Access
                                where Access.RoleId == _RoleId
                                select Access;

                    if (query != null)
                    {
                        foreach (var result in query)
                        {
                            Access _Access = new Access();
                            _Access.id = result.id;
                            _Access.Name = result.Name;
                            _Access.RoleId = result.RoleId;

                            LstAccess.Add(_Access);
                        }
                    }
                }
            }
            return LstAccess;
        }
        public Message CreateRole(BO.Role role)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                Role roleTable = new Role();

                roleTable.roleId = Guid.NewGuid();
                roleTable.name = role.name;
                try
                {
                    adminEntities.Role.Add(roleTable);
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "El usuario se creó en la Lista Negra exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No se puedo crear el Rol ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }
        public Message ModifyRole(BO.Role role)
        {
            Message msg = new Message();
            try
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from roleTable in adminEntities.Role
                                 where roleTable.roleId == role.roleId
                                 select roleTable).FirstOrDefault();

                    query.name = role.name;

                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "Los cambios se realizaron exitosamente";
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }
            return msg;
        }
        public Message RemoveRole(BO.Role role)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                var query = (from roleTable in adminEntities.Role
                                  where roleTable.roleId ==(role.roleId)
                                  select roleTable).FirstOrDefault();

                adminEntities.Role.Remove(query);
                try
                {
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "Se eliminó exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No fue posible eliminar el Rol. Error ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
                return null;
        }
    }
}