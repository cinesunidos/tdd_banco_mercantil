﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;
using System.Xml;
using System.Xml.Linq;

namespace WCF.DAL
{
    public class UserMobileDAL : IMobileDAL
    {
        //Crea un documento xml y separa todos los elementos, luego los pasa en una lista string para 
        //que el objeto UserMobile tome datos de este XML
       private List<string> ReturnListXML(string _XmlContext)
        {
            XmlDocument _XmlDoc = new XmlDocument();
            List<string> List_str = new List<string>();
            /*
               0    NombreUsuario
               1    ApellidoUsuario
               2    TelefonoUsuario
               3    CelularUsuario
               4    DireccionUsuario
               5    CodigoPostal
               6    CiudadUsuario
               7    EstadoUsuario
               8    CiudadResUsuario
                */

            if ((_XmlContext != null) || (_XmlContext != ""))
            {
                _XmlDoc.LoadXml(_XmlContext);
                foreach (XmlNode node in _XmlDoc.DocumentElement.ChildNodes)
                {
                    string text = node.InnerText; //or loop through its children as well
                    List_str.Add(text);
                }
            }
            return List_str;

        }

       public UserWeb GetUserMobileEmail(string email)
        {
            UserWeb userWeb = new UserWeb();            
            using (mobileEntities _mobileEntities = new mobileEntities())
            {
                var query = (from USERS in _mobileEntities.USERS
                             where USERS.LOGIN == email
                             select USERS).FirstOrDefault();
                if (query != null)
                {
                    List<string> Personal = new List<string>();
                    Personal = ReturnListXML(query.PXML);
                    userWeb.Active = false;
                    userWeb.Address = Personal[4].Trim();
                    userWeb.City = Personal[6].Trim();
                    if (string.IsNullOrEmpty(query.EMAIL)) userWeb.Email = query.LOGIN.Trim(); else userWeb.Email = query.EMAIL.Trim();                    
                    userWeb.Id = query.USERID.ToString();
                    if (string.IsNullOrEmpty(query.EXTERNALID)) userWeb.IdCard = ""; else userWeb.IdCard = query.EXTERNALID.Trim();
                    userWeb.LastName = Personal[1].Trim();
                    userWeb.MassMailSubcription = Convert.ToBoolean(query.UNSUBSCRIBE);
                    if (string.IsNullOrEmpty(Personal[3])) userWeb.MobilePhone = "";                    
                    userWeb.Name = Personal[3].Trim();
                    userWeb.Password = query.PASSWORD.Trim();
                    userWeb.Phone = Personal[2].Trim();
                    userWeb.State = Personal[7].Trim();
                    userWeb.Platform = "Mobile";
                }
                else
                    userWeb = null;
            }
            return userWeb;
        }

       public UserWeb GetUserMobileIdCard(string idCard)
       {
            UserWeb userWeb = new UserWeb();

            using (mobileEntities _mobileEntities = new mobileEntities())
            {
                var query = (from USERS in _mobileEntities.USERS
                             where USERS.EXTERNALID == idCard
                             select USERS).FirstOrDefault();
                if (query != null)
                {
                    List<string> Personal = new List<string>();
                    Personal = ReturnListXML(query.PXML);
                    userWeb.Active = false;
                    userWeb.Address = Personal[4].Trim();
                    userWeb.City = Personal[6].Trim();
                    if (string.IsNullOrEmpty(query.EMAIL)) userWeb.Email = query.LOGIN.Trim(); else userWeb.Email = query.EMAIL.Trim();
                    userWeb.Id = query.USERID.ToString();
                    userWeb.IdCard = query.EXTERNALID.Trim();
                    userWeb.LastName = Personal[1].Trim();
                    userWeb.MassMailSubcription = Convert.ToBoolean(query.UNSUBSCRIBE);
                    if (string.IsNullOrEmpty(Personal[3])) userWeb.MobilePhone = "";
                    userWeb.Name = Personal[3].Trim();
                    userWeb.Password = query.PASSWORD.Trim();
                    userWeb.Phone = Personal[2].Trim();
                    userWeb.State = Personal[7].Trim();
                    userWeb.Platform = "Mobile";
                }
                else
                    userWeb = null;
            }
            return userWeb;
        }

       public Message ModifyUserMobile(UserWeb user, bool blackList)
        {
            Message msg = new Message();

            AdminDAL adminDal = new AdminDAL();
            UserWeb userWeb = new UserWeb();

            if (blackList.Equals(false))
            {
                userWeb = adminDal.GetBlackList(user.Email, "Mobile");
            }

            if (userWeb.Id != null)
            {
                msg.error = false;
                msg.message = "El Usuario no se puede modificar porque se encuentra en la Lista Negra";
            }
            else
            {
                try
                {
                    using (mobileEntities _mobileEntities = new mobileEntities())
                    {
                        UserWeb _User = new UserWeb();
                        int usrid = int.Parse(user.Id);
                        var query =  (from USERS in _mobileEntities.USERS
                                      where USERS.USERID == usrid
                                      select USERS).FirstOrDefault();
                        //se setean los valores antes de modificarlo para usarlo en auditoria
                        _User.Email = query.LOGIN;
                        _User.IdCard = query.EXTERNALID;
                        if (_User.MassMailSubcription == true) query.UNSUBSCRIBE = 1; else query.UNSUBSCRIBE = 0;
                        _User.Password = query.PASSWORD;
                        //----------                      
                        if (blackList.Equals(false))
                        {
                            query.EMAIL = user.Email;
                            query.EXTERNALID = user.IdCard;
                            //query.UNSUBSCRIBE = int.Parse  (user.massMailSubcription);
                            if (user.MassMailSubcription == true) query.UNSUBSCRIBE = 1; else query.UNSUBSCRIBE = 0;
                            query.PASSWORD = user.Password;
                        }
                        else
                        {
                            query.EXTERNALID = user.IdCard + "**";
                            query.UNSUBSCRIBE = 0;
                        }
                        _mobileEntities.SaveChanges();
                        msg.error = false;
                        msg.message = "Los cambios se realizaron exitosamente";

                        #region create auditoria
                        Admin_Audit audit = new Admin_Audit();
                        audit.Id = Guid.NewGuid();
                        //Valor Seteado, cambiar cuando se realice el login de un usuario
                        //audit.UserId = new Guid("35E78CE3-CE7A-409B-B4D7-BD6BAB5C8AE1");
                        audit.UserId = user.userAudit;
                        audit.Department = user.Department;
                        //audit.UserId = new Guid(query.EXTERNALID);
                        audit.Action = "MOD";
                        audit.EventDatetime = DateTime.Now;
                        audit.Event =
                            string.Format(
                                 "IdCard oldValue:'{0}' newValue:'{1}'-Email OldValue'{2}' newValue:'{3}'-pass OldValue'{4}' newValue:'{5}' -massMail OldValue'{6}' newValue:'{7}'",
                                 _User.IdCard, user.IdCard,
                                 _User.Email, user.Email,
                                 _User.Password, user.Password,
                                 _User.MassMailSubcription, user.MassMailSubcription);
                        AdminDAL adminDAL = new AdminDAL();
                        adminDAL.CreateAudit(audit);
                        #endregion
                    }
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No fue posible realizar las modificaciones", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }

       public Message RemoveUserMobile(UserWeb user)
        {
            Message msg = new Message();

            AdminDAL adminDal = new AdminDAL();
            if (adminDal.GetBlackList(user.Email, "Mobile") != null)
            {
                msg.error = false;
                msg.message = "El Usuario no se puede eliminar porque se encuentra en la Lista Negra";
            }
            else
            {
                using (mobileEntities _mobileEntities = new mobileEntities())
                {
                    var UserId = int.Parse(user.Id);
                    var queryUSERS = (from USERS in _mobileEntities.USERS                                          
                                      where USERS.USERID == UserId
                                      select USERS);
                    
                    foreach (var rs in queryUSERS)
                    {                        
                        _mobileEntities.USERS.Remove(rs);
                    }
                    try
                    {
                        _mobileEntities.SaveChanges();
                        msg.error = false;
                        msg.message = "Se eliminó el usuario exitosamente";

                        #region create auditoria
                        Admin_Audit audit = new Admin_Audit();
                        audit.Id = Guid.NewGuid();
                        audit.Department = user.Department;
                        audit.UserId = user.userAudit;
                        audit.Action = "DEL";
                        audit.EventDatetime = DateTime.Now;
                        audit.Event = string.Format("EXTERNALID:{0}LOGIN:{1}", user.IdCard, user.Email);
                        //Valor Seteado, cambiar cuando se realice el login de un usuario
                        //audit.UserId = new Guid("35E78CE3-CE7A-409B-B4D7-BD6BAB5C8AE1");
                        audit.UserId = user.userAudit;
                        AdminDAL adminDAL = new AdminDAL();
                        adminDAL.CreateAudit(audit);
                        #endregion
                    }
                    catch (Exception exs)
                    {
                        msg.error = true;
                        msg.message = string.Concat("{0}{1}", "No fue posible eliminar el usuario", exs.InnerException.Message);
                        msg.number = 0;
                    }
                }
            }
            return msg;
        }
    }
}
