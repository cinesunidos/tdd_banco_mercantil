﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF.DAL
{
    public class LoginUserDAL : ILoginUsersDAL
    {

        public Message LogOut(LoginUser User)
        {
            return null;
        }

        public LoginUser GetUserData(BO.LoginUser Usr)
        {
            BO.LoginUser LogUser = new LoginUser();

            Admin_LoginUsers _Usr = null;
            var query = _Usr;

            using (adminEntities LoginUserEntities = new adminEntities())
            {
                
                //LRAMIREZ 01/11/2016, To extract user data, using User Guid filter
                if ((Usr.Id != null) || (Usr.Id.ToString() != "00000000-0000-0000-0000-000000000000"))
                {
                    query = (from loginUsers in LoginUserEntities.Admin_LoginUsers
                             where loginUsers.Id == Usr.Id
                             select loginUsers).FirstOrDefault();
                }
                //LRAMIREZ 01/11/2016, To extract user data when the user make login 
                if ((!string.IsNullOrEmpty(Usr.UserId)) && (!string.IsNullOrEmpty(Usr.Password)))
                {
                    query = (from loginUsers in LoginUserEntities.Admin_LoginUsers
                             where loginUsers.UserId == Usr.UserId && loginUsers.Password == Usr.Password
                             select loginUsers).FirstOrDefault();
                }

                if (query != null)
                {
                    LogUser.Id = query.Id;
                    LogUser.Name = query.Name;
                    LogUser.Department = query.Department;
                    LogUser.Password = query.Password;
                    ManagementDAL Management = new ManagementDAL();
                    List<BO.Permissions> _Access = new List<BO.Permissions>();
                    LogUser.Permissions = Management.GetAccessByRole(query.RoleId);
                    LogUser.RoleId = query.RoleId;
                    LogUser.UserId = query.UserId;
                }

            }

            return LogUser;
        }



        public List<LoginUser> GetUser()
        {
            List<BO.LoginUser> ListLoginUser = new List<LoginUser>();

            using (adminEntities LoginUserEntities = new adminEntities())
            {
                var query = (from loginUsers in LoginUserEntities.Admin_LoginUsers
                             select loginUsers).ToList();

                if (query != null)
                {
                    foreach (var result in query)
                    {
                        BO.LoginUser LoginUser = new LoginUser();
                        LoginUser.Id = result.Id;
                        LoginUser.Name = result.Name;
                        LoginUser.Department = result.Department;
                        LoginUser.Password = result.Password;
                        LoginUser.Permissions = null;
                        LoginUser.RoleId = result.RoleId;
                        LoginUser.UserId = result.UserId;

                        ListLoginUser.Add(LoginUser);
                    }
                }
            }
            return ListLoginUser;
        }


        public Message CreateUser(BO.LoginUser User)
        {
            BO.LoginUser Usr = new BO.LoginUser();
            BO.Message msg = new BO.Message();
            //Validar si el usuario  se encuentra registrado
            Usr = GetUserData(User);

            if (Usr.Id != new Guid("00000000-0000-0000-0000-000000000000"))
            {
                msg.error = true;
                msg.message = "Este Usuario ya se encuentra registrado en el sistema";
            }
            else
            {
                //Crear Usuario
                using (adminEntities adminEntities = new adminEntities())
                {
                    Admin_LoginUsers LoginUserTable = new Admin_LoginUsers();

                    LoginUserTable.Department = User.Department;
                    LoginUserTable.Id = Guid.NewGuid();
                    LoginUserTable.Name = User.Name;
                    LoginUserTable.Password = User.Password;
                    LoginUserTable.RoleId = User.RoleId;
                    LoginUserTable.UserId = User.UserId;
                    try
                    {
                        adminEntities.Admin_LoginUsers.Add(LoginUserTable);
                        adminEntities.SaveChanges();
                        msg.error = false;
                        msg.message = "El Usuario se creó exitosamente";

                    }
                    catch (Exception exs)
                    {
                        msg.error = true;
                        msg.message = string.Concat("{0}{1}", "No se puedo crear el Usuario", exs.InnerException.Message);
                        msg.number = 0;
                    }
                }
            }
            return msg;
        }

        public Message ModifyUser(BO.LoginUser User)
        {
            Message msg = new BO.Message();
            try
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from loginUsers in adminEntities.Admin_LoginUsers
                                 where loginUsers.Id == User.Id
                                 select loginUsers).FirstOrDefault();

                    query.UserId = User.UserId;
                    query.Department = User.Department;
                    query.Name = User.Name;
                    query.Password = User.Password;
                    query.RoleId = User.RoleId;

                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "El Usuario ha sido modificado satisfactoriamente";                    
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }

            return msg;
        }

        public Message DeleteUser(BO.LoginUser User)
        {
            Message msg = new BO.Message();
            try
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from loginUsers in adminEntities.Admin_LoginUsers
                                 where loginUsers.Id == User.Id
                                 select loginUsers).FirstOrDefault();

                    adminEntities.Admin_LoginUsers.Remove(query);
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "El Usuario ha sido eliminado satisfactoriamente";
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }

            return msg;
        }

    }


}