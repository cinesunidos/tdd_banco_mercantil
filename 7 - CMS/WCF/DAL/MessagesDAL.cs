﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BO;
using WCF.DAL.Contract;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using Microsoft.WindowsAzure.Management.Compute;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json;

namespace WCF.DAL
{
    public class MessagesDAL : IMessagesDAL
    {
        public Message ApplyChange(string MessagesList)
        {
            var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysCloudService"].ToString();
            MessagesPack pack = new MessagesPack();
            pack.MessageList = MessagesList;
            pack.Token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
            Message message = new Message();

            if (cloudServiceName == "localhost")
            {
                Service ser = new Service();
                message = ser.Execute<MessagesPack, Message>("Contingency.PublishMessages", pack);
                return message;
            }
            else
            {
                //metodo para acceder a multiples instancias de cloudServices
                #region establish connection to CloudServices 
                try
                {
                    var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
                    var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
                    var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
                    var cert = new X509Certificate2(Convert.FromBase64String(certString));
                    var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
                    var computeManagementClient = new ComputeManagementClient(credentials);
                    var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
                    var deployments = cloudServiceDetails.Deployments;
                    #region Serializa el model
                    //XmlObjectSerializer obj = GetSerializer<T>();
                    XmlObjectSerializer obj = new DataContractJsonSerializer(typeof(MessagesPack));
                    MemoryStream stream = new MemoryStream();
                    obj.WriteObject(stream, pack);
                    string data = Encoding.UTF8.GetString(stream.ToArray(), 0, (int)stream.Length);
                    #endregion Serializa el model
                    foreach (var deployment in deployments)
                    {
                        foreach (var instance in deployment.RoleInstances)
                        {
                            if (deployment.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production)
                            {
                                //llamar a servicio via rest por cada IP interna.
                                string url = string.Format("http://{0}/Contingency.svc/Publish/", instance.IPAddress.ToString());
                                WebClient webClient1 = new WebClient();
                                webClient1.Headers.Add("ClientID", "111.111.111.120 ");
                                webClient1.Headers.Add("Content-type", "application/json");
                                webClient1.Encoding = Encoding.UTF8;
                                var result = webClient1.UploadString(url, "POST", data); //Ejecuta la llamada al servicio
                                message = JsonConvert.DeserializeObject<Message>(result);
                                //byte[] response = System.Text.Encoding.UTF8.GetBytes(result);
                                //return Convert<R>(response);
                                //stream = new MemoryStream(response);
                                //XmlObjectSerializer obj2 = new DataContractSerializer(typeof(Message));
                                //Message model = null;
                                //if (!stream.Length.Equals(0))
                                //{
                                //    message = obj.ReadObject(stream) as Message;
                                //}
                            }
                        }
                    }
                    return message;
                }
                catch (Exception ex)
                {
                    message.message = ex.ToString();
                    message.error = true;
                    message.number = 1;
                    return message;
                }
                #endregion
            }
        }

        public List<MessageForCinema> GetMessagesFromDB()
        {
            List<MessageForCinema> messages = new List<MessageForCinema>();
            try
            {
                using (adminEntities db = new adminEntities())
                {
                    foreach (var item in db.MessagesForCinemas)
                    {
                        MessageForCinema m = new MessageForCinema
                        {
                            Activo = item.Activo,
                            Category = (int)item.Category,
                            Cinema = item.Cinema.ToString(),
                            CinemaID = item.CinemaID,
                            City = item.City.ToString(),
                            Message = item.Message,
                            Retirar = item.Retirar
                        };
                        messages.Add(m);
                    }
                }
            }
            catch (Exception)
            {
            }
            return messages;
        }

        /// <summary>
        /// Metodo para guardar los mensajes en base de datos
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        public Message SaveMessagesToDB(List<MessageForCinema> messages)
        {
            //logica de guardado en DB
            Message message = new Message();
            try
            {
                using (adminEntities db = new adminEntities())
                {
                    db.Database.ExecuteSqlCommand("Truncate Table [MessagesForCinemas]");
                    foreach (MessageForCinema item in messages)
                    {
                        db.MessagesForCinemas.Add(new MessagesForCinema
                        {
                            Activo = item.Activo,
                            Category = item.Category,
                            Cinema = int.Parse(item.Cinema),
                            CinemaID = item.CinemaID,
                            City = int.Parse(item.City),
                            Message = item.Message,
                            Retirar = item.Retirar
                        });
                        db.SaveChanges();
                    }
                }
                message.error = false;
                message.message = "Mensajes Guardados en DB";
                message.number = 0;
            }
            catch (Exception ex)
            {
                message.error = true;
                message.message = ex.Message;
                message.number = 1;
            }
            return message;
        }
    }
}
