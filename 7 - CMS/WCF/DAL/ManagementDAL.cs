﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF.DAL
{
    public class ManagementDAL : IManagementDAL
    {
        public List<BO.Permissions> GetAccessByRole(Guid _RoleId)
        {
            List<BO.Permissions> LstPermissions = new List<BO.Permissions>();

            if (_RoleId != null)
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = from RP in adminEntities.Admin_Role_Permission
                                join P in adminEntities.Admin_Permissions on RP.permissionsId equals P.id
                                join R in adminEntities.Admin_Role on RP.roleId equals R.roleId
                                where RP.roleId == _RoleId
                                select new { id = P.id, Name = P.Name };
                    if (query != null)
                    {

                        foreach (var result in query)
                        {
                            BO.Permissions _Permission = new BO.Permissions();
                            _Permission.id = result.id;
                            _Permission.Name = result.Name;
                            //_Permission.RoleId = result.RoleId;
                            LstPermissions.Add(_Permission);
                        }
                    }
                }
            }
            return LstPermissions;
        }

        public List<BO.Role> GetRole()
        {
            List<BO.Role> roleList = new List<BO.Role>();

            using (adminEntities adminEntities = new adminEntities())
            {
                var query = (from roleTable in adminEntities.Admin_Role
                             select roleTable).ToList();

                foreach (var rs in query)
                {
                    BO.Role role = new BO.Role();
                    role.name = rs.name;
                    role.roleId = rs.roleId;

                    roleList.Add(role);
                }
            }
            return roleList;
        }

        public Message CreateRole(BO.Role role)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                Admin_Role roleTable = new Admin_Role();

                roleTable.roleId = Guid.NewGuid();
                roleTable.name = role.name;
                try
                {
                    adminEntities.Admin_Role.Add(roleTable);
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "El Rol se creó exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No se puedo crear el Rol ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }

        public Message ModifyRole(BO.Role role)
        {
            Message msg = new Message();
            try
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from roleTable in adminEntities.Admin_Role
                                 where roleTable.roleId == role.roleId
                                 select roleTable).FirstOrDefault();

                    query.name = role.name;

                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "Los cambios se realizaron exitosamente";
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }
            return msg;
        }

        public Message RemoveRole(BO.Role role)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                var query_Role_Permissions = (from rp in adminEntities.Admin_Role_Permission
                                              where rp.roleId == role.roleId
                                              select rp);

                foreach (var item in query_Role_Permissions)
                {
                    adminEntities.Admin_Role_Permission.Remove(item);
                }
                var query = (from roleTable in adminEntities.Admin_Role
                             where roleTable.roleId == role.roleId
                             select roleTable).FirstOrDefault();

                adminEntities.Admin_Role.Remove(query);
                try
                {
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "Se eliminó exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No fue posible eliminar el Rol. Error ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }

        public List<BO.Permissions> GetPermissions()
        {
            List<BO.Permissions> permList = new List<BO.Permissions>();

            using (adminEntities adminEntities = new adminEntities())
            {
                var query = (from permissionsTable in adminEntities.Admin_Permissions
                             select permissionsTable).ToList();
                foreach (var rs in query)
                {
                    BO.Permissions perm = new BO.Permissions();
                    perm.Name = rs.Name;
                    perm.id = rs.id;
                    permList.Add(perm);
                }
            }
            return permList;
        }

        public Message CreatePermissions(BO.Permissions perm)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                Admin_Permissions permissionsTable = new Admin_Permissions();

                permissionsTable.id = Guid.NewGuid();
                permissionsTable.Name = perm.Name;
                try
                {
                    adminEntities.Admin_Permissions.Add(permissionsTable);
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "El Permiso se creó exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No se puedo crear el Permiso ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }

        public Message ModifyPermission(BO.Permissions perm)
        {
            Message msg = new Message();
            try
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from permissionsTable in adminEntities.Admin_Permissions
                                 where permissionsTable.id == perm.id
                                 select permissionsTable).FirstOrDefault();

                    query.Name = perm.Name;

                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "Los cambios se realizaron exitosamente";
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }
            return msg;
        }

        public Message RemovePermissions(BO.Permissions perm)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                var query = (from permissionsTable in adminEntities.Admin_Permissions
                             where permissionsTable.id == perm.id
                             select permissionsTable).FirstOrDefault();

                adminEntities.Admin_Permissions.Remove(query);
                try
                {
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "Se eliminó exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No fue posible eliminar el Permiso. Error ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }

        public Message AddPermissionByRole(string[] perm)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                for (var i = 1; i < perm.Length; i++)
                {
                    Admin_Role_Permission rp = new Admin_Role_Permission();

                    rp.id = Guid.NewGuid();
                    rp.roleId = new Guid(perm[0]);
                    rp.permissionsId = new Guid(perm[i]);

                    adminEntities.Admin_Role_Permission.Add(rp);
                }
                try
                {
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "proceso realizado exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "Error ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }

        public Message DeletePermissionByRole(string[] perm)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                var roleID = new Guid(perm[0]);
                for (var i = 1; i < perm.Length; i++)
                {
                    var id = new Guid(perm[i]);

                    var query = (from rp in adminEntities.Admin_Role_Permission
                                 where rp.permissionsId == id && rp.roleId == roleID
                                 select rp).FirstOrDefault();

                    adminEntities.Admin_Role_Permission.Remove(query);
                }
                try
                {
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "proceso realizado exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "Error ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }
    }
}