﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Security.Cryptography.X509Certificates;
using Microsoft.WindowsAzure.Management.Compute;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace WCF.DAL
{
    public class PromotionsDAL : IPromotionsDAL
    {
        public List<BO.Promotion> GetPromotions()
        {
            List<BO.Promotion> ListPromotions = new List<BO.Promotion>();

            using (adminEntities PromotionsEntities = new adminEntities())
            {
                var query = (from promotions in PromotionsEntities.Promotions
                             select promotions).ToList();

                if (query != null)
                {
                    foreach (var result in query)
                    {
                        BO.Promotion promotion = new BO.Promotion();
                        promotion.Id = result.Id;
                        promotion.Title = result.Title.Trim();
                        promotion.Description = result.Description.Trim();
                        promotion.Image = result.Image.Trim();
                        promotion.StartDate = result.StartDate.ToString();
                        promotion.ExpirationDate = result.ExpirationDate.ToString();

                        ListPromotions.Add(promotion);
                    }
                }
            }
            return ListPromotions;
        }

        public Message ModifyPromotions(BO.Promotion promo)
        {
            Message msg = new BO.Message();
            try
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from promotions in adminEntities.Promotions
                                 where promotions.Id == promo.Id
                                 select promotions).FirstOrDefault();

                    query.Id = promo.Id;
                    query.Title = promo.Title.Trim();
                    query.Description = promo.Description.Trim();
                    query.Image = promo.Image;
                    query.StartDate = Convert.ToDateTime(promo.StartDate);
                    query.ExpirationDate = Convert.ToDateTime(promo.ExpirationDate);
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "La Promoción ha sido modificada satisfactoriamente";
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }

            return msg;
        }

        public Message CreatePromotions(BO.Promotion promo)
        {
            Message msg = new Message();
            using (adminEntities adminEntities = new adminEntities())
            {
                Promotions promotions = new Promotions();

                promotions.Title = promo.Title.Trim();
                promotions.Description = promo.Description.Trim();
                promotions.Image = promo.Image;
                promotions.StartDate = Convert.ToDateTime(promo.StartDate);
                promotions.ExpirationDate = Convert.ToDateTime(promo.ExpirationDate);
                try
                {
                    adminEntities.Promotions.Add(promotions);
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "La promoción se creó exitosamente";
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Concat("{0}{1}", "No pudo crear la promoción ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }

        public Message DeletePromotions(BO.Promotion promo)
        {
            Message msg = new BO.Message();
            try
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from promotions in adminEntities.Promotions
                                 where promotions.Id == promo.Id
                                 select promotions).FirstOrDefault();

                    adminEntities.Promotions.Remove(query);
                    adminEntities.SaveChanges();
                    msg.error = false;
                    msg.message = "La promoción ha sido eliminada satisfactoriamente";
                }
            }
            catch (Exception exs)
            {
                msg.error = true;
                msg.message = string.Format("'{0}''{1}'", "No fue posible eliminar la promoción. Error: ", exs.InnerException.Message);
                msg.number = 0;
            }
            return msg;
        }

        public Message ApplyChanges(string promo)
        {
            var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysCloudService"].ToString();
            PromotionPackage promopg = new PromotionPackage();
            Message msg = new BO.Message();
            promopg.ListPromotions = promo;
            promopg.Token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
            Service service = new Service();
            var jsonSerialiser = new JavaScriptSerializer();
            var json_ = jsonSerialiser.Serialize(promopg);
            Message message = new Message();

            if (cloudServiceName == "localhost")
            {
                msg = service.Execute<PromotionPackage, Message>("Promotion.Publish", promopg);
                return message;
            }
            else
            {
                //metodo para acceder a multiples instancias de cloudServices
                #region establish connection to CloudServices 
                try
                {
                    var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
                    var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
                    var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
                    var cert = new X509Certificate2(Convert.FromBase64String(certString));
                    var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
                    var computeManagementClient = new ComputeManagementClient(credentials);
                    var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
                    var deployments = cloudServiceDetails.Deployments;
                    #region Serializa el model
                    //XmlObjectSerializer obj = GetSerializer<T>();
                    XmlObjectSerializer obj = new DataContractJsonSerializer(typeof(PromotionPackage));
                    MemoryStream stream = new MemoryStream();
                    obj.WriteObject(stream, promopg);
                    string data = Encoding.UTF8.GetString(stream.ToArray(), 0, (int)stream.Length);
                    #endregion Serializa el model
                    foreach (var deployment in deployments)
                    {
                        foreach (var instance in deployment.RoleInstances)
                        {
                            if (deployment.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production)
                            {
                                //llamar a servicio via rest por cada IP interna.
                                string url = string.Format("http://{0}/Promotion.svc/Publish/", instance.IPAddress.ToString());
                                WebClient webClient1 = new WebClient();
                                webClient1.Headers.Add("ClientID", "111.111.111.120 ");
                                webClient1.Headers.Add("Content-type", "application/json");
                                webClient1.Encoding = Encoding.UTF8;
                                var result = webClient1.UploadString(url, "POST", data); //Ejecuta la llamada al servicio
                                message = JsonConvert.DeserializeObject<Message>(result);
                            }
                        }
                    }
                    return message;
                }
                catch (Exception ex)
                {
                    message.message = ex.ToString();
                    message.error = true;
                    message.number = 1;
                    return message;
                }
                #endregion
            }

            //Message msg = new BO.Message();
            //try
            //{
            //    PromotionPackage promopg = new PromotionPackage();
            //    promopg.ListPromotions = promo;
            //    promopg.Token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();

            //    Service service = new Service();

            //    var jsonSerialiser = new JavaScriptSerializer();
            //    var json_ = jsonSerialiser.Serialize(promopg);

            //    msg = service.Execute<PromotionPackage, Message>("Promotion.Publish", promopg);

            //    if (msg.number == 0)
            //    {
            //        msg.message = "Cambios realizados satisfactoriamente";
            //    }
            //}
            //catch (Exception exs)
            //{
            //    msg.error = true;
            //    msg.message = string.Format("'{0}''{1}'", "No fue posible enviar la lista: ", exs.InnerException.Message);
            //    msg.number = 0;
            //}

            //return msg;
        }
    }
}