﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;

namespace WCF.DAL
{
    public class UserWebDAL : IWebDAL
    {
        public UserWeb GetUserIdCard(string idCard)
        {
            UserWeb userWeb = new UserWeb();

            using (webEntities webEntities = new webEntities())
            {
                var query = (from login in webEntities.Login
                             where login.IdCard == idCard
                             select login).FirstOrDefault();

                userWeb.Active = query.Active;
                userWeb.Address = query.Address;
                userWeb.City = query.City;
                userWeb.Email = query.Email;
                userWeb.IdCard = query.IdCard;
                userWeb.LastName = query.LastName;
                userWeb.MassMailSubcription = query.MassMailSubscription;
                userWeb.MobilePhone = query.MobilePhone;
                userWeb.Name = query.Name;
                userWeb.Password = query.Password;
                userWeb.Phone = query.Phone;
                userWeb.State = query.State;
                userWeb.Id = query.Id.ToString();
                userWeb.Platform = "Web";
            }
            return userWeb;
        }
        public UserWeb GetUserEmail(string email)
        {
            UserWeb userWeb = new UserWeb();

            using (webEntities webEntities = new webEntities())
            {
                var query = (from login in webEntities.Login
                             where login.Email == email
                             select login).FirstOrDefault();

                userWeb.Active = query.Active;
                userWeb.Address = query.Address;
                userWeb.City = query.City;
                userWeb.Email = query.Email;
                userWeb.IdCard = query.IdCard;
                userWeb.LastName = query.LastName;
                userWeb.MassMailSubcription = query.MassMailSubscription;
                userWeb.MobilePhone = query.MobilePhone;
                userWeb.Name = query.Name;
                userWeb.Password = query.Password;
                userWeb.Phone = query.Phone;
                userWeb.State = query.State;
                userWeb.Id = query.Id.ToString();
                userWeb.Platform = "Web";
            }
            return userWeb;
        }
        public Message ModifyUser(UserWeb user, bool blackList)
        {
            Message msg = new Message();
            AdminDAL adminDal = new AdminDAL();
            UserWeb userWeb = new UserWeb();


            if (blackList.Equals(false))
            {
                userWeb = adminDal.GetBlackList(user.Email, "Web");               
            }
            if (userWeb.Email != null)
            {
                msg.error = false;
                msg.message = "El Usuario no se puede modificar porque se encuentra en la Lista Negra";
            }
            else
            {
                try
                {
                    using (webEntities webEntities = new webEntities())
                    {
                        var query = (from login in webEntities.Login
                                     where login.Id == new Guid(user.Id)
                                     select login).FirstOrDefault();
                        //se setean los valores antes de modificarlo para usarlo en auditoria
                        userWeb.Email = query.Email;
                        userWeb.IdCard = query.IdCard;
                        userWeb.MassMailSubcription = query.MassMailSubscription;
                        userWeb.Password = query.Password;
                        userWeb.Active = query.Active;
                        //----------
                        if (blackList.Equals(false))
                        { 
                            query.Email = user.Email;
                            query.IdCard = user.IdCard;
                            query.MassMailSubscription = user.MassMailSubcription;
                            query.Password = user.Password;
                        }
                        else
                        {
                            query.Active = false;
                            query.MassMailSubscription = false;
                        }
                        webEntities.SaveChanges();
                        msg.error = false;
                        msg.message = "Los cambios se realizaron exitosamente";
                        #region create auditoria

                        Admin_Audit audit = new Admin_Audit();
                        audit.Id= Guid.NewGuid();
                        audit.UserId = user.userAudit;
                        audit.Department = user.Department;
                        //Valor Seteado, cambiar cuando se realice el login de un usuario
                        //audit.UserId = new Guid("35E78CE3-CE7A-409B-B4D7-BD6BAB5C8AE1");
                        audit.Action = "MOD";
                        audit.EventDatetime = DateTime.Now;
                        audit.Event = 
                            string.Format(
                                 "IdCard oldValue:'{0}' newValue:'{1}'-Email OldValue'{2}' newValue:'{3}'-pass OldValue'{4}' newValue:'{5}' -massMail OldValue'{6}' newValue:'{7}' -active OldValue'{8}' newValue:'{9}'",
                                 userWeb.IdCard,user.IdCard,
                                 userWeb.Email,user.Email,
                                 userWeb.Password,user.Password,
                                 userWeb.MassMailSubcription,user.MassMailSubcription,
                                 userWeb.Active,user.Active);
                        AdminDAL adminDAL = new AdminDAL();
                        adminDAL.CreateAudit(audit);
                        #endregion
                    }
                }
                catch (Exception exs)
                {
                    msg.error = true;
                    msg.message = string.Format("'{0}''{1}'", "No fue posible realizar las modificaciones. Error: ", exs.InnerException.Message);
                    msg.number = 0;
                }
            }
            return msg;
        }
        public Message RemoveUser(UserWeb user)
        {
            Message msg = new Message();
            UserWeb _usr = new UserWeb();
            AdminDAL adminDal = new AdminDAL();
            _usr = adminDal.GetBlackList(user.Email, "Web");
            if (_usr.Id != null)
            {
                msg.error = false;
                msg.message = "El Usuario no se puede eliminar porque se encuentra en la Lista Negra";
            }
            else
            {
                using (webEntities webEntities = new webEntities())
                {
                    
                    try
                    {
                        var queryToken = (from Token in webEntities.Token
                                          where Token.LoginId == new Guid(user.Id)
                                          select Token);
                        foreach (var rs in queryToken)
                        {
                            webEntities.Token.Remove(rs);
                        }

                        //    webEntities.Token.RemoveRange(webEntities.Token.Where(t => t.LoginId.ToString() == user.Id.ToString()));

                        webEntities.SaveChanges();

                        var queryLogin = (from login in webEntities.Login
                                          where login.Id == new Guid(user.Id)
                                          select login).FirstOrDefault();
                        webEntities.Login.Remove(queryLogin);

                        webEntities.SaveChanges();

                        msg.error = false;
                        msg.message = "Se eliminó el usuario exitosamente";

                        #region create auditoria
                        Admin_Audit audit = new Admin_Audit();
                        audit.Id = Guid.NewGuid();
                        audit.Department = user.Department;
                        //audit.UserId = user.userAudit;

                        //Valor Seteado, cambiar cuando se realice el login de un usuario
                        //audit.UserId = new Guid("35E78CE3-CE7A-409B-B4D7-BD6BAB5C8AE1");
                        audit.UserId = user.userAudit;                    
                        audit.Action = "DEL";
                        audit.EventDatetime = DateTime.Now;
                        audit.Event =string.Format("IdCard:{0}email{1}", user.IdCard,user.Email);

                        AdminDAL adminDAL = new AdminDAL();
                        adminDAL.CreateAudit(audit);
                        #endregion
                    }
                    catch (Exception exs)
                    {
                        msg.error = true;
                        msg.message = string.Concat("{0}{1}", "No fue posible eliminar el usuario. Error", exs.InnerException.Message);
                        msg.number = 0;
                    }
                }
            }
            return msg;
        }
    }
}
