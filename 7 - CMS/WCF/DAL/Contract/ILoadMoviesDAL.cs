﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;


namespace WCF.DAL.Contract
{
    public interface ILoadMoviesDAL
    {
        List<BO.LoadMovies> GetLoadMovies();

        Message FileGetRediCache(string redicache);
    }
}
