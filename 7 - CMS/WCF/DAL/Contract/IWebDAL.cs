﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;

namespace WCF.DAL
{
    public interface IWebDAL
    {
        UserWeb GetUserIdCard(string idCard);
        UserWeb GetUserEmail(string email);
        Message ModifyUser(UserWeb userWeb, bool blackList);
        Message RemoveUser(UserWeb userWeb);

    }
}
