﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCF.BL.Contract;
using WCF.BO;

namespace WCF.DAL.Contract
{
    public interface IMessagesDAL
    {
        Message ApplyChange(string MessagesList);

        Message SaveMessagesToDB(List<MessageForCinema> messages);

        List<MessageForCinema> GetMessagesFromDB();
    }
}