﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.DAL.Contract
{
    public interface IAdminDAL
    {
        Message CreateBlackList(UserWeb userWeb);
        UserWeb GetBlackList(string email, string platform);
        List<BlackList_User> GetAllBlackListUsers();
        BlackList_User GetBlackListUserByIdCard(string IdCard);
        BlackList_User GetBlackListUserByEmail(string Email);
        Message Delete_BlackListUser(BlackList_User BlackList_User);
        List<BO.Audit> ListAudit(AuditParameters parameters);
    }
}
