﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;

namespace WCF.DAL
{
    public interface IMobileDAL
    {
        UserWeb GetUserMobileIdCard(string idCard);
        UserWeb GetUserMobileEmail(string email);
        Message ModifyUserMobile(UserWeb user, bool blackList);
        Message RemoveUserMobile(UserWeb user);

    }
}