﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.DAL.Contract
{
    public interface ILoginUsersDAL 
    {
        LoginUser GetUserData(LoginUser Usr);
        List<LoginUser> GetUser();
        Message LogOut(LoginUser User);
        Message CreateUser(LoginUser User);
        Message ModifyUser(LoginUser User);
        Message DeleteUser(LoginUser User);
    }
}
