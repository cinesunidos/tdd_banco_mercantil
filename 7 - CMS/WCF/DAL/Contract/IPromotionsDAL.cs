﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.DAL.Contract
{
    public interface IPromotionsDAL
    {
        List<BO.Promotion> GetPromotions();

        Message ModifyPromotions(BO.Promotion promo);

        Message CreatePromotions(BO.Promotion promo);

        Message DeletePromotions(BO.Promotion promo);

        Message ApplyChanges(string promo);
    }
}
