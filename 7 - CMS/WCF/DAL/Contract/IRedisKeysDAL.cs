﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using WCF.BL.Contract;
using WCF.BO;

namespace WCF.DAL.Contract
{
    public interface  IRedisKeysDAL
    {
        RedisKey GetKeyRedis(RedisKey Redis);
        List<RedisKey> GetListRedisKeys();
        Message AddKeyRedis(RedisKey Redis);
        Message ModifyKeyRedis(RedisKey Redis);
        Message RemoveKeyRedis(RedisKey Redis);
        List<Message> ExecuteKeyRedis(RedisKey Redis);
    }
}