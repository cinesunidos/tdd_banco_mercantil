﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;

namespace WCF.DAL.Contract
{
    public interface IConcessionTransactionsDAL
    {

        ConcessionTransactionResponse GetConcessionsSalesReportByDate(string date);

        ConcessionTransactionResponse GetFullSalesReportByDate(string date);

        ConcessionCatalogResponse GetConcessionCatalogByTheater(string theaterId);

        ConcessionItem GetConcessionItem(string itemId);

        Message ModifyModalConcessions(string ModalCinemaList);

        Message GetSalesReportFilterByDate(string date);

        Message GetSalesReportComponentsFilterByDate(string date);

        ModalCinema GetModalConcessionsStatus(ModalCinema modal);

        List<TransferItem> GetConcessionsItemsForCinema(string dinamicConnection);

        List<ItemAndComponets> VerifyItemsForTranslate(List<TransferItem> items);

        List<ItemAndComponets> VerifyItemsForReverse(List<TransferItem> items);

        List<ItemComponent> ConfirmItemsForReverse(List<ItemComponent> items);

        List<ItemComponent> ConfirmItemsForTranslate(List<ItemComponent> items);

        TransferPack UpdateCandiesTable(PackViewModel items);

        List<ItemComponent> ReverseTranslate(List<ItemComponent> items);

        List<ItemAndComponets> GetConcessionsItemsInCinema(string CinemaId);

        List<TransferItem> GetAviableConcessionsItemsInCinema(string CinemaId);

        TransferPack DecreaseCandiesTable(PackViewModel items);

        string ModifyCandiesWeb(string data);

        List<ModalCinema> GetAllModalConcessionsStatus();

        List<TransferItem> GetAllConcecion(string CinemaID);

    }
}