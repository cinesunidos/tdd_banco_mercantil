﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF.DAL.Contract
{
    public interface IManagementDAL
    {
        List<BO.Permissions> GetAccessByRole(Guid RoleId);
        List<BO.Role> GetRole();
        Message CreateRole(BO.Role role);
        Message ModifyRole(BO.Role role);
        Message RemoveRole(BO.Role role);
        List<BO.Permissions> GetPermissions();
        Message CreatePermissions(BO.Permissions perm);
        Message ModifyPermission(BO.Permissions perm);
        Message RemovePermissions(BO.Permissions perm);
        Message AddPermissionByRole(string[] perm);
        Message DeletePermissionByRole(string[] perm);

    }
}
