//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WCF.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Login
    {
        public System.Guid Id { get; set; }
        public Nullable<System.DateTime> Birth { get; set; }
        public string Address { get; set; }
        public bool MassMailSubscription { get; set; }
        public System.DateTime Creation { get; set; }
        public Nullable<System.DateTime> Activation { get; set; }
        public bool Active { get; set; }
        public Nullable<int> Sex { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string IdCard { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public Nullable<System.DateTime> DateOut { get; set; }
        public Nullable<System.DateTime> DateLastVisit { get; set; }
        public Nullable<System.DateTime> DateCurrentVisit { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public Nullable<System.DateTime> DateLastUpdate { get; set; }
        public Nullable<int> ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
    }
}
