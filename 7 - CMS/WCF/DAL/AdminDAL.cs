﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF.DAL
{
    public class AdminDAL : IAdminDAL
    {

        public Message CreateBlackList(UserWeb userWeb)
        {
            Message msg = new Message();
            UserWeb _user = new UserWeb();

            #region Validate variables userWeb and platform

            //To validate if user object and platform don't come empty
            if ((userWeb != null) && (userWeb.Platform != ""))            
            {
                #region to check GetBlackList() method
                _user = GetBlackList(userWeb.Email, userWeb.Platform);
                if (_user.Id != null)
                {
                    msg.error = false;
                    msg.message = "El usuario ya existe en la Lista Negra";
                }
                else
                {
                    using (adminEntities adminEntities = new adminEntities())
                    {
                        //To check if this user exists on BlackList

                        #region BlackListUser and to fill adminEntities

                        // To assign each BlackList fields with UserWeb Object attributes
                        BlackList_BlackList_Users blackList = new BlackList_BlackList_Users();
                        blackList.Id = Guid.NewGuid();
                        blackList.IdCard = userWeb.IdCard;
                        blackList.Email = userWeb.Email;
                        blackList.platform = userWeb.Platform;
                        blackList.create = DateTime.Now; //To adjust time zone - Caracas

                        //To fill adminEntities Entity to register an user to Black List
                        adminEntities.BlackList_BlackList_Users.Add(blackList);
                        #endregion

                        #region Update User
                        //To update the user. Depends of platform (Web, Mobile)  
                        if (userWeb.Platform == "Web")
                        {
                            UserWebDAL userWebDal = new UserWebDAL();
                            userWebDal.ModifyUser(userWeb, true);
                        }
                        else
                        {
                            UserMobileDAL _userMobileDal = new UserMobileDAL();
                            _userMobileDal.ModifyUserMobile(userWeb, true);
                        }
                        #endregion

                        #region "Try..Catch"
                        //Step 3. To save changes on adminEntities and to handle exceptions
                        try
                        {
                            adminEntities.SaveChanges();
                            msg.error = false;
                            msg.message = "El usuario se creó en la Lista Negra exitosamente";

                            #region create auditoria

                            Admin_Audit audit = new Admin_Audit();
                            audit.Id = Guid.NewGuid();
                            audit.UserId = userWeb.userAudit;
                            audit.Department = userWeb.Department;
                            audit.Action = "REG";
                            audit.EventDatetime = DateTime.Now;
                            //audit.Event = string.Format("IdCard:{0}email{1}", user.IdCard, user.Email);
                            audit.Event = string.Format("IdCard:{0}email{1}", userWeb.IdCard, userWeb.Email);
                            AdminDAL adminDAL = new AdminDAL();
                            adminDAL.CreateAudit(audit);

                            #endregion


                        }
                        catch (Exception exs)
                        {
                            msg.error = true;
                            msg.message = string.Concat("{0}{1}", "No fue posible mover a la Lista Negra", exs.Message);
                            msg.number = 0;
                        }
                        #endregion
                    }
                }


                #endregion
            }
            else
            {
                msg.error = false;
                msg.message = "Variable userWeb ó platform se encuentran vacios";
            }
            return msg;
            #endregion
        }

        public UserWeb GetBlackList(string email, string platform)
        {
            UserWeb userWeb = new UserWeb();

            if (platform.Equals("Web"))
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from blackList in adminEntities.BlackList_BlackList_Users
                                 where blackList.Email == email
                                 select blackList).FirstOrDefault();

                    if (query != null)
                    {
                        UserWebDAL userWebDal = new UserWebDAL();
                        userWeb = userWebDal.GetUserEmail(email);
                    }
                }
            }
            else if (platform.Equals("Mobile"))
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from blackList in adminEntities.BlackList_BlackList_Users
                                 where blackList.Email == email
                                 select blackList).FirstOrDefault();

                    if (query != null)
                    {
                        UserWebDAL _userMobileDAL = new UserWebDAL();
                        userWeb = _userMobileDAL.GetUserEmail(email);
                    }
                    else
                    {
                        //userWeb.Id = null;
                        userWeb = null;
                    }
                }
            }

            return userWeb;
        }

        public List<BlackList_User> GetAllBlackListUsers()
        {
            List<BlackList_User> ListBlackListUsers = new List<BlackList_User>();

            using (adminEntities adminEntities = new DAL.adminEntities())
            {
                var query = (from blackList in adminEntities.BlackList_BlackList_Users
                             select blackList);

                if (query != null)
                {
                    foreach (var result in query)
                    {
                        BlackList_User BlackList_Usr = new BlackList_User();
                        BlackList_Usr.create = result.create;
                        BlackList_Usr.description = result.description;
                        BlackList_Usr.Email = result.Email;
                        BlackList_Usr.Id = result.Id;
                        BlackList_Usr.IdCard = result.IdCard;
                        BlackList_Usr.platform = result.platform;

                        ListBlackListUsers.Add(BlackList_Usr);
                    }
                }
            }
            return ListBlackListUsers;
        }
        public BlackList_User GetBlackListUserByIdCard(string IdCard)
        {
            BlackList_User _BlackList_Usr = new BlackList_User();
            if ((IdCard != null) || (IdCard != ""))
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from blackList in adminEntities.BlackList_BlackList_Users
                                 where blackList.IdCard == IdCard
                                 select blackList).FirstOrDefault();

                    if (query != null)
                    {
                        _BlackList_Usr.create = query.create;
                        _BlackList_Usr.description = query.description;
                        _BlackList_Usr.Email = query.Email;
                        _BlackList_Usr.Id = query.Id;
                        _BlackList_Usr.IdCard = query.IdCard;
                        _BlackList_Usr.platform = query.platform;
                    }
                }

            }
            return _BlackList_Usr;
        }
        public BlackList_User GetBlackListUserByEmail(string Email)
        {
            BlackList_User _BlackList_Usr = new BlackList_User();
            if ((Email != null) || (Email != ""))
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from blackList in adminEntities.BlackList_BlackList_Users
                                 where blackList.Email == Email
                                 select blackList).FirstOrDefault();
                    if (query != null)
                    {
                        _BlackList_Usr.create = query.create;
                        _BlackList_Usr.description = query.description;
                        _BlackList_Usr.Email = query.Email;
                        _BlackList_Usr.Id = query.Id;
                        _BlackList_Usr.IdCard = query.IdCard;
                        _BlackList_Usr.platform = query.platform;
                    }
                }

            }

            return _BlackList_Usr;
        }
        public Message Delete_BlackListUser(BlackList_User BlackList_User)
        {            
            Message msg = new Message();

            if ((BlackList_User.IdCard != null) || (BlackList_User.Email != null) ||
                (BlackList_User.IdCard != "") || (BlackList_User.Email != ""))
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    var query = (from blackList in adminEntities.BlackList_BlackList_Users
                                 where blackList.IdCard == BlackList_User.IdCard &&  blackList.Email == BlackList_User.Email 
                                 select blackList).FirstOrDefault();
                    adminEntities.BlackList_BlackList_Users.Remove(query);
                    try
                    {
                        adminEntities.SaveChanges();
                        msg.error = false;
                        msg.message = "Se eliminó el usuario de la Lista Negra exitosamente";

                        #region create auditoria

                        Admin_Audit audit = new Admin_Audit();
                        audit.Id= Guid.NewGuid();
                        audit.UserId = BlackList_User.userAudit;
                        audit.Department = BlackList_User.Department;
                        audit.Action = "DELBL";
                        audit.EventDatetime = DateTime.Now;
                        //audit.Event = string.Format("IdCard:{0}email{1}", user.IdCard, user.Email);
                        audit.Event = string.Format("IdCard:{0}email{1}", BlackList_User.IdCard, BlackList_User.Email);
                        AdminDAL adminDAL = new AdminDAL();
                        adminDAL.CreateAudit(audit);

                        #endregion

                    }
                    catch (Exception exs)
                    {
                        msg.error = true;
                        msg.message = string.Concat("{0}{1}", "No fue posible eliminar el usuario de la lista Negra. Error", exs.InnerException.Message);
                        msg.number = 0;
                    }

                }
            }
            else
            {
                msg.error = true;
                msg.message = "Campo Email ó IdCard se encuentra vacios, no se puede eliminar en la Lista Negra";
            }

            return msg;
        }
        public void CreateAudit(Admin_Audit audit)
        {
            if (audit != null)
            {
                using (adminEntities adminEntities = new adminEntities())
                {
                    adminEntities.Admin_Audit.Add(audit);
                    adminEntities.SaveChanges();
                }
            }
        }
        public List<BO.Audit> ListAudit(AuditParameters parameters)
        {
            List<BO.Audit> ListAudit = new List<BO.Audit>();
            using (adminEntities adminEntities = new adminEntities())
            {              
                string _DateB = Convert.ToDateTime(parameters.DateBegin).ToString("yyyy-MM-dd");             
                string _DateE = Convert.ToDateTime(parameters.DateEnd).ToString("yyyy-MM-dd");

                //LRAMIREZ 27/10/2016, To inicialize var query in null, before assing values through linq query
                List<Admin_Audit> _audit = null;
                var audit_ = _audit;
                string query = "";

                //CASE 1, When user and department equals empty, to get all audit list only dates range
                if ((parameters.UserId == "") && (parameters.Department == ""))
                {
                    query = "SELECT" +
                    " [EventDatetime],[Id],[UserId],[Department],[Action],[Event] " +
                    " FROM [dbo].[Admin_Audit]" +
                    " WHERE CAST(EventDatetime AS DATE) BETWEEN '" + _DateB + "' AND '" + _DateE + "'" +
                    " ORDER BY [EventDatetime] DESC";
                    audit_ = adminEntities.Admin_Audit.SqlQuery(query).ToList();
                }

                //CASE 2, When user !="" and department == "", to get audit list by user and dates range
                if ((parameters.UserId != "") && (parameters.Department == ""))
                {
                    Guid _UserId = new Guid(parameters.UserId);
                    query = "SELECT" +
                   " [EventDatetime],[Id],[UserId],[Department],[Action],[Event] " +
                   " FROM [dbo].[Admin_Audit]" +
                   " WHERE CAST(EventDatetime AS DATE) BETWEEN '" + _DateB + "' AND '" + _DateE + "'" +
                   " AND [UserId] = '" + _UserId + "'" +
                   " ORDER BY [EventDatetime] DESC";
                    audit_ = adminEntities.Admin_Audit.SqlQuery(query).ToList();
                }

                //CASE 2, When user =="" and department != "", to get audit list by department and dates range
                if ((parameters.UserId == "") && (parameters.Department != ""))
                {                   
                    query = "SELECT" +
                   " [EventDatetime],[Id],[UserId],[Department],[Action],[Event] " +
                   " FROM [dbo].[Admin_Audit]" +
                   " WHERE CAST(EventDatetime AS DATE) BETWEEN '" + _DateB + "' AND '" + _DateE + "'" +
                   " AND [Department] = '" + parameters.Department + "'" +
                   " ORDER BY [EventDatetime] DESC";
                    audit_ = adminEntities.Admin_Audit.SqlQuery(query).ToList();

                }

                //CASE 3, When user!="" and department != "", to get audit list by user, department and dates range
                if ((parameters.UserId != "") && (parameters.Department != ""))
                {
                    Guid _UserId = new Guid(parameters.UserId);
                    query = "SELECT" +
                   " [EventDatetime],[Id],[UserId],[Department],[Action],[Event] " +
                   " FROM [dbo].[Admin_Audit]" +
                   " WHERE CAST(EventDatetime AS DATE) BETWEEN '" + _DateB + "' AND '" + _DateE + "'" +
                   " AND [UserId] = '" + _UserId + "'" +
                   " AND [Department] = '" + parameters.Department + "'" +
                   " ORDER BY [EventDatetime] DESC";
                    audit_ = adminEntities.Admin_Audit.SqlQuery(query).ToList();                   
                }
               
                if (audit_.Count > 0)
                {
                    foreach (var result in audit_)
                    {
                        BO.Audit Audit = new BO.Audit();
                        Audit.UserId = result.UserId;
                        Audit.dateTime = result.EventDatetime;
                        switch (result.Action)
                        {
                            case "MOD":
                                Audit.Action = "Modificar Invitado";
                                break;
                            case "DEL":
                                Audit.Action = "Eliminar Invitado";
                                break;
                            case "REG":
                                Audit.Action = "Mover a Lista Negra";
                                break;
                        }
                        Audit.Department = result.Department;                
                        Audit.Event = result.Event;
                        ListAudit.Add(Audit);  
                    }
                }
                else
                {
                    ListAudit = null;
                }
            }
            return ListAudit;
        }
    }
}
