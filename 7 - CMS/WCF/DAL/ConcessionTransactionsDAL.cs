﻿using Microsoft.WindowsAzure.Management.Compute;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Hosting;
using WCF.BO;
using WCF.DAL;
using WCF.DAL.Contract;

namespace WCF.DAL
{
    public class ConcessionTransactionsDAL : IConcessionTransactionsDAL
    {
        public ConcessionTransactionResponse GetConcessionsSalesReportByDate(string date)
        {
            var response = spGetConcessionTransactions(date, true);
            return response;
        }

        public ConcessionTransactionResponse GetFullSalesReportByDate(string date)
        {

            var response = spGetConcessionTransactions(date, false);
            return response;
        }

        private ConcessionTransactionResponse spGetConcessionTransactions(string date, bool restrictByTime)
        {
            //var connectionString = ConfigurationManager.AppSettings["ConnectionName"]; // "VISTAPIR"
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VISTA"].ToString());

            var hashmap = new Dictionary<string, ConcessionTransactionSummary>();
            var listTransactions = new List<ConcessionTransaction>();

            using (var command = new SqlCommand())
            {
                command.Connection = conn;

                command.CommandText = "spCU_GetConcessionsTransactionsByDate";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@TransDate", SqlDbType.VarChar).Value = date;
                command.Parameters.Add("@LocationCode", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["ConcessionsLocationStrCode"]; // "0030";
                command.Parameters.Add("@WorkstationName", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["ConcessionsWorkstationName"]; // "WEB";
                command.Parameters.Add("@RestrictTime", SqlDbType.VarChar).Value = restrictByTime ? "Y" : "N";

                conn.Open();

                using (var reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        // summary

                        string itemId = (string)(reader["Item_strItemId"]);

                        if (!hashmap.ContainsKey(itemId))
                        {
                            var summaryItem = new ConcessionTransactionSummary();
                            summaryItem.ItemId = (string)(reader["Item_strItemId"]);
                            summaryItem.ItemDescription = (string)reader["Item_strItemDescription"];
                            summaryItem.ItemValue = Convert.ToDecimal(reader["TransI_curValueEach"]);
                            summaryItem.ItemQuantity = Convert.ToInt32(reader["TransI_decNoOfItems"]);
                            summaryItem.SubTotal = summaryItem.ItemValue * summaryItem.ItemQuantity;
                            hashmap.Add(itemId, summaryItem);
                        }
                        else
                        {
                            var summaryItem = hashmap[itemId];
                            summaryItem.ItemQuantity += Convert.ToInt32(reader["TransI_decNoOfItems"]);
                            summaryItem.SubTotal = summaryItem.ItemValue * summaryItem.ItemQuantity;
                            hashmap[itemId] = summaryItem;
                        }

                        // transaction
                        var transaction = new ConcessionTransaction();
                        transaction.TransactionId = Convert.ToString(reader["TransI_lgnNumber"]);
                        transaction.TransactionType = (string)(reader["TransI_strType"]);
                        transaction.TransactionStatus = Convert.ToString(reader["TransI_strStatus"]);
                        transaction.ItemId = (string)(reader["Item_strItemId"]);
                        transaction.ItemDescription = (string)reader["Item_strItemDescription"];
                        transaction.ItemValue = Convert.ToDecimal(reader["TransI_curValueEach"]);
                        transaction.ItemQuantity = Convert.ToInt32(reader["TransI_decNoOfItems"]);
                        transaction.SubTotal = transaction.ItemValue * transaction.ItemQuantity;
                        transaction.Vendor = (string)(reader["Vendor_strName"]);
                        transaction.LocationDescription = (string)(reader["Location_strDescription"]);
                        transaction.Workstation = (string)(reader["Workstation_strName"]);
                        transaction.UserName = (string)(reader["User_strUserName"]);
                        transaction.FilmTitle = (string)(reader["Film_strTitle"]);
                        transaction.FilmDate = (DateTime)(reader["Session_dtmShowing"]);
                        transaction.CollectionDate = (DateTime)(reader["TransI_dtmDateCollected"]);
                        transaction.PickupWorkstation = reader["BookingD_strPickupWorkstn"] != DBNull.Value ? (string)(reader["BookingD_strPickupWorkstn"]) : "";
                        transaction.PickupUserId = reader["BookingD_intPickupUser"] != DBNull.Value ? Convert.ToInt32(reader["BookingD_intPickupUser"]) : 0;
                        transaction.PickupUser = reader["PickupUser_strUserName"] != DBNull.Value ? (string)(reader["PickupUser_strUserName"]) : "";
                        listTransactions.Add(transaction);
                    }

                }

                conn.Close();
            }

            var listTransactionsSummary = new List<ConcessionTransactionSummary>();
            decimal totalSales = 0;
            foreach (KeyValuePair<string, ConcessionTransactionSummary> entry in hashmap)
            {
                var transaction = entry.Value;
                listTransactionsSummary.Add(transaction);
                totalSales += transaction.SubTotal;
            }

            var response = new ConcessionTransactionResponse();
            response.Summary = listTransactionsSummary;
            response.TotalSales = totalSales;
            response.Transactions = listTransactions;

            response = ValidarDevoluciones(response);

            return response;
        }

        ///Ramiro Marimón 2018/04/03
        /// <summary>
        /// Metodo para validar que las trasacciones que se muestyran en el reporte de carameleria
        /// no muestran los item duplicados.
        /// este metodo se crea debido a que en la base de datos de vista, cuando se purgam items de consesiones, se inserta un registro con el mismo TransI_lgnNumber.
        /// este hace que el query original de beeconcept traiga esa transaccion dos veces (la venta y la devolucion) y en el reporte se muestra como si fuesen dos compras
        /// ahora, con este metodo, si la trassaccion tiene una devolucion, no se muestra la transaccion ya que es un +/- (positiva-negativa)
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private ConcessionTransactionResponse ValidarDevoluciones(ConcessionTransactionResponse response)
        {
            ConcessionCatalogResponse respo = new ConcessionCatalogResponse();
            var r = response;
            //limpiar las transacciones que tienen devolucion
            foreach (var i in r.Transactions)
            {
                var a = i;
                var b = r.Transactions.Where(t => t.TransactionId == a.TransactionId && t.TransactionType != a.TransactionType).ToList();
                if (b != null && b.Count > 0)
                {
                    response.Transactions = response.Transactions.Where(t => t.TransactionId != b.First().TransactionId).ToList();
                }
            }

            //Calcular los Concessionsumary y el TotalSales
            response.Summary = new List<ConcessionTransactionSummary>();
            foreach (var it in response.Transactions)
            {
                ConcessionTransactionSummary c = new ConcessionTransactionSummary
                {
                    ItemDescription = it.ItemDescription,
                    ItemId = it.ItemId,
                    ItemQuantity = it.ItemQuantity,
                    ItemValue = it.ItemValue,
                    SubTotal = it.SubTotal
                };
                response.Summary.Add(c);
            }
            response.TotalSales = response.Transactions.Sum(t => t.SubTotal);

            return response;
        }


        public ConcessionCatalogResponse GetConcessionCatalogByTheater(string theaterId)
        {

            //var connectionString = WebConfigurationManager.AppSettings["ConnectionName"]; // "VISTAPIR"
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VISTA"].ToString());
            var listProducts = new List<ConcessionItem>();
            using (var command = new SqlCommand())
            {
                command.Connection = conn;

                command.CommandText = "spCU_GetConcessionsByTheater";
                command.CommandType = CommandType.StoredProcedure;

                var locationStrCode = command.CreateParameter();
                locationStrCode.ParameterName = "@Location_strCode";
                locationStrCode.Value = WebConfigurationManager.AppSettings["ConcessionsLocationStrCode"]; // "0030"

                command.Parameters.Add(locationStrCode);

                conn.Open();

                using (var reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        // transaction
                        var product = new ConcessionItem();
                        product.ItemId = (string)(reader["Item_strItemId"]);
                        product.ItemDescription = (string)reader["Item_strItemDescriptionL1"];
                        product.ItemHOPK = (string)reader["HOPK1"];
                        product.ItemPrice = Convert.ToDecimal(reader["PriceD_curPrice"]);
                        listProducts.Add(product);
                    }

                }

                conn.Close();
            }

            listProducts = listProducts.GroupBy(x => x.ItemId).Select(y => y.First()).ToList();
            var response = new ConcessionCatalogResponse();
            response.Products = listProducts;
            return response;
        }

        public ConcessionItem GetConcessionItem(string itemId)
        {
            //var connectionString = WebConfigurationManager.AppSettings["ConnectionName"]; // "VISTAPIR"
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VISTA"].ToString());

            using (var command = new SqlCommand())
            {
                command.Connection = conn;

                command.CommandText = "spCU_GetConcessionItem";
                command.CommandType = CommandType.StoredProcedure;

                var itemStr = command.CreateParameter();
                itemStr.ParameterName = "@ItemId";
                itemStr.Value = itemId;

                command.Parameters.Add(itemStr);

                conn.Open();

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var product = new ConcessionItem();
                        product.ItemId = (string)(reader["Item_strItemId"]);
                        product.ItemDescription = (string)reader["Item_strItemDescription"];
                        return product;
                    }
                }
            }

            return null;
        }

        public List<BO.ConcessionTransactionResponse> GetListFullSalesReportByDate(string date)
        {
            List<BO.ConcessionTransactionResponse> ListConcession = new List<BO.ConcessionTransactionResponse>();

            //var response = spGetListConcessionTransactions(date, false);
            //return response;


            return ListConcession;
        }

        /// <summary>
        /// Ramiro Marimón 25/04/2018
        /// metodo para guardar la configuracion de la ventana modal de carameleria.
        /// a traves de este metodo se especifica en que cine esta activada o desactivada la ventana modal de carameleria
        /// </summary>
        /// <param name="ModalCinemaList"> lista de cines con el estado de la ventana modal, activado o desactivado.</param>
        /// se pasan los daots en string con formato Json.
        /// los cambiso se envian a azure de forma similar a como se envian los mensajes de los cines..
        /// <returns></returns>
        public Message ModifyModalConcessions(string ModalCinemaList)
        {
            Message message = new Message();
            try
            {
                ModalCinema m = JsonConvert.DeserializeObject<ModalCinema>(ModalCinemaList);
                using (SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ToString()))
                {
                    string csql = "update ModalConcessions set  ModalEnabled = @modalenabled, DateChanged = @datechanged where CinemaID = @cinemaid";
                    SqlCommand cmd = new SqlCommand(csql, c);
                    SqlParameter p1 = new SqlParameter
                    {
                        ParameterName = "modalenabled",
                        Value = m.ModalEnabled,
                        DbType = DbType.Boolean
                    };

                    SqlParameter p2 = new SqlParameter
                    {
                        ParameterName = "datechanged",
                        Value = m.DateChanged,
                        DbType = DbType.DateTime
                    };

                    SqlParameter p3 = new SqlParameter
                    {
                        ParameterName = "cinemaid",
                        Value = m.CinemaCode,
                        DbType = DbType.Int16
                    };

                    cmd.Parameters.Add(p1);
                    cmd.Parameters.Add(p2);
                    cmd.Parameters.Add(p3);
                    c.Open();
                    int resultado = cmd.ExecuteNonQuery();
                    c.Close();
                    if (resultado > 0)
                    {
                        message = NotifyModalchanges();
                        return message;
                    }
                    else
                    {
                        message.error = true;
                        message.number = 1;
                        message.message = "No se realizó ninguna modificacion. por favor revise los parametros..";
                        return message;
                    }
                }
            }
            catch (Exception e)
            {
                message.error = true;
                message.number = 1;
                message.message = e.Message;
                return message;
            }

            #region Metodo Viejo (Solo FileCaching)
            //var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysCloudService"].ToString();
            //MessagesPack pack = new MessagesPack();
            //pack.MessageList = ModalCinemaList;
            //pack.Token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();

            //if (cloudServiceName == "localhost")
            //{
            //    Service ser = new Service();
            //    message = ser.Execute<MessagesPack, Message>("Concessions.ModifyModal", pack);
            //    return message;
            //}
            //else
            //{
            //    //metodo para acceder a multiples instancias de cloudServices
            //    #region establish connection to CloudServices 
            //    try
            //    {
            //        var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
            //        var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
            //        var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
            //        var cert = new X509Certificate2(Convert.FromBase64String(certString));
            //        var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
            //        var computeManagementClient = new ComputeManagementClient(credentials);
            //        var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
            //        var deployments = cloudServiceDetails.Deployments;
            //        #region Serializa el model
            //        //XmlObjectSerializer obj = GetSerializer<T>();
            //        XmlObjectSerializer obj = new DataContractJsonSerializer(typeof(MessagesPack));
            //        MemoryStream stream = new MemoryStream();
            //        obj.WriteObject(stream, pack);
            //        string data = Encoding.UTF8.GetString(stream.ToArray(), 0, (int)stream.Length);
            //        #endregion Serializa el model
            //        foreach (var deployment in deployments)
            //        {
            //            foreach (var instance in deployment.RoleInstances)
            //            {
            //                if (deployment.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production)
            //                {
            //                    //llamar a servicio via rest por cada IP interna.
            //                    string url = string.Format("http://{0}/Concessions.svc/ModifyModalConcessions/", instance.IPAddress.ToString());
            //                    WebClient webClient1 = new WebClient();
            //                    webClient1.Headers.Add("ClientID", "111.111.111.120 ");
            //                    webClient1.Headers.Add("Content-type", "application/json");
            //                    webClient1.Encoding = Encoding.UTF8;
            //                    var result = webClient1.UploadString(url, "POST", data); //Ejecuta la llamada al servicio
            //                    message = JsonConvert.DeserializeObject<Message>(result);
            //                    //byte[] response = System.Text.Encoding.UTF8.GetBytes(result);
            //                    //return Convert<R>(response);
            //                    //stream = new MemoryStream(response);
            //                    //XmlObjectSerializer obj2 = new DataContractSerializer(typeof(Message));
            //                    //Message model = null;
            //                    //if (!stream.Length.Equals(0))
            //                    //{
            //                    //    message = obj.ReadObject(stream) as Message;
            //                    //}
            //                }
            //            }
            //        }
            //        return message;
            //    }
            //    catch (Exception ex)
            //    {
            //        message.message = ex.ToString();
            //        message.error = true;
            //        message.number = 1;
            //        return message;
            //    }
            //    #endregion
            //}
            #endregion
        }

        public Message NotifyModalchanges()
        {
            Message message = new Message();
            var cloudServiceName = System.Configuration.ConfigurationManager.AppSettings["DeleteKeysCloudService"].ToString();
            MessagesPack pack = new MessagesPack();
            //pack.MessageList = ModalCinemaList;
            pack.Token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();

            if (cloudServiceName == "localhost")
            {
                Service ser = new Service();
                message = ser.Execute<MessagesPack, Message>("Concessions.ModifyModal", pack);
                return message;
            }
            else
            {
                //metodo para acceder a multiples instancias de cloudServices
                #region establish connection to CloudServices 
                try
                {
                    var token = System.Configuration.ConfigurationManager.AppSettings["Token"].ToString();
                    var subscriptionId = System.Configuration.ConfigurationManager.AppSettings["subscription"].ToString();
                    var certString = System.Configuration.ConfigurationManager.AppSettings["certificate"].ToString();
                    var cert = new X509Certificate2(Convert.FromBase64String(certString));
                    var credentials = new Microsoft.Azure.CertificateCloudCredentials(subscriptionId, cert);
                    var computeManagementClient = new ComputeManagementClient(credentials);
                    var cloudServiceDetails = computeManagementClient.HostedServices.GetDetailed(cloudServiceName);
                    var deployments = cloudServiceDetails.Deployments;
                    #region Serializa el model
                    //XmlObjectSerializer obj = GetSerializer<T>();
                    XmlObjectSerializer obj = new DataContractJsonSerializer(typeof(MessagesPack));
                    MemoryStream stream = new MemoryStream();
                    obj.WriteObject(stream, pack);
                    string data = Encoding.UTF8.GetString(stream.ToArray(), 0, (int)stream.Length);
                    #endregion Serializa el model
                    foreach (var deployment in deployments)
                    {
                        foreach (var instance in deployment.RoleInstances)
                        {
                            if (deployment.DeploymentSlot == Microsoft.WindowsAzure.Management.Compute.Models.DeploymentSlot.Production)
                            {
                                //llamar a servicio via rest por cada IP interna.
                                string url = string.Format("http://{0}/Concessions.svc/ModifyModalConcessions/", instance.IPAddress.ToString());
                                WebClient webClient1 = new WebClient();
                                webClient1.Headers.Add("ClientID", "111.111.111.120 ");
                                webClient1.Headers.Add("Content-type", "application/json");
                                webClient1.Encoding = Encoding.UTF8;
                                var result = webClient1.UploadString(url, "POST", data); //Ejecuta la llamada al servicio
                                message = JsonConvert.DeserializeObject<Message>(result);
                                //byte[] response = System.Text.Encoding.UTF8.GetBytes(result);
                                //return Convert<R>(response);
                                //stream = new MemoryStream(response);
                                //XmlObjectSerializer obj2 = new DataContractSerializer(typeof(Message));
                                //Message model = null;
                                //if (!stream.Length.Equals(0))
                                //{
                                //    message = obj.ReadObject(stream) as Message;
                                //}
                            }
                        }
                    }
                    #endregion
                    return message;
                }
                catch (Exception ex)
                {
                    message.message = ex.ToString();
                    message.error = true;
                    message.number = 1;
                    return message;
                }
            }
        }

        public Message GetSalesReportFilterByDate(String date)
        {
            Message msg = new BO.Message();
            var response = new ConcessionTransactionByDate();
            String json_serialize = string.Empty;
            try
            {
                response = spGetConcessionTransactionsByDate(date, true);
                json_serialize = JsonConvert.SerializeObject(response);
                msg.message = json_serialize;
            }
            catch (Exception ex)
            {
                msg.message = "Error: " + ex.Message.ToString();
            }
            return msg;
        }

        public Message GetSalesReportComponentsFilterByDate(String date)
        {
            Message msg = new BO.Message();
            var response = new ConcessionTransactionComponentsByDate();
            String json_serialize = string.Empty;
            try
            {
                response = spGetConcessionComponentsTransactionsByDate(date, true);
                json_serialize = JsonConvert.SerializeObject(response);
                msg.message = json_serialize;
            }
            catch (Exception ex)
            {
                msg.message = "Error: " + ex.Message.ToString();
            }
            return msg;
        }

        private ConcessionTransactionByDate spGetConcessionTransactionsByDate(String date, bool restrictByTime)
        {
            //var connectionString = ConfigurationManager.AppSettings["ConnectionName"];
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VISTA"].ToString());
            var listTransactions = new List<ConcessionTransactionByDateF>();

            using (var command = new SqlCommand())
            {
                command.Connection = conn;

                command.CommandText = "spCU_GetConcessionsTransactionsFilterByDate";
                command.CommandType = CommandType.StoredProcedure;
                String[] PRUEBA = date.Split(';');

                command.Parameters.Add("@DateFrom", SqlDbType.VarChar).Value = PRUEBA[0];
                command.Parameters.Add("@DateUntil", SqlDbType.VarChar).Value = PRUEBA[1];
                command.Parameters.Add("@LocationCode", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["ConcessionsLocationStrCode"];
                command.Parameters.Add("@WorkstationName", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["ConcessionsWorkstationName"];
                //command.Parameters.Add("@RestrictTime", SqlDbType.VarChar).Value = restrictByTime ? "Y" : "N";

                DateTime value = new DateTime(9999, 1, 1);
                conn.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var transaction = new ConcessionTransactionByDateF();
                        transaction.TransactionId = Convert.ToString(reader["TransI_lgnNumber"]) == null ? "" : reader["TransI_lgnNumber"].ToString();
                        transaction.TransactionType = (string)(reader["TransI_strType"]) == null ? "" : reader["TransI_strType"].ToString();
                        transaction.TransactionStatus = Convert.ToString(reader["TransI_strStatus"]) == null ? "" : reader["TransI_strStatus"].ToString();
                        transaction.ItemId = (string)(reader["Item_strItemId"]) == null ? "" : reader["Item_strItemId"].ToString();
                        transaction.ItemDescription = (string)reader["Item_strItemDescription"] == null ? "" : reader["Item_strItemDescription"].ToString();
                        transaction.ItemValue = Convert.ToDecimal(reader["TransI_curValueEach"]);
                        transaction.ItemQuantity = Convert.ToInt32(reader["TransI_decNoOfItems"]);
                        transaction.SubTotal = transaction.ItemValue * transaction.ItemQuantity;
                        transaction.Vendor = (string)(reader["Vendor_strName"]) == null ? "" : reader["Vendor_strName"].ToString();
                        transaction.LocationDescription = (string)(reader["Location_strDescription"]) == null ? "" : reader["Location_strDescription"].ToString();
                        transaction.Workstation = (string)(reader["Workstation_strName"]) == null ? "" : reader["Workstation_strName"].ToString();
                        transaction.UserName = (string)(reader["User_strUserName"]) == null ? "" : reader["User_strUserName"].ToString();
                        transaction.FilmTitle = reader["Film_strTitle"].ToString() + " " + Convert.ToDateTime(reader["Session_dtmShowing"]).ToString("H:mm");
                        transaction.FilmDate = reader["Session_dtmShowing"].ToString() == "" ? value : (DateTime)(reader["Session_dtmShowing"]);
                        transaction.CollectionDate = reader["TransI_dtmDateCollected"].ToString() == "" ? value : (DateTime)(reader["TransI_dtmDateCollected"]);
                        transaction.PickupWorkstation = reader["BookingD_strPickupWorkstn"] != DBNull.Value ? (string)(reader["BookingD_strPickupWorkstn"]) : "";
                        transaction.PickupUserId = reader["BookingD_intPickupUser"] != DBNull.Value ? Convert.ToInt32(reader["BookingD_intPickupUser"]) : 0;
                        transaction.PickupUser = reader["PickupUser_strUserName"] != DBNull.Value ? (string)(reader["PickupUser_strUserName"]) : "";
                        transaction.Pickup = (string)(reader["Pickup"]);
                        listTransactions.Add(transaction);
                    }
                }

                conn.Close();
            }
            var response = new ConcessionTransactionByDate();
            response.Transactions = listTransactions;
            return response;
        }

        private ConcessionTransactionComponentsByDate spGetConcessionComponentsTransactionsByDate(String date, bool restrictByTime)
        {
            //var connectionString = ConfigurationManager.AppSettings["ConnectionName"];
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VISTA"].ToString());
            var listTransactions = new List<ConcessionTransactionByDateC>();

            using (var command = new SqlCommand())
            {
                command.Connection = conn;

                command.CommandText = "spCU_GetConcessionsComponentsTransactionsFilterByDate";
                command.CommandType = CommandType.StoredProcedure;
                String[] PRUEBA = date.Split(';');

                command.Parameters.Add("@DateFrom", SqlDbType.VarChar).Value = PRUEBA[0];
                command.Parameters.Add("@DateUntil", SqlDbType.VarChar).Value = PRUEBA[1];
                command.Parameters.Add("@LocationCode", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["ConcessionsLocationStrCode"];
                command.Parameters.Add("@WorkstationName", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["ConcessionsWorkstationName"];
                //command.Parameters.Add("@RestrictTime", SqlDbType.VarChar).Value = restrictByTime ? "Y" : "N";

                DateTime value = new DateTime(9999, 1, 1);
                conn.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var transaction = new ConcessionTransactionByDateC();
                        transaction.ItemId = (string)(reader["Item_strItemId"]) == null ? "" : reader["Item_strItemId"].ToString();
                        transaction.ItemDescription = (string)reader["Item_strItemDescription"] == null ? "" : reader["Item_strItemDescription"].ToString();
                        transaction.ItemQuantity = Convert.ToInt32(reader["TransI_decNoOfItems"]);
                        transaction.PickupWorkstation = reader["BookingD_strPickupWorkstn"] != DBNull.Value ? (string)(reader["BookingD_strPickupWorkstn"]) : "";
                        transaction.PickupUser = (string)(reader["PickupUser_strFirstName"]) + " " + (string)(reader["PickupUser_strLastName"]);
                        transaction.Componentes = (string)(reader["HOPK1"]) + " " + (string)(reader["ItemDescriptionComponent"]);
                        transaction.Cantidad_Componentes = reader["BOM_curItemQty"].ToString() == "" ? double.Parse("0.00") : double.Parse(reader["BOM_curItemQty"].ToString());
                        transaction.Total_Cantidad_Componentes = double.Parse(reader["BOM_curItemQty"].ToString()) * Convert.ToInt32(reader["TransI_decNoOfItems"]);
                        listTransactions.Add(transaction);
                    }
                }

                conn.Close();
            }
            var response = new ConcessionTransactionComponentsByDate();
            response.Transactions = listTransactions;
            return response;
        }

        public ModalCinema GetModalConcessionsStatus(ModalCinema modal)
        {
            //ModalCinema modal = new ModalCinema();
            using (SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ToString()))
            {
                string csql = "select * from ModalConcessions where CinemaId = @CinemaID ";
                SqlParameter p = new SqlParameter
                {
                    ParameterName = "CinemaID",
                    Value = modal.CinemaCode,
                    DbType = DbType.Int16
                };
                SqlCommand cmd = new SqlCommand(csql, c);
                cmd.Parameters.Add(p);

                try
                {
                    c.Open();
                    SqlDataReader r = cmd.ExecuteReader();
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            modal.CinemaCode = int.Parse(r[0].ToString());
                            modal.CinemaName = r[1].ToString();
                            modal.ModalEnabled = bool.Parse(r[2].ToString());
                            modal.DateChanged = DateTime.Parse(r[3].ToString());
                        }
                    }
                }
                catch (Exception e)
                {

                }
                finally
                {
                    c.Close();
                }
            }

            return modal;
        }

        /// <summary>
        /// obtiene los items disponibles para la venta en la web siempre y cuabndo el tipo de articulo sea tipo "S"
        /// </summary>
        /// <returns></returns>
        public List<TransferItem> GetConcessionsItemsForCinema(string dinamicConnection)
        {
            List<TransferItem> list = new List<TransferItem>();
            try
            {
                string connectionString;
                if (string.IsNullOrEmpty(dinamicConnection))
                {
                    connectionString = ConfigurationManager.ConnectionStrings["VISTA"].ToString();
                }
                else
                {
                    connectionString = string.Format(ConfigurationManager.ConnectionStrings["VistaDinamicConection"].ToString(), dinamicConnection);
                }
                SqlConnection con = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("spCU_GetConcessionsByTheater", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter p = new SqlParameter
                {
                    ParameterName = "Location_strCode",
                    Value = "0020",
                    SqlDbType = System.Data.SqlDbType.Text
                };
                cmd.Parameters.Add(p);
                con.Open();
                SqlDataReader d = cmd.ExecuteReader();
                while (d.Read())
                {
                    if (d[14].ToString() != "C")
                    {
                        TransferItem i = new TransferItem
                        {
                            itemCode = d[0].ToString(),
                            itemDescription = d[7].ToString(),
                            itemType = d[14].ToString(),
                            Quantity = 0,
                            UnidadDeMedida = d[9].ToString(),
                        };
                        list.Add(i);
                    }

                }
                con.Close();
                return list;
            }
            catch (Exception e)
            {
                return list;
            }
        }


        /// <summary>
        /// obtiene cada uno de los componentes de los combos o articulos finales de venta por la web
        /// </summary>
        /// <param name="items">la lista de combos o articulos finales que se desea trasladar</param>
        /// <returns></returns>
        public List<ItemAndComponets> VerifyItemsForTranslate(List<TransferItem> items)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["VISTA"].ToString());
            SqlCommand cmd;
            string query = "";
            List<ItemAndComponets> lista = new List<ItemAndComponets>();
            foreach (var it in items)
            {
                try
                {
                    if (it.itemType == null || it.itemType == "" || it.itemType == string.Empty)
                    {
                        query = string.Format("select ti.Item_strMasterItemCode, Item_strBaseUOMCode,(select Stock_curAvailable from tblStock_Status ss where ss.Location_strCode = '0020' and ss.Item_strItemId = '{0}') as stock from tblItem ti where ti.Item_strItemId = '{0}'", it.itemCode);
                        cmd = new SqlCommand(query, con);
                        ItemAndComponets ic = new ItemAndComponets();
                        ic.PrincipalItem = it;
                        con.Open();
                        SqlDataReader r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            decimal val;
                            ItemComponent c = new ItemComponent
                            {
                                ComponentId = it.itemCode,
                                ComponentOriginalQty = it.Quantity,
                                ComponentDescription = it.itemDescription,
                                ComponentStock = decimal.TryParse(r[2].ToString(), out val) ? val : 0,
                                UOM = (string)r[1],
                                MasterID = (string)r[0]
                            };
                            ic.ItemComponents.Add(c);
                        }
                        lista.Add(ic);
                        con.Close();
                    }
                    else
                    {
                        query = string.Format("SELECT B.BOM_strItemId, B.BOM_curItemQty, S.Item_strItemDescription, "
                       + "ISNULL(B.Location_strCode, '0020') AS Location_strCode, S.STax_strCode, "
                       + "(select Stock_curAvailable from tblStock_Status ss where ss.Location_strCode = '0020' "
                       + "and ss.Item_strItemId = B.BOM_strItemId) as stock, S.Item_strBaseUOMCode, "
                       + "(select ti.Item_strMasterItemCode from tblItem ti where ti.Item_strItemId = B.BOM_strItemId)  as MasterID "
                       + "FROM tblBOM B INNER JOIN tblItem S  ON S.Item_strItemId = B.BOM_strItemId "
                       + "WHERE S.Item_strStatus = 'A' and B.Item_strItemId = '{0}'", it.itemCode);
                        cmd = new SqlCommand(query, con);
                        ItemAndComponets ic = new ItemAndComponets();
                        ic.PrincipalItem = it;
                        con.Open();
                        SqlDataReader r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            decimal val;
                            ItemComponent c = new ItemComponent
                            {
                                ComponentId = r[0].ToString(),
                                ComponentOriginalQty = decimal.Parse(r[1].ToString()),
                                ComponentDescription = r[2].ToString(),
                                ComponentStock = decimal.TryParse(r[5].ToString(), out val) ? val : 0,
                                UOM = r[6].ToString(),
                                MasterID = r[7].ToString()
                            };
                            ic.ItemComponents.Add(c);
                        }
                        lista.Add(ic);
                        con.Close();
                    }
                }
                catch (Exception e)
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    throw e;
                }
            }
            return lista;
        }

        /// <summary>
        /// ejecuta la llamada al servico que hace el movimiento de inventario en la base de datos.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<ItemComponent> ConfirmItemsForTranslate(List<ItemComponent> items)
        {
            SqlConnection con = new SqlConnection();
            try
            {
                string cine = ConfigurationManager.AppSettings["ConnectionName"].ToString();
                string vistaUser = ConfigurationManager.AppSettings["vistaUser"].ToString();
                string vistaPWD = ConfigurationManager.AppSettings["vistaPWD"].ToString();
                string origen = ConfigurationManager.AppSettings["ItemLocationFrom"].ToString();
                string destino = ConfigurationManager.AppSettings["ItemLocationTo"].ToString();
                con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["VISTA"].ToString();
                SqlCommand cmd = new SqlCommand("CU_ConcesionExpress_MovimientosInventario", con);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (var i in items)
                {
                    #region Parametros SQL
                    SqlParameter[] parametros = new SqlParameter[]
                    {
                    new SqlParameter
                    {
                        ParameterName = "@ParamHOItem",
                        Value = i.MasterID,
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamItem",
                        Value = i.ComponentId
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamQty",
                        Value = i.ComponentOriginalQty,
                        SqlDbType = SqlDbType.Decimal
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamUOM",
                        Value = i.UOM
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamReference",
                        Value = string.Format("{0} - {1}", "Movimiento de Inventario CMS", cine)
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamDescription",
                        Value = string.Format("{0} - {1}", "Movimiento de Inventario CMS", cine)
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamStockLocation",
                        Value = origen
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamDestLocation",
                        Value = destino
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamWORKStationName",
                        Value = cine
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamUserID",
                        Value = vistaUser
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamUserPWD",
                        Value = vistaPWD
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamReasonCode",
                        Value = ""
                    }
                    };
                    #endregion
                    cmd.Parameters.AddRange(parametros);
                    con.Open();
                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        i.ComponentId = i.ComponentId + " - Traslado: " + r[0].ToString();
                    }
                    con.Close();
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception e)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                //si ocurre un error lo mando al front como in item de la lista para poderlo ver
                ItemComponent i = new ItemComponent
                {
                    ComponentId = "ERROR " + e.Message,
                    ComponentDescription = e.InnerException.ToString()
                };
                items.Add(i);
            }
            return items;
        }

        /// <summary>
        /// Ramiro Marimon
        /// este metodo se comunica con uno de los servidores de la nube para poder accerder a azure y poder actualizar la tabla de carameleria
        /// NOTA: debe comunicarse con solo 1 servidor de la nuba ya que si se comunica con dos o mas servidores, 
        /// entonces la data se multiplica en la BD y eso no debe ocurrir.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public TransferPack UpdateCandiesTable(PackViewModel items)
        {
            //do Stuff
            TransferPack message = new TransferPack();
            Service ser = new Service();
            try
            {
                var cloudServiceName = ConfigurationManager.AppSettings["DeleteKeysCloudService"].ToString();
                TransferPack pack = new TransferPack();
                pack.Token = ConfigurationManager.AppSettings["Token"].ToString();
                pack.idCine = items.cinemaId;
                pack.MessageList = JsonConvert.SerializeObject(items.items);
                message = ser.Execute<TransferPack, TransferPack>("Concessions.UpdateCandiesTable", pack);
            }
            catch (Exception ex)
            {
                message.MessageList = ex.Message;
                message.Token = "005";
            }
            return message;
        }


        /// <summary>
        /// Ramiro Marimón
        /// metodo para hacer devoluciones de traslado de inventarios.
        /// se ejecuta caundo falla la actualizacion de la tabla de azure.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<ItemComponent> ReverseTranslate(List<ItemComponent> items)
        {
            SqlConnection con = new SqlConnection();
            try
            {
                string cine = ConfigurationManager.AppSettings["ConnectionName"].ToString();
                string vistaUser = ConfigurationManager.AppSettings["vistaUser"].ToString();
                string vistaPWD = ConfigurationManager.AppSettings["vistaPWD"].ToString();
                string origen = ConfigurationManager.AppSettings["ItemLocationFrom"].ToString();
                string destino = ConfigurationManager.AppSettings["ItemLocationTo"].ToString();
                con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["VISTA"].ToString();
                SqlCommand cmd = new SqlCommand("CU_ConcesionExpress_MovimientosInventario", con);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (var i in items)
                {
                    #region Parametros SQL
                    SqlParameter[] parametros = new SqlParameter[]
                    {
                    new SqlParameter
                    {
                        ParameterName = "@ParamHOItem",
                        Value = i.MasterID,
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamItem",
                        Value = i.ComponentId
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamQty",
                        Value = i.ComponentOriginalQty,
                        SqlDbType = SqlDbType.Decimal
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamUOM",
                        Value = i.UOM
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamReference",
                        Value = string.Format("{0} - {1}", "Reverso Movimiento de Inventario CMS", cine)
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamDescription",
                        Value = string.Format("{0} - {1}", "Reverso Movimiento de Inventario CMS", cine)
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamStockLocation",
                        Value = destino
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamDestLocation",
                        Value = origen
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamWORKStationName",
                        Value = cine.Length >8? cine.Substring(0,8):cine
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamUserID",
                        Value = vistaUser
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamUserPWD",
                        Value = vistaPWD
                    },
                    new SqlParameter
                    {
                        ParameterName = "@ParamReasonCode",
                        Value = " "
                    }
                    };
                    #endregion
                    cmd.Parameters.AddRange(parametros);
                    con.Open();
                    SqlDataReader r = cmd.ExecuteReader(); //falla esta aqui
                    while (r.Read())
                    {
                        i.ComponentId = i.ComponentId + " - Trasladado.";
                    }
                    con.Close();
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception e)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                //si ocurre un error lo mando al front como in item de la lista para poderlo ver
                ItemComponent i = new ItemComponent
                {
                    ComponentId = "ERROR " + e.Message,
                    ComponentDescription = e.InnerException != null ? e.InnerException.ToString() : " "
                };
                items.Add(i);
            }
            return items;
        }

        public List<ItemAndComponets> GetConcessionsItemsInCinema(string CinemaId)
        {
            List<TransferItem> items = new List<TransferItem>();
            List<ItemAndComponets> datos = new List<ItemAndComponets>();
            string csql = string.Format("select * from tblCandiesWeb where Cinema_strCode = '{0}' and Item_decStockAvailable > 0", CinemaId);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ToString());
            SqlCommand cmd = new SqlCommand(csql, con);
            try
            {
                con.Open();
                SqlDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    TransferItem it = new TransferItem
                    {
                        itemCode = (string)r[1],
                        itemDescription = (string)r[2],
                        Quantity = (int)decimal.Parse(r[3].ToString()),
                        itemType = (string)r[5],
                        UnidadDeMedida = (string)r[6]
                    };
                    items.Add(it);
                }
                con.Close();
                cmd.Dispose();

                if (items.Count > 0)
                {
                    datos = VerifyItemsForTranslate(items);
                }
            }
            catch (Exception e)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                TransferItem i = new TransferItem
                {
                    itemCode = "Error",
                    itemDescription = e.Message,
                };
                items.Add(i);
            }
            return datos;
        }

        public List<TransferItem> GetAviableConcessionsItemsInCinema(string CinemaId)
        {
            List<TransferItem> items = new List<TransferItem>();
            //string csql = string.Format("select * from tblCandiesWeb where Cinema_strCode = '{0}' and Item_decStockAvailable > 0", CinemaId);
            string csql = string.Format("select * from tblCandiesWeb where Cinema_strCode = '{0}'", CinemaId);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ToString());
            SqlCommand cmd = new SqlCommand(csql, con);
            try
            {
                con.Open();
                SqlDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    TransferItem it = new TransferItem
                    {
                        itemCode = (string)r[1],
                        itemDescription = (string)r[2],
                        Quantity = (int)decimal.Parse(r[3].ToString()),
                        itemType = (string)r[5],
                        UnidadDeMedida = (string)r[6]
                    };
                    items.Add(it);
                }
                con.Close();
                cmd.Dispose();
            }
            catch (Exception e)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                TransferItem i = new TransferItem
                {
                    itemCode = "Error",
                    itemDescription = e.Message,
                };
                items.Add(i);
            }
            return items;
        }

        public List<ItemComponent> ConfirmItemsForReverse(List<ItemComponent> items)
        {

            throw new NotImplementedException();
        }

        public List<ItemAndComponets> VerifyItemsForReverse(List<TransferItem> items)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["VISTA"].ToString());
            SqlCommand cmd;
            string query = "";
            List<ItemAndComponets> lista = new List<ItemAndComponets>();
            foreach (var it in items)
            {
                try
                {
                    if (it.itemType == null || it.itemType == "" || it.itemType == string.Empty)
                    {
                        query = string.Format("select ti.Item_strMasterItemCode, Item_strBaseUOMCode,(select Stock_curAvailable from tblStock_Status ss where ss.Location_strCode = '0010' and ss.Item_strItemId = '{0}') as stock from tblItem ti where ti.Item_strItemId = '{0}'", it.itemCode);
                        cmd = new SqlCommand(query, con);
                        ItemAndComponets ic = new ItemAndComponets();
                        ic.PrincipalItem = it;
                        con.Open();
                        SqlDataReader r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            decimal val;
                            ItemComponent c = new ItemComponent
                            {
                                ComponentId = it.itemCode,
                                ComponentOriginalQty = it.Quantity,
                                ComponentDescription = it.itemDescription,
                                ComponentStock = decimal.TryParse(r[2].ToString(), out val) ? val : 0,
                                UOM = (string)r[1],
                                MasterID = (string)r[0]
                            };
                            ic.ItemComponents.Add(c);
                        }
                        lista.Add(ic);
                        con.Close();
                    }
                    else
                    {
                        query = string.Format("SELECT B.BOM_strItemId, B.BOM_curItemQty, S.Item_strItemDescription, "
                       + "ISNULL(B.Location_strCode, '0010') AS Location_strCode, S.STax_strCode, "
                       + "(select Stock_curAvailable from tblStock_Status ss where ss.Location_strCode = '0010' "
                       + "and ss.Item_strItemId = B.BOM_strItemId) as stock, S.Item_strBaseUOMCode, "
                       + "(select ti.Item_strMasterItemCode from tblItem ti where ti.Item_strItemId = B.BOM_strItemId)  as MasterID "
                       + "FROM tblBOM B INNER JOIN tblItem S  ON S.Item_strItemId = B.BOM_strItemId "
                       + "WHERE S.Item_strStatus = 'A' and B.Item_strItemId = '{0}'", it.itemCode);
                        cmd = new SqlCommand(query, con);
                        ItemAndComponets ic = new ItemAndComponets();
                        ic.PrincipalItem = it;
                        con.Open();
                        SqlDataReader r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            decimal val;
                            ItemComponent c = new ItemComponent
                            {
                                ComponentId = r[0].ToString(),
                                ComponentOriginalQty = decimal.Parse(r[1].ToString()),
                                ComponentDescription = r[2].ToString(),
                                ComponentStock = decimal.TryParse(r[5].ToString(), out val) ? val : 0,
                                UOM = r[6].ToString(),
                                MasterID = r[7].ToString()
                            };
                            ic.ItemComponents.Add(c);
                        }
                        lista.Add(ic);
                        con.Close();
                    }
                }
                catch (Exception e)
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    throw e;
                }
            }
            return lista;
        }

        public TransferPack DecreaseCandiesTable(PackViewModel items)
        {
            TransferPack message = new TransferPack();
            Service ser = new Service();
            try
            {
                var cloudServiceName = ConfigurationManager.AppSettings["DeleteKeysCloudService"].ToString();
                TransferPack pack = new TransferPack();
                pack.Token = ConfigurationManager.AppSettings["Token"].ToString();
                pack.idCine = items.cinemaId;
                pack.MessageList = JsonConvert.SerializeObject(items.items);
                message = ser.Execute<TransferPack, TransferPack>("Concessions.DecreaseCandiesTable", pack);
            }
            catch (Exception ex)
            {
                message.MessageList = ex.Message;
                message.Token = "005";
            }
            return message;
        }

        public string ModifyCandiesWeb(string data)
        {
            var prueba = JsonConvert.DeserializeObject<List<CandiesWeb>>(data);
            SqlConnection conexion = new SqlConnection();
            try
            {
                conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ToString());

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conexion;
                conexion.Open();
                foreach (var item2 in prueba)
                {
                    cmd.CommandText = @"UPDATE tblCandiesWeb SET Item_decStockAvailable = Item_decStockAvailable + " + Convert.ToInt32(item2.Quantity) +
                                  " WHERE Cinema_strCode = '" + item2.Cinema_strCode + "'  and Item_strItemId = '" + item2.Item_strItemId + "'";

                    cmd.ExecuteNonQuery();
                }
                conexion.Close();
                string ruta = Path.Combine(HostingEnvironment.MapPath("~/App_Data/ModifyCandiesWebLogCarameleria.txt"));
                using (StreamWriter w = new StreamWriter(ruta, true))
                {
                    w.WriteLine("Inicio---" + DateTime.Now.ToString());
                    w.WriteLine("Se modificó la tabla de azue desde: " + prueba[0].Cinema_strCode);
                    w.WriteLine(data);
                    w.WriteLine("Fin---");
                    w.Close();
                }
                return "true";
            }
            catch (Exception e)
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                string ruta = Path.Combine(HostingEnvironment.MapPath("~/App_Data/ModifyCandiesWebLogCarameleria.txt"));
                using (StreamWriter w = new StreamWriter(ruta, true))
                {
                    w.WriteLine("Inicio---" + DateTime.Now.ToLongTimeString());
                    w.WriteLine("NO Se modificó la tabla de azue desde: " + prueba[0].Cinema_strCode);
                    w.WriteLine(data);
                    w.WriteLine(e.Message);
                    w.WriteLine("Fin---");
                    w.Close();
                }
                return "false";
            }
        }
        public List<ModalCinema> GetAllModalConcessionsStatus()
        {
            try
            {
                List<ModalCinema> modalCinemas = new List<ModalCinema>();
                string Query = "select * from ModalConcessions where CinemaID in" + ConfigurationManager.AppSettings["CinesP3"].ToString();
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ToString()))
                {
                    using (SqlCommand command = new SqlCommand(Query, connection))
                    {
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    ModalCinema modalCinema = new ModalCinema()
                                    {
                                        CinemaCode = (int)dataReader["CinemaID"],
                                        CinemaName = (string)dataReader["CinemaName"],
                                        ModalEnabled = (bool)dataReader["ModalEnabled"],
                                        DateChanged = (DateTime)dataReader["DateChanged"],
                                    };
                                    modalCinemas.Add(modalCinema);
                                }
                            }
                            else
                            {
                                dataReader.Close();
                                connection.Close();
                            }
                            dataReader.Close();
                        }
                    }
                    connection.Close();
                }
                return modalCinemas;
            }
            catch (Exception Ex)
            {
                string Mesage = Ex.Message;
                throw;
            }
        }

        public List<TransferItem> GetAllConcecion(string CinemaID)
        {
            try
            {
                List<TransferItem> transferItemsAzure = GetPrincipalItemsInCinema(CinemaID.Substring(3));
                List<TransferItem> transferItemsVista = GetConcessionsItemsForCinema(CinemaID.Substring(0, 3));
                foreach (TransferItem item in transferItemsVista)
                {
                    var a = transferItemsAzure.Where(t => t.itemCode.Equals(item.itemCode)).SingleOrDefault();
                    if (a == null)
                    {
                        item.Quantity = 0;
                    }
                    else
                    {
                        item.Quantity = a.Quantity;
                    }
                }
                return transferItemsVista;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private List<TransferItem> GetPrincipalItemsInCinema(string CinemaId)
        {
            List<TransferItem> items = new List<TransferItem>();
            string Query = string.Format("select * from tblCandiesWeb where Cinema_strCode = '{0}' and Item_decStockAvailable > 0", CinemaId);
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DB"].ToString());
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            SqlDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                TransferItem it = new TransferItem
                {
                    itemCode = (string)dataReader[1],
                    itemDescription = (string)dataReader[2],
                    Quantity = (int)decimal.Parse(dataReader[3].ToString()),
                    itemType = (string)dataReader[5],
                    UnidadDeMedida = (string)dataReader[6]
                };
                items.Add(it);
            }
            connection.Close();
            command.Dispose();
            return items;
        }
    }
}