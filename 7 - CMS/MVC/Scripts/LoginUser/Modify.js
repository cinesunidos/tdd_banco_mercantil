﻿//Modificar usuario
function valueLogin(count) {
    //se cargan los datos al objeto
    var loginUser = {
        Id: $("#id" + count).val(),
        UserId: $("#email" + count).val(),
        Password: $("#pass" + count).val(),
        Name: $("#name" + count).val(),
        Department: $("#dep" + count).val(),
        RoleId: $("#role" + count).val(),
        Permissions: null
    };
    //envia el objeto json al controlador
    $.ajax({
        type: "POST",
        url: "/Management/UpdateU",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(loginUser),
        success: function () {
            CallVista();
        }
    });
};