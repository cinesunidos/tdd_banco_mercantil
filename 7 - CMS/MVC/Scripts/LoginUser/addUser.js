﻿//Registrar nuevo usuarios
function AddLogin() {
    //se cargan los datos al objeto
    var loginUser = {
        Id: null,
        UserId: $("#emailAdd").val(),
        Password: $("#passwordAdd").val(),
        Name: $("#nameAdd").val(),
        Department: $("#depAdd").val(),
        RoleId: $("#roleAdd").val(),
        Permissions: null
    };
    //envia el objeto json al controlador
    $.ajax({
        type: "POST",
        url: "/Management/AddU",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(loginUser),
        success: function () {
            CallVista();
        }
    });
};