﻿window.onload = function () {
    $('#inv-module').hide();
    $('#black-module').hide();
    $('#redis-module').hide();
    $('#user-module').hide();
    $('#audit-module').hide();
    $('#promotions-module').hide();
    $('#loadmovies-module').hide();
    $('#concessions-module').hide();
    $('#Messages-module').hide();

    //3DF4E6C0-967C-4979-87FF-04D2A6907778
    $.ajax({
        type: "POST",
        url: "/Navbar/LoadPermissions",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            if (response.permList != null) {

                for (var i = 0; i < response.permList.length; i++) {
                 
                    switch ((response.permList[i].id).toUpperCase()) {

                        case 'F272A0A2-1F93-4633-B233-4C8DE6F76FFA':
                            $('#inv-module').show();
                            break;

                        case 'B8926247-A81B-4E8C-AD10-A7C47B085B82':
                            $('#black-module').show();
                            break;

                        case '38085660-F42F-454F-A680-17C456FA3EAD':
                            $('#redis-module').show();
                            break;

                        case '5BEAA5A6-4342-4B61-9B7E-5E0A0467ECCB':
                            $('#user-module').show();
                            break;

                        case '7B523437-2E08-47C0-9263-D1653C1F5363':
                            $('#audit-module').show();
                            break;

                        case '5110EDC9-9BF2-474E-A242-A0EF75394E4D':
                            $('#promotions-module').show();
                            break;

                        case '62BD5B02-8ABB-4017-807C-CA04AFE373D3':
                            $('#concessions-module').show();
                            break;

                        case '3DF4E6C0-967C-4979-87FF-04D2A6907778':
                            $('#Messages-module').show();
                            break;
                    }
                }
            }
        },
        error: function (data) {
            alert('ERROR ' + data.status + ' ' + data.statusText);
        }
    });
}