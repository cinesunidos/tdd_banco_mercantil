﻿//elimina una key
function RemoveKey(count) {
    var redis = {
        Id: $("#id" + count).val(),
        value: $("#value" + count).val(),
        Description: $("#description" + count).val()
    };
    $.ajax({
        type: "POST",
        url: "/Redis/RemoveKeyRedis",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(redis),
        success: function () {
            CallVista();
        }
    });
};