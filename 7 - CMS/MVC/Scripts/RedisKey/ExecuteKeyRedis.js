﻿//Confirmacion de Limpieza Redis
function confirmRedis(count) {
    $('.overlay').css('display', 'block');
    $('.confirmRedis').css('display', 'block');
    var link = $("#Value" + count).val();
    $('#btn-accept').val(link);
}

function cancel() {
    //$('.overlay').hide();
    //$('.confirmRedis').hide();
}

//ejecutar link al servicio RedisCacheManagement.svc del WCF de la página CU
function ExecuteRedisKey(count) {
    //$('.overlay').hide();
    //$('.confirmRedis').hide();
    $('.Msg').empty();
    $('.Msg').addClass("hidden");

    $("#exampleModalCenter").modal({
        'backdrop': 'static',
        'keyboard': false
    });

    var redis = {
        Id: null,
        Value: $('#Value' + count).val(),
        Description: null
    };

    $.ajax({
        type: "POST",
        url: "/Redis/ExecuteKeyRedis",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(redis),
        success: function (messages) {
            if (messages != null) {
                $('.Msg').removeClass("hidden");
                var l = messages.length;
                for (var i = 0; i < l; i++) {
                    $('.Msg').append('<div class="alert alert-success col-md-9 col-md-offset-1">"' + messages[i].message + '"</div>');
                }
            }
            $("#exampleModalCenter").modal('hide');
        },
        error: function (redis) {
            alert('ERROR ' + redis.status + ' ' + redis.statusText);
            $("#exampleModalCenter").modal('hide');
        }
    });
};