﻿//Modificar usuario
function ModifyKey(count) {
    //se cargan los datos al objeto
    var redis = {
        Id: $("#id" + count).val(),
        value: $("#value" + count).val(),
        description: $("#description" + count).val(),
    };
    //envia el objeto json al controlador
    $.ajax({
        type: "POST",
        url: "/Redis/ModifyKeyRedis",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(redis),
        success: function () {
            CallVista();
        }
    });
};