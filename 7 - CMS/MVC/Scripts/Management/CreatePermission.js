﻿// agrega un nuevo permiso
function CreatePermission() {

    var perm = {
        roleId: null,
        name: $("#name").val()
    };
    console.log($("#name").val());
    $.ajax({
        type: "POST",
        url: "/Management/CreatePermission",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(perm),
        success: function () {
            CallVista();
        }
    });
};