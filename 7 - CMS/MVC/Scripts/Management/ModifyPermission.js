﻿//actualiza un permiso
function ModifyPermissions(count) {
    var perm = {
        id: $("#id" + count).val(),
        Name: $("#name" + count).val()

    };
    $.ajax({
        type: "POST",
        url: "/Management/ModifyPermissions",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(perm),
        success: function () {
            CallVista();
        }
    });
};