﻿//funcion que lista todos los permisos existentes excepto los permisos del rol seleccionado.
//invocado solo desde AddPermission(); y DeletePermission();
function GetPermissionAll(role) {
    $('.getRoleAll').empty();
    $('.getAll').empty();

    $.ajax({
        type: "POST",
        url: "/Management/GetPermissionAll",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(role),
        success: function (permissions) {
            if (permissions.response != null) {

                for (var i = 0; i < permissions.response.length; i++) {

                    $('.getAll').append('<li class="list-group-item"><label class="checkbox-inline"><input class="disponibles" type="checkbox" value="' + permissions.response[i].id + '"><p>' + permissions.response[i].Name + '</p></label></li>');
                }
            }

            GetPermissionRole(role);
        },
        error: function (role) {
            alert('ERROR ' + data.status + ' ' + data.statusText);
        }
    });
};