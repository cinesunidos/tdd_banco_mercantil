﻿//actualiza un rol
function Update(count) {
    var Role = {
        roleId: $("#id" + count).val(),
        name: $("#name" + count).val()

    };
    $.ajax({
        type: "POST",
        url: "/Management/Update",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(Role),
        success: function () {
            CallVista();
        }
    });
};