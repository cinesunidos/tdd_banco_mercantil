﻿
//funcion que lista todos los permisos existentes excepto los permisos del rol seleccionado. 
//metodo llamado con el onclick del boton.
function GetRoleAll(count) {

    $('.getRoleAll').empty();
    $('.getAll').empty();
    $('.action-move').empty();
    // $('.roleID').empty();
    $('.roleSelectName').empty();
    $('.AllPermissions').empty();

    $('.action-move').append('<div class="row"><br />' +
                                '<button id="AddButton" type="button" onclick="AddPermission();" class="btn btn-default">>></button>' +
                             '</div>' +
                             '<div class="row"><br />' +
                                '<button type="button" onclick="DeletePermission();" class="btn btn-default"><<</button>' +
                             '</div>');

    var role = {
        roleId: $("#id" + count).val(),
        name: null
    };
    $.ajax({
        type: "POST",
        url: "/Management/GetPermissionAll",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(role),
        success: function (permissions) {
            if (permissions.response != null) {

                for (var i = 0; i < permissions.response.length; i++) {

                    $('.getAll').append('<li class="list-group-item"><label class="checkbox-inline"><input class="disponibles" type="checkbox" value="' + permissions.response[i].id + '"><p>' + permissions.response[i].Name + '</p></label></li>');
                }
            }
            $('.roleSelectName').append('<h6 id="selection" value="' + $("#id" + count).val() + '"> ' + $("#name" + count).val() + '</h6>');
            $('.AllPermissions').append('<h6>PERMISOS DISPONIBLES</h6>');
            $('#txtRoleId').val($("#id" + count).val());

            GetPermissionRole(role);
        },
        error: function (role) {
            alert('ERROR ' + data.status + ' ' + data.statusText);
        }
    });
};