﻿//agrega los permisos seleccionados de la lista (todos los permisos) hacia los permisos del rol seleccionado.
function DeletePermission() {

    var permissionID = new Array(($("input[type=checkbox]:checked.seleccionados").length));

    var count = 1;
    permissionID[0] = $("#txtRoleId").val();
    $("input[type=checkbox]:checked.seleccionados").each(function () {
        permissionID[count] = ($(this).val());
        count++;
    });
    console.log(permissionID)
    $.ajax({
        type: "POST",
        url: "/Management/DeletePermission",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(permissionID),
        success: function (response) {

            var role = {
                roleId: permissionID[0],
                name: null
            };
            $('.getRoleAll').empty();
            GetPermissionAll(role);
        },
        error: function (data) {
            alert('ERROR ' + data.status + ' ' + data.statusText);
        }
    });
}