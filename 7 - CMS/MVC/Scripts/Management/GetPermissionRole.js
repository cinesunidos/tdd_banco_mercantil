﻿// devuelve los permisos de un rol seleccionado.
function GetPermissionRole(role) {

    $.ajax({
        type: "POST",
        url: "/Management/GetPermissionRole",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(role),
        success: function (permissions) {
            if (permissions.response != null) {

                for (var i = 0; i < permissions.response.length; i++) {
                    $('.getRoleAll').append('<li class="list-group-item"><label class="checkbox-inline"><input class="seleccionados" type="checkbox" value="' + permissions.response[i].id + '"><p>' + permissions.response[i].Name + '</p></label></li>');
                }
            }
        },
        error: function (data) {
            alert('ERROR ' + data.status + ' ' + data.statusText);
        }
    });
}