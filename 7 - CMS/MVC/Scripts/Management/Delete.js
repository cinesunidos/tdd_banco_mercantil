﻿//elimina un rol
function Delete(count) {
    var Role = {
        roleId: $("#id" + count).val(),
        name: $("#name" + count).val()
    };
    $.ajax({
        type: "POST",
        url: "/Management/Delete",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(Role),
        success: function () {
            CallVista();
        }
    });
};