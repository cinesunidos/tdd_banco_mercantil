﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// La información general sobre un ensamblado se controla mediante lo siguiente
// conjunto de atributos. Cambie los valores de estos atributos para modificar la información
// asociada con un ensamblado.
[assembly: AssemblyTitle("cms-admin")]
[assembly: AssemblyDescription("Content Management System interno de Cines Unidos")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Cines Unidos")]
[assembly: AssemblyProduct("cms-admin")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Si configura ComVisible como falso, los tipos de este ensamblado no se hacen visibles
// para componentes COM.  Si necesita acceder a un tipo de este ensamblado desde
// COM, establezca el atributo ComVisible en True en este tipo.
[assembly: ComVisible(true)]

// El siguiente GUID sirve como ID de typelib si este proyecto se expone a COM
[assembly: Guid("c80bd3ad-09c3-4253-983c-3e9ec639ba38")]

// La información de versión de un ensamblado consta de los siguientes cuatro valores:
//
//      Versión principal
//      Versión secundaria
//      Número de compilación
//      Revisión
//
// Puede especificar todos los valores o puede predeterminar los números de compilación y de revisión
// mediante el carácter '*', como se muestra a continuación:
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
