﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace admin_usuarios
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode,
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-VE");
            //Se comenta este timeZone por el cambio de zona Horaria efectuado el dia 01/05/2016 (UTC - 04:30) Caracas
            //TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            //Se procede a colocar el Time Zone SA Western Standard Time que equivale a (UTC-04:00) Georgetown, La Paz, Manaus, San Juan
            TimeZoneInfo timeZone = System.TimeZoneInfo.FindSystemTimeZoneById("SA Western Standard Time");
            DateTime nowDateTime = DateTime.UtcNow;
            DateTime newDateTime = TimeZoneInfo.ConvertTime(nowDateTime, timeZone);
            newDateTime = new DateTime();

            HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (null == cookie)
            {
                return;
            }
            FormsAuthenticationTicket ticket = null;
            try
            {
                ticket = FormsAuthentication.Decrypt(cookie.Value);
            }
            catch
            {
                return;
            }
           
        }
    }

    public class UserPrincipal : GenericPrincipal
    {
        private string m_name;

        public UserPrincipal(IIdentity identity, string[] roles, string name)
            : base(identity, roles)
        {
            m_name = name;
        }

        public string FullName { get { return m_name; } }

    }
}