﻿using System.Web;
using System.Web.Optimization;

namespace admin_usuarios
{
    public class BundleConfig
    {
        // Para obtener más información sobre Bundles, visite http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                         "~/Scripts/jquery-ui.min.js",
                "~/Scripts/jquery.ui.datepicker-es.js"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/datatablesStyles").Include(
                      "~/Content/datatables/css/dataTables.bootstrap.min.css")
                      );

            bundles.Add(new ScriptBundle("~/bundles/datatablesScripts").Include(
                      "~/Content/datatables/js/jquery.dataTables.min.js",
                      "~/Content/datatables/js/dataTables.bootstrap.min.js"
                      )
                     );

            bundles.Add(new ScriptBundle("~/bundles/globalize").Include(
            "~/Scripts/globalize/globalize.js",
            "~/Scripts/globalize/cultures/globalize.culture.es-VE.js"));

        }
    }
}
