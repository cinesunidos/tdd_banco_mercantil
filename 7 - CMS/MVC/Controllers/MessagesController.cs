﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web.Hosting;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace admin_usuarios.Controllers
{
    public class MessagesController : Controller
    {
        // GET: Messages
        public ActionResult Index()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public ActionResult CreateMessage()
        {
            if (Session["User"] != null)
            {
                MessageForCinemaViewModel messages = new MessageForCinemaViewModel();
                List<MessageForCinema> mensages;
                string filePath = string.Empty;
#if DEBUG
                filePath = HostingEnvironment.MapPath("~/App_Data/Messages.txt");
#else
                string cachePath = RoleEnvironment.GetLocalResource("Messages").RootPath;
                filePath = Path.Combine(cachePath, "Messages.txt");
#endif
                if (!System.IO.File.Exists(filePath))
                {
                    //si el archivo de cache no existe, busco los mensajes en la base de datos
                    Service service = new Service();
                    var m = service.Execute<List<MessageForCinema>>("Messages.GetFromDB");
                    if (m != null && m.Count > 0)
                    {
                        messages.Messages = m;
                    }
                    return View(messages);
                }

                //si el archivo existe, entonces lo leo, serializo y lo devuelvo.
                using (StreamReader sr = new StreamReader(filePath))
                {
                    string a = sr.ReadToEnd();
                    sr.Close();
                    if (string.IsNullOrEmpty(a))
                    {
                        return View(messages);
                    }
                    else
                    {
                        mensages = JsonConvert.DeserializeObject<List<MessageForCinema>>(a);
                        messages.Messages = mensages;
                        return View(messages);
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult CreateMessage(MessageForCinemaViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult CreateMessage(MessageForCinemaViewModel model, string action)
        {
            if (action == "AddMessage")
            {
                if (model.Message.Category == Category.Elija)
                {
                    ModelState.AddModelError("category", "Seleccione categoría");
                    return View(model);
                }

                if (model.Message.Category == Category.GlobalMessage)
                {
                    var a = model.Messages.Where(x => x.Category == Category.GlobalMessage).SingleOrDefault();
                    if (a != null)
                    {
                        ModelState.AddModelError("category", "esta categoria ya tiene un mensaje");
                        return View(model);
                    }
                    else
                    {
                        ViewBag.TipoMessage = "Global";
                        return View(model);
                    }
                }

                if (model.Message.Category == Category.CityMessage)
                {
                    ViewBag.TipoMessage = "City";
                    return View(model);
                }

                if (model.Message.Category == Category.CinemaMessage)
                {
                    ViewBag.TipoMessage = "Cinema";
                    return View(model);
                }
            }

            if (action == "NewMessage")
            {
                if (model.Message.Category == Category.GlobalMessage)
                {
                    if (model.Message.Message == "" || string.IsNullOrEmpty(model.Message.Message))
                    {
                        ModelState.AddModelError("Message", "Debe especificar el mensaje a mostrar");
                        ViewBag.TipoMessage = "Global";
                        return View(model);
                    }

                    var a = model.Messages.Where(x => x.Category == Category.GlobalMessage).SingleOrDefault(); ;
                    if (a != null)
                    {
                        ModelState.AddModelError("Messasge", "Ya existe un mensaje global");
                        return View(model);
                    }
                }

                if (model.Message.Category == Category.CityMessage)
                {
                    if (model.Message.City == city.Elija)
                    {
                        ModelState.AddModelError("City", "Seleccione la ciudad donde se mostrará el mensaje");
                        ViewBag.TipoMessage = "City";
                        return View(model);
                    }

                    var a = model.Messages.Where(x => x.City == model.Message.City).SingleOrDefault();
                    if (a != null)
                    {
                        ModelState.AddModelError("Message", "Ya existe un mensaje para esta ciudad");
                        ViewBag.TipoMessage = "City";
                        return View(model);
                    }

                    if (model.Message.Message == "" || string.IsNullOrEmpty(model.Message.Message))
                    {
                        ModelState.AddModelError("Message", "Debe especificar el mensaje a mostrar");
                        ViewBag.TipoMessage = "City";
                        return View(model);
                    }
                }

                if (model.Message.Category == Category.CinemaMessage)
                {
                    if (model.Message.Cinema == Cinema.Elija_0000)
                    {
                        ModelState.AddModelError("Cinema", "Seleccione el cine donde se mostrará el mensaje");
                        ViewBag.TipoMessage = "Cinema";
                        return View(model);
                    }

                    var a = model.Messages.Where(x => x.Cinema == model.Message.Cinema).SingleOrDefault();
                    if (a != null)
                    {
                        ModelState.AddModelError("Message", "Ya existe mensaje para este cine");
                        ViewBag.TipoMessage = "Cinema";
                        return View(model);
                    }

                    if (model.Message.Message == "" || string.IsNullOrEmpty(model.Message.Message))
                    {
                        ModelState.AddModelError("Message", "Debe especificar el mensaje a mostrar");
                        ViewBag.TipoMessage = "Cinea";
                        return View(model);
                    }
                }

                model.Message.Activo = true;
                model.Messages.Add(model.Message);
                model.Message = new MessageForCinema();
                return View(model);
            }

            if (action == "quitar")
            {
                MessageForCinemaViewModel mm = new MessageForCinemaViewModel();
                mm.Messages = new List<MessageForCinema>();
                List<MessageForCinema> l = model.Messages.Where(m => m.Retirar == true).ToList();

                foreach (MessageForCinema item in l)
                {
                    model.Messages.Remove(item);
                }
                foreach (var item in model.Messages)
                {
                    mm.Messages.Add(item);
                }
                model = mm;
                return View("CreateMessage", model);
                //return RedirectToAction("CreateMessage","Messages",model);
            }

            if (action == "Guardar")
            {
                string jsn = JsonConvert.SerializeObject(model.Messages);
                string filePath = String.Empty;
#if DEBUG
                filePath = HostingEnvironment.MapPath("~/App_Data/Messages.txt");
#endif
#if (!DEBUG)
                string cachePath = RoleEnvironment.GetLocalResource("Messages").RootPath;
                filePath = Path.Combine(cachePath, "Messages.txt");
#endif
                if (!System.IO.File.Exists(filePath))
                {
                    System.IO.File.Create(filePath).Close();
                    using (StreamWriter Sw = new StreamWriter(filePath, false))
                    {
                        Sw.WriteLine(jsn);
                        Sw.Close();
                    };
                    Service ser = new Service();
                    //invocar el metodo Wcf SaveToDb para gradar los mensajes en base de datos
                    ser.Execute<List<MessageForCinema>, Message>("Messages.SaveToDB", model.Messages);
                    //invocar el metodo de Wcf ppublish para hacer caching del archivo en formato json
                    Message response = ser.Execute<string, Message>("Messages.Publish", jsn);

                    if (response.number == 0)
                    {
                        //some actions
                        ViewBag.TipoMessage = string.Empty;
                        ViewBag.Message = response.message;
                    }
                    else
                    {
                        ViewBag.TipoMessage = string.Empty;
                        ViewBag.Message = response.message;
                    }
                    return View();
                }
                else
                {
                    using (StreamReader sr = new StreamReader(filePath))
                    {
                        string ab = sr.ReadToEnd();
                        sr.Close();
                        if (string.IsNullOrEmpty(ab) || ab != jsn)
                        {
                            using (StreamWriter Sw = new StreamWriter(filePath, false))
                            {
                                Sw.WriteLine(jsn);
                                Sw.Close();
                            };
                            //invocar el metodo de Wcf ppublish
                            Service ser = new Service();
                            //invocar el metodo Wcf SaveToDb para gradar los mensajes en base de datos
                            ser.Execute<List<MessageForCinema>, Message>("Messages.SaveToDB", model.Messages);
                            //invocar el metodo Wcf Publish parapublicar los mensajes en los clouds
                            Message response = ser.Execute<string, Message>("Messages.Publish", jsn);

                            if (response.number == 0)
                            {
                                //some actions
                                ViewBag.TipoMessage = string.Empty;
                                ViewBag.Message = response.message;
                            }
                            else
                            {
                                ViewBag.TipoMessage = string.Empty;
                                ViewBag.Message = response.message;
                            }
                            return View();
                        }
                        else
                        {
                            ViewBag.TipoMessage = string.Empty;
                            ViewBag.Message = "Mensajes Guardados";
                            return View();
                        }
                    }
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult DeleteMessages()
        {
            string filePath = string.Empty;
#if DEBUG
            filePath = HostingEnvironment.MapPath("~/App_Data/Messages.txt");
#endif
#if (!DEBUG)
                string cachePath = RoleEnvironment.GetLocalResource("Messages").RootPath;
                filePath = Path.Combine(cachePath, "Messages.txt");
#endif
            if (!System.IO.File.Exists(filePath))
            {
                //si no existe el archivo, busco los emnsajes en base de datos
                Service service = new Service();
                var messages = service.Execute<List<MessageForCinema>>("Messages.GetFromDB");
                if (messages != null && messages.Count > 0)
                {
                    return View(messages);
                }
                else
                {
                    ViewBag.message = "No existen mensajes para borrar";
                    return View();
                }
            }

            using (StreamReader sr = new StreamReader(filePath))
            {
                string a = sr.ReadToEnd();
                sr.Close();
                if (a.Length < 10)
                {
                    ViewBag.message = "No existen mensajes para borrar";
                    return View();
                }
                else
                {
                    var messages = JsonConvert.DeserializeObject<List<MessageForCinema>>(a);
                    return View(messages);
                }
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DeleteMessages(List<MessageForCinema> mess, string action)
        {
            string filePath = string.Empty;
#if DEBUG
            filePath = HostingEnvironment.MapPath("~/App_Data/Messages.txt");
#endif
#if (!DEBUG)
                string cachePath = RoleEnvironment.GetLocalResource("Messages").RootPath;
                filePath = Path.Combine(cachePath, "Messages.txt");
#endif
            if (action == "quitar" && mess.Count > 0)
            {
                mess.Remove(mess.Where(m => m.Retirar == true).SingleOrDefault());
                if (mess.Count == 0)
                {
                    //System.IO.File.Delete(filePath);
                    string jsn = "";
                    using (StreamWriter Sw = new StreamWriter(filePath, false))
                    {
                        Sw.Write(jsn);

                    };
                    Service ser = new Service();
                    //invocar el metodo de Wcf SaveToDB
                    ser.Execute<List<MessageForCinema>, Message>("Messages.SaveToDB", mess);
                    //invocar el metodo de Wcf ppublish
                    Message response = ser.Execute<string, Message>("Messages.Publish", jsn);

                    if (response.number == 0)
                    {
                        ViewBag.TipoMessage = string.Empty;
                        if (response.message == "Publicación realizada satisfactoriamente")
                        {
                            ViewBag.Message = "Mensaje eliminado";
                        }
                        else
                        {
                            ViewBag.Message = response.message;
                        }
                    }
                    else
                    {
                        ViewBag.TipoMessage = string.Empty;
                        ViewBag.Message = response.message;
                    }

                    return View();
                }
                else
                {
                    return View(mess);
                }
            }

            if (action == "guardar" && mess.Count > 0)
            {
                string jsn = JsonConvert.SerializeObject(mess);
                if (!System.IO.File.Exists(filePath))
                {
                    System.IO.File.Create(filePath).Close();
                }
                using (StreamWriter Sw = new StreamWriter(filePath, false))
                {
                    Sw.WriteLine(jsn);
                };

                Service ser = new Service();
                //invocar el metodo de Wcf SaveToDB
                ser.Execute<List<MessageForCinema>, Message>("Messages.SaveToDB", mess);
                //invocar el metodo de Wcf Publish
                Message response = ser.Execute<string, Message>("Messages.Publish", jsn);

                if (response.number == 0)
                {
                    ViewBag.TipoMessage = string.Empty;
                    if (response.message == "Publicación realizada satisfactoriamente")
                    {
                        ViewBag.Message = "Mensaje eliminado";
                    }
                    else
                    {
                        ViewBag.Message = response.message;
                    }
                }
                else
                {
                    ViewBag.TipoMessage = string.Empty;
                    ViewBag.Message = response.message;
                }

                return View();
            }

            return View();
        }

        //public ActionResult SendchangesToLocalStorage()
        //{
        //    Service service = new Service();
        //    return true;
        //}
    }
}
