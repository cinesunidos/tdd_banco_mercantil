﻿using admin_usuarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin_usuarios.Controllers
{
    public class GuestAdminController : Controller
    {
        #region Get User
        [HttpGet]
        public User GetUser(string buscar, string platform)
        {
            string valor = "";
            if (platform == "Web")
            {
                valor = "Web.GetUserIdCard";
                if (buscar.Contains("@"))
                {
                    valor = "Web.GetUserEmail";
                }
            }
            else
            {
                valor = "Mobile.GetUserMobileIdCard";
                if (buscar.Contains("@"))
                {
                    valor = "Mobile.GetUserMobileEmail";
                }
            }
            Service services = new Service();
            User user = services.Execute<User>(valor, new { buscar });
            return (user);
        }

        #endregion

        #region Modify User
        [HttpGet()]
        public ActionResult GuestModify(string platform, string buscar, string buscar2, string cardtype, string type_search)
        {
            if (Session["User"] != null)
            {
                User user = new Models.User();
                Message message = new Message();
                user = null;
                ViewBag.danger = null;
                ViewBag.info = null;

                if (Convert.ToInt32(type_search) == 0)
                {
                    //Consultar
                    if ((platform == "Web") && (!string.IsNullOrEmpty(buscar2)))
                    {
                        user = GetUser(buscar2, "Web");
                    }
                    else if ((platform == "Mobile") && (!string.IsNullOrEmpty(buscar2)))
                    {
                        user = GetUser(buscar2, "mobile");
                    }
                    if ((platform == null && buscar2 == "") || (platform == null && buscar2 != null) || (platform != null && buscar2 == ""))
                    {
                        ViewBag.danger = "Debe seleccionar la plataforma y colocar cedula o correo del invitado en el campo de búsqueda";
                    }
                    if (platform != null && !string.IsNullOrEmpty(buscar2) && user == null)
                    {
                        ViewBag.info = string.Format("{0}{1}{2}{3}", "No se encontró el invitado ", platform, " - ", buscar2);
                    }
                }
                else
                {
                    if (Convert.ToInt32(type_search) == 1)
                    {
                        //Consultar
                        if ((platform == "Web") && (!string.IsNullOrEmpty(buscar)))
                        {
                            user = GetUser(cardtype+buscar, "Web");
                        }
                        else if ((platform == "Mobile") && (!string.IsNullOrEmpty(buscar)))
                        {
                            user = GetUser(cardtype+buscar, "mobile");
                        }
                        if ((platform == null && buscar == "") || (platform == null && buscar != null) || (platform != null && buscar == ""))
                        {
                            ViewBag.danger = "Debe seleccionar la plataforma y colocar cedula o correo del invitado en el campo de búsqueda";
                        }
                        if (platform != null && !string.IsNullOrEmpty(buscar) && user == null)
                        {
                            ViewBag.info = string.Format("{0}{1}{2}{3}", "No se encontró el invitado ", platform, " - ", buscar);
                        }
                    }
                }
                return View(user);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }            
        }

        [HttpPost()]
        public ActionResult GuestModify(string platform, string Email, string IdCard,
                                        string MassMailSubscription, string Active, string Password, string Id)
        {
            User user = new Models.User();
            Message message = new Message();
            user = null;
            message = null;
            ViewBag.message = null;
            //Modificar

            if (!string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(IdCard) && !string.IsNullOrEmpty(Password) &&
                !string.IsNullOrEmpty(platform) && !string.IsNullOrEmpty(Id))
            {
                if (string.IsNullOrEmpty(Active))
                {
                    Active = "false";
                }
                else
                {
                    Active = "true";
                }
                if (string.IsNullOrEmpty(MassMailSubscription))
                {
                    MassMailSubscription = "false";
                }
                else
                {
                    MassMailSubscription = "true";
                }

                if (platform == "Web")
                {
                    message = ModifyUser("Web", Email, IdCard, Active, MassMailSubscription, Password, Id);
                }
                else
                {
                    message = ModifyUser("Mobile", Email, IdCard, Active, MassMailSubscription, Password, Id);
                }
                if (message != null)
                {
                    ViewBag.message = message.message;

                }
            }
            return View(user);
        }


        #endregion
       
        #region Remove User
        [HttpGet]
        public ActionResult GuestDelete(string platform, string buscar, string buscar2, string cardtype, string type_search)
        {
            User user = new Models.User();
            Message message = new Message();
            user = null;
            ViewBag.danger = null;
            ViewBag.info = null;

            if (Convert.ToInt32(type_search) == 0)
            {
                //Consultar
                if ((platform == "Web") && (!string.IsNullOrEmpty(buscar2)))
                {
                    user = GetUser(buscar2, "Web");
                }
                else if ((platform == "Mobile") && (!string.IsNullOrEmpty(buscar2)))
                {
                    user = GetUser(buscar2, "mobile");
                }
                if ((platform == null && buscar2 == "") || (platform == null && buscar2 != null) || (platform != null && buscar2 == ""))
                {
                    ViewBag.danger = "Debe seleccionar la plataforma y colocar cedula o correo del invitado en el campo de búsqueda";
                }
                if (platform != null && !string.IsNullOrEmpty(buscar2) && user == null)
                {
                    ViewBag.info = string.Format("{0}{1}{2}{3}", "No se encontró el invitado ", platform, " - ", buscar2);
                }
            }
            else
            {
                if (Convert.ToInt32(type_search) == 1)
                {
                    //Consultar
                    if ((platform == "Web") && (!string.IsNullOrEmpty(buscar)))
                    {
                        user = GetUser(cardtype + buscar, "Web");
                    }
                    else if ((platform == "Mobile") && (!string.IsNullOrEmpty(buscar)))
                    {
                        user = GetUser(cardtype + buscar, "mobile");
                    }
                    if ((platform == null && buscar == "") || (platform == null && buscar != null) || (platform != null && buscar == ""))
                    {
                        ViewBag.danger = "Debe seleccionar la plataforma y colocar cedula o correo del invitado en el campo de búsqueda";
                    }
                    if (platform != null && !string.IsNullOrEmpty(buscar) && user == null)
                    {
                        ViewBag.info = string.Format("{0}{1}{2}{3}", "No se encontró el invitado ", platform, " - ", buscar);
                    }
                }
            }
            return View(user);
        }

        [HttpPost]
        public ActionResult GuestDelete(string platform, string Id, string Email)
        {
            User user = new Models.User();
            Message message = new Message();
            user = null;
            message = null;
            ViewBag.message = null;
            string valor = "";

            if (Session["User"] != null)
            {
                LoginUser LoginUser = new LoginUser();
                LoginUser = (LoginUser)Session["User"];

                if (!string.IsNullOrEmpty(platform) && !string.IsNullOrEmpty(Id) && !string.IsNullOrEmpty(Email))
                {
                    User _user = new User();
                    if (platform == "Web")
                    {
                        valor = "Web.RemoveUser";
                        _user.Id = Id;
                        _user.Email = Email;
                        //LRAMIREZ 24/10/2016, Incluir propiedad userAudit, el id del usuario logueado y Departamento
                        _user.userAudit = LoginUser.Id;
                        _user.Department = LoginUser.Department;
                    }
                    else
                    {
                        valor = "Mobile.RemoveUserMobile";
                        _user.Id = Id;
                        _user.Email = Email;
                        //LRAMIREZ 24/10/2016, Incluir propiedad userAudit, el id del usuario logueado y Departamento
                        _user.userAudit = LoginUser.Id;
                        _user.Department = LoginUser.Department;
                    }

                    Service services = new Service();
                    message = services.Execute<User, Message>(valor, _user);
                    //user.Email = null;
                }
            }
           
            if (message != null)
            {
                ViewBag.message = message.message;

            }
            return View(user);
        }
        #endregion

        #region Methods
        public Message ModifyUser(string platform, string Email, string IdCard, string Active,
                              string MassMailSubscription, string Password, string Id)
        {
            LoginUser LoginUser = new LoginUser();
            Message msg = new Message();

            if (Session["User"] != null)
            {
                LoginUser = (LoginUser)Session["User"];

                User user = new User();
                string valor = "";

                user.Id = Id;
                user.Email = Email;
                user.IdCard = IdCard;
                //LRAMIREZ 24/10/2016, Incluir propiedad userAudit, el id del usuario logueado
                user.userAudit = LoginUser.Id;
                user.Department = LoginUser.Department;
                if (MassMailSubscription == "true") user.MassMailSubcription = true; else user.MassMailSubcription = false;

                if (Active == "true") user.Active = true; else user.Active = false;
                user.Password = Password;
                user.Platform = platform;

                if (user.Platform == "Web")
                    valor = "Web.ModifyUser";
                else
                    valor = "Mobile.ModifyUserMobile";

                Service services = new Service();
                msg = services.Execute<User, Message>(valor, user);

            }

            return msg;
        }
        #endregion
        
    }
}