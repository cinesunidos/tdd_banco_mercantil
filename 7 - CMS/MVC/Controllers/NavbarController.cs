﻿using admin_usuarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace admin_usuarios.Controllers
{
    public class NavbarController : Controller
    {
        // GET: Navbar
        public ActionResult Navbar()
        {
            if (Session["User"] != null)
            {
                LoginUser user = new LoginUser();
                user = (LoginUser)Session["User"];

                return PartialView("Navbar", user);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [WebMethod]
        public string LoadPermissions()
        {
            List<Permissions> permList = new List<Permissions>();
            LoginUser user = new LoginUser();

            user = (LoginUser)Session["User"];
            foreach (var item in user.Permissions)
            {
                Permissions perm = new Permissions();
                perm.id = item.id;
                perm.Name = item.Name;

                permList.Add(perm);
            }

            return new JavaScriptSerializer().Serialize(new {permList});
        }
    }
}