﻿using admin_usuarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin_usuarios.Controllers
{
    public class BlacklistController : Controller
    {
        #region GetUserBlackListUser_Methods 
        //LRAMIREZ 13/10/2016
        //Método que busca el invitado en lista negra
        public BlackList GetBlackListUser(string buscar)
        //public User GetBlackListUser(string buscar)
        {
            BlackList BlackListUser = new BlackList();
            //User Usr = new User();
            string valor = "";
            if (buscar.Trim().Contains("@"))
            {
                valor = "Admin.GetBlackListUserByEmail";
                Service services = new Service();
                BlackListUser = services.Execute<BlackList>(valor, new { buscar });
                //Usr = GetGuest(BlackListUser.platform.Trim(), BlackListUser.Email.Trim());
            }
            else
            {
                valor = "Admin.GetBlackListUserByIdCard";
                Service services = new Service();
                BlackListUser = services.Execute<BlackList>(valor, new { buscar });
                //Usr = GetGuest(BlackListUser.platform.Trim(), BlackListUser.IdCard.Trim());
            }

            //return Usr;
            return BlackListUser;
        }

        //LRAMIREZ 13/10/2016
        //Obtener Listado de Invitados registrados en lista negra
        public List<BlackList> GetAllBlackListUser()
        //public List<User> GetAllBlackListUser()
        {
            List<BlackList> BlackList = new List<BlackList>();
            List<User> UserList = new List<User>();
            User Usr = new User();
            string valor = "";

            valor = "Admin.GetAllBlackListUsers";
            Service services = new Service();
            BlackList = services.Execute<List<BlackList>>(valor);

            //foreach (var BlackListUsr in BlackList)
            //{
            //    Usr = GetGuest(BlackListUsr.platform, BlackListUsr.Email);
            //}

            //UserList.Add(Usr);            

            return BlackList;

        }

        // LRAMIREZ 10/10/2016
        // Método que valida si un invitado si se encuentra en Lista Negra, dicho metodo lo hace al consultar el invitado 
        // en la plataforma seleccionada
        public Message ValidateBlackListUser(string platform, string buscar)
        {
            //string valor = "";
            User _User = new User();
            Message message = new Message();
            BlackList BlackListUser = new BlackList();
            BlackListUser = null;
            //_User = GetGuest(platform, buscar);

            //if (!string.IsNullOrEmpty(_User.Email) && (!string.IsNullOrEmpty(_User.IdCard))) // &&  (buscar.Contains("@")))
            if (!string.IsNullOrEmpty(buscar))
            {                
                BlackListUser = GetBlackListUser(buscar);                
              // _User = GetBlackListUser(buscar);
            }

            //if (!string.IsNullOrEmpty(BlackListUser.Id.ToString()))
            if (BlackListUser.Id.ToString() != "00000000-0000-0000-0000-000000000000")
            {
                message.message = "Este usuario ya se encuentra registrado en Lista Negra";
            }
            else
            {
                message.message = "";
            }
            return message;
        }


        #endregion
        #region GetBlackList
        // GET: Blacklist
        [HttpGet]
        public ActionResult BlacklistConsult(string buscar, string Button)
        {
            List<Models.User> UserList = new List<Models.User>();
            UserList = null;
            if (Session["User"] != null)
            {
                BlackList BlackListUser = new BlackList();
                User User = new Models.User();                
                User = null;
                BlackListUser = null;
                ViewBag.info = null;

                if (!string.IsNullOrEmpty(Button))
                {
                    UserList = new List<Models.User>();
                    if (!string.IsNullOrEmpty(buscar))
                    {
                        BlackListUser = GetBlackListUser(buscar);
                        //User = new Models.User();
                        //Buscar información del invitado en lista negra
                        if (BlackListUser.Email != null)
                        {
                            User = GetGuest(BlackListUser.platform.Trim(), BlackListUser.Email.Trim());

                            //Fill List
                            UserList.Add(User);
                        }
                        else
                        {
                            ViewBag.info = string.Format("{0}{1}", "No se encontró el invitado en Lista Negra ", buscar);
                            UserList = null;
                        }

                    }
                    else
                    {
                        List<BlackList> BlackList = new List<BlackList>();
                        //Llenar listado de invitados en lista negra
                        BlackList = GetAllBlackListUser();

                        if (BlackList != null)
                        {
                            //UserList = new List<Models.User>();
                            foreach (var BlackListUsr in BlackList)
                            {

                                //Buscar información del invitado en lista negra
                                User = GetGuest(BlackListUsr.platform, BlackListUsr.Email);
                                //Fill List
                                UserList.Add(User);
                            }
                        }
                        else
                        {
                            ViewBag.info = string.Format("No hay invitados registados en lista Negra ");
                            UserList = null;
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }                      
            return View(UserList);
        }

        public User GetGuest(string platform, string buscar)
        {
            User _User = new User();

            if (!string.IsNullOrEmpty(platform) && !string.IsNullOrEmpty(buscar))
            {
                GuestAdminController GuestAdmin = new GuestAdminController();

                _User = GuestAdmin.GetUser(buscar, platform);

            }
            return _User;
            
        }

        [HttpGet]
        public ActionResult BlacklistMove(string buscar, string platform)
        {
            User user = new Models.User();
            Message message = new Message();
            GuestAdminController GuestAdmin = new GuestAdminController();
            message = null;
            user = null;
            ViewBag.danger = null;
            ViewBag.info = null;

            if ((platform == "Web") && (!string.IsNullOrEmpty(buscar)))
            {
                message = ValidateBlackListUser(platform, buscar);
                if (string.IsNullOrEmpty(message.message))
                {
                    user = GetGuest("Web", buscar);
                }
            }

            if ((platform == "Mobile") && (!string.IsNullOrEmpty(buscar)))
            {
                message = ValidateBlackListUser(platform, buscar);
                if (string.IsNullOrEmpty(message.message))
                {
                    user = GetGuest("Mobile", buscar);
                }
            }

            if ((platform == null && buscar == "") || (platform == null && buscar != null) || (platform != null && buscar == ""))
            {
                ViewBag.danger = "Debe seleccionar la plataforma y colocar cedula o correo del invitado en el campo de búsqueda";
            }
            if (platform != null && !string.IsNullOrEmpty(buscar) && user == null && message == null)
            {
                ViewBag.info = string.Format("{0}{1}{2}{3}", "No se encontró el invitado ", platform, " - ", buscar);
            }
            if (platform != null && buscar != "" && message.message.Contains("Este usuario ya se encuentra registrado en Lista Negra"))
            {
                ViewBag.message = string.Format("{0} {1}{2}{3}", message.message, platform, " - ", buscar);
            }
            return View(user);

        }

        #endregion

        #region Move an user to BlackList

        [HttpPost]
        public ActionResult BlacklistMove(string IdCard, string Email, string Platform, string Id)
        {
            Message message = new Message();
            User user = new Models.User();
            string valor = "";
            user = null;
            message = null;
            ViewBag.message = null;

            if (Session["User"] != null)
            {
                LoginUser LoginUser = new LoginUser();
                LoginUser = (LoginUser)Session["User"];

                if (!string.IsNullOrEmpty(IdCard) && !string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(Platform) &&
               !string.IsNullOrEmpty(Id))
                {
                    User _user = new User();
                    valor = "Admin.CreateBlackList";
                    _user.Id = Id;
                    _user.IdCard = IdCard;
                    _user.Email = Email;
                    _user.Platform = Platform;
                    //LRAMIREZ 24/10/2016, Incluir propiedad userAudit, el id y Departamento del usuario logueado
                    _user.userAudit = LoginUser.Id;
                    _user.Department = LoginUser.Department;

                    Service services = new Service();
                    message = services.Execute<User, Message>(valor, _user);
                }
            }
           
            //Si creo el usuario en lista negra
            if (message != null && message.message.Contains("exitosamente"))
            {
                ViewBag.success = message.message;               
            }
            //Si existe alguna incidencia
            if (message != null && !message.message.Contains("exitosamente"))            
            {
                ViewBag.message = message.message;
            }

            return View(user);
        }
        #endregion


    }
}