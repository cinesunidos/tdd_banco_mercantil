﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;
using admin_usuarios.Controllers;

namespace admin_usuarios.Controllers
{
    public class AuditController : Controller
    {
        public List<LoginUser> GetLoginUser()
        {
            Service services = new Service();
            List<LoginUser> loginUser = services.Execute<List<LoginUser>>("LoginUser.Users");
            if (loginUser.Count() < 1) { loginUser = null; }
            return loginUser;
        }

        // GET: Audit
        [HttpGet]
        public ActionResult Audit()
        {
            if (Session["User"] != null)
            {
                Departments _department = new Departments();
                ViewBag.Department = _department.Department();
                ViewBag.UserId = GetLoginUser();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Audit(string DateBegin, string DateEnd, string UserId, string Department)
        {
            ViewBag.UserId = GetLoginUser();

            Departments _department = new Departments();
            ViewBag.Department = _department.Department();

            List<Audit> LstAudit = new List<Audit>();
            AuditParameters Parameters = new AuditParameters();
            Parameters.DateBegin = DateBegin;
            Parameters.DateEnd = DateEnd;
            Parameters.UserId = UserId;
            Parameters.Department = Department;

            LstAudit = GetListUsers(Parameters);

            if (LstAudit != null)
            {
                        ViewBag.Audit = LstAudit;
            }
            else
            {
                ViewBag.Message = "Información no encontrada";
            }
            
            return View();

        }


        public List<Audit> GetListUsers(AuditParameters AuditParameter)
        {
            
            Service services = new Service();
            List<Audit> ListAudit = services.Execute<AuditParameters, List<Audit>>("Admin.ListAudit", AuditParameter);
            List<Audit> ListAuditN = new List<Audit>();
            JsonResult json = new JsonResult();

            if (ListAudit != null)
            {
                //LRAMIREZ 01/11/2016, 
                //  - Create Controller variable to use a specific function "ValidateUser()", to get info user login
                //  - Create another ListAudit object, to clean and show data to the Audit view
               
                AccountController Account = new AccountController();
                foreach (var item in ListAudit)
                {
                    Audit _Audit = new Audit();
                    LoginUser User = new LoginUser();
                    User.Id = item.UserId;
                    User = Account.ValidateUser(User);

                    //Fill each properties in Audit object                    
                    _Audit.UserName = User.Name;
                    _Audit.dateTime = item.dateTime;
                    _Audit.Action = item.Action;
                    _Audit.Event = item.Event;
                    _Audit.Department = item.Department;

                    //Fill List Audit
                    ListAuditN.Add(_Audit);
                }                                
            }
            else
            {
                ListAuditN = null;
            }
            return ListAuditN;

        }

        //[HttpPost]
        //public JsonResult GetListUsers(AuditParameters AuditParameter)
        //{

        //    //Crear usuario
        //    Service services = new Service();
        //    List<Audit> ListAudit = services.Execute<AuditParameters, List<Audit>>("Audit.ListAudit", AuditParameter);            

        //    JsonResult json = new JsonResult();            

        //    if (ListAudit.Count > 0)
        //    {
        //        //LRAMIREZ 01/11/2016, 
        //        //  - Create Controller variable to use a specific function "ValidateUser()", to get info user login
        //        //  - Create another ListAudit object, to clean and show data to the Audit view
        //        List<Audit> ListAuditN = new List<Audit>();

        //        AccountController Account = new AccountController();
        //        foreach (var item in ListAudit)
        //        {
        //            Audit _Audit = new Audit();
        //            LoginUser User = new LoginUser();
        //            User.Id = item.UserId;
        //            User = Account.ValidateUser(User);

        //            //Fill each properties in Audit object                    
        //            _Audit.UserName = User.Name;
        //            _Audit.dateTime = item.dateTime;
        //            _Audit.Action = item.Action;
        //            _Audit.Event = item.Event;

        //            //Fill List Audit
        //            ListAuditN.Add(_Audit);
        //        }
        //        json.Data = "exito";
        //        ViewBag.Audit = ListAuditN;
        //    }

        //    return Json(ViewBag.Audit, JsonRequestBehavior.AllowGet);                                

        //}        
    }
}