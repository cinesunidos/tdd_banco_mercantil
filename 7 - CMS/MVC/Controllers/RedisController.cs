﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;
using System.Web.Script.Serialization;

namespace admin_usuarios.Controllers
{
    public class RedisController : Controller
    {
        // GET: Redis
      
        #region "Methods"
        public List<RedisKey> GetListRedisKey()
        {
            List<RedisKey> LstRedisKey = new List<RedisKey>();

            Service services = new Service();
            List<RedisKey> redis = services.Execute<List<RedisKey>>("RedisKey.GetListRedisKey");

            if (redis.Count() < 1) { redis = null; }

            return redis;

        }

        #endregion
        //[HttpPost]
        //public ActionResult redisCRUD()
        //{
        //    return View();
        //}

        #region "CRUD"
        public ActionResult redisCRUD()
        {
            if (Session["User"] != null)
            {
                return View(GetListRedisKey());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }            
        }        
        [HttpPost]
        public JsonResult AddKeyRedis(RedisKey redis)
        {
                Service services = new Service();
            Message msg = services.Execute<RedisKey, Message>("RedisKey.AddKeyRedis", redis);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            
            }            
            return (json);
        }
        [HttpPost]
        public JsonResult ModifyKeyRedis(RedisKey redis)
        {
            Service services = new Service();
            Message msg = services.Execute<RedisKey, Message>("RedisKey.ModifyKeyRedis", redis);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }
        [HttpPost]
        public JsonResult RemoveKeyRedis(RedisKey redis)
        {
            Service services = new Service();
            Message msg = services.Execute<RedisKey, Message>("RedisKey.RemoveKeyRedis", redis);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }

        #endregion

        #region "Panel de Operaciones"

        public ActionResult redisPanel()
        {
            if (Session["User"] != null)
            {
                ViewBag.Message = null;
                return View(GetListRedisKey());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }            
        }
       
        [HttpPost]
        public JsonResult ExecuteKeyRedis(RedisKey redis)
        {
            string Msg = "";
            List<Message> messages = new List<Message>();
            Service services = new Service();           
            messages = services.Execute<RedisKey, List<Message>>("RedisKey.ExecuteKeyRedis", redis);

            return Json(messages, JsonRequestBehavior.AllowGet);

            //JsonResult json = new JsonResult();

            //if ((message.message != null) || (message.message != ""))
            //{
            //    Msg = message.message;                
            //}
            //else
            //{
            //    Msg = "Imposible ejecutar llave del Redis";
            //}          
            //return new JavaScriptSerializer().Serialize(new { Msg });
        }

        #endregion

    }
}