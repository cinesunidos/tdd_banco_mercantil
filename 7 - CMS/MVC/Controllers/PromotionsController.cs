using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;
using System.Diagnostics;
using System.IO;
using System.Web.Hosting;
using Newtonsoft.Json;
using Newtonsoft;
using System.Xml.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Web.Routing;
using System.Web.Security;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Net.Http;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
//using Microsoft.Azure;

namespace admin_usuarios.Controllers
{
    public class PromotionsController : Controller
    {
        [HttpGet]
        public ActionResult Promotions()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        //public ActionResult CreatePromotions(HttpPostedFileBase fileUpload, string Titles, string Description, string StartDate, string ExpirationDate)
        public ActionResult CreatePromotions(DataPostPromotions promo)
        {
            Message msg = new Message();
            try
            {
                Promotions promotions = new Promotions();

                DateTime dt;


                if (promo.Image == null)
                {
                    throw new System.ArgumentException("Debe seleccionar un archivo para subir la imagen".Replace("�", @"\363"));
                }
                else
                {
                    if (!(promo.Image.FileName.EndsWith(".jpg") ||
                          promo.Image.FileName.EndsWith(".png") ||
                          promo.Image.FileName.EndsWith(".gif") ||
                          promo.Image.FileName.EndsWith(".jpeg")))
                    {
                        throw new System.ArgumentException("El formato de la imagen seleccionado no es valido. Los formatos de imagen permitidos son: jpg, jpeg, gif y png".Replace("�", @"\363"));
                    }
                    else
                    {
                        decimal siezekiloByte = promo.Image.ContentLength / 1024;
                        decimal siezemegaByte = promo.Image.ContentLength / 1048576;
                        if (siezekiloByte > 300)
                        {
                            if (siezekiloByte < 1024)
                            {
                                throw new System.ArgumentException("El peso m�ximo de una imagen es de 300 KB y la imagen cargada pesa ".Replace("�", @"\341") + siezekiloByte + " KB");
                            }
                            else
                            {
                                if (siezekiloByte >= 1024)
                                {
                                    throw new System.ArgumentException("El peso m�ximo de una imagen es de 300 KB y la imagen cargada pesa ".Replace("�", @"\341") + siezekiloByte + " MB");
                                }
                            }
                        }
                    }
                }

                if (promo.Title == "<br>")
                {
                    throw new System.ArgumentException("El campo t�tulo no puede estar vac�o".Replace("�", @"\355"));
                }

                if (promo.Description == "<br>")
                {
                    throw new System.ArgumentException("El campo descripci�n no puede estar vac�o".Replace("�", @"\355").Replace("�", @"\363"));
                }

                if (string.IsNullOrEmpty(promo.StartDate) || string.IsNullOrWhiteSpace(promo.StartDate))
                {
                    throw new System.ArgumentException("El campo fecha de creaci�n no puede estar vac�o".Replace("�", @"\355").Replace("�", @"\363"));
                }

                if (promo.StartDate.Length != 10)
                {
                    throw new System.ArgumentException("El campo fecha de creaci�n debe ser de 10 caracteres".Replace("�", @"\355").Replace("�", @"\363"));
                }

                if (!(DateTime.TryParseExact(Convert.ToDateTime(promo.StartDate).ToString("dd/MM/yyyy").Replace("-", "/").Replace(" ", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)))
                {
                    throw new System.ArgumentException("El formato de fecha del campo fecha de creaci�n no es el correcto ".Replace("�", @"\363"));
                }

                if (string.IsNullOrEmpty(promo.ExpirationDate) || string.IsNullOrWhiteSpace(promo.ExpirationDate))
                {
                    throw new System.ArgumentException("El campo fecha de vencimiento no puede estar vac�o".Replace("�", @"\355"));
                }

                if (promo.ExpirationDate.Length != 10)
                {
                    throw new System.ArgumentException("El campo fecha de vencimiento debe ser de 10 caracteres".Replace("�", @"\355"));
                }

                if (!(DateTime.TryParseExact(Convert.ToDateTime(promo.ExpirationDate).ToString("dd/MM/yyyy").Replace("-", "/").Replace(" ", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)))
                {
                    throw new System.ArgumentException("El formato de fecha del campo fecha de vencimiento no es el correcto".Replace("�", @"\363"));
                }

                if (Convert.ToDateTime(promo.StartDate) > Convert.ToDateTime(promo.ExpirationDate))
                {
                    throw new System.ArgumentException("La fecha de creaci�n debe ser menor que la fecha de vencimiento".Replace("�", @"\363"));
                }

                if (Convert.ToDateTime(promo.ExpirationDate) < Convert.ToDateTime(promo.StartDate))
                {
                    throw new System.ArgumentException("La fecha de vencimiento debe ser mayor que la fecha de creaci�n".Replace("�", @"\363"));
                }

                promotions.Title = promo.Title;
                promotions.Description = promo.Description;
                promotions.Image = ConfigurationManager.AppSettings["CDNUrl"] + promo.Image.FileName;
                promotions.StartDate = promo.StartDate;
                promotions.ExpirationDate = promo.ExpirationDate;
                promotions.Size = (promo.Image.ContentLength / 1024).ToString();

                Service services = new Service();
                msg = services.Execute<Promotions, Message>("Promotions.Create", promotions);

                if (msg.error == false)
                {
                    //se descomento apara probar
                    SaveImageToCloudStorage(promo.Image);
                    //List<Promotions> promot = services.Execute<List<Promotions>>("Promotions.GetPromotions").Where(promo => (!(Convert.ToDateTime(promo.StartDate) > DateTime.Now)) && (DateTime.Now < Convert.ToDateTime(promo.ExpirationDate))).OrderByDescending(promo => Convert.ToDateTime(promo.StartDate)).ToList();
                    List<Promotions> promot = services.Execute<List<Promotions>>("Promotions.GetPromotions");
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json_ = jsonSerialiser.Serialize(promot);
                    string promo_json = JsonConvert.SerializeObject(promot);
                    Message promopg = services.Execute<string, Message>("Promotions.ApplyChanges", promo_json);

                    if (promopg.error == true)
                    {
                        ViewData["Message"] = "Error: A ocurrido al guardar el durante el proceso de cache de las promociones" + msg.message;
                        return View("Promotions");
                    }
                }
                else
                {
                    ViewData["Message"] = "Error: A ocurrido al guardar los datos " + msg.message;
                    return View("Promotions");
                }
            }
            catch (Exception ex)
            {
                ViewData["Message"] = "Error: " + ex.Message.ToString();
                return View("Promotions");
            }

            ViewData["Message"] = "Los datos han sido guardados exitosamente";
            return View("Promotions");
        }

        private void SaveImageToCloudStorage(HttpPostedFileBase fileName)
        {
            string BlobPath = string.Empty;
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            //// Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            //// Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("mercadeo");
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName.FileName);
            using (var fileStream = fileName.InputStream)
            {
                blockBlob.UploadFromStream(fileStream);
            }
        }

        [HttpGet]
        public ActionResult EditPromotions()
        {
            if (Session["User"] != null)
            {
                return View(GetPromotions());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public List<DataPostPromotions> GetPromotions()
        {
            Service services = new Service();
            List<Promotions> promotions = services.Execute<List<Promotions>>("Promotions.GetPromotions");
            List<DataPostPromotions> lista = new List<DataPostPromotions>();
            if (promotions.Count() < 1)
            {
                promotions = null;
            }
            else
            {
                foreach (var item in promotions)
                {
                    DataPostPromotions data = new DataPostPromotions();
                    data.Id = item.Id;
                    data.Title = item.Title;
                    data.Description = item.Description;
                    data.StartDate = item.StartDate;
                    data.ExpirationDate = item.ExpirationDate;
                    data.Size = item.Image;
                    lista.Add(data);
                }
            }

            return lista;
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ActionPromotion(DataPostPromotions promot)
        {
            if (promot.Size.Contains("Update"))
            {
                ViewData["Message"] = UpdatePromotion(promot);
            }

            if (promot.Size == "Delete")
            {
                ViewData["Message"] = DeletePromotion(promot);
            }

            List<DataPostPromotions> promo = GetPromotions();
            return View("EditPromotions", promo);
        }

        public string UpdatePromotion(DataPostPromotions promo)
        {
            string message = string.Empty;
            HttpPostedFileBase fileUpload = promo.Image;
            try
            {
                Promotions promotions = new Promotions();
                DateTime dt;

                string count = promo.Size.Replace("Update", "");
                string Image = Request.Form["Image"+ count];

                if (string.IsNullOrEmpty(Image))
                {
                    throw new System.ArgumentException("Debe seleccionar un archivo para subir la imagen".Replace("�", @"\363"));
                }
                else
                {
                    if (!(Image.EndsWith(".jpg") ||
                         Image.EndsWith(".png") ||
                         Image.EndsWith(".gif") ||
                         Image.EndsWith(".jpeg")))
                    {
                        throw new System.ArgumentException("El formato de la imagen seleccionado no es valido. Los formatos de imagen permitidos son: jpg, jpeg, gif y png".Replace("�", @"\363"));
                    }
                }

                if (promo.Image != null)
                {
                    if (!(promo.Image.FileName.EndsWith(".jpg") ||
                          promo.Image.FileName.EndsWith(".png") ||
                          promo.Image.FileName.EndsWith(".gif") ||
                          promo.Image.FileName.EndsWith(".jpeg")))
                    {
                        throw new System.ArgumentException("El formato de la imagen seleccionado no es valido. Los formatos de imagen permitidos son: jpg, jpeg, gif y png".Replace("�", @"\363"));
                    }
                    else
                    {
                        decimal siezekiloByte = promo.Image.ContentLength / 1024;
                        decimal siezemegaByte = promo.Image.ContentLength / 1048576;
                        if (siezekiloByte > 300)
                        {
                            if (siezekiloByte < 1024)
                            {
                                throw new System.ArgumentException("El peso m�ximo de una imagen es de 300 KB y la imagen cargada pesa ".Replace("�", @"\341") + siezekiloByte + " KB");
                            }
                            else
                            {
                                if (siezekiloByte >= 1024)
                                {
                                    throw new System.ArgumentException("El peso m�ximo de una imagen es de 300 KB y la imagen cargada pesa ".Replace("�", @"\341") + siezekiloByte + " MB");
                                }
                            }
                        }
                    }
                }

                if (promo.Title == "<br>")
                {
                    throw new System.ArgumentException("El campo t�tulo no puede estar vac�o".Replace("�", @"\355"));
                }

                if (promo.Description == "<br>")
                {
                    throw new System.ArgumentException("El campo descripci�n no puede estar vac�o".Replace("�", @"\355").Replace("�", @"\363"));
                }

                if (string.IsNullOrEmpty(promo.StartDate) || string.IsNullOrWhiteSpace(promo.StartDate))
                {
                    throw new System.ArgumentException("El campo fecha de creaci�n no puede estar vac�o".Replace("�", @"\355").Replace("�", @"\363"));
                }

                if (promo.StartDate.Length != 10)
                {
                    throw new System.ArgumentException("El campo fecha de creaci�n debe ser de 10 caracteres".Replace("�", @"\355").Replace("�", @"\363"));
                }

                if (!(DateTime.TryParseExact(Convert.ToDateTime(promo.StartDate).ToString("dd/MM/yyyy").Replace("-", "/").Replace(" ", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)))
                {
                    throw new System.ArgumentException("El formato de fecha del campo fecha de creaci�n no es el correcto ".Replace("�", @"\363"));
                }

                if (string.IsNullOrEmpty(promo.ExpirationDate) || string.IsNullOrWhiteSpace(promo.ExpirationDate))
                {
                    throw new System.ArgumentException("El campo fecha de vencimiento no puede estar vac�o".Replace("�", @"\355"));
                }

                if (promo.ExpirationDate.Length != 10)
                {
                    throw new System.ArgumentException("El campo fecha de vencimiento debe ser de 10 caracteres".Replace("�", @"\355"));
                }

                if (!(DateTime.TryParseExact(Convert.ToDateTime(promo.ExpirationDate).ToString("dd/MM/yyyy").Replace("-", "/").Replace(" ", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)))
                {
                    throw new System.ArgumentException("El formato de fecha del campo fecha de vencimiento no es el correcto".Replace("�", @"\363"));
                }

                if (Convert.ToDateTime(promo.StartDate) > Convert.ToDateTime(promo.ExpirationDate))
                {
                    throw new System.ArgumentException("La fecha de creaci�n debe ser menor que la fecha de vencimiento".Replace("�", @"\363"));
                }

                if (Convert.ToDateTime(promo.ExpirationDate) < Convert.ToDateTime(promo.StartDate))
                {
                    throw new System.ArgumentException("La fecha de vencimiento debe ser mayor que la fecha de creaci�n".Replace("�", @"\363"));
                }

                promotions.Id = promo.Id;
                promotions.Title = promo.Title;
                promotions.Description = promo.Description;
                promotions.Image = ConfigurationManager.AppSettings["CDNUrl"] + Image;
                promotions.StartDate = promo.StartDate;
                promotions.ExpirationDate = promo.ExpirationDate;

                Service services = new Service();
                Message msg = services.Execute<Promotions, Message>("Promotions.Update", promotions);
                if (msg.error == false)
                {
                    //Si no se cargo ningun archivo en el input file no se agregara ninguna imagen en el blob de azure
                    if (promo.Image != null)
                    {
                        if (promo.Image.ContentLength > 0)
                        {
                            SaveImageToCloudStorage(fileUpload);
                        }
                    }
                    
                    //List<Promotions> promot = services.Execute<List<Promotions>>("Promotions.GetPromotions").Where(promo => (!(Convert.ToDateTime(promo.StartDate) > DateTime.Now)) && (DateTime.Now < Convert.ToDateTime(promo.ExpirationDate))).OrderByDescending(promo => Convert.ToDateTime(promo.StartDate)).ToList();
                    List<Promotions> promot = services.Execute<List<Promotions>>("Promotions.GetPromotions");
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json_ = jsonSerialiser.Serialize(promot);
                    string promo_json = JsonConvert.SerializeObject(promot);
                    Message promopg = services.Execute<string, Message>("Promotions.ApplyChanges", promo_json);
                }
            }
            catch (Exception ex)
            {
                message = "Error: " + ex.Message.ToString();
            }

            if (!(message.Contains("Error:")))
            {
                message = "Los datos han sido modificados exitosamente";
            }

            return message;
        }

        [HttpPost]
        public string DeletePromotion(DataPostPromotions promo)
        {
            string message = string.Empty;
            try
            {
                Promotions promotions = new Promotions();
                string Id = promo.Id.ToString();
                promotions.Id = promo.Id;
                if (string.IsNullOrEmpty(Id) || string.IsNullOrWhiteSpace(Id))
                {
                    throw new System.ArgumentException("El Id de la promoci�n es obligatorio para poder eliminar la promoci�n de la base de datos".Replace("�", @"\363"));
                }
                else
                {
                    Service services = new Service();
                    Message msg = services.Execute<Promotions, Message>("Promotions.Delete", promotions);

                    if (msg.error == false)
                    {
                        List<Promotions> promot = services.Execute<List<Promotions>>("Promotions.GetPromotions");
                        var jsonSerialiser = new JavaScriptSerializer();
                        var json_ = jsonSerialiser.Serialize(promot);
                        string promo_json = JsonConvert.SerializeObject(promot);
                        Message promopg = services.Execute<string, Message>("Promotions.ApplyChanges", promo_json);
                    }
                }
            }
            catch (Exception ex)
            {
                message = "Error: " + ex.Message.ToString();
            }

            if (!(message.Contains("Error:")))
            {
                message = "Los datos han sido Eliminados exitosamente";
            }

            return message;
        }
    }
}