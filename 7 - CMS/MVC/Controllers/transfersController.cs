﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;
using System.IO;
using System.Web.Hosting;
using Newtonsoft.Json;
using System.Configuration;

namespace admin_usuarios.Controllers
{
    public class transfersController : Controller
    {
        // GET: transfers
        public ActionResult Index()
        {
            TransferViewModel model = new TransferViewModel();
            Service service = new Service();
            model.ItemsForSelect = service.Execute<List<TransferItem>>("Concession.GetConcessionsItemsForCinema");
            model.ItemsForSelect.Add(new TransferItem
            {
                itemCode = "0",
                itemDescription = "Seleccione..",
            });
            return View(model);
        }

        [HttpPost]
        public ActionResult Transfer(TransferViewModel model, string item, string action, int quantity = 0)
        {
            if (action != "send" && (item == null || item == string.Empty || item == "0" || quantity <= 0))
            {
                ViewBag.mensaje = "Seleccione el item y la cantidad que desea trasladar";
                return View("Index", model);
            }
            switch (action)
            {
                case "add":
                    model.ItemsSelected = model.ItemsSelected.Where(m => m.Quitar == false).ToList();
                    var ItemToAdd = model.ItemsForSelect.Where(i => i.itemCode == item).SingleOrDefault();
                    var verify = model.ItemsSelected.Where(i => i.itemCode == item).SingleOrDefault();
                    if (verify != null)
                    {
                        int i = model.ItemsSelected.IndexOf(verify);
                        model.ItemsSelected[i].Quantity += quantity;
                    }
                    else
                    {
                        ItemToAdd.Quantity = quantity;
                        model.ItemsSelected.Add(ItemToAdd);
                    }
                    break;

                case "send":
                    model.ItemsSelected = model.ItemsSelected.Where(m => m.Quitar == false).ToList();
                    Service ser = new Service();
                    List<ItemAndComponets> items = ser.Execute<List<TransferItem>, List<ItemAndComponets>>("Concession.VerifyItemsForTranslate", model.ItemsSelected);
                    return View("TransferConfirmation", items);

                default:
                    break;
            }
            ModelState.Clear();
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult TransferResume(List<ItemAndComponets> model)
        {
            //guardar la lista de items a tranferir en un archivo txt para luego de hacer el traslado en vista, actualizar la tabla de la nube
            string ruta = HostingEnvironment.MapPath("~/App_Data/traslado.txt");
            var itemsPrincipales = model.Select(m => m.PrincipalItem).ToList();
            using (StreamWriter wr = new StreamWriter(ruta, false))
            {
                wr.WriteLine(JsonConvert.SerializeObject(itemsPrincipales));
                wr.Close();
            }
            //validar cantidades totales de la transaccin
            List<ItemComponent> lista = new List<ItemComponent>();
            foreach (var item in model)
            {
                foreach (var item2 in item.ItemComponents)
                {
                    //se sustituye la cantidad original por la cantidad requerida para saber la cantidad real que se desea trasladar
                    if (item.PrincipalItem.itemType == null || item.PrincipalItem.itemType == string.Empty)
                    {
                        lista.Add(item2);
                    }
                    else
                    {
                        item2.ComponentOriginalQty = item2.ComponentOriginalQty * (decimal)item.PrincipalItem.Quantity;
                        lista.Add(item2);
                    }
                }
            }

            //agrupo los items de la lista resultasnte para evitar hacer varios movimimentos del mismo item
            //agrupo por el master id para segurarme de que no haya problemas en la insersion en vista.
            lista = lista.GroupBy(l => l.MasterID).Select(y => new ItemComponent
            {
                ComponentDescription = y.First().ComponentDescription,
                ComponentId = y.First().ComponentId,
                ComponentStock = y.First().ComponentStock,
                ComponentOriginalQty = y.Sum(q => q.ComponentOriginalQty),
                MasterID = y.First().MasterID,
                UOM = y.First().UOM
            }).ToList();
            ViewBag.option = "1";
            return View(lista);
        }

        public ActionResult VerifyCandies()
        {
            string cinemaId = ConfigurationManager.AppSettings["ModalCinemaCode"].ToString();
            Service ser = new Service();
            var url = System.Configuration.ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString()
                   + System.Configuration.ConfigurationManager.AppSettings["Concession.GetConcessionsItemsInCinema"].ToString();
            var datos = ser.Execute<string, List<ItemAndComponets>>("Concession.GetConcessionsItemsInCinema", cinemaId, url);
            foreach (var item in datos)
            {
                foreach (var i in item.ItemComponents)
                {
                    if (item.PrincipalItem.itemType == "S")
                    {
                        i.ComponentOriginalQty *= item.PrincipalItem.Quantity;
                    }
                }
            }
            ModelState.Clear();
            datos.ToList();
            return View(datos);
        }

        public ActionResult TransferConfirm(List<ItemComponent> components)
        {
            Service ser = new Service();
            var resultado = ser.Execute<List<ItemComponent>, List<ItemComponent>>("Concession.ConfirmItemsForTranslate", components);
            int itemsTransferidos = resultado.Where(r => r.ComponentId.Contains("OK")).Count();
            if (itemsTransferidos != components.Count())
            {
                //resultado = resultado.Where(r => r.ComponentDescription.Contains("Ok")).ToList();
                resultado = ser.Execute<List<ItemComponent>, List<ItemComponent>>("Concession.ReverseTranslate", resultado);
                ViewBag.mensaje = "Hubo un problema al Trasladar los items en vista. el traslado de inventario fue reversado";
                ViewBag.option = "2";
                return View("TransferResume", components);
            }
            //una vez hechos los traslados en vista
            //invocar servicio para actualizar la tabla de la nube a traves de los servicios de la pagina web
            string ruta = HostingEnvironment.MapPath("~/App_Data/traslado.txt");
            List<TransferItem> items = new List<TransferItem>();
            using (StreamReader r = new StreamReader(ruta))
            {
                items = JsonConvert.DeserializeObject<List<TransferItem>>(r.ReadLine());
                r.Close();
            }
            var url = System.Configuration.ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString()
                   + System.Configuration.ConfigurationManager.AppSettings["Concession.UpdateCandiesTable"].ToString();
            PackViewModel pvm = new PackViewModel
            {
                cinemaId = ConfigurationManager.AppSettings["ModalCinemaCode"].ToString(),
                items = items
            };
            TransferPack respuesta = ser.Execute<PackViewModel, TransferPack>("Concession.UpdateCandiesTable", pvm, url);

            //validar si se actualizó o no la tabal de azure; de ser negativa la actualizacion hago reverso del traslado en vista.
            //Concession.ReverseTranslate
            if (respuesta == null || respuesta.Token != "00")
            {
                resultado = ser.Execute<List<ItemComponent>, List<ItemComponent>>("Concession.ReverseTranslate", components);
                ViewBag.mensaje = "Hubo un problema al actualizar la tabla de azure. el traslado de inventario fue reversado";
            }

            //retornar la vista
            ViewBag.option = "2";
            foreach (var item in resultado)
            {
                item.ComponentStock -= item.ComponentOriginalQty;
            }
            return View("TransferResume", resultado);
        }

        /// <summary>
        /// obtener las concesiones disponibles en la web para luego decidir cuales se van a retornar al almacen de predespacho,
        /// </summary>GetAviableConcessionsItemsInCinema
        /// <returns></returns>
        public ActionResult WebToPre()
        {
            string cinemaId = ConfigurationManager.AppSettings["ModalCinemaCode"].ToString();
            Service ser = new Service();
            var url = System.Configuration.ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString()
                   + System.Configuration.ConfigurationManager.AppSettings["Concession.GetAviableConcessionsItemsInCinema"].ToString();
            var datos = ser.Execute<string, List<TransferItem>>("Concession.GetAviableConcessionsItemsInCinema", cinemaId, url);
            List<ReverseViewModel> model = new List<ReverseViewModel>();
            foreach (var item in datos)
            {
                model.Add((ReverseViewModel)item);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult VerifyWebToPre(List<ReverseViewModel> model)
        {
            var m2 = model.Where(m => m.Quantity > 0).ToList();
            if (m2.Count > 0)
            {
                List<TransferItem> lista = new List<TransferItem>();
                foreach (var item in m2)
                {
                    lista.Add((TransferItem)item);
                }
                Service ser = new Service();
                List<ItemAndComponets> items = ser.Execute<List<TransferItem>, List<ItemAndComponets>>("Concession.VerifyItemsForReverse", lista);
                return View("TransferConfirmation2", items);
            }
            else
            {
                ViewBag.error = "Por favor indique los productos y las cantidades que desea trasladar";
                return View("WebToPre", model);
            }
        }

        public ActionResult ResumeReverse(List<ItemAndComponets> model)
        {
            //guardar la lista de items a tranferir en un archivo txt para luego de hacer el traslado en vista, actualizar la tabla de la nube
            string ruta = HostingEnvironment.MapPath("~/App_Data/reverso.txt");
            var itemsPrincipales = model.Select(m => m.PrincipalItem).ToList();
            using (StreamWriter wr = new StreamWriter(ruta, false))
            {
                wr.WriteLine(JsonConvert.SerializeObject(itemsPrincipales));
                wr.Close();
            }
            //validar cantidades totales de la transaccin
            List<ItemComponent> lista = new List<ItemComponent>();
            foreach (var item in model)
            {
                foreach (var item2 in item.ItemComponents)
                {
                    //se sustituye la cantidad original por la cantidad requerida para saber la cantidad real que se desea trasladar
                    if (item.PrincipalItem.itemType == null || item.PrincipalItem.itemType == string.Empty)
                    {
                        lista.Add(item2);
                    }
                    else
                    {
                        item2.ComponentOriginalQty = item2.ComponentOriginalQty * (decimal)item.PrincipalItem.Quantity;
                        lista.Add(item2);
                    }
                }
            }

            //agrupo los items de la lista resultasnte para evitar hacer varios movimimentos del mismo item
            //agrupo por el master id para segurarme de que no jaya problemas en la insersion en vista.
            lista = lista.GroupBy(l => l.MasterID).Select(y => new ItemComponent
            {
                ComponentDescription = y.First().ComponentDescription,
                ComponentId = y.First().ComponentId,
                ComponentStock = y.First().ComponentStock,
                ComponentOriginalQty = y.Sum(q => q.ComponentOriginalQty),
                MasterID = y.First().MasterID,
                UOM = y.First().UOM
            }).ToList();
            ViewBag.option = "1";
            return View(lista);
        }

        public ActionResult ReverseConfirm(List<ItemComponent> items)
        {
            Service ser = new Service();
            var resultado = ser.Execute<List<ItemComponent>, List<ItemComponent>>("Concession.ReverseTranslate", items);
            string ruta = HostingEnvironment.MapPath("~/App_Data/reverso.txt");
            List<TransferItem> itemsweb = new List<TransferItem>();
            using (StreamReader r = new StreamReader(ruta))
            {
                itemsweb = JsonConvert.DeserializeObject<List<TransferItem>>(r.ReadLine());
                r.Close();
            }
            var url = System.Configuration.ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString()
                   + System.Configuration.ConfigurationManager.AppSettings["Concession.DecreaseCandiesTable"].ToString();
            PackViewModel pvm = new PackViewModel
            {
                cinemaId = ConfigurationManager.AppSettings["ModalCinemaCode"].ToString(),
                items = itemsweb
            };
            TransferPack respuesta = ser.Execute<PackViewModel, TransferPack>("Concession.DecreaseCandiesTable", pvm, url);

            //validar si se actualizó o no la tabal de azure; de ser negativa la actualizacion hago reverso del traslado en vista.
            //Concession.ReverseTranslate
            //if (respuesta == null || respuesta.Token != "00")
            //{
            //    resultado = ser.Execute<List<ItemComponent>, List<ItemComponent>>("Concession.ReverseTranslate", components);
            //    ViewBag.mensaje = "Hubo un problema al actualizar la tabla de azure. el traslado de inventario fue reversado";
            //}

            //retornar la vista
            foreach (var item in resultado)
            {
                item.ComponentStock -= item.ComponentOriginalQty;
            }
            ViewBag.option = "2";
            return View("TransferResume", resultado);
        }

        public ActionResult SelectTheater()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public ActionResult VerifyCandiesByTheater(string CinemaId)
        {
            if (Session["User"] != null)
            {
                ViewBag.CineOfConsulting = CinemaId.Substring(0, 3);
                Service service = new Service();
                string url = ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString() + ConfigurationManager.AppSettings["Concession.GetAllConcecion"].ToString();
                List<TransferItem> transferItems = service.Execute<string, List<TransferItem>>("Concession.GetAllConcecion", CinemaId, url);
                if (transferItems == null)
                {
                    return View("SelectTheater");
                }
                else
                {
                    return View(transferItems);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

    }
}