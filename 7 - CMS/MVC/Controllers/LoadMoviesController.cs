using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using StackExchange.Redis;
using System.IO;
using System.Text;
using System.Net;
using System.Web.Helpers;
using System.Configuration;
using System.Drawing.Printing;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Hosting;
using System.Xml;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace admin_usuarios.Controllers
{
    public class LoadMoviesController : Controller
    {
        [HttpGet]
        public ActionResult LoadMovies()
        {
            if (Session["User"] != null)
            {
                return View(GetLoadMovies());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public List<LoadMovies> GetLoadMovies()
        {
            List<LoadMovies> LoadMovies = new List<LoadMovies>();
            Service services = new Service();
            LoadMovies = services.Execute<List<LoadMovies>>("LoadMovies.GetLoadMovies");
            if (LoadMovies.Count() < 1)
            {
                LoadMovies = null;
            }
            return LoadMovies;
        }

        [HttpPost]
        public ActionResult AddLoadMovies(HttpPostedFileBase fileOne, string slOne, HttpPostedFileBase fileTwo, string slTwo, HttpPostedFileBase fileThree, string slThree, HttpPostedFileBase fileFour, string slFour, HttpPostedFileBase fileFive, string slFive, HttpPostedFileBase fileSix, string slSix, HttpPostedFileBase fileSeven, string slSeven, HttpPostedFileBase fileEight, string slEight)
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(System.Configuration.ConfigurationManager.AppSettings["RedisConnection"].ToString());
            try
            {
                IDatabase db = redis.GetDatabase();

                string outputStringData = db.StringGet("BlackBerry_VItem");
                if (String.IsNullOrEmpty(outputStringData))
                {
                    outputStringData = "[{\"ID\":\"22CE8163-D91B-4DDC-BF54-0D9271E179B2\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"0FC7404B-604B-47F8-B932-27ADD61F0DC5\",\"SectionName\":\"Section1\"},{\"ID\":\"F838EA3A-AE18-4E10-AC31-20A506DE5150\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"D0825B2D-F20D-4BCD-91FE-397D616A5C51\",\"SectionName\":\"Section1\"},{\"ID\":\"DC720085-144F-400C-ADA2-3DDDF3C3D896\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"414CC586-1DBE-4953-94B1-9BA89B4F0810\",\"SectionName\":\"Section1\"},{\"ID\":\"B23C5EAA-A9F6-4F41-B548-5FB1BB9C2A6E\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"C8B0891F-5E42-4560-A173-A1DD31B9C7C8\",\"SectionName\":\"Section2\"},{\"ID\":\"52505AC9-ECC7-4D3C-B770-71B92C151FEB\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"3EB920C4-8DB2-41BA-8D8E-E2E205287FA9\",\"SectionName\":\"Section3\"},{\"ID\":\"3A94E4DE-3D1F-4206-B87B-C6C828392CF1\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"25CED96F-824B-466A-B7FB-E456A0E97B49\",\"SectionName\":\"Section4\"},{\"ID\":\"8426F800-637A-4965-BE22-CCBEAB766CE0\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"60B509F1-D615-4879-8B24-80BDDA3ADE86\",\"SectionName\":\"Section5\"},{\"ID\":\"DA2C1C72-F7FF-4B2E-972A-DDAE48ACACCE\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"34AE715B-B4F6-4EA8-BEB7-44989A610D93\",\"SectionName\":\"Section6\"}]";
                    db.StringSet("BlackBerry_VItem", outputStringData);
                }

                List<BlackBerry_VItem> BlackBerry_VItem = new List<BlackBerry_VItem>();
                var json1 = JsonConvert.DeserializeObject(outputStringData);
                var jsonObj = JsonConvert.DeserializeObject<List<BlackBerry_VItem>>(json1.ToString());

                string ho1 = null;
                string title1 = null;
                string error = null;

                if ((fileOne != null) && (slOne != "-1"))
                {
                    ho1 = slOne;
                    title1 = fileOne.FileName.Replace(".jpg", "");

                    if (!(fileOne.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileOne.FileName + ", cargada en el primer campo de la secci�n Slider no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileOne.FileName.StartsWith("Slider_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileOne.FileName + ", cargada en el primer campo de la secci�n Slider debe comenzar con Slider_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileOne);
                }

                string ho2 = null;
                string title2 = null;
                if ((fileTwo != null) && (slTwo != "-1"))
                {
                    ho2 = slTwo;
                    title2 = fileTwo.FileName.Replace(".jpg", "");

                    if (!(fileTwo.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileTwo.FileName + ", cargada en el segundo campo de la secci�n Slider no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileTwo.FileName.StartsWith("Slider_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileTwo.FileName + ", cargada en el segundo campo de la secci�n Slider debe comenzar con Slider_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileTwo);
                }

                string ho3 = null;
                string title3 = null;
                if ((fileThree != null) && (slThree != "-1"))
                {
                    ho3 = slThree;
                    title3 = fileThree.FileName.Replace(".jpg", "");

                    if (!(fileThree.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileThree.FileName + ", cargada en el tercer campo de la secci�n Slider no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileThree.FileName.StartsWith("Slider_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileThree.FileName + ", cargada en el tercer campo de la secci�n Slider debe comenzar con Slider_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileThree);
                }

                string ho4 = null;
                string title4 = null;
                if ((fileFour != null) && (slFour != "-1"))
                {
                    ho4 = slFour;
                    title4 = fileFour.FileName.Replace(".jpg", "");

                    if (!(fileFour.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileFour.FileName + ", cargada en el primer campo de la secci�n Centro no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileFour.FileName.StartsWith("Centro_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileFour.FileName + ", cargada en el primer campo de la secci�n Centro debe comenzar con Centro_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileFour);
                }

                string ho5 = null;
                string title5 = null;
                if ((fileFive != null) && (slFive != "-1"))
                {
                    ho5 = slFive;
                    title5 = fileFive.FileName.Replace(".jpg", "");

                    if (!(fileFive.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileFive.FileName + ", cargada en el segundo campo de la secci�n Centro no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileFive.FileName.StartsWith("Centro_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileFive.FileName + ", cargada en el segundo campo de la secci�n Centro debe comenzar con Centro_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileFive);
                }

                string ho6 = null;
                string title6 = null;
                if ((fileSix != null) && (slSix != "-1"))
                {
                    ho6 = slSix;
                    title6 = fileSix.FileName.Replace(".jpg", "");

                    if (!(fileSix.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileSix.FileName + ", cargada en el tercer campo de la secci�n Centro no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileSix.FileName.StartsWith("Centro_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileSix.FileName + ", cargada en el tercer campo de la secci�n Centro debe comenzar con Centro_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileSix);
                }

                string ho7 = null;
                string title7 = null;
                if ((fileSeven != null) && (slSeven != "-1"))
                {
                    ho7 = slSeven;
                    title7 = fileSeven.FileName.Replace(".jpg", "");

                    if (!(fileSeven.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileSeven.FileName + ", cargada en el cuarto campo de la secci�n Centro no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileSeven.FileName.StartsWith("Centro_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileSeven.FileName + ", cargada en el cuarto campo de la secci�n Centro debe comenzar con Centro_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileSeven);
                }

                string ho8 = null;
                string title8 = null;
                if ((fileEight != null) && (slEight != "-1"))
                {
                    ho8 = slEight;
                    title8 = fileEight.FileName.Replace(".jpg", "");

                    if (!(fileEight.FileName.EndsWith(".jpg")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileEight.FileName + ", cargada en el campo de la secci�n Destacada no es una imagen con extensi�n .jpg".Replace("�", @"\363"));
                    }

                    if (!(fileEight.FileName.StartsWith("Destacada_")))
                    {
                        throw new System.ArgumentException("La imagen llamada " + fileEight.FileName + ", cargada en el campo de la secci�n Destacada debe comenzar con Destacada_".Replace("�", @"\363"));
                    }

                    SaveImageToCloudStorage(fileEight);
                }

                ViewData["Message"] = null;

                foreach (var obj in jsonObj)
                {
                    string imageID = "";
                    string movieID = "";

                    if (obj.ID == "22CE8163-D91B-4DDC-BF54-0D9271E179B2")
                    {
                        if (ho1 != null || title1 != null)
                        {
                            movieID = ho1;
                            imageID = title1;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    if (obj.ID == "F838EA3A-AE18-4E10-AC31-20A506DE5150")
                    {
                        if (ho2 != null || title2 != null)
                        {
                            movieID = ho2;
                            imageID = title2;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    if (obj.ID == "DC720085-144F-400C-ADA2-3DDDF3C3D896")
                    {
                        if (ho3 != null || title3 != null)
                        {
                            movieID = ho3;
                            imageID = title3;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    if (obj.ID == "B23C5EAA-A9F6-4F41-B548-5FB1BB9C2A6E")
                    {
                        if (ho4 != null || title4 != null)
                        {
                            movieID = ho4;
                            imageID = title4;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    if (obj.ID == "52505AC9-ECC7-4D3C-B770-71B92C151FEB")
                    {
                        if (ho5 != null || title5 != null)
                        {
                            movieID = ho5;
                            imageID = title5;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    if (obj.ID == "3A94E4DE-3D1F-4206-B87B-C6C828392CF1")
                    {
                        if (ho6 != null || title6 != null)
                        {
                            movieID = ho6;
                            imageID = title6;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    if (obj.ID == "8426F800-637A-4965-BE22-CCBEAB766CE0")
                    {
                        if (ho7 != null || title7 != null)
                        {
                            movieID = ho7;
                            imageID = title7;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    if (obj.ID == "DA2C1C72-F7FF-4B2E-972A-DDAE48ACACCE")
                    {
                        if (ho8 != null || title8 != null)
                        {
                            movieID = ho8;
                            imageID = title8;
                        }
                        else
                        {
                            movieID = obj.MovieID;
                            imageID = obj.ImageID;
                        }
                    }

                    BlackBerry_VItem.Add(new BlackBerry_VItem
                    {
                        ID = obj.ID,
                        Type = obj.Type,
                        ImageID = imageID,
                        MovieID = movieID,
                        Date = obj.Date,
                        Url = obj.Url,
                        SectionID = obj.SectionID,
                        SectionName = obj.SectionName
                    });
                }

                var json2 = JsonConvert.SerializeObject(BlackBerry_VItem);

                db.StringSet("BlackBerry_VItem", json2);
                redis.Close();

                Service services = new Service();
                Message msg = services.Execute<string, Message>("LoadMovies.FileGetRediCache", json2);

                List<LoadMovies> Lmovie = GetLoadMovies();

                ViewData["Message"] = "Los datos seleccionados han sido guardados";
                return View("LoadMovies", Lmovie);
            }
            catch (Exception ex)
            {
                if (redis.IsConnected)
                {
                    redis.Close();
                }
                ViewData["Message"] = "Error: " + ex.Message.ToString();
                List<LoadMovies> Lmovies = GetLoadMovies();
                return View("LoadMovies", Lmovies);
            }
        }

        [HttpGet]
        public ActionResult ListMovies()
        {
            if (Session["User"] != null)
            {
                ViewData["Message"] = string.Empty;
                string outputStringData = string.Empty;
                List<BlackBerry_VItem> blackBerry_VItem = new List<BlackBerry_VItem>();
                ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(System.Configuration.ConfigurationManager.AppSettings["RedisConnection"].ToString());
                try
                {
                    IDatabase db = redis.GetDatabase();
                    outputStringData = db.StringGet("BlackBerry_VItem");
                    if (String.IsNullOrEmpty(outputStringData))
                    {
                        outputStringData = "[{\"ID\":\"22CE8163-D91B-4DDC-BF54-0D9271E179B2\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"0FC7404B-604B-47F8-B932-27ADD61F0DC5\",\"SectionName\":\"Section1\"},{\"ID\":\"F838EA3A-AE18-4E10-AC31-20A506DE5150\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"D0825B2D-F20D-4BCD-91FE-397D616A5C51\",\"SectionName\":\"Section1\"},{\"ID\":\"DC720085-144F-400C-ADA2-3DDDF3C3D896\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"414CC586-1DBE-4953-94B1-9BA89B4F0810\",\"SectionName\":\"Section1\"},{\"ID\":\"B23C5EAA-A9F6-4F41-B548-5FB1BB9C2A6E\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"C8B0891F-5E42-4560-A173-A1DD31B9C7C8\",\"SectionName\":\"Section2\"},{\"ID\":\"52505AC9-ECC7-4D3C-B770-71B92C151FEB\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"3EB920C4-8DB2-41BA-8D8E-E2E205287FA9\",\"SectionName\":\"Section3\"},{\"ID\":\"3A94E4DE-3D1F-4206-B87B-C6C828392CF1\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"25CED96F-824B-466A-B7FB-E456A0E97B49\",\"SectionName\":\"Section4\"},{\"ID\":\"8426F800-637A-4965-BE22-CCBEAB766CE0\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"60B509F1-D615-4879-8B24-80BDDA3ADE86\",\"SectionName\":\"Section5\"},{\"ID\":\"DA2C1C72-F7FF-4B2E-972A-DDAE48ACACCE\",\"Type\":\"Billboard\",\"ImageID\":\"\",\"MovieID\":\"\",\"Date\":\"\",\"Url\":\"\",\"SectionID\":\"34AE715B-B4F6-4EA8-BEB7-44989A610D93\",\"SectionName\":\"Section6\"}]";
                    }
                    redis.Close();
                    blackBerry_VItem = JsonConvert.DeserializeObject<List<BlackBerry_VItem>>(outputStringData);

                }
                catch (Exception ex)
                {
                    if (redis.IsConnected)
                    {
                        redis.Close();
                    }

                    blackBerry_VItem = null;
                    ViewData["Message"] = ex.Message;
                }
                return View(blackBerry_VItem);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        private void SaveImageToCloudStorage(HttpPostedFileBase fileName)
        {
            string BlobPath = string.Empty;
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            //// Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            //// Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(@"poster/moviles");
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName.FileName);
            using (var fileStream = fileName.InputStream)
            {
                blockBlob.UploadFromStream(fileStream);
            }
        }
    }
}
