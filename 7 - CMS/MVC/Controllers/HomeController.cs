﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;

namespace admin_usuarios.Controllers
{
    public class HomeController : Controller
    {
        public LoginUser Login { get; set; }
        public HomeController()
        {        
               
        }

        public ActionResult Index(Models.LoginUser LoginUser)
        {
            if (Session["User"] != null)
            {
                LoginUser = (LoginUser)Session["User"];
                ViewBag.LoginUser = Session["User"];
                return View(LoginUser);
            }
            else
            {                
                return RedirectToAction("Login", "Account");
            }      
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}