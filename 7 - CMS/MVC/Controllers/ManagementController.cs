﻿using admin_usuarios.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace admin_usuarios.Controllers
{
    public class ManagementController : Controller
    {

        // GET: Management
        #region Metodos de la vista Permissions
        public ActionResult Permissions()
        {
            if (Session["User"] != null)
            {
                return View(GetPermissions());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
        public List<Permissions> GetPermissions()
        {
            Service services = new Service();
            List<Permissions> perm = services.Execute<List<Permissions>>("Management.GetPermissions");
            if (perm.Count() < 1) { perm = null; }

            return perm;
        }
        [HttpPost()]
        public JsonResult CreatePermission(Permissions perm)
        {
            Service services = new Service();
            Message msg = services.Execute<Permissions, Message>("Management.CreatePermissions", perm);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }
        [HttpPost()]
        public JsonResult RemovePermissions(Permissions perm)
        {
            Service services = new Service();
            Message msg = services.Execute<Permissions, Message>("Management.RemovePermissions", perm);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }

        [HttpPost()]
        public JsonResult ModifyPermissions(Permissions perm)
        {
            Service services = new Service();
            Message msg = services.Execute<Permissions, Message>("Management.ModifyPermissions", perm);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }
        #endregion

        #region Metodos de la vista Roles
        public ActionResult Role()
        {
            if (Session["User"] != null)
            {
                return View(GetRole());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        public List<Role> GetRole()
        {
            Service services = new Service();
            List<Role> roles = services.Execute<List<Role>>("Management.GetRole");
            if (roles.Count() < 1) { roles = null; }

            return roles;
        }
        [WebMethod]
        public string GetPermissionAll(Role role)
        {
            List<Permissions> response = new List<Permissions>();
            Service services = new Service();
            List<Permissions> permAll = (services.Execute<List<Permissions>>("Management.GetPermissions")).OrderBy(x => x.Name).ToList();

            var a = (GetPermissionRole(role));
            if (a.Length > 20)
            {
                var b = a.Remove(0, 12);
                var c = b.Replace("}]}", "}]");
                List<Permissions> permRole = new JavaScriptSerializer().Deserialize<List<Permissions>>(c);
                if (permRole.Count() > 0)
                {
                    var query = (from o in permAll
                                 where !(from p in permRole
                                         select p.id).Contains(o.id)
                                 select o).OrderBy(x => x.Name).ToList();

                    if (query.Count() > 0)
                    {
                        foreach (var item in query)
                        {
                            Permissions perm = new Permissions();
                            perm.id = item.id;
                            perm.Name = item.Name;

                            response.Add(perm);
                        }
                    }
                    else
                    {
                        response = null;
                    }
                }
            }
            else
            {
                response = permAll;
            }
            return new JavaScriptSerializer().Serialize(new { response });
        }

        [WebMethod]
        public string GetPermissionRole(Role role)
        {
            List<Permissions> response = null;
            Service services = new Service();
            List<Permissions> permissions = services.Execute<Role, List<Permissions>>("Management.GetPermissionsByRole", role);
            if (permissions != null)
            {
                response = permissions.OrderBy(x => x.Name).ToList();
            }
            return new JavaScriptSerializer().Serialize(new { response });
        }

        [WebMethod]
        public string AddPermission(string[] permissionID)
        {
            Service services = new Service();
            Message msg = services.Execute<string[], Message>("Management.AddPermissionByRole", permissionID);
            
            return new JavaScriptSerializer().Serialize(new { msg });
        }

        [WebMethod]
        public string DeletePermission(string[] permissionID)
        {
            Service services = new Service();
            Message msg = services.Execute<string[], Message>("Management.DeletePermissionByRole", permissionID);
           
            return new JavaScriptSerializer().Serialize(new { msg });
        }

        [HttpPost()]
        public JsonResult Update(Role role)
        {
            Service services = new Service();
            Message msg = services.Execute<Role, Message>("Management.ModifyRole", role);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }

        [HttpPost()]
        public JsonResult Add(Role role)
        {
            Service services = new Service();
            Message msg = services.Execute<Role, Message>("Management.CreateRole", role);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }

        [HttpPost()]
        public JsonResult Delete(Role role)
        {
            Service services = new Service();
            Message msg = services.Execute<Role, Message>("Management.RemoveRole", role);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }
        #endregion

        #region metodos de la vista User

        public ActionResult User()
        {
            if (Session["User"] != null)
            {
                ViewBag.role = RoleUser();
                Departments _Department = new Departments();
                ViewBag.Department = _Department.Department();

                return View(GetLoginUser());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost()]
        public JsonResult UpdateU(LoginUser loginUser)
        {
            //Actualiza al usuario
            Service services = new Service();
            Message msg = services.Execute<LoginUser, Message>("LoginUser.Update", loginUser);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }

        [HttpPost()]
        public JsonResult AddU(LoginUser loginUser)
        {
            loginUser.Id = Guid.NewGuid();

            //Crear usuario
            Service services = new Service();
            Message msg = services.Execute<LoginUser, Message>("LoginUser.Create", loginUser);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }

        [HttpPost()]
        public JsonResult DeleteU(LoginUser loginUser)
        {
            //Actualiza al usuario
            Service services = new Service();
            Message msg = services.Execute<LoginUser, Message>("LoginUser.Delete", loginUser);

            JsonResult json = new JsonResult();

            if (msg.error == false)
            {
                json.Data = "exito";
            }
            return (json);
        }

        public Role[] RoleUser()
        {
            Service services = new Service();
            Role[] role = services.Execute<Role[]>("Management.GetRole");

            return role;
        }

        public List<LoginUser> GetLoginUser()
        {
            Service services = new Service();
            List<LoginUser> loginUser = services.Execute<List<LoginUser>>("LoginUser.Users");
            if (loginUser.Count() < 1) { loginUser = null; }

            return loginUser;
        }
        #endregion
    }

}


