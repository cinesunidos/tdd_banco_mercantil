﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin_usuarios.Models;
using System.Web.Hosting;
using Newtonsoft.Json;
using System.IO;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Configuration;

namespace admin_usuarios.Controllers
{
    public class ModalController : Controller
    {
        // GET: Modal
        public ActionResult Index()
        {
            if (Session["User"] != null)
            {
                ModalCinema m = new ModalCinema();
                m.CinemaCode = int.Parse(ConfigurationManager.AppSettings["ModalCinemaCode"].ToString());
                string url = ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString() + ConfigurationManager.AppSettings["Concession.ModalEstatus"].ToString();
                Service service = new Service();
                m = service.Execute<ModalCinema, ModalCinema>("Concession.ModalEstatus", m, url);
                if (m.CinemaCode != 0 && m.CinemaName != null)
                {
                    List<ModalCinema> cines = new List<ModalCinema>();
                    cines.Add(m);
                    return View(cines);
                }
                else
                {
                    return View(m.GetListCinema());
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public ActionResult Modifymodal(List<ModalCinema> model)
        {
            if (ModelState.IsValid)
            {
                Message response = new Message();
                string filePath = string.Empty;
                ModalCinema mCinema = model.Where(c => c.CinemaCode == int.Parse(ConfigurationManager.AppSettings["ModalCinemaCode"])).SingleOrDefault();
                mCinema.DateChanged = DateTime.Now;
                string ListaJson = JsonConvert.SerializeObject(mCinema);
                var cadena = System.Configuration.ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString()
                    + System.Configuration.ConfigurationManager.AppSettings["Concession.ModifyModal"].ToString();
                Service ser = new Service();
                response = ser.Execute<string, Message>("Concession.ModifyModal", ListaJson, cadena);
                //#if DEBUG
                //                filePath = HostingEnvironment.MapPath("~/App_Data/ModalConcessions.txt");
                //#endif
                //#if (!DEBUG)
                //                string cachePath = RoleEnvironment.GetLocalResource("Messages").RootPath;
                //                filePath = Path.Combine(cachePath, "ModalConcessions.txt");
                //#endif



                //                if (!System.IO.File.Exists(filePath))
                //                {
                //                    System.IO.File.Create(filePath).Close();
                //                    using (StreamWriter Sw = new StreamWriter(filePath, false))
                //                    {
                //                        Sw.WriteLine(ListaJson);
                //                        Sw.Close();
                //                    };
                //                    Service ser = new Service();
                //                    //invocar el metodo de Wcf ppublish para hacer caching del archivo en formato json

                //                    //Concession.svc/ModifyModalConcessions/

                //                    response = ser.Execute<string, Message>("Concession.ModifyModal", ListaJson, cadena);
                //                }
                //                else
                //                {
                //                    using (StreamWriter Sw = new StreamWriter(filePath, false))
                //                    {
                //                        Sw.WriteLine(ListaJson);
                //                        Sw.Close();
                //                    };
                //                    Service ser = new Service();
                //                    //invocar el metodo de Wcf ppublish para hacer caching del archivo en formato json
                //                    // response = ser.Execute<string, Message>("Concession.ModifyModal", ListaJson);
                //                    response = ser.Execute<string, Message>("Concession.ModifyModal", ListaJson, cadena);
                //                }
                if (!response.error)
                {
                    ViewBag.text = "Se realizó la modificacion de forma exitosa!";
                    ViewBag.Error = "";
                }
                else
                {
                    ViewBag.text = "Hubo un problema con la actualización de los dartos!";
                    ViewBag.Error = response.message;
                }
            }
            return View();
        }

        public ActionResult VerifyModal()
        {
            try
            {
                if (Session["User"] != null)
                {
                    List<ModalCinema> modalCinemas = new List<ModalCinema>();
                    string url = ConfigurationManager.AppSettings["Services.Proxy.Url"].ToString() + ConfigurationManager.AppSettings["Concession.AllModalEstatus"].ToString();
                    Service service = new Service();
                    modalCinemas = service.Execute<List<ModalCinema>>("Concession.AllModalEstatus");
                    return View(modalCinemas);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception Ex)
            {
                var mensaje = Ex.Message;
                throw;
            }
        }
    }
}