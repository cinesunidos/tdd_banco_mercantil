﻿using admin_usuarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Web.Services;
using System.Security.Cryptography;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Drawing;
using System.Globalization;
using Newtonsoft.Json;
using System.Web.Script.Serialization;


namespace admin_usuarios.Controllers
{
    public class ConcessionsController : Controller
    {

        #region Modal Http Methods

        // GET: Modal
        [HttpGet]
        public ActionResult Modal()
        {
            if (Session["User"] != null)
            {
                string key = "ModalImageUrl";
                Service services = new Service();
                ConcessionSetting settingsModal = services.Execute<ConcessionSetting>("Concession.GetSetting", new { key });
                ModalViewModel model = new ModalViewModel();
                model.ImageUrl = settingsModal.Value;
                //model.ImageUrl = "http://cinesunidosweb.blob.core.windows.net/cms/modal.png";
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public ActionResult Modal(HttpPostedFileBase file)
        {

            if (file == null)
            {
                TempData["message"] = "No se ha seleccionado ninguna imágen.";
                TempData["messageClass"] = "alert-danger";
                return RedirectToAction("Modal", "Concessions");
            }

            // Validate mime type
            string[] allowedImageTypes = { "image/gif", "image/pjpeg", "image/bmp", "image/x-png", "image/png", "image/jpeg" };
            var postedType = file.ContentType;
            if (!allowedImageTypes.Contains(postedType))
            {
                TempData["message"] = "Seleccione un archivo de imágen válido.";
                TempData["messageClass"] = "alert-danger";
                return RedirectToAction("Modal", "Concessions");
            }

            try
            {
                // Get file info
                FileInfo fileInfo = new FileInfo(file.FileName);
                // Generate a new random name, to prevent cdn from caching the image
                string randomKey = GenerateRandomFilename();
                // upload file to azure
                uploadImageFileToAzureContainer(file, "cms", "modal.png");

                // update database with the new URL
                Service services = new Service();
                ConcessionSetting newSetting = new ConcessionSetting();
                newSetting.Key = "ModalImageUrl";
                newSetting.Value = "http://az693035.vo.msecnd.net/cms/modal.png?" + randomKey;
                Message message = services.Execute<ConcessionSetting, Message>("Concession.UpdateSetting", newSetting);

                TempData["message"] = message.message;
                TempData["messageClass"] = message.error ? "alert-danger" : "alert-success";
                return RedirectToAction("Modal", "Concessions");
            }
            catch (Exception exs)
            {
                TempData["message"] = "Ha ocurrido un error. No se pudo guardar la imagen.";
                TempData["messageClass"] = "alert-danger";
                return RedirectToAction("Modal", "Concessions");
            }

        }

        #endregion

        #region Http Reportes

        // GET: Reportes
        [HttpGet]
        public ActionResult Reportes(string id)
        {
            if (Session["User"] != null)
            {
                DateTime today = DateTime.Today;
                if (id != null)
                {
                    today = DateTime.ParseExact(id, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                }
                string date = today.ToString("yyyyMMdd");

                Service services = new Service();
                ConcessionTransactionResponse response = services.Execute<ConcessionTransactionResponse>("Concession.GetSalesReportByDate", new { date = date });

                var viewModel = new ReporteViewModel();
                viewModel.Date = today;
                viewModel.Transactions = response.Transactions;
                viewModel.Summary = response.Summary;
                viewModel.TotalSales = response.TotalSales;
                viewModel.Param = date;
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: ReporteCompleto
        [HttpGet]
        public ActionResult ReporteCompleto(string id)
        {
            if (Session["User"] != null)
            {
                DateTime today = DateTime.Today;
                if (id != null)
                {
                    today = DateTime.ParseExact(id, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                }
                string date = today.ToString("yyyyMMdd");

                Service services = new Service();
                ConcessionTransactionResponse response = services.Execute<ConcessionTransactionResponse>("Concession.GetFullSalesReportByDate", new { date = date });



                var viewModel = new ReporteViewModel();
                viewModel.Date = today;
                viewModel.Transactions = response.Transactions;
                viewModel.Summary = response.Summary;
                viewModel.TotalSales = response.TotalSales;
                viewModel.Param = date;
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: Catalogo
        [HttpGet]
        public ActionResult Catalogo()
        {
            if (Session["User"] != null)
            {
                var theaterId = "0010";
                Service services = new Service();
                ConcessionCatalog response = services.Execute<ConcessionCatalog>("Concession.GetConcessionCatalogByTheater", new { theaterId = theaterId });

                var viewModel = new ConcessionCatalogViewModel();
                viewModel.Catalog = response;
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: Catalogo
        [HttpGet]
        public ActionResult Producto(string id)
        {
            if (Session["User"] != null)
            {
                Service services = new Service();
            ConcessionItem product = services.Execute<ConcessionItem>("Concession.GetConcessionItem", new { itemId = id });

            return View(product);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public ActionResult Producto(string itemId, HttpPostedFileBase file)
        {

            if (file == null)
            {
                TempData["message"] = "No se ha seleccionado ninguna imágen.";
                TempData["messageClass"] = "alert-danger";
                return RedirectToAction("Catalogo", "Concessions");
            }

            // Validate mime type
            string[] allowedImageTypes = { "image/gif", "image/pjpeg", "image/bmp", "image/x-png", "image/png", "image/jpeg" };
            var postedType = file.ContentType;
            if (!allowedImageTypes.Contains(postedType))
            {
                TempData["message"] = "Seleccione un archivo de imágen válido.";
                TempData["messageClass"] = "alert-danger";
                return RedirectToAction("Catalogo", "Concessions");
            }

            try
            {
                // Get file info
                FileInfo fileInfo = new FileInfo(file.FileName);
                // upload file to azure
                string filename = itemId;
                uploadImageFileToAzureContainer(file, "cms", filename);


                TempData["message"] = "Imagen actualizada";
                TempData["messageClass"] = "alert-success";
            }
            catch (Exception exs)
            {
                TempData["message"] = "Ha ocurrido un error. No se pudo guardar la imagen.";
                TempData["messageClass"] = "alert-danger";
            }

            return RedirectToAction("Catalogo", "Concessions");
        }

        [HttpPost]
        public FileResult ExportExcel()
        {
            string message = string.Empty;
            string id = string.Empty;
            DateTime today = DateTime.Today;
            string date = today.ToString("yyyyMMdd");
            Service services = new Service();
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte Completo de Caramelería");
            try
            {
                ConcessionTransactionResponse response = services.Execute<ConcessionTransactionResponse>("Concession.GetFullSalesReportByDate", new { date = date });

                var Transactions = response.Transactions;

                ws.Cells["B4"].Merge = true;
                ws.Cells["B4"].Value = "Reporte Completo de Caramelería";
                ws.Cells["B4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                Image image = Image.FromFile(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/cu-brand.png"));
                ExcelPicture pic = ws.Drawings.AddPicture("Sample", image);
                pic.SetPosition(0, 0, 0, 0);
                pic.SetSize(160, 160);

                ws.Cells["E2"].Value = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                ws.Cells["E2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                //ws.Cells["B5"].Merge = true;
                //ws.Cells["B5"].Value = "Cine La Piramide";
                //ws.Cells["B5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ws.Cells["A9"].Value = "Resumen";
                ws.Cells["A9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                ws.Cells["A10:B10"].Merge = true;
                ws.Cells["A10"].Value = "Monto en bs: " + string.Format("{0:N}", double.Parse(response.TotalSales.ToString())); ;
                ws.Cells["A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                var Summary = response.Summary;

                ws.Cells["A12"].Value = "ID Producto";
                ws.Cells["A12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["A12"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(1).Width = 15;

                ws.Cells["B12"].Value = "Producto";
                ws.Cells["B12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["B12"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(2).Width = 50;

                ws.Cells["C12"].Value = "Cantidad";
                ws.Cells["C12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["C12"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["D12"].Value = "Precio Unidad";
                ws.Cells["D12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["D12"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(4).Width = 15;

                ws.Cells["E12"].Value = "SubTotal";
                ws.Cells["E12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["E12"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(5).Width = 15;

                int CONTA = 13;
                foreach (var Summa in Summary)
                {
                    ws.Cells["A" + CONTA].Value = Summa.ItemId;
                    ws.Cells["A" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["A" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["B" + CONTA].Value = Summa.ItemDescription;
                    ws.Cells["B" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["B" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["C" + CONTA].Value = Summa.ItemQuantity;
                    ws.Cells["C" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["C" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["D" + CONTA].Value = string.Format("{0:N}", double.Parse(Summa.ItemValue.ToString()));
                    ws.Cells["D" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["D" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["E" + CONTA].Value = string.Format("{0:N}", double.Parse(Summa.SubTotal.ToString()));
                    ws.Cells["E" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["E" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    ++CONTA;
                }

                int det = CONTA + 3;
                ws.Cells["A" + det].Value = "Detalle de Transacciones";
                int det1 = det + 2;

                ws.Cells["A" + det1].Value = "ID Transacción";
                ws.Cells["A" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["A" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["B" + det1].Value = "Producto";
                ws.Cells["B" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["B" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["C" + det1].Value = "Cantidad";
                ws.Cells["C" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["C" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["D" + det1].Value = "Precio Unidad";
                ws.Cells["D" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["D" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["E" + det1].Value = "SubTotal";
                ws.Cells["E" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["E" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["F" + det1].Value = "Función";
                ws.Cells["F" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["F" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(6).Width = 40;

                ws.Cells["G" + det1].Value = "Pickup";
                ws.Cells["G" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["G" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["H" + det1].Value = "Estación Pickup";
                ws.Cells["H" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["H" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(8).Width = 15;

                ws.Cells["I" + det1].Value = "Usuario Pickup";
                ws.Cells["I" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["I" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(9).Width = 15;

                ws.Cells["J" + det1].Value = "Hora Pickup";
                ws.Cells["J" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["J" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(10).Width = 15;

                int det2 = det1 + 1;

                foreach (var Transaction in Transactions)
                {
                    ws.Cells["A" + det2].Value = Transaction.TransactionId;
                    ws.Cells["A" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["A" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["B" + det2].Value = Transaction.ItemDescription;
                    ws.Cells["B" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["B" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["C" + det2].Value = Transaction.ItemQuantity;
                    ws.Cells["C" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["C" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["D" + det2].Value = string.Format("{0:N}", double.Parse(Transaction.ItemValue.ToString()));
                    ws.Cells["D" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["D" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["E" + det2].Value = string.Format("{0:N}", double.Parse(Transaction.SubTotal.ToString()));
                    ws.Cells["E" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["E" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["F" + det2].Value = Transaction.FilmTitle + " " + Transaction.FilmDate.ToString("H:mm");
                    ws.Cells["F" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["F" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["G" + det2].Value = Transaction.IsCollected ? "Sí" : "No";
                    ws.Cells["G" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["G" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["H" + det2].Value = Transaction.PickupWorkstation;
                    ws.Cells["H" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["H" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["I" + det2].Value = Transaction.PickupUser;
                    ws.Cells["I" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["I" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["J" + det2].Value = Transaction.CollectionDate.ToString("H:mm");
                    ws.Cells["J" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["J" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ++det2;
                }
            }
            catch (Exception ex)
            {
                message = "Error: " + ex.Message.ToString();
                ViewData["MessageExcel"] = message;
            }

            if ((message.Contains("Error:")))
            {
                ws.Cells["A1"].Value = message;
                ws.Cells["A1"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(1).Width = 150;
                //ViewData["MessageExcel"] = "El archivo de excel fue creado exitosamente";
                var memoryStream = pck.GetAsByteArray();
                var fileName = string.Format("Reporte Completo de Caramelería.xlsx");
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            else
            {
                //return RedirectToAction("Reportes", "Concessions");
                var memoryStream = pck.GetAsByteArray();
                var fileName = string.Format("Reporte Completo de Caramelería.xlsx");
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }

        }

        public FileResult ExportExcel2()
        {
            string message = string.Empty;
            string id = string.Empty;
            DateTime today = DateTime.Today;
            string date = today.ToString("yyyyMMdd");
            Service services = new Service();
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte de transacciones de Caramelería");
            try
            {
                ConcessionTransactionResponse response = services.Execute<ConcessionTransactionResponse>("Concession.GetFullSalesReportByDate", new { date = date });

                var Transactions = response.Transactions;

                ws.Cells["C4:F4"].Merge = true;
                ws.Cells["C4"].Value = "Reporte de transacciones de Caramelería";
                ws.Cells["C4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                Image image = Image.FromFile(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/cu-brand.png"));
                ExcelPicture pic = ws.Drawings.AddPicture("Sample", image);
                pic.SetPosition(0, 0, 0, 0);
                pic.SetSize(160, 160);

                ws.Cells["J2"].Value = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                ws.Cells["J2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                //ws.Cells["C5:F5"].Merge = true;
                //ws.Cells["C5"].Value = "Cine La Piramide";
                //ws.Cells["C5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ws.Cells["A9"].Value = "Detalle de Transacciones";

                ws.Cells["A10"].Value = "ID Transacción";
                ws.Cells["A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["A10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(1).Width = 15;

                ws.Cells["B10"].Value = "Producto";
                ws.Cells["B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["B10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(2).Width = 50;

                ws.Cells["C10"].Value = "Cantidad";
                ws.Cells["C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["C10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);


                ws.Cells["D10"].Value = "Precio Unidad";
                ws.Cells["D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["D10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(4).Width = 15;

                ws.Cells["E10"].Value = "SubTotal";
                ws.Cells["E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["E10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(5).Width = 15;

                ws.Cells["F10"].Value = "Función";
                ws.Cells["F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["F10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(6).Width = 40;

                ws.Cells["G10"].Value = "Pickup";
                ws.Cells["G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["G10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                ws.Cells["H10"].Value = "Estación Pickup";
                ws.Cells["H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["H10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(8).Width = 15;

                ws.Cells["I10"].Value = "Usuario Pickup";
                ws.Cells["I10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["I10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(9).Width = 15;

                ws.Cells["J10"].Value = "Hora Pickup";
                ws.Cells["J10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["J10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(10).Width = 15;

                int CONTA = 11;

                foreach (var Transaction in Transactions)
                {
                    ws.Cells["A" + CONTA].Value = Transaction.TransactionId;
                    ws.Cells["A" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["A" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["B" + CONTA].Value = Transaction.ItemDescription;
                    ws.Cells["B" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["B" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["C" + CONTA].Value = Transaction.ItemQuantity;
                    ws.Cells["C" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["C" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["D" + CONTA].Value = string.Format("{0:N}", double.Parse(Transaction.ItemValue.ToString()));
                    ws.Cells["D" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["D" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["E" + CONTA].Value = string.Format("{0:N}", double.Parse(Transaction.SubTotal.ToString()));
                    ws.Cells["E" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["E" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["F" + CONTA].Value = Transaction.FilmTitle + " " + Transaction.FilmDate.ToString("H:mm");
                    ws.Cells["F" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["F" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["G" + CONTA].Value = Transaction.IsCollected ? "Sí" : "No";
                    ws.Cells["G" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["G" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["H" + CONTA].Value = Transaction.PickupWorkstation;
                    ws.Cells["H" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["H" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["I" + CONTA].Value = Transaction.PickupUser;
                    ws.Cells["I" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["I" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ws.Cells["J" + CONTA].Value = Transaction.CollectionDate.ToString("H:mm");
                    ws.Cells["J" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["J" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                    ++CONTA;
                }
            }
            catch (Exception ex)
            {
                message = "Error: " + ex.Message.ToString();
                ViewData["MessageExcel"] = message;
            }

            if ((message.Contains("Error:")))
            {
                ws.Cells["A1"].Value = message;
                ws.Cells["A1"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(1).Width = 150;
                var memoryStream = pck.GetAsByteArray();
                var fileName = string.Format("Reporte de transacciones de Caramelería.xlsx");
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            else
            {
                var memoryStream = pck.GetAsByteArray();
                var fileName = string.Format("Reporte de transacciones de Caramelería.xlsx");
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
        }

        [HttpGet]
        public ActionResult Reporte_por_Fecha()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public ActionResult Consulta_X_Fecha(String DateFrom, String DateUntil)
        {
            DateTime dt1;
            DateTime dt2;

            if (String.IsNullOrWhiteSpace(DateFrom) || String.IsNullOrWhiteSpace(DateUntil))
            {
                ViewData["Json"] = "Debe ingresar información en los campos de rango de fecha";
                return View("Reporte_por_Fecha");
            }

            if (!DateTime.TryParseExact(DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt1) ||
                !DateTime.TryParseExact(DateUntil, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt2))
            {
                ViewData["Json"] = "El formato de fecha ingresado no es correcto";
                return View("Reporte_por_Fecha");
            }

            if (Convert.ToDateTime(DateFrom) > Convert.ToDateTime(DateUntil))
            {
                ViewData["Json"] = "El rango de fecha ingresado es incorrecto";
                return View("Reporte_por_Fecha");
            }

            string fechas = Convert.ToDateTime(DateFrom).ToString("yyyyMMdd") + ";" + Convert.ToDateTime(DateUntil).ToString("yyyyMMdd");
            Service services = new Service();
            Message data = services.Execute<string, Message>("Concession.GetSalesReportFilterByDate", fechas);


            services = new Service();
            Message dataComponentes = services.Execute<string, Message>("Concession.GetSalesReportComponentsFilterByDate", fechas);


            string json = string.Empty;
            string jsoncomponentes = string.Empty;

            if (data.message.Contains("Error:"))
            {
                ViewData["ErrorJson"] = data.message.ToString();
                return View("Reporte_por_Fecha");
            }
            else
            {
                json = data.message.Replace("{" + "\"Transactions\":", "").Replace("}]}", "}]").Replace("TransactionType", "Tipo de Transacción").Replace("TransactionStatus", "Estatus").Replace("FilmTitle", "Película").Replace("FilmDate", "Fecha de la Película").Replace("ItemDescription", "Producto").Replace("ItemValue", "Precio").Replace("ItemQuantity", "Cantidad").Replace("PickupUser", "Usuario Pickup").Replace("PickupWorkstation", "Estación Pickup");

                if (json.Length < 10)
                {
                    ViewData["Json"] = "La consulta realizada no contiene registros";
                    return View("Reporte_por_Fecha");
                }
            }

            if (dataComponentes.message.Contains("Error:"))
            {
                ViewData["ErrorJson"] = data.message.ToString();
                return View("Reporte_por_Fecha");
            }
            else
            {
                jsoncomponentes = dataComponentes.message.Replace("{" + "\"Transactions\":", "").Replace("}]}", "}]");

                if (jsoncomponentes.Length < 10)
                {
                    ViewData["Json"] = "La consulta realizada no contiene registros";
                    return View("Reporte_por_Fecha");
                }
            }

            string error = string.Empty;
            try
            {
                string FilePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/pivot/prueba.json");

                if (!System.IO.File.Exists(FilePath))
                {
                    System.IO.File.Create(FilePath).Close();
                    System.IO.File.WriteAllText(FilePath, json);
                }
                else
                {
                    System.IO.File.Delete(FilePath);
                    System.IO.File.Create(FilePath).Close();
                    System.IO.File.WriteAllText(FilePath, json);
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorJson"] = "Error: " + ex.Message.ToString();
                return View("Reporte_por_Fecha");
            }

            var json1 = JsonConvert.DeserializeObject(jsoncomponentes);
            var jsonObj = new JavaScriptSerializer().Deserialize<List<ConcessionTransactionByDateC>>(json1.ToString());
            List<ConcessionTransactionByDateC> ListCompont = jsonObj;

            ViewData["DateFrom"] = DateFrom;
            ViewData["DateUntil"] = DateUntil;
            return View("Vista_Reporte_por_Fecha", ListCompont);

        }

        public FileResult ExportExcel3(String DateFrom, String DateUntil)
        {
            DateTime dt1;
            DateTime dt2;
            string error = null;
            string message = string.Empty;
            string id = string.Empty;
            DateTime today = DateTime.Today;
            string date = today.ToString("yyyyMMdd");
            Service services = new Service();
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte de transacciones por fecha de Caramelería");
            try
            {
                if (!String.IsNullOrWhiteSpace(DateFrom) && !String.IsNullOrWhiteSpace(DateUntil))
                {
                    if (DateTime.TryParseExact(DateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt1) &&
                    DateTime.TryParseExact(DateUntil, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt2))
                    {
                        if (Convert.ToDateTime(DateFrom) <= Convert.ToDateTime(DateUntil))
                        {
                            string fechas = Convert.ToDateTime(DateFrom).ToString("yyyyMMdd") + ";" + Convert.ToDateTime(DateUntil).ToString("yyyyMMdd");
                            Message data = services.Execute<string, Message>("Concession.GetSalesReportFilterByDate", fechas);
                            Message dataComponentes = services.Execute<string, Message>("Concession.GetSalesReportComponentsFilterByDate", fechas);
                            string jsoncomponentes = dataComponentes.message.Replace("{" + "\"Transactions\":", "").Replace("}]}", "}]");
                            String DataJson = data.message.Replace("{" + "\"Transactions\":", "").Replace("}]}", "}]");

                            if (!(data.message.Contains("Error:")) && (DataJson.Length > 10) && !(dataComponentes.message.Contains("Error:")) && (jsoncomponentes.Length > 10))
                            {
                                var json = JsonConvert.DeserializeObject(DataJson);
                                var jsonObj = new JavaScriptSerializer().Deserialize<List<ConcessionTransactionByDateF>>(json.ToString());

                                var json1 = JsonConvert.DeserializeObject(jsoncomponentes);
                                var jsonObj1 = new JavaScriptSerializer().Deserialize<List<ConcessionTransactionByDateC>>(json1.ToString());

                                ws.Cells["C4:F4"].Merge = true;
                                ws.Cells["C4"].Value = "Reporte de transacciones por fecha de Caramelería";
                                ws.Cells["C4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                                Image image = Image.FromFile(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/cu-brand.png"));
                                ExcelPicture pic = ws.Drawings.AddPicture("Sample", image);
                                pic.SetPosition(0, 0, 0, 0);
                                pic.SetSize(160, 160);

                                ws.Cells["J2"].Value = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                                ws.Cells["J2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                                ws.Cells["A10"].Value = "ID Transacción";
                                ws.Cells["A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["A10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(1).Width = 15;

                                ws.Cells["B10"].Value = "Producto";
                                ws.Cells["B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["B10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(2).Width = 50;

                                ws.Cells["C10"].Value = "Cantidad";
                                ws.Cells["C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["C10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);


                                ws.Cells["D10"].Value = "Precio Unidad";
                                ws.Cells["D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["D10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(4).Width = 15;

                                ws.Cells["E10"].Value = "SubTotal";
                                ws.Cells["E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["E10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(5).Width = 15;

                                ws.Cells["F10"].Value = "Función";
                                ws.Cells["F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["F10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(6).Width = 40;

                                ws.Cells["G10"].Value = "Pickup";
                                ws.Cells["G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["G10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                ws.Cells["H10"].Value = "Estación Pickup";
                                ws.Cells["H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["H10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(8).Width = 15;

                                ws.Cells["I10"].Value = "Usuario Pickup";
                                ws.Cells["I10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["I10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(9).Width = 15;

                                ws.Cells["J10"].Value = "Hora Pickup";
                                ws.Cells["J10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["J10"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(10).Width = 15;

                                int CONTA = 11;

                                foreach (var Transaction in jsonObj)
                                {
                                    ws.Cells["A" + CONTA].Value = Transaction.TransactionId;
                                    ws.Cells["A" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["A" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["B" + CONTA].Value = Transaction.ItemDescription;
                                    ws.Cells["B" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["B" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["C" + CONTA].Value = Transaction.ItemQuantity;
                                    ws.Cells["C" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["C" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["D" + CONTA].Value = string.Format("{0:N}", double.Parse(Transaction.ItemValue.ToString()));
                                    ws.Cells["D" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["D" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["E" + CONTA].Value = string.Format("{0:N}", double.Parse(Transaction.SubTotal.ToString()));
                                    ws.Cells["E" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["E" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["F" + CONTA].Value = Transaction.FilmTitle;
                                    ws.Cells["F" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["F" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["G" + CONTA].Value = Transaction.Pickup;
                                    ws.Cells["G" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["G" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["H" + CONTA].Value = Transaction.PickupWorkstation;
                                    ws.Cells["H" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["H" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["I" + CONTA].Value = Transaction.PickupUser;
                                    ws.Cells["I" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["I" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["J" + CONTA].Value = Transaction.CollectionDate.ToString("H:mm");
                                    ws.Cells["J" + CONTA].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["J" + CONTA].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ++CONTA;
                                }

                                int det = CONTA + 3;
                                ws.Cells["A" + det].Value = "Componentes";
                                int det1 = det + 2;

                                ws.Cells["A" + det1].Value = "Producto";
                                ws.Cells["A" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["A" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(1).Width = 50;

                                ws.Cells["B" + det1].Value = "Cantidad";
                                ws.Cells["B" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["B" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                ws.Cells["C" + det1].Value = "Estación Pickup";
                                ws.Cells["C" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["C" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(3).Width = 15;

                                ws.Cells["D" + det1].Value = "Usuario Pickup";
                                ws.Cells["D" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["D" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                ws.Cells["E" + det1].Value = "Componentes";
                                ws.Cells["E" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["E" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                                ws.Column(5).Width = 50;

                                ws.Cells["F" + det1].Value = "Cantidad de Componentes";
                                ws.Cells["F" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["F" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                ws.Cells["G" + det1].Value = "Total";
                                ws.Cells["G" + det1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["G" + det1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                int det2 = det1 + 1;

                                foreach (var Components in jsonObj1)
                                {
                                    ws.Cells["A" + det2].Value = Components.ItemId + " " + Components.ItemDescription;
                                    ws.Cells["A" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["A" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["B" + det2].Value = Components.ItemQuantity;
                                    ws.Cells["B" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["B" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["C" + det2].Value = Components.PickupWorkstation;
                                    ws.Cells["C" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["C" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["D" + det2].Value = Components.PickupUser;
                                    ws.Cells["D" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["D" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["E" + det2].Value = Components.Componentes;
                                    ws.Cells["E" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["E" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["F" + det2].Value = Components.Cantidad_Componentes;
                                    ws.Cells["F" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["F" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ws.Cells["G" + det2].Value = Components.Total_Cantidad_Componentes;
                                    ws.Cells["G" + det2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    ws.Cells["G" + det2].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);

                                    ++det2;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = "Error: " + ex.Message.ToString();
            }

            if (error != null)
            {
                ws.Cells["A1"].Value = error;
                ws.Cells["A1"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ws.Column(1).Width = 150;

                var memoryStream = pck.GetAsByteArray();
                var fileName = string.Format("Reporte de transacciones por fecha de Caramelería.xlsx");
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            else
            {
                var memoryStream = pck.GetAsByteArray();
                var fileName = string.Format("Reporte de transacciones por fecha de Caramelería.xlsx");
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
        }

        #endregion

        #region UploadFile Helper Methods
        private void uploadImageFileToAzureContainer(HttpPostedFileBase file, string containerId, string blobname)
        {
            // retrieve storage account
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));
            // create the blob client
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // retrieve a reference to a container
            CloudBlobContainer container = blobClient.GetContainerReference(containerId);
            // create or overwrite the "modal" blob with the image file
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobname);
            blockBlob.UploadFromStream(file.InputStream);
        }

        private static string GenerateRandomFilename()
        {
            string timestamp = DateTime.Now.ToString("yyMMdd");
            string uniqid = GetUniqueKey(8);
            string filename = timestamp + "_" + uniqid;
            return filename;
        }

        private static string GetUniqueKey(int size)
        {
            byte[] data = new byte[size];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetBytes(data);
            return BitConverter.ToString(data).Replace("-", String.Empty);
        }

        #endregion

    }

}