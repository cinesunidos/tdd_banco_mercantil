﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(admin_usuarios.Startup))]
namespace admin_usuarios
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
