using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using StackExchange.Redis;
using Newtonsoft.Json;
using Newtonsoft;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using admin_usuarios.Controllers;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace admin_usuarios.Models
{
    public class LoadMovies
    {
        public string HO { get; set; }
        public string Title { get; set; }
    }

    public class BlackBerry_VItem
    {
        public string ID { get; set; }
        public string Type { get; set; }
        public string ImageID { get; set; }
        public string MovieID { get; set; }
        public string Date { get; set; }
        public string Url { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
    }
}