﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class Departments
    {
        public string[] Department()
        {            
            string[] dep = { "Aprendizaje","Auditoria","Beneficios","Cobranzas","Compras",
                             "Comunicaciones Internas","Concesiones","Contraloria","Desarrollo Sistemas",
                             "Distribucion","Facturacion","Gerencia General","Gestion Documental","Infraestructura",
                             "Infraestructura Sistemas","Mantenimiento Sistemas","Mercadeo","Mesa De Ayuda","Nomina",
                             "Nuevos Proyectos","Operaciones","Planificacion Y Desarrollo","Procesos Y Calidad","Programacion",
                             "Proyectos","Reclutamiento Y Seleccion","Seguridad","Tecnologias Y Servicios","Tele Ventas",
                             "Tesoreria","Ventas"                             
            };

            return dep;
        }

    }

}