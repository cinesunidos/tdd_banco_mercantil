﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    /// <summary>
    /// Representa la configuracion de la ventana modal para un cine es especifico
    /// </summary>
    public class ModalCinema
    {
        public int CinemaCode { get; set; }
        public string CinemaName { get; set; }
        public bool ModalEnabled { get; set; }
        public DateTime DateChanged { get; set; }

        /// <summary>
        /// devuelve una instancia de la clase Modalcinema, la instancia devuelta depende del codigo de cine colocado en el WebConfig con el TAG "ModalCinemaCode"
        /// </summary>
        /// <returns></returns>
        public List<ModalCinema> GetListCinema()
        {
            List<ModalCinema> Cines = new List<ModalCinema>()
                {
                    new ModalCinema
                    {
                        CinemaCode = 1008,
                        CinemaName = "EL Marques",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1020,
                        CinemaName = "Galerias Avila",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1009,
                        CinemaName = "Galerias Paraiso",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1003,
                        CinemaName = "Guatire PLaza",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1027,
                        CinemaName = "Lider",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1001,
                        CinemaName = "Los Naranjos",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1002,
                        CinemaName = "Metrocenter",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1028,
                        CinemaName = "La Piramide",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1026,
                        CinemaName = "Millennium",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1005,
                        CinemaName = "Sambil Caracas",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1015,
                        CinemaName = "Hiper Jumbo",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1013,
                        CinemaName = "La Granja",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1010,
                        CinemaName = "Las Americas",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1011,
                        CinemaName = "Metropolis",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1025,
                        CinemaName = "Sambil Barquisimeto",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1014,
                        CinemaName = "Sambil Valencia",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1006,
                        CinemaName = "Trinitarias",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1016,
                        CinemaName = "Metrosol",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1017,
                        CinemaName = "Sambil Maracaibo",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },new ModalCinema
                    {
                        CinemaCode = 1024,
                        CinemaName = "Sambil San Cristobal",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1023,
                        CinemaName = "Orinokia",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1007,
                        CinemaName = "Petroriente",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1019,
                        CinemaName = "Regina",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    },
                    new ModalCinema
                    {
                        CinemaCode = 1021,
                        CinemaName = "Sambil Margarita",
                        DateChanged = DateTime.Now,
                        ModalEnabled = false
                    }
                };
            try
            {
                return Cines.Where(c => c.CinemaCode == int.Parse(ConfigurationManager.AppSettings["ModalCinemaCode"])).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}