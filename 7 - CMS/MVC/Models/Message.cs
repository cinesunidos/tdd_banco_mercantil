﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class Message
    {      
        public Boolean error { get; set; }       
        public string message { get; set; }       
        public int number { get; set; }
    }
}
