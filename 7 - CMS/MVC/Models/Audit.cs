﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class Audit
    {         
        public Guid Id { get; set; }        
        public Guid UserId { get; set; }      
        public string UserName { get; set; }  
        public string Action { get; set; }        
        public string Event { get; set; }       
        public string Department { get; set; } 
        public DateTime dateTime { get; set; }
    }
}
