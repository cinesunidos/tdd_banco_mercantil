﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class User
    {
        
        public string Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string IdCard { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Boolean Active { get; set; }
        public bool MassMailSubcription { get; set; }
        public string Platform { get; set; }
        // Propiedades para usarse al crear un registro en la tabla Auditoria
        public Guid userAudit { get; set; }
        public string Department { get; set; }
    }
}