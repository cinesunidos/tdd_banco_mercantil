﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class AuditParameters
    {       
        public string DateBegin { get; set; }       
        public string DateEnd { get; set; }       
        public string UserId { get; set; }
        public string Department { get; set; }
    }
}

