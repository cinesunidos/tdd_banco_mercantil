﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace admin_usuarios.Models
{
   
    public class LoginUser
    {
        
        public System.Guid Id { get; set; }
        
        public string UserId { get; set; }
        
        public string Password { get; set; }
        
        public string Name { get; set; }
        
        public string Department { get; set; }
        
        public System.Guid RoleId { get; set; }
        
        public List<Permissions> Permissions { get; set; }
    }
}