﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class BlackList
    {       
        public System.Guid Id { get; set; }       
        public string IdCard { get; set; }       
        public string Email { get; set; }       
        public string platform { get; set; }        
        public string description { get; set; }        
        public System.DateTime create { get; set; }
        //Para uso de auditoria
        public Guid UserId { get; set; }
        public string Department { get; set; }
    }
}

