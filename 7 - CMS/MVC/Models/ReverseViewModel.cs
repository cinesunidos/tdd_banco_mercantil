﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class ReverseViewModel
    {
        public string itemDescription { get; set; }
        public string itemCode { get; set; }
        public string UnidadDeMedida { get; set; }
        public string itemType { get; set; }
        public int Quantity { get; set; }
        public bool Quitar { get; set; }
        public int AviableToReverse { get; set; }

        public static implicit operator ReverseViewModel(TransferItem t)
        {
            ReverseViewModel r = new ReverseViewModel
            {
                AviableToReverse = t.Quantity,
                itemCode = t.itemCode,
                itemDescription = t.itemDescription,
                itemType = t.itemType,
                Quantity = 0,
                Quitar = false,
                UnidadDeMedida = t.UnidadDeMedida
            };
            return r;
        }
    }
}