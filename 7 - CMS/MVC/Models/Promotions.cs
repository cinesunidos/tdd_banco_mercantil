using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace admin_usuarios.Models
{
    public class DataPostPromotions
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string StartDate { get; set; }
        public string ExpirationDate { get; set; }
        public string Size { get; set; }
    }

    public class Promotions
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string StartDate { get; set; }
        public string ExpirationDate { get; set; }
        public string Size { get; set; }
    }
}