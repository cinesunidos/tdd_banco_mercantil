﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class RedisKey
    {       
        public string Id { get; set; }     
        public string Value { get; set; }        
        public string Description { get; set; }
    }
}