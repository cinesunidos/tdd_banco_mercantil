﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace admin_usuarios.Models
{
    [DataContract]
    public class PackViewModel
    {
        [DataMember]
        public List<TransferItem> items { get; set; }
        [DataMember]
        public string cinemaId { get; set; }
    }
}