﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace admin_usuarios.Models
{
    public class Role
    {
        public System.Guid roleId { get; set; }
        public string name { get; set; }
    }
}