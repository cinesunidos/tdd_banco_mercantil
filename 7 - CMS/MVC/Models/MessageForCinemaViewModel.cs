﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class MessageForCinemaViewModel
    {
        public List<MessageForCinema> Messages { get; set; }
        public MessageForCinema Message { get; set; }

        public MessageForCinemaViewModel()
        {
            this.Message = new MessageForCinema();
            this.Messages = new List<MessageForCinema>();
        }


    }
}