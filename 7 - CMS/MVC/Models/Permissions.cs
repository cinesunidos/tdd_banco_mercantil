﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace admin_usuarios.Models
{
    public class Permissions
    {
        public System.Guid id { get; set; }

        public string Name { get; set; }      
    }
}