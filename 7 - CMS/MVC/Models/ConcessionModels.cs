﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class ConcessionSetting
    {
        
        public string Key { get; set; }

        public string Value { get; set; }

    }

    public class ModalViewModel
    {
        public string ImageUrl { get; set; }
    }

    public class ConcessionTransactionResponse
    {
        public List<ConcessionTransactionSummary> Summary;
        public List<ConcessionTransaction> Transactions;
        public decimal TotalSales { get; set; }
    }

    public class ConcessionTransactionSummary
    {
        public string ItemId { get; set; }

        public string ItemDescription { get; set; }

        public decimal ItemValue { get; set; }

        public int ItemQuantity { get; set; }

        public decimal SubTotal { get; set; }
    }

    public class ConcessionTransaction
    {
        public string TransactionId { get; set; }
        public string TransactionType { get; set; }
        public string TransactionStatus { get; set; }
        public string ItemId { get; set; }
        public string ItemDescription { get; set; }
        public decimal ItemValue { get; set; }
        public int ItemQuantity { get; set; }
        public decimal SubTotal { get; set; }
        public string Vendor { get; set; }
        public string LocationDescription { get; set; }
        public string Workstation { get; set; }
        public string UserName { get; set; }
        public string FilmTitle { get; set; }
        public DateTime FilmDate { get; set; }
        public DateTime CollectionDate { get; set; }
        public String PickupWorkstation { get; set; }
        public int PickupUserId { get; set; }
        public String PickupUser { get; set; }
        public bool IsCollected
        {
            get
            {
                return CollectionDate.Year == 9999 ? false : true;
            }
        }

    }

    public class ReporteViewModel
    {
        public DateTime Date { get; set; }

        public List<ConcessionTransaction> Transactions { get; set; }

        public List<ConcessionTransactionSummary> Summary { get; set; }

        public decimal TotalSales { get; set; }

        public string Param { get; set; }
    }

    public class ConcessionItem
    {
        public string ItemId { get; set; }
        public string ItemDescription { get; set; }
        public string ItemHOPK { get; set; }
        public decimal ItemPrice { get; set; }
    }

    public class ConcessionCatalog
    {
        public List<ConcessionItem> Products { get; set; }
    }

    public class ConcessionCatalogViewModel
    {
        public ConcessionCatalog Catalog;
    }

    public class ConcessionTransactionByDateF
    {
        public string TransactionId { get; set; }
        public string TransactionType { get; set; }
        public string TransactionStatus { get; set; }
        public string ItemId { get; set; }
        public string ItemDescription { get; set; }
        public int ItemQuantity { get; set; }
        public decimal ItemValue { get; set; }
        public decimal SubTotal { get; set; }
        public string Vendor { get; set; }
        public string LocationDescription { get; set; }
        public string Workstation { get; set; }
        public string UserName { get; set; }
        public string FilmTitle { get; set; }
        public DateTime FilmDate { get; set; }
        public DateTime CollectionDate { get; set; }
        public String PickupWorkstation { get; set; }
        public int PickupUserId { get; set; }
        public String PickupUser { get; set; }
        public String Pickup { get; set; }
    }

    public class ConcessionTransactionByDateC
    {
        public string ItemId { get; set; }
        public string ItemDescription { get; set; }
        public int ItemQuantity { get; set; }
        public String PickupWorkstation { get; set; }
        public String PickupUser { get; set; }
        public String Componentes { get; set; }
        public Double Cantidad_Componentes { get; set; }
        public Double Total_Cantidad_Componentes { get; set; }
    }
}