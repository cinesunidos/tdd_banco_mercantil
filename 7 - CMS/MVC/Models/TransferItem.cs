﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class TransferItem
    {
        public string itemDescription { get; set; }
        public string itemCode { get; set; }
        public string UnidadDeMedida { get; set; }
        public string itemType { get; set; }
        public int Quantity { get; set; }
        public bool Quitar { get; set; }

        public static implicit operator TransferItem(ReverseViewModel r)
        {
            TransferItem t = new TransferItem
            {
                itemCode = r.itemCode,
                itemDescription = r.itemDescription,
                itemType = r.itemType,
                Quantity = r.Quantity,
                Quitar = false,
                UnidadDeMedida = r.UnidadDeMedida
            };
            return t;
        }
    }
}