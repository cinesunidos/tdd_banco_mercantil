﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin_usuarios.Models
{
    public class TransferViewModel
    {
        public List<TransferItem> ItemsForSelect { get; set; }

        public List<TransferItem> ItemsSelected { get; set; }

        public TransferViewModel()
        {
            ItemsForSelect = new List<TransferItem>();
            ItemsSelected = new List<TransferItem>();
        }
    }
}