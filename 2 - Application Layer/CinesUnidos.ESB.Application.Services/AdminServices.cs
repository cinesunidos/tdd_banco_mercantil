﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;

namespace CinesUnidos.ESB.Application.Services
{
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class AdminServices : Services, IAdmin
    {
        #region Attributes

        private IAdminDomainContract m_adminDomainContract;

        #endregion Attributes

        #region Contructors

        public AdminServices(IAdminDomainContract adminDomainContract)
            : base()
        {
            m_adminDomainContract = adminDomainContract;
        }

        #endregion Contructors

        #region Methods

        public UserEntity Login(UserEntity usr)
        {
            return m_adminDomainContract.Login(usr);
        }

        #endregion Methods
    }
}