﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using System;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{
    public class UserSessionServices : Services, IUserSession
    {
        #region Attributes

        private IUserSessionDomainContract m_userSessionDomainContract;
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();

        #endregion Attributes

        #region Contructors

        public UserSessionServices(IUserSessionDomainContract userSessionDomainContract)
            : base()
        {
            m_userSessionDomainContract = userSessionDomainContract;
        }

        #endregion Contructors

        #region Methods

        public UserSessionResponseEntity Read(string userSessionId, string readMap, string token)
        {
            if (token == TokenServices)
            {
                m_userSessionDomainContract.Configuration = this.Configuration;
                Guid id = new Guid(userSessionId);
                return m_userSessionDomainContract.Read(id, readMap);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Se emplea desde las applicaciones moviles, recibe el sesion ID que es creado en Redis y es el que se
        /// crea por la app movil. Retorna un objeto Json con el valor de la seleccion del usuario.
        /// </summary>
        /// <param name="userSessionId"></param>
        /// <returns></returns>
    
        public UserSessionResponseMobileEntity ReadMobile(string usessionID)
        {
                       
            m_userSessionDomainContract.Configuration = this.Configuration;
            var id = new Guid(usessionID);
            return m_userSessionDomainContract.ReadMobile(id);
        }

        public UserSessionResponseEntity Create(Domain.BasketEntity userSession)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            return m_userSessionDomainContract.Create(userSession);
        }


        public void Delete(string userSessionId, string token)
        {
            if (token == TokenServices)
            {
                m_userSessionDomainContract.Configuration = this.Configuration;
                Guid id = new Guid(userSessionId);
                m_userSessionDomainContract.Delete(id);
            }
            else
            {
                return;
            }
        }

        public ConfirmationResponseEntity Confirmation(Domain.ConfirmationEntity userSession)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            return m_userSessionDomainContract.Confirmation(userSession);
        }

        public ConfirmationResponseEntity ConfirmationMobile(Domain.ConfirmationEntity userSession)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            return m_userSessionDomainContract.ConfirmationMobile(userSession);
        }

        public PaymentResponseEntity Payment(Domain.PaymentEntity payment)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionDomainContract.Payment(payment);
            return paymentResponse;
        }
        public PaymentResponseEntity PaymentMobile(Domain.PaymentEntity payment)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionDomainContract.PaymentMobile(payment);
            return paymentResponse;
        }

        public UserSessionEntity Get(string userSessionId)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            Guid id = new Guid(userSessionId);
            return m_userSessionDomainContract.Get(id);
        }

        public Boolean CancelOrder(string UserSessionId)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            return m_userSessionDomainContract.CancelOrder(UserSessionId);
        }

        public UserSessionEntity DeleteSession(string SessionId, string token)
        {
            UserSessionEntity user = new UserSessionEntity();
            //Debe coincidir con el token que se encuentra en la capa de servicios
            string tok = System.Configuration.ConfigurationManager.AppSettings["TokenCMS"].ToString();
            if (token == tok)
            {
                m_userSessionDomainContract.Configuration = this.Configuration;
                return m_userSessionDomainContract.DeleteSession(SessionId);
            }
            else
            {
                user.Title = "SUCCESS";
                return user;
            }
            
        }

        public PaymentResponseEntity PaymentCandies(PaymentEntity payment)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionDomainContract.PaymentCandies(payment);
            return paymentResponse;
        }

        public AVippoResponse GetVippoConfirmation(APaymentRequest Payment)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            AVippoResponse paymentResponse = m_userSessionDomainContract.GetVippoConfirmation(Payment);
            return paymentResponse;
        }

        public string BDVGeneratePayment(BDVUserInfo id)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            return m_userSessionDomainContract.BDVGeneratePayment(id);
        }

        public PaymentResponseEntity PaymentAuthMercantil(PaymentEntity payment)
        {
            m_userSessionDomainContract.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionDomainContract.PaymentAuthMercantil(payment);
            return paymentResponse;
        }

        #endregion Methods
    }
}