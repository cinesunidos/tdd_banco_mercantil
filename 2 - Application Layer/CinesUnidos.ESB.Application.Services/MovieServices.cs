﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace CinesUnidos.ESB.Application.Services
{
    public class MovieServices : Services, IMovie
    {
        #region Attributes
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();
        private IMovieDomainContract m_movieDomainServices;
        private IBlobsDomainContract m_storageDomainContract;

        #endregion Attributes

        #region Contructors

        public MovieServices(IMovieDomainContract movieDomainServices, IBlobsDomainContract storageDomainContract)
        {
            m_movieDomainServices = movieDomainServices;
            m_storageDomainContract = storageDomainContract;
        }

        #endregion Contructors

        #region Methods

        private HomeEntity GetXML()
        {
            //string xml;
            Serializer s = new Serializer();
            string sliderXML = string.Empty;
//#if DEBUG
            //MemoryStream stream = m_storageDomainContract.SearchFile("Home2.xml", "files");
            string ruta = HostingEnvironment.MapPath("~/App_Data/Home.xml");
            if (!File.Exists(ruta))
            {
                WebClient client = new WebClient();
                client.DownloadFile("http://az693035.vo.msecnd.net/files/Home.xml", ruta);
            }
            sliderXML = File.ReadAllText(ruta);

//#else
//            // MemoryStream stream = m_storageDomainContract.SearchFile("Home.xml", "files");

//            string sliderPath = Path.Combine(RoleEnvironment.GetLocalResource("AllMoviesLocal").RootPath, "Home.xml");
//            if (!File.Exists(sliderPath))
//            {
//                WebClient client = new WebClient();
//                client.DownloadFile("http://az693035.vo.msecnd.net/files/Home.xml", sliderPath);
//            }
//            sliderXML = File.ReadAllText(sliderPath);

//#endif
            //  xml = Encoding.UTF8.GetString(stream.ToArray());
            //return (HomeEntity)DogFramework.Xml.XmlAdapter.DeserializeFromMemory(xml, typeof(HomeEntity));
            var home = (HomeEntity)s.DeserializeFromMemory(sliderXML, typeof(HomeEntity));
            return home;
        }

        public void Clear(string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainServices.Configuration = this.Configuration;
                m_movieDomainServices.Clear();
            }
            else
            {
                return;
            }
        }

        public MovieBillboardEntity GetById(string city, string movieId, string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainServices.Configuration = this.Configuration;
                return m_movieDomainServices.GetById(city, movieId);
            }
            else
            {
                return null;
            }
        }

        public MovieBillboardEntity[] GetMovieAll(string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainServices.Configuration = this.Configuration;
                return m_movieDomainServices.GetMovieAll();
            }
            else
            {
                return null;
            }
        }

        public MovieBillboardEntity[] GetMovieTheaterList(List<string> cines, string day, string month)
        {
            //Luis Ramírez 17/04/2017, obtener fecha actual del servidor
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("SA Western Standard Time");
            DateTime dateTimeNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
            int iday, imonth;
            //Luis Ramírez 17/04/2017, Si el día y mes vienen vacios, asignar el día actual y convertir a entero; caso contrario, convertir a entero
            if (string.IsNullOrEmpty(day)) iday = dateTimeNow.Day; else int.TryParse(day, out iday);
            if (string.IsNullOrEmpty(month)) imonth = dateTimeNow.Month; else int.TryParse(month, out imonth);
            return m_movieDomainServices.GetMovieTheaterList(cines, iday, imonth);
        }

        public MovieBillboardEntity GetByIdDate(string city, string movieId, string day, string month, string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainServices.Configuration = this.Configuration;
                return m_movieDomainServices.GetByIdDate(city, movieId, int.Parse(day), int.Parse(month));
            }
            else
            {
                return null;
            }
        }

        public MovieBillboardEntity[] GetByCity(string city, string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainServices.Configuration = this.Configuration;
                return m_movieDomainServices.GetByCity(city);
            }
            else
            {
                return null;
            }
        }

        public MovieBillboardEntity[] GetByCityDate(string city, string day, string month, string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainServices.Configuration = this.Configuration;
                var r = m_movieDomainServices.GetByCityDate(city, int.Parse(day), int.Parse(month));
                r = r.OrderByDescending(m => m.Movie.FirstExhibit).ThenBy(m => m.Movie.Id).ToArray();
                return r;
                //m_movieDomainServices.Configuration = this.Configuration;
                //return m_movieDomainServices.GetByCityDate(city, int.Parse(day), int.Parse(month));
            }
            else
            {
                return null;
            }
        }

        public MovieBillboardEntity[] GetSlider(string city, string token)
        {
            if (token != TokenServices)
            {
                return null;
            }

            m_movieDomainServices.Configuration = this.Configuration;
            HomeEntity home = GetXML();
            if (home.Slider.Where(c => c.City.Equals(city)).Count() == 0)
            {
                throw new ApplicationException("El Slider no esta configurado para la ciudad de " + city);
            }

            string[] ids = home.Slider.Where(c => c.City.Equals(city)).First().Hos;
            List<MovieBillboardEntity> movies = new List<MovieBillboardEntity>();

            foreach (var item in ids) //este bucle tiene 4 items definidos en el archivo Home.xml
            {
                MovieBillboardEntity movie = GetById(city, item, TokenServices);
                if (movie.Movie != null)
                    movies.Add(movie);
            }


            //aqui no se puede usar parallel ya que hace llamado a servicios y emplota en la clase services 
            //Parallel.ForEach(ids, item =>
            //{
            //    MovieBillboardEntity movie = GetById(city, item, TokenServices);
            //    if (movie.Movie != null)
            //    {
            //        movies.Add(movie);
            //    }

            //});

            if (movies.Count.Equals(0))
            {
                throw new ApplicationException("El Slider no tiene funciones activas para la ciudad de " + city);
            }

            return movies.ToArray();
        }

        public MovieEntity[] GetBar(string city, string token)
        {
            if (token != TokenServices)
            {
                return null;
            }

            m_movieDomainServices.Configuration = this.Configuration;
            HomeEntity home = GetXML();
            if (home.BarMovie.Where(c => c.City.Equals(city)).Count() == 0)
            {
                throw new ApplicationException("La Barra de Peliculas no esta configurada para la ciudad de " + city);
            }
            string[] ids = home.BarMovie.Where(c => c.City.Equals(city)).First().Hos;
            List<MovieBillboardEntity> mBillboardList = m_movieDomainServices.GetAll().ToList();
            List<MovieEntity> movies = new List<MovieEntity>();

            #region cambio a parallel
            //foreach (var item in ids)
            //{
            //    MovieBillboardEntity movieBillboard = mBillboardList.Where(m => m.Movie.Id.Equals(item)).FirstOrDefault();

            //    if (movieBillboard != null)
            //    {
            //        MovieEntity movie = movieBillboard.Movie;
            //        movies.Add(movie);
            //    }
            //}
            #endregion

            Parallel.ForEach(ids, item =>
            {
                MovieBillboardEntity movieBillboard = mBillboardList.Where(m => m.Movie.Id.Equals(item)).FirstOrDefault();
                if (movieBillboard != null)
                {
                    MovieEntity movie = movieBillboard.Movie;
                    movies.Add(movie);
                }
            });

            if (movies.Count.Equals(0))
            {
                throw new ApplicationException("La Barra de Peliculas no tiene funciones activas para la ciudad de " + city);
            }
            return movies.ToArray();
        }

        public MovieEntity[] GetMoreMovie(string city, string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainServices.Configuration = this.Configuration;
                HomeEntity home = GetXML();
                if (home.MoreMovie.Where(c => c.City.Equals(city)).Count() == 0)
                {
                    throw new ApplicationException("El Catalogo de Peliculas no esta configurado para la ciudad de " + city);
                }
                string[] ids = home.MoreMovie.Where(c => c.City.Equals(city)).First().Hos;
                List<MovieBillboardEntity> mBillboardList = m_movieDomainServices.GetAll().ToList();
                List<MovieEntity> movies = new List<MovieEntity>();

                #region Cambio por parallel
                //foreach (var item in ids)
                //{
                //    MovieBillboardEntity movieBillboard = mBillboardList.Where(m => m.Movie.Id.Equals(item)).FirstOrDefault();
                //    if (movieBillboard != null)
                //    {
                //        MovieEntity movie = movieBillboard.Movie;
                //        if (movie != null)
                //            movies.Add(movie);
                //    }
                //}
                #endregion

                Parallel.ForEach(ids, item =>
                {
                    MovieBillboardEntity movieBillboard = mBillboardList.Where(m => m.Movie.Id.Equals(item)).FirstOrDefault();
                    if (movieBillboard != null)
                    {
                        MovieEntity movie = movieBillboard.Movie;
                        if (movie != null)
                            movies.Add(movie);
                    }
                });

                if (movies.Count.Equals(0))
                {
                    throw new ApplicationException("El Catalogo de Peliculas no tiene funciones activas para la ciudad de " + city);
                }
                return movies.ToArray();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Obtener Peliculas por lista de cines
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public MovieBillboardEntity[] GetAllMoviesByTheaterList(List<string> cines, string day, string month)
        {
            int iday, imonth;
            int.TryParse(day, out iday);
            int.TryParse(month, out imonth);
            return m_movieDomainServices.GetAllMoviesByTheaterList(cines, iday, imonth);
        }

        #endregion Methods
    }
}