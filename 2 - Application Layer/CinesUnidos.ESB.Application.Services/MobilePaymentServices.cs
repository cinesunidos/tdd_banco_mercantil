﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using System;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{
    public class MobilePaymentServices : Services, IMobilePayment
    {
        #region Attributes
        private IMobilePaymentDomainContract m_mobilepaymentDomainContract;
        #endregion Attributes

        #region Constructors    

        public MobilePaymentServices(IMobilePaymentDomainContract MobilePaymentServices) : base()
        {
            m_mobilepaymentDomainContract = MobilePaymentServices;
        }
        #endregion Contructors

        #region Methods
       
        #endregion Methods
    }
}
