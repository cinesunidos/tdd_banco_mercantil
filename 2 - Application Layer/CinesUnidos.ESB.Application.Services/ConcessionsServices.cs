﻿using CinesUnidos.ESB.Application.Contracts;
using System;
using System.Collections.Generic;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{

    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Clase que implementa los metodos de IConcessions
    /// </summary>

    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class ConcessionsServices : Services, IConcessions
    {

        #region Attributes

        private IConcessionsDomainContract m_concessionsDomainContract;

        #endregion

        #region Contructors
        public ConcessionsServices(IConcessionsDomainContract concessionsDomainContract)
            : base()
        {
            m_concessionsDomainContract = concessionsDomainContract;
        }
        #endregion

        #region Methods

        public ConcessionListEntity GetTheaterConcessions(string theaterId, string token)
        {
            if (token == ConfigurationManager.AppSettings["TokenServices"].ToString())
            {
                return m_concessionsDomainContract.GetTheaterConcessions(theaterId);
            }
            else
            {
                ConcessionListEntity c = new ConcessionListEntity();
                return c;
            }
        }

        public ConcessionsRSEntity AddItemsToCart(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsDomainContract.AddItemsToCart(concessionsRq);
        }

        public ConcessionsRSEntity PurchaseConcessions(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsDomainContract.PurchaseConcessions(concessionsRq);
        }

        public ConcessionsRSEntity CancelConcessionsOrder(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsDomainContract.CancelConcessionsOrder(concessionsRq);
        }

        public MessageEntity ModifyModalConcessions(MessagePack ListModal)
        {
            if (ListModal.Token == ConfigurationManager.AppSettings["TokenServices"].ToString())
            {
                return m_concessionsDomainContract.ModifyModalConcessions();
            }
            else
            {
                return new MessageEntity()
                {
                    Code = -1,
                    Message = "todo bien :)",
                };
            }
        }

        public ConcessionListEntity GetCandiesWeb(string theaterId, string token)
        {
            if (token == ConfigurationManager.AppSettings["TokenServices"].ToString())
            {
                return m_concessionsDomainContract.GetCandiesWeb(theaterId);
            }
            else
            {
                ConcessionListEntity c = new ConcessionListEntity();
                return c;
            }
        }

        public ConcessionsRSEntity AddItemsCandiesWeb(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsDomainContract.AddItemsCandiesWeb(concessionsRq);
        }

        public TransferPack UpdateCandiesTable(TransferPack items)
        {
            if (items.Token == ConfigurationManager.AppSettings["TokenServices"].ToString())
            {
                return m_concessionsDomainContract.UpdateCandiesTable(items);
            }
            else
            {
                return null;
            }
        }

        public TransferPack DecreaseCandiesTable(TransferPack items)
        {
            if (items.Token == ConfigurationManager.AppSettings["TokenServices"].ToString())
            {
                return m_concessionsDomainContract.DecreaseCandiesTable(items);
            }
            else
            {
                return null;
            }
        }

        //public string PromoPepsi(string Codigo, string token)
        //{
        //    if (token.Equals(ConfigurationManager.AppSettings["TokenPepsi"]))
        //    {
        //        return m_concessionsDomainContract.PromoPepsi(Codigo);
        //    }
        //    return string.Empty;
        //}

        public PromoPack PromoDesorden(PromoPack Pack)
        {
            try
            {
                if (Pack.Token.Equals(ConfigurationManager.AppSettings["TokenServices"]))
                {
                    return m_concessionsDomainContract.PromoDesorden(Pack);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception Ex)
            {
                Pack.Mensaje = Ex.Message;
                return Pack;
            }
        }

        #endregion
    }
}
