﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;
using System;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class SecurityServices : Services, ISecurity
    {
        #region Attributes
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();
        private ISecurityDomainContract m_securityDomainContract;

        #endregion Attributes

        #region Contructors

        public SecurityServices(ISecurityDomainContract securityDomainContract)
            : base()
        {
            m_securityDomainContract = securityDomainContract;
        }

        #endregion Contructors

        private string ReconvertURLString(string param)
        {
            string Results = param;
            Results = Results.Replace("%25", "%");
            Results = Results.Replace("%3C", "<");
            Results = Results.Replace("%3E", ">");
            Results = Results.Replace("%23", "#");
            Results = Results.Replace("%7B", "{");
            Results = Results.Replace("%7D", "}");
            Results = Results.Replace("%7C", "|");
            Results = Results.Replace("%5C", "'\'");
            Results = Results.Replace("%5E", "^");
            Results = Results.Replace("%7E", "~");
            Results = Results.Replace("%5B", "[");
            Results = Results.Replace("%5D", "]");
            Results = Results.Replace("%60", "`");
            Results = Results.Replace("%3B", ";");
            Results = Results.Replace("%2F", "/");
            Results = Results.Replace("%3F", "?");
            Results = Results.Replace("%3A", ":");
            Results = Results.Replace("%40", "@");
            //Results = Results.Replac, "%3De("");
            Results = Results.Replace("%26", "&");
            Results = Results.Replace("%24", "$");
            Results = Results.Replace("%2B", "+");

            return Results;
        }

        #region Methods

        public Domain.TokenEntity SignIn(SignInEntity login)
        {
            if (!string.IsNullOrEmpty(login.Email) && !string.IsNullOrEmpty(login.Password))
                return m_securityDomainContract.SignIn(login);
            else
            {
                throw new ApplicationException("Login y Password son requeridos");
            }
        }

        public void Signout(string token)
        {
            m_securityDomainContract.Signout(token);
        }

        public bool Valid(string token)
        {
            return m_securityDomainContract.Valid(token);
        }

        public Domain.TokenEntity Refresh(string token)
        {
            return m_securityDomainContract.Refresh(token);
        }

        public UserEntity ReadUser(string id, string tokenServices)
        {
            if (tokenServices == TokenServices)
            {
                return m_securityDomainContract.Read(Guid.Parse(id));
            }
            else
            {
                return null;
            }
        }

        public UserEntity ReadUserByHash(string hash)
        {
            return m_securityDomainContract.ReadByHash(hash);

        }

        public ResponseEntity CreateUser(UserEntity userEntity)
        {
            return m_securityDomainContract.Create(userEntity);
        }

        public ResponseEntity UpdateUser(UserEntity userEntity)
        {
            return m_securityDomainContract.Update(userEntity);
        }

        public string RememberEmail(string idCard, string day, string month, string year, string tokenServices)
        {
            if (tokenServices == TokenServices)
            {
                try
                {
                    DateTime date = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
                    return m_securityDomainContract.RememberEmail(idCard, date);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Formato de fecha no valido", ex);
                }
            }
            else
            {
                return "Processed";
            }
        }

        public UserEntity RememberPassword(string email, string tokenServices)
        {
            if (tokenServices == TokenServices)
            {
                UserEntity user = m_securityDomainContract.RememberPassword(email);
                return user;
            }
            else
            {
                return null;
            }
        }

        public string VerifiedAnswer(string email, string answer, string tokenServices)
        {
            if (tokenServices == TokenServices)
            {
                return m_securityDomainContract.VerifiedAnswer(email, answer).ToString();
            }
            else
            {
                return "Processed";
            }
        }

        public string VerifiedEmailAndIdCard(string email, string idCard, string tokenServices)
        {
            if (tokenServices == TokenServices)
            {
                return m_securityDomainContract.VerifiedEmailAndIdCard(email, idCard).ToString();

            }
            else
            {
                return "Processed";
            }
        }

        public ResponseEntity ChangePassword(PasswordEntity passwordEntity)
        {
            return m_securityDomainContract.ChangePassword(passwordEntity);
        }

        public ResponseEntity ActivateUser(string userId)
        {
            return m_securityDomainContract.ActivateUser(Guid.Parse(userId));
        }

        public TokenInfoEntity Info(string token)
        {
            return m_securityDomainContract.GetTokenInfo(token);
        }
        
        public ResponseEntity CreateUserMobile(UserEntity userEntity)
        {
            return m_securityDomainContract.CreateUserMobile(userEntity);
        }
        
        public string RemoveList(string userEntity)
        {
            return this.m_securityDomainContract.RemoveList(userEntity).MsgResponse;
        }

        public DatosBoleto Passvalidation(DatosBoleto datosBoleto)
        {
            if (datosBoleto.token == TokenServices)
            {
                return m_securityDomainContract.Passvalidation(datosBoleto);
            }
            else
            {
                return null;
            }
        }
        #endregion Methods
    }
}