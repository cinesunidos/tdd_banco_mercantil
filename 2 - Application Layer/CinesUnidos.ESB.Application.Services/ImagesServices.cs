﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain.Contracts;
using System.Configuration;
using System.IO;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Services
{
    public class ImagesServices : IImages
    {
        #region Attributes
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();
        private IBlobsDomainContract m_storageDomainContract;
        #endregion Attributes

        #region Contructors

        public ImagesServices(IBlobsDomainContract storageDomainContract)
        {
            m_storageDomainContract = storageDomainContract;
        }

        #endregion Contructors

        #region Methods

        public Stream GetSlider(string ho, string token)
        {
            if (token == TokenServices)
            {
                MemoryStream memoryStream;
                string fileName = string.Format("{0}.jpg", ho.ToUpper());
                memoryStream = m_storageDomainContract.SearchFile(fileName, "slider");
                WebOperationContext.Current.OutgoingResponse.ContentType = "image/gif";
                memoryStream.Position = 0L;
                return memoryStream;
            }
            else
            {
                return null;
            }
        }

        public Stream GetPoster(string ho, string token)
        {
            if (token == TokenServices)
            {
                MemoryStream memoryStream;
                string fileName = string.Format("{0}.jpg", ho.ToUpper());
                memoryStream = m_storageDomainContract.SearchFile(fileName, "poster");
                WebOperationContext.Current.OutgoingResponse.ContentType = "image/gif";
                memoryStream.Position = 0L;
                return memoryStream;
            }
            else
            {
                return null;
            }
        }

        #endregion Methods
    }
}