﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class PurchasesServices : Services, IPurchases
    {
        #region Attributes

        private IPurchasesDomainContract m_pDomainContract;
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();

        #endregion Attributes

        #region Contructors

        public PurchasesServices(IPurchasesDomainContract pDomainContract)
            : base()
        {
            m_pDomainContract = pDomainContract;
        }

        #endregion Contructors

        #region Methods

        public PurchasesEntity[] GetAll(string usr, string token)
        {
            if (token == TokenServices)
            {
                return m_pDomainContract.GetAll(usr);
            }
            else
            {
                return null;
            }
        }

        public string Register(PurchasesEntity ent)
        {
            return m_pDomainContract.Register(ent);
        }

        #endregion Methods
    }
}