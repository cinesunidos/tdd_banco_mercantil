﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using System.Configuration;
using System.Linq;

namespace CinesUnidos.ESB.Application.Services
{
    public class TheaterServices : Services, ITheater
    {
        #region Attributes

        private ITheaterDomainContract m_theaterDomainContract;
        private IMovieDomainContract m_movieDomainContract;
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();

        #endregion Attributes

        #region Contructors

        public TheaterServices(ITheaterDomainContract theaterDomainContract, IMovieDomainContract movieDomainContract)
            : base()
        {
            m_theaterDomainContract = theaterDomainContract;
            m_movieDomainContract = movieDomainContract;
        }

        #endregion Contructors

        #region Methods

        public void Clear(string token)
        {
            if (token == TokenServices)
            {
                m_theaterDomainContract.Configuration = this.Configuration;
                m_theaterDomainContract.Clear();
            }
            else
            {
                return;
            }
        }

        public Domain.TheaterEntity[] GetAll(string token)
        {
            if (token == TokenServices)
            {
                m_theaterDomainContract.Configuration = this.Configuration;
                return m_theaterDomainContract.GetAll();
            }
            else
            {
                return null;
            }
        }

        public TheaterBillboardEntity GetById(string theaterId, string token)
        {
            if (token == TokenServices)
            {
                m_theaterDomainContract.Configuration = this.Configuration;
                return m_theaterDomainContract.GetById(theaterId);
            }
            else
            {
                return null;
            }
        }

        public TheaterBillboardEntity GetByIdDate(string theaterId, string day, string month, string token)
        {
            if (token == TokenServices)
            {
                m_theaterDomainContract.Configuration = this.Configuration;
                return m_theaterDomainContract.GetById(theaterId, int.Parse(day), int.Parse(month));
            }
            else
            {
                return null;
            }
        }

        public string[] GetAllCity(string token)
        {
            if (token == TokenServices)
            {
                m_theaterDomainContract.Configuration = this.Configuration;
                return m_theaterDomainContract.GetAll().Select(t => t.City).Distinct().OrderBy(s => s).ToArray();
            }
            else
            {
                return null;
            }
        }

        public TheaterBillboardEntity[] GetByCity(string city, string token)
        {
            if (token == TokenServices)
            {
                m_theaterDomainContract.Configuration = this.Configuration;
                return m_theaterDomainContract.GetByCity(city);
            }
            else
            {
                return null;
            }
        }

        public TheaterBillboardEntity[] GetByCityDate(string city, string day, string month, string token)
        {
            if (token == TokenServices)
            {
                m_theaterDomainContract.Configuration = this.Configuration;
                return m_theaterDomainContract.GetByCity(city, int.Parse(day), int.Parse(month));
            }
            else
            {
                return null;
            }
        }

        public SessionInfoEntity GetSession(string theaterId, string sessionId, string token)
        {
            if (token == TokenServices)
            {
                m_movieDomainContract.Configuration = this.Configuration;
                return m_movieDomainContract.GetSession(theaterId, int.Parse(sessionId));
            }
            else
            {
                return null;
            }
        }
                
        #endregion Methods
    }
}