﻿using CinesUnidos.ESB.Domain;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Services
{
    public class Services
    {
        public ConfigurationEntity Configuration
        {
            get
            {
                IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
                string clientID = request.Headers["ClientID"];
                ConfigurationEntity configuration = new ConfigurationEntity();
                configuration.Values.Add("ClientID", clientID);
                return configuration;
            }
        }
    }
}