﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Application.Services
{
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class RedisCacheManagementServices : Services, IRedisCacheManagement

    {
        #region Attributes

        // private IRedisCacheManagementDomainContract m_redisCachemanagementDomainContract;
        private IRedisCacheManagementDomainContract m_redisCachemanagementDomainContract;
        String token = System.Configuration.ConfigurationManager.AppSettings["TokenCMS"].ToString();


        #endregion Attributes

        #region Contructors

        public RedisCacheManagementServices(IRedisCacheManagementDomainContract redisChacheManagementDomainContract)
            : base()
        {
            m_redisCachemanagementDomainContract = redisChacheManagementDomainContract;
        }
        #endregion Contructors

        #region Methods
        public string DeleteCacheAll(string tokenId)
        {
            if (!String.Equals(token, tokenId))
                return ";)";



            m_redisCachemanagementDomainContract.Configuration = this.Configuration;
            return m_redisCachemanagementDomainContract.DeleteCacheAll();
        }

        public string DeleteCacheNoTicketsPrice(string tokenId)
        {
            if (!String.Equals(token, tokenId))
                return ";)";

            m_redisCachemanagementDomainContract.Configuration = this.Configuration;
            return m_redisCachemanagementDomainContract.DeleteCacheNoTicketsPrice();
        }

        public string DeleteCacheTicketsPrice(string tokenId)
        {
            if (!String.Equals(token, tokenId))
                return ";)";

            m_redisCachemanagementDomainContract.Configuration = this.Configuration;
            return m_redisCachemanagementDomainContract.DeleteCacheTicketsPrice();
        }

        public string ReadDateCacheTicketsPrice(string tokenId)
        {
            if (!String.Equals(token, tokenId))
                return ";)";

            m_redisCachemanagementDomainContract.Configuration = this.Configuration;
            return m_redisCachemanagementDomainContract.ReadDateCacheTicketsPrice();
        }
        public string DeletePremiers(string tokenId)
        {
            if (!String.Equals(token, tokenId))
                return ";)";

            m_redisCachemanagementDomainContract.Configuration = this.Configuration;
            return m_redisCachemanagementDomainContract.DeletePremiers();
        }

        public string ReadDateCacheTicketsPrice()
        {
            throw new NotImplementedException();
        }

        public string GetPathLocalStorage(string tokenId)
        {
            if (!String.Equals(token, tokenId))
                return ";)";

            m_redisCachemanagementDomainContract.Configuration = this.Configuration;
            return m_redisCachemanagementDomainContract.GetPathLocalStorage();

        }

        #endregion Methods
    }
}
