﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;

namespace CinesUnidos.ESB.Application.Services
{
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class ContactServices : Services, IContact
    {
        #region Attributes

        private IContactDomainContract m_contactDomainContract;

        #endregion Attributes

        #region Contructors

        public ContactServices(IContactDomainContract contactDomainContract)
            : base()
        {
            m_contactDomainContract = contactDomainContract;
        }

        #endregion Contructors

        #region Methods

        public ResponseEntity SendComplaint(ComplaintEntity complaint)
        {
            return m_contactDomainContract.SendComplaint(complaint);
        }

        #endregion Methods
    }
}