﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;
using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Application.Services
{
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class AppMobileServices : Services, IAppMobile
    {
        #region Attributes

        private IAppMobileDomainContract m_appMobileDomainContract;

        #endregion Attributes

        #region Contructors

        public AppMobileServices(IAppMobileDomainContract appMobileDomainContract)
            : base()
        {
            m_appMobileDomainContract = appMobileDomainContract;
        }

        #endregion Contructors

        #region Methods

        public List<SectionEntity> GetHome(string platform)
        {
            if (!string.IsNullOrEmpty(platform))
                return m_appMobileDomainContract.GetHome(platform);
            else
            {
                throw new ApplicationException("La plataforma es requeridos");
            }
        }

        public TheaterMobileEntity[] GetAllTheatersMobile()
        {
            m_appMobileDomainContract.Configuration = this.Configuration;
            return m_appMobileDomainContract.GetAllTheatersMobile();
        }

        public string GetAllTheatersMobile2()
        {
            return "Processes";
        }
        #endregion Methods
    }
}