﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using System.Collections.Generic;
using System;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{
    public class PromotionServices: Services, IPromotion
    {
        #region Attributes
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();
        private IPromotionDomainContract m_promotionDomainContract;
        #endregion Attributes

        #region Constructors    

        public PromotionServices(IPromotionDomainContract PromotionDomainServices) : base()
        {
            m_promotionDomainContract = PromotionDomainServices;
        }
        #endregion Contructors

        #region Methods

        public ResultEntity PublishPromotion(PromotionPackageEntity PromotionList)
        {
            ResultEntity result = new ResultEntity();
            PromotionPackageEntity promo = new PromotionPackageEntity();

            promo = PromotionList;
            //m_promotionDomainContract.Configuration = this.Configuration;
            string token = PromotionList.Token;
            string tokenweb = System.Configuration.ConfigurationManager.AppSettings["TokenCMS"].ToString();
            if (!tokenweb.Equals(token))
                if (!PromotionList.Token.Equals(tokenweb))
                {
                    result.number = 1;
                    result.message = ":)";
                    return result;
                }
                else
                {
                    result.number = -1;
                    result.message = ":(";
                    return result;
                }
            return m_promotionDomainContract.PublishPromotion(PromotionList);
        }

        public string GetPromotions(string token)
        {
            if (token == TokenServices)
            {
                return m_promotionDomainContract.GetPromotions();
            }
            else
            {
                return "Processed";
            }
        }

        public string GetPromotionsDB(string token)
        {
            if (token == TokenServices)
            {
                return m_promotionDomainContract.GetPromotionsDB();

            }
            else
            {
                return "Processed";
            }
        }
        #endregion Methods

    }
}
