﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Application.Services
{
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class TicketServices : Services, ITicket
    {
        #region Attributes

        private ITicketPriceDomainContract m_ticketPricesDomainContract;
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();

        #endregion Attributes

        #region Contructors

        public TicketServices(ITicketPriceDomainContract ticketPricesDomainContract)
            : base()
        {
            m_ticketPricesDomainContract = ticketPricesDomainContract;
        }

        public InfoVoucher GetInfoVoucher(string token, string cinemaid, string sessionid, string voucher)
        {
            if (token != TokenServices)
            {
                return null;
            }

            m_ticketPricesDomainContract.Configuration = this.Configuration;
            return m_ticketPricesDomainContract.GetInfoVoucher(cinemaid, sessionid, voucher);
        }

        #endregion Contructors

        #region Methods


        public CityTicketPriceEntity[] GetTicketPrice(string date, string token)
        {
            if (token == TokenServices)
            {
                m_ticketPricesDomainContract.Configuration = this.Configuration;
                return m_ticketPricesDomainContract.GetTicketPrice(date);
            }
            else
            {
                return null;
            }
        }

        #endregion Methods
    }
}
   
