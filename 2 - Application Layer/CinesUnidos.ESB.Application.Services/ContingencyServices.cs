﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using System.Collections.Generic;
using System;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{
    /// <summary>
    /// Servicio para Manejar las eventualidades
    /// </summary>
    public class ContingencyServices : Services, IContingency
    {
        #region Attributes
        private IContingencyDomainContract m_contingencyDomainContract;

        private String token = System.Configuration.ConfigurationManager.AppSettings["TokenCMS"].ToString();
        #endregion Attributes

        #region Constructors
        /// <summary>
        /// Constructor del Servicio de las Eventualidades
        /// </summary>
        /// <param name="contingencyDomainContract">Servicios de la Capa de Dominio</param>
        public ContingencyServices(IContingencyDomainContract contingencyDomainContract) : base()
        {
            m_contingencyDomainContract = contingencyDomainContract;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Método para llamar al Dominio y Crear un Mensaje
        /// </summary>
        /// <param name="message">Mensaje a guardar</param>
        /// <returns>La respuesta si se guardó o no el mensaje</returns>
        public ReplyEntity createMessage(MessageEntity message)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.createMessage(message);
        }

        /// <summary>
        /// Método para llamar al Dominio y Crear un Cine
        /// </summary>
        /// <param name="theater">Cine a Guardar</param>
        /// <returns>La respuesta si se guardó o no el cine</returns>
        public ReplyEntity createTheater(TheaterDBEntity theater)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.createTheater(theater);
        }

        /// <summary>
        /// Método para llamar al Dominio y Crear una eventualidad
        /// </summary>
        /// <param name="eventuality">Eventualidad a guardar</param>
        /// <returns>La respuesta si se guardó o no la eventualidad</returns>
        public ReplyEntity createEventuality(EventualityEntity eventuality)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.createEventuality(eventuality);
        }

        /// <summary>
        /// Método para obtener las eventualidades de un cine
        /// </summary>
        /// <param name="code">Código del cine a consultar</param>
        /// <returns>Lista de eventualidades activas en el cine consultado</returns>
        public List<MessageEntity> getEventualityMessage(int code)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            TheaterDBEntity theater = new TheaterDBEntity();
            theater.Code = code;
            return m_contingencyDomainContract.getEventualityMessage(theater);
        }

        /// <summary>
        /// Método para desactivar una eventualidad
        /// </summary>
        /// <param name="eventuality">Eventualidad a desactivar</param>
        /// <returns>La respuesta si se desactivó o no la eventualidad</returns>
        public ReplyEntity deactivateEventuality(EventualityEntity eventuality)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.deactivateEventuality(eventuality);
        }

        /// <summary>
        /// Método para obtener todos los mensajes de eventualidades del sistema
        /// </summary>
        /// <returns>Lista de los Mensajes guardados en la base de datos</returns>
        public List<MessageEntity> getAllMessages()
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.getAllMessages();
        }

        /// <summary>
        /// Método para obtener todos los cines
        /// </summary>
        /// <returns>Lista de los Cines guardados en la base de datos</returns>
        public List<TheaterDBEntity> getAllTheaters()
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.getAllTheaters();
        }

        public ReplyEntity existCodeMessage(int code)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.existCodeMessage(code);
        }

        public ReplyEntity existCodeTheater(int code)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.existCodeTheater(code);
        }

        public ReplyEntity existNameTheater(string name)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.existNameTheater(name);
        }

        /// <summary>
        /// Método para obtener todos los mensajes y solo mostrar las eventualidades activas para un cine 
        /// </summary>
        /// <param name="code">Código del cine a consultar</param>
        /// <returns>Lista de mensajes y eventualidades activas para un cine</returns>
        public List<MessageEventualityEntity> getEventualityMessagesTheater(int code)
        {
            m_contingencyDomainContract.Configuration = Configuration;
            TheaterDBEntity theater = new TheaterDBEntity();
            theater.Code = code;
            return m_contingencyDomainContract.getEventualityMessagesTheater(theater);
        }

        /// <summary>
        /// Se agregó el token para validar los llamados a los servicios. el token viene inmerso en el parametro ListMessages
        /// </summary>
        /// <param name="ListMessages"></param>
        /// <returns></returns>
        public ResultEntity PublishMessages(MessagePack ListMessages)
        {
            if ( ListMessages == null || !ListMessages.Token.Equals(token))
            {
                ResultEntity result = new ResultEntity();
                result.number = 2;
                result.message = " Processed :)";
                return result;
            }
            m_contingencyDomainContract.Configuration = Configuration;
            return m_contingencyDomainContract.PublishMessages(ListMessages);
        }


        /// <summary>
        /// se agregó el token para validar llamados a los servicios, el token debe venir en la llamada al servicios..
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public string GetMessagesForCinema(string token)
        {
            if (token == ConfigurationManager.AppSettings["TokenServices"].ToString())
            {
                m_contingencyDomainContract.Configuration = Configuration;
                return m_contingencyDomainContract.MessagesForCinema();
            }
            else
            {
                return "Processed";
            }
        }
        #endregion Methods
    }
}
