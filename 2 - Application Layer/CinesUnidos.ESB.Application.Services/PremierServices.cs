﻿using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using System.Configuration;

namespace CinesUnidos.ESB.Application.Services
{
    public class PremierServices : Services, IPremier
    {
        #region Attributes
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();
        private IPremiereDomainContract m_premiereDomainServices;

        #endregion Attributes

        #region Contructors

        public PremierServices(IPremiereDomainContract premiereDomainServices)
            : base()
        {
            m_premiereDomainServices = premiereDomainServices;
        }

        #endregion Contructors

        #region Methods

        public PremierEntity[] GetAll(string token)
        {
            if (token == TokenServices)
            {
                m_premiereDomainServices.Configuration = this.Configuration;
                return m_premiereDomainServices.GetAll();
            }
            else
            {
                return null;
            }
        }

        public void Clear(string token)
        {
            if (token == TokenServices)
            {
                m_premiereDomainServices.Configuration = this.Configuration;
                m_premiereDomainServices.Clear();
            }
            else
            {
                return;
            }
        }

        #endregion Methods
    }
}