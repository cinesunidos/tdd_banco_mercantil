﻿using CinesUnidos.ESB.Domain;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    /// <summary>
    /// Contrato para controlar la informacion de los estrenos
    /// </summary>
    [ServiceContract]
    public interface IPremier
    {
        /// <summary>
        /// Borra el cache de Premier
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Clear/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void Clear(string token);

        /// <summary>
        /// Lista de estrenos
        /// </summary>
        /// <returns>Lista de estrenos</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetAll/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PremierEntity[] GetAll(string token);
    }
}