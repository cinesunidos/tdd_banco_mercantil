﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    /// <summary>
    /// Contrato para el control de imagenes
    /// </summary>
    [ServiceContract]
    public interface IImages
    {
        /// <summary>
        /// Entrega las imagenes que se usaron para el slider
        /// </summary>
        /// <param name="ho">Codigo ho de la pelicula</param>
        /// <returns>Imagen del poster</returns>
        [OperationContract, WebGet(UriTemplate = "Slider/{ho}/{token}")]
        Stream GetSlider(string ho, string token);

        /// <summary>
        /// Entrega las imagenes para los poster
        /// </summary>
        /// <param name="ho">Codigo ho de la pelicula</param>
        /// <returns>Imagen del poster</returns>
        [OperationContract, WebGet(UriTemplate = "Poster/{ho}/{token}")]
        Stream GetPoster(string ho, string token);
    }
}