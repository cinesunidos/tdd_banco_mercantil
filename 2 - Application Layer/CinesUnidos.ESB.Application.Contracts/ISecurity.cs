﻿using CinesUnidos.ESB.Domain;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface ISecurity
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/SignIn/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TokenEntity SignIn(SignInEntity login);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Signout?token={token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void Signout(string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Valid?token={token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool Valid(string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Refresh?token={token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TokenEntity Refresh(string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Info/?token={token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TokenInfoEntity Info(string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/User/{userId}/{tokenServices}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserEntity ReadUser(string userId, string tokenServices);

        [OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/UserByHash/{hash}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [WebInvoke(Method = "GET", UriTemplate = "/UserByHash/?hash={hash}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserEntity ReadUserByHash(string hash);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/User/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseEntity CreateUser(UserEntity userEntity);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UpdateUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseEntity UpdateUser(UserEntity userEntity);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/RememberEmail/{idCard}/{day}/{month}/{year}/{tokenServices}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string RememberEmail(string idCard, string day, string month, string year, string tokenServices);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/RememberPassword/{email}/{tokenServices}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserEntity RememberPassword(string email, string tokenServices);

        [OperationContract]

        [WebInvoke(Method = "GET", UriTemplate = "/RemoveList/{userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string RemoveList(string userId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/VerifiedAnswer/{email}/{answer}/{tokenServices}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string VerifiedAnswer(string email, string answer, string tokenServices);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/VerifiedEmailAndIdCard/{email}/{idCard}/{tokenServices}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string VerifiedEmailAndIdCard(string email, string idCard, string tokenServices);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ChangePassword/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseEntity ChangePassword(PasswordEntity passwordEntity);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ActivateUser/{userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseEntity ActivateUser(string userId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UserMobile/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseEntity  CreateUserMobile(UserEntity userEntity);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Passvalidation/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DatosBoleto Passvalidation(DatosBoleto datosBoleto);
    }
}