﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IAppMobile
    {
        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/GetHome?platform={platform}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //List<SectionEntity> GetHome(string platform);

        /// <summary>
        /// Lista de cines en Moviles
        /// </summary>
        /// <returns>Cines</returns>
        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/GetTheaters", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //TheaterMobileEntity[] GetAllTheatersMobile();

        [WebInvoke(Method = "GET", UriTemplate = "/GetHome", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetAllTheatersMobile2();

    }
}