﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{

    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Interface que define los metodos de la clase ConcessionsServices
    /// </summary>

    [ServiceContract]
    public interface IConcessions
    {

        # region Methods    
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/TheaterConcessions/{theaterId}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionListEntity GetTheaterConcessions(string theaterId, string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/AddItemsToCart/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionsRSEntity AddItemsToCart(ConcessionsRQEntity concessionsRq);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/PurchaseConcessions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionsRSEntity PurchaseConcessions(ConcessionsRQEntity concessionsRq);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/CancelConcessionsOrder/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionsRSEntity CancelConcessionsOrder(ConcessionsRQEntity concessionsRq);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ModifyModalConcessions/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MessageEntity ModifyModalConcessions(MessagePack ListMessages);

        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/PromoPepsi/{Codigo}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //string PromoPepsi(string Codigo, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetCandiesWeb/{theaterId}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionListEntity GetCandiesWeb(string theaterId, string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/AddItemsCandiesWeb/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConcessionsRSEntity AddItemsCandiesWeb(ConcessionsRQEntity concessionsRq);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UpdateCandiesTable/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TransferPack UpdateCandiesTable(TransferPack items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/DecreaseCandiesTable/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TransferPack DecreaseCandiesTable(TransferPack items);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Desorden/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PromoPack PromoDesorden(PromoPack Pack);

        #endregion

    }
}
