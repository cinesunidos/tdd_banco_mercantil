﻿using CinesUnidos.ESB.Domain;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IUserSession
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/{userSessionId}/{readMap}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserSessionResponseEntity Read(string userSessionId, string readMap, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/{usessionID}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserSessionResponseMobileEntity ReadMobile(string usessionID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserSessionResponseEntity Create(BasketEntity userSession);

        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/Mobile", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //UserSessionResponseMobileEntity CreateMobile(BasketEntity userSession);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/remove/{userSessionId}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
         void Delete(string userSessionId, string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Confirmation/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConfirmationResponseEntity Confirmation(ConfirmationEntity userSession);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ConfirmationMobile/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ConfirmationResponseEntity ConfirmationMobile(ConfirmationEntity userSession);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Payment/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PaymentResponseEntity Payment(PaymentEntity payment);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/PaymentAuthMercantil/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PaymentResponseEntity PaymentAuthMercantil(PaymentEntity payment);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/PaymentCandies/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PaymentResponseEntity PaymentCandies(PaymentEntity payment);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/PaymentMobile/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PaymentResponseEntity PaymentMobile(PaymentEntity payment);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UserSession/{userSessionId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserSessionEntity Get(string userSessionId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/CancelOrder/{userSessionId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean CancelOrder(string UserSessionID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeleteOrder/{SessionId}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserSessionEntity DeleteSession(string SessionId, string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetVippoConfirmation/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        AVippoResponse GetVippoConfirmation(APaymentRequest Payment);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/BDVGeneratePayment/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string BDVGeneratePayment(BDVUserInfo id);
    }
}