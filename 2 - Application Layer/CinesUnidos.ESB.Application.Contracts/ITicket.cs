﻿using CinesUnidos.ESB.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface ITicket
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/{date}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        CityTicketPriceEntity[] GetTicketPrice(string date, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/{token}/{cinemaid}/{sessionid}/{voucher}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        InfoVoucher GetInfoVoucher(string token, string cinemaid, string sessionid, string voucher);
    }
}
