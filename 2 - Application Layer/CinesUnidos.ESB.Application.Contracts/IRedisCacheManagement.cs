﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IRedisCacheManagement
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeleteAll/{tokenId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string DeleteCacheAll(string tokenId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeleteAllNoTicketsPrice/{tokenId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string DeleteCacheNoTicketsPrice(string tokenId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeleteTicketsPrice/{tokenId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string DeleteCacheTicketsPrice(string tokenId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetDate/{tokenId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ReadDateCacheTicketsPrice(string tokenId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeletePremiers/{tokenId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string DeletePremiers(string tokenId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetLocalStorage/{tokenId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetPathLocalStorage(string tokenId);


    }
}