﻿using CinesUnidos.ESB.Domain;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IContact
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Send/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseEntity SendComplaint(ComplaintEntity complaint);
    }
}