﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IPromotion 
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Publish/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultEntity PublishPromotion(PromotionPackageEntity PromotionList);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPromotions/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetPromotions(string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPromotionsDB/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetPromotionsDB(string token);
    }
}
