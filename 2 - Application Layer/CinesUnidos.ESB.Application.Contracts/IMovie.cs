﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IMovie
    {
        /// <summary>
        /// Borra el cache de Billboard
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Clear/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void Clear(string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetByCity/{city}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity[] GetByCity(string city, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetByCityDate/{city}/{day}/{month}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity[] GetByCityDate(string city, string day, string month, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetById/{city}/{movieId}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity GetById(string city, string movieId, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetMovieAll/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity[] GetMovieAll(string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ListFavorite", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity[] GetMovieTheaterList(List<string> cines, string day, string month);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetByIdDate/{city}/{movieId}/{day}/{month}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity GetByIdDate(string city, string movieId, string day, string month, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Slider/{city}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity[] GetSlider(string city, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Bar/{city}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieEntity[] GetBar(string city, string token);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/MoreMovie/{city}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieEntity[] GetMoreMovie(string city, string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Favorite", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MovieBillboardEntity[] GetAllMoviesByTheaterList(List<string> cines, string day, string month);
    }
}