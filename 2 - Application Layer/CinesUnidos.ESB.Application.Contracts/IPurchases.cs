﻿using CinesUnidos.ESB.Domain;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IPurchases
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetAll/{usr}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PurchasesEntity[] GetAll(string usr, string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Register/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string Register(PurchasesEntity data);
    }
}