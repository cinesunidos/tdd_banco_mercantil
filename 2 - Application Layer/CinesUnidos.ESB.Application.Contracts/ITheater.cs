﻿using CinesUnidos.ESB.Domain;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    /// <summary>
    /// Contrato para control de la informacion de los cines y cartelera.
    /// </summary>
    [ServiceContract]
    public interface ITheater
    {
        /// <summary>
        /// Borra el cache de Premier
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Clear/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void Clear(string token);

        /// <summary>
        /// Lista de cines
        /// </summary>
        /// <returns>Cines</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TheaterEntity[] GetAll(string token);

        /// <summary>
        /// Cartelara agrupada por cine
        /// </summary>
        /// <param name="theaterId">Id del cine</param>
        /// <returns>Cartelera</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/{theaterId}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TheaterBillboardEntity GetById(string theaterId, string token);

        /// <summary>
        /// Cartelara agrupada por cine
        /// </summary>
        /// <param name="theaterId">Id del cine</param>
        /// <param name="day">Dia</param>
        /// <param name="month">Mes</param>
        /// <returns>Cartelera</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/{theaterId}/{day}/{month}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TheaterBillboardEntity GetByIdDate(string theaterId, string day, string month, string token);

        /// <summary>
        /// Lista de Ciudades
        /// </summary>
        /// <returns>Ciudades</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/City/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string[] GetAllCity(string token);

        /// <summary>
        /// Cartelara agrupada por cine
        /// </summary>
        /// <param name="city">Ciudad</param>
        /// <returns>Cartelera</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/City/{city}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TheaterBillboardEntity[] GetByCity(string city, string token);

        /// <summary>
        /// Cartelara agrupada por cine
        /// </summary>
        /// <param name="city">Ciudad</param>
        /// <param name="day">Dia</param>
        /// <param name="month">Mes</param>
        /// <returns>Cartelera</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/City/{city}/{day}/{month}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TheaterBillboardEntity[] GetByCityDate(string city, string day, string month, string token);

        /// <summary>
        /// Detalles de la funcion
        /// </summary>
        /// <param name="theaterId">Id del cine</param>
        /// <param name="sessionId">Id de la funcion</param>
        /// <returns>Informacion de la funcion</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Session/{theaterId}/{sessionId}/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SessionInfoEntity GetSession(string theaterId, string sessionId, string token);
    }
}