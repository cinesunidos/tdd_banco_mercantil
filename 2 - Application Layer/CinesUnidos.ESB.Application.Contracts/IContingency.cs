﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace CinesUnidos.ESB.Application.Contracts
{

    [ServiceContract]
    public interface IContingency
    {

        /// <summary>
        /// Servicio para Crear un Mensaje
        /// </summary>
        /// <param name="mesage"> JSON con atributos:
        ///     Code    = Código del Mensaje
        ///     Message = Texto del Mensaje
        /// </param>
        /// <returns> JSON con atributos:
        ///     IsOk     = Sí se salvó correctamente retorna true, de lo contrario false
        ///     Messages = Si el atributo IsOk retorna false se le anexa una Lista de Errores 
        /// </returns>
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/CreateMessage/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ReplyEntity createMessage(MessageEntity mesage);

        /// <summary>
        /// Servicio para Crear un Cine
        /// </summary>
        /// <param name="theater"> JSON con atributos:
        ///     Code    = Código del Cine
        ///     Name    = Nombre del Cine
        /// </param>
        /// <returns> JSON con atributos:
        ///     IsOk     = Sí se salvó correctamente retorna true, de lo contrario false
        ///     Messages = Si el atributo IsOk retorna false se le anexa una Lista de Errores 
        /// </returns>
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/CreateTheater/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ReplyEntity createTheater(TheaterDBEntity theater);

        /// <summary>
        /// Servicio para crear una eventualidad
        /// </summary>
        /// <param name="eventuality"> JSON con atributos:
        ///     Status      = Estado de la eventualidad true si es activa o  false si está desactivada, si no se pasa este campo tomará el valor false por defecto
        ///     Creation    = Fecha para Activar la Eventualidad en el formato ¨dd-mm-yyyy HH:mm:ss¨ este campo es opcional
        ///     Finish      = Fecha para Desactivar la Eventualidad en el formato ¨dd-mm-yyyy HH:mm:ss¨ este campo es opcional
        ///     TheaterCode = Código del Cine donde se creará la eventualidad 
        ///     MessageCode = Código del Mensaje a mostrar
        /// </param>
        /// <returns> JSON con atributos:
        ///     IsOk     = Sí se salvó correctamente retorna true, de lo contrario false
        ///     Messages = Si el atributo IsOk retorna false se le anexa una Lista de Errores 
        /// </returns>
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/CreateEventuality/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ReplyEntity createEventuality(EventualityEntity eventuality);

        /// <summary>
        /// Servicio para obtener las eventualidades de un Cine
        /// </summary>
        /// <param name="code">Código del Cine</param>
        /// <returns> JSON una lista de Mensajes con los siguientes Atributos
        ///     Code    = Código del Mensaje
        ///     Id      = Id de la Eventualidad
        ///     Message = Texto de la Eventualidad
        /// </returns>
        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/GetEventualities/?code={code}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //List<MessageEntity> getEventualityMessage(int code);

        /// <summary>
        /// Servicio para Desactiva una eventualidad
        /// </summary>
        /// <param name="eventuality"> JSON con atributos
        ///     Id      = Id de la eventualidad
        /// </param>
        /// <returns> JSON con atributos:
        ///     IsOk     = Sí se desactivó correctamente retorna true, de lo contrario false
        ///     Messages = Si el atributo IsOk retorna false se le anexa una Lista de Errores 
        /// </returns>
        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/DeactivateEventuality/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ReplyEntity deactivateEventuality(EventualityEntity eventuality);

        /// <summary>
        /// Servicio que retorna todos los mensajes creados en la base de datos
        /// </summary>
        /// <returns>  JSON una lista de Mensajes con los siguientes Atributos
        ///     Code    = Código del Mensaje
        ///     Message = Texto del Mensaje
        /// </returns>
        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/GetAllMessages/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //List<MessageEntity> getAllMessages();

        /// <summary>
        /// Servicio que retorna todos los cines creados en la base de datos
        /// </summary>
        /// <returns>  JSON una lista de Cines con los siguientes Atributos
        ///     Code    = Código del Cine
        ///     Name    = Nombre del Cine
        /// </returns>
        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/GetAllTheaters/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //List<TheaterDBEntity> getAllTheaters();

        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/ExistMessageCode/?code={code}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ReplyEntity existCodeMessage(int code);

        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/ExistTheaterCode/?code={code}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ReplyEntity existCodeTheater(int code);

        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/ExistTheaterName/?name={name}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ReplyEntity existNameTheater(string name);

        /// <summary>
        /// Servicio para obtener todos los mensajes y solo mostrar las eventualidades activas para un cine
        /// </summary>
        /// <param name="code">Código del Cine</param>
        /// <returns> JSON una lista de Mensajes con los siguientes Atributos
        ///     Code    = Código del Mensaje
        ///     Message = Texto de la Eventualidad
        ///     Id      = Id de la Eventualidad
        ///     Status  = Estado de la Eventualidad true si está activa de lo contrario será false
        ///     Creation = Fecha de Inicio de la Eventualidad
        ///     Finish  = Fecha de Fin de la Eventuadidad
        /// </returns>
        /// comentado ya que no está en produccion
        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "/GetMessagesEventualities/?code={code}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //List<MessageEventualityEntity> getEventualityMessagesTheater(int code);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Publish/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultEntity PublishMessages(MessagePack ListMessages);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetMessagesForCinema/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
       string GetMessagesForCinema(string token);

    }
}
