﻿using CinesUnidos.ESB.Domain;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace CinesUnidos.ESB.Application.Contracts
{
    [ServiceContract]
    public interface IAdmin
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Login/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserEntity Login(UserEntity usr);
    }
}