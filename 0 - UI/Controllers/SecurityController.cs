﻿using CinesUnidos.WebRoles.MVC.Models;
using CinesUnidos.WebRoles.MVC.Models.Filters;
using CinesUnidos.WebRoles.MVC.Models.Infrastructure;
using CinesUnidos.WebRoles.MVC.Models.Support;
using System;
using System.Web;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Services;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QRCoder;
using System.Drawing;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;

namespace CinesUnidos.WebRoles.MVC.Controllers
{
    public class SecurityController : Controller
    {
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();

        public IAuthProvider FormsService { get; set; }

        public SecurityController()
        {
            FormsService = new FormsAuthProvider();
        }

        private void CreateCookie(TokenInfo tokenInfo)
        {
            Serializer serial = new Serializer();
            FormsAuthenticationTicket authTicket =
                new FormsAuthenticationTicket(2, tokenInfo.Email,
                DateTime.Now, DateTime.Now.AddMinutes(FormsService.CookieExpiration),
                FormsService.IsCookiePersistent,
                serial.Serialize<TokenInfo>(tokenInfo));
            //XmlAdapter.Serialize<TokenInfo>(tokenInfo));
            string encryptedticket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedticket);
            if (FormsService.IsCookiePersistent)
                authCookie.Expires = authTicket.Expiration;
            authCookie.Path = FormsAuthentication.FormsCookiePath;
            if (Response != null)
            {
                Response.Cookies.Add(authCookie);
                FormsService.SignIn(tokenInfo.Email);
            }
        }

        private string SanitizeURLString(string param)
        {
            string Results = param;
            Results = Results.Replace("%", "%25");
            Results = Results.Replace("<", "%3C");
            Results = Results.Replace(">", "%3E");
            Results = Results.Replace("#", "%23");
            Results = Results.Replace("{", "%7B");
            Results = Results.Replace("}", "%7D");
            Results = Results.Replace("|", "%7C");
            Results = Results.Replace("'\'", "%5C");
            Results = Results.Replace("^", "%5E");
            Results = Results.Replace("~", "%7E");
            Results = Results.Replace("[", "%5B");
            Results = Results.Replace("]", "%5D");
            Results = Results.Replace("`", "%60");
            Results = Results.Replace(";", "%3B");
            Results = Results.Replace("/", "%2F");
            Results = Results.Replace("?", "%3F");
            Results = Results.Replace(":", "%3A");
            Results = Results.Replace("@", "%40");
            //Results = Results.Replace("=", "%3D");
            Results = Results.Replace("&", "%26");
            Results = Results.Replace("$", "%24");
            Results = Results.Replace("+", "%2B");

            return Results;
        }

        //[Throttle(Name = "Cines Unidos", Message = "Se limpió la caché hace poco. espere unos segundos", Seconds = 300)]
        public JsonResult LimpiarCacheFront(string token)
        {
            string message;
            if (token != TokenServices)
            {
                message = ";)";
            }
            else
            {
                try
                {
                    BarMovieCacheClass.CleanCache();
                    SliderCacheClass.CleanCache();
                    MoreMoviesCacheClass.CleanCache();
                    MovieCacheClass.CleanCache();
                    PremierCacheClass.CleanCache();
                    CityTicketPriceCacheClass.CleanCache();
                    message = "Se Borró la cache del front";
                }
                catch (Exception e)
                {
                    message = "Hubo un error al borrar el caché del front. Detalle: " + e.Message;
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #region Methods

        [HttpGet]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult SignIn(string returnUrl)
        {
            SignIn model = new SignIn();
            model.Url = returnUrl;
            return View(model);
        }

        [HttpPost]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult SignIn(SignIn model)
        {
            TempData["Errors"] = null;
            if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.Email))
            {
                if (model.Email.ToUpper().Contains("yopmail".ToUpper()))
                {
                    TempData["Errors"] = "El correo electrónico no es valido";
                    return View(model);
                }
                model.Platform = "Web";
                Service services = new Service();
                //Token token = services.Execute<SignIn, Token>("Security.SignIn", model);
                Token token = IniciarSesion(model);
                //ViewBag.Hash = token.Hash.ToString();

                // Si no hubo exception, token debe ser != null
                if (token != null)
                {
                    // Analizar posibles errores
                    if (!string.IsNullOrEmpty(token.Error))
                    {
                        TempData["Errors"] = token.Error;
                    }
                    else
                    {
                        // Crear sesion
                        TempData["Hash"] = SanitizeURLString(token.Hash.ToString());
                        TokenInfo tokenInfo = services.Execute<TokenInfo>("Security.Info", new { token = SanitizeURLString(token.Hash) });

                        // Consultar la ci del usuario que inició sesión
                        UserEntity userEntity = services.Execute<UserEntity>("Security.ReadUserByHash", new { hash = SanitizeURLString(token.Hash.ToString()), tokenServices = TokenServices });
                        tokenInfo.Identification = userEntity.IdCardNumber;
                        tokenInfo.IdentificationType = userEntity.IdCardType;

                        //Informacion del Usuario en variable de sesion solo en caso de que no cierren el navegador.
                        Session["UserInfo"] = userEntity;

                        // Cookie
                        CreateCookie(tokenInfo);

                        // Redirect
                        if (Url != null)
                        {
                            if (Url.IsLocalUrl(model.Url))
                            {
                                return Redirect(model.Url);
                            }
                            else
                            {
                                return RedirectToRoute("Index");
                            }
                        }
                        else
                        {
                            return RedirectToRoute("Index");
                        }
                    }
                }
                else
                {
                    //ModelState.AddModelError("", "El nombre de usuario o la contraseña especificados son incorrectos. Si ustes acaba de registrar su cuenta, Por favor ingrese a su correo electr&oacute;nico para activar su cuenta");
                    string strErrormsg = "No ha podido acceder, verifique: </br>• El nombre de usuario y contraseña sean correctos.</br> "
            + "• Recuerde que debe formalizar el registro activando su cuenta a través del enlace enviado a su correo electrónico.</br>" +
              "• Si no encuentra el correo de activación, revise su bandeja de Correos no deseados.";
                    TempData["Errors"] = strErrormsg;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.Email))
                {
                    TempData["Errors"] = "Disculpa, el campo “correo electrónico” es requerido";
                }
                else
                {
                    TempData["Errors"] = "Disculpa, el campo “contraseña” es requerido";
                }
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        //        #if !DEBUG
        //        //[ExitHttpsIfNotRequired()]
        //#endif
        public ActionResult Signout()
        {
            FormsService.SignOut();
            return RedirectToRoute("Index");
        }

        [HttpGet]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif
        public new ActionResult User()
        {
            UserEntity User = new UserEntity();
            ViewData["mod"] = "0";
            return View(User);
        }

        [HttpPost]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif
        public new ActionResult User(UserEntity model)
        {
            Service services = new Service();
            ViewData["mod"] = "0";
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();
            //Esta validación impide que se ingresen correos que tengan yopmail
            if (model.Email.ToUpper().Contains("yopmail".ToUpper()) || model.EmailComparation.ToUpper().Contains("yopmail".ToUpper()))
            {
                ModelState.AddModelError("Email", "El correo ingresado es inválido.");
                ModelState.AddModelError("EmailComparation", "El correo ingresado es inválido.");
                return View(model);
            }
            if (String.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ModelState.AddModelError("EmptyCaptcha", "El captcha es requerido.");
                return View(model);
            }

            RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();

            if (recaptchaResult != RecaptchaVerificationResult.Success)
            {
                ModelState.AddModelError("InvalidCaptcha", "Respuesta incorrecta.");
                return View(model);
            }

            if (ModelState.IsValid)
            {
                Response response = services.Execute<UserEntity, Response>("Security.CreateUser", model);
                if (response.isOk == false)
                {
                    // Tomar error
                    if (response.Dictionary.ContainsKey("ErrorMsg"))
                    {
                        string err = response.Dictionary["ErrorMsg"].ToString();
                        ViewBag.ErrorMsg = err;
                    }

                    if (response.Dictionary.ContainsKey("UserExiste"))
                    {
                        string err = response.Dictionary["UserExiste"].ToString();
                        ViewBag.ErrorMsg = err;
                    }

                    // Vista
                    return View("NotSucceed", model);
                }
                else return View("Success", model);
            }
            return View(model);
        }

        [HttpGet]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif

        public ActionResult UpdateUser(string hash)
        {
            Service services = new Service();
            ViewData["mod"] = "1";
            ViewBag.hash = hash;
            UserEntity userEntity = services.Execute<UserEntity>("Security.ReadUserByHash", new { hash = SanitizeURLString(hash), tokenServices = TokenServices });
            if (userEntity != null)
                userEntity.hash = hash;
            userEntity.EmailComparation = userEntity.Email;
            userEntity.PasswordComparation = userEntity.Password;
            return View("User", userEntity);
        }

        [HttpPost]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult UpdateUser(UserEntity model, SignIn model2)
        {
            ViewData["mod"] = "1";
            Service services = new Service();

            String expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            Int64 i = 0;
            DateTime dt;

            if (string.IsNullOrEmpty(model.Email) || string.IsNullOrWhiteSpace(model.Email) ||
                string.IsNullOrEmpty(model.EmailComparation) || string.IsNullOrWhiteSpace(model.EmailComparation) ||
                string.IsNullOrEmpty(model.SecretQuestion) || string.IsNullOrWhiteSpace(model.SecretQuestion) ||
                string.IsNullOrEmpty(model.SecretAnswer) || string.IsNullOrWhiteSpace(model.SecretAnswer))
            {
                ViewData["Message"] = "Debe llenar todos los campos obligatorios";
                return View("User", model);
            }

            if (model.Email != model.EmailComparation)
            {
                ViewData["Message"] = "El correo debe ser igual en los dos casos";
                return View("User", model);
            }

            if (!(Regex.IsMatch(model.Email, expresion)) || !(Regex.IsMatch(model.EmailComparation, expresion)))
            {

                ViewData["Message"] = "Correo electrónico no es valido".Replace("ó", @"\363").Replace("á", @"\341");
                return View("User", model);
            }

            //Esta validación impide que se pueda modificar el correo por alguno que sea de yopmail
            if (model.Email.ToUpper().Contains("yopmail".ToUpper()) || model.EmailComparation.ToUpper().Contains("yopmail".ToUpper()))
            {
                ViewData["Message"] = "Correo electrónico no es valido".Replace("ó", @"\363").Replace("á", @"\341");
                return View("User", model);
            }

            if (string.IsNullOrEmpty(model.Name) || string.IsNullOrWhiteSpace(model.Name) ||
                string.IsNullOrEmpty(model.LastName) || string.IsNullOrWhiteSpace(model.LastName) ||
                string.IsNullOrEmpty(model.Phone) || string.IsNullOrWhiteSpace(model.Phone) ||
                string.IsNullOrEmpty(model.Address) || string.IsNullOrWhiteSpace(model.Address) ||
                string.IsNullOrEmpty(model.IdCardNumber) || string.IsNullOrWhiteSpace(model.IdCardNumber) ||
                string.IsNullOrEmpty(model.BirthDate.ToString()) || string.IsNullOrWhiteSpace(model.BirthDate.ToString())
                )
            {
                ViewData["Message"] = "Debe llenar todos los campos obligatorios";
                return View("User", model);
            }

            if (!(DateTime.TryParseExact(Convert.ToDateTime(model.BirthDate).ToString("dd/MM/yyyy").Replace("-", "/").Replace(" ", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)))
            {
                ViewData["Message"] = "La fecha ingresada no es valida";
                return View("User", model);
            }

            if (Convert.ToDateTime(model.BirthDate).Year < 1753 || Convert.ToDateTime(model.BirthDate).Year > 9999)
            {
                ViewData["Message"] = "La fecha ingresada no es correcta";
                return View("User", model);
            }

            if (!(Int64.TryParse(model.IdCardNumber, out i)))
            {
                ViewData["Message"] = "El campo cedula solo acepta valores numericos";
                return View("User", model);
            }

            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrWhiteSpace(model.Password) ||
                string.IsNullOrEmpty(model.PasswordComparation) || string.IsNullOrWhiteSpace(model.PasswordComparation))
            {
                ViewData["Message"] = "Debe llenar todos los campos obligatorios";
                return View("User", model);
            }

            if (model.Password != model.PasswordComparation)
            {
                ViewData["Message"] = "La contraseña deber ser igual en los dos casos".Replace("ñ", @"\361");
                return View("User", model);
            }

            if (string.IsNullOrEmpty(model.SexString) || string.IsNullOrWhiteSpace(model.SexString))
            {
                ViewData["Message"] = "Debe seleccionar su sexo";
                return View("User", model);
            }

            if (!(model.SexString == "M" || model.SexString == "F"))
            {
                ViewData["Message"] = "El sexo seleccionado no existe";
                return View("User", model);
            }

            if (string.IsNullOrEmpty(model.IdCardType) || string.IsNullOrWhiteSpace(model.IdCardType))
            {
                ViewData["Message"] = "El campo de opciones del tipo de persona no puede estar vacio".Replace("í", @"\355");
                return View("User", model);
            }

            if (!(model.IdCardType == "V" || model.IdCardType == "E" || model.IdCardType == "J"))
            {
                ViewData["Message"] = "El valor seleccionado en el campo de opciones del tipo de persona no existe";
                return View("User", model);
            }

            if (string.IsNullOrEmpty(model.State) || string.IsNullOrWhiteSpace(model.State))
            {
                ViewData["Message"] = "El campo de selección de estado no puede estar vacío".Replace("í", @"\355");
                return View("User", model);
            }

            if (!(model.State == "Amazonas" || model.State == "Anzoátegui" || model.State == "Apure" ||
                model.State == "Aragua" || model.State == "Barinas" || model.State == "Bolívar" ||
                model.State == "Carabobo" || model.State == "Cojedes" || model.State == "Delta Amacuro" ||
                model.State == "Distrito Federal" || model.State == "Falcón" || model.State == "Guárico" ||
                model.State == "Lara" || model.State == "Mérida" || model.State == "Miranda" ||
                model.State == "Monagas" || model.State == "Nueva Esparta" || model.State == "Portuguesa" ||
                model.State == "Sucre" || model.State == "Táchira" || model.State == "Vargas" ||
                model.State == "Yaracuy" || model.State == "Zulia"))
            {
                ViewData["Message"] = "El estado seleccionado no existe";
                return View("User", model);
            }

            if (ModelState.IsValid)
            {
                UserEntity userEntity = services.Execute<UserEntity>("Security.ReadUserByHash", new { hash = SanitizeURLString(model.hash), tokenServices = TokenServices });
                Guid id = userEntity.Id;
                // Inicio - Adicionado para evitar que el invitado cambie su cedula. para evitar posibles conductas indebidas.
                model.Id = id;
                model.IdCard = userEntity.IdCard;
                model.IdCardType = userEntity.IdCardType;
                model.IdCardNumber = userEntity.IdCardNumber;
                //Fin -
                Response response = services.Execute<UserEntity, Response>("Security.UpdateUser", model);
                if (response.isOk == false)
                {
                    if (response.Dictionary.ContainsKey("ErrorMsg"))
                    {
                        string err = response.Dictionary["ErrorMsg"].ToString();
                        ViewBag.ErrorMsg = err;
                    }

                    ViewData["userId"] = id.ToString();
                    return View("NotSucceed", model);
                }
                else
                {

                    //Esta parte es se usa para actualizar los datos de la cookies al finalizar la actualización de los datos del usuario
                    UserEntity userEntity2 = services.Execute<UserEntity>("Security.ReadUserByHash", new { hash = SanitizeURLString(model.hash), tokenServices = TokenServices });
                    model2.Platform = "Web";
                    Token token = services.Execute<SignIn, Token>("Security.SignIn", model2);
                    TokenInfo tokenInfo = services.Execute<TokenInfo>("Security.Info", new { token = SanitizeURLString(token.Hash) });
                    tokenInfo.Identification = userEntity2.IdCardNumber;
                    tokenInfo.IdentificationType = userEntity2.IdCardType;
                    CreateCookie(tokenInfo);
                    //Esta parte es se usa para actualizar los datos de la cookies al finalizar la actualización de los datos del usuario

                    return View("Success", model);
                }
            }
            return View("User", model);
        }

        [HttpPost]

        //        #if !DEBUG
        //        [RequireHttpsWithPortAttribute()]
        //#endif

        public JsonResult RememberEmail(string idCard, string idCardType, int? day, int? month, int? year)
        {
            if (String.IsNullOrEmpty(idCard) || String.IsNullOrEmpty(idCardType) || day == null || month == null || year == null)
            {
                return Json("<div class='col-md-6 col-md-offset-3'><i class='fa fa-info-circle fa-5x' aria-hidden='true'></i></div>" +
                                "<div id='rem-mail' class='col-md-6 col-md-offset-3'><p>Estimado invitado, la fecha no tiene el formato válido </p><b></b></div>");
            }

            //System.Threading.Thread.Sleep(3000);
            string email = string.Empty;
            try
            {
                DateTime date = new DateTime((int)year, (int)month, (int)day);

                Service services = new Service();
                idCard = idCardType + idCard;
                email = services.Execute<string>("Security.RememberEmail", new { idCard = idCard, day = day, month = month, year = year, tokenServices = TokenServices });
                if (String.IsNullOrEmpty(email))
                {
                    return Json("<div class='col-md-6 col-md-offset-3'><i class='fa fa-exclamation-circle fa-5x'></i></div>" +
                                "<div id='rem-mail' class='col-md-8 col-md-offset-2'><p>Estimado invitado por favor" +
                                " verifique que sus datos sean correctos.</p></div>");
                }


            }
            catch (Exception ex)
            {
                throw new ApplicationException("Formato de fecha no valido", ex);
            }

            return Json("<div class='col-md-6 col-md-offset-3'><i class='fa fa-info-circle fa-5x' aria-hidden='true'></i></div>" +
                                "<div id='rem-mail' class='col-md-6 col-md-offset-3'><p>Estimado invitado su correo es: </p><b>" + email + "</b></div>");
        }

        [HttpPost]
        //        #if !DEBUG
        //        [RequireHttpsWithPortAttribute()]
        //#endif
        public JsonResult RememberPassword(string email)
        {
            //System.Threading.Thread.Sleep(3000);
            Service services = new Service();
            UserEntity user = services.Execute<UserEntity>("Security.RememberPassword", new { email = email, tokenServices = TokenServices });
            UserRememberPsw usremember = new UserRememberPsw();
            usremember.Email = user.Email;
            usremember.SecretQuestion = user.SecretQuestion;
            return Json(usremember);
        }

        [HttpPost]
        //        #if !DEBUG
        //        [RequireHttpsWithPortAttribute()]
        //#endif
        public JsonResult VerifiedAnswer(string email, string answer)
        {
            Service services = new Service();
            string isOk = services.Execute<string>("Security.VerifiedAnswer", new { email = email, answer = answer, tokenServices = TokenServices });
            return Json(isOk);
        }

        [HttpPost]
        //        #if !DEBUG
        //        [RequireHttpsWithPortAttribute()]
        //#endif
        public JsonResult VerifiedEmailAndIdCard(string email, string idCard, string idCardType)
        {
            Service services = new Service();
            idCard = idCardType + idCard;
            string isOk = services.Execute<string>("Security.VerifiedEmailAndIdCard", new { email = email, idCard = idCard, tokenServices = TokenServices });
            return Json(isOk);
        }

        [HttpPost]
        public ActionResult ChangePassword(Password password)
        {
            return View();
        }

        [HttpGet]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult ActivateUser(string userId)
        {
            Service services = new Service();
            //Response response = services.Execute<Response>("Security.ActivateUser", new { userId = userId, tokenServices = TokenServices });
            Response response = services.Execute<Response>("Security.ActivateUser", new { userId = userId });
            if (response.isOk == true) ViewData["activation"] = "1";
            else ViewData["activation"] = "0";
            return View("Success");
        }

        [HttpGet]
#if !DEBUG
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult Purchases()
        {
            Service services = new Service();

            // CI
            string id = string.Empty;
            try
            {
                HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = null;
                ticket = FormsAuthentication.Decrypt(cookie.Value);
                //TokenInfo userdata = (TokenInfo)XmlAdapter.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
                Serializer serial = new Serializer();
                //TokenInfo userdata = (TokenInfo)XmlAdapter.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
                TokenInfo userdata = (TokenInfo)serial.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));

                //TokenInfo userdata = (TokenInfo)XmlAdapter.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
                //id = userdata.IdentificationType + userdata.Identification;
                id = userdata.IdentificationType + userdata.Identification;
            }
            catch
            {
                return RedirectToAction("../Inicio_Sesion");
            }
            PurchaseEntity[] purs = services.Execute<PurchaseEntity[]>("Purchases.GetAll", new { usr = id, token = TokenServices });
            List<PurchaseQR> purchas = new List<PurchaseQR>();
            string json_serialize = JsonConvert.SerializeObject(purs);
            var jsonObj = JsonConvert.DeserializeObject<List<PurchaseQR>>(json_serialize);

            List<PurchaseQR2> p2 = new List<PurchaseQR2>();
            foreach (var item in jsonObj)
            {
                if (item != null)
                {
                    PurchaseQR2 p = new PurchaseQR2();
                    p = (PurchaseQR2)item;
                    p2.Add(p);
                }
            }

            foreach (var obj in p2)
            {
                if (obj != null)
                {
                    byte[] imageqr;
                    bool soloNumeros = obj.BookingNumber.All(char.IsDigit);
                    int validate = 0;

                    if (soloNumeros)
                    {
                        validate = 0;
                        QRCodeGenerator qrGenerator = new QRCodeGenerator();
                        QRCodeData qrCodeData = qrGenerator.CreateQrCode(obj.BookingNumber.ToString(), QRCodeGenerator.ECCLevel.Q);
                        QRCode qrCode = new QRCode(qrCodeData);
                        Bitmap qrCodeImage = qrCode.GetGraphic(12);
                        imageqr = ImageToByte(qrCodeImage);
                    }
                    else
                    {
                        validate = 1;
                        QRCodeGenerator qrGenerator = new QRCodeGenerator();
                        QRCodeData qrCodeData = qrGenerator.CreateQrCode(obj.BookingNumber.ToString(), QRCodeGenerator.ECCLevel.Q);
                        QRCode qrCode = new QRCode(qrCodeData);
                        Bitmap qrCodeImage = qrCode.GetGraphic(12);
                        imageqr = ImageToByte(qrCodeImage);
                    }

                    purchas.Add(new PurchaseQR
                    {
                        Id = obj.Id,
                        UserTypeId = obj.UserTypeId,
                        Date = obj.Date,
                        FilmCode = obj.FilmCode.Trim(),
                        FilmTitle = obj.FilmTitle,
                        CinemaCode = obj.CinemaCode,
                        CinemaTitle = obj.CinemaTitle,
                        Amount = obj.Amount,
                        Tickets = obj.Tickets,
                        BookingNumber = obj.BookingNumber,
                        SessionId = obj.SessionId,
                        QRcode = imageqr,
                        Validate = validate,
                        SessionOBJ2 = obj.SessionOBJ
                    });
                }
            }

            return View(purchas);
        }

        public static byte[] ImageToByte(Bitmap img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        [HttpGet]
        [Throttle(Name = "Eliminar lista de correo", Message = "<br><br><br><br><div style='width:50%;margin:0 auto;border-radius: 25px;text-align:center;color:#FFF;font-size:26px;background-color:#c02328;'><br><img src='/Content/img/logo.png'><br><br> Por favor espere {n} segundos antes de acceder a esta url de nuevo.<br><br><a href='https://www.cinesunidos.com'>Volver al Inicio</a></div>", Seconds = 15)]
        public ActionResult RemoveList(string userId)
        {
            Service services = new Service();
            String response = services.Execute<String>("Security.RemoveList", new { userId = userId });
            ViewBag.Message = response;
            return View();
        }

        /// <summary>
        /// Metodo para enviar el correo de confirmacion de compra desde la vista mis compras
        /// tiene nombre raro debido a que esta expuesto el nombre de la accion.
        /// </summary>
        /// <param name="c">representa el codigo del cine donde sehizo la compra</param>
        /// <param name="b">representa el bookingNumber de la compra</param>
        /// con estos parametros se consulta la tabla tblpurchases de la nube para bircar el objeto UserSession y reenviarlo
        /// <returns>devuelve un json con el resultado de la operacion</returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        [Throttle(Message = "Hemos hecho el envio de esta confrmacion de compra hace poco. Por favor revise su E-mail.", Name = "RecMd", Seconds = 1800)]
        public async Task<JsonResult> RecMd(string c, string b)
        {
            string Result = "";
            string Csql = "select SessionOBJ from tblpurchases where Cinema_strID = @cine and BookingNumber = @booking";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
            {
                SqlCommand cmd = new SqlCommand(Csql, con);
                SqlParameter cine = new SqlParameter
                {
                    ParameterName = "cine",
                    Value = c,
                    DbType = System.Data.DbType.String
                };
                SqlParameter booking = new SqlParameter
                {
                    ParameterName = "booking",
                    Value = b,
                    DbType = System.Data.DbType.String
                };
                cmd.Parameters.Add(cine);
                cmd.Parameters.Add(booking);
                con.Open();
                SqlDataReader r = cmd.ExecuteReader();
                if (r.HasRows)
                {
                    try
                    {
                        r.Read();
                        string u = r["SessionOBJ"].ToString();
                        //UserSession us = JsonConvert.DeserializeObject<UserSession>(u);
                        //string direccion = ConfigurationManager.AppSettings["MailDirection"].ToString();
                        HttpClient cli = new HttpClient();
                        cli.BaseAddress = new Uri(ConfigurationManager.AppSettings["MailDirection"].ToString());
                        cli.DefaultRequestHeaders.Accept.Clear();
                        cli.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                        StringContent contenido = new StringContent(u, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = await cli.PostAsync("/Mailing/SendPurchase", contenido);
                        if (response.IsSuccessStatusCode)
                        {
                            Result = "Se ha enviado la confirmacion de compra a su direccion de email.";
                        }
                        else
                        {
                            Result = "Ocurrio un error en el proceso. por favor intente de nuevo mas tarde.";
                        }
                        contenido.Dispose();
                        response.Dispose();
                        cli.Dispose();
                    }
                    catch (Exception e)
                    {
                        Result = e.Message;
                    }
                }
                else
                {
                    Result = "Lo sentimos; No hemos podido enviar el Email.";
                }
                con.Close();
            };
            return Json(Result);
        }

        public Token IniciarSesion(SignIn model)
        {
            Token token = null;
            if (!string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Password))
            {

                var tokenRepository = new LoginExtension();
                Guid? userId = tokenRepository.FindIdUser(model.Email, model.Password);
                if (userId.HasValue)
                {
                    token = tokenRepository.FindToken(userId.Value, model.Platform);
                    if (token == null)
                    {
                        Guid idHash = Guid.NewGuid();
                        token = new Token()
                        {
                            Id = idHash,
                            UserId = userId.Value,
                            Platform = "Web",
                            Expiration = DateTime.Now.AddHours(24),
                            //Hash = DogFramework.Security.Cryptography.Hash.MD5(idHash.ToString()),
                            Hash = tokenRepository.MD5(idHash.ToString()),
                            Error = ""
                        };
                        tokenRepository.Create(token);
                    }
                    else
                    {
                        token.Expiration = DateTime.Now.AddHours(24);
                        tokenRepository.Update(token);
                    }

                }
                else
                {
                    if (tokenRepository.ExistEmail(model.Email))
                    {
                        if (!tokenRepository.IsUserActive(model.Email))
                        {
                            token = new Token()
                            {
                                Error = "Disculpa, debes formalizar el registro activando tu cuenta a través del enlace enviado a tu correo electrónico"
                            };
                        }
                        else
                        {
                            token = new Token()
                            {
                                Error = "Disculpa, Usuario y/ o Contraseña incorrecta"
                            };
                        }
                    }
                    else
                        token = new Token()
                        {
                            Error = "Disculpa, la dirección de correo electrónico no se encuentra registrada."
                        };
                }

                return token;
            }
            else
            {
                token = new Token()
                {
                    Error = "Login y Password son requeridos"
                };
                return token;
            }

        }


        #region Codigo

        private void getsignininfo(SignIn user)
        {
            Guid? g = getSingInToken(user.Email, user.Password);
            if (!g.HasValue)
            {

            }
        }

        private Guid getSingInToken(string Email, string Password)
        {
            Guid g = Guid.Empty;
            try
            {
                string StringConnection = ConfigurationManager.ConnectionStrings[""].ConnectionString;
                using (SqlConnection connection = new SqlConnection(StringConnection))
                {
                    string Query = string.Format("SELECT [ID] FROM [Access].[Active] WHERE [EMAIL] = {0} AND [PASSWORD] = {1}", Email, Password);
                    using (SqlCommand command = new SqlCommand(Query, connection))
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    g = reader.GetGuid(0);
                                }
                            }
                        }
                    }
                }
                return g;
            }
            catch (SqlException e)
            {
                throw;
            }
        }

        private string validUser(SignIn user)
        {
            try
            {
                string Error = string.Empty;
                if (!checkEmail(user.Email))
                {
                    Error = "Disculpa, la dirección de correo electrónico no se encuentra registrada.";
                }
                if (!checkStatus(user.Email))
                {
                    Error = "Disculpa, debes formalizar el registro activando tu cuenta a través del enlace enviado a tu correo electrónico";
                }
                return Error;
            }
            catch (SqlException e)
            {
                throw;
            }
        }

        private bool checkEmail(string Email)
        {
            bool check = false;
            try
            {
                string StringConnection = ConfigurationManager.ConnectionStrings[""].ConnectionString;
                using (SqlConnection connection = new SqlConnection(StringConnection))
                {
                    string Query = string.Format("SELECT [ID] FROM [Access].[Login] WHERE [EMAIL] = {0}", Email);
                    using (SqlCommand command = new SqlCommand(Query, connection))
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    check = true;
                                }
                            }
                        }
                    }
                }
                return check;
            }
            catch (SqlException e)
            {
                throw;
            }
        }

        private bool checkStatus(string Email)
        {
            bool check = false;
            try
            {
                string StringConnection = ConfigurationManager.ConnectionStrings[""].ConnectionString;
                using (SqlConnection connection = new SqlConnection(StringConnection))
                {
                    string Query = string.Format("SELECT [Active] FROM [Access].[Login] WHERE [EMAIL] = {0}", Email);
                    using (SqlCommand command = new SqlCommand(Query, connection))
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    check = reader.GetBoolean(0);
                                }
                            }
                        }
                    }
                }
                return check;
            }
            catch (SqlException e)
            {
                throw;
            }
        } 
        #endregion

        #endregion Methods

        #region RegisterRoutes

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "SignIn",
                url: "Inicio_Sesion/{ReturnUrl}",
                defaults: new { controller = "Security", action = "SignIn", returnUrl = UrlParameter.Optional });

            routes.MapRoute(
                name: "Signout",
                url: "Cerrar_Sesion",
                defaults: new { controller = "Security", action = "Signout" });

            routes.MapRoute(
                name: "CreateUser",
                url: "Crear_Usuario",
                defaults: new { controller = "Security", action = "User" });

            routes.MapRoute(
                name: "Activate",
                url: "Activacion",
                defaults: new { controller = "Security", action = "ActivateUser" });

            routes.MapRoute(
                name: "UpdateUser",
                url: "Mi_Cuenta/{ReturnUrl}",
                defaults: new { controller = "Security", action = "UpdateUser", returnUrl = UrlParameter.Optional });

            routes.MapRoute(
               name: "Purchases",
               url: "MisCompras",
               defaults: new { controller = "Security", action = "Purchases" });

        }

        #endregion RegisterRoutes
    }
}