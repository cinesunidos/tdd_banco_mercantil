﻿using CinesUnidos.WebRoles.MVC.Models;
using CinesUnidos.WebRoles.MVC.Models.Filters;
using CinesUnidos.WebRoles.MVC.Models.Support;
using System;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Web.Hosting;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Collections.Generic;
using System.Net;
using System.Configuration;

namespace CinesUnidos.WebRoles.MVC.Controllers
{
    //[RequireHttps]
    public class HomeController : Controller
    {
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();

        [HttpGet]
        //////[ExitHttpsIfNotRequired()]
        public ActionResult Index(string city)
        {
            //se evita llamar 11 veces a los servicios para obtener este texto. Despues se mejora trayendolo de una tabla.
            ViewBag.ArrCitiesGeoLoc =
                "var Barquisimeto = { lat: 10.0783, lon: -69.2813, name: 'Barquisimeto'};var Caracas = { lat: 10.44, lon: -66.8356, name: 'Caracas'};var Guatire = { lat: 10.4693, lon: -66.5466, name: 'Guatire' };var Maracaibo = { lat: 10.6001, lon: -71.652, name: 'Maracaibo' };var Maracay = { lat: 10.2615, lon: -67.5878, name: 'Maracay' };var Margarita = { lat: 10.996825, lon: -63.81185, name: 'Margarita' };var Maturin = { lat: 9.7985, lon: -63.2036, name: 'Maturin' };var Puerto_La_Cruz = { lat: 10.21023611, lon: -64.63244999, name: 'Puerto La Cruz' };var Puerto_Ordaz = { lat: 8.29185555, lon: -62.744225, name: 'Puerto Ordaz' };var San_Cristobal = { lat: 7.7943, lon: -72.2346, name: 'San Cristobal' };var Valencia = { lat: 10.1842, lon: -67.9744, name: 'Valencia' };".ToString()
                + "\r\n" + "var arrCities = [Barquisimeto,Caracas,Guatire,Maracaibo,Maracay,Margarita,Maturin,Puerto_La_Cruz,Puerto_Ordaz,San_Cristobal,Valencia ];".ToString();

            // Cookie de ciudad
            if (string.IsNullOrEmpty(city) && ControllerContext.HttpContext.Request.Cookies["ciudadCU"] == null)
                ViewBag.City = "Caracas";
            else if (ControllerContext.HttpContext.Request.Cookies["ciudadCU"] != null && string.IsNullOrEmpty(city))
                ViewBag.City = ControllerContext.HttpContext.Request.Cookies["ciudadCU"].Value.ToString();
            else if (ControllerContext.HttpContext.Request.Cookies["ciudadCU"] != null && !string.IsNullOrEmpty(city))
                ViewBag.City = city;

            #region Llamar el servicio para mensajes en el home
            // Service services = new Service();
            // string res = services.Execute<string>("MessagesForCinema.Get", new { token = TokenServices });
            // if (res == null)
            // {
            //     ViewBag.cityt = String.Empty;
            //     ViewBag.messagecity = String.Empty;
            // }
            // if (res != "N/A" || res.ToString().Length > 10)
            // {
            //     var listMessages = JsonConvert.DeserializeObject<List<MessageForCinema>>(res);
            //     MessageForCinema mess = listMessages.Where(m => m.Category == Category.GlobalMessage).SingleOrDefault();
            //     if (mess != null)
            //     {
            //         ViewBag.GlobalMessage = mess.Message;
            //     }
            // }
            #endregion

            return View();
        }

        [HttpGet]
        //////[ExitHttpsIfNotRequired()]
        public ActionResult Slider(string city)
        {
            try
            {
                ViewBag.City = city;
                MovieBillboard[] movies;
                switch (city.ToLower())
                {
                    case "barquisimeto":
                        movies = SliderCacheClass.SliderBarquisimeto == null ? SliderCacheClass.SliderBarquisimeto = GetCacheSlider(city) : SliderCacheClass.SliderBarquisimeto;
                        return PartialView(movies);
                    case "caracas":
                        movies = SliderCacheClass.SliderCaracas == null ? SliderCacheClass.SliderCaracas = GetCacheSlider(city) : SliderCacheClass.SliderCaracas;
                        return PartialView(movies);
                    case "guatire":
                        movies = SliderCacheClass.SliderGuatire == null ? SliderCacheClass.SliderGuatire = GetCacheSlider(city) : SliderCacheClass.SliderGuatire;
                        return PartialView(movies);
                    case "maracaibo":
                        movies = SliderCacheClass.SliderMaracaibo == null ? SliderCacheClass.SliderMaracaibo = GetCacheSlider(city) : SliderCacheClass.SliderMaracaibo;
                        return PartialView(movies);
                    case "maracay":
                        movies = SliderCacheClass.SliderMaracay == null ? SliderCacheClass.SliderMaracay = GetCacheSlider(city) : SliderCacheClass.SliderMaracay;
                        return PartialView(movies);
                    case "margarita":
                        movies = SliderCacheClass.SliderMargarita == null ? SliderCacheClass.SliderMargarita = GetCacheSlider(city) : SliderCacheClass.SliderMargarita;
                        return PartialView(movies);
                    case "maturin":
                        movies = SliderCacheClass.SliderMaturin == null ? SliderCacheClass.SliderMaturin = GetCacheSlider(city) : SliderCacheClass.SliderMaturin;
                        return PartialView(movies);
                    case "puerto la cruz":
                        movies = SliderCacheClass.SliderPuertoLaCruz == null ? SliderCacheClass.SliderPuertoLaCruz = GetCacheSlider(city) : SliderCacheClass.SliderPuertoLaCruz;
                        return PartialView(movies);
                    case "puerto ordaz":
                        movies = SliderCacheClass.SliderPuertoOrdaz == null ? SliderCacheClass.SliderPuertoOrdaz = GetCacheSlider(city) : SliderCacheClass.SliderPuertoOrdaz;
                        return PartialView(movies);
                    case "san cristobal":
                        movies = SliderCacheClass.SliderSanCristobal == null ? SliderCacheClass.SliderSanCristobal = GetCacheSlider(city) : SliderCacheClass.SliderSanCristobal;
                        return PartialView(movies);
                    case "valencia":
                        movies = SliderCacheClass.SliderValencia == null ? SliderCacheClass.SliderValencia = GetCacheSlider(city) : SliderCacheClass.SliderValencia;
                        return PartialView(movies);
                    default:
                        movies = SliderCacheClass.SliderCaracas == null ? SliderCacheClass.SliderCaracas = GetCacheSlider(city) : SliderCacheClass.SliderCaracas;
                        return PartialView(movies);
                }
            }
            catch (Exception e)
            {
                Error modelError = new Error();
                modelError.Code = 00100;
                modelError.Message = string.Format("Error Message: {0} || Inner Exception: {1}", e.Message, e.InnerException);
                return RedirectToAction("_Error", "Purchase", modelError);
            }
        }

        /// <summary>
        /// Ramiro Marimon - metodo para crear el caché del slider.
        /// </summary>
        /// <param name="city">ciudad a la que quiero consultar el slider</param>
        /// <returns></returns>
        private MovieBillboard[] GetCacheSlider(string city)
        {
            try
            {
                Service services = new Service();
                MovieBillboard[] movies = services.Execute<MovieBillboard[]>("Movie.Slider", new { city = city, token = TokenServices });
                return movies.Where(m => m != null).ToArray();
            }
            catch (WebException e)
            {
                throw;
            }
        }

        //Felix Rosales, vistas de TuBoletoEnLinea
        [HttpGet]
        ////[ExitHttpsIfNotRequired()]
        public ActionResult ConsultaBoleto()
        {
            return View();
        }

        [HttpPost]
        ////[ExitHttpsIfNotRequired()]
        public ActionResult ConsultaBoleto(string Stock_strBarcode)
        {
            DatosBoleto boleto = new DatosBoleto();
            boleto.Stock_strBarcode = Stock_strBarcode;
            boleto.token = ConfigurationManager.AppSettings["TokenServices"].ToString();
            Service Client = new Service();
            boleto = Client.Execute<DatosBoleto, DatosBoleto>("Security.Passvalidation", boleto);
            return View(boleto);
        }


        [HttpGet]
        // ////[ExitHttpsIfNotRequired()]
        public JsonResult Messages(string loc)
        {
            Service services = new Service();
            MessageForCinema message = null;
            string messages = services.Execute<string>("MessagesForCinema.Get", new { token = TokenServices });
            if (messages == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            if (messages != "N/A" || messages.ToString().Length > 10)
            {
                var li = JsonConvert.DeserializeObject<List<MessageForCinema>>(messages);
                message = li.Where(o => o.CinemaID == loc || o.CityString == loc).SingleOrDefault();
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        ////[ExitHttpsIfNotRequired()]
        public ActionResult MoreMovie(string city)
        {
            if (String.IsNullOrEmpty(city))
            {
                return RedirectToAction("Index");
            }
            ViewBag.City = city;
            Movie[] movies;
            switch (city.ToLower())
            {
                case "barquisimeto":
                    movies = MoreMoviesCacheClass.MoreBarquisimeto == null ? MoreMoviesCacheClass.MoreBarquisimeto = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreBarquisimeto;
                    return PartialView(movies);
                case "caracas":
                    movies = MoreMoviesCacheClass.MoreCaracas == null ? MoreMoviesCacheClass.MoreCaracas = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreCaracas;
                    return PartialView(movies);
                case "guatire":
                    movies = MoreMoviesCacheClass.MoreGuatire == null ? MoreMoviesCacheClass.MoreGuatire = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreGuatire;
                    return PartialView(movies);
                case "maracaibo":
                    movies = MoreMoviesCacheClass.MoreMaracaibo == null ? MoreMoviesCacheClass.MoreMaracaibo = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreMaracaibo;
                    return PartialView(movies);
                case "maracay":
                    movies = MoreMoviesCacheClass.MoreMaracay == null ? MoreMoviesCacheClass.MoreMaracay = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreMaracay;
                    return PartialView(movies);
                case "margarita":
                    movies = MoreMoviesCacheClass.MoreMargarita == null ? MoreMoviesCacheClass.MoreMargarita = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreMargarita;
                    return PartialView(movies);
                case "maturin":
                    movies = MoreMoviesCacheClass.MoreMaturin == null ? MoreMoviesCacheClass.MoreMaturin = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreMaturin;
                    return PartialView(movies);
                case "puerto la cruz":
                    movies = MoreMoviesCacheClass.MorePuertoLaCruz == null ? MoreMoviesCacheClass.MorePuertoLaCruz = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MorePuertoLaCruz;
                    return PartialView(movies);
                case "puerto ordaz":
                    movies = MoreMoviesCacheClass.MorePuertoOrdaz == null ? MoreMoviesCacheClass.MorePuertoOrdaz = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MorePuertoOrdaz;
                    return PartialView(movies);
                case "san cristobal":
                    movies = MoreMoviesCacheClass.MoreSanCristobal == null ? MoreMoviesCacheClass.MoreSanCristobal = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreSanCristobal;
                    return PartialView(movies);
                case "valencia":
                    movies = MoreMoviesCacheClass.MoreValencia == null ? MoreMoviesCacheClass.MoreValencia = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreValencia;
                    return PartialView(movies);
                default:
                    movies = MoreMoviesCacheClass.MoreCaracas == null ? MoreMoviesCacheClass.MoreCaracas = GetMoreMoviesCache(city) : MoreMoviesCacheClass.MoreCaracas;
                    return PartialView(movies);
            }
        }

        /// <summary>
        /// Ramiro Marimón - Metodo para generar el moremovies y almacenarlo en cache.
        /// </summary>
        /// <param name="city">la ciudad a la que quiero generar el moremovies</param>
        /// <returns></returns>
        private Movie[] GetMoreMoviesCache(string city)
        {
            Service services = new Service();
            Movie[] movies = services.Execute<Movie[]>("Movie.MoreMovie", new { city = city, token = TokenServices });
            return movies.Where(m => m != null).ToArray();
        }

        private bool CompararHoras(string filename, double hours)
        {
            ////  var fecha = DateTime.Now.AddHours(-hours);
            ////return System.IO.File.GetLastWriteTime(filename).AddHours(-hours) <= fecha;

            bool value;

            DateTime DateSystem = DateTime.Now;
            DateTime DateFile = System.IO.File.GetLastWriteTime(filename);

            TimeSpan result = DateSystem - DateFile;

            if (result.TotalHours >= hours) value = true; else value = false;

            return value;

        }

        /// <summary>
        ///  Obtiene los datos del archivo de cache, si esta vacio o no existe lo crea y lo llena.
        /// </summary>
        /// <param name="city">Nombre de la ciudad</param>
        /// <param name="service">Nombre del servicio que se llama cuando vence la cache</param>
        /// <param name="filename">Nombre del archivo donde se encuenta la data</param>
        /// Ejemplo de uso:
        /// GetCacheArray<Movie>("Caracas", "Movie.MoreMovie", "Movie.MoreMovie.xml", )
        /// <returns></returns>
        /// 

        public dynamic GetCacheArray<T>(string city, string service, string filename, dynamic parameters)
        {
            T[] valueObject = null;
            string filePath = "";
            string cachePath = string.Empty;

            //#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_Data/" + city + "." + filename));
            //#else
            //             if (System.Configuration.ConfigurationManager.AppSettings["DirectoryKeys"].ToString().Equals("Cloud"))
            //            {
            //                if (city != null)
            //                {
            //                    cachePath = RoleEnvironment.GetLocalResource(city).RootPath;
            //                }
            //                else
            //                {
            //                    cachePath = RoleEnvironment.GetLocalResource("DataWeb").RootPath;
            //                }
            //                filePath = Path.Combine(cachePath, filename);
            //            }
            //#endif



            if (!System.IO.File.Exists(filePath))
                System.IO.File.Create(filePath).Close();

            Boolean valido = CompararHoras(filePath, 1);
            FileInfo file = new FileInfo(filePath);

            if (valido || file.Length == 0)
            {
                Service services = new Service();
                if (city != null && parameters != null)
                {
                    valueObject = services.Execute<T[]>(service, parameters);
                }

                if (city == null && parameters == null)
                {
                    valueObject = services.Execute<T[]>(service);
                }

                string json = JsonConvert.SerializeObject(valueObject);
                System.IO.File.WriteAllText(filePath, json);
            }
            else
            {
                valueObject = JsonConvert.DeserializeObject<T[]>(System.IO.File.ReadAllText(filePath));
            }
            return valueObject;
        }

        [HttpGet]
        ////[ExitHttpsIfNotRequired()]
        public ActionResult Releases()
        {
            Premier[] premiers;
            premiers = PremierCacheClass.Premiers == null ? PremierCacheClass.Premiers = GetPremiers() : PremierCacheClass.Premiers;
            return View(premiers);
        }

        /// <summary>
        /// Metodo para generar la caché de peliclas en el front
        /// </summary>
        /// <returns></returns>
        private Premier[] GetPremiers()
        {
            Service services = new Service();
            Premier[] premiers = services.Execute<Premier[]>("Premier.GetAll", new { token = TokenServices });
            return premiers.Where(p => p != null).ToArray();
        }

        [HttpGet]
        //[ExitHttpsIfNotRequired(80)]
        public ActionResult Movie(string city)
        {

            if (String.IsNullOrEmpty(city))
            {
                return RedirectToAction("Index");
            }

            ViewBag.City = city;

            #region BuscarMensajesDelCmsParaLaCiudadSeleccionada
            Service services = new Service();
            string res = services.Execute<string>("MessagesForCinema.Get", new { token = TokenServices });
            if (res == null)
            {
                ViewBag.cityt = String.Empty;
                ViewBag.messagecity = String.Empty;
            }
            if (res != "N/A" || res.ToString().Length > 10)
            {
                var listMessages = JsonConvert.DeserializeObject<List<MessageForCinema>>(res);
                MessageForCinema mess = listMessages.Where(m => m.CityString == city).SingleOrDefault();
                if (mess != null)
                {
                    ViewBag.city = city;
                    ViewBag.citymessage = mess.Message;
                }
            }
            #endregion

            #region LeerCache de las peliculas o buscarlas en capa 2
            MovieBillboard[] theater;
            switch (city.ToLower())
            {
                case "barquisimeto":
                    theater = MovieCacheClass.MoviesBarquisimeto == null ? MovieCacheClass.MoviesBarquisimeto = GetMoviesCache(city) : MovieCacheClass.MoviesBarquisimeto;
                    return View(theater);
                case "caracas":
                    theater = MovieCacheClass.MoviesCaracas == null ? MovieCacheClass.MoviesCaracas = GetMoviesCache(city) : MovieCacheClass.MoviesCaracas;
                    return View(theater);
                case "guatire":
                    theater = MovieCacheClass.MoviesGuatire == null ? MovieCacheClass.MoviesGuatire = GetMoviesCache(city) : MovieCacheClass.MoviesGuatire;
                    return View(theater);
                case "maracaibo":
                    theater = MovieCacheClass.MoviesMaracaibo == null ? MovieCacheClass.MoviesMaracaibo = GetMoviesCache(city) : MovieCacheClass.MoviesMaracaibo;
                    return View(theater);
                case "maracay":
                    theater = MovieCacheClass.MoviesMaracay == null ? MovieCacheClass.MoviesMaracay = GetMoviesCache(city) : MovieCacheClass.MoviesMaracay;
                    return View(theater);
                case "margarita":
                    theater = MovieCacheClass.MoviesMargarita == null ? MovieCacheClass.MoviesMargarita = GetMoviesCache(city) : MovieCacheClass.MoviesMargarita;
                    return View(theater);
                case "maturin":
                    theater = MovieCacheClass.MoviesMaturin == null ? MovieCacheClass.MoviesMaturin = GetMoviesCache(city) : MovieCacheClass.MoviesMaturin;
                    return View(theater);
                case "puerto la cruz":
                    theater = MovieCacheClass.MoviesPuertoLaCruz == null ? MovieCacheClass.MoviesPuertoLaCruz = GetMoviesCache(city) : MovieCacheClass.MoviesPuertoLaCruz;
                    return View(theater);
                case "puerto ordaz":
                    theater = MovieCacheClass.MoviesPuertoOrdaz == null ? MovieCacheClass.MoviesPuertoOrdaz = GetMoviesCache(city) : MovieCacheClass.MoviesPuertoOrdaz;
                    return View(theater);
                case "san cristobal":
                    theater = MovieCacheClass.MoviesSanCristobal == null ? MovieCacheClass.MoviesSanCristobal = GetMoviesCache(city) : MovieCacheClass.MoviesSanCristobal;
                    return View(theater);
                case "valencia":
                    theater = MovieCacheClass.MoviesValencia == null ? MovieCacheClass.MoviesValencia = GetMoviesCache(city) : MovieCacheClass.MoviesValencia;
                    return View(theater);
                default:
                    theater = MovieCacheClass.MoviesCaracas == null ? MovieCacheClass.MoviesCaracas = GetMoviesCache(city) : MovieCacheClass.MoviesCaracas;
                    return View(theater);
            }
            #endregion
            //return View(theater);
        }

        /// <summary>
        /// Metodo para generar el caché de la seccion peliculas
        /// </summary>
        /// <param name="city">la ciudad en la cual voy a generar el caché</param>
        /// <returns></returns>
        private MovieBillboard[] GetMoviesCache(string city)
        {
            DateTime date = DateTime.UtcNow.TimeZoneCaracas();
            int day = date.Day;
            int month = date.Month;
            Service services = new Service();
            MovieBillboard[] theater = services.Execute<MovieBillboard[]>("Movie.GetAllCityDate", new { city = city, day = day, month = month, token = TokenServices });

            if (theater.Count().Equals(0))
            {
                day = DateTime.Now.AddDays(1).Day;
                month = DateTime.Now.AddDays(1).Month;
                theater = services.Execute<MovieBillboard[]>("Movie.GetAllCityDate", new { city = city, day = day, month = month, token = TokenServices });
            }

            return theater.Where(t => t != null).ToArray();
        }

        [HttpGet]
        //[ExitHttpsIfNotRequired(80)]
        public ActionResult DetailMovie(string city, string movieId)
        {
            if (String.IsNullOrEmpty(city) || String.IsNullOrEmpty(movieId))
            {
                return RedirectToAction("Index");
            }

            ViewBag.City = city;
            Service services = new Service();
            MovieBillboard billboard = services.Execute<MovieBillboard>("Movie.GetAllById", new { city = city, movieId = movieId, token = TokenServices });
            return View(billboard);
        }

        [HttpGet]
        //[ExitHttpsIfNotRequired(80)]
        public ActionResult Theater(string city)
        {

            if (String.IsNullOrEmpty(city))
            {
                return RedirectToAction("Index");
            }

            ViewBag.City = city;

            Service services = new Service();

            string messages = services.Execute<string>("MessagesForCinema.Get", new { token = TokenServices });

            TheaterBillboard[] theater = services.Execute<TheaterBillboard[]>("Theater.GetAllByCity", new { city = city, token = TokenServices });

            //TheaterBillboard[] theater = GetCacheArray<TheaterBillboard>(city, "Theater.GetAllByCity", "Theater.GetAllByCity.xml",
            //new { city = city });

            // Halls: evaluar si es mixto


            // View
            //Service services1 = new Service();
            //string c = theater[0].Theater.Id;

            //if (messages == null)
            //{
            //    ViewBag.cityt = String.Empty;
            //    ViewBag.messagecity = String.Empty;
            //}

            //if (messages != "N/A" || messages.ToString().Length > 10)
            //{
            //    var li = JsonConvert.DeserializeObject<List<MessageForCinema>>(messages);
            //    var mensage = li.Where(m => m.CinemaID == c).SingleOrDefault();
            //    if (mensage != null)
            //    {
            //        ViewBag.cinemaa = mensage.Cinema.ToString().Substring(0, mensage.Cinema.ToString().Length - 4).Replace("_", " ");
            //        ViewBag.message = mensage.Message;
            //    }
            //    var messagecity = li.Where(m => m.CityString == city).SingleOrDefault();
            //    if (messagecity != null)
            //    {
            //        ViewBag.cityt = messagecity.CityString;
            //        ViewBag.messagecity = messagecity.Message;
            //    }
            //}

            if (theater.Length > 0)
            {

                return View(theater);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        //[ExitHttpsIfNotRequired(80)]
        public ActionResult Search(string city, string id, int day, int month)
        {
            Service services = new Service();
            MovieBillboard movie = services.Execute<MovieBillboard>("Movie.GetAllByDate", new { city = city, movieId = id, day = day, month = month, token = TokenServices });
            return PartialView("Sessions", movie);
        }

        [HttpGet]
        //[ExitHttpsIfNotRequired(80)]
        public ActionResult SearchTheater(string city, string id, int day, int month)
        {
            ViewBag.City = city;
            Service services = new Service();
            TheaterBillboard theater = services.Execute<TheaterBillboard>("Theater.GetAllByDate", new { theaterId = id, day = day, month = month, token = TokenServices });
            return PartialView("TheaterSessions", theater);
        }

        //[HttpGet]
        //////[ExitHttpsIfNotRequired()]
        //public ActionResult PreventaAvengers(string city)
        //{
        //    ViewBag.City = city;
        //    return View();
        //}

        [HttpGet]
        //[ExitHttpsIfNotRequired()]
        public ActionResult Promotion(string city)
        {
            Service ser = new Service();
            string res = ser.Execute<string>("MessagesForCinema.Get", new { token = TokenServices });
            if (res != "N/A" || res.ToString().Length > 10)
            {
                var listMessages = JsonConvert.DeserializeObject<List<MessageForCinema>>(res);
                MessageForCinema mess = listMessages.Where(m => m.Category.ToString() == "GlobalMessage").SingleOrDefault();
                if (mess != null)
                {
                    ViewBag.global = mess.Message;
                }
            }
            ViewBag.City = city;
            return View();
        }

        #region Promo Desorden Publico

        /// <summary>
        /// Metodo GET de la Vista Desorden
        /// </summary>
        /// <returns>Este Metodo Retorna La Vista Desorden</returns>
        //[HttpGet]
        //////[ExitHttpsIfNotRequired()]
        //public ActionResult Desorden() { return View(); }

        //[HttpPost]
        //////[ExitHttpsIfNotRequired()]
        //public ActionResult Desorden(int TransactionId, int CinemaId)
        //{
        //    PromoPack Pack = new PromoPack();
        //    Pack.CinemaId = CinemaId;
        //    Pack.TransactionId = TransactionId;
        //    Pack.Token = ConfigurationManager.AppSettings["TokenServices"].ToString();
        //    Service Client = new Service();
        //    Pack = Client.Execute<PromoPack, PromoPack>("Concessions.PromoDesorden", Pack);
        //    if (Pack.Status)
        //    {
        //        ///habias puesto AppData en lugar de App_data -.-
        //        string filepath = Server.MapPath("~/App_Data") + "\\DP_4_Ska _Mundo_Ska.mp3";
        //        byte[] filedata = System.IO.File.ReadAllBytes(filepath);
        //        string contentType = System.Web.MimeMapping.GetMimeMapping(filepath);
        //        var cd = new System.Net.Mime.ContentDisposition
        //        {
        //            FileName = "DP_4_Ska _Mundo_Ska.mp3",
        //            Inline = false,
        //        };
        //        Response.AppendHeader("Content-Disposition", cd.ToString());
        //        return File(filedata, contentType);
        //    }
        //    else
        //    {
        //        ViewBag.Mensaje = Pack.Mensaje;
        //        return View();
        //    }
        //}

        #endregion

        #region RegisterRoutes

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               name: "SearchTheater",
               url: "Home/SearchTheater/{city}/{id}/{day}/{month}",
               defaults: new { controller = "Home", action = "SearchTheater" });
            routes.MapRoute(
                name: "Search",
                url: "Home/Search/{city}/{id}/{day}/{month}",
                defaults: new { controller = "Home", action = "Search" });
            routes.MapRoute(
                name: "Index",
                url: "Inicio/{city}",
                defaults: new { controller = "Home", action = "Index", city = UrlParameter.Optional });
            routes.MapRoute(
                name: "DetailMovie",
                url: "Pelicula/{city}/{movieId}",
                defaults: new { controller = "Home", action = "DetailMovie" });
            routes.MapRoute(
                name: "Movie",
                url: "Peliculas/{city}",
                defaults: new { controller = "Home", action = "Movie" });
            routes.MapRoute(
                name: "Theater",
                url: "Cines/{city}",
                defaults: new { controller = "Home", action = "Theater" });
            routes.MapRoute(
                name: "Releases",
                url: "Estrenos",
                defaults: new { controller = "Home", action = "Releases" });
            routes.MapRoute(
                name: "Desorden",
                url: "Desorden",
                defaults: new { controller = "Home", action = "Desorden" });
        }

        #endregion RegisterRoutes

    }
}