﻿using CinesUnidos.WebRoles.MVC.Models;
using System.Web.Mvc;


namespace CinesUnidos.WebRoles.MVC.Controllers
{
    public class ErrorsController : Controller
    {
        public ActionResult Error400()
        {
            Error model = new Error();
            model.Code = 400;
            model.Message = "Lo sentimos, está página no está disponible. Es posible que el enlace que seguiste se haya roto o haya sido eliminado.";
            return View("_Error", model);
        }

        public ActionResult Error401()
        {
            Error model = new Error();
            model.Code = 401;
            model.Message = "Lo sentimos, debes realizar tu registro para realizar la compra";
            return PartialView("_Error", model);
        }

        public ActionResult Error403()
        {
            Error model = new Error();
            model.Code = 403;
            model.Message = "PROHIBIDO";
            return View("_Error", model);
        }

        public ActionResult Error404()
        {
            Error model = new Error();
            model.Code = 404;
            model.Message = "Lo sentimos, esta página no está disponible. Es posible que el enlace que seguiste se haya roto o haya sido eliminado.";
            return View("_Error", model);
        }

        public ActionResult Error500()
        {
            Error model = new Error();
            model.Code = 500;
            model.Message = "Lo sentimos, actualmente presentamos problemas de conexión con el cine. Por favor intente nuevamente";
            return View("_Error", model);
        }

        public ActionResult Error501()
        {
            Error model = new Error();
            model.Code = 501;
            model.Message = "Disculpa, su navegador no es compatible con nuestra versión de página web";
            return View("_Error", model);
        }

        public ActionResult Error503()
        {
            Error model = new Error();
            model.Code = 503;
            model.Message = "Lo sentimos, actualmente hemos perdido la conexión con el cine. Por favor intente nuevamente.";
            return View("_Error", model);
        }

        public ActionResult Error209()
        {
            Error model = new Error();
            model.Code = 209;
            model.Message = "Lo sentimos, el tiempo de la sesión de compra ha expirado.";
            model.Message2 = "Le Invitamos a verificar los boletos adquiridos ingresando en la sección “Mi Cuenta” a través de la opción “Mis Compras” del menú superior.";
            return View("_Error", model);
        }
        /// <summary>
        ///  LUIS RAMIREZ
        ///  27/10/2017
        ///  EL ERROR 300 ESTA DEDICADO PARA LA SECCIÓN DE CARAMELERÍA WEB, INDICA QUE UN PRODUCTO NO TIENE LA CANTIDAD DISPONIBLE EN EL ALMACÉN
        /// </summary>
        /// <returns></returns>
        public ActionResult Error300()
        {
            Error model = new Error();
            model.Code = 300;
            model.Message = "Lo sentimos, no hay disponibilidad para la cantidad de producto(s) seleccionado(s).";
            return View("_Error", model);
        }

        public ActionResult Error301()
        {
            Error model = new Error();
            model.Code = 301;
            model.Message = "Lo sentimos, se presentó un problema al reservar su(s) producto(s), por favor reinicie la compra";
            //model.Message = "Lo sentimos, ";
            return View("_Error", model);
        }
        public ActionResult Error302()
        {
            Error model = new Error();
            model.Code = 302;
            model.Message = "Lo sentimos, se ha generado un error al generar operación de devolución";
            return View("_Error", model);
        }
        //Error 900, este error indica cuando hay problemas al cargar la butaca - Son errores que son originados desde Vista
        public ActionResult Error900()
        {
            Error model = new Error();
            model.Code = 900;
            model.Message = "Lo sentimos, "+ model.Message;
            return View("_Error", model);
        }

    }
}