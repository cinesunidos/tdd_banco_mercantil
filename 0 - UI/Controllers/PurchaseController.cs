﻿using CinesUnidos.WebRoles.MVC.Models;
using CinesUnidos.WebRoles.MVC.Models.Filters;
using CinesUnidos.WebRoles.MVC.Models.Support;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Hosting;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.ServiceRuntime;
using QRCoder;
using System.IO;
using System.Drawing;
using System.Net.Http;
using System.Net;
//using CinesUnidos.ESB.Infrastructure.Agents.Live;
using System.Linq;
using Newtonsoft.Json;
using System.Web.Http.Cors;
using System.Security.Cryptography;
using System.Text;

namespace CinesUnidos.WebRoles.MVC.Controllers
{
    public class PurchaseController : Controller
    {
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();

        /// <summary>
        /// Se Obtiene Todos Los Boletos Asociados a la Funcion Seleccionada
        /// </summary>
        /// <param name="theaterId">Codigo de Cine</param>
        /// <param name="sessionId">Codigo de Funcion</param>
        /// <returns></returns>
        [HttpGet()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult GetTicket(string theaterId, int sessionId)
        {
            //validar que la solicitud de boletos se realize antes de las 11pm y despues de las 6am
            //TimeZoneInfo timeZone = System.TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            //DateTime nowDateTime = DateTime.Now;
            //DateTime newDateTime = TimeZoneInfo.ConvertTime(nowDateTime, timeZone);
            //if (newDateTime.Hour < int.Parse(ConfigurationManager.AppSettings["MinHour"]) || newDateTime.Hour > int.Parse(ConfigurationManager.AppSettings["MaxHour"]))
            //{
            //    Error modelError = new Error();
            //    modelError.Code = 303;
            //    modelError.Message = "Lo sentimos, el horario de compras se encuentra restringido entre las 11:00 PM y 6:00 AM.";
            //    return View("_Error", modelError);
            //}

            var ShowVistaPIR = ConfigurationManager.AppSettings["ShowVistaPIR"];

            if (theaterId != "1028" && theaterId != "1030" && ShowVistaPIR == "true")
            {
                Error modelError = new Error();
                modelError.Code = 304;
                modelError.Message = "Lo sentimos, no se pueden realizar compras en este cine";
                return View("_Error", modelError);
            }

            Service services = new Service();
            SessionInfo sessionInfo = services.Execute<SessionInfoRS>("Theater.GetSession", new { theaterId = theaterId, sessionId = sessionId, token = TokenServices });
            //TODO: Luis Blanco
            //Solucion Temporal del Problema de la Zona Horaria en la pantalla de Seleccion de Tickets se estaba restando 30 min
            //Una vez aplicado el cambio de la zona horaria en el cloud services comentar esta linea de codigo         
            //sessionInfo.ShowTime = sessionInfo.ShowTime.AddMinutes(30);

            if (sessionInfo == null)
            {
                Error modelError = new Error();
                modelError.Code = 209;
                modelError.Message = "Lo sentimos, el tiempo de la sesión de compra ha expirado.";
                ViewBag.TheaterId = theaterId;
                ViewBag.SessionId = sessionId;
                return View("_Error", modelError);
            }

            //eliminar los tickets de promocion para una pelicula con un HO especifico..
            //if (sessionInfo.Movie.Id == "HO00003666")
            //{
            //    sessionInfo.Purchase.Tickets = sessionInfo.Purchase.Tickets.Where(t => !t.Name.Contains("2x1") && !t.Name.Contains("3x1")).ToArray();
            //}

            if (!sessionInfo.OnSale)
            {
                Error model = new Error();
                model.Code = 503;
                model.Message = "Servicio no disponible";
                return View("_Error", model);
            }
            if (sessionInfo.Theater.Id != "") ViewData["theater"] = sessionInfo.Theater.Id;
            //Luis Ramirez, 02/11/2017. Incluir variable de session que capture el código del cine y función TS = TheaterId and SessionId
            if ((Session["Theater"] != null) && (Session["Session"] != null))
            {
                Session["Theater"] = null;
                Session["Session"] = null;
            }

            Session["Theater"] = sessionInfo.Theater.Id;
            Session["Session"] = sessionInfo.Id.ToString();
            Session["Vouchers"] = new List<InfoVoucher>();
            return View(sessionInfo);
        }




        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()] 
#endif
        public JsonResult BDVPayment(BDVConfirmation bdvConfirm)
        {
            UserEntity userEntity = System.Web.HttpContext.Current.Session["UserInfo"] as UserEntity;
            PaymentInfomation infomation = (PaymentInfomation)bdvConfirm;
            infomation.IdCardType = userEntity.IdCardType;
            infomation.IdCardNumber = userEntity.IdCardNumber;
            infomation.Email = userEntity.Email;
            infomation.CellPhone = userEntity.Phone;
            infomation.CreditCardName = userEntity.Name + " " + userEntity.LastName;
            var a = Payments(infomation);
            return Json("");
        }





        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()] 
#endif
        public ActionResult BDVGeneratePayment(string id)
        {
            UserEntity userEntity = Session["UserInfo"] as UserEntity;
            if (userEntity == null)
            {
                return Json("Null");
            }

            BDVUserInfo userInfo = new BDVUserInfo()
            {
                ClientId = id,
                IdCardType = userEntity.IdCardType,
                IdCardNumber = userEntity.IdCardNumber,
                Cellphone = userEntity.Phone,
                Email = userEntity.Email
            };
            Service services = new Service();
            string response = services.Execute<BDVUserInfo, string>("UserSession.BDVGeneratePayment", userInfo);
            return Json(response);
        }



        #region MetodosDeTrabajoParaCanjeDeVouchers

        /// <summary>
        /// RM - Mrtodo para validar Vouchers electronicoa desde el frontEnd.
        /// el nombre del metodo es poco convencional para no revelar mucha informacionde cara al front.
        /// </summary>
        /// <param name="VoNo">el codigo numerico del voucher</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult VlVoVi(string VoNo)
        {
            Service s = new Service();
            var a = s.Execute<InfoVoucher>("Ticket.GetTicketForBarcode",
                new
                {
                    token = TokenServices,
                    cinemaid = Session["Theater"].ToString(),
                    sessionid = Session["Session"].ToString(),
                    voucher = VoNo
                });
            if (a.Redimido == false && a.ValidoParafuncion == true)
            {
                AddVoucherToOrder(a);
            }
            return Json(a, JsonRequestBehavior.AllowGet);
        }

        private void AddVoucherToOrder(InfoVoucher i)
        {
            try
            {
                var vs = Session["Vouchers"] as List<InfoVoucher>;
                if (i.IsMultiVoucher == false)
                {
                    //para los que no son multivouchers automaticamente le coloco cantidad a redimir = 1
                    i.Cant_Redimir = 1;
                }
                vs.Add(i);
                Session["Vouchers"] = vs;
            }
            catch (Exception e)
            {
            }
        }

        [HttpGet]
        public JsonResult UpdateMultiVoucherQtty(string acc, string voucherCode)
        {
            var lista = Session["Vouchers"] as List<InfoVoucher>;
            if (lista == null)
            {
                return Json(new
                {
                    mensaje = "Redirect",
                    success = false
                }, JsonRequestBehavior.AllowGet);
            }
            var existe = lista.Where(v => v.VoucherCode == voucherCode).SingleOrDefault();
            if (existe == null)
            {
                return Json(new
                {
                    mensaje = "Error: El Multivoucher no pudo ser verificado",
                    success = false
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                int id = lista.IndexOf(existe);
                //aumentar Cantidad a redimir en multiVoucher
                if (acc == "+")
                {
                    var valorMaximo = lista[id].Cant_Redimir == lista[id].EntradasRestantes ? true : false;
                    if (valorMaximo)
                    {
                        string singularOrPlural = lista[id].EntradasRestantes == 1 ? "vez" : "veces";
                        return Json(new
                        {
                            mensaje = string.Format("Error: El Código Electrónico no puede se canjeado mas de {0} {1}", lista[id].EntradasRestantes, singularOrPlural),
                            success = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        lista[id].Cant_Redimir += 1;
                        Session["Vouchers"] = lista;
                        return Json(new
                        {
                            mensaje = "Ok",
                            success = true
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                //disminuir Cantidad a redimir en multiVoucher
                if (acc == "-")
                {
                    var valorMinimo = lista[id].Cant_Redimir == 0 ? true : false;
                    if (valorMinimo)
                    {
                        return Json(new
                        {
                            mensaje = "Error: El vouchwer no puede se canjeado 0 veces",
                            success = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        lista[id].Cant_Redimir -= 1;
                        Session["Vouchers"] = lista;
                        return Json(new
                        {
                            mensaje = "Ok",
                            success = true
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new
            {
                mensaje = "Error: Descpnocido",
                success = false
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult RemoveVoucher(string voucherNum)
        {
            try
            {
                var listaRemover = Session["Vouchers"] as List<InfoVoucher>;
                var existe = listaRemover.Where(l => l.VoucherCode == voucherNum).SingleOrDefault();
                if (existe == null)
                {
                    return Json(new
                    {
                        mensaje = "Error: El voucher no está en la lista",
                        success = false
                    }, JsonRequestBehavior.AllowGet);
                }

                var idx = listaRemover.IndexOf(existe);
                listaRemover.RemoveAt(idx);
                Session["Vouchers"] = listaRemover;
                return Json(new
                {
                    mensaje = "Ok",
                    success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    mensaje = e.Message,
                    success = false
                }, JsonRequestBehavior.AllowGet);

            }

        }
        #endregion

        private SessionInfo LoadVouchersToOrder(SessionInfo s)
        {
            List<InfoVoucher> vouchers = Session["Vouchers"] as List<InfoVoucher>;
            if (vouchers == null || vouchers.Count <= 0)
            {
                return s;
            }

            int cant = vouchers.Sum(v => v.Cant_Redimir);
            if (cant <= 0)
            {
                return s;
            }
            var ticketTypeList = vouchers.Select(v => v.CodigoBoletoParaCanje).Distinct().ToList();
            foreach (var tipo in ticketTypeList)
            {
                var boletosDelTipo = vouchers.Where(v => v.CodigoBoletoParaCanje == tipo).ToList();
                if (boletosDelTipo == null || boletosDelTipo.Count <= 0)
                {
                    continue;
                }
                else
                {
                    List<string> codigos = new List<string>();
                    foreach (var tipoBoleto in boletosDelTipo)
                    {
                        if (tipoBoleto.IsMultiVoucher)
                        {
                            for (int i = 0; i < tipoBoleto.Cant_Redimir; i++)
                            {
                                codigos.Add(tipoBoleto.VoucherCode);
                            }
                        }
                        else
                        {
                            codigos.Add(tipoBoleto.VoucherCode);
                        }
                    }

                    var ticket = s.Purchase.Tickets.Where(t => t.Code == tipo).FirstOrDefault();
                    int indice = s.Purchase.Tickets.ToList().IndexOf(ticket);
                    s.Purchase.Tickets[indice].BarcodeRedemption = string.Join(";", codigos.ToArray());
                }
            }

            return s;
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult GetHall(SessionInfo model)
        {
            model = LoadVouchersToOrder(model);
            //RM - validar que el total de boletos comprados sume 8 asientos
            #region PromocionDeBoletos
            int parejas = 0;
            int familia3 = 0;
            int familia4 = 0;
            int familia5 = 0;
            int boletoUnico = model.Purchase.Tickets.Where(x => !x.Name.Contains("Pareja") && !x.Name.Contains("a 3") && !x.Name.Contains("a 4") && !x.Name.Contains("a 5")).Sum(d => d.Quantity);
            if (model.Purchase.Tickets.Where(t => t.Name.Contains("Pareja")).Count() > 0)
            {
                //Saco el total de boletos pareja y lo multiplico por 2 para saebr cuantos asientos son
                parejas = model.Purchase.Tickets.Where(t => t.Name.Contains("Pareja")).Select(t1 => t1.Quantity).First() * 2;
            }
            if (model.Purchase.Tickets.Where(t => t.Name.Contains("a 3")).Count() > 0)
            {
                //Saco el total de boletos familia 3 y lo multiplico por 3 para saebr cuantos asientos son
                familia3 = model.Purchase.Tickets.Where(t => t.Name.Contains("a 3")).Select(t1 => t1.Quantity).First() * 3;
            }
            if (model.Purchase.Tickets.Where(t => t.Name.Contains("a 4")).Count() > 0)
            {
                //Saco el total de boletos familia 4 y lo multiplico por 4 para saebr cuantos asientos son
                familia4 = model.Purchase.Tickets.Where(t => t.Name.Contains("a 4")).Select(t1 => t1.Quantity).First() * 4;
            }
            if (model.Purchase.Tickets.Where(t => t.Name.Contains("a 5")).Count() > 0)
            {
                //Saco el total de boletos familia 5 y lo multiplico por 5 para saebr cuantos asientos son
                familia5 = model.Purchase.Tickets.Where(t => t.Name.Contains("a 5")).Select(t1 => t1.Quantity).First() * 5;
            }

            if ((boletoUnico + parejas + familia3 + familia4 + familia5) > model.MaxSeatsPerTransaction)
            {
                Error modelError = new Error
                {
                    Code = 304,
                    Message = "Lo sentimos, la cantidad máxima permitida de boletos es de 8."
                };
                return View("_Error", modelError);
            }
            #endregion
            //seteo de los valores que seran usados para armar el viewbag de la vista de error a la seleccion de boletos;
            //de no hacerlo, la pagina retorna error 500 al no conseguir dichos valores.
            Session["Theater"] = model.Theater.Id;
            Session["Session"] = model.Id;
            UserSessionRQ request = new UserSessionRQ();
            request.SessionId = model.Id;
            request.TheaterId = model.Theater.Id;
            request.Tickets = model.Purchase.Tickets;
            request.MovieId = model.Movie.Id;
            Service services = new Service();
            UserSessionRS responce = services.Execute<UserSessionRQ, UserSessionRS>("UserSession.Create", request);

            if (responce == null)
            {
                Error modelError = new Error();
                modelError.Code = 207;
                modelError.Message = "Lo sentimos, no se generó su sesión de compra. Por favor seleccione los boletos nuevamente";
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                return View("_Error", modelError);
            }

            if (responce.Id.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                Error modelError = new Error();
                modelError.Code = 500;
                //modelError.Message = "Lo sentimos, el tiempo de la sesión de compra ha expirado. Por favor seleccione nuevamente los boletos.";
                modelError.Message = "Lo sentimos, actualmente hemos perdido la conexión con el cine. Por favor intente nuevamente.";
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                //ViewBag.TheaterId = model.Theater.Id;
                //ViewBag.SessionId = model.Id;
                return View("_Error", modelError);
            }

            // Cambiar tiempo de compra
            //int minutesToSubtract = 3;
            //responce.TimeOut = responce.TimeOut.Subtract(new TimeSpan(0, minutesToSubtract, 0));

            if ((responce.WithError) && !string.IsNullOrEmpty(responce.ErrorMessage))
            {
                //Problema que viene de la capa de vista, por lo general puede ser:
                // Función cerrada
                // Los servicios nativos de vista CCVISTA4 se encuentren apagados, o los del cine
                Error modelError = new Error();
                modelError.Code = 1100;
                modelError.Message = responce.ErrorMessage;
                ViewBag.TheaterId = model.Theater.Id;
                ViewBag.SessionId = model.Movie.Id;
                return View("_Error", modelError);
            }
            UserSession userSession = (UserSessionRS)responce;
            return View(userSession);
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult ViewHall(Guid userSessionId)
        {
            Service services = new Service();
            UserSessionRS responce = services.Execute<UserSessionRS>("UserSession.Read", new { userSessionId = userSessionId, readMap = true, token = TokenServices });

            if ((responce == null) || (responce.Id.ToString() == "00000000-0000-0000-0000-000000000000"))
            {
                Error modelError = new Error();
                modelError.Code = 209;
                modelError.Message = "Lo sentimos, el tiempo de la sesión de compra ha expirado.";
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                return View("_Error", modelError);
            }

            if (responce.WithError)
            {
                Error modelError = new Error();
                modelError.Code = 1100;
                modelError.Message = responce.ErrorMessage;//aqui es donde pone el mensaje FailedSeatDataRetrieve
                if (responce.ErrorMessage.Equals("FailedSeatDataRetrieve"))
                {
                    modelError.Message = "Estimado invitado, la carga de selección de butacas falló debido a que su sesión de usuario expiró. Le invitamos a que inicie nuevamente el proceso de compra.";
                }

                if (responce.ErrorMessage.Equals("UnexpectedError"))
                {
                    modelError.Code = 1101;
                    modelError.Message = "Estimado invitado, la carga de selección de butacas falló debido a que su sesión expiró. Le invitamos a que inicie nuevamente el proceso de compra.";
                }
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                return View("_Error", modelError);
            }
            UserSession userSession = (UserSessionRS)responce;
            return View("GetHall", userSession);
        }

        [HttpGet()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public JsonResult GetConcessionsOrder(Guid userSessionId)
        {
            var userSession = ReadUserSession(userSessionId, false);
            var orderResponse = new
            {
                Concessions = userSession.Concessions != null ? userSession.Concessions.ToArray() : null,
                WebBookingFee = userSession.BookingFee
            };
            return Json(orderResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult EditConcessions(Guid userSessionId, Seat[] seats, ConcessionItem[] carrito)
        {

            ConfirmationRQ request = new ConfirmationRQ();
            request.UserSessionId = userSessionId;
            request.Seats = seats;

            Service services = new Service();
            UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read", new { userSessionId = userSessionId, readMap = true, token = TokenServices });

            if (sessionResponse == null)
            {
                Error modelError = new Error();
                modelError.Code = 503;
                modelError.Message = "Lo sentimos, estamos presentando problemas con la conexión con el cine. Por favor intente nuevamente.";
                return View("_Error", modelError);

            }


            if (sessionResponse.WithError)
            {
                //Service ser = new Service();
                //UserSessionRS sessionRespons = services.Execute<UserSessionRS>("UserSession.DeleteSession", new { SessionId = userSessionId, token = ConfigurationManager.AppSettings["TokenCMS"] });
                Error modelError = new Error();
                modelError.Code = 209;
                //modelError.Message = sessionResponse.ErrorMessage;
                modelError.Message = "Lo sentimos, el tiempo de la sesión de compra ha expirado. Por favor seleccione nuevamente los boletos.";
                //ViewBag.TheaterId = sessionRespons.TheaterId;
                //ViewBag.SessionId = sessionRespons.SessionId;
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                return View("_Error", modelError);
            }
            else
            {
                var transactionId = this.Session["TransIdTemp"].ToString();
                var rollbackRequest = new ConcessionsRQ
                {
                    TheaterId = sessionResponse.TheaterId,
                    TransIdTemp = transactionId,
                    UserSessionId = sessionResponse.Id.ToString()
                };

                var cancelResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.CancelConcessionsOrder", rollbackRequest);

                UserSession userSession = (UserSessionRS)sessionResponse;
                userSession.Time.TimeOut = DateTime.Now.AddMinutes(5);
                userSession.Time.CalculeTime();

                var concessionListResponse = services.Execute<TheaterConcessionsRS>("Concessions.GetCandiesWeb", new
                { theaterId = sessionResponse.TheaterId, token = TokenServices });

                //ConcessionsRS concessionsResponse = services.Execute<ConcessionsRS>("Concessions.GetPorcentageWeb");
                //userSessionConceSession.PorcCargoWEb = Int32.Parse(concessionsResponse.PorcCargoWEb);

                userSession.Concessions = concessionListResponse.Items.ToArray();
                userSession.Purchase.Concessions = carrito;
                if (userSession.BookingFee != null)
                {
                    userSession.CargoWeb = userSession.BookingFee.ItemPriceDCurPrice;
                    userSession.Purchase.WebChargeFee = userSession.BookingFee.ItemPriceDCurPrice;
                }

                return View("GetConcessions", userSession);
            }
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif     
        public ActionResult Confirmation(Guid userSession, Seat[] seats, string command)
        {
            ConfirmationRQ request = new ConfirmationRQ();
            request.UserSessionId = userSession;
            request.Seats = seats;
            Service services = new Service();
            ConfirmationRS confirmationResponse = services.Execute<ConfirmationRQ, ConfirmationRS>("UserSession.Confirmation", request);

            //si no recibo respuesta del servicio mando a la vista de error con los datos de la session(funcion) y cine
            //de esta manera, en la vista de error armo un link que permitira redireccionar al usuario a la seleccion de boletos de la misma pelicula en el mismo cine.
            //confirmationResponse = null;
            if (confirmationResponse == null)
            {
                Service ser = new Service();
                UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.DeleteSession", new { SessionId = userSession, token = ConfigurationManager.AppSettings["TokenCMS"] });
                Error modelError = new Error();
                modelError.Code = 209;
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                modelError.Message = "Lo sentimos, el tiempo de compra ha expirado. Por favor seleccione nuevamente los boletos.";
                return View("_Error", modelError);
            }

            if (command == "Paymment")
            {

                if (confirmationResponse.IsValid)
                {

                    Payment model = (ConfirmationRS)confirmationResponse;

                    //Ramiro Marimón
                    //validar si el usuario ha seleccionado carameleria en un paso previo.
                    //de ser positivo, se asigna el valor del cargo web y de iva para poder realizar los calculos de los impuestos.
                    if (model.Concessions != null && model.Concessions.Length > 0)
                    {
                        //var theaterConcessions = services.Execute<TheaterConcessionsRS>("Concessions.TheaterConcessions",
                        //new { theaterId = confirmationResponse.TheaterId, token = TokenServices });

                        var theaterConcessions = services.Execute<TheaterConcessionsRS>("Concessions.GetCandiesWeb", new
                        { theaterId = confirmationResponse.TheaterId, token = TokenServices });

                        model.Purchase.WebChargeFee = theaterConcessions.BookingFee.ItemPriceDCurPrice; //este bookin viene de capa 2
                        model.Purchase.PercentageTax = decimal.Parse(ConfigurationManager.AppSettings["IVA"]); //y esto lo puse ne la capa uno.
                    }
                    // CI
                    HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                    FormsAuthenticationTicket ticket = null;
                    ticket = FormsAuthentication.Decrypt(cookie.Value);

                    TokenInfo userdata = (TokenInfo)new Serializer().DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));

                    ViewBag.IdentificationType = userdata.IdentificationType;
                    ViewBag.Identification = userdata.Identification;

                    // Vista
                    return View("Confirmation", model);
                }
                else
                {

                    UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read", new { userSessionId = userSession, token = TokenServices });

                    // Si los asientos estan tomados (si son diferentes a los recibidos, quiere decir que no pudo cambiarlos)
                    int i = 0;
                    foreach (Seat s in confirmationResponse.Seats)
                    {
                        if (s.Name != seats[i].Name)
                        {
                            sessionResponse.ErrorMessage = "seatsunavailable";
                            sessionResponse.WithError = true;
                        }
                        i++;
                    }

                    // Error
                    if (sessionResponse.WithError)
                    {
                        Error modelError = new Error();
                        //modelError.Code = 503;

                        // Si es que no hay asientos
                        if (sessionResponse.ErrorMessage.ToLower().Contains("seatsunavailable"))
                        {
                            modelError.Code = 1103;
                            modelError.Message = "Disculpe estos asientos no se encuentran disponibles, por favor seleccione otros.";
                        }
                        else
                        {
                            modelError.Code = 503;
                            modelError.Message = "Lo sentimos, estamos presentando problemas con la conexión con el cine. Por favor intente nuevamente.";
                            // modelError.Message = userSessionResponse.ErrorMessage;
                        }
                        ViewBag.TheaterId = Session["Theater"].ToString();
                        ViewBag.SessionId = Session["Session"].ToString();
                        //ViewBag.TheaterId = sessionResponse.TheaterId;
                        //ViewBag.SessionId = sessionResponse.SessionId;
                        return View("_Error", modelError);
                    }
                    UserSession session = (UserSessionRS)sessionResponse;
                    return View("GetHall", session);
                }

            }
            else
            {
                UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read", new { userSessionId = userSession, readMap = true, token = TokenServices });
                if (sessionResponse.WithError)
                {
                    Error modelError = new Error();
                    modelError.Code = 503;
                    modelError.Message = sessionResponse.ErrorMessage;
                    ViewBag.TheaterId = Session["Theater"].ToString();
                    ViewBag.SessionId = Session["Session"].ToString();
                    return View("_Error", modelError);
                }

                UserSession userSessionConcessions = (UserSessionRS)sessionResponse;
                userSessionConcessions.Time.TimeOut = DateTime.Now.AddMinutes(4);
                userSessionConcessions.Time.CalculeTime();

                //var concessionListResponse = services.Execute<TheaterConcessionsRS>("Concessions.TheaterConcessions", new { theaterId = sessionResponse.TheaterId, token = TokenServices });
                var concessionListResponse = services.Execute<TheaterConcessionsRS>("Concessions.GetCandiesWeb", new { theaterId = sessionResponse.TheaterId, token = TokenServices });

                //List<ConcessionItem> suzy = new List<ConcessionItem>();
                //List<ConcessionItem> concessions = new List<ConcessionItem>();

                //foreach (var item in concessionListResponse.Items)
                //{
                //    if (item.ItemStrItemID == "10001586" || item.ItemStrItemID == "10001587")
                //    {
                //        suzy.Add(item);
                //    }
                //    else
                //    {
                //        concessions.Add(item);
                //    }
                //}

                //foreach (var item2 in concessions)
                //{
                //    suzy.Add(item2);
                //}

                if (concessionListResponse == null)
                {
                    Error modelError = new Error();
                    modelError.Code = 503;
                    modelError.Message = "Lo sentimos, actualmente hemos perdido la conexión con el cine. Por favor intente nuevamente.";
                    ViewBag.TheaterId = Session["Theater"].ToString();
                    ViewBag.SessionId = Session["Session"].ToString();
                    return View("_Error", modelError);
                }
                else
                {
                    userSessionConcessions.Concessions = concessionListResponse.Items.ToArray();
                    //userSessionConcessions.Concessions = suzy.ToArray();
                    userSessionConcessions.CargoWeb = concessionListResponse.BookingFee.ItemPriceDCurPrice;
                    userSessionConcessions.Purchase.WebChargeFee = concessionListResponse.BookingFee.ItemPriceDCurPrice;
                    //ConcessionsRS concessionsResponse = services.Execute<ConcessionsRS>("Concessions.GetPorcentageWeb");
                    //userSessionConceSession.PorcCargoWEb = Int32.Parse(concessionsResponse.PorcCargoWEb);


                }
                return View("GetConcessions", userSessionConcessions);
            }
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult GetConcessions(Guid userSession, ConcessionItem[] Concession)
        {
            Service services = new Service();

            this.Session["guidSession"] = userSession.ToString();

            UserSessionRS readUser = services.Execute<UserSessionRS>("UserSession.Read",
                 new { userSessionId = userSession, readMap = true, token = TokenServices });

            string transactionId = null;
            if (this.Session["TransIdTemp"] != null)
            {
                transactionId = this.Session["TransIdTemp"].ToString();
            }

            var concessionsRequest = new ConcessionsRQ
            {
                SessionId = readUser.SessionId,
                TheaterId = readUser.TheaterId,
                UserSessionId = userSession.ToString(),
                Concessions = Concession,
                BookingNumber = readUser.BookingNumber,
                TransIdTemp = transactionId
            };

            //ConcessionsRS concessionsOrder = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.AddItemsToCart", concessionsRequest);

            //Metodo de prueba para Carameleria WEB
            ConcessionsRS concessionsOrder = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.AddItemsCandiesWeb", concessionsRequest);
            //Metodo de prueba para Carameleria WEB
            //var theaterConcessions = services.Execute<TheaterConcessionsRS>("Concessions.GetCandiesWeb", new{ theaterId = confirmationResponse.TheaterId, token = TokenServices });


            //evalar concessionsOrder
            ConfirmationRQ request = new ConfirmationRQ();
            request.UserSessionId = userSession;
            request.Seats = readUser.Seats;
            request.Concessions = concessionsOrder.Order != null ? concessionsOrder.Order.Items.ToArray() : null;
            request.BookingFee = concessionsOrder.Order != null ? concessionsOrder.Order.BookingFee : null;

            ConfirmationRS confirmationResponse = services.Execute<ConfirmationRQ, ConfirmationRS>("UserSession.Confirmation", request);

            //evaluar confirmationResponse

            Payment model = (ConfirmationRS)confirmationResponse;
            //model.Concessions = Concession;
            model.Purchase.Concessions = model.Concessions;

            if (!concessionsOrder.WithError)
            {
                //UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.ConcessionOrder", new { userSessionId = userSession, transactionId = concessionsResponse.TransIdTemp });
                // set transaction id (temp)
                this.Session["TransIdTemp"] = concessionsOrder.TransIdTemp;
                model.Purchase.WebChargeFee = concessionsOrder.BookingFee.ItemPriceDCurPrice;
            }


            if ((confirmationResponse.IsValid && !concessionsOrder.WithError) || (confirmationResponse.IsValid && Concession == null))
            {

                // CI
                HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = null;
                ticket = FormsAuthentication.Decrypt(cookie.Value);

                TokenInfo userdata =
                    (TokenInfo)new Serializer().DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));

                ViewBag.IdentificationType = userdata.IdentificationType;
                ViewBag.Identification = userdata.Identification;
            }
            else
            {

                UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read",
                    new { userSessionId = userSession, readMap = true, token = TokenServices });

                //

                var theaterConcessions = services.Execute<TheaterConcessionsRS>("Concessions.TheaterConcessions",
                    new { theaterId = sessionResponse.TheaterId, token = TokenServices });


                //error al traer las concesiones
                if (sessionResponse.WithError)
                {
                    Error modelError = new Error();
                    modelError.Code = 503;
                    modelError.Message = sessionResponse.ErrorMessage;
                    ViewBag.TheaterId = Session["Theater"].ToString();
                    ViewBag.SessionId = Session["Session"].ToString();
                    return View("_Error", modelError);
                }
                else if (concessionsOrder.WithError)
                {
                    Error modelError = new Error();
                    //cuando no hay disponibilidad en el producto
                    if (concessionsOrder.ErrorMessage.Equals("Lo sentimos, no hay disponibilidad para la cantidad de producto(s) seleccionado(s)."))
                    {
                        modelError.Code = 300;
                        modelError.Message = concessionsOrder.ErrorMessage;
                    }
                    //cuando no se puede generar la reserva del producto en vista
                    if (concessionsOrder.ErrorMessage.Equals("Lo sentimos, se presentó un problema al reservar su(s) producto(s), por favor reinicie la compra"))
                    {
                        modelError.Code = 301;
                        modelError.Message = concessionsOrder.ErrorMessage;
                    }
                    //Cuando falla al crear la cabecera en las tablas tblCUOrderTrackConcessions ó tblCUOrdenInvConcessions
                    if (concessionsOrder.ErrorMessage.Equals("Lo sentimos, hubo un error al procesar su pedido, por favor intente nuevamente"))
                    {
                        modelError.Code = 302;
                        modelError.Message = concessionsOrder.ErrorMessage;
                    }

                    if (concessionsOrder.ErrorMessage.Equals("Lo sentimos, actualmente hemos perdido la conexión con el cine.Por favor intente nuevamente."))
                    {
                        modelError.Code = 503;
                        modelError.Message = "Lo sentimos, actualmente hemos perdido la conexión con el cine.Por favor intente nuevamente.";
                    }


                    ViewBag.TheaterId = Session["Theater"].ToString();
                    ViewBag.SessionId = Session["Session"].ToString();
                    return View("_Error", modelError);
                }

                UserSession userSessionConcessions = (UserSessionRS)sessionResponse;
                userSessionConcessions.Time.TimeOut = DateTime.Now.AddMinutes(5);
                userSessionConcessions.Time.CalculeTime();
                userSessionConcessions.Concessions = theaterConcessions.Items.ToArray();
                userSessionConcessions.CargoWeb = theaterConcessions.BookingFee.ItemPriceDCurPrice;
                userSessionConcessions.Purchase.WebChargeFee = theaterConcessions.BookingFee.ItemPriceDCurPrice;
            }

            return View("Confirmation", model);
        }

        #region Payment Anterior a Vista de PAGO
        //        [HttpPost()]
        //        [Authorize]
        //#if !DEBUG
        //        [RequireHttpsWithPortAttribute()]
        //#endif
        //        //[AutorizarCompra]
        //        public JsonResult Payments(CreditCard model)
        //        {
        //            PaymentRS responce = new PaymentRS();
        //            //PaymentRS responce;            
        //            if (Session[model.Id.ToString()] != null)
        //            {
        //                if (DateTime.Now < (DateTime)Session[model.Id.ToString()])
        //                {
        //                    HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
        //                    FormsAuthenticationTicket ticket = null;
        //                    ticket = FormsAuthentication.Decrypt(cookie.Value);
        //                    Serializer serial = new Serializer();
        //                    //TokenInfo userdata = (TokenInfo)XmlAdapter.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
        //                    TokenInfo userdata = (TokenInfo)serial.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
        //                    //             

        //                    if (string.Concat(model.CertificateType + model.Certificate) == string.Concat(userdata.IdentificationType + userdata.Identification))
        //                    {
        //                        Service services = new Service();

        //                        //Capturar valor de TotalValue
        //                        UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read",
        //                        new { userSessionId = model.Id, readMap = false, token = TokenServices });
        //                        UserSession userSession = (UserSession)sessionResponse;

        //                        model.Email = HttpContext.User.Identity.Name;
        //                        PaymentRQ request = (PaymentRQ)model;
        //                        request.TotalValue = userSession.Purchase.TotalPurchase;
        //                        //PaymentRS responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);
        //                        //responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);

        //                        //Si la compra tiene carameleria pasa por el nuevo metodo PaymentCandies
        //                        if (userSession.Concessions.Count() > 0)
        //                        {
        //                            responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.PaymentCandies", request);
        //                        }
        //                        else //Si la compra no tiene carameleria pasa por el proceso normal de compra
        //                        {
        //                            responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);
        //                        }

        //                        if (responce != null && sessionResponse.Concessions.Count() > 0)
        //                        {
        //                            // if there are concession items, place concession items and update transaction number
        //                            var concessionsRequest = new ConcessionsRQ
        //                            {
        //                                SessionId = sessionResponse.SessionId,
        //                                TheaterId = sessionResponse.TheaterId,
        //                                UserSessionId = model.Id.ToString(),
        //                                Concessions = sessionResponse.Concessions,
        //                                BookingNumber = responce.BookingNumber,
        //                                TransactionNumber = responce.TransactionNumber,
        //                                TransIdTemp = this.Session["TransIdTemp"].ToString()
        //                            };

        //                            ConcessionsRS concessionsResponse = new ConcessionsRS();
        //                            if (responce.CodeResult == "00")
        //                            {
        //                                concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);
        //                            }
        //                            else
        //                            {
        //                                concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.CancelConcessionsOrder", concessionsRequest);
        //                            }

        //                            //ConcessionsRS concessionsResponse = new ConcessionsRS();
        //                            //ConcessionsRS concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);

        //                            //if (responce.CodeResult == "00")
        //                            //{
        //                            //    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);
        //                            //}
        //                            //else
        //                            //{
        //                            //    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.CancelConcessionsOrder", concessionsRequest);
        //                            //}

        //                        }

        //                        Session.Abandon();
        //                    }
        //                    else
        //                    {
        //                        //responce = new PaymentRS();
        //                        responce.CodeResult = "Cedula no coincide";
        //                        responce.DescriptionResult = "La cédula del usuario registrado no coincide con la cédula del tarjetahabiente";
        //                        //Session.Abandon(); no elimino la sesion acá para poder usarla en el metodo Result para setear el viewBag.
        //                        //luego de haber armado el viewBag si hago session.abandon()
        //                    }
        //                }
        //                else
        //                {
        //                    responce.CodeResult = "Error Tiempo";
        //                    //Session.Abandon(); no elimino la sesion acá para poder usarla en el metodo Result para setear el viewBag.
        //                    //luego de haber armado el viewBag si hago session.abandon()
        //                }
        //            }
        //            else
        //            {
        //                responce.CodeResult = "Error Session Nula";
        //                Session.Abandon();
        //            }
        //            return Json(responce, JsonRequestBehavior.AllowGet);
        //        } 
        #endregion

        /// <summary>
        /// Muestra el mensaje al usuario una vez realizado el pago sea exitoso o no
        /// si el usuario realiza operaciones que no debe llamese, retroceder y volver a pagar
        /// la accion payment elimina la sesion y a través de esta accion se elimina el valor
        /// del redis para que result muestre el mensaje adecuado, en este caso tiempo de sesion de compra expiro.
        /// </summary>
        /// <param name="userSessionId"></param>
        /// <returns></returns>
        [HttpGet()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult Result(Guid userSessionId)
        {
            //Promocion - Productivo
            //string Promo = System.Configuration.ConfigurationManager.AppSettings["Promo_Moana"];
            //string Promo_3D = System.Configuration.ConfigurationManager.AppSettings["Promo_Moana3D"];

            //Promocion - Pruebas
            //string Promo = System.Configuration.ConfigurationManager.AppSettings["Promo_Moana"];

            Service services = new Service();
            // Leer
            UserSessionRS responce = services.Execute<UserSessionRS>("UserSession.Read", new { userSessionId = userSessionId, readMap = false, token = TokenServices });

            // Si la transacción es fallida (no retorna el CodeResult en 00) borro la sesión del redis
            if (responce.CodeResult != "00")
            {
                UserSessionRS session = services.Execute<UserSessionRS>("UserSession.DeleteSession", new { SessionId = userSessionId, token = ConfigurationManager.AppSettings["TokenCMS"] });
            }

            if (responce == null)
            {
                Error modelError = new Error();
                modelError.Code = 503;
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                Session.Abandon();
                //ViewBag.Home = "Ok";  //ya que no se puede armar un viewbag con el cine y la funcion, redirijo al invitado a la pagina principal
                modelError.Message = "Lo sentimos, estamos presentando problemas con la conexión con el cine. Por favor intente nuevamente.";
                return View("_Error", modelError);
            }


            UserSession userSession = (UserSession)responce;
            //Chequea si la sesión del redis aún sigue vigente o expiró
            if (userSession.BookingNumber == 0 && userSession.Id.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                //throw new HttpException(209, "Lo sentimos, la sesión de compra expiró. Le invitamos a ver sus boletos adquiridos en el menu Mis Compras.");
                // throw new HttpException(209, "Lo sentimos, el tiempo de la sesión de compra ha expirado. Le Invitamos a verificar los boletos adquiridos ingresando en la sección “Mi Cuenta” a través de la opción “Mis Compras” del menú superior.");
                Error modelError = new Error();
                modelError.Code = 208;
                ViewBag.TheaterId = Session["Theater"].ToString();
                ViewBag.SessionId = Session["Session"].ToString();
                Session.Abandon(); //
                //ViewBag.Home = "Ok";  //ya que no se puede armar un viewbag con el cine y la funcion, redirijo al invitado a la pagina principal
                modelError.Message = "Lo sentimos, el tiempo de la sesión de compra ha expirado. Le Invitamos a verificar los boletos adquiridos ingresando en la sección “Mi Cuenta” a través de la opción “Mis Compras” del menú superior.";
                return View("_Error", modelError);
            }
            else
            {
                //string TheaterId = System.Configuration.ConfigurationManager.AppSettings["CinemaQR"].ToString();

                //Evaluar si el cine está habilitado con P1
                //if (TheaterId.Contains(userSession.TheaterId))
                //{
                    //Evaluar si el Nombre del archivo se encuentra nulo o vacio
                    if (!String.IsNullOrEmpty(userSession.BookingByte))
                    {
                        bool value = false;
                        string[] mobileDevices = new string[] {"iphone","ppc",
                                                   "windows ce","blackberry",
                                                   "opera mini","mobile","palm",
                                                   "portable","opera mobi","android","mob","ios","nexus"};

                        var variable = Request.ServerVariables["HTTP_USER_AGENT"].ToLower();

                        foreach (string s in mobileDevices)
                        {
                            if (variable.Contains(s))
                            {
                                value = true;
                                break;
                            }
                        }

                        if (value == true)
                        {
                            ViewBag.IsMobile = true;
                            ViewBag.NotMobile = false;
                        }
                        else
                        {
                            ViewBag.IsMobile = false;
                            ViewBag.NotMobile = true;
                        }

                        //LUIS RAMIREZ, 12/07/2017 - Generar imagen a base64
                        string QR = String.Concat("https://cinesunidosweb.blob.core.windows.net/qrcodes/", userSession.BookingByte);

                        var webClient = new WebClient();
                        byte[] imageBytes = webClient.DownloadData(QR);
                        //MemoryStream ms = new MemoryStream(imageBytes);

                        var base64string = Convert.ToBase64String(imageBytes);
                        ViewBag.FilePath = base64string;
                    }
                //}
                HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = null;
                ticket = FormsAuthentication.Decrypt(cookie.Value);

            }
            // Vista
            return View(userSession);
        }

        [HttpGet()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public ActionResult ViewTerms()
        {
            return View("Terms");
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()]
#endif
        public JsonResult GetVippoConfirmation(APaymentRequest Account)
        {
            try
            {
                string IDCard = string.Empty;
                HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = null;
                TokenInfo userdata = null;

                if (cookie != null)
                {
                    ticket = FormsAuthentication.Decrypt(cookie.Value);
                    Serializer serial = new Serializer();
                    userdata = (TokenInfo)serial.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
                    IDCard = userdata.IdentificationType + userdata.Identification;
                }

                Account.Client_UserVippo = Account.Client_UserVippo;// + "|" + IDCard;
                Account.Client_PasswordVippo = EncryptVippo(Account.Client_PasswordVippo);
                Service Client = new Service();
                AVippoResponse response = Client.Execute<APaymentRequest, AVippoResponse>("UserSession.GetVippoConfirmation", Account);
                return Json(response);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()] 
#endif
        public JsonResult CreditCardPayment(CreditCard creditCard)
        {
            //UserEntity UserInfo = System.Web.HttpContext.Current.Session["UserInfo"] as UserEntity;
            PaymentInfomation infomation = (PaymentInfomation)creditCard;
            PaymentRS response = Payments(infomation);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MercantilCardPayment(CreditCard creditCard)
        {
            //UserEntity UserInfo = System.Web.HttpContext.Current.Session["UserInfo"] as UserEntity;
            PaymentInfomation infomation = (PaymentInfomation)creditCard;
            PaymentRS response = Payments(infomation);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MercantilAuthPayment(CreditCard creditCard)
        {
            //UserEntity UserInfo = System.Web.HttpContext.Current.Session["UserInfo"] as UserEntity;
            PaymentInfomation infomation = (PaymentInfomation)creditCard;
            PaymentRS response = MercantilAuthPayments(infomation);
            var twofactor_auth = response.DescriptionResult;
            ViewBag.twofactor_auth = twofactor_auth;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost()]
        [Authorize]
#if DEBUG
#else
        [RequireHttpsWithPortAttribute()] 
#endif
        public JsonResult VippoPayment(Vippo vippo)
        {
            //UserEntity UserInfo = System.Web.HttpContext.Current.Session["UserInfo"] as UserEntity;
            PaymentInfomation infomation = (PaymentInfomation)vippo;
            PaymentRS Response = Payments(infomation);
            return Json(Response, JsonRequestBehavior.AllowGet);
        }

        private PaymentRS Payments(PaymentInfomation model)
        {
            PaymentRS responce = new PaymentRS();
            //PaymentRS responce;            
            if (Session[model.IdClient.ToString()] != null)
            {
                if (DateTime.Now < (DateTime)Session[model.IdClient.ToString()])
                {
                    HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                    FormsAuthenticationTicket ticket = null;
                    ticket = FormsAuthentication.Decrypt(cookie.Value);
                    Serializer serial = new Serializer();
                    //TokenInfo userdata = (TokenInfo)XmlAdapter.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
                    TokenInfo userdata = (TokenInfo)serial.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
                    //             

                    if (string.Concat(model.IdCardType + model.IdCardNumber) == string.Concat(userdata.IdentificationType + userdata.Identification))
                    {
                        Service services = new Service();

                        //Capturar valor de TotalValue
                        UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read",
                        new { userSessionId = model.IdClient, readMap = false, token = TokenServices });
                        UserSession userSession = (UserSession)sessionResponse;

                        model.Email = HttpContext.User.Identity.Name;
                        PaymentRQ request = (PaymentRQ)model;
                        request.TotalValue = userSession.Purchase.TotalPurchase;
                        //PaymentRS responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);
                        //responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);

                        //Si la compra tiene carameleria pasa por el nuevo metodo PaymentCandies
                        if (userSession.Concessions.Count() > 0)
                        {
                            responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.PaymentCandies", request);
                        }
                        else //Si la compra no tiene carameleria pasa por el proceso normal de compra
                        {
                            responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);
                        }

                        if (responce != null && sessionResponse.Concessions.Count() > 0)
                        {
                            // if there are concession items, place concession items and update transaction number
                            var concessionsRequest = new ConcessionsRQ
                            {
                                SessionId = sessionResponse.SessionId,
                                TheaterId = sessionResponse.TheaterId,
                                UserSessionId = model.IdClient.ToString(),
                                Concessions = sessionResponse.Concessions,
                                BookingNumber = responce.BookingNumber,
                                TransactionNumber = responce.TransactionNumber,
                                TransIdTemp = this.Session["TransIdTemp"].ToString()
                            };

                            ConcessionsRS concessionsResponse = new ConcessionsRS();
                            if (responce.CodeResult == "00")
                            {
                                concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);
                            }
                            else
                            {
                                concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.CancelConcessionsOrder", concessionsRequest);
                            }

                            //ConcessionsRS concessionsResponse = new ConcessionsRS();
                            //ConcessionsRS concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);

                            //if (responce.CodeResult == "00")
                            //{
                            //    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);
                            //}
                            //else
                            //{
                            //    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.CancelConcessionsOrder", concessionsRequest);
                            //}

                        }

                        Session.Abandon();

                    }
                    else
                    {
                        //responce = new PaymentRS();
                        responce.CodeResult = "Cedula no coincide";
                        responce.DescriptionResult = "La cédula del usuario registrado no coincide con la cédula del tarjetahabiente";
                        //Session.Abandon(); no elimino la sesion acá para poder usarla en el metodo Result para setear el viewBag.
                        //luego de haber armado el viewBag si hago session.abandon()
                    }
                }
                else
                {
                    responce.CodeResult = "Error Tiempo";
                    //Session.Abandon(); no elimino la sesion acá para poder usarla en el metodo Result para setear el viewBag.
                    //luego de haber armado el viewBag si hago session.abandon()
                }
            }
            else
            {
                responce.CodeResult = "Error Session Nula";
                Session.Abandon();
            }
            return responce;
        }

        private PaymentRS MercantilAuthPayments(PaymentInfomation model)
        {
            PaymentRS responce = new PaymentRS();
            Service services = new Service();
            PaymentRQ request = (PaymentRQ)model;
            Session["Model"] = request;
            responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.PaymentAuthMercantil", request);
            //PaymentRS responce;            
            //if (Session[model.IdClient.ToString()] != null)
            //{
            //    if (DateTime.Now < (DateTime)Session[model.IdClient.ToString()])
            //    {
            //        HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //        FormsAuthenticationTicket ticket = null;
            //        ticket = FormsAuthentication.Decrypt(cookie.Value);
            //        Serializer serial = new Serializer();
            //        //TokenInfo userdata = (TokenInfo)XmlAdapter.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
            //        TokenInfo userdata = (TokenInfo)serial.DeserializeFromMemory(ticket.UserData, typeof(TokenInfo));
            //        //             

            //        if (string.Concat(model.IdCardType + model.IdCardNumber) == string.Concat(userdata.IdentificationType + userdata.Identification))
            //        {
            //            Service services = new Service();

            //            //Capturar valor de TotalValue
            //            UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read",
            //            new { userSessionId = model.IdClient, readMap = false, token = TokenServices });
            //            UserSession userSession = (UserSession)sessionResponse;

            //            model.Email = HttpContext.User.Identity.Name;
            //            PaymentRQ request = (PaymentRQ)model;
            //            request.TotalValue = userSession.Purchase.TotalPurchase;
            //            //PaymentRS responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);
            //            //responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);

            //            //Si la compra tiene carameleria pasa por el nuevo metodo PaymentCandies
            //            if (userSession.Concessions.Count() > 0)
            //            {
            //                responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.PaymentCandies", request);
            //            }
            //            else //Si la compra no tiene carameleria pasa por el proceso normal de compra
            //            {
            //                responce = services.Execute<PaymentRQ, PaymentRS>("UserSession.Payment", request);
            //            }

            //            if (responce != null && sessionResponse.Concessions.Count() > 0)
            //            {
            //                // if there are concession items, place concession items and update transaction number
            //                var concessionsRequest = new ConcessionsRQ
            //                {
            //                    SessionId = sessionResponse.SessionId,
            //                    TheaterId = sessionResponse.TheaterId,
            //                    UserSessionId = model.IdClient.ToString(),
            //                    Concessions = sessionResponse.Concessions,
            //                    BookingNumber = responce.BookingNumber,
            //                    TransactionNumber = responce.TransactionNumber,
            //                    TransIdTemp = this.Session["TransIdTemp"].ToString()
            //                };

            //                ConcessionsRS concessionsResponse = new ConcessionsRS();
            //                if (responce.CodeResult == "00")
            //                {
            //                    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);
            //                }
            //                else
            //                {
            //                    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.CancelConcessionsOrder", concessionsRequest);
            //                }

            //                //ConcessionsRS concessionsResponse = new ConcessionsRS();
            //                //ConcessionsRS concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);

            //                //if (responce.CodeResult == "00")
            //                //{
            //                //    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.PurchaseConcessions", concessionsRequest);
            //                //}
            //                //else
            //                //{
            //                //    concessionsResponse = services.Execute<ConcessionsRQ, ConcessionsRS>("Concessions.CancelConcessionsOrder", concessionsRequest);
            //                //}

            //            }

            //            if (responce.CodeResult != "Z")
            //            {
            //                Session.Abandon();
            //            }

            //        }
            //        else
            //        {
            //            //responce = new PaymentRS();
            //            responce.CodeResult = "Cedula no coincide";
            //            responce.DescriptionResult = "La cédula del usuario registrado no coincide con la cédula del tarjetahabiente";
            //            //Session.Abandon(); no elimino la sesion acá para poder usarla en el metodo Result para setear el viewBag.
            //            //luego de haber armado el viewBag si hago session.abandon()
            //        }
            //    }
            //    else
            //    {
            //        responce.CodeResult = "Error Tiempo";
            //        //Session.Abandon(); no elimino la sesion acá para poder usarla en el metodo Result para setear el viewBag.
            //        //luego de haber armado el viewBag si hago session.abandon()
            //    }
            //}
            //else
            //{
            //    responce.CodeResult = "Error Session Nula";
            //    Session.Abandon();
            //}
            return responce;
        }

        #region Private Methods

        private string EncryptVippo(string inputString)
        {
            try
            {
                //DECLARACION DE LA LIBRERIA PARA ENCRIPTAR
                TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();

                //DECLARACION DE LAS LLAVES PARA EL ALGORITMO DE ENCRIPTACION
                byte[] keyDest = new byte[24] { 78, 58, 78, 31, 44, 63, 121, 29, 41, 27, 113, 17, 3, 64, 27, 2, 78, 58, 78, 31, 44, 63, 11, 29 };
                byte[] IVector = new byte[8] { 122, 23, 98, 32, 67, 32, 86, 57 };

                //ASIGNACION DE LAS LLAVES DE ENCRIPTACION AL OBJETO QUE NOS SERVIRÁ PARA ENCRIPTAR
                tripleDes.Key = keyDest;
                tripleDes.IV = IVector;

                //DECLARACIÓN DEL TIPO DE ENCRIPTACIÓN Y EL MÉTODO DE PADDING A REALIZAR 
                tripleDes.Mode = CipherMode.CBC;
                tripleDes.Padding = PaddingMode.PKCS7;

                //ASIGNACIÓN DEL TEXTO DE ENTRADA AL PROCESO DE ENCRIPTACIÓN Y EJECUCIÓN DE LA ENCRIPTACIÓN
                byte[] buffer = Encoding.UTF8.GetBytes(inputString);
                ICryptoTransform ITransform = tripleDes.CreateEncryptor();
                return Convert.ToBase64String(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
            }
            catch (Exception)
            {
                throw;
            }
        }

        private UserSession ReadUserSession(Guid userSessionId, bool readMap)
        {
            Service services = new Service();
            UserSessionRS sessionResponse = services.Execute<UserSessionRS>("UserSession.Read",
                new { userSessionId = userSessionId, readMap = readMap, token = TokenServices });
            UserSession userSession = (UserSession)sessionResponse;
            return userSession;
        }

        [Authorize]
        private string SaveImageToCloudStorage(string fileName, string filePath)
        {
            string BlobPath = string.Empty;

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("qrcodes");

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

            using (var fileStream = System.IO.File.OpenRead(filePath))
            {
                blockBlob.UploadFromStream(fileStream);
            }


            return BlobPath = fileName;

            //return BlobPath;
        }

        #endregion Private Methods

        #region RegisterRoutes
        [Authorize]
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
              name: "GetTicket",
              url: "Compra/Seleccion_Tickets/{theaterId}/{sessionId}",
              defaults: new { controller = "Purchase", action = "GetTicket" });

            routes.MapRoute(
              name: "GetHall",
              url: "Compra/Seleccion_Butacas",
              defaults: new { controller = "Purchase", action = "GetHall" });

            routes.MapRoute(
            name: "GetConcessions",
            url: "Compra/Carameleria",
            defaults: new { controller = "Purchase", action = "GetConcessions" });

            routes.MapRoute(
              name: "GetConcessionsOrder",
              url: "Compra/Concessions/Order/{userSessionId}",
              defaults: new { controller = "Purchase", action = "GetConcessionsOrder" });

            routes.MapRoute(
             name: "ViewHall",
             url: "Compra/Cambio_Butacas",
             defaults: new { controller = "Purchase", action = "ViewHall" });

            routes.MapRoute(
             name: "Payment",
             url: "Compra/Pago",
             defaults: new { controller = "Purchase", action = "Confirmation" });

            routes.MapRoute(
              name: "PaymentError",
              url: "Compra/Error/{userSessionId}",
              defaults: new { controller = "Purchase", action = "NotSucceed" });

            routes.MapRoute(
              name: "PaymentSuccess",
              url: "Compra/Exito/{userSessionId}",
              defaults: new { controller = "Purchase", action = "Success" });

            routes.MapRoute(
            name: "ViewTerms",
            url: "Compra/Terminos",
            defaults: new { controller = "Purchase", action = "ViewTerms" });

            routes.MapRoute(
              name: "EditConcessions",
              url: "Compra/Cambio_Carameleria",
             defaults: new { controller = "Purchase", action = "EditConcessions" });

            routes.MapRoute(
              name: "PaymentResult",
              url: "Compra/Resultado/{userSessionId}",
              defaults: new { controller = "Purchase", action = "Result" });
        }

        #endregion RegisterRoutes
    }
}