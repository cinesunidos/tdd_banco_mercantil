﻿using CinesUnidos.WebRoles.MVC.Models.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CinesUnidos.WebRoles.MVC.Controllers
{
    [Authorize]
    public class QuinielaController : Controller
    {
        // GET: Quiniela
        [HttpGet]
        public ActionResult Index()
        {
            HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = null;
            ticket = FormsAuthentication.Decrypt(cookie.Value);
            Models.Support.Serializer serial = new Models.Support.Serializer();
            Models.TokenInfo userdata = (Models.TokenInfo)serial.DeserializeFromMemory(ticket.UserData, typeof(Models.TokenInfo));
            using (SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
            {
                string csql = "select * from tblQuiniela where [IdentificationNumber] = @parametro";
                SqlParameter parametro = new SqlParameter
                {
                    ParameterName = "parametro",
                    Value = userdata.Identification
                };
                SqlCommand cmd = new SqlCommand(csql, c);
                cmd.Parameters.Add(parametro);
                c.Open();
                SqlDataReader r = cmd.ExecuteReader();
                if (r.HasRows)
                {
                    r.Read();
                    QuinielaObject q = JsonConvert.DeserializeObject<QuinielaObject>(r[4].ToString());
                    cmd.Dispose();
                    c.Close();
                    QuinielaObject2 q2 = new QuinielaObject2();
                    q2.MActor = q.MActor.Where(a => a.Selected == true).ToList();
                    q2.MActorSecundario = q.MActorSecundario.Where(a => a.Selected == true).ToList();
                    q2.MActriz = q.MActriz.Where(a => a.Selected == true).ToList();
                    q2.MActrizSecundaria = q.MActrizSecundaria.Where(a => a.Selected == true).ToList();
                    q2.MBandaSonora = q.MBandaSonora.Where(a => a.Selected == true).ToList();
                    q2.MCortoDeAccionReal = q.MCortoDeAccionReal.Where(a => a.Selected == true).ToList();
                    q2.MCortoDeAnimacion = q.MCortoDeAnimacion.Where(a => a.Selected == true).ToList();
                    q2.MCortoDocumental = q.MCortoDocumental.Where(a => a.Selected == true).ToList();
                    q2.MDirector = q.MDirector.Where(a => a.Selected == true).ToList();
                    q2.MDisenoProduccion = q.MDisenoProduccion.Where(a => a.Selected == true).ToList();
                    q2.MDocumental = q.MDocumental.Where(a => a.Selected == true).ToList();
                    q2.MEfectoSonoro = q.MEfectoSonoro.Where(a => a.Selected == true).ToList();
                    q2.MEfectosVisuales = q.MEfectosVisuales.Where(a => a.Selected == true).ToList();
                    q2.MFotografia = q.MFotografia.Where(a => a.Selected == true).ToList();
                    q2.MGuionAdaptado = q.MGuionAdaptado.Where(a => a.Selected == true).ToList();
                    q2.MGuionOriginal = q.MGuionOriginal.Where(a => a.Selected == true).ToList();
                    q2.MMaquillajePeliqueria = q.MMaquillajePeliqueria.Where(a => a.Selected == true).ToList();
                    q2.MmejorCancion = q.MmejorCancion.Where(a => a.Selected == true).ToList();
                    q2.MMejorMontaje = q.MMejorMontaje.Where(a => a.Selected == true).ToList();
                    q2.MMejorVestuario = q.MMejorVestuario.Where(a => a.Selected == true).ToList();
                    q2.MPelicula = q.MPelicula.Where(a => a.Selected == true).ToList();
                    q2.MPeliculAanimada = q.MPeliculAanimada.Where(a => a.Selected == true).ToList();
                    q2.MPeliculaLenguaNoInglesa = q.MPeliculaLenguaNoInglesa.Where(a => a.Selected == true).ToList();
                    q2.MSonido = q.MSonido.Where(a => a.Selected == true).ToList();
                    ViewBag.Activo = false;
                    ViewBag.Expirado = false;
                    return View("QuinielaP", q2);
                }
                else
                {
                    cmd.Dispose();
                    c.Close();
                    DateTime horaFinal = new DateTime(2019, 2, 24, 16, 0, 0);
                    DateTime LocalDate = DateTime.Now.ToUniversalTime();
                    TimeZoneInfo TimeZone = TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
                    LocalDate = TimeZoneInfo.ConvertTimeFromUtc(LocalDate, TimeZone);
                    if (LocalDate > horaFinal)
                    {
                        ViewBag.Expirado = true;
                        QuinielaObject Q = new QuinielaObject();
                        return View(Q);
                    }
                    else
                    {
                        QuinielaObject Q = new QuinielaObject();
                        ViewBag.Expirado = false;
                        return View(Q);
                    }
                }
            }
        }
        [HttpPost]
        public ActionResult QuinielaP(QuinielaObject2 q, string instagram)
        {
            //DateTime date = new DateTime(2018, 03, 04, 21, 0, 0);
            //if (DateTime.Now >= date)
            //{
            //    ViewBag.Message = "Estimado Invitado, el concurso ya culminó. Gracias por tu participación.";
            //    ViewBag.Activo = true;
            //    QuinielaObject2 q1 = new QuinielaObject2();
            //    return View(q1);
            //}
            QuinielaObject2 q2 = new QuinielaObject2();
            q2.MActor = q.MActor.Where(a => a.Selected == true).ToList();
            q2.MActorSecundario = q.MActorSecundario.Where(a => a.Selected == true).ToList();
            q2.MActriz = q.MActriz.Where(a => a.Selected == true).ToList();
            q2.MActrizSecundaria = q.MActrizSecundaria.Where(a => a.Selected == true).ToList();
            q2.MBandaSonora = q.MBandaSonora.Where(a => a.Selected == true).ToList();
            q2.MCortoDeAccionReal = q.MCortoDeAccionReal.Where(a => a.Selected == true).ToList();
            q2.MCortoDeAnimacion = q.MCortoDeAnimacion.Where(a => a.Selected == true).ToList();
            q2.MCortoDocumental = q.MCortoDocumental.Where(a => a.Selected == true).ToList();
            q2.MDirector = q.MDirector.Where(a => a.Selected == true).ToList();
            q2.MDisenoProduccion = q.MDisenoProduccion.Where(a => a.Selected == true).ToList();
            q2.MDocumental = q.MDocumental.Where(a => a.Selected == true).ToList();
            q2.MEfectoSonoro = q.MEfectoSonoro.Where(a => a.Selected == true).ToList();
            q2.MEfectosVisuales = q.MEfectosVisuales.Where(a => a.Selected == true).ToList();
            q2.MFotografia = q.MFotografia.Where(a => a.Selected == true).ToList();
            q2.MGuionAdaptado = q.MGuionAdaptado.Where(a => a.Selected == true).ToList();
            q2.MGuionOriginal = q.MGuionOriginal.Where(a => a.Selected == true).ToList();
            q2.MMaquillajePeliqueria = q.MMaquillajePeliqueria.Where(a => a.Selected == true).ToList();
            q2.MmejorCancion = q.MmejorCancion.Where(a => a.Selected == true).ToList();
            q2.MMejorMontaje = q.MMejorMontaje.Where(a => a.Selected == true).ToList();
            q2.MMejorVestuario = q.MMejorVestuario.Where(a => a.Selected == true).ToList();
            q2.MPelicula = q.MPelicula.Where(a => a.Selected == true).ToList();
            q2.MPeliculAanimada = q.MPeliculAanimada.Where(a => a.Selected == true).ToList();
            q2.MPeliculaLenguaNoInglesa = q.MPeliculaLenguaNoInglesa.Where(a => a.Selected == true).ToList();
            q2.MSonido = q.MSonido.Where(a => a.Selected == true).ToList();


            HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = null;
            ticket = FormsAuthentication.Decrypt(cookie.Value);
            Models.Support.Serializer serial = new Models.Support.Serializer();
            Models.TokenInfo userdata = (Models.TokenInfo)serial.DeserializeFromMemory(ticket.UserData, typeof(Models.TokenInfo));
            var rrrrrr = JsonConvert.SerializeObject(q2);
            using (SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
            {
                string csql = "select * from tblQuiniela where [IdentificationNumber] = @parametro";
                SqlParameter parametro = new SqlParameter
                {
                    ParameterName = "parametro",
                    Value = userdata.Identification
                };
                SqlCommand cmd = new SqlCommand(csql, c);
                cmd.Parameters.Add(parametro);
                c.Open();
                SqlDataReader r = cmd.ExecuteReader();
                if (r.HasRows)
                {
                    ViewBag.Message = "Estimado Invitado, Ya usted tiene una participacion activa en esta Quiniela.";
                    ViewBag.Activo = true;
                }
                else
                {
                    c.Close();
                    c.Open();
                    csql = "insert into tblQuiniela (IdentificationType, IdentificationNumber, Email, Selection, DateSelection, InstagramUser)"
                        + " values (@IdentificationType, @IdentificationNumber, @Email, @Selection, getdate(), @InstagramUser)";
                    SqlParameter IdentificationType = new SqlParameter
                    {
                        ParameterName = "IdentificationType",
                        Value = userdata.IdentificationType
                    };

                    SqlParameter IdentificationNumber = new SqlParameter
                    {
                        ParameterName = "IdentificationNumber",
                        Value = userdata.Identification
                    };
                    SqlParameter Email = new SqlParameter
                    {
                        ParameterName = "Email",
                        Value = userdata.Email
                    };

                    SqlParameter Selection = new SqlParameter
                    {
                        ParameterName = "Selection",
                        Value = JsonConvert.SerializeObject(q2)
                    };

                    SqlParameter InstagramUser = new SqlParameter
                    {
                        ParameterName = "InstagramUser",
                        Value = instagram
                    };
                    cmd = new SqlCommand(csql, c);
                    SqlParameter[] ps = new SqlParameter[] { IdentificationType, IdentificationNumber, Email, Selection, InstagramUser };
                    cmd.Parameters.AddRange(ps);
                    cmd.ExecuteNonQuery();
                    ViewBag.Message = "¡Gracias por tu participación! Recuerda chequear nuestro términos y condiciones.";
                    ViewBag.Activo = false;
                }
                c.Close();
                cmd.Dispose();
            }
            return View(q2);
        }
    }
}