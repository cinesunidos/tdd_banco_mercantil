﻿using CinesUnidos.WebRoles.MVC.Models;
using CinesUnidos.WebRoles.MVC.Models.Filters;
using CinesUnidos.WebRoles.MVC.Models.Support;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace CinesUnidos.WebRoles.MVC.Controllers
{
    public class ContactController : Controller
    {
        [HttpPost]
        public ActionResult SendComplaint(Complaint model)
        {
            var contexto = HttpContext;
            var ip1 = HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var ip2 = HttpContext.Request.ServerVariables["REMOTE_ADDR"];
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (String.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ModelState.AddModelError("EmptyCaptcha", "El captcha es requerido.");
                return View(model);
            }

            RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();

            if (recaptchaResult != RecaptchaVerificationResult.Success)
            {
                ModelState.AddModelError("InvalidCaptcha", "Respuesta incorrecta.");
                return View(model);
            }

            if (ModelState.IsValid)
            {
                #region compleintDelivery

                Service services = new Service();
                Response response = services.Execute<Complaint, Response>("Contact.SendComplaint", model);
                if (response.isOk == false)
                {
                    return View("NotSucceed", model);
                }
                else
                {
                    return View("Success", model);
                }

                #endregion compleintDelivery
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        //////[ExitHttpsIfNotRequired()]
        public ActionResult SendComplaint()
        {
            return View();
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
             name: "Contact",
             url: "Contacto",
             defaults: new { controller = "Contact", action = "SendComplaint" });
        }
    }
}