﻿using CinesUnidos.WebRoles.MVC.Models;
using CinesUnidos.WebRoles.MVC.Models.Support;
using CinesUnidos.WebRoles.MVC.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CinesUnidos.WebRoles.MVC.Controllers
{
    public class AdminController : Controller
    {
        //AdminRepository repo = new AdminRepository();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Login(LoginViewModel model)
        {
            if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.Email))
            {
                Service services = new Service();
                UserEntity usr = new UserEntity();
                usr.Password = model.Password;
                usr.Email = model.Email;
                UserEntity uss = services.Execute<UserEntity, UserEntity>("Admin.Login", usr);
                if (uss != null && uss.Name != null)
                {
                    if (!uss.Name.Contains("Error"))
                    {
                        Session["AdminName"] = uss.Name + " " + uss.LastName;
                        return View("AdminPanel");
                    }
                    else
                    {
                        ModelState.AddModelError("", uss.Name);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "El nombre de usuario y la contraseña son inválidos");
                }                
            }
            else
            {
                ModelState.AddModelError("", "El nombre de usuario y la contraseña son requeridos");
            }
            //if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.Email))
            //{
            //    Service services = new Service();
            //    UserEntity usr = new UserEntity();
            //    usr.Password = model.Password;
            //    usr.Email = model.Email;
            //    UserEntity uss = services.Execute<UserEntity, UserEntity>("Admin.Login", usr);
            //    if (uss != null && uss.Name != null)
            //    {
            //        if (!uss.Name.Contains("Error"))
            //        {
            //            Session["AdminName"] = uss.Name + " " + uss.LastName;
            //            return View("AdminPanel");
            //        }
            //        else
            //        {
            //            ModelState.AddModelError("", uss.Name);
            //        }
            //    }
            //    else
            //    {
            //        ModelState.AddModelError("", "El nombre de usuario y la contraseña son inválidos");
            //    }                
            //}
            //else
            //{
            //    ModelState.AddModelError("", "El nombre de usuario y la contraseña son requeridos");
            //}
            return View(model);
        }

        public ActionResult AdminPanel()
        {
            //if (Session["AdminName"] == null)
            //    return View("Login");
            //else
            return View();
        }

        [HttpPost]
        public ActionResult Process(string accion)
        {
            //try
            //{
            //    if (accion == "RemoverCache")
            //    {
            //        Service services = new Service();
            //       /* string premiers = services.Execute<string>("Premier.Clear");
            //        string theater = services.Execute<string>("Theater.Clear");
            //        string movieBillboard = services.Execute<string>("Movie.Clear");*/
            //        ViewBag.Mensaje="Se ha removido la caché";
            //    }
            //   else  if (accion == "CerrarSesion")
            //    {
            //        Session["AdminName"] = null;
            //        Session.Abandon();
            //        return RedirectToAction("Login");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ViewBag.Mensaje = ex.Message;
            //}
            return View("AdminPanel");
        }
    }
}