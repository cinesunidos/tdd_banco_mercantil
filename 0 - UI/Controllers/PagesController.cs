﻿using CinesUnidos.WebRoles.MVC.Models;
using CinesUnidos.WebRoles.MVC.Models.Filters;
using CinesUnidos.WebRoles.MVC.Models.Support;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;

namespace CinesUnidos.WebRoles.MVC.Controllers
{
    public class PagesController : Controller
    {
        private string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();
        //Acción que despliega la sección de
        //carameleria del sitio web
        [HttpGet]
        //[ExitHttpsIfNotRequired()]
        public ActionResult CandyMaker()
        {
            return View();
        }

        public ActionResult MailMovies(string city = "Caracas")
        {
            //Para saber el búmero de la semana
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            DateTime dateWeek = DateTime.UtcNow.TimeZoneCaracas();
            Calendar cal = dfi.Calendar;
            ViewBag.WeekNumber = cal.GetWeekOfYear(dateWeek, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
            ViewBag.Date = dateWeek.ToString("ddMMyy");

            ViewBag.City = city;
            DateTime date = DateTime.UtcNow.TimeZoneCaracas().AddDays(1);
            int day = date.Day;
            int month = date.Month;
            Service services = new Service();
            MovieBillboard[] theater = services.Execute<MovieBillboard[]>("Movie.GetAllCityDate", new { city = city, day = day, month = month, token = TokenServices }).OrderByDescending(x => x.Movie.Id).ToArray();
            if (theater.Count().Equals(0))
            {
                day = DateTime.Now.AddDays(1).Day;
                month = DateTime.Now.AddDays(1).Month;
                theater = services.Execute<MovieBillboard[]>("Movie.GetAllCityDate", new { city = city, day = day, month = month, token = TokenServices }).OrderByDescending(x => x.Movie.Id).ToArray();
            }
            return View(theater);
        }

        public CityTicketPrice[] GetTicketPrice()
        {
            CityTicketPrice[] cityTicketPrice;

            cityTicketPrice = CityTicketPriceCacheClass.CityTicketPrice == null ? CityTicketPriceCacheClass.CityTicketPrice = GetTicketPricesCache() : CityTicketPriceCacheClass.CityTicketPrice;
            return (cityTicketPrice);
        }

        private CityTicketPrice[] GetTicketPricesCache()
        {
            DateTime date = DateTime.Now;
            string str_date = date.ToString("yyyy-MM-dd");
            Service services = new Service();
            CityTicketPrice[] cityTicketPrice = services.Execute<CityTicketPrice[]>("Ticket.GetTicketPrice", new { str_date, token = TokenServices }).OrderBy(x => x.city).ToArray();
            return cityTicketPrice;
        }

        //Acción que despliega la sección de
        //comercialización del sitio web
        [HttpGet]
        //[ExitHttpsIfNotRequired()]
        public ActionResult Merchandising()
        {
            return View();
        }

        //Acción que despliega la seccion de
        //Promociones de la página
        //[ExitHttpsIfNotRequired()]
        public ActionResult Promotions()
        {
            return View(GetTicketPrice());
        }

        [HttpGet]
        public ActionResult PromotionsCMS()
        {
            List<Promotions> promotion = new List<Promotions>();
            List<PromotionsDB> promotionDB = new List<PromotionsDB>();
            Service services = new Service();
            string promo = services.Execute<string>("Promotions.GetPromotions", new { token = TokenServices });
#if DEBUG
            IFormatProvider formato = new CultureInfo("es-VE", true);
#endif
#if !DEBUG
                        IFormatProvider formato = new CultureInfo("es-VE", true);
#endif
            if (!promo.Contains("Error: ") && promo.Length > 20)
            {
                try
                {
                    var pro = JsonConvert.DeserializeObject(promo);
                    promotion = JsonConvert.DeserializeObject<List<Promotions>>(pro.ToString());
                    foreach (var p in promotion)
                    {
                        string[] SeparatorsStartDate = null;
                        string[] SeparatorsExpirationDate = null;

                        string diaSD = null;
                        string mesSD = null;
                        string SDfecha = null;

                        string diaED = null;
                        string mesED = null;
                        string EDfecha = null;

#if DEBUG
                        SeparatorsStartDate = null;
                        SeparatorsStartDate = p.StartDate.Split('/');
                        SeparatorsExpirationDate = null;
                        SeparatorsExpirationDate = p.ExpirationDate.Split('/');

                        diaSD = Convert.ToInt32(SeparatorsStartDate[0].Length) == 1 ? "0" + SeparatorsStartDate[0] : SeparatorsStartDate[0];
                        mesSD = Convert.ToInt32(SeparatorsStartDate[1].Length) == 1 ? "0" + SeparatorsStartDate[1] : SeparatorsStartDate[1];
                        SDfecha = diaSD + "/" + mesSD + "/" + SeparatorsStartDate[2];

                        diaED = Convert.ToInt32(SeparatorsExpirationDate[0].Length) == 1 ? "0" + SeparatorsExpirationDate[0] : SeparatorsExpirationDate[0];
                        mesED = Convert.ToInt32(SeparatorsExpirationDate[1].Length) == 1 ? "0" + SeparatorsExpirationDate[1] : SeparatorsExpirationDate[1];
                        EDfecha = diaED + "/" + mesED + "/" + SeparatorsExpirationDate[2];
#endif
#if !DEBUG

                        SeparatorsStartDate = null;
                        SeparatorsStartDate = p.StartDate.Replace("-", "/").Split('/');
                        SeparatorsExpirationDate = null;
                        SeparatorsExpirationDate = p.ExpirationDate.Replace("-", "/").Split('/');

                        diaSD = Convert.ToInt32(SeparatorsStartDate[0].Length) == 1 ? "0" + SeparatorsStartDate[0] : SeparatorsStartDate[0];
                        mesSD = Convert.ToInt32(SeparatorsStartDate[1].Length) == 1 ? "0" + SeparatorsStartDate[1] : SeparatorsStartDate[1];
                        SDfecha = diaSD + "/" + mesSD + "/" + SeparatorsStartDate[2];

                        diaED = Convert.ToInt32(SeparatorsExpirationDate[0].Length) == 1 ? "0" + SeparatorsExpirationDate[0] : SeparatorsExpirationDate[0];
                        mesED = Convert.ToInt32(SeparatorsExpirationDate[1].Length) == 1 ? "0" + SeparatorsExpirationDate[1] : SeparatorsExpirationDate[1];
                        EDfecha = diaED + "/" + mesED + "/" + SeparatorsExpirationDate[2];
#endif

                        promotionDB.Add(new PromotionsDB
                        {
                            Description = p.Description,
                            Image = p.Image,
                            Title = p.Title,
                            StartDate = DateTime.Parse(SDfecha, formato),
                            ExpirationDate = DateTime.Parse(EDfecha, formato)
                        });
                    }
                }
                catch (Exception e)
                {

                }
            }

            if (promo.Contains("Error: "))
            {
                ViewData["ErrorService"] = "error";
                return PartialView();
            }

            if (promo.Contains("No_File"))
            {
                string promoDB = services.Execute<string>("Promotions.GetPromotionsDB", new { token = TokenServices });

                if (!(promoDB.Contains("Error: ")))
                {
                    var json1 = JsonConvert.DeserializeObject(promoDB);
                    var jsonObj1 = new JavaScriptSerializer().Deserialize<List<Promotions>>(json1.ToString());
                    List<PromotionsDB> PromotionsDB = new List<PromotionsDB>();

                    foreach (var obj1 in jsonObj1)
                    {
                        if (!(string.IsNullOrEmpty(obj1.Description)) || !(string.IsNullOrEmpty(obj1.Title)) || !(string.IsNullOrEmpty(obj1.Image)))
                        {

                            string[] SeparatorsStartDate = null;
                            SeparatorsStartDate = obj1.StartDate.Split('/');
                            string[] SeparatorsExpirationDate = null;
                            SeparatorsExpirationDate = obj1.ExpirationDate.Split('/');

                            string diaSD = Convert.ToInt32(SeparatorsStartDate[0].Length) == 1 ? "0" + SeparatorsStartDate[0] : SeparatorsStartDate[0];
                            string mesSD = Convert.ToInt32(SeparatorsStartDate[1].Length) == 1 ? "0" + SeparatorsStartDate[1] : SeparatorsStartDate[1];
                            string SDfecha = mesSD + "/" + diaSD + "/" + SeparatorsStartDate[2];

                            string diaED = Convert.ToInt32(SeparatorsExpirationDate[0].Length) == 1 ? "0" + SeparatorsExpirationDate[0] : SeparatorsExpirationDate[0];
                            string mesED = Convert.ToInt32(SeparatorsExpirationDate[1].Length) == 1 ? "0" + SeparatorsExpirationDate[1] : SeparatorsExpirationDate[1];
                            string EDfecha = mesED + "/" + diaED + "/" + SeparatorsExpirationDate[2];

                            PromotionsDB.Add(new PromotionsDB
                            {
                                Title = obj1.Title,
                                Description = obj1.Description,
                                Image = obj1.Image,
                                StartDate = DateTime.Parse(SDfecha, formato),
                                ExpirationDate = DateTime.Parse(EDfecha, formato)
                            });
                        }
                    }
                    ViewData["Promotion"] = "PromotionsDB";
                    return PartialView(PromotionsDB);
                }
                else
                {
                    ViewData["ErrorDB"] = "error";
                    return PartialView();
                }
            }
            //IFormatProvider cultura = new CultureInfo("en-US", true);
            //promotionDB = promotionDB.Where(promotio => (promotio.StartDate) <= DateTime.Now && (promotio.ExpirationDate)).OrderByDescending(promotio => Convert.ToDateTime(promotio.StartDate)).ToList();
            promotionDB = promotionDB.Where(p => p.StartDate <= DateTime.Now && p.ExpirationDate >= DateTime.Now).OrderByDescending(p => p.StartDate).ToList();
            //List<Promotions> promoti = promotion.Where(promotio => ((DateTime.Parse(promotio.StartDate, cultura) <= DateTime.Now)) && (DateTime.Now <= DateTime.Parse(promotio.ExpirationDate, cultura))).OrderByDescending(promotio => Convert.ToDateTime(promotio.StartDate)).ToList();

            ViewData["Promotion"] = "Promotion";
            return PartialView(promotionDB);
        }

        //Acción para la promoción de 50 sombras de Grey
        //[HttpGet]
        //public ActionResult ShadesOfGrey()
        //{
        //    return PartialView();
        //}


        //Acción para la promoción de 50 sombras de Grey
        //[HttpGet]
        //public ActionResult Minions2015(string city)
        //{
        //    ViewBag.City = city;
        //    return View();
        //}

        //public ActionResult Test()
        //{
        //    return View();
        //}

        //public ActionResult Anniversary()
        //{
        //    return View();
        //}

        //public ActionResult EscuadronSuicida()
        //{
        //    return View();
        //}

        public ActionResult Newsletter()
        {
            return View();
        }

        #region RegisterRoutes

        //Registro de las rutas de las acciones del controladores
        //para hacer mas amistoso su acceso desde el browser
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               name: "SuicideSquad",
               url: "EscuadronSuicida",
               defaults: new { controller = "Pages", action = "EscuadronSuicida" });
            routes.MapRoute(
                name: "CandyMaker",
                url: "Carameleria",
                defaults: new { controller = "Pages", action = "CandyMaker" });
            routes.MapRoute(
                name: "Merchandising",
                url: "Comercializacion",
                defaults: new { controller = "Pages", action = "Merchandising" });
            routes.MapRoute(
                name: "Promotions",
                url: "Promociones",
                defaults: new { controller = "Pages", action = "Promotions" });
            routes.MapRoute(
                name: "MailMovie",
                url: "PeliculasCorreo",
                defaults: new { controller = "Pages", action = "MailMovies" });
            routes.MapRoute(
                name: "Anniversary",
                url: "Anniversary",
                defaults: new { controller = "Pages", action = "Anniversary" });
            routes.MapRoute(
                name: "Newsletter",
                url: "Newsletter",
                defaults: new { controller = "Pages", action = "Newsletter" });
        }

        #endregion RegisterRoutes
    }
}