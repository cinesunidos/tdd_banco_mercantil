var map = null;
var infowindow = new google.maps.InfoWindow();
var ic = 0;
var allinfo = null;
function mostrar_mapa(ubi_mapa) {
    idmapa = ubi_mapa.idmapa;
    lat = ubi_mapa.lat;
    long = ubi_mapa.long;
    html = ubi_mapa.html;
    titlem = ubi_mapa.titlem;

    var myLatlng = new google.maps.LatLng(lat, long);
    var myOptions = {
        zoom: 10,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById(idmapa), myOptions);

    var point1 = new google.maps.LatLng(lat, long);
    var titlem1 = titlem;
    var html1 = html;
    marker_mapa(point1, html1, titlem1);
}

function marker_mapa(point, html, titlem) {
    var marker = new google.maps.Marker({
        position: point,
        map: map,
        title: titlem
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.close();
        infowindow.setContent(html);
        infowindow.open(map, marker);
    });
}