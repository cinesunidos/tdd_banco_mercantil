var ubi_mapa = new Array();
ubi_mapa[0] = 'gmap0 , 10.487873 , -66.86157 , titulo texto , Cines1 Unidos';
ubi_mapa[1] = 'gmap1 , 8.689639 , -71.019288 , titulo2 texto , Cines 2Unidos';
ubi_mapa[2] = 'gmap2 , 9.860628 , -69.371338 , ti texto , Cines 3Unidos';
ubi_mapa[3] = 'gmap3 , 9.838979 , -64.691163 , ubicacion texto , Cines4 Unidos';
ubi_mapa[4] = 'gmap4 , 7.689217 , -64.031983 , ubicacion texto , Cines5 Unidos';
ubi_mapa[5] = 'gmap5 , 11.070603 , -64.141846 , ti texto , Cines 3Unidos';
ubi_mapa[6] = 'gmap6 , 10.53102 , -71.590577 , ubicacion texto , Cines4 Unidos';
ubi_mapa[7] = 'gmap7 , 11.480025 , -69.700928 , ubicacion texto , Cines5 Unidos';

$(document).ready(function () {
    //***************Iniciar sesión*****************************/
    $(".micuenta").click(function () {
        $(".caja-iniciar-sesion-01").stop();
        $(".caja-iniciar-sesion-01").animate({ "height": "65px" }, 400);
    });
    $(".micuenta").hover(function () {
        $(".caja-iniciar-sesion-01").stop();
        $(".caja-iniciar-sesion-01").animate({ "height": "65px" }, 400);
    },
function () {
    jQuery(this).find(".descrip1").stop();
    $(".caja-iniciar-sesion-01").stop();
    $(".caja-iniciar-sesion-01").animate({ "height": "0px" }, 400);
});
    //***************Seleccionar ciudad sesión*****************************/
    //$("#ciudad_id").selectbox();
    //***************Seleccionar salas*****************************/
    $(".mnu_salasciudad li").click(function () {
        //alert(this.html());
        $('a.select_salas').html(jQuery(this).find('a').html() + "<span></span>");
        pos = $(this).index();
        var vet = document.getElementById('mnu_salasciudad').getElementsByTagName('li');
        var i = vet.length;
        while (i--) {
            vet[i].className = ' ';
            if (i != pos)
                $("#vent" + i).css("display", "none");
        }
        vet[pos].className = 'mnu_salasciudadact';
        $("#vent" + pos).css("display", "table");
        mostrar_mapa(ubi_mapa[pos]);
    });

    mostrar_mapa(ubi_mapa[0]);
    tooltip();
});

//***************Tooltip*****************************/
this.tooltip = function () {
    xOffset = 10;
    yOffset = 30;
    $("a.screenshot").hover(function (e) {
        this.t = this.title;
        this.title = "";
        var c = (this.t != "") ? "<br/>" + this.t : "";
        $("body").append("<p id='screenshot'><img src='" + this.rel + "' alt='url preview' />" + c + "</p>");
        $("#screenshot")
			.css("top", (e.pageY - xOffset) + "px")
			.css("left", (e.pageX + yOffset) + "px")
			.fadeIn("fast");
    },
	function () {
	    this.title = this.t;
	    $("#screenshot").remove();
	});
    $("a.screenshot").mousemove(function (e) {
        $("#screenshot")
			.css("top", (e.pageY - xOffset) + "px")
			.css("left", (e.pageX + yOffset) + "px");
    });

    $(".tooltip").hover(function (e) {
        this.t = this.title;
        this.title = "";
        $("body").append("<p id='tooltip'>" + this.t + "</p>");
        $("#tooltip")
			.css("top", (e.pageY - xOffset) + "px")
			.css("left", (e.pageX + yOffset) + "px")
			.fadeIn("fast");
    },
	function () {
	    this.title = this.t;
	    $("#tooltip").remove();
	});
    $(".tooltip").mousemove(function (e) {
        $("#tooltip")
			.css("top", (e.pageY - xOffset) + "px")
			.css("left", (e.pageX + yOffset) + "px");
    });
};