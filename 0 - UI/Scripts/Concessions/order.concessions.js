﻿var Order = function () {

    //console.log("Order::new");

    this.orderTotal = 0;
    this.subTotalConcessions = 0;
    this.totalConcessions = 0;
    this.totalTickets = 0;
    this.items = [];

    this.MAX_ITEM_QUANTITY = 0;
    this.WEB_FEE_PRICE = 0.0;
    this.IVA = 0.0;

    //
    // Private functions
    //

    this.addItem = function (item) {
        if (this.hasItem(item.itemId)) {
            var index = this.indexOfItem(item.itemId);
            this.items[index] = item;
        }
        else {
            this.items.push(item);
        }
    };

    this.removeItem = function (item) {
        var index = this.indexOfItem(item.itemId);
        if (index !== -1) {
            this.items.splice(index, 1);
            return true;
        }
        return false;
    };

    this.hasItem = function (itemId) {
        for (var i in this.items) {
            var item = this.items[i];
            if (item.itemId == itemId) {
                return true;
            }
        }
        return false;
    };

    this.indexOfItem = function (itemId) {
        for (var i in this.items) {
            var item = this.items[i];
            if (item.itemId == itemId) {
                return i;
            }
        }
        return -1;
    };

    this.pct = function (value, n) {
        //return ((value * n) / 100);
        //console.log("pct: " + value);
        return ((value) / ((n / 100) + 1));
    };

    this.formatValue = function (value) {
        //console.log("formatvalue:" + value);
        var valueStr = value.toFixed(2).replace(".", ",");
        var indice;
        if (valueStr.split(",")[0].length == 4) {
            indice = 1;
        } else if (valueStr.split(",")[0].length == 5) {
            indice = 2;
        } else {
            return valueStr;
        }
        return valueStr.splice(indice, 0, ".");
    };

};

Order.prototype.configure = function (config) {
    if (config["maxItemQuantity"]) {
        this.MAX_ITEMS_QUANTITY = parseInt(config["maxItemQuantity"]);
    }
    if (config["webFeePrice"]) {
        this.WEB_FEE_PRICE = parseFloat(config["webFeePrice"]);
    }
    if (config["iva"]) {
        this.IVA = parseFloat(config["iva"]);
    }
    if (config["totalTickets"]) {
        this.totalTickets = parseFloat(config["totalTickets"]);
    }
};

Order.prototype.calculateTotal = function () {

    this.totalConcessions = 0;
    this.subTotalConcessions = 0;

    for (var i in this.items) {
        var item = this.items[i];
        this.subTotalConcessions += parseFloat(item.price) * parseFloat(item.quantity);
    }

    this.totalConcessions = this.subTotalConcessions;

    if (this.items.length > 0) {
        this.totalConcessions += this.WEB_FEE_PRICE;
    }

    this.orderTotal = this.totalTickets + this.totalConcessions;

    /*
    console.log({
        totalTickets: this.totalTickets,
        subTotalConcessions: this.subTotalConcessions,
        totalConcessions: this.totalConcessions,
        orderTotal: this.orderTotal
    });*/
};

Order.prototype.updateItem = function (item) {
    //console.log('Order::updateItem', item);
    if (item.quantity <= 0) {
        // remove item
        this.removeItem(item);
    }
    else {
        this.addItem(item);
    }
    this.calculateTotal();
};

Order.prototype.getSubTotalCarameleriaSinIva = function () {
    var iva = this.pct(this.subTotalConcessions, this.IVA);
    //console.log("subtotal carameleria: " + iva);
    var value = this.subTotalConcessions - iva;
    return this.formatValue(value);
};

Order.prototype.getIVACarameleria = function () {
    var value = this.pct(this.subTotalConcessions, this.IVA);
    return this.formatValue(value);
};

Order.prototype.getCargoWebSinIVA = function () {
    var iva = this.pct(this.WEB_FEE_PRICE, this.IVA);
    var value = this.WEB_FEE_PRICE - iva;
    return this.formatValue(value);
    //return value;
};

Order.prototype.getIVACargoWeb = function () {
    var value = this.pct(this.WEB_FEE_PRICE, this.IVA);
    return this.formatValue(value);
};

// iva concesiones + iva cargo web
Order.prototype.getTotalIVA = function () {
    var ivaConcesiones = this.subTotalConcessions - this.pct(this.subTotalConcessions, this.IVA);
    //console.log("WEB_FEE_PRICE:" + this.WEB_FEE_PRICE);
    var ivaCargoWeb = this.WEB_FEE_PRICE - this.pct(this.WEB_FEE_PRICE, this.IVA);
    var value = parseFloat(ivaConcesiones) + parseFloat(ivaCargoWeb);
    return this.formatValue(value);
};

Order.prototype.getTotalConcessions = function () {
    var value = this.totalConcessions;
    return this.formatValue(value);
};

Order.prototype.getOrderTotal = function () {
    var value = this.orderTotal;
    return this.formatValue(value);
};

$('.buy-cant').on('keydown', function (e) {
    e.preventDefault();
});

var handlerAdd = function (event) {

    event.preventDefault();

    // get item values from html
    var $parent = $(this).parent().parent().first();
    var $parentBox = $(this).parent().parent().parent().parent().first();
    var itemName = $(this).parent().parent().find(".buy-action").attr('data-name');
    var itemQuantity = parseInt($parentBox.find(".buy-cant").val());
    var itemPrice = Formatear_javascript($parentBox.find(".item-price").text().split("Bs. ")[1]);
    var itemAvailable = parseInt($parentBox.find(".buy-cant").first().data('available'));

    if (parseInt(itemQuantity) >= parseInt($(this).attr('data-valormax'))) {
        return false;
    }

    if (order.MAX_ITEMS_QUANTITY != 0 && itemQuantity >= order.MAX_ITEMS_QUANTITY) {
        // no permitir seleccionar mas del limite establecido en MAX_ITEMS_QUANTITY
        return false;
    }

    if (itemAvailable > 0 && itemQuantity >= itemAvailable) {
        // no permitir seleccionar mas de la cantidad disponible
        return false;
    }

    // update html info for item
    itemQuantity += 1;
    $parent.find(".buy-cant").val(itemQuantity);
    $parentBox.addClass("selected-item");

    // update order & render calculations...
    var item = {
        itemId: $parentBox.data('id'),
        itemName: itemName,
        quantity: itemQuantity,
        price: itemPrice
    };
    order.updateItem(item);
    updateOrderDisplay(order);
};

var handlerLess = function (event) {

    event.preventDefault();

    // get item values from html
    var $parent = $(this).parent().parent().first();
    var $parentBox = $(this).parent().parent().parent().parent().first();
    var itemName = $(this).parent().parent().find(".buy-action").attr('data-name');
    var itemQuantity = parseInt($parentBox.find(".buy-cant").val());
    var itemPrice = Formatear_javascript($parentBox.find(".item-price").text().split("Bs. ")[1]);

    if (itemQuantity <= 0) {
        return false;
    }

    // update html info for item
    itemQuantity -= 1;
    $parent.find(".buy-cant").val(itemQuantity);
    if (itemQuantity == 0) {
        $parentBox.removeClass("selected-item");
    }

    // update order & render calculations...
    var item = {
        itemId: $parentBox.data('id'),
        itemName: itemName,
        quantity: itemQuantity,
        price: itemPrice
    };
    order.updateItem(item);
    updateOrderDisplay(order);
};

var updateOrderDisplay = function (order) {

    // show/hide concessions block...
    if (order.items.length > 0) {
        $("#SecctionCarameleria").show();
    }
    else {
        $("#SecctionCarameleria").hide();
    }

    // update items box...
    updateItemsBox(order);

    // update order calculations...

    //  $("#SubTotalConcessions").html(order.getSubTotalCarameleriaSinIva());

    $("#SubTotalConcessions").html(order.getIVACarameleria()); //es subtotal de concesiones

    $("#TotalConcessions").html(order.getTotalConcessions());
    $(".TotalConcessions_").html(order.getTotalConcessions());

    // $("#ConcessionsTax").html(order.getIVACarameleria());
    $("#ConcessionsTax").html(order.getSubTotalCarameleriaSinIva());//es subtotal de iva

    $('#Total').html(order.getOrderTotal());

    //$("#ConcessionsBookingTax").html(Formatear(total_concessions));
    //  $("#BookingTaxTotalConcessions").html(order.getTotalIVA());

    //console.log("iva: " + parseFloat(order.getCargoWebSinIVA()));
    //console.log("iva carameleria: " + parseFloat(order.getSubTotalCarameleriaSinIva()));

    $("#BookingTaxTotalConcessions").html(order.getTotalIVA());



    //    $("#BookingFeeConcessions").html(order.getCargoWebSinIVA());
    //   $("#BookingTaxConcessions").html(order.getIVACargoWeb());

    $("#BookingFeeConcessions").html(order.getIVACargoWeb());
    $("#BookingTaxConcessions").html(order.getCargoWebSinIVA());



};

var updateItemsBox = function (order) {
    var $tab = $('#carameleriatab');
    $tab.empty();
    $tab.append('<h5 style="padding-top:0px;">CARAMELERIA SELECCIONADA</h5>');
    for (var i in order.items) {
        var item = order.items[i];
        $tab.append('<p id=item_id_' + item.itemId + ' item_id = "' + item.itemId + '" class="new_span"><span> ' +
            item.quantity + ' x ' + item.itemName + ':</span> <span>Bs. <strong>' +
            Formatear(item.quantity * item.price) + '</strong></span></p>');
    }
};

var fetchRemoteOrder = function (uuid) {
    //console.log('Order::fetch');
    $.getJSON('/Compra/Concessions/Order/' + uuid, function (data) {
        //console.log(data);
    });
};

function Formatear(valor) {
    valor = valor.toFixed(2).replace(".", ",");
    var indice;

    //  var t = valor.split(",")[0];

    if (valor.split(",")[0].length == 4) {
        indice = 1;
    } else if (valor.split(",")[0].length == 5) {
        indice = 2;
    } else {
        return valor;
    }
    return valor.splice(indice, 0, ".");
}

function Formatear_javascript(valor) {
    //reemplazado mystring.replace(/\./g,' ')
    // var valor = valor.replace(".", " ").replace(".", " ");
    //    valor = valor.replace(",", ".");

    var test = valor.replaceAll(".", "").replaceAll(",", ".");//.replaceAll(" ", ",");

    return parseFloat(test);
    //return (test);
}

String.prototype.replaceAll = function (str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
}

String.prototype.splice = function (idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function calcularPorcentaje(monto, porcentaje) {
    //return ((monto * porcentaje) / 100);
    return ((monto) / ((porcentaje / 100) + 1));
}