var item_id = total_concessions = totalcargoweb = total_iva_concessions = Sub_Total_Concessions = iva_concessions = iva_concessions_web = total_prev = concessions_qty = concessions_stock = 0;
var iva = 16;

$(document).ready(function () {
    $(".more-item").bind("click", handlerAdd);
    $(".less-item").bind("click", handlerLess);
    total_prev = parseFloat(Formatear_javascript($('#Total').html()));
    CheckStock();
});

function handlerAdd(event) {
    var parent = $(this).parent().parent()[0];
    var parentBox = $(this).parent().parent().parent().parent()[0];
    var item_qty = $(parentBox).find(".buy-cant").val();

    if (parseInt(item_qty) < maxConcessionsPerTransaction) {
        event.preventDefault();
        if ($(parent).find(".buy-cant").val() > 0) {
            var cant = parseInt($(parent).find(".buy-cant").val()) + parseInt(1);
        }
        else {
            var cant = 1;
            $(parentBox).addClass("selected-item");
            $("#SecctionCarameleria").show();
        }

        $(parent).find(".buy-cant").val(cant);
        render_bill(parentBox, "Add");
    }
}

function handlerLess(event) {
    var parent = $(this).parent().parent()[0];
    var parentBox = $(this).parent().parent().parent().parent()[0];
    event.preventDefault();

    var cant_actual = $(parent).find(".buy-cant").val();

    if (cant_actual > 1) {
        var cant = parseInt($(parent).find(".buy-cant").val()) - parseInt(1);
    } else {
        var cant = 0;
        $(parentBox).removeClass("selected-item");
    }

    $(parent).find(".buy-cant").val(cant);

    if (cant_actual > 0) {
        render_bill(parentBox, "Less");
    }

    if (Sub_Total_Concessions <= 0 && cant_actual > 0) {
        $("#SecctionCarameleria").hide();
    }
}

function render_bill(parentBox, operation) {

    var item_name = $(parentBox).find(".item-name").text();
    var item_qty = $(parentBox).find(".buy-cant").val();
    var total = total_prev;
    var id = $(parentBox).attr('data-id');
    var item_price = Formatear_javascript($(parentBox).find(".item-price").text().split("Bs. ")[1]);
    var item_buy_cant = item_price * item_qty;

    var exist = false;

    if (item_qty != 0) {
        item_id = id;
    }

    if ($('#carameleriatab p').html() === null) {
        if (item_qty != 0) {
            $('#carameleriatab').append('<p id=item_id_' + id + ' item_id = "' + id + '"><span>' + item_name + ' (' + item_qty + '):</span> <span>Bs. <strong>' + Formatear(item_buy_cant) + '</strong></span></p>');
        }
    } else {
        $('#carameleriatab p').each(function () {
            if (id == $(this).attr('item_id')) {
                exist = true;
            }
        });

        if (exist) {
            var dyn_item = '#item_id_' + id;
            if (item_qty == 0) {
                $(dyn_item).remove();
            }

            $(dyn_item).replaceWith('<p id=item_id_' + id + ' item_id = "' + id + '" class="old_span"><span> ' + item_qty + ' x ' + item_name + ':</span> <span>Bs. <strong>' + Formatear(item_buy_cant) + '</strong></span></p>');
        } else if (item_qty != 0) {
            $('#carameleriatab').append('<p id=item_id_' + id + ' item_id = "' + id + '" class="new_span"><span> ' + item_qty + ' x ' + item_name + ':</span> <span>Bs. <strong>' + Formatear(item_buy_cant) + '</strong></span></p>');
        }
    }

    if (operation == "Add") {
        total += item_price;
        total_concessions += item_price;
    } else {
        total -= item_price;
        total_concessions -= item_price;
    }

    iva_concessions = calcularPorcentaje(total_concessions);
    totalcargoweb = porcweb; //calcularPorcentaje(total_concessions, porcweb);   
    iva_concessions_web = calcularPorcentaje(totalcargoweb);
    var cargo_web_base = totalcargoweb - iva_concessions_web;
    Sub_Total_Concessions = Math.max(total_concessions - iva_concessions, 0);
    total_iva_concessions = iva_concessions + iva_concessions_web;

    if (Sub_Total_Concessions == 0) {
        totalcargoweb = 0;
    }

    total_prev = total;

    $("#SubTotalConcessions").html(Formatear(Sub_Total_Concessions));
    console.log(Sub_Total_Concessions);
    $("#ConcessionsTax").html(Formatear(iva_concessions));
    console.log(iva_concessions);
    $('#Total').html(Formatear(total + totalcargoweb));
    console.log(total + totalcargoweb);
    $("#ConcessionsBookingTax").html(Formatear(total_concessions));
    console.log(total_concessions);
    $("#BookingTaxTotalConcessions").html(Formatear(total_iva_concessions));
    console.log(total_iva_concessions);
    $("#BookingFeeConcessions").html(Formatear(cargo_web_base));
    console.log(cargo_web_base);
    $("#BookingTaxConcessions").html(Formatear(iva_concessions_web));
    console.log(iva_concessions_web);
    $("#TotalConcessions").html(Formatear(total_concessions + totalcargoweb));
    console.log(total_concessions + totalcargoweb);


    concessions_qty = 0;

    CheckZeroQuantity();
}

function CheckZeroQuantity() {
    $("#carameleria_stock").find('input:text')
        .each(function () {
            if ($(this).val() == 0) {
                concessions_qty = concessions_qty + 1;
            }
        });
}

function CheckStock() {
    $("#carameleria_stock").find('input:text')
        .each(function () {
            concessions_stock = concessions_stock + 1;
        });
}

$('.buy-cant').on('keydown', function (e) {
    e.preventDefault();
});

function Formatear(valor) {
    console.log(valor);
    valor = valor.toFixed(2).replace(".", ",");
    var indice;
    if (valor.split(",")[0].length == 4) {
        indice = 1;
    } else if (valor.split(",")[0].length == 5) {
        indice = 2;
    } else {
        return valor;
    }
    return valor.splice(indice, 0, ".");
}

function Formatear_javascript(valor) {
    var valor = valor.replace(".", "");
    valor = valor.replace(",", ".");
    return parseFloat(valor);
}

String.prototype.splice = function (idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

//function calcularPorcentaje(monto, porcentaje) {
//    return ((monto * porcentaje) / 100);
//}

/* cambio por error en el calculo del iva  */
function calcularPorcentaje(monto) {
    console.log("monto: " + monto);
    return ((monto) / ((16 / 100) + 1));
}