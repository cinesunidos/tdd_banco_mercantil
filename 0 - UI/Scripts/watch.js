function Watch(minutes, seconds, url) {
    this.minutes = minutes;
    this.seconds = seconds;
    this.url = url;
    this.interval = setInterval("watch.next()", 1000);
}
Watch.prototype.stop = function () {
     clearInterval(this.interval);
};
Watch.prototype.toString = function () {
    var time = "";
    if (this.seconds < 10)
        time = '0' + this.minutes + ':0' + this.seconds;
    else
        time = '0' + this.minutes + ':' + this.seconds;
    return time;
};
Watch.prototype.completed = function () {
    if (this.minutes === 0 && this.seconds === 0) {
        clearInterval(this.interval);
        $.msgbox('Disculpe, El tiempo para realizar su compra ha expirado.', {
            type: 'info'
        }, function (buttonPressed) {
            after_close();
        });
    }
};
Watch.prototype.next = function () {
    if (this.seconds == 0) {
        this.minutes--;
        this.seconds = 59;
    }
    else
        this.seconds--;

    document.getElementById('contratiempo').innerHTML = this.toString();
    document.getElementById('minutos').value = this.minutes;
    document.getElementById('segundos').value = this.seconds;
    this.completed();
};
Watch.prototype.afterClose = function () {
    document.location.href = this.url;
};