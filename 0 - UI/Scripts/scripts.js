jQuery(function($) {
	var height = $(window).innerHeight();
	var width = $(window).innerWidth();

	if (document.images) {
	    img1 = new Image();
	    img2 = new Image();
	    img3 = new Image();
	    img4 = new Image();
	    img5 = new Image();
	    img6 = new Image();
	    img7 = new Image();
	    img8 = new Image();
	    img9 = new Image();
	    img10 = new Image();
	    img11 = new Image();
	    img12 = new Image();

	    img1.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/cotufas-combo-para-2.jpg";
	    img2.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/perrocaliente-combo.jpg";
	    img3.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/cotufas-combo-med.jpg";
	    img4.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/combo-hamburguesa.jpg";
	    img5.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/nuggets-combo.jpg";
	    img6.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/papas.jpg";
	    img7.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/tequenos-combo.jpg";
	    img8.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/cotufas-med.jpg";
	    img9.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/ComboCriollitasdequesoconref.jpg";
	    img10.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/Criollitasdequeso.jpg";
	    img11.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/ComboCriollitasdulcesconref.jpg";
	    img12.src = "https://cinesunidosweb.blob.core.windows.net/carameleria/Criollitasdulces.jpg";
	}
	$(".portfolio_items").isotope({
	    itemSelector: '.single_items',
	    layoutMode: 'fitRows',
	});

	$('.portfolio_filter li').click(function () {
	    $(".portfolio_filter li").removeClass("active");
	    $(this).addClass("active");

	    var selector = $(this).attr('data-filter');
	    $(".portfolio_items").isotope({
	        filter: selector,
	        animationOptions: {
	            duration: 750,
	            easing: 'linear',
	            queue: false,
	        }
	    });
	    return false;
	})

	$('.carousel-inner .item:first').addClass('active');
	$('.carousel-indicators li:first').addClass('active')

	$("#datepicker").datepicker({
	    changeMonth: true,
	    changeYear: true,
	    yearRange: "-100:+100",
	    dateFormat: 'dd/mm/yy'
	});


	//$("#myAccountMenu").hover(function () {
	//    $(this).find('#myAccountMenu-Login').slideToggle('slow');
	//});
	
	$(".nav-tabs a").click(function () {
	    $(this).tab('show');
	});

	if(width<=1200){
		$('.pago .head .head').removeClass('head')
	}
	
	var nombre_cine = $('.mnu_salasciudadact a').text();
	$('#title_cine').text(nombre_cine);

	$('.mnu_salasciudad li a').click(function () {
	    var nombre_cine = $(this).text()
	    $('#title_cine').text(nombre_cine)

	})

});
$(document).ready(function () {


    $("#time-picker").timepicki();

	var height = $(window).innerHeight();
	var width = $(window).innerWidth();
	var height_foo = $('footer').innerHeight();
	var fondo_rojo = $('.fondo_rojo').innerHeight() + 138;

	if(width>1200){
		$('.sesion').css({
		    'height': height - height_foo - fondo_rojo + 'px',
            'min-height': '500px'
		});
	}
	if(width<372){
		$('.radio_2 img').after("<br />");
	}
	$('a#ciudad_id').click(function () {
	    $('.sbOptions').slideToggle('fast')
	})

	if ($(window).scrollTop() == 0) {
	    //console.log($(window).scrollTop())
	    $('.subir').fadeOut(200);
	}
	else {
	    //console.log($(window).scrollTop())
	    $('.subir').fadeIn(200);
	}
	$(window).scroll(function () {
	    if ($(window).scrollTop() == 0) {
	        //console.log($(window).scrollTop())
	        $('.subir').fadeOut(200);
	    }
	    else {
	        //console.log($(window).scrollTop())
	        $('.subir').fadeIn(200);
	    }
	})
	
	$(window).resize(function(){

	var height = $(window).innerHeight();
	var width = $(window).innerWidth();
	var height = $(window).innerHeight();
	var width = $(window).innerWidth();
	var height_foo = $('footer').innerHeight();
	var fondo_rojo = $('.fondo_rojo').innerHeight() + 138;


		$('.promociones').css({
			'height' : height - height_foo - fondo_rojo + 'px'
		});

		if(width>1200){
			$('.sesion').css({
			'height' : height - height_foo - fondo_rojo + 'px'
			});
		}
		else {
		    $('.pago .head .head').removeClass('head')
		}
	});


})
