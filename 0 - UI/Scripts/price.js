function chevronState () {
    
    //Barquisimeto ---------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city1").on("hide.bs.collapse", function(){
        $(".linkc1city1").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Trinitarias</h4>');
    });
    
    $("#c1city1").on("show.bs.collapse", function(){
        $(".linkc1city1").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Trinitarias</h4>');
    });
    
    $("#c2city1").on("hide.bs.collapse", function(){
        $(".linkc2city1").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Barquisimeto</h4>');
    });
    
    $("#c2city1").on("show.bs.collapse", function(){
        $(".linkc2city1").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Barquisimeto</h4>');
    });
    
    
    //Caracas ---------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city2").on("hide.bs.collapse", function(){
        $(".linkc1city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Los Naranjos</h4>');
    });
    
    $("#c1city2").on("show.bs.collapse", function(){
        $(".linkc1city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Los Naranjos</h4>');
    });
    
    $("#c2city2").on("hide.bs.collapse", function(){
        $(".linkc2city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Metrocenter</h4>');
    });
    
    $("#c2city2").on("show.bs.collapse", function(){
        $(".linkc2city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Metrocenter</h4>');
    });
    
    $("#c3city2").on("hide.bs.collapse", function(){
        $(".linkc3city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Caracas</h4>');
    });
    
    $("#c3city2").on("show.bs.collapse", function(){
        $(".linkc3city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Caracas</h4>');
    });
    
    $("#c4city2").on("hide.bs.collapse", function(){
        $(".linkc4city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">El Marques</h4>');
    });
    
    $("#c4city2").on("show.bs.collapse", function(){
        $(".linkc4city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">El Marques</h4>');
    });
    
    $("#c5city2").on("hide.bs.collapse", function(){
        $(".linkc5city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Galerias Paraiso</h4>');
    });
    
    $("#c5city2").on("show.bs.collapse", function(){
        $(".linkc5city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Galerias Paraiso</h4>');
    });
    
    $("#c6city2").on("hide.bs.collapse", function(){
        $(".linkc6city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Galerias Avila</h4>');
    });
    
    $("#c6city2").on("show.bs.collapse", function(){
        $(".linkc6city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Galerias Avila</h4>');
    });
    
    $("#c7city2").on("hide.bs.collapse", function(){
        $(".linkc7city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Millenium</h4>');
    });
    
    $("#c7city2").on("show.bs.collapse", function(){
        $(".linkc7city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Millenium</h4>');
    });
    
    $("#c8city2").on("hide.bs.collapse", function(){
        $(".linkc8city2").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Lider</h4>');
    });
    
    $("#c8city2").on("show.bs.collapse", function(){
        $(".linkc8city2").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Lider</h4>');
    });
    
    
    //Guatire ---------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city3").on("hide.bs.collapse", function(){
        $(".linkc1city3").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Guatire Plaza</h4>');
    });
    
    $("#c1city3").on("show.bs.collapse", function(){
        $(".linkc1city3").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Guatire Plaza</h4>');
    });
    
    
    //Maracaibo ---------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city4").on("hide.bs.collapse", function(){
        $(".linkc1city4").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Centro Sur</h4>');
    });
    
    $("#c1city4").on("show.bs.collapse", function(){
        $(".linkc1city4").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Centro Sur</h4>');
    });
    
    $("#c2city4").on("hide.bs.collapse", function(){
        $(".linkc2city4").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Maracaibo</h4>');
    });
    
    $("#c2city4").on("show.bs.collapse", function(){
        $(".linkc2city4").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Maracaibo</h4>');
    });
    
    
    //Maracay -----------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city5").on("hide.bs.collapse", function(){
        $(".linkc1city5").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Las Americas</h4>');
    });
    
    $("#c1city5").on("show.bs.collapse", function(){
        $(".linkc1city5").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Las Americas</h4>');
    });
    
    $("#c2city5").on("hide.bs.collapse", function(){
        $(".linkc2city5").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Hiperjumbo</h4>');
    });
    
    $("#c2city5").on("show.bs.collapse", function(){
        $(".linkc2city5").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Hiperjumbo</h4>');
    });
    
    
    //Margarita ---------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city6").on("hide.bs.collapse", function(){
        $(".linkc1city6").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Margarita</h4>');
    });
    
    $("#c1city6").on("show.bs.collapse", function(){
        $(".linkc1city6").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Margarita</h4>');
    });
    
    
    //Maturin ------------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city7").on("hide.bs.collapse", function(){
        $(".linkc1city7").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Petroriente</h4>');
    });
    
    $("#c1city7").on("show.bs.collapse", function(){
        $(".linkc1city7").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Petroriente</h4>');
    });
    
    
    //Puerto La Cruz ----------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city8").on("hide.bs.collapse", function(){
        $(".linkc1city8").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Regina</h4>');
    });
    
    $("#c1city8").on("show.bs.collapse", function(){
        $(".linkc1city8").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Regina</h4>');
    });
    
    
    //Puerto Ordaz ------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city9").on("hide.bs.collapse", function(){
        $(".linkc1city9").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Orinokia</h4>');
    });
    
    $("#c1city9").on("show.bs.collapse", function(){
        $(".linkc1city9").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Orinokia</h4>');
    });
    
    
    //San Cristobal ------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city10").on("hide.bs.collapse", function(){
        $(".linkc1city10").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Sambil San Cristobal</h4>');
    });
    
    $("#c1city10").on("show.bs.collapse", function(){
        $(".linkc1city10").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Sambil San Cristobal</h4>');
    });
    
    
    //Valencia --------------------------------------------------------------------------------------------------------------------------------------
    
    $("#c1city11").on("hide.bs.collapse", function(){
        $(".linkc1city11").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Metropolis</h4>');
    });
    
    $("#c1city11").on("show.bs.collapse", function(){
        $(".linkc1city11").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Metropolis</h4>');
    });
    
    $("#c2city11").on("hide.bs.collapse", function(){
        $(".linkc2city11").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">La Granja</h4>');
    });
    
    $("#c2city11").on("show.bs.collapse", function(){
        $(".linkc2city11").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">La Granja</h4>');
    });
    
    $("#c3city11").on("hide.bs.collapse", function(){
        $(".linkc3city11").html('<i class="fa fa-caret-right selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Valencia</h4>');
    });
    
    $("#c3city11").on("show.bs.collapse", function(){
        $(".linkc3city11").html('<i class="fa fa-caret-down selector" aria-hidden="true"></i><h4 class="panel-title">Sambil Valencia</h4>');
    });
    
}