﻿function openQr(popTitle, popSms) {
    //Some Code here
    $('#overlay').css('display', 'block');
    $('#overlay').fadeIn('fast', function () {
        $('#popup').css('display', 'block');
        $('#popup').animate({ 'left': '12.5%' }, 500);

        $('#popup #popTitle').append(popTitle);
        $('#popup #popSms').append(popSms);
    });
}

function closeQr(id) {
    $('#' + id).css('position', 'fixed');
    $('#' + id).animate({ 'left': '110%' }, 1000, function () {
        $('#' + id).css('position', 'fixed');
        $('#' + id).css('left', '100%');
        $('#overlay').fadeOut('fast');
        var session = sessionStorage.setItem('checkMessage', 'viewed');
    });
}
