
function ticketsViewModel(tickets, maxSeatsPerTransaction, seatsAvailable) {
    this.tickets = tickets;
    this.maxSeatsPerTransaction = maxSeatsPerTransaction;
    this.seatsAvailable = seatsAvailable;
    this.TotalBoletos = 0;
    this.factoriva = 1.16;
    this.qttyVou = 0;
    this.Updating = false;
}

ticketsViewModel.prototype.total = function () {
    var quantity = 0;
    for (var i = 0, length = this.tickets.length; i < length; i++) {
        quantity = quantity + this.tickets[i].Quantity;
    }
    return quantity;
};

ticketsViewModel.prototype.addTicket = function (descripcion, code) {
    for (var i = 0, length = this.tickets.length; i < length; i++) {
        if (this.tickets[i].Code == code) {
            switch (descripcion) {
                case '2x1':
                    this.TotalBoletos += 2;
                    var total = this.total() + 1;
                    if (this.TotalBoletos <= this.maxSeatsPerTransaction) {
                        if (total <= this.maxSeatsPerTransaction && total <= seatsAvailable) {
                            this.tickets[i].Quantity = this.tickets[i].Quantity + 1;
                            this.set(i, code);
                        }
                    } else {
                        this.TotalBoletos -= 2;
                    }
                    break;
                case '3x2':
                    this.TotalBoletos += 3;
                    if (this.TotalBoletos <= this.maxSeatsPerTransaction) {
                        var total = this.total() + 1;
                        if (total <= this.maxSeatsPerTransaction && total <= seatsAvailable) {
                            this.tickets[i].Quantity = this.tickets[i].Quantity + 1;
                            this.set(i, code);
                        }
                    } else {
                        this.TotalBoletos -= 3;
                    }
                    break;
                case 'eja':
                    this.TotalBoletos += 2;
                    var total = this.total() + 1;
                    if (this.TotalBoletos <= this.maxSeatsPerTransaction) {
                        if (total <= this.maxSeatsPerTransaction && total <= seatsAvailable) {
                            this.tickets[i].Quantity = this.tickets[i].Quantity + 1;
                            this.set(i, code);
                        }
                    } else {
                        this.TotalBoletos -= 2;
                    }
                    break;
                case 'a 3':
                    this.TotalBoletos += 3;
                    var total = this.total() + 1;
                    if (this.TotalBoletos <= this.maxSeatsPerTransaction) {
                        if (total <= this.maxSeatsPerTransaction && total <= seatsAvailable) {
                            this.tickets[i].Quantity = this.tickets[i].Quantity + 1;
                            this.set(i, code);
                        }
                    } else {
                        this.TotalBoletos -= 3;
                    }
                    break;
                case 'a 4':
                    this.TotalBoletos += 4;
                    var total = this.total() + 1;
                    if (this.TotalBoletos <= this.maxSeatsPerTransaction) {
                        if (total <= this.maxSeatsPerTransaction && total <= seatsAvailable) {
                            this.tickets[i].Quantity = this.tickets[i].Quantity + 1;
                            this.set(i, code);
                        }
                    } else {
                        this.TotalBoletos -= 4;
                    }
                    break;
                case 'a 5':
                    this.TotalBoletos += 5;
                    var total = this.total() + 1;
                    if (this.TotalBoletos <= this.maxSeatsPerTransaction) {
                        if (total <= this.maxSeatsPerTransaction && total <= seatsAvailable) {
                            this.tickets[i].Quantity = this.tickets[i].Quantity + 1;
                            this.set(i, code);
                        }
                    } else {
                        this.TotalBoletos -= 5;
                    }
                    break;
                default:
                    this.TotalBoletos += 1;
                    if (this.TotalBoletos <= this.maxSeatsPerTransaction) {
                        var total = this.total() + 1;
                        if (total <= this.maxSeatsPerTransaction && total <= seatsAvailable) {
                            this.tickets[i].Quantity = this.tickets[i].Quantity + 1;
                            this.set(i, code);
                        }
                    } else {
                        this.TotalBoletos -= 1;
                    }
                    break;
            }
        }
    }
};

ticketsViewModel.prototype.subTicket = function (descripcion, code) {
    for (var i = 0, length = this.tickets.length; i < length; i++) {
        if (this.tickets[i].Code == code) {
            var quantity = this.tickets[i].Quantity - 1;
            if (quantity > -1) {
                this.tickets[i].Quantity = quantity;
                switch (descripcion) {
                    case '2x1':
                        this.TotalBoletos -= 2;
                        break;
                    case '3x2':
                        this.TotalBoletos -= 3;
                        break;
                    case 'eja':
                        this.TotalBoletos -= 2;
                        break;
                    case 'a 3':
                        this.TotalBoletos -= 3;
                        break;
                    case 'a 4':
                        this.TotalBoletos -= 4;
                        break;
                    case 'a 5':
                        this.TotalBoletos -= 5;
                        break;
                    default:
                        this.TotalBoletos -= 1;
                        break;
                }
                this.set(i, code);
            }
        }
    }
    //console.log(this.TotalBoletos);
};

ticketsViewModel.prototype.set = function (index, code) {
    document.getElementById(code).innerHTML = this.tickets[index].Quantity;
    document.getElementById("Purchase_Tickets_ " + index + " __Quantity").setAttribute("value", this.tickets[index].Quantity);
    this.sum();
};

ticketsViewModel.prototype.sum = function () {

    var basePrice = 0.0;
    var bookingFee = 0.0;
    var tax = 0.0;
    var serviceFee = 0.0;
    var promotionFee = 0.0;
    var promotionTax = 0.0;
    // GP: A�adido 2016-11-02
    var ticketTax = 0.0;
    var bookingTax = 0.0;
    var serviceTax = 0.0;

    for (var i = 0, length = this.tickets.length; i < length; i++) {
        var quantity = this.tickets[i].Quantity;
        basePrice = basePrice + this.tickets[i].BasePrice * quantity;
        tax = tax + this.tickets[i].Tax * quantity;
        bookingFee = bookingFee + this.tickets[i].BookingFee * quantity;
        serviceFee = serviceFee + this.tickets[i].ServiceFee * quantity;
        promotionFee = promotionFee + this.tickets[i].PromotionFee * quantity;
        promotionTax = promotionTax + this.tickets[i].PromotionTax * quantity;
        // GP: A�adido 2016-11-02
        ticketTax = ticketTax + this.tickets[i].TicketTax * quantity;
        bookingTax = bookingTax + this.tickets[i].BookingTax * quantity;
        serviceTax = serviceTax + this.tickets[i].ServiceTax * quantity;
    }

    var total = basePrice + tax + bookingFee + serviceFee + promotionFee;
    document.getElementById("BasePrice").innerHTML = Formatear(basePrice);// basePrice.toFixed(2).replace(".", ",");
    // GP: A�adido 2016-11-02 IVA boletos
    document.getElementById("TicketTax").innerHTML = Formatear(ticketTax); //ticketTax.toFixed(2).replace(".", ",");
    document.getElementById("BookingFee").innerHTML = Formatear(bookingFee);// bookingFee.toFixed(2).replace(".", ",");
    //GP: A�adido 2016-11-02 IVA Cargos web
    document.getElementById("BookingTax").innerHTML = Formatear(bookingTax);//bookingTax.toFixed(2).replace(".", ",");
    document.getElementById("Tax").innerHTML = Formatear(tax);//tax.toFixed(2).replace(".", ",");
    document.getElementById("ServiceFee").innerHTML = Formatear(serviceFee);//serviceFee.toFixed(2).replace(".", ",");
    //GP: A�adido 2016-11-02 IVA Servicios
    document.getElementById("ServiceTax").innerHTML = Formatear(serviceTax);//serviceTax.toFixed(2).replace(".", ",");

    //RM - 2019/07/16
    document.getElementById("Total").innerHTML = Formatear(total);//total.toFixed(2).replace(".", ",");
    document.getElementById("PromotionFee").innerHTML = Formatear(promotionFee);// basePrice.toFixed(2).replace(".", ",");
    document.getElementById("PromotionTax").innerHTML = Formatear(promotionTax);// basePrice.toFixed(2).replace(".", ",");

    if (promotionFee > 0) {
        $('.Promotion').show();
    } else {
        $('.Promotion').hide();
    }


    if (total.toFixed(2).replace(".", ",") == '0,00') {
        $('#Send').attr('disabled', true);
    } else {
        $('#Send').attr('disabled', false);
    }
};

ticketsViewModel.prototype.click = function () {
    var quantity = this.total();
    if (quantity > 0 && quantity <= this.maxSeatsPerTransaction) {
        document.getElementById("btnSend").click();
    }
    else {
        alert('Disculpe, Ud. Debe seleccionar el numero de entradas para procesar su compra.', 'error');
    }
};

String.prototype.splice = function (idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function Formatear(valor) {
    valor = valor.toFixed(2).replace(".", ",");
    var indice;
    if (valor.split(",")[0].length == 4) {
        indice = 1;
    } else if (valor.split(",")[0].length == 5) {
        indice = 2;
    } else if (valor.split(",")[0].length == 6) {
        indice = 3;
    } else if (valor.split(",")[0].length == 7) {
        indice = 4;
    } else {
        return valor;
    }
    return valor.splice(indice, 0, ".");
}

ticketsViewModel.prototype.valvou = function () {
    $('#fountainG').removeClass('hidden');
    var d = $('#VocucherCode').val();
    var xpr = /^[0-9]{16}$/;
    if (!d.match(xpr)) {
        alert("C\&oacute;digo Electr\&oacute;nico no v\&aacute;lido","error");
        $('#fountainG').addClass('hidden');
        $('#VocucherCode').val("");
    } else {
        if (this.TotalBoletos == maxSeatsPerTransaction) {
            alert("No puede agregar mas C\&oacute;digos Electr\&oacute;nicos a esta orden","error");
            $('#fountainG').addClass('hidden');
            return;
        }

        var bolVal = $('.vld');
        if (bolVal.length > 0) {
            for (var i = 0; i < bolVal.length; i++) {
                var dato = $(bolVal[i]).text();
                if (dato.indexOf(d) > 0) {
                    alert("C\&oacute;digo Electr\&oacute;nico ya validado, intente con otro","error");
                    $('#fountainG').addClass('hidden');
                    $('#VocucherCode').val("");
                    return;
                }
            }
        }

        var bolValn = $('.vldn');
        if (bolValn.length > 0) {
            for (var i = 0; i < bolValn.length; i++) {
                var dato = $(bolValn[i]).text();
                if (dato.indexOf(d) > 0) {
                    alert("C\&oacute;digo Electr\&oacute;nico ya validado, intente con otro","error");
                    $('#fountainG').addClass('hidden');
                    $('#VocucherCode').val("");
                    return;
                }
            }
        }

        $.ajax({
            type: 'GET',
            url: '/purchase/VlVoVi',
            data: { VoNo: d }, //, __RequestVerificationToken: getTkn()
            success: function (data) {
                console.log(data);

                if (data["Redimido"] == false && data["ValidoParafuncion"] == true) {

                    if (data["IsMultiVoucher"] == true) {
                        var botonMenos = '<a onclick="AddOrUpdateVoucher(\'' + data["CodigoBoletoParaCanje"] + '\', \'-\',\'' + data["VoucherCode"] + '\'); return false;" class=\'btn_menosentrada\' ></a >';
                        var botonMas = '<a onclick="AddOrUpdateVoucher(\'' + data["CodigoBoletoParaCanje"] + '\', \'+\',\'' + data["VoucherCode"] + '\'); return false;" class=\'btn_masentrada\' ></a >';
                        var input = '<input value="0" name="' + data["VoucherCode"] + '" type="text" class="campoentrada quantity" readonly="true">';
                        var btndelete = '<a onclick="viewModel.remove(this.id)" type="button" class="btn_deletevou" style="margin-left: 7px;" id="' + data["VoucherCode"] + ';' + data["CodigoBoletoParaCanje"] + '"></a>';
                        var inicioli = '<li class="comprainfo1" id ="' + data["VoucherCode"] + '">';
                        var finli = '</li>';
                        var e = $('.iprice.' + data["CodigoBoletoParaCanje"]).val();
                        var monto = Formatear(parseFloat(e));
                        var colDesc = '<div class="col-lg-4 col-sm-4 col-xs-4"><span class="comprainfotipo vld" data-qtty ="' + data["EntradasRestantes"] + '" data-price="' + data["ValorRedencion"] + '">' + data["TextoMostrar"] + ': ' + data["VoucherCode"] + '</div>';
                        var colMont = '<div class="col-lg-4 col-sm-4 col-xs-4"><span class="comprainfomonto"> Bs. ' + monto + '</span></div>';
                        var colCant = '<div class="col-lg-4 col-sm-4 col-xs-4"><span class="comprainfocant">' + botonMenos + input + botonMas + btndelete + '</span></div>';
                        //$('.relleno').append('<label style="font-size:90%" data-qtty ="' + data["EntradasRestantes"] + '" data-price="' + data["ValorRedencion"] + '" class="label label-success label-lg vld">' + data["TextoMostrar"] + ': ' + data["VoucherCode"] + '</label> ' + '<span class="comprainfocant ' + data["VoucherCode"] + '" style="position: absolute;" > ' + botonMenos + input + botonMas + ' <button style="margin-top: -12px; margin-left: 3px;" onclick="viewModel.remove(this.id)" type="button" class="btn btn-danger btn-xs fa fa-2x fa-remove" id="' + data["VoucherCode"] + ';' + data["CodigoBoletoParaCanje"] + '"></button></span>' + ' <hr class="' + data["VoucherCode"] + '" style="margin-top: 4px; margin-bottom: 4px;"/>')
                        $('.relleno').append(inicioli + colDesc + colMont + colCant + finli);

                    } else {
                        //$('.relleno').append('<label style="font-size:90%" data-qtty ="' + data["EntradasRestantes"] + '" data-price="' + data["ValorRedencion"] + '" class="label label-success label-lg vld">' + data["TextoMostrar"] + ': ' + data["VoucherCode"] + '</label> <button onclick="viewModel.remove(this.id)" type="button" class="btn btn-danger btn-xs fa fa-2x fa-remove" id="' + data["VoucherCode"] + ';' + data["CodigoBoletoParaCanje"] + '"></button> <hr class="' + data["VoucherCode"] + '" style="margin-top: 4px; margin-bottom: 4px;"/>')
                        //var botonMenos = '<a onclick="AddOrUpdateVoucher(\'' + data["CodigoBoletoParaCanje"] + '\', \'-\',\'' + data["VoucherCode"] + '\'); return false;" class=\'btn_menosentrada\' ></a >';
                        //var botonMas = '<a onclick="AddOrUpdateVoucher(\'' + data["CodigoBoletoParaCanje"] + '\', \'+\',\'' + data["VoucherCode"] + '\'); return false;" class=\'btn_masentrada\' ></a >';
                        //var input = '<input value="0" name="' + data["VoucherCode"] + '" type="text" class="campoentrada quantity" readonly="true" value="0">';
                        var btndelete = '<a onclick="viewModel.remove(this.id)" type="button" class="btn_deletevou" id="' + data["VoucherCode"] + ';' + data["CodigoBoletoParaCanje"] + '"></a>';
                        var inicioli = '<li class="comprainfo1" id ="' + data["VoucherCode"] + '">';
                        var finli = '</li>';
                        var e = $('.iprice.' + data["CodigoBoletoParaCanje"]).val();
                        var monto = Formatear(parseFloat(e));
                        var colDesc = '<div class="col-lg-4 col-sm-4 col-xs-4"><span class="comprainfotipo vld" data-qtty ="' + data["EntradasRestantes"] + '" data-price="' + data["ValorRedencion"] + '">' + data["TextoMostrar"] + ': ' + data["VoucherCode"] + '</div>';
                        var colMont = '<div class="col-lg-4 col-sm-4 col-xs-4"><span class="comprainfomonto"> Bs. ' + monto + '</span></div>';
                        var colCant = '<div class="col-lg-4 col-sm-4 col-xs-4"><span class="comprainfocant"> <input value="1" type="text" class="campoentrada" readonly="true">' + btndelete + '</span></div>';
                        //$('.relleno').append('<label style="font-size:90%" data-qtty ="' + data["EntradasRestantes"] + '" data-price="' + data["ValorRedencion"] + '" class="label label-success label-lg vld">' + data["TextoMostrar"] + ': ' + data["VoucherCode"] + '</label> ' + '<span class="comprainfocant ' + data["VoucherCode"] + '" style="position: absolute;" > ' + botonMenos + input + botonMas + ' <button style="margin-top: -12px; margin-left: 3px;" onclick="viewModel.remove(this.id)" type="button" class="btn btn-danger btn-xs fa fa-2x fa-remove" id="' + data["VoucherCode"] + ';' + data["CodigoBoletoParaCanje"] + '"></button></span>' + ' <hr class="' + data["VoucherCode"] + '" style="margin-top: 4px; margin-bottom: 4px;"/>')
                        $('.relleno').append(inicioli + colDesc + colMont + colCant + finli);
                        viewModel.addTicket("aaa", data["CodigoBoletoParaCanje"]);
                    }

                    $('p.' + data["CodigoBoletoParaCanje"]).removeClass('hidden');
                }

                if (data["Redimido"] == true) {
                    alert('Este C\&oacute;digo Electr\&oacute;nico ya ha sido utilizado, Intente con otro.', "error")
                    $('.relleno').append('<label style="font-size:0%;" class="label label-success vldn">' + data["TextoMostrar"] + ': ' + data["VoucherCode"] + '</label>');
                } else if (data["ValidoParafuncion"] == false) {
                    alert('C\&oacute;digo Electr\&oacute;nico no valido para esta pel\&iacute;cula',"error")
                    $('.relleno').append('<label style="font-size:0%;" class="label label-success vldn">' + data["TextoMostrar"] + ': ' + data["VoucherCode"] + '</label>');
                }
                $('#fountainG').addClass('hidden');

            },
            error: function (eee) {
                console.log("Error");
                location.reload(true);
            },
            fail: function (o) {
                console.log("Fail");
                location.reload(true);
            }
        });

        $('#VocucherCode').val("");
    }
}

ticketsViewModel.prototype.remove = function (id_code) {

    RemoveVoucher(id_code);
}

function AddOrUpdateVoucher(codBoleto, acc, VoucherNum) {
    if (acc == "+" && viewModel.TotalBoletos == viewModel.maxSeatsPerTransaction) {
        return false;
    }
    if (viewModel.Updating == true) {
        return false;
    } else {
        viewModel.Updating = true;
    }
    var cantidad = parseInt($('input[name=' + VoucherNum + ']').val());
    $.ajax({
        url: '/purchase/UpdateMultiVoucherQtty/',
        data: {
            acc: acc,
            voucherCode: VoucherNum
        },
        success: function (info) {
            if (info.success == false && info.mensaje == "Redirect") {
                window.location = "/inicio";
            }
            if (info.success == true) {
                //add
                if (acc == "+") {
                    cantidad += 1;
                    $('input[name=' + VoucherNum + ']').val(cantidad);
                    viewModel.addTicket("aaa", codBoleto);
                }

                //minus
                if (acc == "-") {
                    cantidad -= 1;
                    $('input[name=' + VoucherNum + ']').val(cantidad);
                    viewModel.subTicket("aaa", codBoleto);
                }
            } else {
                alert(info.mensaje, "error");
            }
        },
        error: function (err) {
        }
    });
    viewModel.Updating = false;
}

function RemoveVoucher(id_code) {
    var datos = id_code.split(';');
    var id = datos[0];
    var code = datos[1];
    $.ajax({
        url: '/purchase/RemoveVoucher/',
        data: {
            voucherNum: id
        },
        success: function (valor) {
            if (valor.success == true) {
                var checks = $('.vld');
                for (var i = 0; i < checks.length; i++) {
                    var d1 = $(checks[i]).text();
                    if (d1.indexOf(id) > 0) {
                        var cantidad = parseInt($('input[name=' + id + ']').val());
                        if (isNaN(cantidad)) {
                            viewModel.subTicket("aaa", code);
                        } else {
                            console.log("tiene " + cantidad);
                            for (var i2 = 0; i2 < cantidad; i2++) {
                                viewModel.subTicket("aaa", code);
                            }
                        }

                        var label = $('p.' + code);
                        var res = $(label).children('span')[1];
                        var res2 = $(res).text();
                        if (parseInt(res2) == 0) {
                            $(label).addClass('hidden');
                        }

                        var li = $('li#' + id);
                        $(li).remove();

                        //$(checks[i]).remove();
                        //document.getElementById(id_code).remove();
                        //var dae = $('input.' + code).val();
                        //$('input.' + code).val(dae);
                        //$('.' + id).remove();
                        return;
                    }
                }
            } else {
            }
        },
        error: function (err) {
        }
    })
}