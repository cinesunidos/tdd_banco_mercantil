function openDialog(popTitle, popSms) {
    $('#overlay').css('display', 'block');
    $('#overlay').fadeIn('fast', function () {
        $('#popup').css('display', 'block');
        $('#popup').css({ 'left': '12.5%' }, 500);
    });
}

function closeDialog(id) {
    $('#' + id).css('position', 'fixed');
    $('#' + id).animate({ 'left': '110%' }, 1000, function () {
        $('#' + id).css('position', 'fixed');
        $('#' + id).css('left', '100%');
        $('#overlay').fadeOut('fast');
        var session = sessionStorage.setItem('checkMessage', 'viewed');
    });
}

window.onload = function () {
    var session = sessionStorage.getItem('checkMessage');
    if (session == null) {
        openDialog();
    }

    /*        
    switch (city) {
        case 'Barquisimeto':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'Caracas':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'Guatire':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;
                
        case 'Maracaibo':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'Maracay':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'Margarita':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'Maturin':
            if (session == null)
                openDialog('popTitle', 'popSm');
            break;

        case 'Puerto La Cruz':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'Puerto Ordaz':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'San Cristobal':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;

        case 'Valencia':
            if (session == null)
                openDialog('popTitle', 'popSms');
            break;
        
    }
    */
};

