﻿// Elementos ocultos
$(".btn-container").hide();
$(".panel-left").hide();
$(".panel-center").hide();
$(".panel-right").hide();

$("#reflex-left").hide();
$("#reflex-right").hide();
$("#light-left").hide();
$("#light-right").hide();

$("#trn").hide();
$("#sbq").hide();
$("#lna").hide();
$("#mce").hide();
$("#sam").hide();
$("#uni").hide();
$("#gpa").hide();
$("#gav").hide();
$("#mln").hide();
$("#ldr").hide();
$("#gua").hide();
$("#csu").hide();
$("#smb").hide();
$("#ame").hide();
$("#hip").hide();
$("#sma").hide();
$("#ccp").hide();
$("#rgn").hide();
$("#ork").hide();
$("#ssc").hide();
$("#mtr").hide();
$("#gra").hide();
$("#sva").hide();

$("#web").hide();
$("#taq").hide();
$("#tep").hide();
$("#conc").hide();

var logo = document.getElementById("logo"),
    refLef = document.getElementById("reflex-left"),
    refRig = document.getElementById("reflex-right"),
    ligLef = document.getElementById("light-left"),
    ligRig = document.getElementById("light-right");

TweenLite.from(logo, 5, {
    autoAlpha: 0,
    ease: Power1.easeInOut,
    delay: 2
});

var aniv = document.getElementById("aniv");
TweenLite.from(aniv, 6, {
    x: -300,
    autoAlpha: 0,
    ease: Power2.easeInOut,
    delay: 4,
    onComplete: displayBtn
});

function displayBtn() {
    $("#reflex-left-off").hide();
    $("#reflex-right-off").hide();
    $("#reflex-left").show();
    $("#reflex-right").show();
    $("#light-left").show();
    $("#light-right").show();
    $(".btn-container").show();
}

function tutorial() {
    $(".btn-container").hide();

    displayForms();

    TweenLite.to(logo, 2.2, {
        x: -15,
        y: -160,
        scaleX: .6,
        scaleY: .6,
        ease: Linear.easeOut
    });

    TweenLite.to(aniv, 1, {
        autoAlpha: 0,
        ease: Linear.easeOut
    });

    // Rotation Lights Animation
    TweenLite.to(refLef, 4, {
        y: -15,
        skewX: "5deg",
        scaleY: .80
    });

    TweenLite.to(ligLef, 4, {
        rotation: -2.8,
        y: -173,
        skewX: "30deg",
        scaleY: .75
    });

    TweenLite.to(refRig, 4, {
        y: -15,
        skewX: "-5deg",
        scaleY: .80
    });

    TweenLite.to(ligRig, 4, {
        rotation: 2.8,
        y: -173,
        skewX: "-30deg",
        scaleY: .75
    });
    // END Rotation Lights Animation

}

function displayForms() {
    var pLeft = $(".panel-left");
    pLeft.show();
    TweenLite.from(pLeft, 2, {
        y: 720,
        ease: Power1.easeInOut
    });

    var pCenter = $(".panel-center");
    pCenter.show();
    TweenLite.from(pCenter, 2, {
        y: 720,
        ease: Power2.easeInOut,
        delay: 1
    });

    var pRight = $(".panel-right");
    pRight.show();
    TweenLite.from(pRight, 2, {
        y: 720,
        ease: Power3.easeInOut,
        delay: 2
    });
}

function setCinema(selectValues) {
    $.each(selectValues, function (key, value) {
        $('#cinema')
            .append($("<option></option>")
                       .attr("value", key)
                       .text(value));
    });
}

$('#city').on('change', function () {
    var cine = this.value;
    barquisimeto = { "1025": "Sambil Barquisimeto", "1006": "Trinitarias" };
    caracas = { "1001": "Los Naranjos", "1002": "Metrocenter", "1005": "Sambil Caracas", "1008": "El Marques", "1009": "Galerías Paraíso", "1020": "Galerías Ávila", "1026": "Millenium", "1027": "Lider" };
    guatire = { "1003": "Guatire Plaza" };
    maracaibo = { "1016": "Metrosol", "1017": "Sambil Maracaibo" };
    maracay = { "1010": "Las Americas", "1015": "Hiperjumbo" };
    margarita = { "1021": "Sambil Margarita" };
    maturin = { "1007": "Petroriente" };
    puertocruz = { "1019": "Regina" };
    puertoordaz = { "1023": "Orinokia" };
    sancristobal = { "1024": "Sambil San Cristobal" };
    valencia = { "1011": "Metropolis", "1013": "La Granja", "1014": "Sambil Valencia" };

    $('#cinema').html('');
    switch (cine) {
        case "1":
            setCinema(barquisimeto);
            break;
        case "2":
            setCinema(caracas);
            break;
        case "3":
            setCinema(guatire);
            break;
        case "4":
            setCinema(maracaibo);
            break;
        case "5":
            setCinema(maracay);
            break;
        case "6":
            setCinema(margarita);
            break;
        case "7":
            setCinema(maturin);
            break;
        case "8":
            setCinema(puertocruz);
            break;
        case "9":
            setCinema(puertoordaz);
            break;
        case "10":
            setCinema(sancristobal);
            break;
        case "11":
            setCinema(valencia);
            break;
    }
})

function setBuyway(selectValues) {
    $.each(selectValues, function (key, value) {
        $('#web')
            .append($("<option></option>")
            .attr("value", key)
            .text(value));
    });
}

$('#city').on('change', function () {
    var buyway = this.value;
})


function setCompra() {
    var compra = $("select#compra option:selected").val();

    switch (compra) {
        case "1":
            $("#web").show();
            $("#taq").hide();
            $("#tep").hide();
            $("#conc").hide();
            break;
        case "2":
            $("#web").hide();
            $("#taq").show();
            $("#tep").hide();
            $("#conc").hide();
        case "3":
            $("#web").hide();
            $("#taq").hide();
            $("#tep").show();
            $("#conc").hide();
        case "4":
            $("#web").hide();
            $("#taq").hide();
            $("#tep").hide();
            $("#conc").show();
    }
}

$(document).on("click", "#crear", function (e) {
    var _moneda = $("#IDMoneda").val();
    //  var _pais = $("#IDPais").val();
    var _listaprecio = $("#IDListaPrecios").val();
    var _fechaini = $("#open_date_from").val();
    var _fechafin = $("#open_date_to").val();

    var idnombre = $("#idnombre").val();
    var idemail = $("#idemail").val();
    var idtelefono = $("#idtelefono").val();
    var idcomentarios = $("#idcomentarios").val();
    var iddocumento = $("#iddocumento").val();
    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    var _habitacion = $("#book_room").val();

    var _adultos = $("#book_room_adults").val();
    var _ninos = $("#book_room_children").val();
    var infant = $("#book_room_infants").val();

    BootstrapDialog.confirm('Desea crear la reserva?', function (result) {
        if (result) {

            $("#LoadingImage").show();

            $.ajax({
                url: '@Url.Action("CrearReservacion")',
                type: 'post',
                data: {
                    __RequestVerificationToken: token,
                    fechaini: _fechaini,
                    fechafin: _fechafin,
                    habitacion: _habitacion,
                    listaprecio: _listaprecio,
                    moneda: _moneda,
                    //   pais: _pais,
                    adultos: _adultos,
                    ninos: _ninos,
                    nombre: idnombre,
                    email: idemail,
                    telefono: idtelefono,
                    comentario: idcomentarios,
                    iddocumento: iddocumento,
                    infant: infant
                },
                dataType: "json",
                success: function (data) {
                    $("#LoadingImage").hide();

                    if (data.Mensaje != '0') {
                        $("#costoreserva").hide();
                        $("#datosreserva").hide();
                        bootbox.alert("Reserva creada satisfactoriamente bajo el número de identificador: " + data.Mensaje);
                        $("#IDMoneda").val("");
                        //   $("#IDPais").val("");
                        $("#IDListaPrecios").val("");
                        $("#open_date_from").val("");
                        $("#open_date_to").val("");

                        $("#idnombre").val("");
                        $("#idemail").val("");
                        $("#idtelefono").val("");
                        $("#idcomentarios").val("");
                        $("#iddocumento").val("");
                        $("#book_room").val("");

                        $("#book_room_adults").val(0);
                        $("#book_room_children").val(0);
                    }


                },
                error: function () {
                    $("#LoadingImage").hide();
                    bootbox.alert(data.error);
                }
            });
        }
    });
});

