﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class UserSessionRS
    {
        #region Properties

        public string TheaterId { get; set; }

        public int SessionId { get; set; }

        public Guid Id { get; set; }

        public DateTime TimeOut { get; set; }

        public DateTime PaymentTimeOut { get; set; }

        public string Title { get; set; }

        public string Theater { get; set; }

        public string HO { get; set; }

        public string HallName { get; set; }

        public DateTime ShowTime { get; set; }

        public Ticket[] Tickets { get; set; }

        public Map Map { get; set; }

        public Seat[] Seats { get; set; }

        public string OrderId
        {
            get
            {
                return Id.ToString().Replace("-", "");
            }
        }

        public string TransIdTemp { get; set; }

        public ConcessionItem[] Concessions { get; set; }

        public ConcessionItem BookingFee { get; set; }
        public decimal WebChargeConcessions { get; set; }
        public decimal PercentageTax { get; set; }

        public bool ValidConcessions { get; set; }

        #region Payment
        public string BookingId { get; set; }
        public int BookingNumber { get; set; }

        public string CodeResult { get; set; }

        public string DescriptionResult { get; set; }

        public string[] Voucher { get; set; }

        public string Name { get; set; }

        public string BookingByte { get; set; }

       // public string BookingConcat { get; set; }

        #endregion Payment

        #region Error

        public bool WithError { get; set; }

        public string ErrorMessage { get; set; }

        #endregion Error

        #endregion Properties
    }
}