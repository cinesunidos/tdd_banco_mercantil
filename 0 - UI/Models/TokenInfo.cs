﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class TokenInfo
    {
        #region Properties

        public DateTime Expiration { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Hash { get; set; }

        public string IdentificationType { get; set; }

        public string Identification { get; set; }

        #endregion Properties

        public override string ToString()
        {
            return string.Format("{0} {1}", Name, LastName);
        }
    }
}