﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models.ViewModel
{
    public class QuinielaObject
    {
        #region metodos
        //public List<MejorPelicula> GetMejorPelicula()
        //{
        //    return MPelicula;
        //}

        //public List<MejorDirector> GetMejorDirector()
        //{
        //    return MDirector;
        //}

        //public List<MejorActor> GetMejorActor()
        //{
        //    return MActor;
        //}

        //public List<MejorActriz> GetMejorActriz()
        //{
        //    return MActriz;
        //}

        //public List<MejorActorSecundario> GetMejorActorSecundario()
        //{
        //    return MActorSecundario;
        //}

        //public List<MejorActrizSecundaria> GetMejorActrizSecundaria()
        //{
        //    return MActrizSecundaria;
        //}

        //public List<MejorPeliculaLenguaNoinglesa> GetPeliculaLenguaNoInglesa()
        //{
        //    return MPeliculaLenguaNoInglesa;
        //}

        //public List<GuionOriginal> GetGuionOriginal()
        //{
        //    return MGuionOriginal;
        //}

        //public List<GuionAdaptado> GetGuonAdaptado()
        //{
        //    return MGuionAdaptado;
        //}

        //public List<MejorFotografia> GetMejorFotografia()
        //{
        //    return MFotografia;
        //}

        //public List<MejorPeliculaAnimada> GetPeliculaAnimada()
        //{
        //    return MPeliculAanimada;
        //}

        //public List<MejorSonido> GetMejorSonido()
        //{
        //    return MSonido;
        //}

        //public List<MejorDocumental> GetDocumental()
        //{
        //    return MDocumental;
        //}

        //public List<MejorEfectoSonoro> GetMejorEfectosonoro()
        //{
        //    return MEfectoSonoro;
        //}

        //public List<MejorDisenoDeProduccion> GetDisenoProduccion()
        //{
        //    return MDisenoProduccion;
        //}

        //public List<MejorBandaSonora> GetBandaSonora()
        //{
        //    return MBandaSonora;
        //}

        //public List<MejoresEfectosVisuales> GetEfectosvisuales()
        //{
        //    return MEfectosVisuales;
        //}

        //public List<MejorCancion> GetCancion()
        //{
        //    return MmejorCancion;
        //}

        //public List<MejorVestuario> GetVestuario()
        //{
        //    return MMejorVestuario;
        //}

        //public List<MejorMontaje> GetMontaje()
        //{
        //    return MMejorMontaje;
        //}

        //public List<MaquillajePeluqueria> GetMaquillajePeluqueria()
        //{
        //    return MMaquillajePeliqueria;
        //}

        //public List<MejorCortoDeAccionReal> GetCortoAccionReal()
        //{
        //    return MCortoDeAccionReal;
        //}

        //public List<MejorCortoDeAnimacion> GetCortoAnimacion()
        //{
        //    return MCortoDeAnimacion;
        //}

        //public List<MejorCortoDocumental> GetCortoDcumental()
        //{
        //    return MCortoDocumental;
        //}
        #endregion


        public List<MejorPelicula> MPelicula = new List<MejorPelicula> {
            new MejorPelicula
            {
                Nombre = "Bohemian Rhapsody",
                ////Selected = false,
                ImageUrl = "BohemianRhapsody.jpg"
            },
            new MejorPelicula
            {
                Nombre = "El Vicio Del Poder",
                ////Selected = false,
                ImageUrl = "ElVicioDelPoder.jpg"
            },
            new MejorPelicula
            {
                Nombre = "Green Book: Una Amistad Sin Fronteras",
                ////Selected = false,
                ImageUrl = "GreenBook.UnaAmistadSinFronteras.jpg"
            },
            new MejorPelicula
            {
                Nombre = "Infiltrado En El Kkklan",
                ////Selected = false,
                ImageUrl = "InfiltradoEnElKkklan.jpg"
            },
            new MejorPelicula
            {
                Nombre = "La Favorita",
                ////Selected = false,
                ImageUrl = "LaFavorita.jpg"
            },
            new MejorPelicula
            {
                Nombre = "Nace Una Estrella",
                ////Selected = false,
                ImageUrl = "NaceUnaEstrella.jpg"
            },
            new MejorPelicula
            {
                Nombre = "Pantera Negra",
                ////Selected = false,
                ImageUrl = "PanteraNegra.jpg"
            },
            new MejorPelicula
            {
                Nombre = "Roma",
                ////Selected = false,
                ImageUrl = "Roma.jpg"
            },
            //new MejorPelicula
            //{
            //    Nombre = "Three Billboards Outside Ebbing, Missouri",
            //    ////Selected = false,
            //    ImageUrl = "ThreeBillboardsOutside.png"
            //}
        };

        public List<MejorDirector> MDirector = new List<MejorDirector>
        {
            new MejorDirector
            {
                Nombre = "Adam Mckay",
                ImageUrl = "AdamMckay(Vice).jpg",
                Pelicula = "El Vicio del Poder",
                ////Selected = false
            },
            new MejorDirector
            {
                Nombre = "Alfonso Cuarón",
                ImageUrl = "AlfonsoCuarón(Roma).jpg",
                Pelicula = "Roma",
                ////Selected = false
            },
            new MejorDirector
            {
                Nombre = "Pawel Pawlikowski",
                ImageUrl = "PawelPawlikowski(ColdWar).jpg",
                Pelicula = "Cold War",
                ////Selected = false
            },
            new MejorDirector
            {
                Nombre = "Spike Lee",
                ImageUrl = "SpikeLee(InfiltradoEnElKkklan).jpg",
                Pelicula = "Infiltrado En El Kkklan",
                ////Selected = false
            },new MejorDirector
            {
                Nombre = "Yorgos Lanthimos",
                ImageUrl = "YorgosLanthimos(LaFavorita).jpg",
                Pelicula = "La Favorita",
                ////Selected = false
            }
        };

        public List<MejorActor> MActor = new List<MejorActor>
        {
            new MejorActor
            {
                ImageUrl = "BradleyCooper(NaceUnaEstrella).jpg",
                Nombre = "Bradley Cooper",
                Pelicula = "Nace Una Estrella",
                //Selected = false
            },
            new MejorActor
            {
                ImageUrl = "ChristianBale(Vice).jpg",
                Nombre = "Christian Bale",
                Pelicula = "El Vicio del Poder",
                //Selected = false
            },
            new MejorActor
            {
                ImageUrl = "RamiMalek(BohemianRhapsody).jpg",
                Nombre = "Rami Malek",
                Pelicula = "Bohemian Rhapsody",
                //Selected = false
            },
            new MejorActor
            {
                ImageUrl = "ViggoMortensen(GreenBook).jpg",
                Nombre = "Viggo Mortensen",
                Pelicula = "Green Book: Una Amistad sin Fronteras",
                //Selected = false
            },
            new MejorActor
            {
                ImageUrl = "WillemDafoe(VangoghEnLaPuertaDeLaEternidad).jpg",
                Nombre = "Willem Dafoe",
                Pelicula = "Van Gogh En La Puerta De La Eternidad",
                //Selected = false
            }
        };

        public List<MejorActriz> MActriz = new List<MejorActriz>
        {
            new MejorActriz
            {
                ImageUrl = "GlennClose(TheWife).jpg",
                Nombre = "Glenn Close",
                Pelicula = "The Wife",
                //Selected = false
            },
            new MejorActriz
            {
                ImageUrl = "LadyGaga(NaceUnaEstrella).jpg",
                Nombre = "Lady Gaga",
                Pelicula = "Nace Una Estrella",
                //Selected = false
            },
            new MejorActriz
            {
                ImageUrl = "MelissaMcCarthy(CanYouEverForgiveMe).jpg",
                Nombre = "Melissa McCarthy",
                Pelicula = "¿Podrás perdonarme algún día?",
                //Selected = false
            },
            new MejorActriz
            {
                ImageUrl = "OliviaColman(LaFavorita).jpg",
                Nombre = "Olivia Colman",
                Pelicula = "La Favorita",
                //Selected = false
            },
            new MejorActriz
            {
                ImageUrl = "YalitzaAparicio(Roma).jpg",
                Nombre = "Yalitza Aparicio",
                Pelicula = "Roma",
                //Selected = false
            },
        };

        public List<MejorActorSecundario> MActorSecundario = new List<MejorActorSecundario>
        {
            new MejorActorSecundario
            {
                ImageUrl = "AdamDriver(InfiltradoEnElKkklan).jpg",
                Nombre = "Adam Driver",
                Pelicula = "Infiltrado En El Kkklan",
                //Selected = false
            },
            new MejorActorSecundario
            {
                ImageUrl = "MahershalaAli(GreenBook).jpg",
                Nombre = "Mahershala Ali",
                Pelicula = "Green Book",
                //Selected = false
            },
            new MejorActorSecundario
            {
                ImageUrl = "RichardEGrant(CanYouEverForgiveMe).jpg",
                Nombre = "Richard E. Grant",
                Pelicula = "¿Podrás perdonarme algún día?",
                //Selected = false
            },
            new MejorActorSecundario
            {
                ImageUrl = "SamElliott(NaceUnaEstrella).jpg",
                Nombre = "Sam Elliott",
                Pelicula = "Nace Una Estrella",
                //Selected = false
            },
            new MejorActorSecundario
            {
                ImageUrl = "SamRockwell(Vice).jpg",
                Nombre = "Sam Rockwell",
                Pelicula = "El vicio del poder",
                //Selected = false
            }
        };

        public List<MejorActrizSecundaria> MActrizSecundaria = new List<MejorActrizSecundaria>
        {
            new MejorActrizSecundaria
            {
                ImageUrl = "AmyAdams(Vice).jpg",
                Nombre = "Amy Adams",
                Pelicula = "El vicio del poder",
                //Selected = false
            },
            new MejorActrizSecundaria
            {
                ImageUrl = "EmmaStone(LaFavorita).jpg",
                Nombre = "Emma Stone",
                Pelicula = "La Favorita",
                //Selected = false
            },
            new MejorActrizSecundaria
            {
                ImageUrl = "MarinaDeTavira(Roma).jpg",
                Nombre = "Marina De Tavira",
                Pelicula = "Roma",
                //Selected = false
            },
            new MejorActrizSecundaria
            {
                ImageUrl = "RachelWeisz(LaFavorita).jpg",
                Nombre = "Rachel Weisz",
                Pelicula = "La Favorita",
                //Selected = false
            },
            new MejorActrizSecundaria
            {
                ImageUrl = "ReginaKing(IfBealeStreetCouldTalk).jpg",
                Nombre = "Regina King",
                Pelicula = "If Beale Street Could Talk",
                //Selected = false
            },
        };

        public List<MejorPeliculaLenguaNoinglesa> MPeliculaLenguaNoInglesa = new List<MejorPeliculaLenguaNoinglesa>
        {
            new MejorPeliculaLenguaNoinglesa
            {
                Nombre = "Capernaum",
                pais = "Líbano",
                ImageUrl = "Capernaum.jpg"
                //Selected = false
            },
            new MejorPeliculaLenguaNoinglesa
            {
                Nombre = "Cold War",
                pais = "Polonia",
                ImageUrl = "ColdWar.jpg"
                //Selected = false
            },
            new MejorPeliculaLenguaNoinglesa
            {
                Nombre = "Never Look Away",
                pais = "Alemania",
                ImageUrl = "NeverLookAway.jpg"
                //Selected = false
            },
            new MejorPeliculaLenguaNoinglesa
            {
                Nombre = "Roma",
                pais = "México",
                ImageUrl = "Roma.jpg"
                //Selected = false
            },
            new MejorPeliculaLenguaNoinglesa
            {
                Nombre = "Shoplifters",
                pais = "Japón",
                ImageUrl = "Shoplifters.jpg"
                //Selected = false
            }
        };

        public List<GuionOriginal> MGuionOriginal = new List<GuionOriginal>
        {
            new GuionOriginal
            {
                ImageUrl = "ElReverendo.jpg",
                Nombre = "El Reverendo "
                //Selected = false
            },
            new GuionOriginal
            {
                ImageUrl = "ElVicioDelPoder.jpg",
                Nombre = "El Vicio Del Poder"
                //Selected = false
            },
            new GuionOriginal
            {
                ImageUrl = "GreenBook.UnaAmistadSinFronteras.jpg",
                Nombre = "Green Book: Una Amistad Sin Fronteras",
                //Selected = false
            },
            new GuionOriginal
            {
                Nombre = "La Favorita",
                ImageUrl = "LaFavorita.jpg"
                //Selected = false
            },
            new GuionOriginal
            {
                Nombre = "Roma",
                ImageUrl = "Roma.jpg"
                //Selected = false
            }
        };

        public List<GuionAdaptado> MGuionAdaptado = new List<GuionAdaptado>
        {
            new GuionAdaptado
            {
                Nombre = "If Beale Street Could Talk",
                ImageUrl = "ElBluesdeBealeStreet.jpg"
                //Selected = false
            },
            new GuionAdaptado
            {
                Nombre = "Infiltrado En El Kkklan",
                ImageUrl = "InfiltradoEnElKkklan.jpg"
                //Selected = false
            },
            new GuionAdaptado
            {
                Nombre = "Nace Una Estrella",
                ImageUrl = "NaceUnaEstrella.jpg"
                //Selected = false
            },
            new GuionAdaptado
            {
                Nombre = "¿Podrás perdonarme algún día?",
                ImageUrl = "PuedesPerdonarme.jpg"
                //Selected = false
            },
            new GuionAdaptado
            {
                Nombre = "The Ballad Of Buster Scruggs",
                ImageUrl = "TheBalladOfBusterScruggs.jpg"
                //Selected = false
            }
        };

        public List<MejorFotografia> MFotografia = new List<MejorFotografia>
        {
            new MejorFotografia
            {
                Nombre = "Cold War",
                ImageUrl = "ColdWar.jpg"
                //Selected = false
            },
            new MejorFotografia
            {
                Nombre = "La Favorita",
                ImageUrl = "LaFavorita.jpg"
                //Selected = false
            },
            new MejorFotografia
            {
                Nombre = "Nace Una Estrella",
                ImageUrl = "NaceUnaEstrella.jpg"
                //Selected = false
            },
            new MejorFotografia
            {
                Nombre = "Never Look Away",
                ImageUrl = "NeverLookAway.jpg"
                //Selected = false
            },
            new MejorFotografia
            {
                Nombre = "Roma",
                ImageUrl = "Roma.jpg"
                //Selected = false
            },
        };

        public List<MejorPeliculaAnimada> MPeliculAanimada = new List<MejorPeliculaAnimada>
        {
            new MejorPeliculaAnimada
            {
                Nombre = "Isla De Perros",
                ImageUrl = "IslaDePerros.jpg"
                //Selected = false
            },
            new MejorPeliculaAnimada
            {
                Nombre = "Los Increíbles 2",
                ImageUrl = "LosIncreíbles2.jpg"
                //Selected = false
            },
            new MejorPeliculaAnimada
            {
                Nombre = "Mirai",
                ImageUrl = "Mirai.jpg"
                //Selected = false
            },
            new MejorPeliculaAnimada
            {
                Nombre = "Spider Man: Un Nuevo Universo",
                ImageUrl = "SpiderMan.UnNuevoUniverso.jpg"
                //Selected = false
            },
            new MejorPeliculaAnimada
            {
                Nombre = "Wifi Ralph",
                ImageUrl = "WifiRalph.jpg"
                //Selected = false
            }
        };

        public List<MejorSonido> MSonido = new List<MejorSonido>
        {
            new MejorSonido
            {
                Nombre = "Bohemian Rhapsody",
                ImageUrl = "BohemianRhapsody.jpg"
                //Selected = false
            },
            new MejorSonido
            {
                Nombre = "El Primer Hombre en la Luna",
                ImageUrl = "ElPrimerHombreenlaLuna.jpg"
                //Selected = false
            },
            new MejorSonido
            {
                Nombre = "Pantera Negra",
                ImageUrl = "PanteraNegra.jpg"
                //Selected = false
            },
            new MejorSonido
            {
                Nombre = "Roma",
                ImageUrl = "Roma.jpg"
                //Selected = false
            },
            new MejorSonido
            {
                Nombre = "Nace Una Estrella",
                ImageUrl = "NaceUnaEstrella.jpg"
                //Selected = false
            },
        };

        public List<MejorDocumental> MDocumental = new List<MejorDocumental>
        {
            new MejorDocumental
            {
                Nombre = "Free Solo",
                ImageUrl = "FreeSolo.jpg"
                //Selected = false
            },
            new MejorDocumental
            {
                Nombre = "Hale Country This Morning, This Evening",
                ImageUrl = "HaleCountryThisMorningThisEvening.jpg"
                //Selected = false
            },
            new MejorDocumental
            {
                Nombre = "Minding The Gap",
                ImageUrl = "MindingTheGap.jpg"
                //Selected = false
            },
            new MejorDocumental
            {
                Nombre = "Of Fathers And Sons",
                ImageUrl = "OfFathersAndSons.jpg"
                //Selected = false
            },
            new MejorDocumental
            {
                Nombre = "RBG",
                ImageUrl = "RGB.jpg"
                //Selected = false
            },
        };

        public List<MejorEfectoSonoro> MEfectoSonoro = new List<MejorEfectoSonoro>
        {
            new MejorEfectoSonoro
            {
                Nombre = "Bohemian Rhapsody",
                ImageUrl = "BohemianRhapsody.jpg"
                //Selected = false
            },
            new MejorEfectoSonoro
            {
                Nombre = "El Primer Hombre en la Luna",
                ImageUrl = "ElPrimerHombreenlaLuna.jpg"
                //Selected = false
            },
            new MejorEfectoSonoro
            {
                Nombre = "Un Lugar en Silencio",
                ImageUrl = "UnLugarenSilencio.jpg"
                //Selected = false
            },
            new MejorEfectoSonoro
            {
                Nombre = "Pantera Negra",
                ImageUrl = "PanteraNegra.jpg"
                //Selected = false
            },
            new MejorEfectoSonoro
            {
                Nombre = "Roma",
                ImageUrl = "Roma.jpg"
                //Selected = false
            }
        };

        public List<MejorDisenoDeProduccion> MDisenoProduccion = new List<MejorDisenoDeProduccion>
        {
            new MejorDisenoDeProduccion
            {
                Nombre = "El Primer Hombre En La Luna",
                ImageUrl = "ElPrimerHombreEnLaLuna.jpg"
                //Selected = false
            },
            new MejorDisenoDeProduccion
            {
                Nombre = "El Regreso de Mary Poppins",
                ImageUrl = "ElRegresodeMaryPoppins.jpg"
                //Selected = false
            },
            new MejorDisenoDeProduccion
            {
                Nombre = "La Favorita",
                ImageUrl = "LaFavorita.jpg"
                //Selected = false
            },
            new MejorDisenoDeProduccion
            {
                Nombre = "Pantera Negra",
                ImageUrl = "PanteraNegra.jpg"
                //Selected = false
            },
            new MejorDisenoDeProduccion
            {
                Nombre = "Roma",
                ImageUrl = "Roma.jpg"
                //Selected = false
            }
        };

        public List<MejorBandaSonora> MBandaSonora = new List<MejorBandaSonora>
        {
            new MejorBandaSonora
            {
                Nombre = "El Regreso de Mary Poppins",
                ImageUrl = "ElRegresodeMaryPoppins.jpg"
                //Selected = false
            },
            new MejorBandaSonora
            {
                Nombre = "If Beale Street Could Talk",
                ImageUrl = "IfBealeStreetCouldTalk.jpg"
                //Selected = false
            },
            new MejorBandaSonora
            {
                Nombre = "Infiltrado En El Kkklan",
                ImageUrl = "InfiltradoEnElKkklan.jpg"
                //Selected = false
            },
            new MejorBandaSonora
            {
                Nombre = "Isla De Perros",
                ImageUrl = "IslaDePerros.jpg"
                //Selected = false
            },
            new MejorBandaSonora
            {
                Nombre = "Pantera Negra",
                ImageUrl = "PanteraNegra.jpg"
                //Selected = false
            }
        };

        public List<MejoresEfectosVisuales> MEfectosVisuales = new List<MejoresEfectosVisuales>
        {
            new MejoresEfectosVisuales
            {
                Nombre = "Avengers: Infinity War",
                ImageUrl = "AvengersInfinityWar.jpg"
                //Selected = false
            },
            new MejoresEfectosVisuales
            {
                Nombre = "Christopher Robin: Un Reencuentro Inolvidable",
                ImageUrl = "ChristopherRobin.UnReencuentroInolvidable.jpg"
                //Selected = false
            },
            new MejoresEfectosVisuales
            {
                Nombre = "El Primer Hombre en La Luna",
                ImageUrl = "ElPrimerHombreenLaLuna.jpg"
                //Selected = false
            },
            new MejoresEfectosVisuales
            {
                Nombre = "Han Solo: Una Historia de Star Wars",
                //Selected = false
                ImageUrl = "HanSolo.jpg"
            },
            new MejoresEfectosVisuales
            {
                Nombre = "Ready Player One",
                ImageUrl = "ReadyPlayerOne.jpg"
                //Selected = false
            }
        };

        public List<MejorCancion> MmejorCancion = new List<MejorCancion>
        {
            new MejorCancion
            {
                Nombre = "The Place Where Lost Things Go de El Regreso de Mary Poppins",
                ImageUrl = "ElRegresodeMaryPoppins(ThePlaceWhereLostThingsGo).jpg"
                //Selected = false
            },
            new MejorCancion
            {
                Nombre = "When A Cowboy Trades His Spurs For Wings de La Balada De Buster Scruggs",
                ImageUrl = "LaBaladaDeBusterScruggs(WhenACowboyTradesHisSpursForWings).jpg"
                //Selected = false
            },
            new MejorCancion
            {
                Nombre = "Shallow de Nace Una Estrella",
                ImageUrl = "NaceUnaEstrella(Shallow).jpg"
                //Selected = false
            },
            new MejorCancion
            {
                Nombre = "All The Stars de Pantera Negra",
                ImageUrl = "PanteraNegra(AllTheStars).jpg"
                //Selected = false
            },
            new MejorCancion
            {
                Nombre = "I´llFight de RBG",
                ImageUrl = "RGB(I´llFight).jpg"
                //Selected = false
            }
        };

        public List<MejorVestuario> MMejorVestuario = new List<MejorVestuario>
        {
            new MejorVestuario
            {
                Nombre = "El Regreso de Mary Poppins",
                ImageUrl = "ElRegresodeMaryPoppins.jpg"
                //Selected = false
            },
            new MejorVestuario
            {
                Nombre = "La Balada De Buster Scruggs",
                ImageUrl = "LaBaladaDeBusterScruggs.jpg"
                //Selected = false
            },
            new MejorVestuario
            {
                Nombre = "La Favorita",
                ImageUrl = "LaFavorita.jpg"
                //Selected = false
            },
            new MejorVestuario
            {
                Nombre = "Las Dos Reinas",
                ImageUrl = "LasDosReinas_.jpg"
                //Selected = false
            },
            new MejorVestuario
            {
                Nombre = "Pantera Negra",
                ImageUrl = "PanteraNegra.jpg"
                //Selected = false
            }
        };

        public List<MejorMontaje> MMejorMontaje = new List<MejorMontaje>
        {
            new MejorMontaje
            {
                Nombre = "Bohemian Rhapsody",
                ImageUrl = "BohemianRhapsody.jpg"
                //Selected = false
            },
            new MejorMontaje
            {
                Nombre = "El Vicio Del Poder",
                ImageUrl = "ElVicioDelPoder.jpg"
                //Selected = false
            },
            new MejorMontaje
            {
                Nombre = "Green Book: Una Amistad Sin Fronteras",
                ImageUrl = "GreenBook.UnaAmistadSinFronteras.jpg"
                //Selected = false
            },
            new MejorMontaje
            {
                Nombre = "Infiltrado En El Kkklan",
                ImageUrl = "InfiltradoEnElKkklan.jpg"
                //Selected = false
            },
            new MejorMontaje
            {
                Nombre = "La Favorita",
                ImageUrl = "LaFavorita.jpg"
                //Selected = false
            }
        };

        public List<MaquillajePeluqueria> MMaquillajePeliqueria = new List<MaquillajePeluqueria>
        {
            new MaquillajePeluqueria
            {
                Nombre = "Border",
                ImageUrl = "Border.jpg"
                //Selected = false
            },
            new MaquillajePeluqueria
            {
                Nombre = "El Vicio Del Poder",
                ImageUrl = "ElVicioDelPoder.jpg"
                //Selected = false
            },
            new MaquillajePeluqueria
            {
                Nombre = "Las Dos Reinas",
                ImageUrl = "LasDosReinas_.jpg"
                //Selected = false
            }
        };

        public List<MejorCortoDeAccionReal> MCortoDeAccionReal = new List<MejorCortoDeAccionReal>
        {
            new MejorCortoDeAccionReal
            {
                Nombre = "Detainment",
                ImageUrl = "Detainment.jpg"
                //Selected = false
            },
            new MejorCortoDeAccionReal
            {
                Nombre = "Fauve",
                ImageUrl = "Fauve.jpg"
                //Selected = false
            },
            new MejorCortoDeAccionReal
            {
                Nombre = "Madre",
                ImageUrl = "Madre.jpg"
                //Selected = false
            },
            new MejorCortoDeAccionReal
            {
                Nombre = "Marguerite",
                ImageUrl = "Marguerite.jpg"
                //Selected = false
            },
            new MejorCortoDeAccionReal
            {
                Nombre = "Skin",
                ImageUrl = "Skin.jpg"
                //Selected = false
            }
        };

        public List<MejorCortoDeAnimacion> MCortoDeAnimacion = new List<MejorCortoDeAnimacion>
        {
            new MejorCortoDeAnimacion
            {
                Nombre = "Animal Behaviour",
                ImageUrl = "AnimalBehaviour.jpg"
                //Selected = false
            },
            new MejorCortoDeAnimacion
            {
                Nombre = "Bao",
                ImageUrl = "Bao.jpg"
                //Selected = false
            },
            new MejorCortoDeAnimacion
            {
                Nombre = "Late Afternoon",
                ImageUrl = "LateAfternoon.jpg"
                //Selected = false
            },
            new MejorCortoDeAnimacion
            {
                Nombre = "One Small Step",
                ImageUrl = "OneSmallStep.jpg"
                //Selected = false
            },
            new MejorCortoDeAnimacion
            {
                Nombre = "Weekends",
                ImageUrl = "Weekends.jpg"
                //Selected = false
            }
        };

        public List<MejorCortoDocumental> MCortoDocumental = new List<MejorCortoDocumental>
        {
            new MejorCortoDocumental
            {
                Nombre = "A Night At The Garden",
                ImageUrl = "ANightAtTheGarden.jpg"
                //Selected = false
            },
            new MejorCortoDocumental
            {
                Nombre = "Black Sheep",
                ImageUrl = "BlackSheep.jpg"
                //Selected = false
            },
            new MejorCortoDocumental
            {
                Nombre = "End Game",
                ImageUrl = "EndGame.jpg"
                //Selected = false
            },
            new MejorCortoDocumental
            {
                Nombre = "Life Boat",
                ImageUrl = "LifeBoat.jpg"
                //Selected = false
            },
            new MejorCortoDocumental
            {
                Nombre = "Period End Of Sentence",
                ImageUrl = "PeriodEndOfSentence.jpg"
                //Selected = false
            }
        };
    }

    public class QuinielaObject2
    {
        public List<MejorPelicula> MPelicula { get; set; }
        public List<MejorDirector> MDirector { get; set; }
        public List<MejorActor> MActor { get; set; }
        public List<MejorActriz> MActriz { get; set; }
        public List<MejorActorSecundario> MActorSecundario { get; set; }
        public List<MejorActrizSecundaria> MActrizSecundaria { get; set;}
        public List<MejorPeliculaLenguaNoinglesa> MPeliculaLenguaNoInglesa { get; set;}
        public List<GuionOriginal> MGuionOriginal { get; set;}
        public List<GuionAdaptado> MGuionAdaptado { get; set;}
        public List<MejorFotografia> MFotografia { get; set;}
        public List<MejorPeliculaAnimada> MPeliculAanimada { get; set;}
        public List<MejorSonido> MSonido { get; set;}
        public List<MejorDocumental> MDocumental { get; set;}
        public List<MejorEfectoSonoro> MEfectoSonoro { get; set;}
        public List<MejorDisenoDeProduccion> MDisenoProduccion { get; set;}
        public List<MejorBandaSonora> MBandaSonora { get; set;}
        public List<MejoresEfectosVisuales> MEfectosVisuales { get; set;}
        public List<MejorCancion> MmejorCancion { get; set;}
        public List<MejorVestuario> MMejorVestuario { get; set;}
        public List<MejorMontaje> MMejorMontaje { get; set;}
        public List<MaquillajePeluqueria> MMaquillajePeliqueria { get; set; }
        public List<MejorCortoDeAccionReal> MCortoDeAccionReal { get; set; }
        public List<MejorCortoDeAnimacion> MCortoDeAnimacion { get; set; }
        public List<MejorCortoDocumental> MCortoDocumental { get; set; }
    }

    public class MejorPelicula
    {
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
    }

    public class MejorDirector
    {
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
        public string Pelicula { get; set; }
    }

    public class MejorActor
    {
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
        public string Pelicula { get; set; }
    }

    public class MejorActriz
    {
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
        public string Pelicula { get; set; }
    }

    public class MejorActorSecundario
    {
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
        public string Pelicula { get; set; }
    }

    public class MejorActrizSecundaria
    {
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
        public string Pelicula { get; set; }
    }

    public class MejorPeliculaLenguaNoinglesa
    {
        public string Nombre { get; set; }
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
        public string pais { get; set; }
    }

    public class GuionOriginal
    {
        public bool Selected { get; set; }
        public string ImageUrl { get; set; }
        public string Nombre { get; set; }
    }

    public class GuionAdaptado
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorFotografia
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorPeliculaAnimada
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorSonido
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorDocumental
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorEfectoSonoro
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorDisenoDeProduccion
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorBandaSonora
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejoresEfectosVisuales
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorCancion
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorVestuario
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorMontaje
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MaquillajePeluqueria
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorCortoDeAccionReal
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorCortoDeAnimacion
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }

    public class MejorCortoDocumental
    {
        public string Nombre { get; set; }
        public string ImageUrl { get; set; }
        public bool Selected { get; set; }
    }
}