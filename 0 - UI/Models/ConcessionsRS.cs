﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class ConcessionsRS
    {
        #region Properties

        public ConcessionOrder Order { get; set; }

        public string TransIdTemp
        {
            get
            {
                return Order.TransIdTemp;
            }
        }

        public ConcessionItem BookingFee
        {
            get
            {
                return Order.BookingFee;
            }
        }

        public bool WithError { get; set; }

        public string ErrorMessage { get; set; }

        #endregion
    }
}