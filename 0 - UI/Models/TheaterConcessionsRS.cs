﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class TheaterConcessionsRS
    {
        public List<ConcessionItem> Items;
        public ConcessionItem BookingFee;
    }
}