﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class UserEntity
    {
        public UserEntity()
        {
            //ReceiveMassMail = true;

            //if (ReceiveMassMail == true)
            //{
            //    ReceiveMassMail = true;
            //}
            //else
            //{
            //    ReceiveMassMail = false;
            //}
        }

        public Guid Id { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        [DataType(DataType.Date, ErrorMessage = "Formato de Fecha Inválido")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? BirthDate { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string Address { get; set; }

        public bool ReceiveMassMail { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? ActivationDate { get; set; }

        public Boolean Active { get; set; }

        public int Sex { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public String SexString
        {
            get { if (Sex == 0) return "M"; else return "F"; }
            set { if (value == "M") Sex = 0; else Sex = 1; }
        }

        [EmailAddress(ErrorMessage = "Correo no valido")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Email", ErrorMessage = "Los Correos no concuerdan")]
        public string EmailComparation { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "Las contraseñas no concuerdan")]
        public string PasswordComparation { get; set; }

        public string IdCard
        {
            get { return IdCardType + IdCardNumber; }
            set
            {
                if (value != null && value.Count() > 1)
                {
                    IdCardType = value.Substring(0, 1);
                    IdCardNumber = value.Substring(1, value.Length - 1);
                }
            }
        }

        public string IdCardType { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string IdCardNumber { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string LastName { get; set; }

        public string MobilePhone { get; set; }

        public DateTime DateOut { get; set; }

        public DateTime DateLastVisit { get; set; }

        public DateTime DateCurrentVisit { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string SecretQuestion { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string SecretAnswer { get; set; }

        public DateTime DateLastUpdate { get; set; }

        public int Zipcode { get; set; }

        public string City { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string State { get; set; }

        public string Twitter { get; set; }

        public string Facebook { get; set; }

        public string Instagram { get; set; }

        public string Country { get; set; }

        public string hash { get; set; }

        public List<SelectListItem> States = new List<SelectListItem>{
                        new SelectListItem {Value = "Amazonas", Text = "Amazonas", Selected = true},
                        new SelectListItem {Value = "Anzoátegui", Text = "Anzoátegui"},
                        new SelectListItem {Value = "Apure", Text = "Apure"},
                        new SelectListItem {Value = "Aragua", Text = "Aragua"},
                        new SelectListItem {Value = "Barinas", Text = "Barinas"},
                        new SelectListItem {Value = "Bolívar", Text = "Bolívar"},
                        new SelectListItem {Value = "Carabobo", Text = "Carabobo"},
                        new SelectListItem {Value = "Cojedes", Text = "Cojedes"},
                        new SelectListItem {Value = "Delta Amacuro", Text = "Delta Amacuro"},
                        new SelectListItem {Value = "Distrito Federal", Text = "Distrito Federal"},
                        new SelectListItem {Value = "Falcón", Text = "Falcón"},
                        new SelectListItem {Value = "Guárico", Text = "Guárico"},
                        new SelectListItem {Value = "Lara", Text = "Lara"},
                        new SelectListItem {Value = "Mérida", Text = "Mérida"},
                        new SelectListItem {Value = "Miranda", Text = "Miranda"},
                        new SelectListItem {Value = "Monagas", Text = "Monagas"},
                        new SelectListItem {Value = "Nueva Esparta", Text = "Nueva Esparta"},
                        new SelectListItem {Value = "Portuguesa", Text = "Portuguesa"},
                        new SelectListItem {Value = "Sucre", Text = "Sucre"},
                        new SelectListItem {Value = "Táchira", Text = "Táchira"},
                        new SelectListItem {Value = "Vargas", Text = "Vargas"},
                        new SelectListItem {Value = "Yaracuy", Text = "Yaracuy"},
                        new SelectListItem {Value = "Zulia", Text = "Zulia"}
        };
    }
}