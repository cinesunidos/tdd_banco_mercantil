﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class SessionInfo
    {
        #region Properties

        public Movie Movie { get; set; }

        public Theater Theater { get; set; }

        public int Id { get; set; }

        public string HallName { get; set; }

        public int MaxSeatsPerTransaction { get; set; }

        public bool SeatAllocation { get; set; }

        public int SeatsAvailable { get; set; }

        public bool OnSale { get; set; }

        public DateTime ShowTime { get; set; }

        public string Time
        {
            get
            {
                return ShowTime.ToShortTimeString().Replace(".", "");
            }
        }

        public string Day
        {
            get
            {
                return Session.GetDay(ShowTime);
            }
        }

        public Purchase Purchase { get; set; }

        #endregion Properties
        #region
        // Operador de casteo implicito de SessionInfoRS a SessionInfo
        public static implicit operator SessionInfo(SessionInfoRS responce)
        {
            SessionInfo sessionInfo = new SessionInfo();
            sessionInfo.HallName = responce.HallName;
            sessionInfo.Id = responce.Id;
            sessionInfo.MaxSeatsPerTransaction = responce.MaxSeatsPerTransaction;
            sessionInfo.SeatAllocation = responce.SeatAllocation;
            sessionInfo.SeatsAvailable = responce.SeatsAvailable;
            sessionInfo.ShowTime = responce.ShowTime;
            sessionInfo.Movie = responce.Movie;
            sessionInfo.Theater = responce.Theater;
            sessionInfo.Purchase = new Purchase(responce.Tickets);
            sessionInfo.OnSale = responce.OnSale;
            return sessionInfo;
        }

        #endregion
    }
}