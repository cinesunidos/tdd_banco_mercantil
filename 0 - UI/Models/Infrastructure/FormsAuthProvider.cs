﻿using System;
using System.Configuration;
using System.Web.Security;

namespace CinesUnidos.WebRoles.MVC.Models.Infrastructure
{
    public class FormsAuthProvider : IAuthProvider
    {
        #region Fields

        private bool m_IsCookiePersistent;
        private int m_CookieExpiration;
        private string m_Url;

        #endregion Fields

        #region Properties

        public bool IsCookiePersistent
        {
            get { return m_IsCookiePersistent; }
        }

        public int CookieExpiration
        {
            get { return m_CookieExpiration; }
        }

        public string Url
        {
            get { return m_Url; }
        }

        #endregion Properties

        #region Methods

        public void SignIn(string userName)
        {
            m_Url = FormsAuthentication.GetRedirectUrl(userName, m_IsCookiePersistent);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public FormsAuthProvider()
        {
            m_IsCookiePersistent = Convert.ToBoolean(ConfigurationManager.AppSettings["CookiePersistent"]);
            m_CookieExpiration = Convert.ToInt32(ConfigurationManager.AppSettings["CookieExpiration"]);
        }

        #endregion Methods
    }
}