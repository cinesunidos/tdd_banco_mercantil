﻿namespace CinesUnidos.WebRoles.MVC.Models.Infrastructure
{
    public interface IAuthProvider
    {
        #region Properties

        bool IsCookiePersistent { get; }

        int CookieExpiration { get; }

        string Url { get; }

        #endregion Properties

        #region Methods

        void SignIn(string userName);

        void SignOut();

        #endregion Methods
    }
}