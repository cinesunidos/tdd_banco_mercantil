﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class SessionInfoRS
    {
        #region Properties

        public Movie Movie { get; set; }

        public Theater Theater { get; set; }

        public int Id { get; set; }

        public string HallName { get; set; }

        public int MaxSeatsPerTransaction { get; set; }

        public bool SeatAllocation { get; set; }

        public int SeatsAvailable { get; set; }

        public DateTime ShowTime { get; set; }

        public bool OnSale { get; set; }

        public Ticket[] Tickets { get; set; }

        #endregion Properties
    }
}