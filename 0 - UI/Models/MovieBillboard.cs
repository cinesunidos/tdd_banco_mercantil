﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class MovieBillboard
    {
        #region Properties

        public Movie Movie { get; set; }

        public List<MovieBillboardTheater> Theaters { get; set; }

        public DateTime[] Dates { get; set; }

        #endregion Properties
    }

    public class MovieBillboardTheater
    {
        #region Properties

        public Theater Theater { get; set; }

        public List<Session> Sessions { get; set; }

        public List<GroupScreen> GroupScreen
        {
            get
            {
                List<GroupScreen> list = (from s in Sessions
                                          group s by new { HallName = s.HallName, HallNumber = s.HallNumber } into g
                                          select new GroupScreen
                                          {
                                              Theater = Theater.Name,
                                              HallName = g.Key.HallName,
                                              HallNumber = g.Key.HallNumber,
                                              Sessions = g.OrderBy(h => h.ShowTime).ToList()
                                          }).OrderBy(g => g.Theater).ThenBy(h => h.HallNumber).ToList();
                return list;
            }
        }

        #endregion Properties
    }

    public class GroupScreen
    {
        public string Theater { get; set; }

        public string HallName { get; set; }

        public int HallNumber { get; set; }

        public List<Session> Sessions { get; set; }
    }
}