﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public static class BarMovieCacheClass
    {
        #region MoreMoviesPorCiudad
        public static Movie[] BarCaracas { get; set; }
        public static Movie[] BarBarquisimeto { get; set; }
        public static Movie[] BarGuatire { get; set; }
        public static Movie[] BarMaracaibo { get; set; }
        public static Movie[] BarMaracay { get; set; }
        public static Movie[] BarMargarita { get; set; }
        public static Movie[] BarMaturin { get; set; }
        public static Movie[] BarPuertoLaCruz { get; set; }
        public static Movie[] BarPuertoOrdaz { get; set; }
        public static Movie[] BarSanCristobal { get; set; }
        public static Movie[] BarValencia { get; set; }
        #endregion

        public static void CleanCache()
        {
            BarBarquisimeto = null;
            BarCaracas = null;
            BarGuatire = null;
            BarMaracaibo = null;
            BarMaracay = null;
            BarMargarita = null;
            BarMaturin = null;
            BarPuertoLaCruz = null;
            BarPuertoOrdaz = null;
            BarSanCristobal = null;
            BarValencia = null;
        }
    }
}