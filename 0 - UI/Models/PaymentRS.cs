﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class PaymentRS
    {
        #region Properties

        public int BookingNumber { get; set; }

        public int TransactionNumber { get; set; }

        public string CodeResult { get; set; }

        public string DescriptionResult { get; set; }

        public string[] Voucher { get; set; }

        public ConfirmationRS Confirmation { get; set; }

        public string Name { get; set; }

        #endregion Properties
    }
}