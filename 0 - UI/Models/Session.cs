﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Session
    {
        #region Properties

        public DateTime ShowTime { get; set; }

        public int Id { get; set; }

        public string HallName { get; set; }

        public int HallNumber { get; set; }

        public bool SeatAllocated { get; set; }

        public string Censor { get; set; }

        public string Time
        {
            get
            {
                int hour = ShowTime.Hour > 12 ? ShowTime.Hour - 12 : ShowTime.Hour;
                return ShowTime.ToShortTimeString().Replace(".", "");
            }
        }

        public string Day
        {
            get
            {
                return GetDay(ShowTime);
            }
        }

        public string Month
        {
            get
            {
                return GetMonth(ShowTime);
            }
        }

        public string WeekDay
        {
            get
            {
                return string.Format("{0} {1}", Day, ShowTime.Day);
            }
        }

        #endregion Properties

        #region Methods

        public static string GetMonth(int month)
        {
            switch (month)
            {
                case 1:
                    return "Enero";

                case 2:
                    return "Febrero";

                case 3:
                    return "Marzo";

                case 4:
                    return "Abril";

                case 5:
                    return "Mayo";

                case 6:
                    return "Junio";

                case 7:
                    return "Julio";

                case 8:
                    return "Agosto";

                case 9:
                    return "Septiembre";

                case 10:
                    return "Octubre";

                case 11:
                    return "Noviembre";

                default:
                    return "Diciembre";
            }
        }

        public static string GetMonth(DateTime date)
        {
            switch (date.Month)
            {
                case 1:
                    return "Enero";

                case 2:
                    return "Febrero";

                case 3:
                    return "Marzo";

                case 4:
                    return "Abril";

                case 5:
                    return "Mayo";

                case 6:
                    return "Junio";

                case 7:
                    return "Julio";

                case 8:
                    return "Agosto";

                case 9:
                    return "Septiembre";

                case 10:
                    return "Octubre";

                case 11:
                    return "Noviembre";

                default:
                    return "Diciembre";
            }
        }

        public static string GetDay(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Friday:
                    return "Viernes";

                case DayOfWeek.Monday:
                    return "Lunes";

                case DayOfWeek.Saturday:
                    return "Sabado";

                case DayOfWeek.Sunday:
                    return "Domingo";

                case DayOfWeek.Thursday:
                    return "Jueves";

                case DayOfWeek.Wednesday:
                    return "Miercoles";

                default:
                    return "Martes";
            }
        }

        #endregion Methods
    }
}