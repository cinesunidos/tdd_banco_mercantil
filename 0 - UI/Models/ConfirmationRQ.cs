﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class ConfirmationRQ
    {
        #region Properties

        public Guid UserSessionId { get; set; }

        public Seat[] Seats { get; set; }
        
        public ConcessionItem[] Concessions { get; set; }

        public ConcessionItem BookingFee { get; set; }

        #endregion Properties
    }
}