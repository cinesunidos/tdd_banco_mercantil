﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models.Support
{
    public class LoginExtension
    {

        public void Create(Token entity)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            command.CommandText = "Access.RegisterToken";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                var idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.Value = Guid.NewGuid();

                var expirationParameter = command.CreateParameter();
                expirationParameter.ParameterName = "@Expiration";
                expirationParameter.Value = entity.Expiration;

                var hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = entity.Hash;

                var plataformParameter = command.CreateParameter();
                plataformParameter.ParameterName = "@Platform";
                plataformParameter.Value = entity.Platform;

                var loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@LoginId";
                loginParameter.Value = entity.UserId;

                command.Parameters.Add(idParameter);
                command.Parameters.Add(expirationParameter);
                command.Parameters.Add(hashParameter);
                command.Parameters.Add(plataformParameter);
                command.Parameters.Add(loginParameter);

                con.Open();
                command.ExecuteNonQuery();
                con.Close();
                command.Dispose();
                con.Dispose();
            

        }

        public void Update(Token entity)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            command.CommandText = "Access.RefreshToken";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                DateTime expiration = DateTime.Now.AddHours(24);
                var hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = entity.Hash;

                var expirationParameter = command.CreateParameter();
                expirationParameter.ParameterName = "@Expiration";
                expirationParameter.Value = expiration;

                command.Parameters.Add(hashParameter);
                command.Parameters.Add(expirationParameter);

                con.Open();
                command.ExecuteNonQuery();
                con.Close();
            command.Dispose();
            con.Dispose();

        }

        public void Delete(Token entity)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            command.CommandText = @"Access.DeleteToken";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                var hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = entity.Hash;
                command.Parameters.Add(hashParameter);
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
            command.Dispose();
            con.Dispose();

        }

        public Guid? FindIdUser(string login, string password)
        {
            Guid? Id = null;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            string sql = "SELECT [ID] FROM [Access].[Active] WHERE [EMAIL] = @Login AND [PASSWORD] = @Password";
                command.CommandText = sql;
        
                var loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@Login";
                loginParameter.Value = login;
                var passwordParameter = command.CreateParameter();
                passwordParameter.ParameterName = "@Password";
                passwordParameter.Value = password;
                command.Parameters.Add(loginParameter);
                command.Parameters.Add(passwordParameter);

                con.Open();
                var reader = command.ExecuteReader();
                
                    if (reader.Read())
                    {
                        Id = reader.GetGuid(0);
                    }
                
            con.Close();

            command.Dispose();
            con.Dispose();
            return Id;
        }

 
        public Token FindToken(Guid loginId, string platform)
        {
            Token token = null;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            string sql = "SELECT  [Id],[Expiration],[Hash],[LoginId],[Platform]  FROM [Access].[Token] WHERE [LoginId] = @loginId AND [Platform] = @platform";
                command.CommandText = sql;

                var loginIdParameter = command.CreateParameter();
                loginIdParameter.ParameterName = "@loginId";
                loginIdParameter.Value = loginId;

                var platformParameter = command.CreateParameter();
                platformParameter.ParameterName = "@platform";
                platformParameter.Value = platform;

                command.Parameters.Add(loginIdParameter);
                command.Parameters.Add(platformParameter);

                con.Open();
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        token = new Token();
                        token.Id = reader.GetGuid(0);
                        token.Expiration = reader.GetDateTime(1);
                        token.Hash = reader.GetString(2);
                        token.UserId = reader.GetGuid(3);
                    }
                }
                con.Close();
            command.Dispose();
            con.Dispose();
            return token;
        }

        public string MD5(string Flattext)
        {
            System.Security.Cryptography.MD5 Supplier = System.Security.Cryptography.MD5CryptoServiceProvider.Create();
            return Convert.ToBase64String(Supplier.ComputeHash(Encoding.ASCII.GetBytes(Flattext)));
        }

        public bool ExistEmail(string email)
        {
            bool exist = false;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            string sql = "SELECT [ID] FROM [Access].[Login] WHERE [EMAIL] = @email";
                command.CommandText = sql;

                var loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@email";
                loginParameter.Value = email;
                command.Parameters.Add(loginParameter);
                 con.Open();
                 var reader = command.ExecuteReader();
              
                    if (reader.Read())
                    {
                        exist = true;
                    }
            
             con.Close();
            command.Dispose();
            con.Dispose();
            return exist;
        }

        public bool IsUserActive(string email)
        {
            bool ret = false;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            string sql = "SELECT [Active] FROM [Access].[Login] WHERE [EMAIL] = @email";
                command.CommandText = sql;

                var loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@email";
                loginParameter.Value = email;
                command.Parameters.Add(loginParameter);

                con.Open();
                var reader = command.ExecuteReader();
                
                    if (reader.Read())
                    {
                        ret = reader.GetBoolean(0);
                    }
                con.Close();
            command.Dispose();
            con.Dispose();
            return ret;
        }


    }
}