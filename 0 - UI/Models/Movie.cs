﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Movie
    {
        #region Properties

        public string Id { get; set; }

        public string Title { get; set; }

        public string TitleShort
        {
            get
            {
                if (Title.Length > 23)
                {
                    return Title.Substring(0, 20) + "...";
                }
                else
                    return Title;
            }
        }

        public string Country { get; set; }

        public string Sinopsis { get; set; }

        public string Director { get; set; }

        public string Actor { get; set; }

        public Information Information { get; set; }

        #endregion Properties
    }
}