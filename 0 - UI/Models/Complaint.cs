﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Complaint
    {
        [Display(Name = "*TEMA")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string Subject { get; set; }

        [Display(Name = "*CINE")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string Theater { get; set; }

        [Display(Name = "*PELÍCULA")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string Movie { get; set; }

        [Display(Name = "FECHA")]
        [DataType(DataType.Date, ErrorMessage = "Formato de Fecha Inválido")]
        [Required(ErrorMessage = "Campo Requerido")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Date { get; set; }

        [Display(Name = "HORA")]
        [DataType(DataType.Time, ErrorMessage = "Formato de Feacha Inválido")]
        [Required(ErrorMessage = "Campo Requerido")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        public DateTime? Time { get; set; }

        [Display(Name = "*NOMBRE")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string Name { get; set; }

        [Display(Name = "*APELLIDO")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string LastName { get; set; }

        [Display(Name = "*CÉDULA")]
        [Required(ErrorMessage = "Campo Requerido")]
        [RegularExpression(@"^\d+$", ErrorMessage = "La cédula debe contener solo números")]
        public string IDCard { get; set; }

        [Display(Name = "CÓDIGO TEL")]
        [Required(ErrorMessage = "Campo Requerido")]
        [StringLength(4, ErrorMessage = ("Máximo {0} números"))]
        public string PhoneCode { get; set; }

        [Display(Name = "*TELÉFONO")]
        [Required(ErrorMessage = "Campo Requerido")]
        [StringLength(7, ErrorMessage = ("Máximo {0} números"))]
        [RegularExpression(@"^\d+$", ErrorMessage = "El teléfono debe contener solo números")]
        public string Phone { get; set; }

        [Display(Name = "*EMAIL")]
        [Required(ErrorMessage = "Campo Requerido")]
        [EmailAddress(ErrorMessage = "Formato de Email inválido")]
        public string Email { get; set; }

        [Display(Name = "*COMENTARIO")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string Comment { get; set; }
    }
}