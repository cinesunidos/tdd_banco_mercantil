﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class PaymentRQ
    {
        #region Properties

        public Guid UserSessionId { get; set; }

        public TypeCreditCard Type { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public string Number { get; set; }

        public string SecurityCode { get; set; }

        public string Name { get; set; }

        public string CertificateType { get; set; }

        public string Certificate { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string AccountType { get; set; }

        public string KeyAuth { get; set; }

        public decimal TotalValue { get; set; }

        public string VippoSessionToken { get; set; }

        public string VippoToken { get; set; }

        public string VippoReference { get; set; }

        public string BDVToken { get; set; }

        public string BDVAccountType { get; set; }

        public string BDVReference { get; set; }

        #endregion Properties

        #region Methods

        public static implicit operator PaymentRQ(PaymentInfomation creditCard)
        {
            PaymentRQ paymentRQ = new PaymentRQ();
            paymentRQ.Certificate = creditCard.IdCardNumber;
            paymentRQ.CertificateType = creditCard.IdCardType;
            paymentRQ.Email = creditCard.Email;
            paymentRQ.Month = creditCard.ExpirationMonth;
            paymentRQ.Name = creditCard.CreditCardName;
            paymentRQ.Number = creditCard.CreditCardNumber;
            paymentRQ.Phone = creditCard.CellPhone;
            paymentRQ.SecurityCode = creditCard.CreditCardSecurityCode;
            paymentRQ.UserSessionId = creditCard.IdClient;
            paymentRQ.Year = creditCard.ExpirationYear;
            paymentRQ.Type = creditCard.CreditCardType;
            paymentRQ.VippoToken = creditCard.VippoOTPCode;
            paymentRQ.VippoReference = creditCard.VippoTransactionReference;
            paymentRQ.VippoSessionToken = creditCard.VippoSessionToken;
            paymentRQ.BDVAccountType = creditCard.BDVAccountType;
            paymentRQ.BDVReference = creditCard.BDVReference;
            paymentRQ.BDVToken = creditCard.BDVToken;
            paymentRQ.AccountType = creditCard.AccountType;
            paymentRQ.KeyAuth = creditCard.KeyAuth;
            return paymentRQ;
        }

        #endregion Methods
    }
}