﻿
using CinesUnidos.ESB.Infrastructure.Agents.Concessions;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class ConcessionsRQ
    {


        #region Properties
        public string UserSessionId { get; set; }
          
        public string TheaterId { get; set; }
          
        public int SessionId { get; set; }

        public int BookingNumber { get; set; }

        public int TransactionNumber { get; set; }

        public string TransIdTemp { get; set; }

        public ConcessionItem[] Concessions { get; set; }

        public string PorcCargoWEb { get; set; }

        #endregion


    }
}