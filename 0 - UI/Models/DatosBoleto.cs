﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class DatosBoleto
    {
        //public bool CodUtilizado { get; set; }
        //public bool BoletoNoValido { get; set; }
        //public bool BoletoVencido { get; set; }
        //public string Stock_strBarcode { get; set; } //Codido de cortesia
        //public string Workstation_strCode { get; set; } // Taquilla
        //public string sName { get; set; } // Cine
        //public DateTime Redeemed_dtmUpdated { get; set; } // Fecha transaccion
        //public string Redeemed_strDescription { get; set; } // descripcion
        //public DateTime Session_dtmRealShow { get; set; }
        //public string User_intUserNo { get; set; } // Usuario
        //public DateTime dExpiryDate { get; set; } //Fecha de vencimiento
        //public string Error { get; set; } // Mensaje de error
        //public string token { get; set; }
        /// <summary>
        /// indica si es o no un multivoucher
        /// </summary>
        public bool IsMultiVoucher
        {
            get
            {
                return this.NumeroEntradas > 1;
            }
        }
        /// <summary>
        /// si es multivoucher, dice cuantas veces puede ser redimido originalmente
        /// </summary>
        public int NumeroEntradas { get; set; }
        /// <summary>
        /// si es multivoucher, dice cuentas veces mas puede ser canjeado
        /// </summary>
        public int EntradasRestantes { get; set; }
        /// <summary>
        /// indica si el boleto fue utilizado
        /// </summary>
        public bool CodUtilizado { get; set; }
        /// <summary>
        /// indica si es un voucher que no es valido o no está registrado el Vista VM
        /// </summary>
        public bool BoletoNoValido { get; set; }
        /// <summary>
        /// indica si la fecha de expiracion de voucher ya se cumplio
        /// </summary>
        public bool BoletoVencido { get; set; }
        /// <summary>
        /// el codigo del voucher (los digitos numericos)
        /// </summary>
        public string Stock_strBarcode { get; set; }
        /// <summary>
        /// indica la taquilla en la que se cambio el voucherr en caso de que haya sido canjeado
        /// </summary>
        public string Workstation_strCode { get; set; }
        /// <summary>
        /// el nombre del cine en que se camjeo el boleto
        /// </summary>
        public string sName { get; set; }
        /// <summary>
        /// la fecha en que se canjeo el voucher
        /// </summary>
        public DateTime Redeemed_dtmUpdated { get; set; }
        /// <summary>
        /// la pelicula por la cual se canjeo el voucher
        /// </summary>
        public string Redeemed_strDescription { get; set; }
        /// <summary>
        /// lka fecha de la funcion para la cual se canjeo el voucher
        /// </summary>
        public DateTime Session_dtmRealShow { get; set; }
        /// <summary>
        /// el usuario de la estacion de trabajo donde se canjeo el voucher
        /// </summary>
        public string User_intUserNo { get; set; }
        /// <summary>
        /// la fecha de expiracion del voucher
        /// </summary>
        public DateTime dExpiryDate { get; set; }
        /// <summary>
        /// la descripcion del resiltado de la consulta del voucher
        /// </summary>
        public string Error { get; set; }
        public string token { get; set; }
    }
}