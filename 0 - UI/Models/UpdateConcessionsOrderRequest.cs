﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class UpdateConcessionsOrderRequest
    {
        public string TheaterId { get; set; }

        public string TransIdTemp { get; set; }

        public bool PaymentStart { get; set; }

        public bool PaymentOK { get; set; }

        public bool OrderComplete { get; set; }

        public bool OrderCancel { get; set; }
    }
}