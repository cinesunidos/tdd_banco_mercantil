﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    /// <summary>
    /// Ramiro Marimón - clase Estatica para guardar contenido del slider que emplea mucho procesamiento para ser generado.
    /// esta clase, se crea una sola vez en 
    /// </summary>
    public static class SliderCacheClass
    {
        #region sliderPorCiudad
        public static MovieBillboard[] SliderCaracas { get; set; }
        public static MovieBillboard[] SliderBarquisimeto { get; set; }
        public static MovieBillboard[] SliderGuatire { get; set; }
        public static MovieBillboard[] SliderMaracaibo { get; set; }
        public static MovieBillboard[] SliderMaracay { get; set; }
        public static MovieBillboard[] SliderMargarita { get; set; }
        public static MovieBillboard[] SliderMaturin { get; set; }
        public static MovieBillboard[] SliderPuertoLaCruz { get; set; }
        public static MovieBillboard[] SliderPuertoOrdaz { get; set; }
        public static MovieBillboard[] SliderSanCristobal { get; set; }
        public static MovieBillboard[] SliderValencia { get; set; }
        #endregion

        /// <summary>
        /// Ramiro Marimón - Reiniciar los valores del cache a null, 
        /// para hacer que se refresquen en una proxima llamada al servicio
        /// </summary>
        public static void CleanCache()
        {
            SliderBarquisimeto = null;
            SliderCaracas = null;
            SliderGuatire = null;
            SliderMaracaibo = null;
            SliderMaracay = null;
            SliderMargarita = null;
            SliderMaturin = null;
            SliderPuertoLaCruz = null;
            SliderPuertoOrdaz = null;
            SliderSanCristobal = null;
            SliderValencia = null;
        }
    }
}