﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class TheaterBillboard
    {
        #region Properties

        public Theater Theater { get; set; }

        public List<TheaterBillboardMovie> Movies { get; set; }

        public DateTime[] Dates { get; set; }

        #endregion Properties
    }

    public class TheaterBillboardMovie
    {
        #region Properties

        public Movie Movie { get; set; }

        public List<Session> Sessions { get; set; }

        #endregion Properties
    }
}