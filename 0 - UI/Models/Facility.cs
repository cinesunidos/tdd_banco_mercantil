﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Facility
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        #endregion Properties
    }
}