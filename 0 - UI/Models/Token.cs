﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Token
    {
        #region Properties-Login

        public Guid UserId { get; set; }

        public Guid Id { get; set; }

        public DateTime Expiration { get; set; }

        public string Hash { get; set; }

        public string Platform { get; set; }

        public string Error { get; set; }

        #endregion Properties-Login
    }
}