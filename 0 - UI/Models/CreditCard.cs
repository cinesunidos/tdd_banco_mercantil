﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class CreditCard
    {
        #region Properties

        public Guid IdClient { get; set; }

        public TypeCreditCard CreditCardType { get; set; }

        [Required]
        public int ExpirationMonth { get; set; }

        [Required]
        public int ExpirationYear { get; set; }

        [Required]
        public string CreditCardNumber { get; set; }

        [Required]
        public string CreditCardSecurityCode { get; set; }

        [Required]
        public string CreditCardName { get; set; }

        [Required]
        public string IdCardType { get; set; }

        [Required]
        public string IdCardNumber { get; set; }

        [Required]
        public string CellPhone { get; set; }

        [Required]
        public string Email { get; set; }

        public string AccountType { get; set; }

        public string KeyAuth { get; set; }

        #endregion Properties

        #region Methods

        public static IEnumerable<SelectListItem> GetYear()
        {
            List<SelectListItem> years = new List<SelectListItem>();
            int year = int.Parse(DateTime.Now.Year.ToString().Substring(2, 2));
            int yearTop = year + 20;
            while (year <= yearTop)
            {
                years.Add(new SelectListItem() { Text = year.ToString(), Value = year.ToString() });
                year++;
            }
            return years;
        }

        public static IEnumerable<SelectListItem> GetPreviousYears()
        {
            List<SelectListItem> years = new List<SelectListItem>();
            int year = int.Parse(DateTime.Now.Year.ToString().Substring(2, 2));
            int yearTop = year + 20;

            for (int previousYears = 10; previousYears <= yearTop; previousYears++)
            {
                years.Add(new SelectListItem() { Text = previousYears.ToString(), Value = previousYears.ToString() });
            }

            return years;
        }

        public static IEnumerable<SelectListItem> GetMonth()
        {
            List<SelectListItem> months = new List<SelectListItem>();
            int month = 1;
            int monthTop = 12;
            while (month <= monthTop)
            {
                months.Add(new SelectListItem() { Text = month.ToString(), Value = month.ToString() });
                month++;
            }
            return months;
        }

        #endregion Methods
    }
}