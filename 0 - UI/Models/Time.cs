﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.WebRoles.MVC.Models
{
    [Serializable]
    [DataContract]
    public class Time
    {
        #region Properties
        [DataMember]
        public int Minutes { get; protected set; }
        [DataMember]
        public int Seconds { get; protected set; }

        [DataMember]
        public DateTime TimeOut { get; set; }

        [DataMember]
        public DateTime ShowTime { get; set; }

        #endregion Properties

        #region Methods

        public Time(DateTime timeOut, DateTime showTime)
        {
            TimeOut = timeOut;
            ShowTime = showTime;
        }

        public string LabelTime()
        {
            string format = "{0} {1} DE {2} DE {3} A LAS {4}";
            return string.Format(format, Session.GetDay(ShowTime), ShowTime.Day, Session.GetMonth(ShowTime), ShowTime.Year, ShowTime.ToShortTimeString().Replace(".", ""));
        }

        public void CalculeTime()
        {
            DateTime Start = DateTime.Now;
            TimeSpan time = TimeOut - Start;
            Minutes = time.Minutes;
            Seconds = time.Seconds;

            if (Minutes<=0)
            {
                Minutes = 0;
            }
            if (Seconds<=0)
            {
                Seconds = 0;
            }
        }

        #endregion Methods
    }
}