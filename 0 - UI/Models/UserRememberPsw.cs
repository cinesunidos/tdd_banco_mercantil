﻿using System.ComponentModel.DataAnnotations;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class UserRememberPsw
    {
        [EmailAddress(ErrorMessage = "Correo no valido")]
        [Required(ErrorMessage = "Campo Requerido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public string SecretQuestion { get; set; }
    }
}