﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public static class CityTicketPriceCacheClass
    {
        public static CityTicketPrice[] CityTicketPrice { get; set; }

        public static void CleanCache()
        {
            CityTicketPrice = null;
        }
    }
}