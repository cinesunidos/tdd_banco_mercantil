﻿using System.Linq;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Map
    {
        public Tier[] Tiers { get; set; }

        public int CountColummns()
        {
            return Tiers[0].Seats.Count();
        }
    }

    public class Tier
    {
        public char Name { get; set; }

        public Seat[] Seats { get; set; }
    }

    public class Seat
    {
        public string AreaCategoryCode { get; set; }

        public int AreaNumber { get; set; }

        public int ColumnIndex { get; set; }

        public string Name { get; set; }

        public int RowIndex { get; set; }

        public SeatStatus Status { get; set; }

        public SeatType Type { get; set; }
    }

    public enum SeatStatus : int
    {
        Booked = 0,
        House = 1,
        Selected = 2,
        Available = 3,
    }

    public enum SeatType : int
    {
        Chair = 0,
        Aisle = 1,
    }
}