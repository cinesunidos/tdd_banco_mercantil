﻿using System.Linq;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Purchase
    {
    

        #region Properties

        public Ticket[] Tickets { get; set; }

        public ConcessionItem[] Concessions { get; set; }

        public int Quantity
        {
            get
            {
                return Tickets.Sum(t => t.Quantity);
            }
        }

        public decimal TotalPrice
        {
            get
            {
                return Tickets.Sum(t => t.TotalPrice);
            }
        }

        /// <summary>
        /// Precio Base Total de los Tickets (Sin IVA)
        /// IM: Añadido/Modificado 2016-06-28
        /// </summary>
        public decimal TotalBasePrice
        {
            get
            {
                return Tickets.Sum(t => t.TotalBasePrice);
            }
        }

        public decimal TotalBookingFee
        {
            get
            {
                return Tickets.Sum(t => t.TotalBookingFee);
            }
        }

        public decimal TotalServiceFee
        {
            get
            {
                return Tickets.Sum(t => t.TotalServiceFee);
            }
        }

        /// <summary>
        /// Suma de Cargos por Servicios y Booking (sin IVA)
        /// IM: Añadido/Modificado 2016-06-28
        /// </summary>
        public decimal TotalServices
        {
            get
            {
                return Tickets.Sum(t => t.TotalServiceFee + t.TotalBookingFee);
            }
        }

        public decimal TotalPromotionFee
        {
            get
            {
                return Tickets.Sum(t => t.TotalPromotionFee);

            }
        }
        public decimal TotalPromotionTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalPromotionTax);
            }
        }

        public decimal TotalTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalTax);
            }
        }

        public decimal Total
        {
            get
            {
                return Tickets.Sum(t => t.Total);
            }
        }

        /// <summary>
        /// Suma del monto total de la boleteria y el total carameleria
        /// </summary>
        public decimal TotalPurchase
        {
            get
            {
                return Concessions == null ? Total : Total + TotalConcessionsWebCharge;
            }
        }


        /// <summary>
        /// Sumatoria de TotalServiceTax para todos los tipos de boleto contenidos en la compra
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalServiceTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalServiceTax);
            }
        }

        /// <summary>
        /// Sumatoria de TotalTicketTax para todos los tipos de boleto contenidos en la compra
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalTicketTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalTicketTax);
            }
        }

        /// <summary>
        /// Sumatoria de TotalBookingTax para todos los tipos de boleto contenidos en la compra
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalBookingTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalBookingTax);
            }
        }

        /// <summary>
        /// suma de TotalBasePrice y TotalTicketTax, para obtener el Total Boletos
        /// </summary>
        public decimal TotalTicketPlusTax
        {
            get
            {
                return TotalBasePrice + TotalTicketTax;
            }
        }

        /// <summary>
        /// suma de TotalBookingFee y TotalBookingTax, para obtener el Total Cargos Web
        /// </summary>
        public decimal TotalBookingPlusTax
        {
            get
            {
                return TotalBookingFee + TotalBookingTax;
            }
        }

        /// <summary>
        /// suma de TotalServiceFee y TotalServiceTax, para obtener el Total Servicios
        /// </summary>
        public decimal TotalServicePlusTax
        {
            get
            {
                return TotalServiceFee + TotalServiceTax;
            }
        }
        public decimal TotalPromotionPlusTax
        {
            get
            {
                return TotalPromotionFee + TotalPromotionTax;
            }
        }

        /// <summary>
        /// Cantidad de articulos total de carameleria
        /// </summary>
        public int QuantityConcessions
        {
            get
            {
                return Concessions == null
                ? 0
                : Concessions.Sum(t => (int)t.ItemQuantityPurchase);
            }
        }

        /// <summary>
        /// Sumatoria del precio de la carameleria con iva
        /// </summary>
        public decimal TotalConcessions
        {
            get
            {
                return Concessions == null
                ? 0
                : Concessions.Sum(t => t.ItemPurchaseByQuantity);
            }
        }

        public decimal TaxConcessions
        {
            get
            {
                if (Concessions == null)
                {
                    return 0;
                }
                else
                {
                    if (PercentageTax == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        //return TotalConcessions * (PercentageTax / 100);
                        return TotalConcessions - TotalBasePriceConcessions;
                    }
                }               
            }
        }

        public decimal WebChargeFee { get; set; }
        public decimal PercentageTax { get; set; }

        //public decimal WebChargeConcessions
        //{
        //    get
        //    {
        //        return Concessions == null
        //            ? default_concessions_value
        //            : WebChargeFee;
        //        //Concessions.Sum(t => t.ItemStrItemID == "999999999" ? t.ItemPurchaseByQuantity : default_concessions_value);
        //    }
        //}

        public decimal WebChargeTaxConcessions
        {
            get
            {
                if (WebChargeFee <= 0)
                {
                    return 0;
                }
                else
                {
                    if (PercentageTax == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        //return ((WebChargeFee * PercentageTax) / 100);
                        return WebChargeFee - WebBaseChargeConcessions;
                    }
                }
            }
        }

        public decimal WebBaseChargeConcessions
        {
            get
            {
                if (WebChargeFee <= 0)
                {
                    return 0;
                }
                else
                {
                    if (PercentageTax == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        // return WebChargeFee - WebChargeTaxConcessions;
                        return (WebChargeFee / (1 + (PercentageTax / 100)));
                    }
                }
            }
        }

        public decimal TotalBasePriceConcessions
        {
            get
            {
                return Concessions == null
                    ? 0
                    //: TotalConcessions - TaxConcessions;
                    : (TotalConcessions / (1 +(PercentageTax / 100)));
            }
        }

        public decimal TotalTaxConcessions
        {
            get
            {
                return Concessions == null
                    ? 0
                    //: TaxConcessions - WebChargeTaxConcessions;
                    : TaxConcessions + WebChargeTaxConcessions;
            }
        }

        /// <summary>
        /// Suma del total de la caramelería y el Cargos Web
        /// </summary>
        public decimal TotalConcessionsWebCharge
        {
            get
            {
                return Concessions == null || Concessions.Length == 0
                ? 0
                : Concessions.Sum(t => t.ItemPurchaseByQuantity) + WebChargeFee;
            }
        }

        #endregion Properties

        #region Methods

        public Purchase()
        {

        }

        public Purchase(Ticket[] tickets)
        {
            Tickets = tickets;
            Concessions = null;
        }


        public Purchase(Ticket[] tickets, ConcessionItem[] concessions, decimal Tax, decimal BookingFee)
        {
            Tickets = tickets;
            Concessions = concessions;
            //Cargo web
            WebChargeFee = BookingFee;
            //IVA
            PercentageTax = Tax;
        }

        #endregion Methods
    }
}