﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    /// <summary>
    /// Ramiro Marimón - clase Estatica para guardar el contenido de la seccion barmovies que emplea mucho procesamiento para ser generado.
    /// esta clase, se crea una sola vez en 
    /// </summary>
    public static class MoreMoviesCacheClass
    {
        #region MoreMoviesPorCiudad
        public static Movie[] MoreCaracas { get; set; }
        public static Movie[] MoreBarquisimeto { get; set; }
        public static Movie[] MoreGuatire { get; set; }
        public static Movie[] MoreMaracaibo { get; set; }
        public static Movie[] MoreMaracay { get; set; }
        public static Movie[] MoreMargarita { get; set; }
        public static Movie[] MoreMaturin { get; set; }
        public static Movie[] MorePuertoLaCruz { get; set; }
        public static Movie[] MorePuertoOrdaz { get; set; }
        public static Movie[] MoreSanCristobal { get; set; }
        public static Movie[] MoreValencia { get; set; }
        #endregion

        public static void CleanCache()
        {
            MoreBarquisimeto = null;
            MoreCaracas = null;
            MoreGuatire = null;
            MoreMaracaibo = null;
            MoreMaracay = null;
            MoreMargarita = null;
            MoreMaturin = null;
            MorePuertoLaCruz = null;
            MorePuertoOrdaz = null;
            MoreSanCristobal = null;
            MoreValencia = null;
        }
    }
}