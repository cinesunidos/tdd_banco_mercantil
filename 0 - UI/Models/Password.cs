﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Password
    {
        public Guid UserId { get; set; }

        public string ActualPassword { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmationPassword { get; set; }
    }
}