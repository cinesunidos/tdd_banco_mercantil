﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Premier
    {
        #region Properties

        public string Id { get; set; }

        public string Title { get; set; }

        public DateTime Date { get; set; }

        public string Country { get; set; }

        public string Sinopsis { get; set; }

        public string Director { get; set; }

        public string Actor { get; set; }

        public Information Information { get; set; }

        #endregion Properties
    }
}