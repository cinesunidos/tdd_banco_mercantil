﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class TicketPrice
    {
        #region properties
        public string ticketName { set; get; }
       
        public string formatMovie { set; get; }
       
        public string typeHall { set; get; }
       
        public decimal price { set; get; }
        public decimal fee { set; get; }
        public decimal totalPrice { get { return price + fee; } }
        #endregion properties
    }

    public class CityTicketPrice
    {
        #region properties
        public string city { set; get; }
       
        public DateTime date { set; get; }
       
        public List<TheaterTicketPrice> theaterTicketPrice { set; get; }
        public CityTicketPrice()
        {
            theaterTicketPrice = new List<TheaterTicketPrice>();
        }
        #endregion properties
    }

    public class TheaterTicketPrice
    {
        #region properties
        public string theaterID { set; get; }
        public string name { set; get; }
        public List<TicketPrice> listTicketPrice { set; get; }
        public TheaterTicketPrice()
        {
            listTicketPrice = new List<TicketPrice>();
        }
        #endregion properties
    }
}