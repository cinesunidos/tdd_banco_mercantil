﻿using Newtonsoft.Json;
using QRCoder;
using System;
using System.Drawing;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class PurchaseEntity
    {
        #region Properties


        public int Id { get; set; }
        
        public string UserTypeId { get; set; }

        public string UserId { get; set; }
        
        public DateTime Date { get; set; }
        
        public string FilmCode { get; set; }
        
        public string FilmTitle { get; set; }
        
        public string CinemaCode { get; set; }
        
        public string CinemaTitle { get; set; }
        
        public decimal Amount { get; set; }
        
        public int Tickets { get; set; }

        public string BookingNumber { get; set; }

        public int SessionId { get; set; }

        public string SessionOBJ { get; set; }

        #endregion Properties
    }

    public class PurchaseQR
    {
        public int Id { get; set; }
        public string UserTypeId { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public string FilmCode { get; set; }
        public string FilmTitle { get; set; }
        public string CinemaCode { get; set; }
        public string CinemaTitle { get; set; }
        public decimal Amount { get; set; }
        public int Tickets { get; set; }
        public string BookingNumber { get; set; }
        public int SessionId { get; set; }
        public Byte[] QRcode { get; set; }
        public int Validate { get; set; }
        public string SessionOBJ { get; set; }
        public UserSession SessionOBJ2 { get; set; }

        //public static implicit operator PurchaseQR2(PurchaseEntity pe)
        //{
        //    int bn;
        //    PurchaseQR qr = new PurchaseQR();
        //    qr.Amount = pe.Amount;
        //    qr.BookingNumber = pe.BookingNumber;
        //    qr.CinemaCode = pe.CinemaCode;
        //    qr.CinemaTitle = pe.CinemaTitle;
        //    qr.Date = pe.Date;
        //    qr.FilmCode = pe.FilmCode;
        //    qr.FilmTitle = pe.FilmTitle;
        //    qr.Id = pe.Id;
        //    qr.SessionId = pe.SessionId;
        //    qr.Tickets = pe.Tickets;
        //    qr.UserTypeId = pe.UserTypeId;
        //    if (pe.SessionOBJ != "" && pe.SessionOBJ != string.Empty)
        //    {
        //        qr.SessionOBJ = JsonConvert.DeserializeObject<UserSessionRS>(pe.SessionOBJ);
        //    }
        //    else
        //    {
        //        qr.SessionOBJ = null;
        //    }
        //    if (int.TryParse(pe.BookingNumber, out bn))
        //    {
        //        qr.Validate = 0;
        //        qr.QRcode = null;
        //    }
        //    else
        //    {
        //        qr.Validate = 1;
        //        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        //        QRCodeData qrCodeData = qrGenerator.CreateQrCode(pe.BookingNumber.ToString(), QRCodeGenerator.ECCLevel.Q);
        //        QRCode qrCode = new QRCode(qrCodeData);
        //        Bitmap qrCodeImage = qrCode.GetGraphic(12);
        //        qr.QRcode = ImageToByte(qrCodeImage);
        //    }
        //    return qr;
        //}
        public static byte[] ImageToByte(Bitmap img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
    }

    public class PurchaseQR2
    {
        public int Id { get; set; }
        public string UserTypeId { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public string FilmCode { get; set; }
        public string FilmTitle { get; set; }
        public string CinemaCode { get; set; }
        public string CinemaTitle { get; set; }
        public decimal Amount { get; set; }
        public int Tickets { get; set; }
        public string BookingNumber { get; set; }
        public int SessionId { get; set; }
        public Byte[] QRcode { get; set; }
        public int Validate { get; set; }
        public UserSession SessionOBJ { get; set; }

        public static implicit operator PurchaseQR2(PurchaseQR pe)
        {
            int bn;
            PurchaseQR2 qr = new PurchaseQR2();
            qr.Amount = pe.Amount;
            qr.BookingNumber = pe.BookingNumber;
            qr.CinemaCode = pe.CinemaCode;
            qr.CinemaTitle = pe.CinemaTitle;
            qr.Date = pe.Date;
            qr.FilmCode = pe.FilmCode;
            qr.FilmTitle = pe.FilmTitle;
            qr.Id = pe.Id;
            qr.SessionId = pe.SessionId;
            qr.Tickets = pe.Tickets;
            qr.UserTypeId = pe.UserTypeId;
            try
            {
                if (pe.SessionOBJ != "" && pe.SessionOBJ != string.Empty && pe.SessionOBJ != null)
                {

                    qr.SessionOBJ = JsonConvert.DeserializeObject<UserSession>(pe.SessionOBJ);
                }
                else
                {
                    qr.SessionOBJ = null;
                }
            }
            catch (Exception e)
            {

                qr.SessionOBJ = null;
            }
            if (int.TryParse(pe.BookingNumber, out bn))
            {
                qr.Validate = 0;
                qr.QRcode = null;
            }
            else
            {
                qr.Validate = 1;
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(pe.BookingNumber.ToString(), QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(12);
                qr.QRcode = ImageToByte(qrCodeImage);
            }
            return qr;
        }
        public static byte[] ImageToByte(Bitmap img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
    }
}