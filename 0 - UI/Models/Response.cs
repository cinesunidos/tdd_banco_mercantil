﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Response
    {
        public Boolean isOk { get; set; }

        public Dictionary<string, object> Dictionary { get; set; }
    }
}