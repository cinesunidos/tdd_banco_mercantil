﻿using System;
using System.Linq;
using System.Web;
namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Payment
    {
        #region Properties

        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Theater { get; set; }

        public Seat[] Seats { get; set; }

        public ConcessionItem[] Concessions { get; set; }

        public string TheaterId { get; set; }

        public string HO { get; set; }

        public int SessionId { get; set; }

        public bool IsValid { get; set; }

        public CreditCard CreditCard { get; set; }

        public Time Time { get; set; }

        public Purchase Purchase { get; set; }

        public DateTime ShowTime { get; set; }

        public string Date
        {
            get
            {
                string time = ShowTime.ToShortTimeString().Replace(".", "");
                return string.Format("{0} {1} DE {2} DE {3} A LAS {4}"
                    , Session.GetDay(ShowTime)
                    , ShowTime.Day
                    , Session.GetMonth(ShowTime)
                    , ShowTime.Year
                    , time);
            }
        }

        #endregion Properties

        #region Methods

        public static implicit operator Payment(ConfirmationRS responce)
        {
            Payment payment = new Payment();
            payment.TheaterId = responce.TheaterId;
            payment.SessionId = responce.SessionId;
            payment.Id = responce.Id;
            payment.Seats = responce.Seats.ToArray();
            payment.Concessions = responce.Concessions.ToArray();
            payment.Title = responce.Title;
            payment.Theater = responce.Theater;
            payment.Purchase = new Purchase(responce.Tickets,responce.Concessions,responce.PercentageTax,responce.WebChargeConcessions);
            payment.Time = new Time(responce.TimeOut, responce.ShowTime);
            //payment.Time = new Time(responce.PaymentTimeOut, responce.ShowTime);
            payment.Time.CalculeTime();                      
            payment.ShowTime = responce.ShowTime;
            payment.HO = responce.HO;

            var session = HttpContext.Current.Session;
            session[payment.Id.ToString()] = payment.Time.TimeOut;
           
            return payment;
        }

        #endregion Methods
    }
}