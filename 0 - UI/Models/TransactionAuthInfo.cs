﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class TransactionAuthInfo
    {
        public string trx_type { get; set; }
        public string payment_method { get; set; }
        public string card_number { get; set; }
        public string customer_id { get; set; }
    }
}