﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models.Support
{
    public static class Extensions
    {
        public static DateTime TimeZoneCaracas(this DateTime date)
        {
            //Se comenta este timeZone por el cambio de zona Horaria efectuado el dia 01/05/2016 (UTC - 04:30) Caracas
            //TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            //Se procede a colocar el Time Zone SA Western Standard Time que equivale a (UTC-04:00) Georgetown, La Paz, Manaus, San Juan
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("SA Western Standard Time");
            DateTime now = TimeZoneInfo.ConvertTimeFromUtc(date, timeZone);
            return now;
        }
    }
}