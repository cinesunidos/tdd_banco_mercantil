﻿using CinesUnidos.WebRoles.MVC.Models;
using CinesUnidos.WebRoles.MVC.Models.Support;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Hosting;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Configuration;

namespace System.Web.Mvc.Html
{
    public static class HtmlHelpersExtensions
    {
        /// <summary>
        /// este string es el utilizado para llamar los servicios de la capa dos de forma legitima.
        /// la idea es que solo la capa uno (o alguien que conozca el token) pueda llamar los metos de la capa dos y obtener las respuestas;
        /// el token se valida en cada llamado a los metodos de la capa dos
        /// </summary>
        private static string TokenServices = ConfigurationManager.AppSettings["TokenServices"].ToString();
        public static MvcHtmlString BarDays(this HtmlHelper html, string actionName, string controllerName, string city, string id, DateTime[] dates, string @class)
        {
            #region Coment

            /*
            //string href ="#" + "{0}{1}{2}{3}";
            string href = "#" + "{0}{1}{2}{3}";
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass(@class);
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("nav nav-tabs tab_2");

            #region Agrupacion por mes

            //var group = from d in dates
            //            group d by d.Month into g
            //            orderby g.Key
            //            select new { Month = g.Key, Days = g };
            var group = from d in dates
                        orderby d.Year, d.Month
                        group d by d.Month into g
                        select new { Month = g.Key, Days = g };

            #endregion Agrupacion por mes

            StringBuilder fullmonth = new StringBuilder();
            bool first = true;
            foreach (var month in group)
            {
                #region Mes

                TagBuilder li_month = new TagBuilder("li");
                li_month.AddCssClass("mes");
                TagBuilder p_month = new TagBuilder("p");
                p_month.InnerHtml = Session.GetMonth(month.Month).Substring(0, 3);
                li_month.InnerHtml = p_month.ToString();

                #endregion Mes

                fullmonth.Append(li_month.ToString());

                #region Lista de Dias

                foreach (var day in month.Days.Take(15))
                {
                    TagBuilder li = new TagBuilder("li");
                    TagBuilder a = new TagBuilder("a");
                    if (first)
                    {
                        first = false;
                    }
                    string link = string.Format(href, city, id, day.Day.ToString("#00"), day.Month);
                    a.Attributes.Add("href", "#menu");
                    a.Attributes.Add("data-toggle", "tab");                    
                    TagBuilder span = new TagBuilder("span");
                    span.InnerHtml = Session.GetDay(day).Substring(0, 3);
                    var br = new TagBuilder("br").ToString(TagRenderMode.SelfClosing);
                    a.InnerHtml = span.ToString() + br + day.Day.ToString() ;
                    li.InnerHtml = a.ToString();
                    fullmonth.Append(li.ToString());
                }

                #endregion Lista de Dias
            }
            ul.InnerHtml = fullmonth.ToString();
            div.InnerHtml = ul.ToString();
            return MvcHtmlString.Create(div.ToString());
            */
            #endregion

            string href = "/" + controllerName + "/" + actionName + "/{0}/{1}/{2}/{3}";
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass(@class);
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("barra_dias");

            #region Agrupacion por mes

            //var group = from d in dates
            //            group d by d.Month into g
            //            orderby g.Key
            //            select new { Month = g.Key, Days = g };
            var group = from d in dates
                        orderby d.Year, d.Month
                        group d by d.Month into g
                        select new { Month = g.Key, Days = g };

            #endregion Agrupacion por mes

            StringBuilder fullmonth = new StringBuilder();
            bool first = true;
            foreach (var month in group)
            {
                #region Mes

                TagBuilder li_month = new TagBuilder("li");
                li_month.AddCssClass("barra_mesli");
                TagBuilder p_month = new TagBuilder("p");
                p_month.AddCssClass("barra_mes");
                p_month.InnerHtml = Session.GetMonth(month.Month).Substring(0, 3);
                li_month.InnerHtml = p_month.ToString();

                #endregion Mes

                fullmonth.Append(li_month.ToString());

                #region Lista de Dias

                foreach (var day in month.Days.Take(15))
                {
                    TagBuilder li = new TagBuilder("li");
                    TagBuilder a = new TagBuilder("a");
                    if (first)
                    {
                        li.AddCssClass("barradia_act");
                        first = false;
                    }
                    string link = string.Format(href, city, id, day.Day, day.Month);
                    a.Attributes.Add("href", link);
                    a.Attributes.Add("data-id", id);
                    a.AddCssClass("searchDay");
                    TagBuilder span = new TagBuilder("span");
                    span.InnerHtml = Session.GetDay(day).Substring(0, 3);
                    a.InnerHtml = span.ToString() + day.Day.ToString();
                    li.InnerHtml = a.ToString();
                    fullmonth.Append(li.ToString());
                }

                #endregion Lista de Dias
            }
            ul.InnerHtml = fullmonth.ToString();
            div.InnerHtml = ul.ToString();
            return MvcHtmlString.Create(div.ToString());
        }

        public static bool CompararHoras(string filename, double hours)
        {
            ////  var fecha = DateTime.Now.AddHours(-hours);
            ////return System.IO.File.GetLastWriteTime(filename).AddHours(-hours) <= fecha;

            bool value;

            DateTime DateSystem = DateTime.Now;
            DateTime DateFile = System.IO.File.GetLastWriteTime(filename);

            TimeSpan result = DateSystem - DateFile;

            if (result.TotalHours >= hours) value = true; else value = false;

            return value;

        }

        /// <summary>
        /// Ramiro Marimón - metodo para generar el cache del BarMovies.
        /// </summary>
        /// <param name="city">la ciudad a la que se quiere generar el barMovies</param>
        /// <returns></returns>
        public static Movie[] GetBarMoviesCache(string city)
        {
            Service services = new Service();
            Movie[] movies = services.Execute<Movie[]>("Movie.Bar", new { city = city, token = TokenServices });
            return movies.Where(m => m != null).ToArray();
        }

        public static dynamic GetCache<T>(string city, string service, string filename, dynamic parameters)
        {
            T[] valueObject = null;
            string filePath = "";
            string cachePath = string.Empty;

            if (System.Configuration.ConfigurationManager.AppSettings["DirectoryKeys"].ToString().Equals("Cloud"))
            {
                if (city != null)
                {
                    cachePath = RoleEnvironment.GetLocalResource(city).RootPath;
                }
                else
                {
                    cachePath = RoleEnvironment.GetLocalResource("DataWeb").RootPath;
                }
                filePath = IO.Path.Combine(cachePath, filename);
            }
            else
            {
                filePath = (HostingEnvironment.MapPath("~/App_Data/" + city + "." + filename));
            }

            if (!System.IO.File.Exists(filePath))
                System.IO.File.Create(filePath).Close();

            Boolean valido = CompararHoras(filePath, 1);
            FileInfo file = new FileInfo(filePath);

            if (valido || file.Length == 0)
            {
                Service services = new Service();
                if (city != null && parameters != null)
                {
                    valueObject = services.Execute<T[]>(service, parameters);
                }

                if (city == null && parameters == null)
                {
                    valueObject = services.Execute<T[]>(service);
                }

                string json = JsonConvert.SerializeObject(valueObject);
                System.IO.File.WriteAllText(filePath, json);
            }
            else
            {
                valueObject = JsonConvert.DeserializeObject<T[]>(System.IO.File.ReadAllText(filePath));
            }
            return valueObject;
        }

        public static MvcHtmlString BarMovie(this HtmlHelper html, string city)
        {
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass("mod_pelivelofin");
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("mod_pelivelofinlist");

            #region Lista de Imagenes

            Movie[] movies;
            switch (city.ToLower())
            {
                case "barquisimeto":
                    movies = BarMovieCacheClass.BarBarquisimeto == null ? BarMovieCacheClass.BarBarquisimeto = GetBarMoviesCache(city) : BarMovieCacheClass.BarBarquisimeto;
                    break;
                case "caracas":
                    movies = BarMovieCacheClass.BarCaracas == null ? BarMovieCacheClass.BarCaracas = GetBarMoviesCache(city) : BarMovieCacheClass.BarCaracas;
                    break;
                case "guatire":
                    movies = BarMovieCacheClass.BarGuatire == null ? BarMovieCacheClass.BarGuatire = GetBarMoviesCache(city) : BarMovieCacheClass.BarGuatire;
                    break;
                case "maracaibo":
                    movies = BarMovieCacheClass.BarMaracaibo == null ? BarMovieCacheClass.BarMaracaibo = GetBarMoviesCache(city) : BarMovieCacheClass.BarMaracaibo;
                    break;
                case "maracay":
                    movies = BarMovieCacheClass.BarMaracay == null ? BarMovieCacheClass.BarMaracay = GetBarMoviesCache(city) : BarMovieCacheClass.BarMaracay;
                    break;
                case "margarita":
                    movies = BarMovieCacheClass.BarMaracay == null ? BarMovieCacheClass.BarMaracay = GetBarMoviesCache(city) : BarMovieCacheClass.BarMaracay;
                    break;
                case "maturin":
                    movies = BarMovieCacheClass.BarMaturin == null ? BarMovieCacheClass.BarMaturin = GetBarMoviesCache(city) : BarMovieCacheClass.BarMaturin;
                    break;
                case "puerto la cruz":
                    movies = BarMovieCacheClass.BarPuertoLaCruz == null ? BarMovieCacheClass.BarPuertoLaCruz = GetBarMoviesCache(city) : BarMovieCacheClass.BarPuertoLaCruz;
                    break;
                case "puerto ordaz":
                    movies = BarMovieCacheClass.BarPuertoOrdaz == null ? BarMovieCacheClass.BarPuertoOrdaz = GetBarMoviesCache(city) : BarMovieCacheClass.BarPuertoOrdaz;
                    break;
                case "san cristobal":
                    movies = BarMovieCacheClass.BarSanCristobal == null ? BarMovieCacheClass.BarSanCristobal = GetBarMoviesCache(city) : BarMovieCacheClass.BarSanCristobal;
                    break;
                case "valencia":
                    movies = BarMovieCacheClass.BarValencia == null ? BarMovieCacheClass.BarValencia = GetBarMoviesCache(city) : BarMovieCacheClass.BarValencia;
                    break;
                default:
                    movies = BarMovieCacheClass.BarCaracas == null ? BarMovieCacheClass.BarCaracas = GetBarMoviesCache(city) : BarMovieCacheClass.BarCaracas;
                    break;
            }

            // Movie[] movies = GetCache<Movie>(city, "Movie.Bar", "Movie.Bar.xml", new { city = city });

            if (movies != null)
            {
                foreach (var item in movies)
                {
                    string ho = item.Id;
                    string url = UrlMaker.ToPoster(ho);
                    TagBuilder col_3 = new TagBuilder("li");
                    col_3.AddCssClass("");
                    TagBuilder a = new TagBuilder("a");
                    TagBuilder img = new TagBuilder("img");
                    img.Attributes.Add("src", url);
                    a.Attributes.Add("href", string.Format("/Pelicula/{0}/{1}", city, ho));
                    a.InnerHtml = img.ToString();
                    col_3.InnerHtml = a.ToString();
                    ul.InnerHtml = ul.InnerHtml + col_3.ToString();
                }
                div.InnerHtml = ul.ToString();
                return MvcHtmlString.Create(div.ToString());
            }

            #endregion Lista de Imagenes

            else
            {
                return MvcHtmlString.Create(string.Empty);
            }
            /*
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass("mod_pelivelofin");
            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("mod_pelivelofinlist");

            #region Lista de Imagenes

            Service services = new Service();
            Movie[] movies = services.Execute<Movie[]>("Movie.Bar", new { city = city });
            if (movies != null)
            {
                foreach (var item in movies)
                {
                    string ho = item.Id;
                    string url = UrlMaker.ToPoster(ho);
                    TagBuilder li = new TagBuilder("li");
                    TagBuilder a = new TagBuilder("a");
                    TagBuilder img = new TagBuilder("img");
                    img.Attributes.Add("src", url);
                    a.Attributes.Add("href", string.Format("/Pelicula/{0}/{1}", city, ho));
                    a.InnerHtml = img.ToString();
                    li.InnerHtml = a.ToString();
                    ul.InnerHtml = ul.InnerHtml + li.ToString();
                }
                div.InnerHtml = ul.ToString();
                return MvcHtmlString.Create(div.ToString());
            }

            #endregion Lista de Imagenes

            else
            {
                return MvcHtmlString.Create(string.Empty);
            }*/
        }

        public static MvcHtmlString Cities(this HtmlHelper html, string selected)
        {
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass("select_ciudad");
            TagBuilder i = new TagBuilder("i");
            i.Attributes.Add("class", "fa fa-angle-down");
            TagBuilder select = new TagBuilder("select");
            select.Attributes.Add("name", "ciudad_id");
            select.Attributes.Add("id", "ciudad_id");
            Service services = new Service();
            string[] cities = new string[] { "Barquisimeto", "Caracas", "Guatire", "Maracaibo", "Maracay", "Margarita", "Maturin", "Puerto La Cruz", "Puerto Ordaz", "San Cristobal", "Valencia" };
            //string[] cities = services.Execute<string[]>("Theater.GetAllCity");
            foreach (var item in cities)
            {
                TagBuilder option = new TagBuilder("option");
                option.InnerHtml = item;
                option.Attributes.Add("value", item);
                if (string.IsNullOrEmpty(selected))
                {
                    if (item.Equals("Caracas"))
                        option.Attributes.Add("selected", "true");
                }
                else
                {
                    if (item.Equals(selected))
                    {
                        var userCookie = new HttpCookie("ciudadCU", item);
                        userCookie.Expires.AddDays(365);
                        HttpContext.Current.Response.SetCookie(userCookie);
                        option.Attributes.Add("selected", "true");
                    }
                }
                select.InnerHtml = select.InnerHtml + option.ToString();
            }
            TagBuilder a = new TagBuilder("a");
            a.Attributes.Add("href", "#");
            a.Attributes.Add("name", "ciudad_id");
            a.Attributes.Add("id", "ciudad_id");
            TagBuilder span = new TagBuilder("span");
            a.InnerHtml = a.InnerHtml + span.ToString();
            div.InnerHtml = select.ToString() + a.ToString();

            return MvcHtmlString.Create(div.ToString());
        }

        public static MvcHtmlString helpSanitizeURLString(this HtmlHelper html, string param)
        {
            string Results = param;
            Results = Results.Replace("%", "%25");
            Results = Results.Replace("<", "%3C");
            Results = Results.Replace(">", "%3E");
            Results = Results.Replace("#", "%23");
            Results = Results.Replace("{", "%7B");
            Results = Results.Replace("}", "%7D");
            Results = Results.Replace("|", "%7C");
            Results = Results.Replace("'\'", "%5C");
            Results = Results.Replace("^", "%5E");
            Results = Results.Replace("~", "%7E");
            Results = Results.Replace("[", "%5B");
            Results = Results.Replace("]", "%5D");
            Results = Results.Replace("`", "%60");
            Results = Results.Replace(";", "%3B");
            Results = Results.Replace("/", "%2F");
            Results = Results.Replace("?", "%3F");
            Results = Results.Replace(":", "%3A");
            Results = Results.Replace("@", "%40");
            //Results = Results.Replace("=", "%3D");
            Results = Results.Replace("&", "%26");
            Results = Results.Replace("$", "%24");
            Results = Results.Replace("+", "%2B");

            return MvcHtmlString.Create(Results);
        }
    }
}