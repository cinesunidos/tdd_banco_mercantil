﻿using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models.Support
{
    public class Service
    {
        #region Attributes

        private WebClient m_client;
        
        private SerializerType m_type;
        private string m_clientId;

        #endregion Attributes

        #region Constructor

        public Service()
        {
            m_client = new WebClient();
            SetHeader();
            m_type = SerializerType.json;
        }

        public Service(SerializerType serializer)
        {
            m_type = serializer;
        }

        #endregion Constructor

        #region Methods

        protected string Name(string key)
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["Services.Url"].ToString();
            return url + System.Configuration.ConfigurationManager.AppSettings[key].ToString();
        }

        public R Execute<T, R>(string key, T model)
            where T : class
            where R : class
        {
            string url = Name(key);//Busca el Url del servicio

            #region Serializa el model

            XmlObjectSerializer obj = GetSerializer<T>();
            MemoryStream stream = new MemoryStream();
            obj.WriteObject(stream, model);
            string data = Encoding.UTF8.GetString(stream.ToArray(), 0, (int)stream.Length);

            #endregion Serializa el model

            #region Cear Request

            WebClient webClient = new WebClient();
            webClient.Headers.Add("ClientID", m_clientId);
            webClient.Headers["Content-type"] = GetContentType();
            webClient.Encoding = Encoding.UTF8;

            #endregion Cear Request

            string result = webClient.UploadString(url, "POST", data); //Ejecuta la llamada al servicio
            byte[] response = System.Text.Encoding.UTF8.GetBytes(result);
            return Convert<R>(response);
        }

        public T Execute<T>(string key)
             where T : class
        {
            string url = Name(key);
            byte[] data = m_client.DownloadData(url);
            return Convert<T>(data);
        }

        public T Execute<T>(string key, object parameters)
           where T : class
        {
            string url = Name(key);
           
            if (parameters != null)
            {
                foreach (PropertyInfo item in parameters.GetType().GetProperties())
                {
                    string value = item.GetValue(parameters, null).ToString();
                    string oldvalue = "{" + item.Name + "}";
                    url = url.Replace(oldvalue, value);
                }
            }
            byte[] data = m_client.DownloadData(url);
            return Convert<T>(data);
        }

        private T Convert<T>(byte[] data)
            where T : class
        {
            Stream stream = new MemoryStream(data);
            XmlObjectSerializer obj = GetSerializer<T>();
            T model = null;
            if (!stream.Length.Equals(0))
            {
                model = obj.ReadObject(stream) as T;
            }
            return model;
        }

        private XmlObjectSerializer GetSerializer<T>()
            where T : class
        {
            XmlObjectSerializer serializer;
            switch (m_type)
            {
                case SerializerType.xml:
                    serializer = new DataContractSerializer(typeof(T));
                    break;

                default:
                    serializer = new DataContractJsonSerializer(typeof(T));
                    break;
            }
            return serializer;
        }

        private string GetContentType()
        {
            switch (m_type)
            {
                case SerializerType.xml:
                    return "application/xml";

                default:
                    return "application/json";
            }
        }

        private void SetHeader()
        {
            m_clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"];
            m_client.Headers.Add("ClientID", m_clientId);
            UserPrincipal principal = HttpContext.Current.User as UserPrincipal;
            if (principal != null)
            {
                m_client.Headers.Add("Token", principal.Info.Hash);
            }
        }

        #endregion Methods
    }

    public enum SerializerType : int
    {
        json = 0,
        xml = 1
    }
}