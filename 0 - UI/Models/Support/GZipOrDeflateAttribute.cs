﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CinesUnidos.WebRoles.MVC.Models.Support
{
    public class GZipOrDeflateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting
             (ActionExecutingContext filterContext)
        {
            //string acceptencoding = filterContext.HttpContext.Request.Headers["Accept-Encoding"];

            //if (!string.IsNullOrEmpty(acceptencoding))
            //{
            //    acceptencoding = acceptencoding.ToLower();
            //    var response = filterContext.HttpContext.Response;
            //    if (acceptencoding.Contains("gzip"))
            //    {
            //        response.AppendHeader("Content-Encoding", "gzip");
            //        response.BufferOutput = false;
            //        response.Filter = new GZipStream(response.Filter,
            //                              CompressionMode.Compress,false);
            //    }
            //    else if (acceptencoding.Contains("deflate"))
            //    {
            //        response.AppendHeader("Content-Encoding", "deflate");
            //        response.Filter = new DeflateStream(response.Filter,
            //                          CompressionMode.Compress);
            //    }
            //}

            if (filterContext.IsChildAction)
                return;

            HttpRequestBase request = filterContext.HttpContext.Request;

            string acceptEncoding = request.Headers["Accept-Encoding"];

            if (string.IsNullOrEmpty(acceptEncoding)) return;

            acceptEncoding = acceptEncoding.ToUpperInvariant();

            HttpResponseBase response = filterContext.HttpContext.Response;

            if (acceptEncoding.Contains("GZIP"))
            {
                response.AppendHeader("Content-encoding", "gzip");
                response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
            }
            else if (acceptEncoding.Contains("DEFLATE"))
            {
                response.AppendHeader("Content-encoding", "deflate");
                response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
            }


        }
    }
}