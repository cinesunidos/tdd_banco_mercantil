﻿using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models.Support
{
    public static class UrlMaker
    {
        public static string ToImages(string image)
        {
            return string.Format("https://cinesunidosweb.blob.core.windows.net/img/{0}", image);
        }

        public static string ToSlider(string ho)
        {
            try
            {
                string cdn = System.Configuration.ConfigurationManager.AppSettings["CDN"].ToString();
                string url = "{0}/slider/{1}.jpg";
                return string.Format(url, cdn, ho);
            }
            catch (System.Exception)
            {

                return string.Empty;
            }
        }

        public static string ToPoster(string ho)
        {
            string cdn = System.Configuration.ConfigurationManager.AppSettings["CDN"].ToString();
            string url = "{0}/poster/{1}.jpg";
            return string.Format(url, cdn, ho);
        }

        #region Private

        private static string Path(string virtualPath)
        {
            if (HttpContext.Current == null)
                return "";
            return VirtualPathUtility.ToAbsolute(virtualPath);
        }

        private static string Path(string virtualPath, params object[] args)
        {
            return Path(string.Format(virtualPath, args));
        }

        #endregion Private
    }
}