﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    /// <summary>
    /// RM - informacion sobre un voocher que se desea validar para una funcion determinada
    /// </summary>
    public class InfoVoucher
    {
        /// <summary>
        /// saber cuantas veces mas puedo redimir el voucher si es multivoucher
        /// </summary>
        public int EntradasRestantes { get; set; }
        /// <summary>
        /// indica si es o no un multivoucher
        /// </summary>
        public bool IsMultiVoucher { get; set; }
        /// <summary>
        /// indica si el voucher es valido para la funcion seleccionada
        /// </summary>
        public bool ValidoParafuncion { get; set; }
        /// <summary>
        /// indica si el voucher ya fue redimido o ssigue disponible
        /// </summary>
        public bool Redimido { get; set; }
        /// <summary>
        /// Codigo del tipo de boleto por el cual s epuede canjear este voucher
        /// </summary>
        public string CodigoBoletoParaCanje { get; set; }
        /// <summary>
        /// indica cuanto debe pagar el invitado por la redencion de este codigo
        /// </summary>
        public decimal ValorRedencion { get; set; }
        /// <summary>
        /// indica el codigo del voucher a redimir
        /// </summary>
        public string VoucherCode { get; set; }

        /// <summary>
        /// indica el texto que se le mostrará al invitado en caso de que el boleto no sea valido, este usado o vencido
        /// </summary>
        public string TextoMostrar { get; set; }

        public int Cant_Redimir { get; set; }
    }
}