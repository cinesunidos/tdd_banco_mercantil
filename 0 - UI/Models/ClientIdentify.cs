﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class ClientIdentify
    {
        public string ipaddress { get; set; }
        public string browser_agent { get; set; }
        public Mobile mobile { get; set; }
    }
}