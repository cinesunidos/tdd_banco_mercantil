﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class UserSession
    {
        #region Properties

        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Theater { get; set; }

        public string HO { get; set; }

        public string HallName { get; set; }

        public Map Map { get; set; }

        public Seat[] Seats { get; set; }

        public ConcessionItem[] Concessions { get; set; }

        public ConcessionItem BookingFee { get; set; }

        public string TheaterId { get; set; }

        public int SessionId { get; set; }

        public Time Time { get; set; }

        public Purchase Purchase { get; set; }

        public DateTime ShowTime { get; set; }

        public string BookingByte { get; set; }

        public string BookingId { get; set; }

        public bool ValidConcessions { get; set; }

        /// <summary>
        /// Habilitar la venta de concesiones si esta identificado en el Web.config para el cine (TheatersValidConcessions)
        /// y si los tickets son comprados para el mismo dia (No se permite ventas futuras de concesiones).
        /// </summary>
        public bool ConcessionsSaleEnabled
        {
            get
            {
                TimeZoneInfo timeZone = System.TimeZoneInfo.FindSystemTimeZoneById("SA Western Standard Time");
                DateTime nowDateTime = DateTime.UtcNow;
                DateTime newDateTime = TimeZoneInfo.ConvertTime(nowDateTime, timeZone);
                int c = 0;
                foreach (var i in Purchase.Tickets)
                {
                    if (i.Name.Contains("Pa Ti Pa Mi"))
                    {
                        c++;
                    }
                }
                return ValidConcessions && ShowTime.Date == newDateTime.Date && c == 0;
            }
        }

        public decimal CargoWeb { get; set; }

        public string Date
        {
            get
            {
                return string.Format("{0} {1} de {2} de {3}", Session.GetDay(ShowTime), ShowTime.Day, Session.GetMonth(ShowTime), ShowTime.Year);
            }
        }

        public string TimeSession
        {
            get
            {
                return ShowTime.ToShortTimeString().Replace(".", "");
            }
        }

        #region Payment
        //public string BookingId { get; set; }

        public int BookingNumber { get; set; }

        public string CodeResult { get; set; }

        public string DescriptionResult { get; set; }

        public string[] Voucher { get; set; }

        public string Name { get; set; }

        // public string BookingByte { get; set; }
        // public string BookingConcat { get; set; }

        #endregion Payment

        public decimal PercentageTax { get; set; }
        public decimal WebChargeConcessions { get; set; }
        #endregion Properties

        #region Methods

        public static implicit operator UserSession(UserSessionRS response)
        {
            UserSession userSession = new UserSession();
            userSession.Id = response.Id;
            userSession.TheaterId = response.TheaterId;
            userSession.SessionId = response.SessionId;
            //
            userSession.WebChargeConcessions = response.WebChargeConcessions;
            userSession.PercentageTax = response.PercentageTax;
            //
            userSession.Purchase = new Purchase(response.Tickets, response.Concessions, response.PercentageTax, response.WebChargeConcessions);
            userSession.Time = new Time(response.TimeOut, response.ShowTime);
            userSession.Time.CalculeTime();
            userSession.Seats = response.Seats;
            userSession.Map = response.Map;
            userSession.Title = response.Title;
            userSession.Theater = response.Theater;
            userSession.ShowTime = response.ShowTime;
            userSession.HallName = response.HallName;
            userSession.Concessions = response.Concessions;
            userSession.BookingFee = response.BookingFee;
            userSession.BookingNumber = response.BookingNumber;
            userSession.CodeResult = response.CodeResult;
            userSession.DescriptionResult = response.DescriptionResult;
            userSession.Voucher = response.Voucher;
            userSession.Name = response.Name;
            userSession.BookingByte = response.BookingByte;
            userSession.HO = response.HO;

            //RM - determinar el el invitado está comprando el boleto plan pareja. de ser positivo, no muestro la modal de carameleria..
            var BoletoPromo = response.Tickets.Where(t => t.Name.ToLower().Contains("pareja") || t.Name.ToLower().Contains("familia")).Count();
            if (BoletoPromo > 0)
            {
                userSession.ValidConcessions = false;
            }
            else
            {
                userSession.ValidConcessions = response.ValidConcessions;
            }
            //userSession.ValidConcessions = response.ValidConcessions;

            return userSession;
        }

        #endregion Methods
    }
}