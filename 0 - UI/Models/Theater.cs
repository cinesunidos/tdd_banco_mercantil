﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public sealed class Theater
    {
        #region Properties

        public string Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string City { get; set; }

        public Facility[] Facilities { get; set; }

        #endregion Properties
    }
}