﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    
    public class ConcessionOrder
    {
        public string TransIdTemp { get; set; }

        public string ClientId { get; set; }

        public string LocationStrCode { get; set; }

        public decimal PaymentOrderTotal { get; set; }

        public bool PaymentStart { get; set; }

        public bool PaymentOK { get; set; }

        public bool OrderComplete { get; set; }

        public bool OrderCancel { get; set; }

        public ConcessionItem BookingFee { get; set; }

        public List<ConcessionItem> Items { get; set; }

        //public List<Concessions> Items { get; set; }

    }
}