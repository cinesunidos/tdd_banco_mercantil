﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    /// <summary>
    /// Clase para hacer caché de la seccion peliculas por ciudad.
    /// </summary>
    public static class MovieCacheClass
    {
        public static MovieBillboard[] MoviesCaracas { get; set; }
        public static MovieBillboard[] MoviesBarquisimeto { get; set; }
        public static MovieBillboard[] MoviesGuatire { get; set; }
        public static MovieBillboard[] MoviesMaracaibo { get; set; }
        public static MovieBillboard[] MoviesMaracay { get; set; }
        public static MovieBillboard[] MoviesMargarita { get; set; }
        public static MovieBillboard[] MoviesMaturin { get; set; }
        public static MovieBillboard[] MoviesPuertoLaCruz { get; set; }
        public static MovieBillboard[] MoviesPuertoOrdaz { get; set; }
        public static MovieBillboard[] MoviesSanCristobal { get; set; }
        public static MovieBillboard[] MoviesValencia { get; set; }

        public static void CleanCache()
        {
            MoviesBarquisimeto = null;
            MoviesCaracas = null;
            MoviesGuatire = null;
            MoviesMaracaibo = null;
            MoviesMaracay = null;
            MoviesMargarita = null;
            MoviesMaturin = null;
            MoviesPuertoLaCruz = null;
            MoviesPuertoOrdaz = null;
            MoviesSanCristobal = null;
            MoviesValencia = null;
        }
    }
}