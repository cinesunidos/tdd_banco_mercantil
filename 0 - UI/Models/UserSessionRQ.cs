﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class UserSessionRQ
    {
        #region Properties

        public string TheaterId { get; set; }

        public int SessionId { get; set; }

        public Ticket[] Tickets { get; set; }

        public string MovieId { get; set; }

        #endregion Properties
    }
}