﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class BDVConfirmation
    {
        public Guid IdClient { get; set; }
        public string BDVToken { get; set; }
        public string AccountType { get; set; }
        public string Reference { get; set; }
    }
}