﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RetainHttpsAttribute : Attribute
    {
    }
}