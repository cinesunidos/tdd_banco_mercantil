﻿using System;
using System.Web.Mvc;

namespace CinesUnidos.WebRoles.MVC.Models.Filters
{
    public class RequireHttpsWithPortAttribute : System.Web.Mvc.RequireHttpsAttribute
    {
        public int? Port { get; set; }

        public RequireHttpsWithPortAttribute(int Port)
            : base()
        {
            this.Port = Port;
        }

        public RequireHttpsWithPortAttribute()
            : base()
        {
            this.Port = null;
        }

        protected override void HandleNonHttpsRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            base.HandleNonHttpsRequest(filterContext);
            var result = (RedirectResult)filterContext.Result;
            var uri = new UriBuilder(result.Url);
            if (Port.HasValue)
                uri.Port = Port.Value;
            filterContext.Result = new RedirectResult(uri.ToString());
        }
    }
}