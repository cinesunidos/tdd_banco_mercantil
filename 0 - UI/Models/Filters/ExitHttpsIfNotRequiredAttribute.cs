﻿using System;
using System.Web.Mvc;

namespace CinesUnidos.WebRoles.MVC.Models.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ExitHttpsIfNotRequiredAttribute : FilterAttribute, IAuthorizationFilter
    {
        public int? Port { get; set; }

        public ExitHttpsIfNotRequiredAttribute()
            : base()
        { }

        public ExitHttpsIfNotRequiredAttribute(int Port)
            : base()
        {
            this.Port = Port;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            // abort if it's not a secure connection
            if (!filterContext.HttpContext.Request.IsSecureConnection) return;
            // abort if a [RequireHttps] attribute is applied to controller or action
            if (filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(RequireHttpsWithPortAttribute), true).Length > 0) return;
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(RequireHttpsWithPortAttribute), true).Length > 0) return;
            // abort if a [RetainHttps] attribute is applied to controller or action
            if (filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(RetainHttpsAttribute), true).Length > 0) return;
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(RetainHttpsAttribute), true).Length > 0) return;
            // abort if it's not a GET request - we don't want to be redirecting on a form post
            if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase)) return;
            // redirect to HTTP
            string url = string.Empty;
            if (Port.HasValue)
                url = string.Format("http://{0}:{1}{2}", filterContext.HttpContext.Request.Url.Host, Port.Value, filterContext.HttpContext.Request.RawUrl);
            else
                url = string.Format("http://{0}{1}", filterContext.HttpContext.Request.Url.Host, filterContext.HttpContext.Request.RawUrl);
            filterContext.Result = new RedirectResult(url);
        }
    }
}