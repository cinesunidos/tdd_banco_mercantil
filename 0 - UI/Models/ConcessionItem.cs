﻿
using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Clase que implementa las propiedades de Concessions
    /// </summary>
    public class ConcessionItem
    {
        public string ItemStrItemID { get; set; }

        public string ItemHOPK1 { get; set; }

        public string ItemHOPK2 { get; set; }

        public string ItemHOPK3 { get; set; }

        public string ItemStrItemDescription { get; set; }

        public decimal ItemPriceDCurPrice { get; set; }

        public string ItemStrItemDescriptionRecipes { get; set; }

        public decimal ItemQuantityStock { get; set; }

        public string ItemStrAbbriation { get; set; }

        public string ItemLocationStrCode { get; set; }

        public string ItemLocationStrDescription { get; set; }

        public decimal ItemStockCurAvailable { get; set; }

        //para mejorar el comportamiento 
        //Ramiro Marimon.
        public decimal ItemQuantityPurchase { get; set; }

        public string ItemStockUOMCode { get; set; }

        public string ItemType { get; set; }

        public decimal RecipeQuantity { get; set; }

        public string VendorStrCode { get; set; }

        public string ClassStrCode { get; set; }

        public decimal ItemPurchaseByQuantity
        {
            get
            {
                return Decimal.Multiply(ItemPriceDCurPrice, ItemQuantityPurchase);
            }
        }


    }
}