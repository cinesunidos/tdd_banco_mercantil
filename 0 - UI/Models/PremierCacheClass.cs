﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public static class PremierCacheClass
    {
        #region MoreMoviesPorCiudad
        public static Premier[] Premiers { get; set; }
        #endregion

        public static void CleanCache()
        {
            Premiers = null;
        }
    }
}