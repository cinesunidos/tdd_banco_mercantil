﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class SignIn
    {
        #region Properties

        public string Email { get; set; }

        public string Password { get; set; }

        public string Platform { get; set; }

        public string Url { get; set; }

        #endregion Properties
    }
}