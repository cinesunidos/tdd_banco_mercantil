﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class APaymentRequest
    {
        public string Amount { get; set; }
        public string Client_UserVippo { get; set; }
        public string Client_PasswordVippo { get; set; }
    }
}