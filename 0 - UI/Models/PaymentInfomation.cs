﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class PaymentInfomation
    {
        #region ClientInfo
        public Guid IdClient { get; set; }
        public string IdCardType { get; set; }
        public string IdCardNumber { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public decimal TotalToPay { get; set; }
        #endregion

        #region CreditCard
        public TypeCreditCard CreditCardType { get; set; }
        public string CreditCardNumber { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string CreditCardSecurityCode { get; set; }
        public string CreditCardName { get; set; }

        public string AccountType { get; set; }

        public string KeyAuth { get; set; }
        #endregion

        #region ViPPO
        public string VippoUser { get; set; }
        public string VippoPassword { get; set; }
        public string VippoTransactionReference { get; set; }
        public string VippoSessionToken { get; set; }
        public string VippoOTPCode { get; set; }
        #endregion

        #region Banco de Venezuela

        public string BDVToken { get; set; }
        public string BDVReference { get; set; }
        public string BDVAccountType { get; set; }

        #endregion

        #region Operadores Implicitos

        public static implicit operator PaymentInfomation(CreditCard creditcard)
        {
            PaymentInfomation infomation = new PaymentInfomation
            {
                IdClient = creditcard.IdClient,
                CreditCardType = creditcard.CreditCardType,
                CreditCardNumber = creditcard.CreditCardNumber,
                ExpirationMonth = creditcard.ExpirationMonth,
                ExpirationYear = creditcard.ExpirationYear,
                CreditCardSecurityCode = creditcard.CreditCardSecurityCode,
                IdCardType = creditcard.IdCardType,
                IdCardNumber = creditcard.IdCardNumber,
                CreditCardName = creditcard.CreditCardName,
                CellPhone = creditcard.CellPhone,
                Email = creditcard.Email,
                AccountType = creditcard.AccountType,
                KeyAuth = creditcard.KeyAuth
            };
            return infomation;
        }

        public static implicit operator PaymentInfomation(Vippo vippo)
        {
            PaymentInfomation infomation = new PaymentInfomation
            {
                IdClient = vippo.Id,
                IdCardType = vippo.IdCardType,
                IdCardNumber = vippo.IdCardNumber,
                VippoUser = vippo.VippoUser,
                VippoPassword = vippo.VippoPassword,
                VippoOTPCode = vippo.VippoOTPCode,
                VippoSessionToken = vippo.VippoSessionToken,
                VippoTransactionReference = vippo.VippoTransactionReference,
                CreditCardType = TypeCreditCard.PAGOVIPPO
            };
            return infomation;
        }

        public static implicit operator PaymentInfomation(BDVConfirmation bdvConfirm)
        {
            PaymentInfomation infomation = new PaymentInfomation
            {
                IdClient = bdvConfirm.IdClient,
                BDVAccountType = bdvConfirm.AccountType,
                BDVToken = bdvConfirm.BDVToken,
                BDVReference = bdvConfirm.Reference,
            };
            if (bdvConfirm.AccountType == "1" || bdvConfirm.AccountType == "2")
            {
                infomation.CreditCardType = TypeCreditCard.PAGOVZLATDD;
            }
            else
            {
                infomation.CreditCardType = TypeCreditCard.PAGOVZLATDC;

            }
            return infomation;
        }

        #endregion
    }
    public enum TypeCreditCard : int
    {
        Visa = 0,
        Master = 1,
        Amex = 2,
        Giftcard = 3,
        PAGOVIPPO = 5,
        PAGOVZLATDD = 6,
        PAGOVZLATDC = 7,
        PAGOMERCTDD = 8,
        PAGOMERCTDC = 9,
    }
}