﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Information
    {
        #region Properties

        public string Gender { get; set; }

        public string Class { get; set; }

        public string Subtitle { get; set; }

        public string Format { get; set; }

        public int Time { get; set; }

        public string Trailer { get; set; }

        public string Web { get; set; }

        #endregion Properties

        #region Methods

        public override string ToString()
        {
            string info = "{0} - {1} - {2} - {3} MIN";
            return string.Format(info, Gender, Subtitle, Format, Time);
        }

        #endregion Methods
    }
}