﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class ConfirmationRS
    {
        #region Properties

        public Guid Id { get; set; }

        public DateTime TimeOut { get; set; }

        public DateTime PaymentTimeOut { get; set; }

        public string Title { get; set; }

        public string Theater { get; set; }

        public string HO { get; set; }

        public DateTime ShowTime { get; set; }

        public Ticket[] Tickets { get; set; }

        public Seat[] Seats { get; set; }

        public ConcessionItem[] Concessions { get; set; }

        public string TheaterId { get; set; }

        public int SessionId { get; set; }

        public bool IsValid { get; set; }
        public decimal WebChargeConcessions { get; set; } 
        public decimal PercentageTax { get; set; }

        #endregion Properties
    }
}