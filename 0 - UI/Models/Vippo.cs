﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class Vippo
    {
        public Guid Id { get; set; }
        public string IdCardType { get; set; }
        public string IdCardNumber { get; set; }
        public string VippoUser { get; set; }
        public string VippoPassword { get; set; }
        public string VippoTransactionReference { get; set; }
        public string VippoSessionToken { get; set; }
        public string VippoOTPCode { get; set; }
    }
}