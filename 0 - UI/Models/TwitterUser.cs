﻿namespace CinesUnidos.WebRoles.MVC.Models
{
    public class TwitterUser
    {
        public string UserName { get; set; }

        public string Name { get; set; }
    }
}