﻿using System;

namespace CinesUnidos.WebRoles.MVC.Models
{
    public class User
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }

        public DateTime Expiration { get; set; }
    }
}