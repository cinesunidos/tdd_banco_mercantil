﻿using System.Web.Optimization;

namespace CinesUnidos.WebRoles.MVC
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Hoja de JS "scripts.js" - Medianet
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                "~/Scripts/jquery.js",
               "~/Scripts/html5.js",
               "~/Scripts/jquery-1.7.2.min.js",
               "~/Scripts/jquery.selectbox-0.2.min.js",
               "~/Scripts/swipe.js",
               "~/Scripts/msgbox/jquery.msgbox.min.js",
               "~/Scripts/timepicki.js"
               /*"~/Scripts/scripts.js"*/
               ));
            //Hoja de JS "scripts.js" - Medianet
            bundles.Add(new ScriptBundle("~/bundles/scripts_2").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/scripts.min.js"
                ));
            //Archivo JSON que contiene configuración optimizada de la libreria boostrap - Medianet
            bundles.Add(new ScriptBundle("~/bundles/bootstrap_config").Include(
                "~/Scripts/config.json"
                ));

            bundles.Add(new ScriptBundle("~/bundles/fancybox").Include(
               "~/Scripts/fancybox/jquery.fancybox.js",
               "~/Scripts/fancybox/jquery.fancybox-buttons.js",
               "~/Scripts/fancybox/jquery.fancybox-thumbs.js",
               "~/Scripts/fancybox/jquery.easing-1.3.pack.js",
               "~/Scripts/fancybox/jquery.mousewheel-3.0.6.pack.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery2").Include(
                "~/Scripts/jquery-2.1.1.js"));

            //Plugin isotope - Medianet
            bundles.Add(new ScriptBundle("~/bundles/isotope").Include(
                "~/Scripts/jquery.isotope.js"));

            bundles.Add(new ScriptBundle("~/bundles/timepickerjs").Include(
                /*"~/Scripts/timepicki.js"*/));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*",
                "~/Scripts/jquery-ui.min.js",
                "~/Scripts/jquery.ui.datepicker-es.js"
            ));

            //Bundle para la Globalización
            bundles.Add(new ScriptBundle("~/bundles/globalize").Include(
                "~/Scripts/globalize/globalize.js",
                "~/Scripts/globalize/cultures/globalize.culture.es-VE.js"));

            //Bundle de Scripts para las páginas estáticas
            //Por los momentos se usa el CDN
            //Optimizar si en caso de que falle el cdn usar el local
            bundles.Add(new ScriptBundle("~/bundles/staticScripts").Include(
                "~/Scripts/jquery-1.11.0.min.js",
                "~/Scripts/jquery.waitforimages.js",
                "~/Scripts/jquery.isotope.min.js",
                "~/Scripts/modernizr.custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/flipclockjs").Include(
                "~/Scripts/flipclock.js"));

            //Bundle para páginas estáticas
            //Por los momentos se usa el CDN
            //Optimizar si en caso de que falle el cdn usar el local
            bundles.Add(new StyleBundle("~/bundles/bootstrap").Include(
                "~/Content/styles/sections-frmwrk-styles.min.css"));

            //Bundle para páginas estáticas internos
            bundles.Add(new StyleBundle("~/bundles/innerStatic").Include(
                "~/Content/styles/styles_internos_estatic.min.css"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Content/styles/jquery-ui.min.css",
                "~/Content/fancybox/jquery.fancybox-buttons.css",
                "~/Content/fancybox/jquery.fancybox-thumbs.css",
                "~/Content/fancybox/jquery.fancybox.css",
                "~/Scripts/msgbox/jquery.msgbox.css"
            ));
            /*
            bundles.Add(new StyleBundle("~/bundles/timepicker").Include(
                "~/Content/timepicker/timepicki.css",
                new CssRewriteUrlTransform()));
                */

            bundles.Add(new StyleBundle("~/bundles/jqueryui").Include(
                "~/Content/jqueryui/jquery-ui.min.css",
                new CssRewriteUrlTransform()));

            //Hoja de CSS "styles_cinesunidos" - Medianet

            bundles.Add(new StyleBundle("~/bundles/cu").Include(

                //"~/Content/styles/style_comun.css",
                //"~/Content/styles/style_front.css",
                //"~/Content/styles/styles_compra.css",
                //"~/Content/styles/styles_registro.css",
                /*"~/Content/styles/1024x768.css",
                "~/Content/styles/320x480.css",
                "~/Content/styles/360x640.css",
                "~/Content/styles/480x800.css",
                "~/Content/styles/540x960.css",
                "~/Content/styles/600.css",
                "~/Content/styles/640x960.css",
                "~/Content/styles/720x1280.css",
                "~/Content/styles/768x1024.css",
                "~/Content/styles/avengers-preventa.css",*/
                "~/Content/styles/font-awesome.min.css",
                "~/Content/styles/bootstrap.min.css",
                "~/Content/styles/styles_cinesunidos_v0.0.23.css", /*Se agrega versionado para resolver inconveniente con la cache*/
                "~/Content/styles/media_v0.0.3.css"  /*Se agrega versionado para resolver inconveniente con la cache*/
            ));
            BundleTable.EnableOptimizations = false;

            bundles.Add(new StyleBundle("~/Content/Concessions").Include(
          "~/Content/styles/Concessions.css"
          ));


            bundles.Add(new ScriptBundle("~/Script/Concessions").Include(
               //"~/Scripts/Concessions/Concessions.js",
               "~/Scripts/Concessions/order.concessions.js"
               ));
        }
    }
}