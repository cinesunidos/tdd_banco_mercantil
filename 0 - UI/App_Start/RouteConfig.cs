﻿using CinesUnidos.WebRoles.MVC.Controllers;
using System.Web.Mvc;
using System.Web.Routing;

namespace CinesUnidos.WebRoles.MVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            HomeController.RegisterRoutes(routes);
            PurchaseController.RegisterRoutes(routes);
            SecurityController.RegisterRoutes(routes);
            ContactController.RegisterRoutes(routes);
            PagesController.RegisterRoutes(routes);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}