using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Configuration;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using CinesUnidos.ESB.Queue.Configuration;
using CinesUnidos.ESB.Queue.AzureQueue;
using CinesUnidos.ESB.Domain;
namespace CinesUnidos.WorkerRole.Mailing
{
    public class WorkerRole : RoleEntryPoint
    {
        private AzureQueue m_queues;
        private QueueConfiguration c_queus;
        private string altConnectionStrinig;
        private string accountName;
        private string accountKey;
        private string Queues;
        public override void Run()
        {
            while (true)
            {
                #region Execute
                int? countMessage = m_queues.ApproximateMessageCount("services");
                if (countMessage.HasValue)
                {
                    if (countMessage.Value > 0)
                    {
                        MailingServiceEntity mailService = null;
                        try
                        {
                            mailService = m_queues.DequeueMessage<MailingServiceEntity>("services");
                            if (mailService != null)
                            {
                                ServiceEntity services = new ServiceEntity();
                                if (mailService.Tipo.Equals("RecoverPassword"))
                                {
                                    services.Execute<UserEntity>("Send.RecoverPassword", mailService.User);
                                }
                                else if (mailService.Tipo.Equals("Register"))
                                {
                                    services.Execute<UserEntity>("Send.Register", mailService.User);
                                }
                                else if (mailService.Tipo.Equals("Complain"))
                                {
                                    services.Execute<ComplaintEntity>("Send.Complain", mailService.Complain);
                                }
                                else if (mailService.Tipo.Equals("Department"))
                                {
                                    services.Execute<ComplaintEntity>("Send.Department", mailService.Complain);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string err = "Error SendEmail :  " + ex.Message;
                            if (ex.InnerException != null)
                            {
                                err += " Inner Exception: " + ex.InnerException;
                            }
                        }
                    }
                }
                #endregion
            }
        }
        public override bool OnStart()
        {
            altConnectionStrinig = ConfigurationManager.AppSettings["AltConnectionString"]; //Opcional: para usar Azure Storage Emulator
            accountName = ConfigurationManager.AppSettings["AccountName"];
            accountKey = ConfigurationManager.AppSettings["AccountKey"];
            Queues = ConfigurationManager.AppSettings["Queues"];
            c_queus = new QueueConfiguration(accountName, accountKey, Queues, altConnectionStrinig);
            m_queues = new AzureQueue(c_queus);
            ServicePointManager.DefaultConnectionLimit = 12;// Set the maximum number of concurrent connections 
            return base.OnStart();
        }
    }
}
