using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Queue.Configuration;
using CinesUnidos.ESB.Queue.AzureQueue;

namespace CinesUnidos.WorkerRole.Cancelled
{
    public class WorkerRole : RoleEntryPoint
    {
        private AzureQueue  m_queues;
        private QueueConfiguration c_queus;
        private string altConnectionStrinig;
        private string accountName;
        private string accountKey;
        private string Queues;
        public override void Run()
        {
            while (true)
            {
                #region Execute
                int? countMessage = m_queues.ApproximateMessageCount("cancelled");
                if (countMessage.HasValue)
                {
                    if (countMessage.Value > 0)
                    {
                        UserSessionEntity userSession = null;
                        try
                        {
                            userSession = m_queues.DequeueMessage<UserSessionEntity>("cancelled");
                            if (userSession != null)
                            {
                                ServiceEntity services = new ServiceEntity();
                                services.Execute<UserSessionEntity>("Send.Error", userSession);
                            }
                        }
                        catch (Exception ex)
                        {
                            string err = "Error SendEmail :  " + ex.Message;
                            if (ex.InnerException != null)
                            {
                                err += " Inner Exception: " + ex.InnerException;
                            }
                        }
                    }
                }
                #endregion
            }
        }
        public override bool OnStart()
        {
            altConnectionStrinig = ConfigurationManager.AppSettings["AltConnectionString"]; //Opcional: para usar Azure Storage Emulator
            accountName = ConfigurationManager.AppSettings["AccountName"];
            accountKey = ConfigurationManager.AppSettings["AccountKey"];
            Queues = ConfigurationManager.AppSettings["Queues"];
            c_queus = new QueueConfiguration(accountName, accountKey, Queues, altConnectionStrinig);
            m_queues = new AzureQueue(c_queus);
            ServicePointManager.DefaultConnectionLimit = 12;// Set the maximum number of concurrent connections 
            return base.OnStart();
        }
    }
}
