using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Configuration;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using CinesUnidos.ESB.Queue.Configuration;
using CinesUnidos.ESB.Queue.AzureQueue;
using CinesUnidos.ESB.Domain;
namespace CinesUnidos.WorkerRole.Mailing
{
    public class WorkerRole : RoleEntryPoint
    {
        private AzureQueue m_queues;
        private QueueConfiguration c_queus;
        private string altConnectionStrinig;
        private string accountName;
        private string accountKey;
        private string Queues;
        public override void Run()
        {
            while (true)
            {
                #region Execute
                int? countMessage = m_queues.ApproximateMessageCount("purchases");
                if (countMessage.HasValue)
                {
                    if (countMessage.Value > 0)
                    {
                        UserSessionEntity userSession = null;
                        try
                        {
                            userSession = m_queues.DequeueMessage<UserSessionEntity>("purchases");
                            //var ss = m_queues.DequeueMessage<UserSessionEntity>("purchases");
                            //userSession = ss;
                            if (userSession != null)
                            //if (ss != null)
                            {
                                ServiceEntity services = new ServiceEntity();
                                services.Execute<UserSessionEntity>("Send.Purchase", userSession);
                                //services.Execute<UserSessionEntity>("Send.Purchase", ss);
                            }
                        }
                        catch (Exception ex)
                        {
                            string err = "Error SendEmail :  " + ex.Message;
                            if (ex.InnerException != null)
                            {
                                err += " Inner Exception: " + ex.InnerException;
                            }
                        }
                    }
                }
                #endregion
            }
        }
        public override bool OnStart()
        {
            altConnectionStrinig = ConfigurationManager.AppSettings["AltConnectionString"]; //Opcional: para usar Azure Storage Emulator
            accountName = ConfigurationManager.AppSettings["AccountName"];
            accountKey = ConfigurationManager.AppSettings["AccountKey"];
            Queues = ConfigurationManager.AppSettings["Queues"];
            c_queus = new QueueConfiguration(accountName, accountKey, Queues, altConnectionStrinig);
            m_queues = new AzureQueue(c_queus);
            ServicePointManager.DefaultConnectionLimit = 12;// Set the maximum number of concurrent connections 
            return base.OnStart();
        }
    }
}
