using CinesUnidos.WebRole.Mailing.Models;
using Mvc.Mailer;
using System;
using System.Collections.Generic;
using System.IO;
namespace CinesUnidos.WebRole.Mailing.Mailers
{ 
    public class Service : MailerBase, IService
    {
        #region Attributes
        
        #endregion
        #region Contructors
        public Service()
		{
			MasterName="_Layout";            
		}
        #endregion
        #region Methods
        public virtual MvcMailMessage SendRecoverPassword(User user)
		{
            ViewData.Model = user;
            var resources = new Dictionary<string, string>();

            string path = AppDomain.CurrentDomain.BaseDirectory;
            string file = Path.Combine(path, @"Content\img\logo.jpg");

            resources["logo"] = file;
			return Populate(x =>
			{
                x.Subject = "RECUPERACIÓN DE CONTRASEÑA";
                x.ViewName = "SendRecoverPassword";
                x.To.Add(user.Email);
                x.LinkedResources = resources;
			});
        }

        public virtual MvcMailMessage SendCreateUser(User user)
        {
            ViewData.Model = user;
            var resources = new Dictionary<string, string>();

            string path = AppDomain.CurrentDomain.BaseDirectory;
            string file = Path.Combine(path, @"Content\img\logo.jpg");

            resources["logo"] = file;
            return Populate(x =>
            {
                x.Subject = "ACTIVACIÓN DE CUENTA";
                x.ViewName = "SendCreateUser";
                x.To.Add(user.Email);
                x.LinkedResources = resources;
            });
        }
        public virtual MvcMailMessage SendComplain(Complain complain)
        {
            ViewData.Model = complain;
            var resources = new Dictionary<string, string>();

            string path = AppDomain.CurrentDomain.BaseDirectory;
            string file = Path.Combine(path, @"Content\img\logo.jpg");

            resources["logo"] = file;
            return Populate(x =>
            {
                x.Subject = "Gracias por comunicarte con CinesUnidos.com - el cine en un click!";
                x.ViewName = "SendComplain";
                x.To.Add(complain.Email);
                x.LinkedResources = resources;
            });
        }
        public virtual MvcMailMessage SendDepartment(Complain complain)
        {
            ViewData.Model = complain;
            var resources = new Dictionary<string, string>();

            string path = AppDomain.CurrentDomain.BaseDirectory;
            string file = Path.Combine(path, @"Content\img\logo.jpg");

            resources["logo"] = file;
            return Populate(x =>
            {
                x.Subject = complain.Subject;
                x.ViewName = "SendDepartment";
                x.To.Add(complain.Email);                
                x.LinkedResources = resources;
            });
        }
        #endregion
    }
}