using CinesUnidos.WebRole.Mailing.Models;
using Mvc.Mailer;
using System;
namespace CinesUnidos.WebRole.Mailing.Mailers
{ 
    public interface IPurchase
    {
        MvcMailMessage Send(Session userSession);
        MvcMailMessage SendError(Session userSession);
	}
}