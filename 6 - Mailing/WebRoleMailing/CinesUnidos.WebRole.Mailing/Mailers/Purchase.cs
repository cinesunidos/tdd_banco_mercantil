using CinesUnidos.WebRole.Mailing.Models;
using Mvc.Mailer;
using System;
using System.Collections.Generic;
using System.IO;
namespace CinesUnidos.WebRole.Mailing.Mailers
{
    public class Purchase : MailerBase, IPurchase
    {
        #region Attributes

        #endregion
        #region Contructors
        public Purchase()
        {
            MasterName = "_Layout";
        }
        #endregion
        #region Methods
        //      public virtual MvcMailMessage Send(Session userSession)
        //{
        //          ViewData.Model = userSession;
        //          var resources = new Dictionary<string, string>();

        //          string path = AppDomain.CurrentDomain.BaseDirectory;
        //          string file = Path.Combine(path, @"Content\img\logo.jpg");

        //          resources["logo"] = file;
        //	return Populate(x =>
        //	{
        //              x.Subject = "CONFIRMACIÓN DE COMPRA";
        //		x.ViewName = "Send";
        //              x.To.Add(userSession.Email);
        //              x.LinkedResources = resources;
        //	});
        //      }

        public virtual MvcMailMessage Send(Session userSession)
        {
            ViewData.Model = userSession;
            var resources = new Dictionary<string, string>();

            string path = AppDomain.CurrentDomain.BaseDirectory;
            string file = Path.Combine(path, @"Content\img\logo.jpg");

            //string TheaterId = System.Configuration.ConfigurationManager.AppSettings["CinemaQR"].ToString();

            //if (TheaterId.Contains(userSession.TheaterId))
            //{
            string codigoQR = "http://cinesunidosweb.blob.core.windows.net/qrcodes/" + userSession.BookingByte; //se debe tomar el codigo qr con la URL del cloud
            resources["codigoQR"] = codigoQR;
            //}

            //viajan los recuersos dentro del cuerpo del correo.
            resources["logo"] = file;

            return Populate(x =>
            {
                x.Subject = "CONFIRMACIÓN DE COMPRA";
                x.ViewName = "Send";
                x.To.Add(userSession.Email);
                x.LinkedResources = resources;
            });

        }
        public virtual MvcMailMessage SendError(Session userSession)
        {
            ViewData.Model = userSession;
            var resources = new Dictionary<string, string>();

            string path = AppDomain.CurrentDomain.BaseDirectory;
            string file = Path.Combine(path, @"Content\img\logo.jpg");

            resources["logo"] = file;
            return Populate(x =>
            {
                x.Subject = "COMPRA FALLIDA";
                x.ViewName = "SendError";
                x.To.Add(userSession.Email);
                x.LinkedResources = resources;
            });
        }
        #endregion
    }
}