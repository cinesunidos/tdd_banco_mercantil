using CinesUnidos.WebRole.Mailing.Models;
using Mvc.Mailer;
using System;
namespace CinesUnidos.WebRole.Mailing.Mailers
{ 
    public interface IService
    {
        MvcMailMessage SendRecoverPassword(User user);
        MvcMailMessage SendCreateUser(User user);
        MvcMailMessage SendComplain(Complain complain);
        MvcMailMessage SendDepartment(Complain complain);
	}
}