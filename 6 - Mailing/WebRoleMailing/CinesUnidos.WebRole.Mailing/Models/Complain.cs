﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace CinesUnidos.WebRole.Mailing.Models
{
    public class Complain
    {
        #region Properties
        public string Subject { get; set; }
        public string Theater { get; set; }
        public string Movie { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]  
        public DateTime? Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:HH:mm tt}")]  
        public DateTime? Time { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string IDCard { get; set; }
        public string PhoneCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        #endregion
    }
}
