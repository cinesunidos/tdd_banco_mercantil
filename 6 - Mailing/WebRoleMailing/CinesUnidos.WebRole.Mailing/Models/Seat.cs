﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.WebRole.Mailing.Models
{
    public class Seat
    {
        #region Enum
        public enum SeatStatus : int
        {
            Booked = 0,
            House = 1,
            Selected = 2,
            Available = 3,
        }
        public enum SeatType : int
        {
            Chair = 0,
            Aisle = 1,
        }
        #endregion
        #region Properties
        public string AreaCategoryCode { get; set; }
        public int AreaNumber { get; set; }
        public int ColumnIndex { get; set; }
        public string Name { get; set; }
        public int RowIndex { get; set; }
        public SeatStatus Status { get; set; }
        public SeatType Type { get; set; }
        #endregion
    }
}
