﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.WebRole.Mailing.Models
{
    public class Ticket
    {
        #region Properties
        public string Code { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        /// <summary>
        /// Ticket Base Price (Precio base del Ticket)
        /// No Incluye el  IVA
        /// IM: Añadido/Modificado 2016-06-28
        /// </summary>
        public decimal BasePrice { get; set; }

        public decimal BookingFee { get; set; }

        public decimal ServiceFee { get; set; }

        public decimal ServiceFeeWithTax { get; set; }

        public decimal PromotionFee { get; set; }

        public decimal PromotionFeeWithTax { get; set; }

        public decimal PromotionTax { get; set; }

        public decimal TotalPromotionFee { get { return PromotionFee * Quantity; } }

        public decimal Tax { get; set; }

        public int Quantity { get; set; }

        public decimal TotalServiceFee { get { return ServiceFee * Quantity; } }

        public decimal TotalPrice { get { return Price * Quantity; } }

        /// <summary>
        /// Sumatoria de Ticket Base Price (Precio base del Ticket * Cantidad)
        /// No Incluye el  IVA
        /// IM: Añadido/Modificado 2016-06-28
        /// </summary>
        public decimal TotalBasePrice { get { return BasePrice * Quantity; } }

        /// <summary>
        /// Sumatoria de Booking Fee para la cantidad de tickets seleccionados
        /// </summary>
        public decimal TotalBookingFee { get { return BookingFee * Quantity; } }

        /// <summary>
        /// Sumatoria de IVA Completo para la cantidad de tickets seleccionados
        /// </summary>
        public decimal TotalTax { get { return Tax * Quantity; } }

        // IM: Añadido/Modificado 2016-06-28
        // Se cambió TotalPrice por TotalBasePrice para no sumar el IVA del Ticket 2 veces
        // IM: Añadido/Modificado 2016-06-28
        // Se cambió la expresión para usar FullPrice y simplificar
        /// <summary>
        /// Sumatoria de Precio Completo para la cantidad de tickets seleccionados (TotalAmount x Cantidad)
        /// </summary>
        public decimal Total { get { return FullPrice * Quantity; } }

        /// <summary>
        /// Precio Completo de un Ticket incluyendo cargos e IVA
        /// </summary>
        public decimal FullPrice { get; set; }

        /// <summary>
        /// IVA sobre el precio del boleto (si el boleto no supera las 2UT debe ser cero)
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TicketTax { get; set; }

        /// <summary>
        /// IVA sobre el cargo por reservación (Cargo Web)
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal BookingTax { get; set; }

        /// <summary>
        /// IVA sobre el cargo por servicios.
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal ServiceTax { get; set; }

        /// <summary>
        /// IVA sobre el precio del boleto por cantidad de boletos seleccionados (Quantity)
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalTicketTax { get { return TicketTax * Quantity; } }

        /// <summary>
        /// IVA sobre el cargo por reservación (Cargo Web) por cantidad de boletos seleccionados (Quantity)
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalBookingTax { get { return BookingTax * Quantity; } }

        /// <summary>
        /// IVA sobre el cargo por servicios por cantidad de boletos seleccionados (Quantity)
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalServiceTax { get { return ServiceTax * Quantity; } }

        public decimal TotalPromotionTax { get { return PromotionTax * Quantity; } }

        #endregion
    }
}
