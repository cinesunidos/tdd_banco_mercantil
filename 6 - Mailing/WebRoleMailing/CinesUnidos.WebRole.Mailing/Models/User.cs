﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.WebRole.Mailing.Models
{
    public class User
    {
        #region Properties
        public Guid Id { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Address { get; set; }
        public bool ReceiveMassMail { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ActivationDate { get; set; }
        public Boolean Active { get; set; }
        public int Sex { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string IdCard { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public DateTime DateOut { get; set; }
        public DateTime DateLastVisit { get; set; }
        public DateTime DateCurrentVisit { get; set; }
        public string SecretQuestion { get; set; }
        public string SecretAnswer { get; set; }
        public DateTime DateLastUpdate { get; set; }
        public int Zipcode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        #endregion
    }
}
