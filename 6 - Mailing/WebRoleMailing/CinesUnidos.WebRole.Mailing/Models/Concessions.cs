﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace CinesUnidos.WebRole.Mailing.Models
{
    public class Concessions
    {
        public string ItemStrItemID { get; set; }

        public string ItemHOPK1 { get; set; }

        public string ItemHOPK2 { get; set; }

        public string ItemHOPK3 { get; set; }

        public string ItemStrItemDescription { get; set; }

        public decimal ItemPriceDCurPrice { get; set; }

        public string ItemStrItemDescriptionRecipes { get; set; }

        public decimal ItemQuantityStock { get; set; }

        public string ItemStrAbbriation { get; set; }

        public string ItemLocationStrCode { get; set; }

        public string ItemLocationStrDescription { get; set; }

        public decimal ItemStockCurAvailable { get; set; }

        public int ItemQuantityPurchase { get; set; }

        public string ItemStockUOMCode { get; set; }

        public string ItemType { get; set; }

        public string VendorStrCode { get; set; }

        public decimal ItemPurchaseByQuantity
        {
            get
            {
                return ItemPriceDCurPrice * ItemQuantityPurchase;
            }
        }
    }
}
