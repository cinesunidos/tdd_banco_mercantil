﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.WebRole.Mailing.Models
{
    public class Session
    {

        #region Properties
        public Guid Id { get; set; }
        public string TheaterId { get; set; }
        public int SessionId { get; set; }
        public Ticket[] Tickets { get; set; }
        public DateTime TimeOut { get; set; }
        public List<Seat> Seats { get; set; }
        public string Title { get; set; }
        public string Theater { get; set; }
        public DateTime ShowTime { get; set; }
        public string HallName { get; set; }
        public string ClientId { get; set; }

        public Concessions[] Concessions { get; set; }

        public int Quantity
        {
            get
            {
                return Tickets.Sum(t => t.Quantity);
            }
        }

        public decimal TotalPrice
        {
            get
            {
                return Tickets.Sum(t => t.TotalPrice);
            }
        }

        /// <summary>
        /// Precio Base Total de los Tickets (Sin IVA)
        /// IM: Añadido/Modificado 2016-06-28
        /// </summary>
        public decimal TotalBasePrice
        {
            get
            {
                return Tickets.Sum(t => t.TotalBasePrice);
            }
        }

        public decimal TotalBookingFee
        {
            get
            {
                return Tickets.Sum(t => t.TotalBookingFee);
            }
        }

        public decimal TotalServiceFee
        {
            get
            {
                return Tickets.Sum(t => t.TotalServiceFee);
            }
        }
        public decimal TotalPromotionFee
        {
            get
            {
                return Tickets.Sum(t => t.TotalPromotionFee);
            }
        }


        /// <summary>
        /// Suma de cargos por servicios y booking
        /// </summary>
        public decimal TotalServices
        {
            get
            {
                return Tickets.Sum(t => t.TotalServiceFee + t.TotalBookingFee);
            }
        }

        public decimal TotalTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalTax);
            }
        }

        public decimal Total
        {
            get
            {
                return Tickets.Sum(t => t.Total);
            }
        }

        /// <summary>
        /// Sumatoria de TotalTicketTax para todos los tipos de boleto contenidos en la compra
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalTicketTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalTicketTax);
            }
        }

        /// <summary>
        /// Sumatoria de TotalBookingTax para todos los tipos de boleto contenidos en la compra
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalBookingTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalBookingTax);
            }
        }

        /// <summary>
        /// Sumatoria de TotalServiceTax para todos los tipos de boleto contenidos en la compra
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalServiceTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalServiceTax);
            }
        }

        public decimal TotalPromotionTax
        {
            get
            {
                return Tickets.Sum(t => t.TotalPromotionTax);
            }
        }
        public decimal TotalTicketPlusTax
        {
            get
            {
                return TotalBasePrice + TotalTicketTax;
            }
        }

        public decimal TotalBookingPlusTax
        {
            get
            {
                return TotalBookingFee + TotalBookingTax;
            }
        }

        public decimal TotalServicePlusTax
        {
            get
            {
                return TotalServiceFee + TotalServiceTax;
            }
        }
        public decimal TotalPromotionPlusTax
        {
            get
            {
                return TotalPromotionFee + TotalPromotionTax;
            }
        }

        /// <summary>
        /// Suma del monto total de la boleteria y el total carameleria
        /// </summary>
        public decimal TotalPurchase
        {
            get
            {
                return Concessions == null ? Total : Total + TotalConcessions + WebChargeConcessions;
            }
        }
        #region caramelería

        /// <summary>
        /// Cantidad de articulos total de carameleria
        /// </summary>
        public int QuantityConcessions
        {
            get
            {
                return Concessions == null
                ? 0
                : Concessions.Sum(t => t.ItemStrItemID != "999999999" ? t.ItemQuantityPurchase : 0);
            }
        }

        /// <summary>
        /// Sumatoria del precio de la carameleria con iva
        /// </summary>
        public decimal TotalConcessions
        {
            get
            {
                return Concessions == null
                ? 0
                : Concessions.Sum(t => t.ItemStrItemID != "999999999" ? t.ItemPurchaseByQuantity : 0);
            }
        }

        public decimal TaxConcessions
        {
            get
            {
                //return Concessions == null
                //    ? defaul_concessions_value
                //    : TotalConcessions - (TotalConcessions / ((concessions_iva + 100) / 100));

                return Concessions == null
                    ? 0
                    //: TotalConcessions * (PercentageTax / 100);
                    : TotalConcessions - TotalBasePriceConcessions;
            }
        }

        public decimal WebChargeConcessions { get; set; }

        public decimal PercentageTax { get; set; }

        public decimal WebChargeTaxConcessions
        {
            get
            {
                if (WebChargeConcessions <= 0)
                {
                    return 0;
                }
                else
                {
                    return PercentageTax <= 0
                        ? 0
                        //: ((WebChargeConcessions * PercentageTax) / 100);
                        : WebChargeConcessions - WebBaseChargeConcessions;
                }
            }
        }

        public decimal WebBaseChargeConcessions
        {
            get
            {
                if (WebChargeConcessions <= 0)
                {
                    return 0;
                }
                else
                {
                    return PercentageTax <= 0
                        ? 0
                        //: WebChargeConcessions - WebChargeTaxConcessions;
                        : (WebChargeConcessions / (1 + (PercentageTax / 100)));
                }
            }
        }

        public decimal TotalBasePriceConcessions
        {
            get
            {
                return Concessions == null
                    ? 0
                    //: TotalConcessions - TaxConcessions - WebChargeConcessions;
                    //: TotalConcessions - TaxConcessions;
                    : (TotalConcessions / (1 + (PercentageTax / 100))); 
            }
        }
        /// <summary>
        /// ADDED - se modifico este metodo porque deberia retornar solo el iva de la caramelería. y no la suma del iva de carameleria mas el iva de boleteria.
        /// </summary>
        public decimal TotalTaxConcessions
        {
            get
            {
                return Concessions == null
                    ? 0
                    //: TaxConcessions - WebChargeTaxConcessions;
                    : TaxConcessions; //+ WebChargeTaxConcessions;
            }
        }

        public decimal TotalConcessionsWebCharge
        {
            get
            {
                return Concessions == null
                ? 0
                : Concessions.Sum(t => t.ItemPurchaseByQuantity);
            }
        }
        #endregion

        #region Payment
        public int BookingNumber { get; set; }
        public string BookingId { get; set; }
        public string CodeResult { get; set; }
        public string DescriptionResult { get; set; }
        public string[] Voucher { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        #endregion
        public string BookingByte { get; set; }
        //public string BookingConcat { get; set; }
        #endregion

    }
}
