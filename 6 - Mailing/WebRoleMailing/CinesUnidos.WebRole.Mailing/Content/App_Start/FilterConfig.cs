﻿using System.Web;
using System.Web.Mvc;

namespace CinesUnidos.WebRole.Mailing
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}