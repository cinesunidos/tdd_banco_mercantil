﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CinesUnidos.WebRole.Mailing
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "SendPurchase",
               url: "Mailing/SendPurchase",
               defaults: new { controller = "Mailing", action = "SendPurchase"}
           );


            routes.MapRoute(
               name: "SendError",
               url: "Mailing/SendError",
               defaults: new { controller = "Mailing", action = "SendError" }
           );

            routes.MapRoute(
               name: "SendRecoverPassword",
               url: "Mailing/SendRecoverPassword",
               defaults: new { controller = "Mailing", action = "SendRecoverPassword" }
           );

            routes.MapRoute(
               name: "SendRegister",
               url: "Mailing/SendRegister",
               defaults: new { controller = "Mailing", action = "SendRegister" }
           );

            routes.MapRoute(
               name: "SendComplain",
               url: "Mailing/SendComplain",
               defaults: new { controller = "Mailing", action = "SendComplain" }
           );

            routes.MapRoute(
               name: "SendDeparment",
               url: "Mailing/SendDeparment",
               defaults: new { controller = "Mailing", action = "SendDepartment" }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}