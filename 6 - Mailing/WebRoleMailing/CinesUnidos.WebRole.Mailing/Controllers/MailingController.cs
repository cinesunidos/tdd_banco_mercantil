﻿
using CinesUnidos.WebRole.Mailing.Mailers;
using CinesUnidos.WebRole.Mailing.Models;
using Elmah;
using Mvc.Mailer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QRCoder;
using System.IO;
using System.Drawing;
using System.Web.Hosting;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.ServiceRuntime;
namespace CinesUnidos.WebRole.Mailing.Controllers
{
    public class MailingController : Controller
    {
        #region Attributes
        private IPurchase _purchaseMailer;
        private IService _serviceMailer;
        #endregion
        #region Properties
        public IPurchase PurchaseMailer
        {
            get { return _purchaseMailer; }
            set { _purchaseMailer = value; }
        }
        public IService ServiceMailer
        {
            get { return _serviceMailer; }
            set { _serviceMailer = value; }
        }
        #endregion
        #region Constructor
        public MailingController()
        {
            _purchaseMailer = new Purchase();
            _serviceMailer = new Service();
        }
        #endregion
        #region Actions
        [HttpPost]
        public ActionResult SendPurchase(Session userSession)
        {
            try
            {                                
                MvcMailMessage email = _purchaseMailer.Send(userSession);

                email.Send();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Error(ex));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

        }
        [HttpPost]
        public ActionResult SendError(Session userSession)
        {
            try
            {
                MvcMailMessage email = _purchaseMailer.SendError(userSession);

                email.Send();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Error(ex));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

        }
        [HttpPost]
        public ActionResult SendRecoverPassword(User user)
        {
            try
            {
                MvcMailMessage email = _serviceMailer.SendRecoverPassword(user);

                email.Send();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Error(ex));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

        }
        [HttpPost]
        public ActionResult SendRegister(User user)
        {
            try
            {
                MvcMailMessage email = _serviceMailer.SendCreateUser(user);

                email.Send();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Error(ex));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

        }
        [HttpPost]
        public ActionResult SendComplain(Complain complain)
        {
            try
            {
                MvcMailMessage email = _serviceMailer.SendComplain(complain);

                email.Send();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Error(ex));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

        }
        //Aquí se envia el correo electrónico de las quejas al departamento correspondiene
        [HttpPost]
        public ActionResult SendDepartment(Complain complain)
        {
            try
            {
                MvcMailMessage email = _serviceMailer.SendDepartment(complain);

                email.Send();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Error(ex));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

        }
        #endregion
        #region Private Methods
        private string SaveImageToCloudStorage(string userSessionId, string filePath)
        {
            string BlobPath = string.Empty;

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("qrcodes");

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(userSessionId + ".jpeg");

            using (var fileStream = System.IO.File.OpenRead(filePath))
            {
                blockBlob.UploadFromStream(fileStream);
            }


            return BlobPath = userSessionId + ".jpeg";

            //return BlobPath;
        }

        #endregion Private Methods
    }
}
