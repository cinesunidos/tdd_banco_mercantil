﻿using Castle.Core;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CinesUnidos.ESB.Application.Contracts;
using CinesUnidos.ESB.Application.Services;
using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Cache.AzureRedis;
using CinesUnidos.ESB.Cache.Configuration;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Domain.Services;
using CinesUnidos.ESB.Infrastructure.Repositories;
using CinesUnidos.ESB.Queue;
using CinesUnidos.ESB.Queue.AzureQueue;
using CinesUnidos.ESB.Queue.Configuration;
using CinesUnidos.Infrastructure.Repositories;
using System;

namespace CinesUnidos.WebRole.WCF
{
    public class Global : System.Web.HttpApplication
    {
        private IWindsorContainer _container;

        protected void Application_Start(object sender, EventArgs e)
        {
            //Parametro opcional para cambiar el connection string para usar el emulador de azure
            // string altConnectionString = System.Configuration.ConfigurationManager.AppSettings["AltConnectionString"];

            string accountName = System.Configuration.ConfigurationManager.AppSettings["AccountName"];
            string accountKey = System.Configuration.ConfigurationManager.AppSettings["AccountKey"];
            string queues = System.Configuration.ConfigurationManager.AppSettings["Queues"];

            string keyRedisCache = System.Configuration.ConfigurationManager.AppSettings["KeyRedisCache"];
            string hostRedisCache = System.Configuration.ConfigurationManager.AppSettings["HostRedisCache"];
            string sslRedisCache = System.Configuration.ConfigurationManager.AppSettings["sslRedisCache"];

            _container = new WindsorContainer()
            .Register
            (
                Component.For<IQueue>().ImplementedBy<AzureQueue>(),
                Component.For<ICache<PremierEntity[]>>().ImplementedBy<AzureRedisCache<PremierEntity[]>>(),
               Component.For<ICache<TheaterEntity[]>>().ImplementedBy<AzureRedisCache<TheaterEntity[]>>(),
                Component.For<ICache<TheaterMobileEntity[]>>().ImplementedBy<AzureRedisCache<TheaterMobileEntity[]>>(),
                 Component.For<ICache<TheaterBillboardEntity[]>>().ImplementedBy<AzureRedisCache<TheaterBillboardEntity[]>>(),
                 Component.For<ICache<MovieBillboardEntity[]>>().ImplementedBy<AzureRedisCache<MovieBillboardEntity[]>>(),
                Component.For<ICache<UserSessionEntity>>().ImplementedBy<AzureRedisCache<UserSessionEntity>>(),
                Component.For<ICache<CityTicketPriceEntity[]>>().ImplementedBy<AzureRedisCache<CityTicketPriceEntity[]>>(),
                Component.For<ICache<string>>().ImplementedBy<AzureRedisCache<string>>(),
                Component.For<ICache<UserSessionMobileEntity>>().ImplementedBy<AzureRedisCache<UserSessionMobileEntity>>(),
                Component.For<ICache<TicketEntity[]>>().ImplementedBy<AzureRedisCache<TicketEntity[]>>(),

                Component.For<IContactDomainContract>().ImplementedBy<ContactDomainService>(),
                Component.For<IMovieDomainContract>().ImplementedBy<MovieDomainService>(),
                Component.For<IPremiereDomainContract>().ImplementedBy<PremiereDomainService>(),
                Component.For<ITheaterDomainContract>().ImplementedBy<TheaterDomainService>(),
                Component.For<IUserSessionDomainContract>().ImplementedBy<UserSessionDomainService>(),
                Component.For<IBlobsDomainContract>().ImplementedBy<BlobsDomainService>(),
                Component.For<ISecurityDomainContract>().ImplementedBy<SecurityDomainService>(),
                Component.For<ITicketPriceDomainContract>().ImplementedBy<TicketPriceDomainService>(),
                Component.For<IRedisCacheManagementDomainContract>().ImplementedBy<RedisCacheManagementDomainService>(),
                Component.For<IPurchasesDomainContract>().ImplementedBy<PurchasesDomainService>(),
                Component.For<IAppMobileDomainContract>().ImplementedBy<AppMobileDomainService>(),
                Component.For<IContingencyDomainContract>().ImplementedBy<ContingencyDomainService>(),
                Component.For<IConcessionsDomainContract>().ImplementedBy<ConcessionsDomainService>(),
                Component.For<IPromotionDomainContract>().ImplementedBy<PromotionDomainService>(),
                Component.For<IMobilePaymentDomainContract>().ImplementedBy<MobilePaymentDomainService>(),

                Component.For<IContactRepository>().ImplementedBy<ContactRepository>(),
                Component.For<IMovieBillboardRepository>().ImplementedBy<MovieBillboardRepository>(),
                Component.For<IPremiereRepository>().ImplementedBy<PremiereRepository>(),
                Component.For<ITheaterBillboardRepository>().ImplementedBy<TheaterBillboardRepository>(),
                Component.For<ITheaterRepository>().ImplementedBy<TheaterRepository>(),
                Component.For<IUserSessionRepository>().ImplementedBy<UserSessionRepository>(),
                Component.For<ISecurityRepository>().ImplementedBy<SecurityRepository>(),
                Component.For<ITicketPriceRepository>().ImplementedBy<TicketPriceRepository>(),
                Component.For<IRedisCacheManagementRepository>().ImplementedBy<RedisCacheManagementRepository>(),
                Component.For<IAdminRepository>().ImplementedBy<AdminRepository>(),
                Component.For<IPurchasesRepository>().ImplementedBy<PurchasesRepository>(),
                Component.For<IAppMobileRepository>().ImplementedBy<AppMobileRepository>(),
                Component.For<IContingencyRepository>().ImplementedBy<ContingencyRepository>(),
                Component.For<IPromotionRepository>().ImplementedBy<PromotionRepository>(),
                Component.For<IConcessionsRepository>().ImplementedBy<ConcessionsRepository>(),
                Component.For<IMobilePaymentRepository>().ImplementedBy<MobilePaymentRepository>(),
                Component.For<IBlobsRepository>().ImplementedBy<BlobsRepository>()
                
                .DependsOn(
                    Parameter.ForKey("accountName").Eq(accountName),
                    Parameter.ForKey("accountKey").Eq(accountKey)
                ),
                Component.For<IQueueConfiguration>().ImplementedBy<QueueConfiguration>()
                 .DependsOn(
                    Parameter.ForKey("accountName").Eq(accountName),
                    Parameter.ForKey("accountKey").Eq(accountKey),
                    Parameter.ForKey("queues").Eq(queues)//,
                                                         //  Parameter.ForKey("altConnectionString").Eq(altConnectionString)
                ),
                Component.For<ICacheConfiguration>().ImplementedBy<CacheConfiguration>()
                 .DependsOn(
                    Parameter.ForKey("connectionString").Eq(hostRedisCache),
                    Parameter.ForKey("password").Eq(keyRedisCache),
                    Parameter.ForKey("sslRedisCache").Eq(sslRedisCache)
                )
            );
            _container.AddFacility<WcfFacility>()
            .Register
            (
                Component.For<LoggingInterceptor>().ImplementedBy<LoggingInterceptor>(),
                Component.For<IContact>().ImplementedBy<ContactServices>().Named("ContactServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IImages>().ImplementedBy<ImagesServices>().Named("ImagesServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IMovie>().ImplementedBy<MovieServices>().Named("MovieServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IPremier>().ImplementedBy<PremierServices>().Named("PremierServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IUserSession>().ImplementedBy<UserSessionServices>().Named("UserSessionServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<ITheater>().ImplementedBy<TheaterServices>().Named("TheaterServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<ISecurity>().ImplementedBy<SecurityServices>().Named("SecurityServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<ITicket>().ImplementedBy<TicketServices>().Named("TicketServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IRedisCacheManagement>().ImplementedBy<RedisCacheManagementServices>().Named("RedisCacheManagementServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IAdmin>().ImplementedBy<AdminServices>().Named("AdminServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IPurchases>().ImplementedBy<PurchasesServices>().Named("PurchasesServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IAppMobile>().ImplementedBy<AppMobileServices>().Named("AppMobileServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IPromotion>().ImplementedBy<PromotionServices>().Named("PromotionServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IConcessions>().ImplementedBy<ConcessionsServices>().Named("ConcessionsServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IContingency>().ImplementedBy<ContingencyServices>().Named("ContingencyServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere,
                Component.For<IMobilePayment>().ImplementedBy<MobilePaymentServices>().Named("MobilePaymentServices").Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere
                );
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-VE");
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (_container != null)
                _container.Dispose();
        }
    }
}