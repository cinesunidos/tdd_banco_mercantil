﻿using Castle.DynamicProxy;
using Elmah;
using System;

namespace CinesUnidos.WebRole.WCF
{
    public class LoggingInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Error(ex));
            }
        }
    }
}