﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class SectionEntity
    {
        public SectionEntity()
        {
        }
        #region Properties

        [IgnoreDataMember]
        public Guid Id { get; set; }
        [DataMember(Order = 0)]
        public string Name { get; set; }
        [DataMember(Order = 1)]
        public string PlatformName { get; set; }
        [DataMember(Order = 2)]
        public List<ItemEntity> Items { get; set; }
        #endregion Properties
    }
}