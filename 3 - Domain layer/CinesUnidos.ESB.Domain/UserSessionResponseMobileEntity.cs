﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain { 
[DataContract]
[Serializable]
public class UserSessionResponseMobileEntity
{
    #region Properties

    [DataMember]
    public string TheaterId { get; set; }

    [DataMember]
    public int SessionId { get; set; }

    [DataMember]
    public Guid Id { get; set; }

    [DataMember]
    public DateTime TimeOut { get; set; }

    [DataMember]
    public string Title { get; set; }

    [DataMember]
    public string Theater { get; set; }

    [DataMember]
    public string HallName { get; set; }

    [DataMember]
    public DateTime ShowTime { get; set; }

    [DataMember]
    public TicketEntity[] Tickets { get; set; }

    [DataMember]
    public string Seats { get; set; }

    #region Payment

    [DataMember]
    public int BookingNumber { get; set; }

    [DataMember]
    public string CodeResult { get; set; }

    [DataMember]
    public string DescriptionResult { get; set; }

    [DataMember]
    public string[] Voucher { get; set; }

    [DataMember]
    public string Name { get; set; }

    #endregion Payment

    #region Error

    [DataMember]
    public bool WithError { get; set; }

    [DataMember]
    public string ErrorMessage { get; set; }

    #endregion Error

    #endregion Properties

    #region Methods

    public UserSessionResponseMobileEntity()
    {
        WithError = false;
        ErrorMessage = string.Empty;
    }

    public static implicit operator UserSessionResponseMobileEntity(UserSessionMobileEntity userSession)
    {
        UserSessionResponseMobileEntity userSessionRS = new UserSessionResponseMobileEntity();
        userSessionRS.TheaterId = userSession.TheaterId;
        userSessionRS.SessionId = userSession.SessionId;
        userSessionRS.TimeOut = userSession.TimeOut;
        userSessionRS.Id = userSession.Id;
        userSessionRS.Tickets = userSession.Tickets;

        userSessionRS.Seats = userSession.Seats;
        userSessionRS.ShowTime = userSession.ShowTime;
        userSessionRS.Title = userSession.Title;
        userSessionRS.Theater = userSession.Theater;
        userSessionRS.HallName = userSession.HallName;

        userSessionRS.BookingNumber = userSession.BookingNumber;
        userSessionRS.CodeResult = userSession.CodeResult;
        userSessionRS.DescriptionResult = userSession.DescriptionResult;
        userSessionRS.Voucher = userSession.Voucher;
        userSessionRS.Name = userSession.Name;

        return userSessionRS;
    }

    #endregion Methods
}
}