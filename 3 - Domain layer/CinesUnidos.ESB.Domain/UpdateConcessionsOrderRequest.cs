﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class UpdateConcessionsOrderRequest
    {

        [DataMember]
        public string TheaterId { get; set; }

        [DataMember]
        public string TransIdTemp { get; set; }

        [DataMember]
        public string TransactionNumber { get; set; }

        [DataMember]
        public string BookingNumber { get; set; }

        [DataMember]
        public bool PaymentStart { get; set; }

        [DataMember]
        public bool PaymentOK { get; set; }

        [DataMember]
        public bool OrderComplete { get; set; }

        [DataMember]
        public bool OrderCancel { get; set; }

    }

}
