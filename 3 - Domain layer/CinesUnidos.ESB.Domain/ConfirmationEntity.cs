﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ConfirmationEntity
    {
        #region Properties

        [DataMember]
        public Guid UserSessionId { get; set; }

        [DataMember]
        public SeatEntity[] Seats { get; set; }

        [DataMember]
        public ConcessionsEntity[] Concessions { get; set; }

        [DataMember]
        public ConcessionsEntity BookingFee { get; set; }

        #endregion Properties
    }
}