﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class UserSessionResponseEntity
    {
        #region Properties

        [DataMember]
        public string TheaterId { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public DateTime TimeOut { get; set; }
        
        [DataMember]
        public DateTime PaymentTimeOut { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Theater { get; set; }

        [DataMember]
        public string HO { get; set; }

        [DataMember]
        public string HallName { get; set; }

        [DataMember]
        public DateTime ShowTime { get; set; }

        [DataMember]
        public TicketEntity[] Tickets { get; set; }

        [DataMember]
        public MapEntity Map { get; set; }

        [DataMember]
        public SeatEntity[] Seats { get; set; }

        [DataMember]
        public string TransIdTemp { get; set; }

        [DataMember]
        public ConcessionsEntity[] Concessions { get; set; }

        [DataMember]
        public string BookingByte { get; set; }

        [DataMember]
        public ConcessionsEntity BookingFee { get; set; }
        [DataMember]
        public decimal WebChargeConcessions { get; set; }        
        [DataMember]
        public decimal PercentageTax { get; set; }

        [DataMember]
        public bool ValidConcessions { get; set; }
        #region Payment

        [DataMember]
        public int BookingNumber { get; set; }
        [DataMember]
        public string BookingId { get; set; }

        [DataMember]
        public string CodeResult { get; set; }

        [DataMember]
        public string DescriptionResult { get; set; }

        [DataMember]
        public string[] Voucher { get; set; }

        [DataMember]
        public string Name { get; set; }

        //[DataMember]
        //public string BookingConcat { get; set; }

        #endregion Payment

        #region Error

        [DataMember]
        public bool WithError { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        #endregion Error

        #endregion Properties

        #region Methods

        public UserSessionResponseEntity()
        {
            WithError = false;
            ErrorMessage = string.Empty;
        }

        public static implicit operator UserSessionResponseEntity(UserSessionEntity userSession)
        {
            UserSessionResponseEntity userSessionRS = new UserSessionResponseEntity();
            userSessionRS.TheaterId = userSession.TheaterId;
            userSessionRS.SessionId = userSession.SessionId;
            userSessionRS.TimeOut = userSession.TimeOut;
            userSessionRS.PaymentTimeOut = userSession.PaymentTimeOut;
            userSessionRS.Id = userSession.Id;
            userSessionRS.Tickets = userSession.Tickets;
            userSessionRS.Seats = userSession.Seats.ToArray();
            userSessionRS.TransIdTemp = userSession.TransIdTemp;
            userSessionRS.Concessions = userSession.Concessions.ToArray();
            userSessionRS.ShowTime = userSession.ShowTime;
            userSessionRS.Title = userSession.Title;
            userSessionRS.Theater = userSession.Theater;
            userSessionRS.HallName = userSession.HallName;
            userSessionRS.BookingFee = userSession.BookingFee;
            userSessionRS.HO = userSession.HO;
            userSessionRS.ValidConcessions = userSession.ValidConcessions;

            userSessionRS.BookingNumber = userSession.BookingNumber;
            userSessionRS.CodeResult = userSession.CodeResult;
            userSessionRS.DescriptionResult = userSession.DescriptionResult;
            userSessionRS.Voucher = userSession.Voucher;
            userSessionRS.Name = userSession.Name;
            //23/05/2017,LUIS RAMIREZ, Agregar campo bookingByte que almacena el nombre del archivo de la imagen qr
            userSessionRS.BookingByte = userSession.BookingByte;
            userSessionRS.BookingId = userSession.BookingId;
            // userSessionRS.BookingConcat = userSession.BookingConcat;
            userSessionRS.PercentageTax = userSession.PercentageTax;
            userSessionRS.WebChargeConcessions = userSession.WebChargeConcessions;

            return userSessionRS;
        }

        #endregion Methods
    }
}