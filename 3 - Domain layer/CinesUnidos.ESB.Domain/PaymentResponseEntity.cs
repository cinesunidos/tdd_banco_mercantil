﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class PaymentResponseEntity
    {
        #region Properties

        [DataMember]
        public int BookingNumber { get; set; }
        [DataMember]
        public string BookingId { get; set; }

        [DataMember]
        public string CodeResult { get; set; }

        [DataMember]
        public string DescriptionResult { get; set; }

        [DataMember]
        public string[] Voucher { get; set; }

        [DataMember]
        public ConfirmationResponseEntity Confirmation { get; set; }     
        
        [DataMember]
        public int TransactionNumber { get; set; }

        [DataMember]
        public string Name { get; set; }

        #endregion Properties
    }
}