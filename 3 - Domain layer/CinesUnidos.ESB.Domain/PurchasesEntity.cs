﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class PurchasesEntity : ICloneable
    {
        #region Properties

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string BookingNumber { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public string UserTypeId { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string FilmCode { get; set; }

        [DataMember]
        public string FilmTitle { get; set; }

        [DataMember]
        public string CinemaCode { get; set; }

        [DataMember]
        public string CinemaTitle { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public int Tickets { get; set; }

        [DataMember]
        public string SessionOBJ { get; set; }
        #endregion Properties

        #region Methods

        public PurchasesEntity Clone()
        {
            PurchasesEntity p = new PurchasesEntity();
            p.Id = this.Id;
            p.Amount = this.Amount;
            p.Date = this.Date;
            p.CinemaCode = this.CinemaCode;
            p.CinemaTitle = this.CinemaTitle;
            p.FilmCode = this.FilmCode;
            p.FilmTitle = this.FilmTitle;
            p.Tickets = this.Tickets;
            p.UserId = this.UserId;
            p.BookingNumber = this.BookingNumber;
            p.SessionId = this.SessionId;
            p.SessionOBJ = this.SessionOBJ;
            return p;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion Methods
    }
}