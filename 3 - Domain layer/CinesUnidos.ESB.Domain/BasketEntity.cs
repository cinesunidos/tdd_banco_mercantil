﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class BasketEntity
    {
        #region Properties

        [DataMember]
        public string TheaterId { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public string MovieId { get; set; }

        [DataMember]
        public TicketEntity[] Tickets { get; set; }

        #endregion Properties

        #region Methods

        public void Clear()
        {
            List<TicketEntity> list = Tickets.ToList();
            int count = list.RemoveAll(t => t.Quantity <= 0);
            if (count > 0)
                Tickets = list.ToArray();
        }

        #endregion Methods
    }
}