﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    public class PromotionEntity
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Image { get; set; }
        //[DataMember]
        //public bool Active { get; set; }
        //[DataMember]
        //public string Date { get; set; }
        [DataMember]
        public string StartDate { get; set; }
        [DataMember]
        public string ExpirationDate { get; set; }
        [DataMember]
        public string Size { get; set; }
    }
    [DataContract]
    public class PromotionPackageEntity
    {
        [DataMember]
        public string ListPromotions { get; set; }
        [DataMember]
        public string Token { get; set; }
    }
}
