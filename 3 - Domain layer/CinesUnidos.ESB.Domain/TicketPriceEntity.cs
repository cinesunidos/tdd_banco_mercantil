﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class TicketPriceEntity
    {
        [DataMember]
        public string ticketName { set; get; }
        [DataMember]
        public string formatMovie { set; get; }
        [DataMember]
        public string typeHall { set; get; }
        [DataMember]
        public decimal price { set; get; }
        [DataMember]
        public decimal fee { set; get; }


    }
    [DataContract]
    [Serializable]
    public class CityTicketPriceEntity
    {
        [DataMember]
        public string city { set; get; }
        [DataMember]
        public DateTime date { set; get; }
        [DataMember]
        public List<TheaterTicketPriceEntity> theaterTicketPrice { set; get; }
        public CityTicketPriceEntity()
        {
            theaterTicketPrice = new List<TheaterTicketPriceEntity>();
        }

    }
    [DataContract]
    [Serializable]
    public class TheaterTicketPriceEntity
    {
        [DataMember]
        public string theaterID { set; get; }
        [DataMember]
        public string name { set; get; }
        [DataMember]
        public List<TicketPriceEntity> listTicketPrice { set; get; }
        public TheaterTicketPriceEntity()
        {
            listTicketPrice = new List<TicketPriceEntity>();
        }
    }

}