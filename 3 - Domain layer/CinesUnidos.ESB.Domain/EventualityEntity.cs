﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class EventualityEntity
    {
        #region Properties
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public string Creation { get; set; }
        [DataMember]
        public string Finish { get; set; }
        [DataMember]
        public int TheaterCode { get; set; }
        [DataMember]
        public int MessageCode { get; set; }
        [DataMember]
        public bool IsValid { get; set; }
        [DataMember]
        public List<string> Messages { get; set; }
        #endregion Properties
    }
}
