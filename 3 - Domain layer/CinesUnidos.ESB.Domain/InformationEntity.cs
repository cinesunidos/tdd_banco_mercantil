﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class InformationEntity : ICloneable
    {
        #region Properties

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string Class { get; set; }

        [DataMember]
        public string Subtitle { get; set; }

        [DataMember]
        public string Format { get; set; }

        [DataMember]
        public int Time { get; set; }

        [DataMember]
        public string Trailer { get; set; }

        [DataMember]
        public string Web { get; set; }

        #endregion Properties

        #region Methods

        public InformationEntity Clone()
        {
            InformationEntity information = new InformationEntity();
            information.Class = this.Class;
            information.Format = this.Format;
            information.Gender = this.Gender;
            information.Subtitle = this.Subtitle;
            information.Time = this.Time;
            information.Trailer = this.Trailer;
            information.Web = this.Web;
            return information;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion Methods
    }
}