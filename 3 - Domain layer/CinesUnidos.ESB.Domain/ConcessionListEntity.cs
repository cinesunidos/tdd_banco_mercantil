﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ConcessionListEntity
    {
        [DataMember]
        public List<ConcessionsEntity> Items;
        [DataMember]
        public ConcessionsEntity BookingFee;
    }
}
