﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class MovieBillboardEntity //: ICloneable
    {
        #region Properties

        [DataMember]
        public MovieEntity Movie { get; set; }

        [DataMember]
        public List<MovieBillboardTheaterEntity> Theaters { get; set; }

        [DataMember]
        public DateTime[] Dates { get; set; }

        #endregion Properties

        #region Methods

        public MovieBillboardEntity()
        {
            Theaters = new List<MovieBillboardTheaterEntity>();
        }

        //public void Add(MovieBillboardTheaterEntity theater)
        //{
        //    if (Theaters != null)
        //    {
        //        MovieBillboardTheaterEntity temp = Theaters.Where(t => t.Theater.Id.Equals(theater.Theater.Id)).FirstOrDefault();
        //        if (temp != null)
        //            temp.Sessions.AddRange(theater.Sessions);
        //        else
        //            Theaters.Add(theater);
        //    }
        //    else
        //    {
        //        Theaters = new List<MovieBillboardTheaterEntity>();
        //        Theaters.Add(theater);
        //    }
        //}

        public void GetDates()
        {
            List<DateTime> list = new List<DateTime>();
            foreach (var item in Theaters)
            {
                list.AddRange(item.GetDates());
            }

            Dates = list.Distinct().OrderBy(d => d).ToArray();
        }

        //public MovieBillboardEntity Clone()
        //{
        //    MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
        //    movieBillboard.Movie = this.Movie.Clone();
        //    List<DateTime> dates = new List<DateTime>();
        //    foreach (var date in this.Dates)
        //    {
        //        dates.Add(date);
        //    }
        //    movieBillboard.Dates = dates.ToArray();
        //    return movieBillboard;
        //}

        //object ICloneable.Clone()
        //{
        //    return Clone();
        //}

        #endregion Methods
    }
}