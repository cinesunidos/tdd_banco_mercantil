﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class UserEntity
    {
        #region Properties

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public bool ReceiveMassMail { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public DateTime? ActivationDate { get; set; }

        [DataMember]
        public Boolean Active { get; set; }

        [DataMember]
        public int Sex { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string IdCard { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string MobilePhone { get; set; }

        [DataMember]
        public DateTime DateOut { get; set; }

        [DataMember]
        public DateTime DateLastVisit { get; set; }

        [DataMember]
        public DateTime DateCurrentVisit { get; set; }

        [DataMember]
        public string SecretQuestion { get; set; }

        [DataMember]
        public string SecretAnswer { get; set; }

        [DataMember]
        public DateTime DateLastUpdate { get; set; }

        [DataMember]
        public int Zipcode { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Twitter { get; set; }

        [DataMember]
        public string Facebook { get; set; }

        [DataMember]
        public string Instagram { get; set; }

        #endregion Properties
    }
}