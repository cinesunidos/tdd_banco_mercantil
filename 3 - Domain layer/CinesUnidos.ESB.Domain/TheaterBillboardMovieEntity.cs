﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class TheaterBillboardMovieEntity
    {
        #region Properties

        [DataMember]
        public MovieEntity Movie { get; set; }

        [DataMember]
        public List<SessionEntity> Sessions { get; set; }

        #endregion Properties

        #region Methods

        public TheaterBillboardMovieEntity()
        {
            Sessions = new List<SessionEntity>();
        }

        internal DateTime[] GetDates()
        {
            DateTime[] dates = (from d in Sessions
                                select new DateTime(d.ShowTime.Year, d.ShowTime.Month, d.ShowTime.Day)).Distinct().ToArray();
            return dates;
        }

        #endregion Methods
    }
}