﻿using System;

namespace CinesUnidos.ESB.Domain
{
    public class PasswordEntity
    {
        public Guid UserId { get; set; }

        public string ActualPassword { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmationPassword { get; set; }
    }
}