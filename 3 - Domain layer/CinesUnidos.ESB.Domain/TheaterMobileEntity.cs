﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class TheaterMobileEntity : ICloneable

    {
        #region Properties

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public double Longitude { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public FacilityEntity[] Facilities { get; set; }

        [DataMember]
        public HallEntity[] Halls { get; set; }

        #endregion Properties

        #region Methods

        public TheaterMobileEntity Clone()
        {
            TheaterMobileEntity theater = new TheaterMobileEntity();
            theater.Id = this.Id;
            theater.Name = this.Name;
            theater.Address = this.Address;
            theater.Latitude = this.Latitude;
            theater.Longitude = this.Longitude;
            theater.City = this.City;
            theater.Phone = this.Phone;
            List<FacilityEntity> facilities = new List<FacilityEntity>();
            foreach (var item in this.Facilities)
            {
                facilities.Add(item.Clone());
            }
            theater.Facilities = facilities.ToArray();
            List<HallEntity> halls = new List<HallEntity>();
            foreach (var item in this.Halls)
            {
                halls.Add(item.Clone());
            }
            theater.Halls = halls.ToArray();
            return theater;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion Methods
    }
}
