﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace CinesUnidos.ESB.Domain
{
    public class Serializer
    {
        /// <summary>
        /// Este metodo serializa un objeto cualquiera y almacena el resultado en la ruta indicada
        /// </summary>
        /// <param name="DataObject">Objeto que se va a serialize</param>
        /// <param name="Path">Ruta donde se va a guardar el resultado</param>
        public void Serialize<T>(T DataObject, string Path)
        {
            try
            {
                XmlRootAttribute root = new XmlRootAttribute();
                XmlSerializer serializer = new XmlSerializer(DataObject.GetType(), root);
                using (Stream fs = new FileStream(Path, FileMode.Create))
                {
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.OmitXmlDeclaration = true;
                    settings.CloseOutput = false;
                    XmlWriter writer = XmlWriter.Create(fs, settings);
                    serializer.Serialize(writer, DataObject);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                string msg = (ex.InnerException != null) ? msg = ex.InnerException.Message : ex.Message;
                throw new Exception(msg);
            }
        }
        /// <summary>
        /// Este metodo serializa un objeto cualquiera y almacena el resultado en un string
        /// </summary>
        /// <param name="DataObject">Objeto que se va a serialize</param>
        /// <returns>Objeto serializado</returns>
        public string Serialize<T>(T DataObject)
        {
            string m = String.Empty;
            try
            {
                MemoryStream ms = new MemoryStream();
                XmlRootAttribute root = new XmlRootAttribute();
                XmlSerializer serializer = new XmlSerializer(DataObject.GetType(), root);
                using (ms)
                {
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.OmitXmlDeclaration = true;
                    settings.CloseOutput = false;
                    XmlWriter writer = XmlWriter.Create(ms, settings);
                    serializer.Serialize(writer, DataObject);
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms, new UTF8Encoding());
                    m = sr.ReadToEnd();
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                string msg = (ex.InnerException != null) ? msg = ex.InnerException.Message : ex.Message;
                throw new Exception(msg);
            }
            return m;
        }


        public object Deserialize(Type Type, string Path)
        {
            object deserializedobject = null;
            try
            {
                XmlRootAttribute root = new XmlRootAttribute();
                XmlSerializer serializer = new XmlSerializer(Type, root);
                if (System.IO.File.Exists(Path))
                {
                    using (FileStream fs = new FileStream(Path, FileMode.Open, FileAccess.Read))
                    {
                        XmlReader reader = new XmlTextReader(fs);
                        deserializedobject = serializer.Deserialize(reader);
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = (ex.InnerException != null) ? msg = ex.InnerException.Message : ex.Message;
                throw new Exception(msg);
            }
            return deserializedobject;
        }


        public object DeserializeFromMemory(string DataObject, Type Type)
        {
            object deserializedobject = null;
            try
            {
                XmlRootAttribute root = new XmlRootAttribute();
                XmlSerializer serializer = new XmlSerializer(Type, root);
                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(DataObject)))
                {
                    XmlReader reader = new XmlTextReader(ms);
                    deserializedobject = serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                string msg = (ex.InnerException != null) ? msg = ex.InnerException.Message : ex.Message;
                throw new Exception(msg);
            }
            return deserializedobject;
        }
    }
}