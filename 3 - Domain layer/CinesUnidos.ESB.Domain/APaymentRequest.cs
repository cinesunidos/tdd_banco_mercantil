﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain
{
    public class APaymentRequest
    {
        public string Amount { get; set; }
        public string Client_UserVippo { get; set; }
        public string Client_PasswordVippo { get; set; }
    }
}
