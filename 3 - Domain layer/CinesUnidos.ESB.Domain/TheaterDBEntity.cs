﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class TheaterDBEntity
    {
        [DataMember]
        public Guid Id { get; set; }
        #region Properties
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsValid { get; set; }
        [DataMember]
        public List<string> Messages { get; set; }
        #endregion Properties
    }
}
