﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{

    [DataContract]
    [Serializable]
    public class ConcessionsRSEntity
    {

        #region Properties

        [DataMember]
        public ConcessionOrderEntity Order { get; set; }

        public string TransIdTemp
        {
            get
            {
                if( Order == null )
                {
                    return "";
                }
                return Order.TransIdTemp;
            }
            set
            {
                if( Order != null )
                {
                    Order.TransIdTemp = value;
                }
            }
        }

        public ConcessionsEntity BookingFee
        {
            get
            {
                return Order.BookingFee;
            }
        }

        [DataMember]
        public bool WithError { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        #endregion

    }
}
