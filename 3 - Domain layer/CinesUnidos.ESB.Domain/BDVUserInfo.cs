﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain
{
    public class BDVUserInfo
    {
        public string ClientId { get; set; }
        public string IdCardType { get; set; }
        public string IdCardNumber { get; set; }
        public string Email { get; set; }
        public string Cellphone { get; set; }
    }
}
