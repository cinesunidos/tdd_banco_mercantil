﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class SessionInfoEntity
    {
        #region Properties

        [DataMember]
        public MovieEntity Movie { get; set; }

        [DataMember]
        public TheaterEntity Theater { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string HallName { get; set; }

        [DataMember]
        public int MaxSeatsPerTransaction { get; set; }

        [DataMember]
        public bool SeatAllocation { get; set; }

        [DataMember]
        public int SeatsAvailable { get; set; }

        [DataMember]
        public bool OnSale { get; set; }

        [DataMember]
        public DateTime ShowTime { get; set; }

        [DataMember]
        public TicketEntity[] Tickets { get; set; }

        #endregion Properties

        #region Methods

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ Theater.Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            SessionInfoEntity entity = obj as SessionInfoEntity;
            if (obj != null)
            {
                if (entity.GetHashCode().Equals(this.GetHashCode()))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        #endregion Methods
    }
}