﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain
{
    public class ResponseEntity
    {
        public Boolean isOk { get; set; }

        public Dictionary<string, object> Dictionary { get; set; }

        public string CodResponse { get; set; }

        public string MsgResponse { get; set; }
    }
}