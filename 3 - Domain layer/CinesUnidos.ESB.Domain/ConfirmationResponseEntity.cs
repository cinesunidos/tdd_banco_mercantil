﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ConfirmationResponseEntity
    {
        #region Properties

        [DataMember]
        public string TheaterId { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public DateTime TimeOut { get; set; }

        [DataMember]
        public DateTime PaymentTimeOut { get; set; }
        
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Theater { get; set; }

        [DataMember]
        public string HO { get; set; }

        [DataMember]
        public DateTime ShowTime { get; set; }

        [DataMember]
        public TicketEntity[] Tickets { get; set; }

        [DataMember]
        public SeatEntity[] Seats { get; set; }

        [DataMember]
        public ConcessionsEntity[] Concessions { get; set; }

        [DataMember]
        public bool IsValid { get; set; }

        [DataMember]
        public decimal WebChargeConcessions { get; set; }

        //Porcentaje IVA
        [DataMember]
        public decimal PercentageTax { get; set; }
        #endregion Properties

        #region Methods

        public static implicit operator ConfirmationResponseEntity(UserSessionEntity userSession)
        {
            ConfirmationResponseEntity confirmationRS = new ConfirmationResponseEntity();
            confirmationRS.TheaterId = userSession.TheaterId;
            confirmationRS.SessionId = userSession.SessionId;
            confirmationRS.TimeOut = userSession.TimeOut;
            confirmationRS.PaymentTimeOut = userSession.PaymentTimeOut;

            confirmationRS.Id = userSession.Id;
            confirmationRS.Tickets = userSession.Tickets;

            confirmationRS.Seats = userSession.Seats.ToArray();
            confirmationRS.Concessions = userSession.Concessions.ToArray();
            confirmationRS.ShowTime = userSession.ShowTime;
            confirmationRS.Title = userSession.Title;
            confirmationRS.Theater = userSession.Theater;
            confirmationRS.HO = userSession.HO;
            return confirmationRS;
        }

        #endregion Methods
    }
}