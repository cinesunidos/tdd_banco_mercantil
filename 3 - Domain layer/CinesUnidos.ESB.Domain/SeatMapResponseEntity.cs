﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class SeatMapResponseEntity
    {
        [DataMember]
        public string SeatData { get; set; }
        [DataMember]
        public TicketEntity[] Tickets { get; set; }
    }
}
