﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class UserSessionMobileEntity
    {
        #region Properties

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string TheaterId { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public TicketEntity[] Tickets { get; set; }

        [DataMember]
        public DateTime TimeOut { get; set; }

        [DataMember]
        public string Seats { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Theater { get; set; }

        [DataMember]
        public DateTime ShowTime { get; set; }

        [DataMember]
        public string HallName { get; set; }

        [DataMember]
        public string ClientId { get; set; }

        public decimal TotalPrice
        {
            get
            {
                return Tickets.Sum(t => t.Price * t.Quantity);
            }
        }
        /// <summary>
        /// Sumatoria de Ticket Base Price (Precio base del Ticket * Cantidad)
        /// No Incluye el  IVA
        /// IM: Añadido/Modificado 2016-06-28
        /// </summary>
        public decimal TotalBasePrice
        {
            get
            {
                return Tickets.Sum(t => t.BasePrice * t.Quantity);
            }
        }

        public decimal TotalBookingFee
        {
            get
            {
                return Tickets.Sum(t => t.BookingFee * t.Quantity);
            }
        }

        public decimal TotalServiceFee
        {
            get
            {
                return Tickets.Sum(t => t.ServiceFee * t.Quantity);
            }
        }

        public decimal TotalTax
        {
            get
            {
                return Tickets.Sum(t => t.Tax * t.Quantity);
            }
        }


        public decimal Total
        {
            get
            {
                return TotalBasePrice + TotalBookingFee + TotalTax + TotalServiceFee;
            }
        }

        #region Payment

        [DataMember]
        public int BookingNumber { get; set; }
        //Localizador Alfanúmerico
        [DataMember]
        public string BookingId { get; set; }

        [DataMember]
        public string CodeResult { get; set; }

        [DataMember]
        public string DescriptionResult { get; set; }

        [DataMember]
        public string[] Voucher { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Email { get; set; }

        #endregion Payment

        #endregion Properties
    }
}
