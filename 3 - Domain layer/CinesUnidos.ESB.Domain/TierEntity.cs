﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class TierEntity
    {
        #region Properties

        [DataMember]
        public char Name { get; set; }

        [DataMember]
        public SeatEntity[] Seats { get; set; }

        #endregion Properties
    }
}