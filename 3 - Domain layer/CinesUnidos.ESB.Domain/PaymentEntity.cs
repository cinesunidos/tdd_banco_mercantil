﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class PaymentEntity
    {
        #region Enum

        [DataContract]
        public enum TypeCreditCard : int
        {
            [EnumMember]
            Visa = 0,

            [EnumMember]
            Master = 1,

            [EnumMember]
            Amex = 2,

            [EnumMember]
            PAGOVIPPO = 5,

            [EnumMember]
            PAGOVZLATDD = 6,

            [EnumMember]
            PAGOVZLATDC = 7,

            [EnumMember]
            PAGOMERCTDD = 8,

            [EnumMember]
            PAGOMERCTDC = 9
        }

        #endregion Enum

        #region Properties

        [DataMember]
        public Guid UserSessionId { get; set; }

        [DataMember]
        public TypeCreditCard Type { get; set; }

        [DataMember]
        public int Month { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public string SecurityCode { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string CertificateType { get; set; }

        [DataMember]
        public string Certificate { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public decimal TotalValue { get; set; }

        [DataMember]
        public string VippoSessionToken { get; set; }

        [DataMember]
        public string VippoToken { get; set; }

        [DataMember]
        public string VippoReference { get; set; }

        [DataMember]
        public string BDVToken { get; set; }

        [DataMember]
        public string BDVAccountType { get; set; }

        [DataMember]
        public string BDVReference { get; set; }  

        [DataMember]
        public string mrkcodeauth { get; set; }

        [DataMember]
        public string AccountType { get; set; }

        [DataMember]
        public string KeyAuth { get; set; }

        #endregion Properties
    }
}