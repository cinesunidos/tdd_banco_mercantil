﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class MessageEntity
    {
        #region Properties
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool IsValid { get; set; }
        [DataMember]
        public List<string> Messages { get; set; }
        #endregion Properties
    }
    public class MessageForCinemaEntity
    {
        [DataMember]
        public Cinema Cinema { get; set; }
        [DataMember]
        public string CinemaID
        {
            get
            {
                return Cinema.ToString().Substring(Cinema.ToString().Length - 4, 4);
            }
        }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool Activo { get; set; }
        [DataMember]
        public Category Category { get; set; }
        [DataMember]
        public city City { get; set; }
    }

    public enum Category
    {
        Elija,
        GlobalMessage,
        CityMessage,
        CinemaMessage
    }

    public enum city
    {
        Elija,
        Barquisimeto,
        Caracas,
        Guatire,
        Maracaibo,
        Maracay,
        Margarita,
        Maturin,
        Puerto_La_Cruz,
        Puerto_Ordaz,
        San_Cristobal,
        Valencia
    }

    public enum Cinema
    {
        Elija_0000,
        El_Marquez_1008,
        Galerias_Avila_1020,
        Galerias_Paraizo_1009,
        Guatire_Plaza_1003,
        Lider_1027,
        Los_Naranjos_1001,
        Metro_Center_1002,
        Millennium_1026,
        Sambil_Caracas_1005,
        Hiper_Jumbo_1015,
        La_Granja_1013,
        Las_Americas_1010,
        Metropolis_1011,
        Sambil_Barquisimeto_1025,
        Sambil_valencia_1014,
        Trinitarias_1006,
        Centro_Sur_1016,
        Costa_Azul_1022,
        Sambil_Maracaibo_1017,
        Sambil_San_Cristobal_1024,
        Orinokia_1023,
        Petroriente_1007,
        Regina_1019,
        Sambil_Margarita_1021
    }

    [DataContract]
    public class MessagePack
    {
        [DataMember]
        public string MessageList { get; set; }
        [DataMember]
        public string Token { get; set; }
    }

}
