﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class MessageEventualityEntity
    {
        #region Properties
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public string Creation { get; set; }
        [DataMember]
        public string Finish { get; set; }
        #endregion Properties
    }
}
