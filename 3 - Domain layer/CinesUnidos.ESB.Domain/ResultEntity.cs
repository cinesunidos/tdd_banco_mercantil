﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ResultEntity
    {
        [DataMember]
        public Boolean error { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public int number { get; set; }
    }
}
