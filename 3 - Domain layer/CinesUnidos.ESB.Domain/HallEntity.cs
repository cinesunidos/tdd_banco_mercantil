﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class HallEntity : ICloneable
    {
        #region Properties

        [DataMember]
        public int Hall_intId { get; set; }
        [DataMember]
        public string Cinema_strID { get; set; }
        [DataMember]
        public int Hall_intNumber { get; set; }
        [DataMember]
        public string Hall_strType { get; set; }
        [DataMember]
        public string Hall_strName { get; set; }
        [DataMember]
        public int Hall_intSeat { get; set; }
        [DataMember]
        public string Hall_strSeatAllocated { get; set; }
        
        #endregion

        #region Methods
        public HallEntity Clone()
        {
            HallEntity hall = new HallEntity();
            hall.Cinema_strID = this.Cinema_strID;
            hall.Hall_intNumber = this.Hall_intNumber;
            hall.Hall_strType = this.Hall_strType;
            hall.Hall_strName = this.Hall_strName;
            hall.Hall_intSeat = this.Hall_intSeat;
            hall.Hall_strSeatAllocated = this.Hall_strSeatAllocated;
          
            return hall;
        }
        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion
    }
}
