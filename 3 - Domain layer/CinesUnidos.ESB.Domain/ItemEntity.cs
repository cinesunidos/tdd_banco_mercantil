﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class ItemEntity
    {
        #region Properties

        [IgnoreDataMember]
        public Guid Id { get; set; }
        [DataMember(Order = 0)]
        public string Type { get; set; }
        [DataMember(Order = 1)]
        public string ImageID { get; set; }
        [DataMember(Order = 2)]
        public string MovieID { get; set; }
        [DataMember(Order = 3)]
        public string MovieHO { get; set; }
        [DataMember(Order = 4)]
        public DateTime Date { get; set; }
        [DataMember(Order = 5)]
        public string URL { get; set; }
        [IgnoreDataMember]
        public SectionEntity Section { get; set; }
        #endregion Properties
    }
}