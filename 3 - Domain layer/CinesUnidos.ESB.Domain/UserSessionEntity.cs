﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class UserSessionEntity
    {
        #region Properties

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string TheaterId { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public TicketEntity[] Tickets { get; set; }

        [DataMember]
        public DateTime TimeOut { get; set; }

        [DataMember]
        public bool ValidConcessions { get; set; }

        [DataMember]
        public DateTime PaymentTimeOut { get; set; }

        [DataMember]
        public List<SeatEntity> Seats { get; set; }

        [DataMember]
        public List<ConcessionsEntity> Concessions { get; set; }
        [DataMember]
        public ConcessionsEntity BookingFee { get; set; }
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Theater { get; set; }

        [DataMember]
        public string HO { get; set; }

        [DataMember]
        public DateTime ShowTime { get; set; }

        [DataMember]
        public string HallName { get; set; }

        [DataMember]
        public string ClientId { get; set; }

        [DataMember]
        public string TransIdTemp { get; set; }
        
        [DataMember]
        public string BookingByte { get; set; }
        public decimal TotalPrice
        {
            get
            {
                return Tickets.Sum(t => t.Price * t.Quantity);
            }
        }

        /// <summary>
        /// Sumatoria de IVA sobre el precio del boleto por cantidad de boletos seleccionados de ese tipo para todos los tipos de boleto contenidos en la sesión
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalTicketTax
        {
            get
            {
                return Tickets.Sum(t => t.TicketTax * t.Quantity );
            }
        }

        /// <summary>
        /// Sumatoria de IVA sobre el cargo por reservación (Cargo Web) por cantidad de boletos seleccionados de ese tipo para todos los tipos de boleto contenidos en la sesión    
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalBookingTax
        {
            get
            {
                return Tickets.Sum(t => t.BookingTax * t.Quantity);
            }
        }

        /// <summary>
        /// Sumatoria de IVA sobre el cargo por servicios por cantidad de boletos seleccionados de ese tipo para todos los tipos de boleto contenidos en la sesión
        /// GP: Añadido 2016-11-01
        /// </summary>
        public decimal TotalServiceTax
        {
            get
            {
                return Tickets.Sum(t => t.ServiceTax * t.Quantity);
            }
        }
        
        /// <summary>
        /// Sumatoria de Ticket Base Price (Precio base del Ticket * Cantidad)
        /// No Incluye el  IVA
        /// IM: Añadido/Modificado 2016-06-28
        /// </summary>
        public decimal TotalBasePrice
        {
            get
            {
                return Tickets.Sum(t => t.BasePrice * t.Quantity);
            }
        }

        public decimal TotalBookingFee
        {
            get
            {
                return Tickets.Sum(t => t.BookingFee * t.Quantity);
            }
        }

        public decimal TotalServiceFee
        {
            get
            {
                return Tickets.Sum(t => t.ServiceFee * t.Quantity);
            }
        }
        public decimal TotalPromotionFee
        {
            get
            {
                return Tickets.Sum(t => t.PromotionFeeWithTax * t.Quantity);
            }
        }
        public decimal TotalPromotionTax
        {
            get
            {
                return Tickets.Sum(t => t.PromotionTax * t.Quantity);
            }
        }

        public decimal TotalTax
        {
            get
            {
                return Tickets.Sum(t => t.Tax * t.Quantity);
            }
        }


        public decimal Total
        {
            get
            {
                return TotalBasePrice + TotalBookingFee + TotalTax + TotalServiceFee;
            }
        }

        //CARGO WEB CARAMELERIA                 
        [DataMember]
        public decimal WebChargeConcessions { get; set; }

        //Porcentaje IVA
        [DataMember]
        public decimal PercentageTax { get; set; }

        //Impuesto
        public decimal WebChargeTaxConcessions
        {
            get
            {                
                if (WebChargeConcessions <= 0)
                {
                    return 0;
                }
                else
                {
                    if (PercentageTax == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        //return ((WebChargeConcessions * PercentageTax) / 100);
                        return WebChargeConcessions - WebBaseChargeConcessions;
                    }
                    
                }                
            }
        }
        //Base
        public decimal WebBaseChargeConcessions
        {
            get
            {
                if (WebChargeConcessions <= 0)
                {
                    return 0;
                }
                else
                {
                    if (PercentageTax == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        //return WebChargeConcessions - WebChargeTaxConcessions;
                        return (WebChargeConcessions / (1 + (PercentageTax / 100)));
                    }
                }                
            }
        }
        //CARGO WEB CARAMELERIA FIN

        #region Payment

        [DataMember]
        public int BookingNumber { get; set; }
        //Localizador Alfanúmerico
        [DataMember]
        public string BookingId { get; set; }

        [DataMember]
        public string CodeResult { get; set; }

        [DataMember]
        public string DescriptionResult { get; set; }

        [DataMember]
        public string[] Voucher { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Email { get; set; }
        
        //[DataMember]
        //public string BookingConcat { get; set; }

        #endregion Payment

        #endregion Properties

        #region Methods

        public UserSessionEntity()
        {
            Seats = new List<SeatEntity>();
            Concessions = new List<ConcessionsEntity>();
        }

        public void SetSeats(MapEntity map, string[] seatsSelected)
        {
            foreach (string seat in seatsSelected)
            {
                char tier = char.Parse(seat.Substring(0, 1));
                TierEntity tierEntity = map.Tiers.Where(t => t.Name.Equals(tier)).FirstOrDefault();
                SeatEntity seatentity = tierEntity.Seats.Where(s => s.Name.Equals(seat)).SingleOrDefault();
                Seats.Add(seatentity);
                seatentity.Status = SeatEntity.SeatStatus.Selected;
            }
        }

        public static void SetAvailable(MapEntity map, string[] seatsAvailable)
        {
            foreach (string seat in seatsAvailable)
            {
                char tier = char.Parse(seat.Substring(0, 1));
                SeatEntity seatentity = map.Tiers.FirstOrDefault(t => t.Name.Equals(tier)).Seats
                                            .Where(s => s.Name.Equals(seat)).SingleOrDefault();
                seatentity.Status = SeatEntity.SeatStatus.Available;
            }
        }

        public static void SetBooked(MapEntity map, string[] seatsBooked)
        {
            foreach (string seat in seatsBooked)
            {
                char tier = char.Parse(seat.Substring(0, 1));
                SeatEntity seatentity = map.Tiers.FirstOrDefault(t => t.Name.Equals(tier)).Seats
                                            .Where(s => s.Name.Equals(seat)).SingleOrDefault();
                seatentity.Status = SeatEntity.SeatStatus.Booked;
            }
        }

        public static void SetSelected(MapEntity map, string[] seatsSelected)
        {
            foreach (string seat in seatsSelected)
            {
                char tier = char.Parse(seat.Substring(0, 1));
                SeatEntity seatentity = map.Tiers.FirstOrDefault(t => t.Name.Equals(tier)).Seats
                                            .Where(s => s.Name.Equals(seat)).SingleOrDefault();
                seatentity.Status = SeatEntity.SeatStatus.Selected;
            }
        }

        public bool ChageSeat(SeatEntity[] seatsSelect)
        {
            int countEqual = seatsSelect.Count(seat => Seats.Count(s => s.Name.Equals(seat.Name)).Equals(1));
            if (countEqual.Equals(Seats.Count()))
                return false;
            else
                return true;
        }

        #endregion Methods
    }
}