﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class SessionEntity
    {
        #region Properties

        [DataMember]
        public DateTime ShowTime { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string HallName { get; set; }

        [DataMember]
        public int HallNumber { get; set; }

        [DataMember]
        public bool SeatAllocated { get; set; }


        /// <summary>
        /// Se creo para diferenciar la censura por cine y asi poder mostrar las diferentes censuras
        /// en una sola pelicula en un archivo que sera procesado por las app moviles.
        /// Se coloco aqui para poder agruparla en el formato que Pedro de mobilemedia
        /// pueda mostrar sin repetir la pelicula y por censura.
        /// La censura puede variar por municipio.
        /// </summary>
        [DataMember]
        public string Censor { get; set; }


        #endregion Properties
    }
}