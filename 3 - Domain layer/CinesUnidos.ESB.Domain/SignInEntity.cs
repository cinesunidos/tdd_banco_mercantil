﻿using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    public class SignInEntity
    {
        #region Properties

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Platform { get; set; }

        #endregion Properties
    }
}