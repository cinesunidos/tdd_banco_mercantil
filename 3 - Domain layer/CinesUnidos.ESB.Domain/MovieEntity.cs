﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class MovieEntity : ICloneable
    {
        #region Properties

        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public DateTime FirstExhibit { get; set; }
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Sinopsis { get; set; }

        [DataMember]
        public string Director { get; set; }

        [DataMember]
        public string Actor { get; set; }

        [DataMember]
        public InformationEntity Information { get; set; }

        #endregion Properties

        #region Methods

        public MovieEntity Clone()
        {
            MovieEntity movie = new MovieEntity();
            movie.Actor = this.Actor;
            movie.Country = this.Country;
            movie.Director = this.Director;
            movie.Id = this.Id;
            movie.Information = this.Information.Clone();
            movie.Sinopsis = this.Sinopsis;
            movie.Title = this.Title;
            movie.FirstExhibit = this.FirstExhibit;
            return movie;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion Methods
    }
}