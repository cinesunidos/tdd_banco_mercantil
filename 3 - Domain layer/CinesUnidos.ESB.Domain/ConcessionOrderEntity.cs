﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ConcessionOrderEntity
    {
        [DataMember]
        public string TransIdTemp;

        [DataMember]
        public List<ConcessionsEntity> Items;

        //[DataMember]
        //public ConcessionOrderEntity Order;

        [DataMember]
        public ConcessionsEntity BookingFee;
    }
}
