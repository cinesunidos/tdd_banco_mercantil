﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{

    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Entity para las concesiones
    /// </summary>
    /// 
    [DataContract]
    [Serializable]
    public sealed class ConcessionsEntity : ICloneable
    {

        #region Properties
        [DataMember]
        public string ItemStrItemID { get; set; }
        [DataMember]
        public string ItemHOPK { get; set; }
        [DataMember]
        public string ItemStrItemDescription { get; set; }
        [DataMember]
        public decimal ItemPriceDCurPrice { get; set; }
        [DataMember]
        public string ItemStrItemDescriptionRecipes { get; set; }
        [DataMember]
        public string ItemStrAbbriation { get; set; }
        [DataMember]
        public string ItemLocationStrCode { get; set; }
        [DataMember]
        public string ItemLocationStrDescription { get; set; }
        [DataMember]
        public decimal ItemStockCurAvailable { get; set; }
        [DataMember]
        public int ItemSelectedItemQuantityStockPurchase { get; set; }
        [DataMember]
        public decimal ItemPriceDtotal { get; set; }
        [DataMember]
        public decimal ItemQuantityPurchase { get; set; }
        [DataMember]
        public string ItemType { get; set; }
        [DataMember]
        public string ClassStrCode { get; set; }
        [DataMember]
        public decimal RecipeQuantity { get; set; }
        #endregion

        #region Methods

        public ConcessionsEntity Clone()
        {
            ConcessionsEntity concessions = new ConcessionsEntity();
            concessions.ItemStrItemID = this.ItemStrItemID;
            concessions.ItemHOPK = this.ItemHOPK;
            concessions.ItemStrItemDescription = this.ItemStrItemDescription;
            concessions.ItemPriceDCurPrice = this.ItemPriceDCurPrice;
            concessions.ItemStrItemDescriptionRecipes = this.ItemStrItemDescriptionRecipes;
            concessions.ItemStrAbbriation = this.ItemStrAbbriation;
            concessions.ItemLocationStrCode = this.ItemLocationStrCode;
            concessions.ItemLocationStrDescription = this.ItemLocationStrDescription;
            concessions.ItemStockCurAvailable = this.ItemStockCurAvailable;
            concessions.ItemSelectedItemQuantityStockPurchase = this.ItemSelectedItemQuantityStockPurchase;
            concessions.ItemPriceDtotal = this.ItemPriceDtotal;
            concessions.ItemQuantityPurchase = this.ItemQuantityPurchase;
            concessions.ItemType = this.ItemType;
            concessions.ClassStrCode = this.ClassStrCode;
            
            return concessions;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion
    }
}
