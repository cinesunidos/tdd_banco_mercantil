﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain
{
    public class PromoPack
    {
        public int TransactionId { get; set; }
        public int CinemaId { get; set; }
        public string Cinema { get; set; }
        public string Token { get; set; }
        public bool Status { get; set; }
        public string Mensaje { get; set; }
        public string Workstation { get; set; }
        public DateTime DateRedemption { get; set; }
    }
}
