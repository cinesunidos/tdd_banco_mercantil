﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ModalConcesions
    {
        [DataMember]
        public int CinemaCode { get; set; }
        [DataMember]
        public string CinemaName { get; set; }
        [DataMember]
        public bool ModalEnabled { get; set; }
        [DataMember]
        public DateTime DateChanged { get; set; }
    }
}
