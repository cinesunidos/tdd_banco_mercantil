﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain
{
    public class ReplyEntity
    {
        public bool isOk { get; set; }

        public Guid Id { get; set; }
        public List<string> Messages { get; set; }
    }
}
