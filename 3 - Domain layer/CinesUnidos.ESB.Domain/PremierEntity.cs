﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class PremierEntity : ICloneable
    {
        #region Properties

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Sinopsis { get; set; }

        [DataMember]
        public string Director { get; set; }

        [DataMember]
        public string Actor { get; set; }

        [DataMember]
        public InformationEntity Information { get; set; }

        #endregion Properties

        #region Methods

        public PremierEntity Clone()
        {
            PremierEntity premier = new PremierEntity();
            premier.Id = this.Id;
            premier.Title = this.Title;
            premier.Date = this.Date;
            premier.Country = this.Country;
            premier.Sinopsis = this.Sinopsis;
            premier.Director = this.Director;
            premier.Actor = this.Actor;
            premier.Information = this.Information.Clone();
            return premier;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion Methods
    }
}