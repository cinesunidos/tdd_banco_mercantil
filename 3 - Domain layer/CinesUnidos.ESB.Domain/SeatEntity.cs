﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class SeatEntity
    {
        #region Enum

        [DataContract]
        public enum SeatStatus : int
        {
            [EnumMember]
            Booked = 0,

            [EnumMember]
            House = 1,

            [EnumMember]
            Selected = 2,

            [EnumMember]
            Available = 3,
        }

        [DataContract]
        public enum SeatType : int
        {
            [EnumMember]
            Chair = 0,

            [EnumMember]
            Aisle = 1,
        }

        #endregion Enum

        #region Properties

        [DataMember]
        public string AreaCategoryCode { get; set; }

        [DataMember]
        public int AreaNumber { get; set; }

        [DataMember]
        public int ColumnIndex { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int RowIndex { get; set; }

        [DataMember]
        public SeatStatus Status { get; set; }

        [DataMember]
        public SeatType Type { get; set; }

        #endregion Properties
    }
}