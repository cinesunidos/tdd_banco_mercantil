﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class MapEntity
    {
        #region Properties

        [DataMember]
        public TierEntity[] Tiers { get; set; }

        #endregion Properties

        public void Clear(bool isSelected)
        {
            foreach (TierEntity tier in Tiers)
            {
                foreach (SeatEntity seat in tier.Seats)
                {
                    if (isSelected)
                        seat.Status = SeatEntity.SeatStatus.Available;
                    else
                        seat.Status = SeatEntity.SeatStatus.Booked;
                }
            }
        }
    }
}