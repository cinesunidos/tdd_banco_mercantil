﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    [DataContract]
    public class ComplaintEntity
    {
        #region Properties

        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string Theater { get; set; }

        [DataMember]
        public string Movie { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public DateTime? Time { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string IDCard { get; set; }

        [DataMember]
        public string PhoneCode { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Comment { get; set; }

        #endregion Properties
    }
}