﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class FacilityEntity : ICloneable
    {
        #region Properties

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        #endregion Properties

        #region Methods

        public FacilityEntity Clone()
        {
            FacilityEntity facility = new FacilityEntity();
            facility.Id = this.Id;
            facility.Name = this.Name;
            return facility;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion Methods
    }
}