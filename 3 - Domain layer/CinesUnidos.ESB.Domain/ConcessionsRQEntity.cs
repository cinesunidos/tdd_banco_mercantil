﻿using System;
using System.Runtime.Serialization;


namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ConcessionsRQEntity
    {

        #region Properties

        [DataMember]
        public string UserSessionId { get; set; }

        [DataMember]
        public string TheaterId { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public int BookingNumber { get; set; }

        [DataMember]
        public string TransactionNumber { get; set; }

        [DataMember]
        public string TransIdTemp { get; set; }

        [DataMember]
        public ConcessionsEntity[] Concessions { get; set; }

        public string PorcCargoWEb { get; set; }

        #endregion


    }
}
