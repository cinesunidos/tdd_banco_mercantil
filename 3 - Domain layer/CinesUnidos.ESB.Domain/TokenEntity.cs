﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    public class TokenEntity
    {
        #region Properties-Login

        public Guid UserId { get; set; }

        public Guid Id { get; set; }

        [DataMember]
        public DateTime Expiration { get; set; }

        [DataMember]
        public string Hash { get; set; }

        [DataMember]
        public string Platform { get; set; }

        [DataMember]
        public string Error { get; set; }

        #endregion Properties-Login
    }
}