﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [Serializable]
    public class HomeEntity
    {
        [XmlArray]
        public Slider[] Slider { get; set; }

        [XmlArray]
        public BarMovie[] BarMovie { get; set; }

        [XmlArray]
        public MoreMovie[] MoreMovie { get; set; }

        public static HomeEntity Get()
        {
            Serializer s = new Serializer();
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string file = Path.Combine(path, @"Content\Home.xml");
            //return (HomeEntity)DogFramework.Xml.XmlAdapter.Deserialize(typeof(HomeEntity), file);
            return (HomeEntity)s.Deserialize(typeof(HomeEntity), file);
        }
    }

    [Serializable]
    public class Slider
    {
        [XmlAttribute]
        public string City { get; set; }

        [XmlElement]
        public string[] Hos { get; set; }
    }

    [Serializable]
    public class BarMovie
    {
        [XmlAttribute]
        public string City { get; set; }

        [XmlElement]
        public string[] Hos { get; set; }
    }

    [Serializable]
    public class MoreMovie
    {
        [XmlAttribute]
        public string City { get; set; }

        [XmlElement]
        public string[] Hos { get; set; }
    }
}