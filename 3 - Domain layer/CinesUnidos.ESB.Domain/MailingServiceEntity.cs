﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class MailingServiceEntity
    {
        #region Properties

        [DataMember]
        public string Tipo { get; set; }

        [DataMember]
        public UserEntity User { get; set; }

        [DataMember]
        public ComplaintEntity Complain { get; set; }

        #endregion Properties

        #region Methods

        public MailingServiceEntity()
        {
        }

        #endregion Methods
    }
}