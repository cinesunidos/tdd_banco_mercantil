﻿using System;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class TicketEntity
    {
        #region Properties
        [DataMember]
        /// <summary>
        /// RM - indica si el boleto es solo para redenciones
        /// </summary>
        public bool RedemptionOnly { get; set; }

        [DataMember]
        /// <summary>
        /// indica cual voucher quier cambiar por este boleto
        /// </summary>
        public string BarcodeRedemption { get; set; }

        [DataMember]
        public decimal BookingFee { get; set; }

        [DataMember]
        public decimal ServiceFee { get; set; }

        [DataMember]
        public decimal ServiceFeeWithTax { get; set; }

        [DataMember]
        public decimal PromotionFee { get; set; }

        [DataMember]
        public decimal PromotionFeeWithTax { get; set; }

        [DataMember]
        public decimal PromotionTax { get; set; }

        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Precio del Ticket incluyendo IVA si aplica
        /// </summary>
        [DataMember]
        public decimal Price { get; set; }

        /// <summary>
        ///  Precio base del Ticket sin IVA
        ///  IM: Añadido/Modificado 2016-06-28
        /// </summary>
        [DataMember]
        public decimal BasePrice { get; set; }

        /// <summary>
        ///  Precio Total con cargos e IVA
        ///  IM: Añadido/Modificado 2016-06-28
        /// </summary>
        [DataMember]
        public decimal FullPrice { get; set; }

        [DataMember]
        public decimal Tax { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        /// <summary>
        /// IVA sobre el precio del boleto (si el boleto no supera las 2UT debe ser cero)
        /// </summary>
        [DataMember]
        public decimal TicketTax { get; set; }

        /// <summary>
        /// IVA sobre el cargo por reservación (Cargo Web)
        /// </summary>
        [DataMember]
        public decimal BookingTax { get; set; }

        /// <summary>
        /// IVA sobre el cargo por servicios
        /// </summary>
        [DataMember]
        public decimal ServiceTax { get; set; }

        #endregion Properties

        #region Methods

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            TicketEntity ticket = obj as TicketEntity;
            if (ticket != null)
            {
                if (ticket.GetHashCode().Equals(this.GetHashCode()))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        #endregion Methods
    }
}