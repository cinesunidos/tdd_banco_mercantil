﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class TheaterBillboardEntity : ICloneable
    {
        #region Properties

        [DataMember]
        public TheaterEntity Theater { get; set; }

        [DataMember]
        public List<TheaterBillboardMovieEntity> Movies { get; set; }

        [DataMember]
        public DateTime[] Dates { get; set; }

        #endregion Properties

        #region Methods

        public TheaterBillboardEntity()
        {
            Movies = new List<TheaterBillboardMovieEntity>();
        }

        public void GetDates()
        {
            List<DateTime> list = new List<DateTime>();
            foreach (var item in Movies)
            {
                list.AddRange(item.GetDates());
            }
            Dates = list.Distinct().OrderBy(d => d).ToArray();
        }

        public TheaterBillboardEntity Clone()
        {
            TheaterBillboardEntity theaterBillboard = new TheaterBillboardEntity();
            theaterBillboard.Theater = this.Theater.Clone();
            List<DateTime> dates = new List<DateTime>();
            foreach (var date in this.Dates)
            {
                dates.Add(date);
            }
            theaterBillboard.Dates = dates.ToArray();
            return theaterBillboard;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion Methods
    }
}