﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinesUnidos.ESB.Domain
{
    [DataContract]
    [Serializable]
    public class ConfigurationEntity : ICloneable, IDisposable
    {
        #region Properties

        [DataMember]
        public Dictionary<string, string> Values { get; set; }

        #endregion Properties

        #region Methods

        public ConfigurationEntity()
        {
            Values = new Dictionary<string, string>();
        }

        public ConfigurationEntity Clone()
        {
            ConfigurationEntity configuration = new ConfigurationEntity();
            foreach (var value in Values)
            {
                configuration.Values.Add(value.Key, value.Value);
            }
            return configuration;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public void Dispose()
        {
            this.Values = null;
        }

        #endregion Methods
    }
}