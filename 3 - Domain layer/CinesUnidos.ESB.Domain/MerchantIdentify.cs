﻿namespace CinesUnidos.ESB.Domain
{
    public class MerchantIdentify
    {
        public string integratorId { get; set; }
        public string merchantId { get; set; }
        public string terminalId { get; set; }
    }
}