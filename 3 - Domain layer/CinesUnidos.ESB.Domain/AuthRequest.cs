﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain
{
    public class AuthRequest
    {
        public MerchantIdentify merchant_identify { get; set; }
        public ClientIdentify client_identify { get; set; }
        public TransactionAuthInfo transaction_authInfo { get; set; }
    }
}
