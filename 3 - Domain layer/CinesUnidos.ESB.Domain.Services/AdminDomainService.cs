﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;

namespace CinesUnidos.ESB.Domain.Services
{
    public class AdminDomainService : IAdminDomainContract
    {
        #region Attributes

        private IAdminRepository m_contactRepository;

        #endregion Attributes

        #region Contructors

        public AdminDomainService(IAdminRepository adminRepository)
        {
            m_contactRepository = adminRepository;
        }

        #endregion Contructors

        #region Methods

        public UserEntity Login(UserEntity usr)
        {
            m_contactRepository.Configuration = this.Configuration;
            return m_contactRepository.Login(usr);
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}