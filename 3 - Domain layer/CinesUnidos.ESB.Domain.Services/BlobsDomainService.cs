﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System.IO;

namespace CinesUnidos.ESB.Domain.Services
{
    public class BlobsDomainService : IBlobsDomainContract
    {
        #region Attributes

        private IBlobsRepository m_storageRepository;

        #endregion Attributes

        #region Contructors

        public BlobsDomainService(IBlobsRepository storageRepository)
        {
            m_storageRepository = storageRepository;
        }

        #endregion Contructors

        #region Methods

        public MemoryStream SearchFile(string fileName, string containerName)
        {
            return m_storageRepository.SearchFile(fileName, containerName);
        }

        public bool Exists(string fileName, string containerName)
        {
            return m_storageRepository.Exists(fileName, containerName);
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}