﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain.Services
{
    public class TicketPriceDomainService : ITicketPriceDomainContract
    {
        #region Attributes

        private ITicketPriceRepository m_ticketPriceRepository;

        #endregion Attributes

        #region Contructors

        public TicketPriceDomainService(ITicketPriceRepository ticketPriceRepository)
        {
            m_ticketPriceRepository = ticketPriceRepository;
        }

        #endregion Contructors

        #region Methods

        public CityTicketPriceEntity[] GetTicketPrice(string date)
        {
            m_ticketPriceRepository.Configuration = this.Configuration;
            return m_ticketPriceRepository.GetTicketPrice(date);
        }

        public InfoVoucher GetInfoVoucher(string cinemaid, string sessionid, string voucher)
        {
            m_ticketPriceRepository.Configuration = this.Configuration;
            return m_ticketPriceRepository.GetInfoVoucher(cinemaid, sessionid, voucher);
        }
        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}