﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System.Collections.Generic;
using System.IO;
using System;

namespace CinesUnidos.ESB.Domain.Services
{
    public class AppMobileDomainService : IAppMobileDomainContract
    {
        #region Attributes

        private IAppMobileRepository m_appMobileRepository;

        #endregion Attributes

        #region Contructors

        public AppMobileDomainService(IAppMobileRepository appMobileRepository)
        {
            m_appMobileRepository = appMobileRepository;
        }

        #endregion Contructors

        #region Methods

        public List<SectionEntity> GetHome(string platform)
        {
            m_appMobileRepository.Configuration = this.Configuration;
            return m_appMobileRepository.GetHome(platform);
        }

        public TheaterMobileEntity[] GetAllTheatersMobile()
        {
            m_appMobileRepository.Configuration = this.Configuration;
            return m_appMobileRepository.GetAllTheatersMobile();
        }
        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}