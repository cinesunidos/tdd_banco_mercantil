﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System.Collections.Generic;
using System;

namespace CinesUnidos.ESB.Domain.Services
{
    public class MobilePaymentDomainService : IMobilePaymentDomainContract
    {
        #region Attributes

        private IMobilePaymentRepository m_mobilepaymentRepository;

        #endregion Attributes


        #region Contructors

        public MobilePaymentDomainService(IMobilePaymentRepository mobilepaymentRepository)
        {
            m_mobilepaymentRepository = mobilepaymentRepository;
        }

        #endregion Contructors

        #region Methods

        #endregion Methods
    }
}
