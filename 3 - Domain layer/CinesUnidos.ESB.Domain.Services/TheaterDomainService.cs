﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;

namespace CinesUnidos.ESB.Domain.Services
{
    public class TheaterDomainService : ITheaterDomainContract
    {
        #region Attributes

        private ITheaterBillboardRepository m_theaterBillboardRepository;
        private ITheaterRepository m_theaterRepository;

        #endregion Attributes

        #region Contructors

        public TheaterDomainService(ITheaterBillboardRepository theaterBillboardRepository, ITheaterRepository theaterRepository)
        {
            m_theaterBillboardRepository = theaterBillboardRepository;
            m_theaterRepository = theaterRepository;
        }

        #endregion Contructors

        #region Methods

        public TheaterEntity[] GetAll()
        {
            m_theaterRepository.Configuration = this.Configuration;
            return m_theaterRepository.Data;
        }

        public TheaterBillboardEntity GetById(string theaterId)
        {
            m_theaterBillboardRepository.Configuration = this.Configuration;
            return m_theaterBillboardRepository.GetById(theaterId);
        }

        public TheaterBillboardEntity GetById(string theaterId, int day, int month)
        {
            m_theaterBillboardRepository.Configuration = this.Configuration;
            return m_theaterBillboardRepository.GetById(theaterId, day, month);
        }

        public TheaterBillboardEntity[] GetByCity(string city, int day, int month)
        {
            m_theaterBillboardRepository.Configuration = this.Configuration;
            return m_theaterBillboardRepository.GetByCity(city, day, month);
        }

        public TheaterBillboardEntity[] GetByCity(string city)
        {
            m_theaterBillboardRepository.Configuration = this.Configuration;
            return m_theaterBillboardRepository.GetByCity(city);
        }

        public string[] GetAllCity()
        {
            m_theaterBillboardRepository.Configuration = this.Configuration;
            return m_theaterBillboardRepository.GetAllCity();
        }

        public void Clear()
        {
            m_theaterBillboardRepository.Configuration = this.Configuration;
            m_theaterBillboardRepository.Clear();
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}