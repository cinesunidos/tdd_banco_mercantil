﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;

namespace CinesUnidos.ESB.Domain.Services
{
    public class ContactDomainService : IContactDomainContract
    {
        #region Attributes

        private IContactRepository m_contactRepository;

        #endregion Attributes

        #region Contructors

        public ContactDomainService(IContactRepository contactRepository)
        {
            m_contactRepository = contactRepository;
        }

        #endregion Contructors

        #region Methods

        public ResponseEntity SendComplaint(ComplaintEntity complaint)
        {
            m_contactRepository.Configuration = this.Configuration;
            return m_contactRepository.SendComplaint(complaint);
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}