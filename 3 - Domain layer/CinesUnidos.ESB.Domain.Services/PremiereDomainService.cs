﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;

namespace CinesUnidos.ESB.Domain.Services
{
    public class PremiereDomainService : IPremiereDomainContract
    {
        #region Attributes

        private IPremiereRepository m_premiereRepository;

        #endregion Attributes

        #region Contructors

        public PremiereDomainService(IPremiereRepository premiereRepository)
        {
            m_premiereRepository = premiereRepository;
        }

        #endregion Contructors

        #region Methods

        public PremierEntity[] GetAll()
        {
            m_premiereRepository.Configuration = this.Configuration;
            return m_premiereRepository.Data;
        }

        public void Clear()
        {
            m_premiereRepository.Clear();
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}