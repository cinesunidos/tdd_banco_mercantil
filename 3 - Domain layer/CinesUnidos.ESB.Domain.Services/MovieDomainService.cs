﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Services
{
    public class MovieDomainService : IMovieDomainContract
    {
        #region Attributes

        private IMovieBillboardRepository m_movieRepository;

        #endregion Attributes

        #region Contructors

        public MovieDomainService(IMovieBillboardRepository movieRepository)
        {
            m_movieRepository = movieRepository;
        }

        #endregion Contructors

        #region Methods

        public MovieBillboardEntity[] GetAll()
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.Data;
        }

        public void Clear()
        {
            m_movieRepository.Clear();
        }

        public MovieBillboardEntity GetById(string city, string movieId)
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetByCity(city, movieId);
        }

        public MovieBillboardEntity[] GetMovieAll()
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetMovieAll();
        }
        public MovieBillboardEntity[] GetMovieTheaterList(List<string> cines, int day, int month)
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetMovieTheaterList(cines, day, month);
        }
        public MovieBillboardEntity GetByIdDate(string city, string movieId, int day, int month)
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetByDate(city, movieId, day, month);
        }

        public MovieBillboardEntity[] GetByCity(string city)
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetByCity(city);
        }

        public MovieBillboardEntity[] GetByCityDate(string city, int day, int month)
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetByDate(city, day, month);
        }

        public SessionInfoEntity GetSession(string theaterId, int sessionId)
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetSession(theaterId, sessionId);
        }
        public MovieBillboardEntity[] GetAllMoviesByTheaterList(List<string> cines, int day, int month)
        {
            m_movieRepository.Configuration = this.Configuration;
            return m_movieRepository.GetAllMoviesByTheaterList(cines, day, month);
        }
        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}