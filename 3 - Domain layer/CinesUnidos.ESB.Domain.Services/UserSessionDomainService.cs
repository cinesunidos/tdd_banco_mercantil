﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Queue;
using System;

namespace CinesUnidos.ESB.Domain.Services
{
    public class UserSessionDomainService : IUserSessionDomainContract
    {
        #region Attributes

        private IUserSessionRepository m_userSessionRepository;
        private IQueue m_queue;

        #endregion Attributes

        #region Contructors

        public UserSessionDomainService(IUserSessionRepository userSessionRepository, IQueue @queue)
        {
            m_userSessionRepository = userSessionRepository;
            m_queue = queue;
        }

        #endregion Contructors

        #region Methods

        public ConfirmationResponseEntity Confirmation(ConfirmationEntity userSession)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.Confirmation(userSession);
        }

        public ConfirmationResponseEntity ConfirmationMobile(ConfirmationEntity userSession)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.ConfirmationMobile(userSession);
        }

        public UserSessionResponseEntity Create(BasketEntity userSession)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.Create(userSession);
        }
        //public UserSessionResponseMobileEntity CreateMobile(BasketEntity userSession)
        //{
        //    m_userSessionRepository.Configuration = this.Configuration;
        //    return m_userSessionRepository.CreateMobile(userSession);
        //}

        public void Delete(Guid userSessionId)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            m_userSessionRepository.Delete(userSessionId);
        }

        public PaymentResponseEntity Payment(PaymentEntity payment)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionRepository.Payment(payment);
            if (paymentResponse.CodeResult.Equals("00"))
            {
                UserSessionEntity userSession = Get(payment.UserSessionId);
                m_queue.InsertMessage<UserSessionEntity>("purchases", userSession);
            }
            else
            {
                UserSessionEntity userSession = Get(payment.UserSessionId);
                m_queue.InsertMessage<UserSessionEntity>("cancelled", userSession);
            }
            return paymentResponse;
        }
        public PaymentResponseEntity PaymentMobile(PaymentEntity payment)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionRepository.PaymentMobile(payment);
            ///Luis Ramírez
            ///Date:16-02-2016
            ///Send Email by Mobile platform
            if (paymentResponse.CodeResult.Equals("00"))
            {
                UserSessionEntity userSession = GetMobile(payment.UserSessionId);
                m_queue.InsertMessage<UserSessionEntity>("purchases", userSession);
            }
            else
            {
                UserSessionEntity userSession = GetMobile(payment.UserSessionId);
                m_queue.InsertMessage<UserSessionEntity>("cancelled", userSession);
            }
            return paymentResponse;
        }

        public PaymentResponseEntity PaymentAuthMercantil(PaymentEntity payment)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionRepository.PaymentAuthMercantil(payment);

            UserSessionEntity userSession = Get(payment.UserSessionId);
            m_queue.InsertMessage<UserSessionEntity>("cancelled", userSession);

            return paymentResponse;
        }

        public UserSessionResponseEntity Read(Guid userSessionId, string readMap)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.Read(userSessionId, readMap);
        }
        public UserSessionResponseMobileEntity ReadMobile(Guid userSessionId)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.ReadMobile(userSessionId);
        }

        public UserSessionEntity Get(Guid userSessionId)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.Get(userSessionId);
        }
        ///Luis Ramírez
        ///Date:16-02-2016
        ///Get UserSessionMobile data to redis cache key
        public UserSessionEntity GetMobile(Guid userSessionId)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.GetMobile(userSessionId);
        }

        public Boolean CancelOrder(string userSessionId)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.CancelOrder(userSessionId);
        }

        public UserSessionEntity DeleteSession(string userSessionId)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.DeleteSession(userSessionId);
        }

        public PaymentResponseEntity PaymentCandies(PaymentEntity payment)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            PaymentResponseEntity paymentResponse = m_userSessionRepository.PaymentCandies(payment);
            if (paymentResponse.CodeResult.Equals("00"))
            {
                UserSessionEntity userSession = Get(payment.UserSessionId);
                m_queue.InsertMessage<UserSessionEntity>("purchases", userSession);
            }
            else
            {
                UserSessionEntity userSession = Get(payment.UserSessionId);
                m_queue.InsertMessage<UserSessionEntity>("cancelled", userSession);
            }
            return paymentResponse;
        }

        public AVippoResponse GetVippoConfirmation(APaymentRequest Payment)
        {
            m_userSessionRepository.Configuration = this.Configuration;
            AVippoResponse paymentResponse = m_userSessionRepository.GetVippoConfirmation(Payment);
            return paymentResponse;
        }

        public string BDVGeneratePayment(BDVUserInfo id) {

            m_userSessionRepository.Configuration = this.Configuration;
            return m_userSessionRepository.BDVGeneratePayment(id);
        }


        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}