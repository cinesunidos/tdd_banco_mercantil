﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;

namespace CinesUnidos.ESB.Domain.Services
{
    public class PurchasesDomainService : IPurchasesDomainContract
    {
        #region Attributes

        private IPurchasesRepository m_purRepo;

        #endregion Attributes

        #region Contructors

        public PurchasesDomainService(IPurchasesRepository pRepository)
        {
            m_purRepo = pRepository;
        }

        #endregion Contructors

        #region Methods

        public PurchasesEntity[] GetAll(string usr)
        {
            m_purRepo.Configuration = this.Configuration;
            return m_purRepo.GetAll(usr);
        }

        public string Register(PurchasesEntity pur)
        {
            m_purRepo.Configuration = this.Configuration;
            return m_purRepo.Register(pur);
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}