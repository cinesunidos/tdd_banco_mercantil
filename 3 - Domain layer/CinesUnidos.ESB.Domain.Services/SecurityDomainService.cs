﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System;

namespace CinesUnidos.ESB.Domain.Services
{
    public class SecurityDomainService : ISecurityDomainContract
    {
        #region Attributes

        private ISecurityRepository m_securityRepository;

        #endregion Attributes

        #region Contructors

        public SecurityDomainService(ISecurityRepository securityRepository)
        {
            m_securityRepository = securityRepository;
        }

        #endregion Contructors

        #region Methods

        public TokenEntity Refresh(string token)
        {
            return m_securityRepository.Refresh(token);
        }

        public TokenEntity SignIn(SignInEntity login)
        {
            return m_securityRepository.SignIn(login);
        }

        public void Signout(string token)
        {
            m_securityRepository.Signout(token);
        }

        public bool Valid(string token)
        {
            return m_securityRepository.Valid(token);
        }

        public TokenInfoEntity GetTokenInfo(string token)
        {
            return m_securityRepository.GetInfo(token);
        }

        public ResponseEntity Create(UserEntity user)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.Create(user);
        }

        public ResponseEntity Update(UserEntity user)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.Update(user);
        }

        public void Delete(UserEntity user)
        {
            m_securityRepository.Configuration = this.Configuration;
            m_securityRepository.Update(user);
        }

        public UserEntity Read(Guid id)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.Read(id);
        }

        public UserEntity ReadByHash(string hash)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.ReadByHash(hash);
        }

        public string RememberEmail(string idCard, DateTime birthDate)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.RememberEmail(idCard, birthDate);
        }

        public UserEntity RememberPassword(string email)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.Read(email);
        }

        public string VerifiedAnswer(string email, string answer)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.VerifiedAnswer(email, answer);
        }

        public string VerifiedEmailAndIdCard(string email, string idCard)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.VerifiedEmailAndIdCard(email, idCard);
        }

        public ResponseEntity ChangePassword(PasswordEntity passwordEntity)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.ChangePassword(passwordEntity);
        }

        public ResponseEntity ActivateUser(Guid userId)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.ActivateUser(userId);
        }

        public ResponseEntity CreateUserMobile(UserEntity user)
        {
            m_securityRepository.Configuration = this.Configuration;
            return m_securityRepository.CreateUserMobile(user);

        }

        public ResponseEntity RemoveList(string userEntity)
        {
            this.m_securityRepository.Configuration = this.Configuration;
            return this.m_securityRepository.RemoveList(userEntity);
        }

        public DatosBoleto Passvalidation(DatosBoleto datosBoleto)
        {
            this.m_securityRepository.Configuration = this.Configuration;
            return this.m_securityRepository.Passvalidation(datosBoleto);
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}