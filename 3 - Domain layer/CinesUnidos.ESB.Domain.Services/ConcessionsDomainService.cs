﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Services
{
    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Clase que implementa las metodos de la clase ConcessionsDomainService
    /// </summary>
    public class ConcessionsDomainService : IConcessionsDomainContract
    {
        #region Attributes

        private IConcessionsRepository m_concessionsRepository;

        #endregion Attributes

        #region Contructors

        public ConcessionsDomainService(IConcessionsRepository concessionsRepository)
        {
            m_concessionsRepository = concessionsRepository;
        }

        #endregion Contructors

        #region Methods
        public ConcessionListEntity GetTheaterConcessions(string theaterId)
        {
            return m_concessionsRepository.GetTheaterConcessions(theaterId);
        }

        public ConcessionsRSEntity AddItemsToCart(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsRepository.AddItemsToCart(concessionsRq);
        }

        public ConcessionsRSEntity PurchaseConcessions(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsRepository.PurchaseConcessions(concessionsRq);
        }

        public ConcessionsRSEntity CancelConcessionsOrder(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsRepository.CancelConcessionsOrder(concessionsRq);
        }

        public MessageEntity ModifyModalConcessions()
        {
            return m_concessionsRepository.ModifyModalConcessions();
        }

        public ConfigurationEntity Configuration { get; set; }

        //public string PromoPepsi(string Codigo)
        //{

        //    return m_concessionsRepository.PromoPepsi(Codigo);

        //}

        public ConcessionListEntity GetCandiesWeb(string theaterId)
        {
            return m_concessionsRepository.GetCandiesWeb(theaterId);
        }

        public ConcessionsRSEntity AddItemsCandiesWeb(ConcessionsRQEntity concessionsRq)
        {
            return m_concessionsRepository.AddItemsCandiesWeb(concessionsRq);
        }

        public TransferPack UpdateCandiesTable(TransferPack items)
        {
            return m_concessionsRepository.UpdateCandiesTable(items);
        }

        public TransferPack DecreaseCandiesTable(TransferPack items)
        {
            return m_concessionsRepository.DecreaseCandiesTable(items);
        }

        public PromoPack PromoDesorden(PromoPack Pack)
        {
            try
            {
                return m_concessionsRepository.PromoDesorden(Pack);
            }
            catch (Exception Ex)
            {
                Pack.Mensaje = Ex.Message;
                return Pack;
            }
        }

        #endregion

    }
}