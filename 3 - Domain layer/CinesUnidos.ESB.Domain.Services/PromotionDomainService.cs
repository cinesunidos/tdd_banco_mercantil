﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System.Collections.Generic;
using System;

namespace CinesUnidos.ESB.Domain.Services
{
    public class PromotionDomainService : IPromotionDomainContract
    {
        #region Attributes
        private IPromotionRepository m_promotionRepository;
        #endregion Attributes

        #region Constructors
        public PromotionDomainService(IPromotionRepository promotionRepository)
        {
            m_promotionRepository = promotionRepository;
        }
        #endregion Constructors
        #region Methods
        #endregion Methods
        public ResultEntity PublishPromotion(PromotionPackageEntity PromotionList)
        {
            return m_promotionRepository.PublishPromotions(PromotionList);
        }

        public string GetPromotions()
        {
            return m_promotionRepository.GetPromotions();
        }

        public string GetPromotionsDB()
        {
            return m_promotionRepository.GetPromotionsDB();
        }
    }
}
