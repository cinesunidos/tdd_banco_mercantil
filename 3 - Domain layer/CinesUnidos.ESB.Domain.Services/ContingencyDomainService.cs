﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CinesUnidos.ESB.Domain.Services
{
    public class ContingencyDomainService : IContingencyDomainContract
    {
        #region Attributes
        private IContingencyRepository m_contingencyRepository;
        #endregion Attributes

        #region Constructor
        public ContingencyDomainService(IContingencyRepository contingencyRepository)
        {
            m_contingencyRepository = contingencyRepository;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Método para llamar a la capa Repository y guardar un mensaje
        /// se valida que el código del mensaje no se repita
        /// </summary>
        /// <param name="message">Mensaje a guardar</param>
        /// <returns>La respuesta si se guardó o no el mensaje en la base de datos</returns>
        public ReplyEntity createMessage(MessageEntity message)
        {
            m_contingencyRepository.Configuration = Configuration;
            var response = m_contingencyRepository.existCodeMessage(message.Code);
            if (response.isOk)
                return m_contingencyRepository.createMessage(message);
            else
                return response;
        }

        /// <summary>
        /// Método para llamar a la capa Repository y guardar un cine
        /// se valida que el código y el nombre del cine no se repita
        /// </summary>
        /// <param name="theater">Cine a guardar</param>
        /// <returns>La respuesta si se guardó o no  el cine en la base de datos</returns>
        public ReplyEntity createTheater(TheaterDBEntity theater)
        {
            m_contingencyRepository.Configuration = Configuration;
            var response = m_contingencyRepository.existCodeTheater(theater.Code);
            var response2 = m_contingencyRepository.existNameTheater(theater.Name);
            if (response.isOk && response2.isOk)
                return m_contingencyRepository.createTheater(theater);
            else
            {
                var errors = new List<string>();
                if (!response.isOk)
                    errors = errors.Concat(response.Messages).ToList();
                if (!response2.isOk)
                    errors = errors.Concat(response2.Messages).ToList();
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para llamar a la capa Repository y guardar una eventualidad
        /// </summary>
        /// <param name="eventuality">Eventualidad a guardar</param>
        /// <returns>La respuesta si se guardó o no la eventualidad en la base de datos</returns>
        public ReplyEntity createEventuality(EventualityEntity eventuality)
        {
            DateTime dateValue;
            if (!string.IsNullOrEmpty(eventuality.Creation))
            {
                if ((!DateTime.TryParseExact(eventuality.Creation, ResourcesDomain.FormatDate, null,
                           DateTimeStyles.None, out dateValue)) || (!DateTime.TryParseExact(eventuality.Finish, ResourcesDomain.FormatDate, null,
                           DateTimeStyles.None, out dateValue)))
                {
                    var errors = new List<string>();
                    errors.Add(ResourcesDomain.MessageErrorDate);
                    return new ReplyEntity
                    {
                        isOk = false,
                        Messages = errors
                    };
                }
            }
            var responseTheater = m_contingencyRepository.existCodeTheater(eventuality.TheaterCode);
            var responseMessage = m_contingencyRepository.existCodeMessage(eventuality.MessageCode);
            if (!responseTheater.isOk && !responseMessage.isOk)
            {
                return m_contingencyRepository.createEventuality(eventuality);
            }
            else
            {
                var errors = new List<string>();
                if (responseTheater.isOk)
                    errors.Add(ResourcesDomain.MessageTheaterNoExist);
                if (responseMessage.isOk)
                    errors.Add(ResourcesDomain.MessageMessageNoExist);
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para llamar a la capa Repository y consultar las eventualidades activas de un cine y posteriormente
        /// se valida que esté en el rango de fechas correspondiente
        /// </summary>
        /// <param name="theater">Cine a Consultar</param>
        /// <returns>Lista de Mensajes Activos para ese cine</returns>
        public List<MessageEntity> getEventualityMessage(TheaterDBEntity theater)
        {
            m_contingencyRepository.Configuration = Configuration;

            DateTime localDate = DateTime.Now.ToUniversalTime();
            TimeZoneInfo estTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            localDate = TimeZoneInfo.ConvertTimeFromUtc(localDate, estTimeZone);
            //TODO:Paolo Sivila
            //Solucion Temporal del Problema de la Zona Horaria en la pantalla de Seleccion de Tickets se estaba restando 30 min
            //Una vez aplicado el cambio de la zona horaria en el cloud services comentar esta linea de codigo         
            localDate = localDate.AddMinutes(30);
            List<MessageEventualityEntity> messages = m_contingencyRepository.getEventualityMessage(theater);

            var responseList = new List<MessageEntity>();
            foreach (MessageEventualityEntity message in messages)
            {
                if (message.Creation == null && message.Finish == null)
                {
                    var responseMessage = new MessageEntity();
                    responseMessage.Code = message.Code;
                    responseMessage.Message = message.Message;
                    responseList.Add(responseMessage);
                }
                else
                {
                    var beginDate = DateTime.Parse(message.Creation);
                    var endDate = DateTime.Parse(message.Finish);
                    if ((localDate > beginDate) && (localDate < endDate))
                    {
                        var responseMessage = new MessageEntity();
                        responseMessage.Code = message.Code;
                        responseMessage.Message = message.Message;
                        responseList.Add(responseMessage);
                    }
                }
            }

            return responseList;
        }

        /// <summary>
        /// Método para Desactivar una Eventualidad 
        /// </summary>
        /// <param name="eventuality">Eventualidad a desactivar</param>
        /// <returns>La respuesta si se desactivó o no la eventualidad</returns>
        public ReplyEntity deactivateEventuality(EventualityEntity eventuality)
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.deactivateEventuality(eventuality);
        }

        /// <summary>
        /// Método para Consultar todos los Mensajes de Eventualidades del Sistema
        /// </summary>
        /// <returns>Lista de Mensajes de Eventualidades</returns>
        public List<MessageEntity> getAllMessages()
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.getAllMessages();
        }

        /// <summary>
        /// Método para Consultar todos los Cines del Sistema
        /// </summary>
        /// <returns>Lista de Cines</returns>
        public List<TheaterDBEntity> getAllTheaters()
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.getAllTheaters();
        }

        public ReplyEntity existCodeMessage(int code)
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.existCodeMessage(code);
        }

        public ReplyEntity existCodeTheater(int code)
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.existCodeTheater(code);
        }

        public ReplyEntity existNameTheater(string name)
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.existNameTheater(name);
        }

        /// <summary>
        /// Método para Consultar todos los Mensajes y las Eventualidades activas para un cine
        /// </summary>
        /// <returns>Lista de Mensajes y evetualidades activas</returns>
        public List<MessageEventualityEntity> getEventualityMessagesTheater(TheaterDBEntity theater)
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.getEventualityMessagesTheater(theater);

        }

        public ResultEntity PublishMessages(MessagePack ListMessages)
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.PublishMessage(ListMessages);
            
        }

        public string MessagesForCinema()
        {
            m_contingencyRepository.Configuration = Configuration;
            return m_contingencyRepository.MessagesForCinema();
        }



        #endregion Methods

        #region Properties
        public ConfigurationEntity Configuration { get; set; }
        #endregion
    }
}
