﻿using CinesUnidos.ESB.Domain.Contracts;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain.Services
{
    public class RedisCacheManagementDomainService : IRedisCacheManagementDomainContract
    {
        #region Attributes

        private IRedisCacheManagementRepository m_RedisCacheManagementRepository;

        #endregion Attributes

        #region Contructors

        public RedisCacheManagementDomainService(IRedisCacheManagementRepository RedisCacheManagement)
        {
            m_RedisCacheManagementRepository = RedisCacheManagement;
        }

        #endregion Contructors

        #region Methods

        public string DeleteCacheAll()
        {
            m_RedisCacheManagementRepository.Configuration = this.Configuration;
            return m_RedisCacheManagementRepository.DeleteCacheAll();
        }

        public string DeleteCacheNoTicketsPrice()
        {
            m_RedisCacheManagementRepository.Configuration = this.Configuration;
            return m_RedisCacheManagementRepository.DeleteCacheNoTicketsPrice();
        }

        public string DeleteCacheTicketsPrice()
        {
            m_RedisCacheManagementRepository.Configuration = this.Configuration;
            return m_RedisCacheManagementRepository.DeleteCacheTicketsPrice();
        }

        public string ReadDateCacheTicketsPrice()
        {
            m_RedisCacheManagementRepository.Configuration = this.Configuration;
            return m_RedisCacheManagementRepository.ReadDateCacheTicketsPrice();
        }

        public string DeletePremiers()
        {
            m_RedisCacheManagementRepository.Configuration = this.Configuration;
            return m_RedisCacheManagementRepository.DeletePremiers();
        }

        public string GetPathLocalStorage()
        {
            m_RedisCacheManagementRepository.Configuration = this.Configuration;
            return m_RedisCacheManagementRepository.GetPathLocalStorage();
        }
        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}
