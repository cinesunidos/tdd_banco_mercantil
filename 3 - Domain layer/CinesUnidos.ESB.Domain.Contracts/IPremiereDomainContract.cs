﻿namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IPremiereDomainContract : IConfiguration
    {
        #region Methods

        void Clear();

        PremierEntity[] GetAll();

        #endregion Methods
    }
}