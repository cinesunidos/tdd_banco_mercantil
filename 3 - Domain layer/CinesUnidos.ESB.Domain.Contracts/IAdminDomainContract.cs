﻿namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IAdminDomainContract : IConfiguration
    {
        #region Methods

        UserEntity Login(UserEntity usr);

        #endregion Methods
    }
}