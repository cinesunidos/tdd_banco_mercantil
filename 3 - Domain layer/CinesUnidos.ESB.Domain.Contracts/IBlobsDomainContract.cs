﻿using System.IO;

namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IBlobsDomainContract : IConfiguration
    {
        #region Methods

        MemoryStream SearchFile(string fileName, string containerName);

        bool Exists(string fileName, string containerName);

        #endregion Methods
    }
}