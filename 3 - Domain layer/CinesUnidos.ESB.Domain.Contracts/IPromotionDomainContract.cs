﻿using System.Collections.Generic;
using System;


namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IPromotionDomainContract 
    {
        #region Methods
        ResultEntity PublishPromotion(PromotionPackageEntity PromotionList);
        string GetPromotions();
        string GetPromotionsDB();
        #endregion Methods
    }
}
