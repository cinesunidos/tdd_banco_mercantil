﻿using System.IO;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IBlobsRepository
    {
        #region Properties

        string[] Containers { get; }

        string Key { get; }

        string Name { get; }

        #endregion Properties

        #region Methods

        MemoryStream SearchFile(string fileName, string containerName);

        bool Exists(string fileName, string containerName);

        #endregion Methods
    }
}