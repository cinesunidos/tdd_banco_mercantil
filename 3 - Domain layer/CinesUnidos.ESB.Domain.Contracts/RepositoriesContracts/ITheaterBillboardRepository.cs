﻿namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface ITheaterBillboardRepository : IConfiguration
    {
        #region Properties

        TheaterBillboardEntity[] Data { get; }

        #endregion Properties

        #region Methods

        TheaterBillboardEntity GetById(string theaterId);

        TheaterBillboardEntity GetById(string theaterId, int day, int month);

        TheaterBillboardEntity[] GetByCity(string city, int day, int month);

        TheaterBillboardEntity[] GetByCity(string city);

        string[] GetAllCity();

        void Clear();

        #endregion Methods
    }
}