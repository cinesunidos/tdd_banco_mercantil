﻿namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IPremiereRepository : IConfiguration
    {
        #region Properties

        PremierEntity[] Data { get; }

        #endregion Properties

        #region Methods

        void Clear();

        #endregion Methods
    }
}