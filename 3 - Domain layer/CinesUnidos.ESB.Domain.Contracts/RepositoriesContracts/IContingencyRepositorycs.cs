﻿using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IContingencyRepository : IConfiguration
    {
        #region Methods
        /// <summary>
        /// Método para consultar si ya existe un mensaje con el código pasado en el parámetro
        /// </summary>
        /// <param name="code">Código del Mensaje a Consultar</param>
        /// <returns>Retorna un objeto con la respuesta de la consulta de la base de datos
        /// si el código ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        ReplyEntity existCodeMessage(int code);

        /// <summary>
        /// Método para verificar si ya existe en la base de datos un cine con el código pasado en el parámetro
        /// </summary>
        /// <param name="code">Código del cine</param>
        /// <returns>Retorna un objeto con la respuesta de la consulta de la base de datos
        /// si el código ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        ReplyEntity existCodeTheater(int code);

        /// <summary>
        /// Método para verificar en la base de datos si existe un cine con el nombre pasado en el parámetro
        /// </summary>
        /// <param name="name">Nombre del Cine</param>
        /// <returns>Retorna un objeto con la respuesta de la consulta de la base de datos
        /// si el nombre ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        ReplyEntity existNameTheater(string name);

        /// <summary>
        /// Método para crear en la Base de Datos un mensaje
        /// </summary>
        /// <param name="message">Objeto Mensaje</param>
        /// <returns>
        /// Retorna un objeto del tipo Reply si su atributo IsOk vale true es que se salvó correctamente
        /// en la base de datos, de lo contrario retornará con valor false y una lista de errores.
        /// </returns>
        ReplyEntity createMessage(MessageEntity message);

        /// <summary>
        /// Método para crear un Cine en la base de datos
        /// </summary>
        /// <param name="theater">Objeto del tipo Cine</param>
        /// <returns>
        /// Retorna un objeto del tipo Reply si su atributo IsOk tiene valor true es que se salvó
        /// correctamente en la base de datos, de lo contrario retornará con valor false y una lista de errores.
        /// </returns>
        ReplyEntity createTheater(TheaterDBEntity theater);

        /// <summary>
        /// Método para crear una eventualidad en la base de datos
        /// </summary>
        /// <param name="eventuality">Objeto del tipo Eventuality</param>
        /// <returns>Retorna un objeto del tipo Reply si su atributo IsOk tiene valor true es que se salvó
        /// correctamente en la base de datos, de lo contrario retornará con valor false y una lista de errores
        /// </returns>
        ReplyEntity createEventuality(EventualityEntity eventuality);

        /// <summary>
        /// Método para obtener las eventualidades activas en un cine
        /// </summary>
        /// <param name="theater">Objeto del tipo cine</param>
        /// <returns>
        /// Retorna una lista de objetos del tipo Mensaje, el atributo Id es el Id de la eventualidad
        /// los otros atributos si pertenecen al mensaje
        /// </returns>
        List<MessageEventualityEntity> getEventualityMessage(TheaterDBEntity theater);

        /// <summary>
        /// Método para desactivar una eventualidad
        /// </summary>
        /// <param name="eventuality">Objeto del tipo Eventuality</param>
        /// <returns>
        /// Retorna un objeto del tipo Reply, si la eventualidad se desactivó correctamente retornará 
        /// el atributo IsOk con valor false, de lo contrario retornará false y una lista de errores.
        /// </returns>
        ReplyEntity deactivateEventuality(EventualityEntity eventuality);

        /// <summary>
        /// Método para obtener todos los mensajes
        /// </summary>
        /// <returns>Retorna una lista de Mensajes</returns>
        List<MessageEntity> getAllMessages();

        /// <summary>
        /// Método para obtener todos los cines
        /// </summary>
        /// <returns>Retorna una lista de Cines</returns>
        List<TheaterDBEntity> getAllTheaters();

        /// <summary>
        /// Método para obtener todos los mensajes y las eventualidades activas para un Cine
        /// </summary>
        /// <returns>Retorna una lista de Eventualidades activas y desactivadas para un Cine</returns>
        List<MessageEventualityEntity> getEventualityMessagesTheater(TheaterDBEntity theater);

        ResultEntity PublishMessage(MessagePack MessagesList);


        string MessagesForCinema();
        #endregion Methods
    }
}
