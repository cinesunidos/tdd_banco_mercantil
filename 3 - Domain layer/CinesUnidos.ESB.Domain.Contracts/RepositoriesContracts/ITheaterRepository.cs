﻿namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface ITheaterRepository : IConfiguration
    {
        #region Properties

        TheaterEntity[] Data { get; }

        #endregion Properties

        #region Methods

        void Clear();

        #endregion Methods
    }
}