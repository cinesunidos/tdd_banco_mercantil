﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IRedisCacheManagementRepository : IConfiguration
    {
        string DeleteCacheAll();
        string DeleteCacheNoTicketsPrice();
        string DeleteCacheTicketsPrice();
        string ReadDateCacheTicketsPrice();
        string DeletePremiers();
        string GetPathLocalStorage();
    }
}
