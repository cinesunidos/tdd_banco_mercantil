﻿namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IAdminRepository : IConfiguration
    {
        #region Methods

        UserEntity Login(UserEntity complaint);

        #endregion Methods
    }
}