﻿using System;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface ISecurityRepository : IConfiguration
    {
        TokenEntity Refresh(string token);

        TokenEntity SignIn(SignInEntity login);

        void Signout(string token);

        bool Valid(string token);

        TokenInfoEntity GetInfo(string token);

        ResponseEntity Create(UserEntity user);

        ResponseEntity Update(UserEntity user);

        void Delete(UserEntity user);

        UserEntity Read(Guid id);

        UserEntity ReadByHash(string hash);

        UserEntity Read(string email);

        string RememberEmail(string idCard, DateTime birthDate);

        string VerifiedAnswer(string email, string answer);

        string VerifiedEmailAndIdCard(string email, string idCard);

        ResponseEntity ChangePassword(PasswordEntity passwordEntity);

        ResponseEntity ActivateUser(Guid userId);    

        ResponseEntity CreateUserMobile(UserEntity user);

        ResponseEntity RemoveList(string userEntity);

        DatosBoleto Passvalidation(DatosBoleto datosBoleto);
    }
}