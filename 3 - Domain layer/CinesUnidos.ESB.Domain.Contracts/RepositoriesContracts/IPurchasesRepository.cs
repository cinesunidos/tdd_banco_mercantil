﻿namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IPurchasesRepository : IConfiguration
    {
        #region Methods

        PurchasesEntity[] GetAll(string usr);
        string Register(PurchasesEntity ent);

        string RegisterComplete(PurchasesEntity ent, UserSessionEntity UserSession);
        #endregion Methods
    }
}