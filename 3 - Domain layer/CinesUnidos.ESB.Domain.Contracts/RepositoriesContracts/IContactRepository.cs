﻿namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IContactRepository : IConfiguration
    {
        #region Methods

        ResponseEntity SendComplaint(ComplaintEntity complaint);

        #endregion Methods
    }
}