﻿using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IMovieBillboardRepository : IConfiguration
    {
        #region Properties

        MovieBillboardEntity[] Data { get; }

        #endregion Properties

        #region Methods

        void Clear();
        void Clear2();

        MovieBillboardEntity[] GetByCity(string city);

        MovieBillboardEntity GetByCity(string city, string movieId);

        MovieBillboardEntity[] GetMovieAll();
        MovieBillboardEntity[] GetMovieTheaterList(List<string> cines, int day, int month);

        MovieBillboardEntity[] GetByDate(string city, int day, int month);

        MovieBillboardEntity GetByDate(string city, string movieId, int day, int month);

        SessionInfoEntity GetSession(string theaterId, int sessionId);

        MovieBillboardEntity[] GetAllMoviesByTheaterList(List<string> cines, int day, int month);

        #endregion Methods
    }
}