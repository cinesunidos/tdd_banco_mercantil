﻿using System;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IUserSessionRepository : IConfiguration
    {
        #region Methods

        ConfirmationResponseEntity Confirmation(ConfirmationEntity userSession);

        ConfirmationResponseEntity ConfirmationMobile(ConfirmationEntity userSession);

        UserSessionResponseEntity Create(BasketEntity userSession);

        //UserSessionResponseMobileEntity CreateMobile(BasketEntity userSession);

        void Delete(Guid userSessionId);

        PaymentResponseEntity Payment(PaymentEntity payment);

        PaymentResponseEntity PaymentAuthMercantil(PaymentEntity payment);

        PaymentResponseEntity PaymentCandies(PaymentEntity payment);

        PaymentResponseEntity PaymentMobile(PaymentEntity payment);

        UserSessionResponseEntity Read(Guid userSessionId, string readMap);

        UserSessionResponseMobileEntity ReadMobile(Guid userSessionId);

        UserSessionEntity Get(Guid userSessionId);

        UserSessionEntity GetMobile(Guid userSessionId);

        Boolean CancelOrder(string UserSessionID);

        UserSessionEntity DeleteSession(string UserSessionId);

        AVippoResponse GetVippoConfirmation(APaymentRequest Payment);

        string BDVGeneratePayment(BDVUserInfo info);
        #endregion Methods
    }
}