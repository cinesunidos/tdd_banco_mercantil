﻿

using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{

    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Interface que define los metodos repositorio de la clase ConcessionsRepository
    /// </summary>
    public interface IConcessionsRepository : IConfiguration
    {
        ConcessionListEntity GetTheaterConcessions(string theaterId);
        ConcessionsRSEntity AddItemsToCart(ConcessionsRQEntity concessionsRq);
        ConcessionsRSEntity PurchaseConcessions(ConcessionsRQEntity concessionsRq);
        ConcessionsRSEntity CancelConcessionsOrder(ConcessionsRQEntity concessionsRq);
        ConcessionsRSEntity UpdateConcessionsOrder(UpdateConcessionsOrderRequest request);

        /// <summary>
        /// Ramiro Marimón 24/04/2018
        /// modificar el estatus de la model de un cine en la tabla de azure
        /// </summary>
        /// <param name="ListModalConcessions"></param>
        /// <returns></returns>
        MessageEntity ModifyModalConcessions();

        ConcessionListEntity GetCandiesWeb(string theaterId);

        ConcessionsRSEntity AddItemsCandiesWeb(ConcessionsRQEntity concessionsRq);

        //string PromoPepsi(string Codigo);

        /// <summary>
        /// Ramiro Marimon
        /// metodo para actualizar la tabla de carameleria en la nube
        /// luego de haber hecho un traslado de inventarios desde el cms
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        TransferPack UpdateCandiesTable(TransferPack items);

        TransferPack DecreaseCandiesTable(TransferPack items);

        PromoPack PromoDesorden(PromoPack Pack);

        bool PromoDesordenVerify(PromoPack Pack);

        void PromoDesordenRegister(PromoPack Pack);
    }

}
