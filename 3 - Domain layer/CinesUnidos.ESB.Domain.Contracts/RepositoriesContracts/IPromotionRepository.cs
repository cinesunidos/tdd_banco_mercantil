﻿#region Using
using System.Collections.Generic;
#endregion Using

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface  IPromotionRepository 
    {
        #region Methods
        ResultEntity PublishPromotions(PromotionPackageEntity PromotionList);
        string GetPromotions();
        string GetPromotionsDB();
        #endregion Methods
    }
}
