﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts
{
    public interface IAppMobileRepository : IConfiguration
    {
        List<SectionEntity> GetHome(string platform);
        TheaterMobileEntity[] GetAllTheatersMobile();
    }
}