﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IContingencyDomainContract : IConfiguration
    {
        #region Methods
        /// <summary>
        /// Método para llamar a la capa Repository y guardar un mensaje
        /// se valida que el código del mensaje no se repita
        /// </summary>
        /// <param name="message">Mensaje a guardar</param>
        /// <returns>La respuesta si se guardó o no el mensaje en la base de datos</returns>
        ReplyEntity createMessage(MessageEntity message);

        /// <summary>
        /// Método para llamar a la capa Repository y guardar un cine
        /// se valida que el código y el nombre del cine no se repita
        /// </summary>
        /// <param name="theater">Cine a guardar</param>
        /// <returns>La respuesta si se guardó o no  el cine en la base de datos</returns>
        ReplyEntity createTheater(TheaterDBEntity theater);

        /// <summary>
        /// Método para llamar a la capa Repository y guardar una eventualidad
        /// </summary>
        /// <param name="eventuality">Eventualidad a guardar</param>
        /// <returns>La respuesta si se guardó o no la eventualidad en la base de datos</returns>
        ReplyEntity createEventuality(EventualityEntity eventuality);

        /// <summary>
        /// Método para llamar a la capa Repository y consultar las eventualidades activas de un cine
        /// </summary>
        /// <param name="theater">Cine a Consultar</param>
        /// <returns>Lista de Mensajes Activos para ese cine</returns>
        List<MessageEntity> getEventualityMessage(TheaterDBEntity theater);

        /// <summary>
        /// Método para Desactivar una Eventualidad 
        /// </summary>
        /// <param name="eventuality">Eventualidad a desactivar</param>
        /// <returns>La respuesta si se desactivó o no la eventualidad</returns>
        ReplyEntity deactivateEventuality(EventualityEntity eventuality);

        /// <summary>
        /// Método para Consultar todos los Mensajes de Eventualidades del Sistema
        /// </summary>
        /// <returns>Lista de Mensajes de Eventualidades</returns>
        List<MessageEntity> getAllMessages();

        /// <summary>
        /// Método para Consultar todos los Cines del Sistema
        /// </summary>
        /// <returns>Lista de Cines</returns>
        List<TheaterDBEntity> getAllTheaters();

        /// <summary>
        /// Método para consultar si ya existe un mensaje con el código pasado en el parámetro
        /// </summary>
        /// <param name="code">Código del Mensaje a Consultar</param>
        /// <returns>Retorna un objeto del tipo ReplyEntity
        /// si el código ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        ReplyEntity existCodeMessage(int code);

        /// <summary>
        /// Método para verificar si ya existe en la base de datos un cine con el código pasado en el parámetro
        /// </summary>
        /// <param name="code">Código del cine</param>
        /// <returns>Retorna un objeto del tipo ReplyEntity
        /// si el código ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        ReplyEntity existCodeTheater(int code);

        /// <summary>
        /// Método para verificar en la base de datos si existe un cine con el nombre pasado en el parámetro
        /// </summary>
        /// <param name="name">Nombre del Cine</param>
        /// <returns>Retorna un objeto del tipo ReplyEntity
        /// si el nombre ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        ReplyEntity existNameTheater(string name);

        /// <summary>
        /// Método para llamar a la capa Repository y consultar todos los mensajes y las eventualidades activas para un cine
        /// </summary>
        /// <param name="theater">Cine a Consultar</param>
        /// <returns>Lista de todos los Mensajes y las eventualdiades activas de un cine</returns>
        List<MessageEventualityEntity> getEventualityMessagesTheater(TheaterDBEntity theater);

        //Servicio para publicar mensajes del cms en la pagina web de CU
        ResultEntity PublishMessages(MessagePack MessagesList);


        /// <summary>
        /// metodo para leer el archivo de mensages para la pagina web. retorna un string (el contenido del archivo), que luego debe ser serealizado en otra capa..
        /// </summary>
        /// <returns>texto contenido en el archivo messages.xml</returns>
        string MessagesForCinema();
        #endregion
    }
}
