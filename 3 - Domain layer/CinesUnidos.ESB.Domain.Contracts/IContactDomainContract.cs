﻿namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IContactDomainContract : IConfiguration
    {
        #region Methods

        ResponseEntity SendComplaint(ComplaintEntity complaint);

        #endregion Methods
    }
}