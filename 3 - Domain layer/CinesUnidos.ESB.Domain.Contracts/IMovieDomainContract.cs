﻿using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IMovieDomainContract : IConfiguration
    {
        #region Methods

        MovieBillboardEntity[] GetAll();

        void Clear();

        MovieBillboardEntity[] GetByCity(string city);

        MovieBillboardEntity[] GetByCityDate(string city, int day, int month);

        MovieBillboardEntity GetById(string city, string movieId);

        MovieBillboardEntity[] GetMovieAll();

        MovieBillboardEntity[] GetMovieTheaterList(List<string> cines, int day, int month);

        MovieBillboardEntity GetByIdDate(string city, string movieId, int day, int month);

        SessionInfoEntity GetSession(string theaterId, int sessionId);

        MovieBillboardEntity[] GetAllMoviesByTheaterList(List<string> cines, int day, int month);
        #endregion Methods
    }
}