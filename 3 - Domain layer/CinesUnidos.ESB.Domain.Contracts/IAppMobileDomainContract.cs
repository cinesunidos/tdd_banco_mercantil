﻿using System.Collections.Generic;
using System.IO;

namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IAppMobileDomainContract : IConfiguration
    {
        #region Methods

        List<SectionEntity> GetHome(string platform);
        TheaterMobileEntity[] GetAllTheatersMobile();

        #endregion Methods
    }
}