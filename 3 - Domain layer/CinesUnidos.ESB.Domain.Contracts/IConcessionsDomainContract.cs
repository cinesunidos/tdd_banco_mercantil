﻿using System.Collections.Generic;

namespace CinesUnidos.ESB.Domain.Contracts
{
    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Interface que define los metodos de la clase ConcessionsDomainServices
    /// </summary>
    public interface IConcessionsDomainContract : IConfiguration
    {
        ConcessionListEntity GetTheaterConcessions(string theaterId);

        ConcessionsRSEntity AddItemsToCart(ConcessionsRQEntity concessionsRq);

        ConcessionsRSEntity PurchaseConcessions(ConcessionsRQEntity concessionsRq);

        ConcessionsRSEntity CancelConcessionsOrder(ConcessionsRQEntity concessionsRq);

        /// <summary>
        /// Ramiro Marimón 24/04/2018
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        MessageEntity ModifyModalConcessions();

        //string PromoPepsi(string Codigo);
   
        ConcessionsRSEntity AddItemsCandiesWeb(ConcessionsRQEntity concessionsRq);

        ConcessionListEntity GetCandiesWeb(string theaterId);


        /// <summary>
        /// Ramiro Marimon
        /// Metodo para actualizar las tablñas de stock de carameleria en la nube 
        /// luego de haber realizado un traslado de iventario desde el cms
        /// </summary>
        /// <param name="ListMessages"></param>
        /// <returns></returns>
        TransferPack UpdateCandiesTable(TransferPack items);

        TransferPack DecreaseCandiesTable(TransferPack items);

        PromoPack PromoDesorden(PromoPack Pack);
    }
}
