﻿namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IPurchasesDomainContract : IConfiguration
    {
        #region Methods

        PurchasesEntity[] GetAll(string usr);
        string Register(PurchasesEntity ent);

        #endregion Methods
    }
}