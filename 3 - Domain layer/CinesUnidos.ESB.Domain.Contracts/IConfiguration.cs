﻿namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IConfiguration
    {
        #region Properties

        ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}