﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface IRedisCacheManagementDomainContract : IConfiguration
    {
        string DeleteCacheAll();

        string DeleteCacheNoTicketsPrice();

        string DeleteCacheTicketsPrice();

        string ReadDateCacheTicketsPrice();

        string DeletePremiers();

        string GetPathLocalStorage();
    }
}
   