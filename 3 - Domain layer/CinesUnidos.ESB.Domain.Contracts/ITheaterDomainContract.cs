﻿namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface ITheaterDomainContract : IConfiguration
    {
        #region Methods

        TheaterEntity[] GetAll();

        TheaterBillboardEntity GetById(string theaterId);

        TheaterBillboardEntity GetById(string theaterId, int day, int month);

        TheaterBillboardEntity[] GetByCity(string city, int day, int month);

        TheaterBillboardEntity[] GetByCity(string city);

        string[] GetAllCity();

        void Clear();

        #endregion Methods
    }
}