﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Domain.Contracts
{
    public interface ITicketPriceDomainContract : IConfiguration
    {
        CityTicketPriceEntity[] GetTicketPrice(string date);

        InfoVoucher GetInfoVoucher(string cinemaid, string sessionid, string voucher);
    }
}
