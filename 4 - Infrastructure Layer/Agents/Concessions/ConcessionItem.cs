﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Concessions
{
    public partial class ConcessionItem
    {

        #region Methods

        public static explicit operator ConcessionsEntity(ConcessionItem concession)
        {
            if (concession != null)
            {
                ConcessionsEntity concessionEntity = new ConcessionsEntity();
                
                concessionEntity.ItemStrItemID = concession.ItemStrItemID;
                concessionEntity.ItemHOPK = concession.ItemHOPK1;
                concessionEntity.ItemStrItemDescription = concession.ItemStrItemDescription;
                concessionEntity.ItemPriceDCurPrice = concession.ItemPriceDCurPrice;
                concessionEntity.ItemStockCurAvailable = concession.ItemStockCurAvailable;
                concessionEntity.ItemQuantityPurchase = concession.ItemQuantityPurchase;
                concessionEntity.ItemType = concession.ItemType;
                concessionEntity.RecipeQuantity = concession.RecipeQuantity;
                concessionEntity.ClassStrCode = concession.ClassStrCode;
                
                return concessionEntity;
            }
            
            return null;
        }

        #endregion Methods

    }
}
