﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Infrastructure.Agents.Live;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Infrastructure.Agents.Premium
{
    public partial class SeatMapResponse
    {
        #region Methods
        public static explicit operator SeatMapResponseEntity(SeatMapResponse response)
        {
            if (response != null)
            {
                SeatMapResponseEntity MapResponse = new SeatMapResponseEntity();
                //string map for mobile platform
                MapResponse.SeatData = response.SeatData;
                
                //TicketList
                List<TicketEntity> TicketsList = new List<TicketEntity>();
                foreach (var item in response.Tickets)
                {
                    TicketEntity ticket = new TicketEntity();
                    ticket.BasePrice = item.BasePrice;
                    ticket.BookingFee = item.BookingFee;
                    ticket.BookingTax = item.BookingTax;
                    ticket.Code = item.Code;
                    ticket.FullPrice = item.FullPrice;
                    ticket.Name = item.Name;
                    ticket.Price = item.Price;
                    ticket.Quantity = item.Quantity;
                    //Promotions
                    ticket.PromotionFee = item.PromotionFee;
                    ticket.PromotionFeeWithTax = item.TotalPromotionFee;
                    ticket.PromotionTax = item.PromotionTax;
                    //Services
                    ticket.ServiceFee = item.ServiceFee;
                    ticket.ServiceFeeWithTax = item.TotalServiceFee;
                    ticket.ServiceTax = item.ServiceTax;
                    //Tax
                    ticket.Tax = item.Tax;
                    ticket.TicketTax = item.TicketTax;

                    TicketsList.Add(ticket);
                }
                MapResponse.Tickets = TicketsList.ToArray();

                return MapResponse;
            }
            else
            {
                return null;
            }

        }
        #endregion Methods
        public string SeatData { get; set; }

        public Ticket[] Tickets { get; set; }
    }
}