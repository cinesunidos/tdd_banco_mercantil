﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Premium
{
    public partial class Seat
    {
        #region Methods

        public static implicit operator SeatEntity(Seat seat)
        {
            SeatEntity seatEntity = new SeatEntity();
            seatEntity.AreaCategoryCode = seat.AreaCategoryCode;
            seatEntity.AreaNumber = seat.AreaNumber;
            seatEntity.ColumnIndex = seat.ColumnIndex;
            seatEntity.Name = seat.Name;
            seatEntity.RowIndex = seat.RowIndex;
            seatEntity.Status = SeatEntity.SeatStatus.Booked;
            switch (seat.Type)
            {
                case SeatType.Chair:
                    seatEntity.Type = SeatEntity.SeatType.Chair;
                    break;

                default:
                    seatEntity.Type = SeatEntity.SeatType.Aisle;
                    break;
            }
            return seatEntity;
        }

        public static implicit operator Seat(SeatEntity seatEntity)
        {
            Seat seat = new Seat();
            seat.AreaCategoryCode = seatEntity.AreaCategoryCode;
            seat.AreaNumber = seatEntity.AreaNumber;
            seat.ColumnIndex = seatEntity.ColumnIndex;
            seat.Name = seatEntity.Name;
            seat.RowIndex = seatEntity.RowIndex;
            switch (seatEntity.Type)
            {
                case SeatEntity.SeatType.Chair:
                    seat.Type = SeatType.Chair;
                    break;

                default:
                    seat.Type = SeatType.Aisle;
                    break;
            }
            return seat;
        }

        #endregion Methods
    }
}