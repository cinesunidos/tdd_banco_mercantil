﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Infrastructure.Agents.Premium
{
    public partial class Tier
    {
        #region Methods

        public static implicit operator TierEntity(Tier tier)
        {
            TierEntity tierEntity = new TierEntity();
            tierEntity.Name = tier.Name;
            List<SeatEntity> seats = new List<SeatEntity>();
            foreach (var seat in tier.Seats)
            {
                seats.Add((SeatEntity)seat);
            }
            tierEntity.Seats = seats.ToArray();
            return tierEntity;
        }

        #endregion Methods
    }
}