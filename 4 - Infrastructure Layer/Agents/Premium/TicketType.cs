﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;
using System.Linq;

namespace CinesUnidos.ESB.Infrastructure.Agents.Premium
{
    public partial class TicketType
    {
        #region Methods

        public static TicketType[] GetTickets(TicketEntity[] tickets)
        {
            List<TicketType> ticketsType = new List<TicketType>();
            var redemptions = tickets.Where(t => t.RedemptionOnly == true && t.Quantity > 0).ToList(); //lista de boletos a redimir RM
            var purchases = tickets.Where(t => t.RedemptionOnly == false && t.Quantity > 0).ToList(); //lista de Boletos a comprar RM
            #region CodigoSoloCompra
            //foreach (var item in tickets.Where(t => t.Quantity > 0))
            //{
            //    TicketType ticket = new TicketType();
            //    ticket.Price = item.Price;
            //    ticket.Quantity = item.Quantity;
            //    ticket.TicketTypeCode = item.Code;
            //    ticketsType.Add(ticket);
            //} 
            #endregion
            foreach (var item in purchases)
            {
                TicketType ticket = new TicketType
                {
                    Price = item.Price,
                    Quantity = item.Quantity,
                    TicketTypeCode = item.Code
                };
                ticketsType.Add(ticket);
            }

            //RM - si tengo redenciones las recorro y armo un ticketType para cada redencion.
            if (redemptions.Count > 0)
            {
                foreach (var i in redemptions)
                {
                    int index = redemptions.IndexOf(i);
                    var cantidad = i.Quantity;
                    if (cantidad > 1)
                    {
                        var codigos = i.BarcodeRedemption.Split(';');
                        foreach (var codigo in codigos)
                        {
                            TicketType t = new TicketType
                            {
                                Price = i.Price,
                                Quantity = 1,
                                TicketTypeCode = i.Code,
                                TicketBarcode = codigo
                            };
                            ticketsType.Add(t);
                        }
                    }
                    else
                    {
                        TicketType t = new TicketType
                        {
                            Price = i.Price,
                            Quantity = 1,
                            TicketTypeCode = i.Code,
                            TicketBarcode = i.BarcodeRedemption
                        };
                        ticketsType.Add(t);
                    }
                }
            }

            return ticketsType.ToArray();
        }

        #endregion Methods
    }
}