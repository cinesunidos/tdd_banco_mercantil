﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Infrastructure.Agents.Premium
{
    public partial class Map
    {
        #region Methods

        public static implicit operator MapEntity(Map map)
        {
            MapEntity mapEntity = new MapEntity();
            List<TierEntity> tiers = new List<TierEntity>();
            foreach (var tier in map.Tiers)
            {
                tiers.Add((TierEntity)tier);
            }
            mapEntity.Tiers = tiers.ToArray();
            return mapEntity;
        }

        #endregion Methods
    }
}