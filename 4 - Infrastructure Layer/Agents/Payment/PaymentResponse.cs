﻿using CinesUnidos.ESB.Domain;
using System.Linq;

namespace CinesUnidos.ESB.Infrastructure.Agents.Payment
{
    public partial class PaymentResponse
    {
        #region Methods

        public static implicit operator PaymentResponseEntity(PaymentResponse payment)
        {
            PaymentResponseEntity paymentResponse = new PaymentResponseEntity();
            paymentResponse.BookingNumber = payment.BookingNumber;
            paymentResponse.CodeResult = payment.CodeResult;
            paymentResponse.DescriptionResult = payment.DescriptionResult;            
            paymentResponse.BookingId= payment.BookingId;
            if (payment.Voucher != null)
                paymentResponse.Voucher = payment.Voucher.ToArray();
            return paymentResponse;
        }

        #endregion Methods
    }
}