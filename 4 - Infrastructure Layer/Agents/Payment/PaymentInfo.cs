﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Payment
{
    public partial class PaymentInfo
    {
        #region Methods

        public static implicit operator PaymentInfo(PaymentEntity paymentEntity)
        {
            PaymentInfo payment = new PaymentInfo();
            payment.CardCVV = paymentEntity.SecurityCode;
            payment.CardExpiryMonth = paymentEntity.Month;
            payment.CardExpiryYear = paymentEntity.Year;
            payment.CardNumber = paymentEntity.Number;
            payment.Email = paymentEntity.Email;
            payment.Name = paymentEntity.Name;
            payment.Phone = paymentEntity.Phone;
            payment.IDCard = string.Format("{0}{1}", paymentEntity.CertificateType, paymentEntity.Certificate);
            payment.VippoToken = paymentEntity.VippoToken;
            payment.VippoReference = paymentEntity.VippoReference;
            payment.VippoSessionToken = paymentEntity.VippoSessionToken;
            payment.BDVReference = paymentEntity.BDVReference;
            payment.BDVAccountType = paymentEntity.BDVAccountType;
            payment.BDVToken = paymentEntity.BDVToken;
            payment.AccountType = paymentEntity.AccountType;
            payment.KeyAuth = paymentEntity.KeyAuth;
            

            switch (paymentEntity.Type)
            {
                case PaymentEntity.TypeCreditCard.Amex:
                    payment.CardType = PaymentCardType.AMERICANEXPRESS;
                    break;

                case PaymentEntity.TypeCreditCard.Master:
                    payment.CardType = PaymentCardType.MASTERCARD;
                    break;

                case PaymentEntity.TypeCreditCard.Visa:
                    payment.CardType = PaymentCardType.VISA;
                    break;

                case PaymentEntity.TypeCreditCard.PAGOVIPPO:
                    payment.CardType = PaymentCardType.PAGOVIPPO;
                    break;

                case PaymentEntity.TypeCreditCard.PAGOVZLATDD:
                    payment.CardType = PaymentCardType.PAGOVZLATDD;
                    break;
                case PaymentEntity.TypeCreditCard.PAGOVZLATDC:
                    payment.CardType = PaymentCardType.PAGOVZLATDC;
                    break;
                case PaymentEntity.TypeCreditCard.PAGOMERCTDD:
                    payment.CardType = PaymentCardType.PAGOMERCTDD;
                    break;
            }

            return payment;
        }

        #endregion Methods
    }
}