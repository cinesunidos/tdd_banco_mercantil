﻿using CinesUnidos.ESB.Domain;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Infrastructure.Agents.Data
{
    public partial class Theater
    {
        #region Methods

        public static explicit operator TheaterEntity(Theater theater)
        {
            if (theater != null)
            {
                TheaterEntity theaterEntity = new TheaterEntity();
                theaterEntity.Id = theater.ID;
                theaterEntity.Name = theater.FullName;
                theaterEntity.Address = theater.Address;
                theaterEntity.Phone = theater.Phone;
                theaterEntity.Longitude = theater.Longitude;
                theaterEntity.Latitude = theater.Latitude;
                theaterEntity.City = theater.City;
                List<FacilityEntity> facilities = new List<FacilityEntity>();
                foreach (var aitem in theater.P_Facilities)
                {
                    FacilityEntity facility = (FacilityEntity)aitem;
                    facilities.Add(facility);
                }
                theaterEntity.Facilities = facilities.ToArray();
                return theaterEntity;
            }
            else
                return null;
        }

        #endregion Methods
    }
}