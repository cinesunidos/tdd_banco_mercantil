﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Data
{
    public partial class Session
    {
        public static explicit operator SessionEntity(Session session)
        {
            if (session != null)
            {
                SessionEntity obj = new SessionEntity();
                obj.ShowTime = session.ShowTime;
                obj.Id = session.ID;
                obj.HallName = session.HallName;
                obj.HallNumber = session.HallNumber;
                obj.SeatAllocated = session.SeatAllocated;
                return obj;
            }
            else
                return null;
        }
    }
}