﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Data
{
    public partial class Movie
    {
        #region Methods

        public static explicit operator PremierEntity(Movie movie)
        {
            if (movie != null)
            {
                PremierEntity premiere = new PremierEntity();
                premiere.Information = new InformationEntity()
                {
                    Gender = movie.Gender,
                    Time = movie.Duration,
                    Format = movie.Format,
                    Class = movie.Censor,
                    Subtitle = movie.Subtitle,
                    Trailer = movie.Trailer,
                    Web = movie.OfficialSite
                };
                premiere.Country = movie.OriginCountry;
                premiere.Date = movie.FirstExhibit;
                premiere.Title = movie.SpanishName;
                premiere.Sinopsis = movie.Synopsis;
                premiere.Director = movie.Director;
                premiere.Actor = movie.Actor;
                premiere.Id = movie.ID;
                return premiere;
            }
            else
                return null;
        }

        public static explicit operator MovieEntity(Movie movie)
        {
            if (movie != null)
            {
                MovieEntity obj = new MovieEntity();
                obj.Title = movie.SpanishName;
                obj.Information = new InformationEntity()
                {
                    Gender = movie.Gender,
                    Time = movie.Duration,
                    Format = movie.Format,
                    Class = movie.Censor,
                    Subtitle = movie.Subtitle,
                    Web = movie.OfficialSite,
                    Trailer = movie.Trailer
                };
                obj.Country = movie.OriginCountry;
                obj.Id = movie.ID;
                obj.Sinopsis = movie.Synopsis;
                obj.Director = movie.Director;
                obj.Actor = movie.Actor;
                obj.FirstExhibit = movie.FirstExhibit;
                return obj;
            }
            else
            {
                return null;
            }
        }

        #endregion Methods
    }
}