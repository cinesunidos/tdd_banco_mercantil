﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Data
{
    public partial class MoviePack
    {
        [System.Runtime.Serialization.DataMemberAttribute()]
        public TheaterEntity Theater { get; set; }
    }
}