﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Data
{
    public partial class Facilities
    {
        #region Methods

        public static explicit operator FacilityEntity(Facilities facility)
        {
            if (facility != null)
            {
                FacilityEntity facilityEntity = new FacilityEntity();
                facilityEntity.Id = facility.ID;
                facilityEntity.Name = facility.Name;
                return facilityEntity;
            }
            else
                return null;
        }

        #endregion Methods
    }
}