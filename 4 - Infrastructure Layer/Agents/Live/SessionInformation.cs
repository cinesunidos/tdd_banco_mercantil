﻿using CinesUnidos.ESB.Domain;
using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Infrastructure.Agents.Live
{
    public partial class SessionInformation
    {
        #region Methods

        public static implicit operator SessionInfoEntity(SessionInformation sessionReference)
        {
            SessionInfoEntity session = new SessionInfoEntity();
            session.HallName = sessionReference.HallName;
            session.MaxSeatsPerTransaction = sessionReference.MaxSeatsPerTransaction;
            session.SeatAllocation = sessionReference.SeatAllocation;
            session.SeatsAvailable = sessionReference.SeatsAvailable;

            #region Cambio de Horario

            //TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            //DateTime date = TimeZoneInfo.ConvertTime(sessionReference.ShowTime, timeZone);
            //session.ShowTime = date;
            session.ShowTime = sessionReference.ShowTime;

            #endregion Cambio de Horario
            List<TicketEntity> tickets = new List<TicketEntity>();
            foreach (var item in sessionReference.Tickets)
            {
                tickets.Add((TicketEntity)item);
            }
            session.Tickets = tickets.ToArray();
            return session;
        }

        #endregion Methods
    }
}