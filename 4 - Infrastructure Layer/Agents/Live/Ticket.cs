﻿using CinesUnidos.ESB.Domain;

namespace CinesUnidos.ESB.Infrastructure.Agents.Live
{
    public partial class Ticket
    {
        #region Methods

        public static implicit operator TicketEntity(Ticket ticketReference)
        {
            TicketEntity ticket = new TicketEntity();
            ticket.RedemptionOnly = ticketReference.RedemptionOnly;
            ticket.BookingFee = ticketReference.BookingFee;
            ticket.ServiceFee = ticketReference.ServiceFee;
            ticket.ServiceFeeWithTax = ticketReference.TotalServiceFee;
            ticket.Name = ticketReference.Name;
            ticket.Price = ticketReference.Price;

            ticket.PromotionFee = ticketReference.PromotionFee;
            ticket.PromotionFeeWithTax = ticketReference.TotalPromotionFee;
            ticket.PromotionTax = ticketReference.PromotionTax;

            // Añadido Precio Base sin IVA
            // IM: Añadido/Modificado 2016-06-28
            ticket.BasePrice = ticketReference.BasePrice;
            ticket.FullPrice = ticketReference.FullPrice;
            // Añadido Precio Base sin IVA
            // GP: Añadido/Modificado 2016-11-02
            ticket.ServiceTax = ticketReference.ServiceTax;
            ticket.BookingTax = ticketReference.BookingTax;
            ticket.TicketTax = ticketReference.TicketTax;

            ticket.Tax = ticketReference.Tax; // Es el tax total
            ticket.Code = ticketReference.Code;
            return ticket;
        }

        #endregion Methods
    }
}