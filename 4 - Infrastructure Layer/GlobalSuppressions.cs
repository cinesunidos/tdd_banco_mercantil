[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "type", Target = "CinesUnidos.ESB.Infrastructure.DataAccess.UnitOfWork")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "member", Target = "CinesUnidos.ESB.Infrastructure.DataAccess.UnitOfWork.#Dispose()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails", Scope = "member", Target = "CinesUnidos.ESB.Infrastructure.DataAccess.UnitOfWork.#SaveChanges()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails", Scope = "member", Target = "CinesUnidos.ESB.Infrastructure.Repositories.PurchasesRepositoryDB.#Create(CinesUnidos.ESB.Domain.PurchasesEntity)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", Target = "CinesUnidos.ESB.Infrastructure.Repositories.PurchasesRepositoryDB.#Create(CinesUnidos.ESB.Domain.PurchasesEntity)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Scope = "member", Target = "CinesUnidos.ESB.Infrastructure.Repositories.UserSessionRepository.#ReadMobile(System.Guid)")]
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

