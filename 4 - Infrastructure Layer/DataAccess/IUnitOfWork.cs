﻿using System;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        #region Methods

        void SaveChanges();

        IDbCommand CreateCommand();

        #endregion Methods
    }
}