﻿namespace CinesUnidos.ESB.Infrastructure.DataAccess
{
    internal interface IRepository<TEntity>
     where TEntity : new()
    {
        #region Methods

        void Create(TEntity entity);

        void Delete(TEntity entity);

        void Update(TEntity entity);

        #endregion Methods
    }
}