﻿using System;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Attributes

        private IDbTransaction m_transaction;
        private IDbConnection m_connection;

        #endregion Attributes



        #region Methods

        public UnitOfWork(IConnectionFactory connectionFactory)
        {
            m_connection = connectionFactory.Create();
            m_connection.Open();
            m_transaction = m_connection.BeginTransaction();
        }

        public IDbCommand CreateCommand()
        {
            var cmd = m_connection.CreateCommand();
            cmd.Transaction = m_transaction;
            cmd.Connection = m_connection;
            return cmd;
        }

        public void Dispose()
        {
            if (m_transaction != null)
            {
                m_transaction.Rollback();
                m_transaction.Dispose();
            }
            if (m_connection.State == ConnectionState.Open)
            {
                m_connection.Close();
            }
            m_connection.Dispose();
        }

        public void SaveChanges()
        {
            if (m_transaction == null)
                throw new InvalidOperationException("May not call save changes twice.");
            try
            {
                m_transaction.Commit();
            }
            catch (Exception ex)
            {
                m_transaction.Rollback();
                throw ex;
            }
            m_transaction = null;
        }

        #endregion Methods
    }
}