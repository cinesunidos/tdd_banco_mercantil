﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace CinesUnidos.ESB.Infrastructure.DataAccess
{
    public class ConnectionFactory : IConnectionFactory
    {
        #region Attribute

        private readonly DbProviderFactory m_provider;
        private readonly string m_connectionString;
        private readonly string m_name;

        #endregion Attribute

        #region Methods

        public ConnectionFactory(string connectionName)
        {
            if (connectionName == null) throw new ArgumentNullException("connectionName");

            var conStr = ConfigurationManager.ConnectionStrings[connectionName];
            if (conStr == null)
                throw new ConfigurationErrorsException(string.Format("Failed to find connection string named '{0}' in app/web.config.", connectionName));

            m_name = conStr.ProviderName;
            m_provider = DbProviderFactories.GetFactory(conStr.ProviderName);
            m_connectionString = conStr.ConnectionString;
        }

        public IDbConnection Create()
        {
            var connection = m_provider.CreateConnection();
            if (connection == null)
                throw new ConfigurationErrorsException(string.Format("Failed to create a connection using the connection string named '{0}' in app/web.config.", m_name));

            connection.ConnectionString = m_connectionString;
            return connection;
        }

        #endregion Methods
    }
}