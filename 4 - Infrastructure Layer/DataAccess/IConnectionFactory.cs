﻿using System.Data;

namespace CinesUnidos.ESB.Infrastructure.DataAccess
{
    public interface IConnectionFactory
    {
        #region Methods

        IDbConnection Create();

        #endregion Methods
    }
}