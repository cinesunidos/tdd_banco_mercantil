﻿using System.Collections.Generic;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.DataAccess
{
    public abstract class Repository<TEntity> : IRepository<TEntity>
        where TEntity : new()
    {
        #region Attributes

        private IUnitOfWork m_unitOfWork;

        #endregion Attributes

        #region Properties

        protected IUnitOfWork Context { get { return m_unitOfWork; } }

        #endregion Properties

        #region Methods

        public Repository(IUnitOfWork unitOfWork)
        {
            m_unitOfWork = unitOfWork;
        }

        protected IEnumerable<TEntity> ToList(IDbCommand command)
        {
            using (var reader = command.ExecuteReader())
            {
                List<TEntity> items = new List<TEntity>();
                while (reader.Read())
                {
                    var item = new TEntity();
                    Map(reader, item);
                    items.Add(item);
                }
                return items;
            }
        }

        protected abstract void Map(IDataRecord record, TEntity entity);

        public abstract void Create(TEntity entity);

        public abstract void Delete(TEntity entity);

        public abstract void Update(TEntity entity);

        #endregion Methods
    }
}