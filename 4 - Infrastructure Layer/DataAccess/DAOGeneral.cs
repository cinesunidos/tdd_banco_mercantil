﻿using System.Configuration;
using System.Data.SqlClient;

namespace CinesUnidos.ESB.Infrastructure.DataAccess
{
    public class DAOGeneral
    {
        public SqlConnection Connect()
        {
            var conStr = ConfigurationManager.ConnectionStrings[ResourcesGeneralDAO.DataBaseName];
            var m_connectionString = conStr.ConnectionString;
            return new SqlConnection(m_connectionString);
        }
    }
}