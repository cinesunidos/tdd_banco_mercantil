﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    internal class TokenRepository : Repository<TokenEntity>
    {
        #region Methods

        public TokenRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public override void Create(TokenEntity entity)
        {
            using (var command = this.Context.CreateCommand())
            {
                command.CommandText = "Access.RegisterToken";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.Value = Guid.NewGuid();

                IDbDataParameter expirationParameter = command.CreateParameter();
                expirationParameter.ParameterName = "@Expiration";
                expirationParameter.Value = entity.Expiration;

                IDbDataParameter hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = entity.Hash;

                IDbDataParameter plataformParameter = command.CreateParameter();
                plataformParameter.ParameterName = "@Platform";
                plataformParameter.Value = entity.Platform;

                IDbDataParameter loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@LoginId";
                loginParameter.Value = entity.UserId;

                command.Parameters.Add(idParameter);
                command.Parameters.Add(expirationParameter);
                command.Parameters.Add(hashParameter);
                command.Parameters.Add(plataformParameter);
                command.Parameters.Add(loginParameter);

                command.ExecuteNonQuery();
            }
        }

        public override void Update(TokenEntity entity)
        {
            using (var command = this.Context.CreateCommand())
            {
                command.CommandText = "Access.RefreshToken";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                DateTime expiration = DateTime.Now.AddHours(24);
                IDbDataParameter hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = entity.Hash;

                IDbDataParameter expirationParameter = command.CreateParameter();
                expirationParameter.ParameterName = "@Expiration";
                expirationParameter.Value = expiration;

                command.Parameters.Add(hashParameter);
                command.Parameters.Add(expirationParameter);

                command.ExecuteNonQuery();
            }
        }

        public override void Delete(TokenEntity entity)
        {
            using (var command = this.Context.CreateCommand())
            {
                command.CommandText = @"Access.DeleteToken";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                IDbDataParameter hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = entity.Hash;
                command.Parameters.Add(hashParameter);
                command.ExecuteNonQuery();
            }
        }

        public Guid? FindIdUser(string login, string password)
        {
            Guid? Id = null;
            using (var command = this.Context.CreateCommand())
            {
                string sql = "SELECT [ID] FROM [Access].[Active] WHERE [EMAIL] = @Login AND [PASSWORD] = @Password";
                command.CommandText = sql;

                IDbDataParameter loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@Login";
                loginParameter.Value = login;
                IDbDataParameter passwordParameter = command.CreateParameter();
                passwordParameter.ParameterName = "@Password";
                passwordParameter.Value = password;
                command.Parameters.Add(loginParameter);
                command.Parameters.Add(passwordParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        Id = reader.GetGuid(0);
                    }
                }
            }
            return Id;
        }

        public TokenEntity FindToken(string hash)
        {
            TokenEntity token = null;
            using (var command = this.Context.CreateCommand())
            {
                string sql = "SELECT [ID],[EXPIRATION],[HASH],[LOGIN] FROM [Access].[AllToken] WHERE [HASH] = @Hash";
                command.CommandText = sql;

                IDbDataParameter hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = hash;

                command.Parameters.Add(hashParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        token = new TokenEntity();
                        Map(reader, token);
                    }
                }
            }
            return token;
        }

        public TokenEntity FindToken(Guid loginId, string platform)
        {
            TokenEntity token = null;
            using (var command = this.Context.CreateCommand())
            {
                string sql = "SELECT  [Id],[Expiration],[Hash],[LoginId],[Platform]  FROM [Access].[Token] WHERE [LoginId] = @loginId AND [Platform] = @platform";
                command.CommandText = sql;

                IDbDataParameter loginIdParameter = command.CreateParameter();
                loginIdParameter.ParameterName = "@loginId";
                loginIdParameter.Value = loginId;

                IDbDataParameter platformParameter = command.CreateParameter();
                platformParameter.ParameterName = "@platform";
                platformParameter.Value = platform;

                command.Parameters.Add(loginIdParameter);
                command.Parameters.Add(platformParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        token = new TokenEntity();
                        Map(reader, token);
                    }
                }
            }
            return token;
        }

        public TokenInfoEntity FindTokenInfo(string hash)
        {
            TokenInfoEntity token = null;
            using (var command = this.Context.CreateCommand())
            {
                string sql = "SELECT     Access.Token.Expiration, Access.Login.Name, Access.Login.LastName, Access.Login.Email FROM   Access.Login INNER JOIN "
                      + " Access.Token ON Access.Login.Id = Access.Token.LoginId WHERE     (Access.Token.Hash = @Hash)";
                command.CommandText = sql;

                IDbDataParameter hashParameter = command.CreateParameter();
                hashParameter.ParameterName = "@Hash";
                hashParameter.Value = hash;

                command.Parameters.Add(hashParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        token = new TokenInfoEntity();
                        Map(reader, token);
                        token.Hash = hash;
                    }
                }
            }
            return token;
        }

        protected override void Map(System.Data.IDataRecord record, TokenEntity entity)
        {
            entity.Id = record.GetGuid(0);
            entity.Expiration = record.GetDateTime(1);
            entity.Hash = record.GetString(2);
            entity.UserId = record.GetGuid(3);
        }

        protected void Map(System.Data.IDataRecord record, TokenInfoEntity entity)
        {
            entity.Expiration = record.GetDateTime(0);
            entity.Name = record.GetString(1);
            entity.LastName = record.GetString(2);
            entity.Email = record.GetString(3);
        }

        /// <summary>
        /// Función para validar si el email existe.
        /// </summary>
        /// <param name="email">Email a validar</param>
        /// <returns>True si existe, false si no.</returns>
        public bool ExistEmail(string email)
        {
            bool exist = false;
            using (var command = this.Context.CreateCommand())
            {
                string sql = "SELECT [ID] FROM [Access].[Login] WHERE [EMAIL] = @email";
                command.CommandText = sql;

                IDbDataParameter loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@email";
                loginParameter.Value = email;
                command.Parameters.Add(loginParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        exist = true;
                    }
                }
            }
            return exist;
        }

        /// <summary>
        /// Función para validar si el email fue confirmado.
        /// </summary>
        /// <param name="email">Email a validar</param>
        /// <returns>True si existe, false si no.</returns>
        public bool IsUserActive(string email)
        {
            bool ret = false;
            using (var command = this.Context.CreateCommand())
            {
                string sql = "SELECT [Active] FROM [Access].[Login] WHERE [EMAIL] = @email";
                command.CommandText = sql;

                IDbDataParameter loginParameter = command.CreateParameter();
                loginParameter.ParameterName = "@email";
                loginParameter.Value = email;
                command.Parameters.Add(loginParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        ret = reader.GetBoolean(0);
                    }
                }
            }
            return ret;
        }

        #endregion Methods
    }
}