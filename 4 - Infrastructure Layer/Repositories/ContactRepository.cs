﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Queue;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class ContactRepository : IContactRepository //: Repository<ComplaintEntity>
    {
        private IQueue m_queue;

        public ContactRepository(IQueue @queue)
        {
            m_queue = queue;
        }

        #region Methods

        public ResponseEntity SendComplaint(ComplaintEntity complaint)
        {
            ResponseEntity ResponseEntity = new ResponseEntity();
            try
            {
                // todo: aquí se debe enviar el correo de la queja
                Services.CRM.Library.BL.Comment Comment = new Services.CRM.Library.BL.Comment();
                Services.CRM.Library.BO.Complaint Complaint = new Services.CRM.Library.BO.Complaint();
                Complaint.Comment = complaint.Comment;

                Complaint.AreaCode = complaint.PhoneCode;
                Complaint.Category = complaint.Subject;
                Complaint.ID = complaint.IDCard;// Verificar la Cedula
                Complaint.City = "";
                Complaint.Comment = complaint.Comment;
                Complaint.CountryCode = 58;
                Complaint.Email = complaint.Email;
                Complaint.LastName = complaint.LastName;
                Complaint.Movie = complaint.Movie;
                Complaint.MovieDate = complaint.Date.Value;
                Complaint.MovieTime = complaint.Time.Value;
                Complaint.Name = complaint.Name;
                Complaint.PhoneNumber = complaint.Phone; //validar que sea entero
                Complaint.PhoneType = "";
                Complaint.Theater = complaint.Theater;

                Comment.SaveComplaint(Complaint);

                //Sacar los destinatarios del correo

                //Correo Para el Usuario

                MailingServiceEntity mail_service_obj = new MailingServiceEntity();
                mail_service_obj.Tipo = "Complain";
                mail_service_obj.User = null;
                mail_service_obj.Complain = complaint;
                m_queue.InsertMessage<MailingServiceEntity>("services", mail_service_obj);

                //Correo Para el Departamento
                //Temporalemnte esta cableado mientras se cree una entidad que involucre al usuario (email) y departamentos
                MailingServiceEntity mail_service_obj_2 = new MailingServiceEntity();
                mail_service_obj_2.Tipo = "Department";
                mail_service_obj_2.User = null;
                mail_service_obj_2.Complain = complaint;
                mail_service_obj_2.Complain.Comment = complaint.Comment; //complaint.Email + "</br>" + complaint.Comment;
                mail_service_obj_2.Complain.Email = Comment.getTo(Complaint.Category);// TODO contemplar enviar uno por uno los correos de grupo
                m_queue.InsertMessage<MailingServiceEntity>("services", mail_service_obj_2);

                ResponseEntity.isOk = true;
                return ResponseEntity;
            }
            catch
            {
                ResponseEntity.isOk = false;
                return ResponseEntity;
            }
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}