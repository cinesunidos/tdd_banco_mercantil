﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Configuration;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class TheaterBillboardRepository : ITheaterBillboardRepository
    {
        #region Attributos

        private ICache<TheaterBillboardEntity[]> m_cache;
        private ITheaterRepository m_theaterRepository;

        #endregion Attributos

        #region Methods

        public TheaterBillboardRepository(ICache<TheaterBillboardEntity[]> cache, ITheaterRepository theaterRepository)
        {
            m_cache = cache;
            m_theaterRepository = theaterRepository;
        }

        public TheaterBillboardEntity GetById(string theaterId)
        {
            TheaterBillboardEntity result = new TheaterBillboardEntity();
            TheaterBillboardEntity billboard = this.Data.Where(b => b.Theater.Id.Equals(theaterId)).FirstOrDefault();
            if (billboard != null)
            {
                result = billboard.Clone();



                #region Busqueda de las funciones que cumplen con la fecha
                foreach (var item in billboard.Movies)
                {
                    var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(billboard.Dates.First().Month) && s.ShowTime.Day.Equals(billboard.Dates.First().Day)).ToList();
                    sessions = ValidateDate(sessions);
                    if (sessions.Count > 0)
                    {
                        TheaterBillboardMovieEntity movie = new TheaterBillboardMovieEntity();
                        movie.Movie = item.Movie.Clone();
                        movie.Sessions = sessions;
                        result.Movies.Add(movie);
                    }
                }
                #endregion Busqueda de las funciones que cumplen con la fecha
            }
            return result;
        }

        public TheaterBillboardEntity GetById(string theaterId, int day, int month)
        {
            TheaterBillboardEntity result = new TheaterBillboardEntity();
            TheaterBillboardEntity billboard = this.Data.Where(b => b.Theater.Id.Equals(theaterId)).FirstOrDefault(); //Busqueda por Id de cine
            if (billboard != null)
            {
                result = billboard.Clone();

                #region Busqueda de las funciones que cumplen con la fecha

                foreach (var item in billboard.Movies)
                {
                    var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();
                    sessions = ValidateDate(sessions);
                    if (sessions.Count > 0)
                    {
                        TheaterBillboardMovieEntity movie = new TheaterBillboardMovieEntity();
                        movie.Movie = item.Movie.Clone();
                        movie.Sessions = sessions;
                        result.Movies.Add(movie);
                    }
                }

                #endregion Busqueda de las funciones que cumplen con la fecha
            }
            return result;
        }

        public TheaterBillboardEntity[] GetByCity(string city, int day, int month)
        {
            List<TheaterBillboardEntity> result = new List<TheaterBillboardEntity>();
            List<TheaterBillboardEntity> billboards = this.Data.Where(b => b.Theater.City.Equals(city)).ToList();
            if (billboards.Count() > 0)
            {
                foreach (var aitem in billboards)
                {
                    TheaterBillboardEntity theater = aitem.Clone();

                    #region Busqueda de las funciones que cumplen con la fecha

                    foreach (var item in aitem.Movies)
                    {
                        var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();
                        sessions = ValidateDate(sessions);
                        if (sessions.Count > 0)
                        {
                            TheaterBillboardMovieEntity movie = new TheaterBillboardMovieEntity();
                            movie.Movie = item.Movie.Clone();
                            movie.Sessions = sessions;
                            theater.Movies.Add(movie);
                        }
                    }

                    #endregion Busqueda de las funciones que cumplen con la fecha

                    result.Add(theater);
                }
            }
            return result.ToArray();
        }

        public TheaterBillboardEntity[] GetByCity(string city)
        {
            List<TheaterBillboardEntity> result = new List<TheaterBillboardEntity>();
            List<TheaterBillboardEntity> billboards = this.Data.Where(b => b.Theater.City.Equals(city)).ToList();
            if (billboards.Count() > 0)
            {
                foreach (var aitem in billboards)
                {
                    TheaterBillboardEntity theater = aitem.Clone();
                    theater.Theater = aitem.Theater;
                    #region Busqueda de las funciones que cumplen con la fecha

                    foreach (var item in aitem.Movies)
                    {
                        var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(aitem.Dates.First().Month) && s.ShowTime.Day.Equals(aitem.Dates.First().Day)).ToList();
                        sessions = ValidateDate(sessions);
                        if (sessions.Count > 0)
                        {
                            // Sala es premium?
                            //ConnectionFactory m_factory = new ConnectionFactory("ELMAH");
                            //List<SessionEntity> sessionsEx = new List<SessionEntity>();
                            //using (var uow = new UnitOfWork(m_factory))
                            //{
                            //    HallRepository hRepo = new HallRepository(uow);

                            //    // Para cada sesión se busca si la sala es premium
                            //    foreach (SessionEntity sess in sessions)
                            //    {
                            //        SessionEntity sessEx = sess;
                            //        sessEx = hRepo.Read(sess, theater.Theater.Id);
                            //        sessionsEx.Add(sessEx);
                            //    }
                            //}

                            // Retorno
                            TheaterBillboardMovieEntity movie = new TheaterBillboardMovieEntity();
                            movie.Movie = item.Movie.Clone();
                            movie.Movie = item.Movie;
                            movie.Sessions = sessions;
                            movie.Sessions = sessions;
                            theater.Movies.Add(movie);
                        }
                    }

                    #endregion Busqueda de las funciones que cumplen con la fecha

                    result.Add(theater);
                }
            }
            return result.ToArray();
        }

        public string[] GetAllCity()
        {
            return m_theaterRepository.Data.Select(t => t.City).Distinct().OrderBy(s => s).ToArray();
        }

        public MoviePack GetAllMoviesCache(string itemId)
        {
            MoviePack[] moviePack;
            //llamo al servicio de la capa de vista y almaceno en cache
            //todo el contenido del resultset para no hacer 24 llamados al servicio web
            //cuando sea necesario segun fecha de expiración de la cache
            //la refresco.

            //consulto a la cache de movies si esta actualizada la consulto por cine.
            //colocar codigo aqui

            //si no esta actualizada voy a la capa de vista y consulto toda la informacion
            //pero una vez.

            String filePath = String.Empty;

            //using (DataClient client = new DataClient())
            //{
            //    moviePack = client.GetAllMoviesCache().ToList();
            //    string json = JsonConvert.SerializeObject(moviePack);
            //    File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/AllMoviesLocal.xml"), json);
            //    // TheaterEntity[] lista = JsonConvert.DeserializeObject<TheaterEntity[]>(File.WriteAllText(filePath));
            //}

//#if DEBUG
            filePath = HostingEnvironment.MapPath("~/App_Data/AllMoviesLocal.xml");
//#endif

//#if (!DEBUG)
//            string cachePath = RoleEnvironment.GetLocalResource("AllMoviesLocal").RootPath;
//            filePath = Path.Combine(cachePath, "AllMoviesLocal.xml");
//#endif
            if (!File.Exists(filePath))
                File.Create(filePath).Close();


            Boolean valido = CompararHoras(filePath);
            FileInfo file = new FileInfo(filePath);

            if (valido || file.Length < 20)
            {
                using (DataClient client = new DataClient())
                {
                    moviePack = client.GetAllMoviesCache().ToArray();
                    string json = JsonConvert.SerializeObject(moviePack);
                    //File.WriteAllText(HostingEnvironment.MapPath(filePath), json);
                    File.WriteAllText(filePath, json);
                }

            }
            else
            {
                moviePack = JsonConvert.DeserializeObject<MoviePack[]>(File.ReadAllText(filePath));
            }

            var lista = moviePack.Where(t => t.TheaterID == itemId).FirstOrDefault();
            return lista;
        }


        private TheaterBillboardEntity[] Execute()
        {
            List<MoviePack> billboard = new List<MoviePack>();
            List<TheaterBillboardEntity> theaters = new List<TheaterBillboardEntity>();

            string filePath = string.Empty;
            //List<MoviePack> billboard = new List<MoviePack>();            

            //#if DEBUG
            filePath = HostingEnvironment.MapPath("~/App_Data/Theaters.xml");
            //#endif

            //#if (!DEBUG)
            //            string cachePath = RoleEnvironment.GetLocalResource("Theaters").RootPath;
            //            filePath = Path.Combine(cachePath, "Theaters.xml");
            //#endif
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
                TheaterEntity[] theater_array = m_theaterRepository.Data;
                theater_array = null;
            }

            TheaterEntity[] lista = JsonConvert.DeserializeObject<TheaterEntity[]>(File.ReadAllText(filePath));
            //List<MovieBillboardEntity> movies = new List<MovieBillboardEntity>();

            try
            {


                using (DataClient client = new DataClient())
                {
                    //foreach (var item in m_theaterRepository.Data)
                    foreach (var item in lista)
                    {
                        //MoviePack moviePack = client.GetAllMovies(item.Id);
                        MoviePack moviePack = GetAllMoviesCache(item.Id);
                        // moviePack.Theater = item.Clone();

                        TheaterEntity theater = new TheaterEntity();
                        theater.Id = item.Id;
                        theater.Name = item.Name;
                        theater.Address = item.Address;
                        theater.Latitude = item.Latitude;
                        theater.Longitude = item.Longitude;
                        theater.City = item.City;
                        theater.Phone = item.Phone;
                        //theater.Censor = moviePack.Theater.Censor;             

                        List<FacilityEntity> facilities = new List<FacilityEntity>();

                        foreach (var item2 in item.Facilities)
                        {
                            FacilityEntity facility = new FacilityEntity();
                            facility.Id = item2.Id;
                            facility.Name = item2.Name;
                            facilities.Add(facility);
                        }
                        theater.Facilities = facilities.ToArray();
                        //return theater;
                        moviePack.Theater = theater;

                        billboard.Add(moviePack);
                    }
                    //  client.Close();
                }


                foreach (var aitem in billboard)
                {
                    TheaterBillboardEntity theater = new TheaterBillboardEntity();
                    //theater.Theater = aitem.Theater.Clone();
                    theater.Theater = aitem.Theater;
                    foreach (var bitem in aitem.Movies)
                    {
                        TheaterBillboardMovieEntity movie = new TheaterBillboardMovieEntity();
                        movie.Movie = (MovieEntity)bitem;
                        foreach (var citem in bitem.Sessions)
                        {
                            SessionEntity session = (SessionEntity)citem;
                            session.Censor = citem.Censor;
                            movie.Sessions.Add(session);
                        }
                        theater.Movies.Add(movie);
                    }
                    theater.GetDates();
                    theaters.Add(theater);
                }
            }
            catch (Exception ex)
            {
                //  var ai = new TelemetryClient();
                //ai.TrackException(ex);
            }
            return theaters.ToArray();
        }

        public bool CompararHoras(string filename)
        {
            ////  var fecha = DateTime.Now.AddHours(-hours);
            ////return System.IO.File.GetLastWriteTime(filename).AddHours(-hours) <= fecha;
            int hours = int.Parse(ConfigurationManager.AppSettings["TimeCache"].ToString());
            bool value;

            DateTime DateSystem = DateTime.Now;
            DateTime DateFile = System.IO.File.GetLastWriteTime(filename);

            TimeSpan result = DateSystem - DateFile;

            if (result.TotalHours >= hours) value = true; else value = false;

            return value;
        }


        private TheaterBillboardEntity[] Load()
        {
            TheaterBillboardEntity[] theaterBillboards;
            string filePath = "";
            //theaterBillboards = m_cache.Get("TheaterBillboard");
            //if (theaterBillboards == null || theaterBillboards.Count() == 0)
            //{
            //    theaterBillboards = Execute();
            //    m_cache.Set("TheaterBillboard", theaterBillboards, TimeSpan.FromMinutes(240));
            //}
            //m_cache.Close();

            //#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_data/TheaterBillboard.xml"));
            //#endif

            //#if (!DEBUG)
            //            string cachePath = RoleEnvironment.GetLocalResource("TheaterBillboard").RootPath;
            //            filePath = Path.Combine(cachePath, "TheaterBillboard.xml");
            //#endif                  
            if (!File.Exists(filePath))
                File.Create(filePath).Close();

            Boolean valido = CompararHoras(filePath);
            FileInfo file = new FileInfo(filePath);

            if (valido || file.Length < 20)
            {
                theaterBillboards = Execute();
                string json = JsonConvert.SerializeObject(theaterBillboards);
                File.WriteAllText(filePath, json);
            }
            else
            {
                theaterBillboards = JsonConvert.DeserializeObject<TheaterBillboardEntity[]>(File.ReadAllText(filePath));
            }

            return theaterBillboards;
        }

        public void Clear()
        {
            m_cache.Delete("TheaterBillboard");
            m_cache.Close();
        }

        private List<SessionEntity> ValidateDate(List<SessionEntity> sessions)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            DateTime dateServer = DateTime.UtcNow;
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(dateServer, timeZone);
            List<SessionEntity> functions = sessions.Where(s => s.ShowTime.AddMinutes(20) > date).ToList();
            return functions;
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        public TheaterBillboardEntity[] Data
        {
            get
            {
                TheaterBillboardEntity[] theaterBillboards = Load();
                return theaterBillboards;
            }
        }

        #endregion Properties
    }
}