﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using CinesUnidos.ESB.Infrastructure.Agents.Live;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
//using System.Diagnostics;
using System.Linq;
using System.Web.Hosting;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Configuration;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class MovieBillboardRepository : IMovieBillboardRepository
    {
        #region Attributos

        private ICache<MovieBillboardEntity[]> m_cache;
        private ITheaterRepository m_theaterRepository;

        #endregion Attributos

        #region Methods

        public MovieBillboardRepository(ICache<MovieBillboardEntity[]> cache, ITheaterRepository theaterRepository)
        {
            m_cache = cache;
            m_theaterRepository = theaterRepository;
        }


        public MoviePack GetAllMoviesCache(string itemId)
        {
            MoviePack[] moviePack;
            //llamo al servicio de la capa de vista y almaceno en cache
            //todo el contenido del resultset para no hacer 24 llamados al servicio web
            //cuando sea necesario segun fecha de expiración de la cache
            //la refresco.

            //consulto a la cache de movies si esta actualizada la consulto por cine.
            //colocar codigo aqui

            //si no esta actualizada voy a la capa de vista y consulto toda la informacion
            //pero una vez.

            String filePath = String.Empty;

            //using (DataClient client = new DataClient())
            //{
            //    moviePack = client.GetAllMoviesCache().ToList();
            //    string json = JsonConvert.SerializeObject(moviePack);
            //    File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/AllMoviesLocal.xml"), json);
            //    // TheaterEntity[] lista = JsonConvert.DeserializeObject<TheaterEntity[]>(File.WriteAllText(filePath));
            //}

            //#if DEBUG
            filePath = HostingEnvironment.MapPath("~/App_Data/AllMoviesLocal.xml");
            //#endif

            //#if (!DEBUG)
            //                string cachePath = RoleEnvironment.GetLocalResource("AllMoviesLocal").RootPath;
            //                filePath = Path.Combine(cachePath, "AllMoviesLocal.xml");
            //#endif

            if (!File.Exists(filePath))
                File.Create(filePath).Close();


            Boolean valido = CompararHoras(filePath);
            FileInfo file = new FileInfo(filePath);

            if (valido || file.Length < 20)
            {
                using (DataClient client = new DataClient())
                {
                    moviePack = client.GetAllMoviesCache().ToArray();
                    string json = JsonConvert.SerializeObject(moviePack);
                    //File.WriteAllText(HostingEnvironment.MapPath(filePath), json);
                    File.WriteAllText(filePath, json);
                }

            }
            else
            {
                moviePack = JsonConvert.DeserializeObject<MoviePack[]>(File.ReadAllText(filePath));
            }
            var lista = moviePack.Where(t => t.TheaterID == itemId).FirstOrDefault();
            lista.Movies = lista.Movies.OrderByDescending(m => m.FirstExhibit).ToArray();
            return lista;

            //var lista =  moviePack.Where(t=>t.TheaterID == itemId).FirstOrDefault();
            //return lista;
        }


        private MovieBillboardEntity[] Execute()
        {
            List<MoviePack> billboard = new List<MoviePack>();
            string filePath = String.Empty;
            //#if DEBUG
            filePath = HostingEnvironment.MapPath("~/App_Data/Theaters.xml");
            //#endif

            //#if (!DEBUG)
            //                string cachePath = RoleEnvironment.GetLocalResource("Theaters").RootPath;
            //                filePath = Path.Combine(cachePath, "Theaters.xml");
            //#endif
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
                TheaterEntity[] theater_array = m_theaterRepository.Data;
                theater_array = null;
            }
            TheaterEntity[] lista = JsonConvert.DeserializeObject<TheaterEntity[]>(File.ReadAllText(filePath));
            List<MovieBillboardEntity> movies = new List<MovieBillboardEntity>();

            try
            {
                foreach (var item in lista)
                {
                    //MoviePack moviePack = client.GetAllMovies(item.Id);
                    MoviePack moviePack = GetAllMoviesCache(item.Id);

                    TheaterEntity theater = new TheaterEntity();
                    theater.Id = item.Id;
                    theater.Name = item.Name;
                    theater.Address = item.Address;
                    theater.Latitude = item.Latitude;
                    theater.Longitude = item.Longitude;
                    theater.City = item.City;
                    theater.Phone = item.Phone;
                    //theater.Censor = moviePack.Theater.Censor;             

                    //List<FacilityEntity> facilities = new List<FacilityEntity>();

                    #region No Necesario
                    //foreach (var item2 in item.Facilities)
                    //{
                    //    FacilityEntity facility = new FacilityEntity();
                    //    facility.Id = item2.Id;
                    //    facility.Name = item2.Name;
                    //    facilities.Add(facility);
                    //}
                    //theater.Facilities = facilities.ToArray(); 
                    #endregion

                    theater.Facilities = item.Facilities;
                    //return theater;
                    moviePack.Theater = theater;
                    billboard.Add(moviePack);
                }

                foreach (var aitem in billboard)
                {
                    foreach (var bitem in aitem.Movies)
                    {
                        MovieBillboardEntity movie = movies.Where(m => m.Movie.Id.Equals(bitem.ID)).FirstOrDefault();
                        //MovieBillboardEntity movie = movies.Where(m => (m.Movie.Id == bitem.ID && m.Movie.Information.Class == bitem.Censor)).FirstOrDefault();
                        if (movie == null)
                        {
                            movie = new MovieBillboardEntity();
                            movie.Movie = (MovieEntity)bitem;
                            movies.Add(movie);
                        }
                        MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                        theater.Theater = aitem.Theater;

                        #region cambio a paralel
                        foreach (var citem in bitem.Sessions)
                        {
                            SessionEntity session = (SessionEntity)citem;
                            session.Censor = citem.Censor;
                            theater.Sessions.Add(session);
                        }
                        #endregion

                        //Parallel.ForEach(bitem.Sessions, citem =>
                        //{
                        //    SessionEntity session = (SessionEntity)citem;
                        //    session.Censor = citem.Censor;
                        //    theater.Sessions.Add(session);
                        //});

                        //movie.Add(theater); //es el add sobreescrito
                        if (movie.Theaters != null)
                        {
                            MovieBillboardTheaterEntity temp = movie.Theaters.Where(t => t.Theater.Id.Equals(theater.Theater.Id)).FirstOrDefault();
                            if (temp != null)
                                temp.Sessions.AddRange(theater.Sessions);
                            else
                                movie.Theaters.Add(theater);
                        }
                        else
                        {
                            movie.Theaters = new List<MovieBillboardTheaterEntity>();
                            movie.Theaters.Add(theater);
                        }

                    }

                }

                foreach (var movie in movies)
                {
                    movie.GetDates();
                }
            }
            catch (Exception ex)
            {
                var mensaje = ex.Message;
            }

            return movies.ToArray();
        }

        public MovieBillboardEntity[] GetByDate(string city, int day, int month)
        {
            List<MovieBillboardEntity> result = new List<MovieBillboardEntity>();
            List<MovieBillboardEntity> billboards = this.Data.Where(m => m.Theaters.Where(t => t.Theater.City.Equals(city)).Count() > 0).ToList();
            if (billboards.Count > 0)
            {
                #region cambio a paralel
                foreach (var aitem in billboards)
                {
                    // MovieBillboardEntity movie = aitem.Clone();

                    MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
                    // movieBillboard.Movie = aitem.Movie.Clone();
                    MovieEntity movie2 = new MovieEntity();
                    movie2.Actor = aitem.Movie.Actor;
                    movie2.Country = aitem.Movie.Country;
                    movie2.Director = aitem.Movie.Director;
                    movie2.Id = aitem.Movie.Id;
                    //movie2.Information = aitem.Movie.Information.Clone();
                    InformationEntity information = new InformationEntity();
                    information.Class = aitem.Movie.Information.Class;
                    information.Format = aitem.Movie.Information.Format;
                    information.Gender = aitem.Movie.Information.Gender;
                    information.Subtitle = aitem.Movie.Information.Subtitle;
                    information.Time = aitem.Movie.Information.Time;
                    information.Trailer = aitem.Movie.Information.Trailer;
                    information.Web = aitem.Movie.Information.Web;
                    movie2.Information = information;
                    movie2.Sinopsis = aitem.Movie.Sinopsis;
                    movie2.Title = aitem.Movie.Title;
                    movie2.FirstExhibit = aitem.Movie.FirstExhibit;
                    movieBillboard.Movie = movie2;

                    #region Cambio variables
                    List<DateTime> dates = new List<DateTime>();
                    foreach (var date in aitem.Dates)
                    {
                        dates.Add(date);
                    }
                    movieBillboard.Dates = dates.ToArray();
                    #endregion

                    //movieBillboard.Dates = aitem.Dates.ToArray();

                    MovieBillboardEntity movie = movieBillboard;

                    #region cambio a parallel
                    foreach (var item in aitem.Theaters.Where(t => t.Theater.City.ToUpper().Equals(city.ToUpper())))
                    {
                        var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();
                        sessions = ValidateDate(sessions);
                        if (sessions.Count > 0)
                        {
                            MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                            //theater.Theater = item.Theater.Clone();

                            TheaterEntity theater2 = new TheaterEntity();
                            theater2.Id = item.Theater.Id;
                            theater2.Name = item.Theater.Name;
                            theater2.Address = item.Theater.Address;
                            theater2.Latitude = item.Theater.Latitude;
                            theater2.Longitude = item.Theater.Longitude;
                            theater2.City = item.Theater.City;
                            theater2.Phone = item.Theater.Phone;

                            #region cambio a variable
                            List<FacilityEntity> facilities = new List<FacilityEntity>();
                            foreach (var item3 in item.Theater.Facilities)
                            {
                                facilities.Add(item3.Clone());
                            }
                            theater2.Facilities = facilities.ToArray();
                            #endregion

                            //theater2.Facilities = item.Theater.Facilities;
                            theater.Theater = theater2;

                            theater.Sessions = sessions;
                            movie.Theaters.Add(theater);
                        }
                    }
                    #endregion

                    if (movie.Theaters.Count > 0)
                    {
                        result.Add(movie);
                    }
                }
                #endregion

                #region Parallel
                //Parallel.ForEach(billboards, aitem =>
                //{
                //    MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
                //    // movieBillboard.Movie = aitem.Movie.Clone();
                //    MovieEntity movie2 = new MovieEntity();
                //    movie2.Actor = aitem.Movie.Actor;
                //    movie2.Country = aitem.Movie.Country;
                //    movie2.Director = aitem.Movie.Director;
                //    movie2.Id = aitem.Movie.Id;
                //    //movie2.Information = aitem.Movie.Information.Clone();
                //    InformationEntity information = new InformationEntity();
                //    information.Class = aitem.Movie.Information.Class;
                //    information.Format = aitem.Movie.Information.Format;
                //    information.Gender = aitem.Movie.Information.Gender;
                //    information.Subtitle = aitem.Movie.Information.Subtitle;
                //    information.Time = aitem.Movie.Information.Time;
                //    information.Trailer = aitem.Movie.Information.Trailer;
                //    information.Web = aitem.Movie.Information.Web;
                //    movie2.Information = information;
                //    movie2.Sinopsis = aitem.Movie.Sinopsis;
                //    movie2.Title = aitem.Movie.Title;
                //    movie2.FirstExhibit = aitem.Movie.FirstExhibit;
                //    movieBillboard.Movie = movie2;
                //    movieBillboard.Dates = aitem.Dates != null ? aitem.Dates.ToArray() : null;
                //    MovieBillboardEntity movie = movieBillboard;
                //    Parallel.ForEach(aitem.Theaters.Where(t => t.Theater.City.ToUpper().Equals(city.ToUpper())), item =>
                //    {
                //        var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();
                //        sessions = ValidateDate(sessions);
                //        if (sessions.Count > 0)
                //        {
                //            MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                //            TheaterEntity theater2 = new TheaterEntity();
                //            theater2.Id = item.Theater.Id;
                //            theater2.Name = item.Theater.Name;
                //            theater2.Address = item.Theater.Address;
                //            theater2.Latitude = item.Theater.Latitude;
                //            theater2.Longitude = item.Theater.Longitude;
                //            theater2.City = item.Theater.City;
                //            theater2.Phone = item.Theater.Phone;
                //            theater2.Facilities = item.Theater.Facilities;
                //            theater.Theater = theater2;
                //            theater.Sessions = sessions;
                //            movie.Theaters.Add(theater);
                //        }
                //    });

                //    if (movie.Theaters.Count > 0)
                //    {
                //        result.Add(movie);
                //    }
                //}); 
                #endregion
            }

            return result.ToArray();
        }

        public MovieBillboardEntity GetByDate(string city, string movieId, int day, int month)
        {
            MovieBillboardEntity result = new MovieBillboardEntity();
            MovieBillboardEntity billboard = this.Data.Where(b => b.Movie.Id.Equals(movieId)).FirstOrDefault(); //Busqueda por Ho de la pelicula
            if (billboard != null)
            {
                // result = billboard.Clone();

                MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
                movieBillboard.Movie = billboard.Movie;

                #region cambio a variable
                //List<DateTime> dates = new List<DateTime>();
                //foreach (var date in billboard.Dates)
                //{
                //    dates.Add(date);
                //}
                //movieBillboard.Dates = dates.ToArray(); 
                #endregion

                movieBillboard.Dates = billboard.Dates;
                result = movieBillboard;

                #region Busqueda de las funciones que cumplen con la fecha

                foreach (var item in billboard.Theaters.Where(t => t.Theater.City.ToUpper().Equals(city.ToUpper())))
                {
                    var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();
                    sessions = ValidateDate(sessions);
                    if (sessions.Count > 0)
                    {
                        MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                        // theater.Theater = item.Theater.Clone();

                        TheaterEntity theater2 = new TheaterEntity();
                        theater2.Id = item.Theater.Id;
                        theater2.Name = item.Theater.Name;
                        theater2.Address = item.Theater.Address;
                        theater2.Latitude = item.Theater.Latitude;
                        theater2.Longitude = item.Theater.Longitude;
                        theater2.City = item.Theater.City;
                        theater2.Phone = item.Theater.Phone;
                        #region cambio a variable
                        //List<FacilityEntity> facilities = new List<FacilityEntity>();
                        //foreach (var item2 in item.Theater.Facilities)
                        //{
                        //    facilities.Add(item2.Clone());
                        //}
                        //item.Theater.Facilities = facilities.ToArray(); 
                        #endregion
                        item.Theater.Facilities = item.Theater.Facilities;
                        theater.Theater = theater2;

                        theater.Sessions = sessions;
                        result.Theaters.Add(theater);
                    }
                }

                #endregion Busqueda de las funciones que cumplen con la fecha
            }
            return result;
        }

        //no se usa. WTF?
        public MovieBillboardEntity[] GetByCity(string city)
        {

            List<MovieBillboardEntity> result = new List<MovieBillboardEntity>();
            List<MovieBillboardEntity> billboards = Data.Where(m => m.Theaters.Where(t => t.Theater.City.Equals(city)).Count() > 0).ToList();

            billboards = (from u in billboards
                          where u.Theaters.Any(t => t.Theater.City == city)
                          select new MovieBillboardEntity()
                          {
                              Dates = (from y in u.Theaters
                                       from c in y.Sessions
                                       where y.Theater.City == city && y.Theater.Id != "1028"
                                       select c.ShowTime.Date).Distinct().ToArray(),
                              Movie = u.Movie,
                              Theaters = u.Theaters.Where(i => i.Theater.City == city && i.Theater.Id != "1028").ToList()
                          }).ToList();


            #region MyRegion
            //MovieBillboardEntity[] movieBillboards = Load();
            //List<MovieBillboardEntity> billboards = movieBillboards.Where(m => m.Theaters.Where(t => t.Theater.City.Equals(city)).Count() > 0).ToList();
            //var valor = billboards.Where(t => t.Theaters.Any(r=>r.Theater.City.ToUpper().Equals(city.ToUpper()))           

            //er r = (from y in billboards
            //         where y.Theaters.Any(p => p.Theater.City.ToUpper().Equals(city.ToUpper()))
            //         select new MovieBillboardTheaterEntity()
            //         {
            //             Theater = y.Theaters.Select(t => t.Theater).FirstOrDefault(),
            //             Sessions = ValidateDate(y.Theaters.Select(t => t.Sessions).FirstOrDefault().Where(s => s.ShowTime.Month.Equals(y.Dates.First().Month)
            //             && s.ShowTime.Day.Equals(y.Dates.First().Day)).ToList())
            //         }).ToList();

            //var r = (from y in billboards
            //         where y.Theaters.Any(p => p.Theater.City.ToUpper().Equals(city.ToUpper()))
            //         select new MovieBillboardTheaterEntity()
            //         {
            //             Theater = y.Theaters.Select(t => t.Theater).FirstOrDefault(),
            //             Sessions = ValidateDate(y.Theaters.Select(t => t.Sessions).FirstOrDefault().Where(s => s.ShowTime.Month.Equals(y.Dates.First().Month)
            //             && s.ShowTime.Day.Equals(y.Dates.First().Day)).ToList())
            //         }).Aggregate(new MovieBillboardEntity(), (a, b) =>
            //         {
            //             a.Theaters = b.
            //         }
            //        );


            //List<MovieBillboardEntity> r = (from y in billboards
            //                                select new MovieBillboardEntity()
            //                                {
            //                                    Dates = y.Dates,
            //                                    Movie = y.Movie
            //                                }).Where(r => r.Theaters.Any(t => t.Theater.City.ToUpper().Equals(city.ToUpper())))
            //                                .Aggregate(new MovieBillboardEntity(),(a,b)=>
            //                                {
            //                                    a.Dates = b.Dates;
            //                                    a.Movie = b.Movie;
            //                                    return a;
            //                                });


            //sesiones por Teatros filtrados por ciudad
            //List<MovieBillboardEntity> valor = billboards.Where(t => t.Theaters.Any(r => r.Theater.City.ToUpper().Equals(city.ToUpper()))).ToList();

            //var valores = (from b in billboards
            //               from c in b.Theaters
            //               from a in c.Sessions
            //               where c.Theater.City.ToUpper() == city.ToUpper() && c.Sessions.Count() > 0
            //               select new MovieBillboardTheaterEntity()
            //               {
            //                   Theater = c.Theater,
            //                   Sessions = ValidateDate(c.Sessions.Where(s => s.ShowTime.Month.Equals(b.Dates.First().Month) && s.ShowTime.Day.Equals(b.Dates.First().Day)).ToList())
            //               })
            //               .Aggregate(new List<MovieBillboardEntity>(), (s, d) =>
            //                {
            //                    MovieBillboardEntity t = new MovieBillboardEntity();

            //                    t.Theaters.Add(d);
            //                    s.Add(t);
            //                    return s;
            //                });
            //Stopwatch r1 = new Stopwatch();
            //r1.Start();
            //var valores = (from b in billboards
            //               from c in b.Theaters
            //               from a in c.Sessions
            //               where c.Theater.City.ToUpper() == city.ToUpper() && c.Sessions.Count() > 0
            //               select new MovieBillboardEntity()
            //               {
            //                   Dates = b.Dates,
            //                   Movie = b.Movie,
            //                   Theaters = (from f in b.Theaters
            //                               //where f.Theater.City.ToUpper() == city.ToUpper() && c.Sessions.Count() > 0
            //                               select new MovieBillboardTheaterEntity()
            //                               {
            //                                   Theater = f.Theater,
            //                                   Sessions = ValidateDate(c.Sessions.Where(s => s.ShowTime.Month.Equals(b.Dates.First().Month) && s.ShowTime.Day.Equals(b.Dates.First().Day)).ToList())
            //                               }
            //                               ).ToList()
            //               });
            //r1.Stop();
            //var tiempo1 = r1.Elapsed.ToString();

            //Stopwatch r3 = new Stopwatch();
            //r3.Start();
            //var cadena = JsonConvert.SerializeObject(valores);
            //r3.Stop();
            //var tiempo3 = r3.Elapsed.ToString(); 
            #endregion

            //Stopwatch r = new Stopwatch();
            //r.Start();

            if (billboards.Count > 0)
            {
                foreach (MovieBillboardEntity aitem in billboards)
                {
                    MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
                    MovieEntity movie2 = new MovieEntity();
                    movie2.Actor = aitem.Movie.Actor;
                    movie2.Country = aitem.Movie.Country;
                    movie2.Director = aitem.Movie.Director;
                    movie2.Id = aitem.Movie.Id;

                    //InformationEntity information = new InformationEntity();
                    //information.Class = aitem.Movie.Information.Class;
                    //information.Format = aitem.Movie.Information.Format;
                    //information.Gender = aitem.Movie.Information.Gender;
                    //information.Subtitle = aitem.Movie.Information.Subtitle;
                    //information.Time = aitem.Movie.Information.Time;
                    //information.Trailer = aitem.Movie.Information.Trailer;
                    //information.Web = aitem.Movie.Information.Web;

                    //movie2.Information = information;
                    movie2.Information = new InformationEntity()
                    {
                        Class = aitem.Movie.Information.Class,
                        Format = aitem.Movie.Information.Format,
                        Gender = aitem.Movie.Information.Gender,
                        Subtitle = aitem.Movie.Information.Subtitle,
                        Time = aitem.Movie.Information.Time,
                        Trailer = aitem.Movie.Information.Trailer,
                        Web = aitem.Movie.Information.Web
                    };


                    movie2.Sinopsis = aitem.Movie.Sinopsis;
                    movie2.Title = aitem.Movie.Title;
                    movieBillboard.Movie = movie2;
                    List<DateTime> dates = new List<DateTime>();

                    dates = (from t in aitem.Dates
                             select t).ToList();

                    //foreach (var date in aitem.Dates)
                    //{
                    //    dates.Add(date);
                    //}

                    movieBillboard.Dates = dates.ToArray();
                    MovieBillboardEntity movie = movieBillboard;


                    #region foreach MovieBillboardTheaterEntity 
                    foreach (MovieBillboardTheaterEntity item in aitem.Theaters.Where(t => t.Theater.City.ToUpper().Equals(city.ToUpper())))
                    {
                        var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(aitem.Dates.First().Month)
                        && s.ShowTime.Day.Equals(aitem.Dates.First().Day)).ToList();

                        sessions = ValidateDate(sessions);

                        MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                        TheaterEntity theater2 = new TheaterEntity();
                        theater2.Id = item.Theater.Id;
                        theater2.Name = item.Theater.Name;
                        theater2.Address = item.Theater.Address;
                        theater2.Latitude = item.Theater.Latitude;
                        theater2.Longitude = item.Theater.Longitude;
                        theater2.City = item.Theater.City;
                        theater2.Phone = item.Theater.Phone;

                        List<FacilityEntity> facilities = new List<FacilityEntity>();
                        facilities = (from t in item.Theater.Facilities
                                      select t).ToList();

                        //foreach (var item3 in item.Theater.Facilities)
                        //{
                        //    facilities.Add(item3.Clone());
                        //}

                        theater2.Facilities = facilities.ToArray();
                        theater.Theater = theater2;
                        theater.Sessions = item.Sessions;
                        movie.Theaters.Add(theater);

                    }
                    #endregion

                    result.Add(movie);
                }
            }
            //r.Stop();
            //var tiempo = r.Elapsed.ToString();


            //  var cadena2 = JsonConvert.SerializeObject(result);
            return result.ToArray();
            //  return valores.ToArray();
        }

        public MovieBillboardEntity GetByCity(string city, string movieId)
        {
            MovieBillboardEntity result = new MovieBillboardEntity();
            List<MovieBillboardEntity> List = this.Data.ToList();
            //MovieBillboardEntity billboard = this.Data.Where(b => b.Movie.Id.Equals(movieId)).FirstOrDefault(); //Busqueda por Ho de la pelicula
            MovieBillboardEntity billboard = List.Where(b => b.Movie.Id.Equals(movieId)).FirstOrDefault();  //se quitan las peliculas que no tengan fechas programadas para evitar que explote el metodo siguinete.
            if (billboard != null)
            {
                //  result = billboard.Clone();

                MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
                movieBillboard.Movie = billboard.Movie;

                #region Cambio de ForeArch a Lambda RM
                //List<DateTime> dates = new List<DateTime>();
                //foreach (var date in billboard.Dates)
                //{
                //    dates.Add(date);
                //}
                //movieBillboard.Dates = dates.ToArray();
                #endregion

                movieBillboard.Dates = billboard.Dates != null ? billboard.Dates : null;
                result = movieBillboard;


                #region cambio a parallel
                foreach (var item in billboard.Theaters.Where(t => t.Theater.City.ToUpper().Equals(city.ToUpper())))
                {
                    var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(billboard.Dates.First().Month) && s.ShowTime.Day.Equals(billboard.Dates.First().Day)).ToList();
                    sessions = ValidateDate(sessions);
                    if (sessions.Count > 0)
                    {
                        //MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                        // theater.Theater = item.Theater.Clone();

                        MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                        // theater.Theater = item.Theater.Clone();
                        TheaterEntity theater2 = new TheaterEntity();
                        theater2.Id = item.Theater.Id;
                        theater2.Name = item.Theater.Name;
                        theater2.Address = item.Theater.Address;
                        theater2.Latitude = item.Theater.Latitude;
                        theater2.Longitude = item.Theater.Longitude;
                        theater2.City = item.Theater.City;
                        theater2.Phone = item.Theater.Phone;

                        #region Cambio de ForeArch a Lambda RM
                        //List<FacilityEntity> facilities = new List<FacilityEntity>();
                        //foreach (var item2 in item.Theater.Facilities)
                        //{
                        //    facilities.Add(item2.Clone());
                        //}
                        //item.Theater.Facilities = facilities.ToArray();
                        #endregion

                        item.Theater.Facilities = item.Theater.Facilities != null ? item.Theater.Facilities.ToArray() : null;
                        theater.Theater = theater2;
                        theater.Sessions = sessions;
                        result.Theaters.Add(theater);
                    }
                }
                #endregion

                //Parallel.ForEach(billboard.Theaters.Where(t => t.Theater.City.ToUpper().Equals(city.ToUpper())), item =>
                //{
                //    var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(billboard.Dates.First().Month) && s.ShowTime.Day.Equals(billboard.Dates.First().Day)).ToList();
                //    sessions = ValidateDate(sessions);
                //    if (sessions.Count > 0)
                //    {
                //        MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                //        TheaterEntity theater2 = new TheaterEntity();
                //        theater2.Id = item.Theater.Id;
                //        theater2.Name = item.Theater.Name;
                //        theater2.Address = item.Theater.Address;
                //        theater2.Latitude = item.Theater.Latitude;
                //        theater2.Longitude = item.Theater.Longitude;
                //        theater2.City = item.Theater.City;
                //        theater2.Phone = item.Theater.Phone;
                //        item.Theater.Facilities = item.Theater.Facilities.ToArray();
                //        theater.Theater = theater2;
                //        theater.Sessions = sessions;
                //        result.Theaters.Add(theater);
                //    }
                //});
            }
            return result;
        }


        //no se usa?? WTF.
        public MovieBillboardEntity[] GetMovieAll()
        {
            List<MovieBillboardEntity> rs = new List<MovieBillboardEntity>();

            MovieBillboardEntity result = new MovieBillboardEntity();

            List<MovieBillboardEntity> billboards = this.DataAll.ToList();
            var movies = (from mov in billboards select (mov.Movie)).Distinct();

            #region cambio a parallel
            //foreach (var mov in movies)
            //{

            //    MovieBillboardEntity billboard = billboards.Where(b => b.Movie.Id.Equals(mov.Id)).FirstOrDefault(); //Busqueda por Ho de la pelicula
            //    if (billboard != null)
            //    {
            //        // result = billboard.Clone();
            //        foreach (var item in billboard.Theaters)
            //        {
            //            var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(billboard.Dates.First().Month) && s.ShowTime.Day.Equals(billboard.Dates.First().Day)).ToList();
            //            sessions = ValidateDate(sessions);
            //            if (sessions.Count > 0)
            //            {
            //                MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
            //                // theater.Theater = item.Theater.Clone();
            //                theater.Sessions = sessions;
            //                result.Theaters.Add(theater);
            //            }
            //        }
            //    }
            //    rs.Add(result);
            //} 
            #endregion

            Parallel.ForEach(movies, mov =>
            {
                MovieBillboardEntity billboard = billboards.Where(b => b.Movie.Id.Equals(mov.Id)).FirstOrDefault(); //Busqueda por Ho de la pelicula
                if (billboard != null)
                {
                    // result = billboard.Clone();
                    foreach (var item in billboard.Theaters)
                    {
                        var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(billboard.Dates.First().Month) && s.ShowTime.Day.Equals(billboard.Dates.First().Day)).ToList();
                        sessions = ValidateDate(sessions);
                        if (sessions.Count > 0)
                        {
                            MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                            // theater.Theater = item.Theater.Clone();
                            theater.Sessions = sessions;
                            result.Theaters.Add(theater);
                        }
                    }
                }
                rs.Add(result);
            });
            return rs.ToArray();
        }

        //no se usa en la pagina WTF??
        /// <summary>
        /// Obtiene las peliculas por cine para los dispositivos moviles, recibe la lista de cines 
        /// por un string y se consulta a través de un post.
        /// </summary>
        /// <param name="cines"></param>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public MovieBillboardEntity[] GetMovieTheaterList(List<string> cines, int day, int month)
        {
            List<MovieBillboardEntity> rs = new List<MovieBillboardEntity>();

            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("SA Western Standard Time");
            DateTime dateTimeNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            MovieBillboardEntity result = new MovieBillboardEntity();

            List<MovieBillboardEntity> billboards = this.DataAll.ToList();
            //var movies = (from mov in billboards select (mov.Movie)).Distinct();

            var movies = (from mov in billboards where mov.Theaters.Any(t => cines.Contains(t.Theater.Id)) select (mov.Movie));

            foreach (var mov in movies)
            {
                MovieBillboardEntity billboard = billboards.Where(b => b.Movie.Id.Equals(mov.Id)).FirstOrDefault(); //Busqueda por Ho de la pelicula
                if (billboard != null)
                {
                    // result = billboard.Clone();

                    MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
                    movieBillboard.Movie = billboard.Movie;

                    #region CambioALambda RM
                    //List<DateTime> dates = new List<DateTime>();
                    //foreach (var date in billboard.Dates)
                    //{
                    //    dates.Add(date);
                    //}
                    //movieBillboard.Dates = dates.ToArray();
                    #endregion

                    movieBillboard.Dates = billboard.Dates.ToArray();
                    result = movieBillboard;

                    #region cambio a parallel
                    //foreach (var cine in cines)
                    //{
                    //    foreach (var item in billboard.Theaters.Where(t => t.Theater.Id.Equals(cine)))
                    //    {
                    //        //var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals((month == 0 ? billboard.Dates.First().Month : month)) && s.ShowTime.Day.Equals((day == 0 ? billboard.Dates.First().Day : day))).ToList();
                    //        var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();

                    //        sessions = ValidateDate(sessions);
                    //        if (sessions.Count > 0)
                    //        {
                    //            MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                    //            // theater.Theater = item.Theater.Clone();
                    //            TheaterEntity theater2 = new TheaterEntity();
                    //            theater2.Id = item.Theater.Id;
                    //            theater2.Name = item.Theater.Name;
                    //            theater2.Address = item.Theater.Address;
                    //            theater2.Latitude = item.Theater.Latitude;
                    //            theater2.Longitude = item.Theater.Longitude;
                    //            theater2.City = item.Theater.City;
                    //            theater2.Phone = item.Theater.Phone;

                    //            #region Cambio a Lambda RM
                    //            //List<FacilityEntity> facilities = new List<FacilityEntity>();

                    //            //foreach (var item2 in item.Theater.Facilities)
                    //            //{
                    //            //    facilities.Add(item2.Clone());
                    //            //}

                    //            //item.Theater.Facilities = facilities.ToArray();
                    //            #endregion

                    //            item.Theater.Facilities = item.Theater.Facilities.ToArray();
                    //            theater.Theater = theater2;
                    //            theater.Sessions = sessions;
                    //            result.Theaters.Add(theater);
                    //        }
                    //    }
                    //}
                    #endregion

                    Parallel.ForEach(cines, cine =>
                    {
                        foreach (var item in billboard.Theaters.Where(t => t.Theater.Id.Equals(cine)))
                        {
                            //var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals((month == 0 ? billboard.Dates.First().Month : month)) && s.ShowTime.Day.Equals((day == 0 ? billboard.Dates.First().Day : day))).ToList();
                            var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();

                            sessions = ValidateDate(sessions);
                            if (sessions.Count > 0)
                            {
                                MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                                // theater.Theater = item.Theater.Clone();
                                TheaterEntity theater2 = new TheaterEntity();
                                theater2.Id = item.Theater.Id;
                                theater2.Name = item.Theater.Name;
                                theater2.Address = item.Theater.Address;
                                theater2.Latitude = item.Theater.Latitude;
                                theater2.Longitude = item.Theater.Longitude;
                                theater2.City = item.Theater.City;
                                theater2.Phone = item.Theater.Phone;
                                item.Theater.Facilities = item.Theater.Facilities.ToArray();
                                theater.Theater = theater2;
                                theater.Sessions = sessions;
                                result.Theaters.Add(theater);
                            }
                        }
                    });
                }
                if (result.Theaters.Count() > 0)
                {
                    rs.Add(result);
                }
            }
            return rs.OrderByDescending(x => x.Movie.Title).ToArray();

        }

        //no se una en la pagina WTF?
        /// <summary>
        /// Retorna el listado de las peliculas para que sean visualizadas en los dispositivos moviles.
        /// </summary>
        /// <param name="cines"></param>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public MovieBillboardEntity[] GetAllMoviesByTheaterList(List<string> cines, int day, int month)
        {
            List<MovieBillboardEntity> result = new List<MovieBillboardEntity>();
            foreach (var cine in cines)
            {
                List<MovieBillboardEntity> billboards = this.Data.Where(m => m.Theaters.Where(t => t.Theater.Id.Equals(cine)).Count() > 0).ToList();
                // List<MovieBillboardEntity> billboards =      Data.Where(m => m.Theaters.Where(t => t.Theater.City.Equals(city)).Count() > 0).ToList();

                billboards = (from u in billboards
                              where u.Theaters.Any(t => t.Theater.Id == cine)
                              select new MovieBillboardEntity()
                              {
                                  Dates = (from y in u.Theaters
                                           from c in y.Sessions
                                           where y.Theater.Id == cine && y.Theater.Id != "1028"
                                           select c.ShowTime.Date).Distinct().ToArray(),
                                  Movie = u.Movie,
                                  Theaters = u.Theaters.Where(i => i.Theater.Id == cine && i.Theater.Id != "1028").ToList()
                              }).ToList();

                if (billboards.Count > 0)
                {
                    foreach (var aitem in billboards)
                    {
                        MovieBillboardEntity movie = null;// aitem.Clone();

                        MovieBillboardEntity movieBillboard = new MovieBillboardEntity();
                        //movieBillboard.Movie = billboard.Movie;
                        movieBillboard.Movie = movie.Movie;

                        #region cambio a Lambda RM
                        //List<DateTime> dates = new List<DateTime>();
                        //foreach (var date in movie.Dates)
                        //{
                        //    dates.Add(date);
                        //}
                        //movieBillboard.Dates = dates.ToArray();
                        #endregion

                        movieBillboard.Dates = movie.Dates.ToArray();
                        movie = movieBillboard;

                        #region Cambio a parallel
                        //foreach (var item in aitem.Theaters.Where(t => t.Theater.Id.Equals(cine)))
                        //{
                        //    var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();
                        //    sessions = ValidateDate(sessions);
                        //    if (sessions.Count > 0)
                        //    {
                        //        MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                        //        // theater.Theater = item.Theater.Clone();

                        //        TheaterEntity theater2 = new TheaterEntity();
                        //        theater2.Id = item.Theater.Id;
                        //        theater2.Name = item.Theater.Name;
                        //        theater2.Address = item.Theater.Address;
                        //        theater2.Latitude = item.Theater.Latitude;
                        //        theater2.Longitude = item.Theater.Longitude;
                        //        theater2.City = item.Theater.City;
                        //        theater2.Phone = item.Theater.Phone;

                        //        #region cambio a lambda RM
                        //        //List<FacilityEntity> facilities = new List<FacilityEntity>();
                        //        //foreach (var item2 in item.Theater.Facilities)
                        //        //{
                        //        //    facilities.Add(item2.Clone());
                        //        //}
                        //        //item.Theater.Facilities = facilities.ToArray();
                        //        #endregion

                        //        item.Theater.Facilities = item.Theater.Facilities.ToArray();
                        //        theater.Theater = theater2;

                        //        theater.Sessions = sessions;
                        //        movie.Theaters.Add(theater);
                        //    }
                        //}
                        #endregion

                        Parallel.ForEach(aitem.Theaters.Where(t => t.Theater.Id.Equals(cine)), item =>
                        {
                            var sessions = item.Sessions.Where(s => s.ShowTime.Month.Equals(month) && s.ShowTime.Day.Equals(day)).ToList();
                            sessions = ValidateDate(sessions);
                            if (sessions.Count > 0)
                            {
                                MovieBillboardTheaterEntity theater = new MovieBillboardTheaterEntity();
                                // theater.Theater = item.Theater.Clone();

                                TheaterEntity theater2 = new TheaterEntity();
                                theater2.Id = item.Theater.Id;
                                theater2.Name = item.Theater.Name;
                                theater2.Address = item.Theater.Address;
                                theater2.Latitude = item.Theater.Latitude;
                                theater2.Longitude = item.Theater.Longitude;
                                theater2.City = item.Theater.City;
                                theater2.Phone = item.Theater.Phone;

                                #region cambio a lambda RM
                                //List<FacilityEntity> facilities = new List<FacilityEntity>();
                                //foreach (var item2 in item.Theater.Facilities)
                                //{
                                //    facilities.Add(item2.Clone());
                                //}
                                //item.Theater.Facilities = facilities.ToArray();
                                #endregion

                                item.Theater.Facilities = item.Theater.Facilities.ToArray();
                                theater.Theater = theater2;

                                theater.Sessions = sessions;
                                movie.Theaters.Add(theater);
                            }
                        });

                        result.Add(movie);
                    }
                }
            }
            return result.ToArray();
        }

        public SessionInfoEntity GetSession(string theaterId, int sessionId)
        {
            string key = string.Format("{0}-{1}", theaterId, sessionId);

            SessionInfoEntity sessionInfoEntity;

            LiveClient client = new LiveClient();
            SessionInformation sessionInformation = client.GetSessionInformation(theaterId, sessionId);
            client.Close();
            sessionInfoEntity = (SessionInfoEntity)sessionInformation;
            sessionInfoEntity.Id = sessionId;
            sessionInfoEntity.OnSale = sessionInformation.OnSale;

            ////Busca informacion del  cine
            //sessionInfoEntity.Theater = this.Data.FirstOrDefault(t => t.Id.Equals(theaterId));
            sessionInfoEntity.Theater = (from x in Data
                                         from r in x.Theaters
                                         where r.Theater.Id == theaterId
                                         select r.Theater).FirstOrDefault();

            foreach (MovieBillboardEntity movieBillboardEntity in this.Data)
            {

                //Procedemos a buscar el Movie en la lista de Movies
                if (movieBillboardEntity.Movie.Id.Equals(sessionInformation.MovieID))
                {

                    //Procedemos a buscar el Cine en la lista de Cines de la Pelicula
                    var TheatherFind = movieBillboardEntity.Theaters.FirstOrDefault(k => k.Theater.Id.Equals(theaterId));
                    if (TheatherFind != null)
                    {
                        sessionInfoEntity.Movie = movieBillboardEntity.Movie;
                        sessionInfoEntity.Movie.Information.Class = (from x in movieBillboardEntity.Theaters
                                                                     from r in x.Sessions
                                                                     where x.Theater.Id == theaterId
                                                                     select r.Censor).FirstOrDefault().ToString();

                        break;
                    }
                }
            }
            return sessionInfoEntity;
        }

        public bool CompararHoras(string filename)
        {
            ////  var fecha = DateTime.Now.AddHours(-hours);
            ////return System.IO.File.GetLastWriteTime(filename).AddHours(-hours) <= fecha;

            int hours = int.Parse(ConfigurationManager.AppSettings["TimeCache"].ToString());

            bool value;

            DateTime DateSystem = DateTime.Now;
            DateTime DateFile = System.IO.File.GetLastWriteTime(filename);

            TimeSpan result = DateSystem - DateFile;

            if (result.TotalHours >= hours) value = true; else value = false;

            return value;

        }

        private MovieBillboardEntity[] Load()
        {
            MovieBillboardEntity[] movieBillboards;
            string filePath = String.Empty;
            //Boolean valido = CompararHoras(HostingEnvironment.MapPath(@"~/App_Data/MovieBillboard.xml"), 4);

            //string movieBillboardsFile = File.ReadAllText(HostingEnvironment.MapPath("~/App_Data/MovieBillboard.xml"));

            //if (System.Configuration.ConfigurationManager.AppSettings["DirectoryKeys"].ToString().Equals("Cloud"))
            //{
            //    string cachePath = RoleEnvironment.GetLocalResource("MovieBillboard").RootPath;
            //    filePath = Path.Combine(cachePath, "MovieBillboard.xml");
            //}
            //else
            //{
            //    filePath = (HostingEnvironment.MapPath("~/App_Data/MovieBillboard.xml"));
            //}

            //#if DEBUG
            filePath = HostingEnvironment.MapPath("~/App_Data/MovieBillboard.xml");
            //#endif

            //#if (!DEBUG)
            //            string cachePath = RoleEnvironment.GetLocalResource("MovieBillboard").RootPath;
            //            filePath = Path.Combine(cachePath, "MovieBillboard.xml");
            //#endif


            if (!File.Exists(filePath))
                File.Create(filePath).Close();

            Boolean invalido = CompararHoras(filePath);
            FileInfo file = new FileInfo(filePath);

            JsonSerializerSettings jsonvalue = new JsonSerializerSettings();
            jsonvalue.DefaultValueHandling = DefaultValueHandling.Ignore;

            if (invalido || file.Length < 20)
            {
                movieBillboards = Execute();
                string json = JsonConvert.SerializeObject(movieBillboards);
                File.WriteAllText(filePath, json);
            }
            else
            {
                movieBillboards = JsonConvert.DeserializeObject<MovieBillboardEntity[]>(File.ReadAllText(filePath), jsonvalue);
            }


            //movieBillboards = m_cache.Get("MovieBillboard");
            //if (movieBillboards == null || movieBillboards.Count() == 0)
            //{
            //    movieBillboards = Execute();
            //    m_cache.Set("MovieBillboard", movieBillboards, TimeSpan.FromMinutes(240));
            //}
            //m_cache.Close();
            return movieBillboards;
        }
        private MovieBillboardEntity[] LoadAll()
        {
            MovieBillboardEntity[] movieBillboards;

            Boolean valido = CompararHoras(HostingEnvironment.MapPath(@"~/App_Data/MovieBillboardAll.xml"));

            string MovieBillboardAllFile = File.ReadAllText(HostingEnvironment.MapPath("~/App_Data/MovieBillboardAll.xml"));

            if (!valido || String.IsNullOrEmpty(MovieBillboardAllFile))
            {
                movieBillboards = Execute();
                string json = JsonConvert.SerializeObject(movieBillboards);
                File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/MovieBillboardAll.xml"), json);
            }
            else
            {
                movieBillboards = JsonConvert.DeserializeObject<MovieBillboardEntity[]>(MovieBillboardAllFile);
            }


            //movieBillboards = m_cache.Get("MovieBillboardAll");
            //if (movieBillboards == null || movieBillboards.Count() == 0)
            //{
            //    movieBillboards = Execute();
            //    m_cache.Set("MovieBillboardAll", movieBillboards, TimeSpan.FromMinutes(240));
            //}
            //m_cache.Close();
            return movieBillboards;
        }

        public void Clear()
        {
            m_cache.Delete("MovieBillboard");
            m_cache.Close();
        }
        public void Clear2()
        {
            m_cache.Delete("MovieBillboardAll");
            m_cache.Close();
        }

        private List<SessionEntity> ValidateDate(List<SessionEntity> sessions)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            DateTime dateServer = DateTime.UtcNow;
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(dateServer, timeZone);
            List<SessionEntity> functions = sessions.Where(s => s.ShowTime.AddMinutes(20) > date).ToList();
            return functions;
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        public MovieBillboardEntity[] Data
        {
            get
            {
                MovieBillboardEntity[] movieBillboards = Load();
                return movieBillboards;
            }
        }

        public MovieBillboardEntity[] DataAll
        {
            get
            {
                MovieBillboardEntity[] movieBillboards = Load(); //LoadAll();
                return movieBillboards;
            }
        }

        #endregion Properties
    }
}