﻿using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinesUnidos.ESB.Domain;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class HallRepository : Repository<SessionEntity>
    {
        #region Constructor

        public HallRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion Constructor

        #region Methods

        public override void Create(Domain.SessionEntity user)
        {
            throw new NotImplementedException();
        }

        public override void Update(Domain.SessionEntity user)
        {
            throw new NotImplementedException();
        }

        public void UpdatePassword(Guid userId, string Password)
        {
            throw new NotImplementedException();
        }

        public void Activate(Guid userId)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Domain.SessionEntity user)
        {
            throw new NotImplementedException();
        }

        public HallEntity[] ReadHallsByTheater(string CinemaId)
        {
            List<HallEntity> Halls = new List<HallEntity>();
           
            using (var command = this.Context.CreateCommand())
            {

                string sql = @"SELECT [Hall_intid], [Cinema_strId], [Hall_intNumber], [Hall_strName], [Hall_strType], [Hall_intSeat], [Hall_strSeatAllocated] 
                          FROM [dbo].[tblHalls]
                          WHERE [Cinema_strID] = @cinema";
                command.CommandText = sql;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@cinema";
                idParameter.Value = CinemaId;

                command.Parameters.Add(idParameter);
                using (var reader = command.ExecuteReader())
                {
                    DataTable schemaTable = reader.GetSchemaTable();
                    Int32 rows = schemaTable.Rows.Count;
                    
                    Int32 IndexControl = 0;

                    
                    List<HallEntity> h = new List<HallEntity>();

                    while (reader.Read())
                    {
                        Domain.HallEntity hall = new HallEntity();                        
                        hall.Hall_intId = Int32.Parse(reader["Hall_intID"].ToString());                        
                        hall.Cinema_strID = reader["Cinema_strID"].ToString();                        
                        hall.Hall_intNumber = Int32.Parse(reader["Hall_intNumber"].ToString());                        
                        hall.Hall_strName = reader["Hall_strName"].ToString();                        
                        hall.Hall_strType = reader["Hall_strType"].ToString();                        
                        hall.Hall_intSeat = Int32.Parse(reader["Hall_intSeat"].ToString());
                        hall.Hall_strSeatAllocated = reader["Hall_strSeatAllocated"].ToString();                        
                        h.Add(hall);

                        IndexControl++;
                    }

                    Domain.HallEntity[] _Halls = new HallEntity[IndexControl];
                    _Halls = h.ToArray();
                    return _Halls;
                }
            }

        }

        public Domain.SessionEntity Read(SessionEntity ent, string cinemaId)
        {
            Domain.SessionEntity entidad = ent;
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [Hall_strType], Hall_strName
                          FROM [dbo].[tblHalls]
                          WHERE [Cinema_strID] = @cinema and 
                          Hall_intNumber = @hallnumber";

                command.CommandText = sql;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@cinema";
                idParameter.Value = cinemaId;

                IDbDataParameter idParameter2 = command.CreateParameter();
                idParameter2.ParameterName = "@hallnumber";
                idParameter2.Value = ent.HallNumber;

                command.Parameters.Add(idParameter);
                command.Parameters.Add(idParameter2);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        entidad.HallName = reader["Hall_strName"].ToString() + " - " + reader["Hall_strType"].ToString();                        
                    }
                }
            }
            return entidad;
        }

        protected override void Map(IDataRecord record, SessionEntity entity)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}
