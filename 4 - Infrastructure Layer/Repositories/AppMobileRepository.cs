﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using CinesUnidos.ESB.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using CinesUnidos.ESB.Infrastructure.Agents.Premium;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Web.Hosting;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class AppMobileRepository : IAppMobileRepository
    {
        #region Attributos

        private IConnectionFactory m_factory;

        private ICache<TheaterMobileEntity[]> m_cache;

        #endregion Attributos

        #region Methods


        public AppMobileRepository(ICache<TheaterMobileEntity[]> cache)
        {
            m_cache = cache;
        }

        public AppMobileRepository()
        {
            m_factory = new ConnectionFactory("ELMAH");
        }

        public List<SectionEntity> GetHome(string platform)
        {
            var sections = new List<SectionEntity>();
            var Items = new List<ItemEntity>();
            ConnectionFactory m_factory = new ConnectionFactory("ELMAH");
            using (var uow = new UnitOfWork(m_factory))
            {
                var sectionRepo = new SectionRepository(uow);
                sections = sectionRepo.Read(platform).OrderBy(p => p.PlatformName).ThenBy(p => p.Name).ToList();

                var itemRepo = new ItemRepository(uow);
                Items = itemRepo.Read();

                foreach (SectionEntity sec in sections)
                {
                    sec.Items = Items.Where(p => p.Section.Id == sec.Id).Take(1).ToList();
                }
            }
            return sections;
        }

        public TheaterMobileEntity[] GetAllTheatersMobile()
        {
            TheaterMobileEntity[] theaters;
            string filePath = string.Empty;
            //comparar las dos fechas y ver si ya tiene mas de 24 horas. alli entonces actualizar el
            //archivo.
//#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_Data/TheatersMobile.xml"));
//#endif

//#if (!DEBUG)
//                string cachePath = RoleEnvironment.GetLocalResource("TheatersMobile").RootPath;
//                filePath = Path.Combine(cachePath, "TheatersMobile.xml");
//#endif
            if (!File.Exists(filePath))
                File.Create(filePath).Close();

            //Boolean valido = CompararHoras(filePath, 168);

            FileInfo file = new FileInfo(filePath);

            //if (valido || file.Length < 20)
            if (file.Length < 20)
            {
                theaters = Execute();
                string json = JsonConvert.SerializeObject(theaters);
                File.WriteAllText(filePath, json);
            }
            else
            {
                theaters = JsonConvert.DeserializeObject<TheaterMobileEntity[]>(File.ReadAllText(filePath));
            }

            return theaters;
            // theaters = m_cache.Get("TheatersMobile");
            // if (theaters == null || theaters.Count() == 0)
            //{
            //   theaters = Execute();
            //   m_cache.Set("TheatersMobile", theaters, TimeSpan.FromHours(24));
            // }
            //return theaters;
        }

        private TheaterMobileEntity[] Execute()
        {
            DataClient client = new DataClient();

            string CompanyID = System.Configuration.ConfigurationManager.AppSettings["CompanyID"].ToString();
            Theater[] result = client.GetTheaters(CompanyID);
            client.Close();

            List<TheaterMobileEntity> Theaters = new List<TheaterMobileEntity>();
            foreach (var item in result)
            {
                TheaterMobileEntity theater = new TheaterMobileEntity { };

                theater.Address = item.Address;
                theater.City = item.City;
                theater.Id = item.ID;
                theater.Latitude = item.Latitude;
                theater.Longitude = item.Longitude;
                theater.Name = item.DisplayName;
                theater.Phone = item.Phone;
                List<FacilityEntity> facilities = new List<FacilityEntity>();
                foreach (var aitem in item.P_Facilities)
                {
                    FacilityEntity facility = (FacilityEntity)aitem;
                    facilities.Add(facility);
                }
                theater.Facilities = facilities.ToArray();
                ConnectionFactory m_factory = new ConnectionFactory("ELMAH");
                using (var uow = new UnitOfWork(m_factory))
                {
                    HallRepository hRepo = new HallRepository(uow);
                    theater.Halls = hRepo.ReadHallsByTheater(item.ID.ToString());
                }

                Theaters.Add(theater);
            }
            return Theaters.ToArray();
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}