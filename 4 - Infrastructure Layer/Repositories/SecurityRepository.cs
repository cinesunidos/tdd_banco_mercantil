﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using CinesUnidos.ESB.Queue;
using System;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class SecurityRepository : ISecurityRepository
    {
        #region Attributos

        private IConnectionFactory m_factory;
        private IQueue m_queue;

        #endregion Attributos

        #region Methods

        public SecurityRepository(IQueue @queue)
        {
            m_factory = new ConnectionFactory("ELMAH");
            m_queue = queue;
        }

        public string MD5(string Flattext)
        {
            System.Security.Cryptography.MD5 Supplier = System.Security.Cryptography.MD5CryptoServiceProvider.Create();
            return Convert.ToBase64String(Supplier.ComputeHash(Encoding.ASCII.GetBytes(Flattext)));
        }

        public TokenEntity SignIn(SignInEntity login)
        {
            TokenEntity token = null;
            using (var uow = new UnitOfWork(m_factory))
            {
                var tokenRepository = new TokenRepository(uow);
                Guid? userId = tokenRepository.FindIdUser(login.Email, login.Password);
                if (userId.HasValue)
                {
                    token = tokenRepository.FindToken(userId.Value, login.Platform);
                    if (token == null)
                    {
                        Guid idHash = Guid.NewGuid();
                        token = new TokenEntity()
                        {
                            Id = idHash,
                            UserId = userId.Value,
                            Platform = login.Platform,
                            Expiration = DateTime.Now.AddHours(24),
                            //Hash = DogFramework.Security.Cryptography.Hash.MD5(idHash.ToString()),
                            Hash = MD5(idHash.ToString()),
                            Error = ""
                        };
                        tokenRepository.Create(token);
                    }
                    else
                    {
                        token.Expiration = DateTime.Now.AddHours(24);
                        tokenRepository.Update(token);
                    }
                    uow.SaveChanges();
                }
                else
                {
                    if (tokenRepository.ExistEmail(login.Email))
                    {
                        if (!tokenRepository.IsUserActive(login.Email))
                        {
                            token = new TokenEntity()
                            {
                                Error = "Disculpa, debes formalizar el registro activando tu cuenta a través del enlace enviado a tu correo electrónico"
                            };
                        }
                        else
                        {
                            token = new TokenEntity()
                            {
                                Error = "Disculpa, Usuario y/ o Contraseña incorrecta"
                            };
                        }
                    }
                    else
                        token = new TokenEntity()
                        {
                            Error = "Disculpa, la dirección de correo electrónico no se encuentra registrada."
                        };
                }                
            }
            return token;
        }

        public void Signout(string token)
        {
            TokenEntity tokenEntity = new TokenEntity();
            tokenEntity.Hash = token;
            using (var uow = new UnitOfWork(m_factory))
            {
                var tokenRepository = new TokenRepository(uow);
                tokenRepository.Delete(tokenEntity);
                uow.SaveChanges();
            }
        }

        public bool Valid(string token)
        {
            bool isValid = false;
            using (var uow = new UnitOfWork(m_factory))
            {
                var tokenRepository = new TokenRepository(uow);
                TokenEntity tokenFind = tokenRepository.FindToken(token);
                if (tokenFind != null)
                {
                    if (tokenFind.Expiration > DateTime.Now)
                        isValid = true;
                    else
                        isValid = false;
                }
                uow.SaveChanges();
            }
            return isValid;
        }

        public TokenEntity Refresh(string token)
        {
            TokenEntity tokenFind = null;
            TokenEntity tokenEntity = new TokenEntity();
            tokenEntity.Hash = token;
            using (var uow = new UnitOfWork(m_factory))
            {
                var tokenRepository = new TokenRepository(uow);
                tokenFind = tokenRepository.FindToken(token);
                if (tokenFind != null)
                {
                    tokenFind.Expiration = DateTime.Now.AddHours(24);
                    tokenRepository.Update(tokenFind);
                }
                uow.SaveChanges();
            }
            return tokenEntity;
        }

        public ResponseEntity Create(Domain.UserEntity user)
        {
            ResponseEntity ResponseEntity = new ResponseEntity();
            ResponseEntity.Dictionary = new System.Collections.Generic.Dictionary<string, object>();
            try
            {

                using (var uow = new UnitOfWork(m_factory))
                {
                    LoginRepository loginRepository = new LoginRepository(uow);
                    var exist = loginRepository.Read(user.IdCard, "");
                    if (exist.IdCard != null)
                    {
                        ResponseEntity.Dictionary.Add("UserExiste", "Disculpe estimado invitado su número de cédula ya se encuentra registrado en nuestra página");
                        ResponseEntity.isOk = false;
                        ResponseEntity.MsgResponse = "Sugerir Recordar contrasena";
                        ResponseEntity.CodResponse = "UsEx001";
                        return ResponseEntity;
                    }
                    else
                    {
                        loginRepository.Create(user);
                        uow.SaveChanges();
                    }
                }
                MailingServiceEntity mail_service_obj = new MailingServiceEntity();
                mail_service_obj.Tipo = "Register";
                mail_service_obj.User = user;
                mail_service_obj.Complain = null;
                m_queue.InsertMessage<MailingServiceEntity>("services", mail_service_obj);
                ResponseEntity.isOk = true;
            }
            catch (SqlException exs)
            {
                if (exs.Number == 2627 || exs.Number == 2601)
                {
                    if (exs.Message.Contains("IX_Card"))
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Disculpe estimado invitado su número de cédula ya se encuentra registrado en nuestra página");
                    else if (exs.Message.Contains("IX_Email"))
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Disculpe estimado invitado su correo electrónico ya se encuentra registrado en nuestra página");
                    else
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Error de duplicidad en base de datos");
                }
                else
                {
                    ResponseEntity.Dictionary.Add("ErrorMsg", "Error en base de datos");
                }
                ResponseEntity.isOk = false;
            }
            catch (Exception ex)
            {
                ResponseEntity.Dictionary.Add("ErrorMsg", ex.Message);
                ResponseEntity.isOk = false;
            }
            return ResponseEntity;
        }

        public ResponseEntity Update(Domain.UserEntity user)
        {
            ResponseEntity ResponseEntity = new ResponseEntity();
            ResponseEntity.Dictionary = new System.Collections.Generic.Dictionary<string, object>();
            try
            {
                using (var uow = new UnitOfWork(m_factory))
                {
                    LoginRepository loginRepository = new LoginRepository(uow);
                    loginRepository.Update(user);
                    uow.SaveChanges();
                }
                ResponseEntity.isOk = true;
            }
            catch (SqlException exs)
            {
                if (exs.Number == 2627 || exs.Number == 2601)
                {
                    if (exs.Message.Contains("IX_Card"))
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Se ha presentado un problema, el usuario no ha podido ser modificado, debido a que la cedula ya se encuentra registrada en nuestra pagina");
                    else if (exs.Message.Contains("IX_Email"))
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Se ha presentado un problema, el usuario no ha podido ser modificado, debido a que el correo electrónico ya se encuentra registrado en nuestra pagina");
                    else
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Error de duplicidad en base de datos");
                }
                else
                {
                    ResponseEntity.Dictionary.Add("ErrorMsg", "Error en base de datos");
                }
                ResponseEntity.isOk = false;
            }
            catch (Exception ex)
            {
                ResponseEntity.Dictionary.Add("ErrorMsg", ex.Message);
                ResponseEntity.isOk = false;
            }
            return ResponseEntity;
        }

        public void Delete(Domain.UserEntity user)
        {
            throw new NotImplementedException();
        }

        public Domain.UserEntity Read(Guid id)
        {
            UserEntity user = null;
            using (var uow = new UnitOfWork(m_factory))
            {
                LoginRepository loginRepository = new LoginRepository(uow);
                user = loginRepository.Read(id);
                uow.SaveChanges();
            }
            return user;
        }

        public Domain.UserEntity ReadByHash(string hash)
        {
            TokenEntity tokenEntity = null;
            using (var uow = new UnitOfWork(m_factory))
            {
                TokenRepository tokenRepository = new TokenRepository(uow);
                tokenEntity = tokenRepository.FindToken(hash);
                uow.SaveChanges();
            }
            return this.Read(tokenEntity.UserId);
        }

        public Domain.UserEntity Read(string email)
        {
            UserEntity user = null;
            using (var uow = new UnitOfWork(m_factory))
            {
                LoginRepository loginRepository = new LoginRepository(uow);
                user = loginRepository.Read(email);
                uow.SaveChanges();
            }
            return user;
        }

        public string RememberEmail(string idCard, DateTime birthDate)
        {
            string email = string.Empty;
            using (var uow = new UnitOfWork(m_factory))
            {
                LoginRepository loginRepository = new LoginRepository(uow);
                UserEntity user = loginRepository.Read(idCard, birthDate);
                uow.SaveChanges();
                email = user.Email;
            }
            return email;
        }

        public string VerifiedAnswer(string email, string answer)
        {
            UserEntity user = Read(email);
            if (user.SecretAnswer.ToUpper() == answer.ToUpper())
            {
                MailingServiceEntity mail_service_obj = new MailingServiceEntity();
                mail_service_obj.Tipo = "RecoverPassword";
                mail_service_obj.User = user;
                mail_service_obj.Complain = null;
                m_queue.InsertMessage<MailingServiceEntity>("services", mail_service_obj);
                return Boolean.TrueString;
            }
            else return Boolean.FalseString;
        }

        public string VerifiedEmailAndIdCard(string email, string idCard)
        {
            UserEntity user = Read(email);
            if (user.IdCard == idCard)
            {
                MailingServiceEntity mail_service_obj = new MailingServiceEntity();
                mail_service_obj.Tipo = "RecoverPassword";
                mail_service_obj.User = user;
                mail_service_obj.Complain = null;
                m_queue.InsertMessage<MailingServiceEntity>("services", mail_service_obj);
                return Boolean.TrueString;
            }
            else return Boolean.FalseString;
        }

        public ResponseEntity ChangePassword(PasswordEntity passwordEntity)
        {
            ResponseEntity ResponseEntity = new ResponseEntity();
            try
            {
                using (var uow = new UnitOfWork(m_factory))
                {
                    LoginRepository loginRepository = new LoginRepository(uow);
                    loginRepository.UpdatePassword(passwordEntity.UserId, passwordEntity.NewPassword);
                    uow.SaveChanges();
                }
                ResponseEntity.isOk = true;
            }
            catch (Exception)
            {
                ResponseEntity.isOk = false;
            }
            return ResponseEntity;
        }

        public ResponseEntity ActivateUser(Guid userId)
        {
            ResponseEntity ResponseEntity = new ResponseEntity();
            try
            {
                using (var uow = new UnitOfWork(m_factory))
                {
                    LoginRepository loginRepository = new LoginRepository(uow);
                    loginRepository.Activate(userId);
                    uow.SaveChanges();
                }
                ResponseEntity.isOk = true;
            }
            catch (Exception)
            {
                ResponseEntity.isOk = false;
            }
            return ResponseEntity;
        }

        public TokenInfoEntity GetInfo(string token)
        {
            TokenInfoEntity tokenFind = null;
            using (var uow = new UnitOfWork(m_factory))
            {
                var tokenRepository = new TokenRepository(uow);
                tokenFind = tokenRepository.FindTokenInfo(token);
                uow.SaveChanges();
            }
            return tokenFind;
        }

        //-------------------------------------------------------
        public ResponseEntity CreateUserMobile(Domain.UserEntity user)
        {
            ResponseEntity ResponseEntity = new ResponseEntity();
            ResponseEntity.Dictionary = new System.Collections.Generic.Dictionary<string, object>();
            try
            {
                using (var uow = new UnitOfWork(m_factory))
                {
                    LoginRepository loginRepository = new LoginRepository(uow);
                    loginRepository.Create(user);
                    uow.SaveChanges();
                }
                MailingServiceEntity mail_service_obj = new MailingServiceEntity();
                mail_service_obj.Tipo = "Register";
                mail_service_obj.User = user;
                mail_service_obj.Complain = null;
                m_queue.InsertMessage<MailingServiceEntity>("services", mail_service_obj);
                ResponseEntity.isOk = true;
                ResponseEntity.CodResponse = "COD_OOO";
                ResponseEntity.MsgResponse = "Usuario creado Satisfactoriamente";
                ResponseEntity.Dictionary.Add("OkMsg", "El usuario ha sido creado satisfactoriamente");
            }
            catch (SqlException exs)
            {
                if (exs.Number == 2627 || exs.Number == 2601)
                {
                    if (exs.Message.Contains("IX_Card"))
                    {
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Disculpa tu número de cédula ya se encuentra registrada en nuestra plataforma");
                        ResponseEntity.CodResponse = "CODE_OO1";
                        ResponseEntity.MsgResponse = "Cedula de Usuario ya registrada";
                    }                        
                    else if (exs.Message.Contains("IX_Email"))
                    {
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Disculpa tu correo electrónico ya se encuentra registrado en nuestra plataforma");
                        ResponseEntity.CodResponse = "CODE_OO2";
                        ResponseEntity.MsgResponse = "Email de Usuario ya registrado";
                    }                        
                    else
                        ResponseEntity.Dictionary.Add("ErrorMsg", "Error de duplicidad en base de datos");                        
                }
                else
                {
                    ResponseEntity.Dictionary.Add("ErrorMsg", "Error en base de datos");
                }
                ResponseEntity.isOk = false;
            }
            catch (Exception ex)
            {
                ResponseEntity.Dictionary.Add("ErrorMsg", ex.Message);
                ResponseEntity.isOk = false;
            }
            
            return ResponseEntity;
        }

        public ResponseEntity RemoveList(string token)
        {
            ResponseEntity responseEntity1;
            bool valor = (new Regex("^[a-zA-Z0-9]+$")).IsMatch(token);
            if ((string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token) ? false : valor))
            {
                ResponseEntity responseEntity = new ResponseEntity();
                try
                {
                    using (UnitOfWork uow = new UnitOfWork(this.m_factory))
                    {
                        responseEntity.CodResponse = (new LoginRepository(uow)).UpdateFromList(token);
                        switch (responseEntity.CodResponse)
                        {
                            case "0":
                                responseEntity.MsgResponse = "El email no esta registrado en la lista de correo.";
                                break;
                            case "1":
                                responseEntity.MsgResponse = "El usuario no está recibiendo MassMail.";
                                break;
                            case "2":
                                responseEntity.MsgResponse = "Su correo fue eliminado de la lista de correo.";
                                break;
                            default:
                                responseEntity.MsgResponse = "Respuesta inesperada";
                                break;
                        }
                        //if (responseEntity.CodResponse != "5")
                        //{
                        //    responseEntity.MsgResponse = "002 - Su correo fue eliminado de la lista de correo.";
                        //}
                        //else
                        //{
                        //    responseEntity.MsgResponse = "001 - Disculpe, El invitado no se encuentra en la lista de correo.";
                        //}
                        uow.SaveChanges();
                    }
                    responseEntity.isOk = true;
                }
                catch (Exception ex)
                {
                    responseEntity.isOk = false;
                    responseEntity.MsgResponse = ex.Message;
                }
                responseEntity1 = responseEntity;
            }
            else
            {
                ResponseEntity respuesta = new ResponseEntity()
                {
                    CodResponse = "99",
                    isOk = true,
                    MsgResponse = "999 - Su solicitud ya fue procesada."
                };
                responseEntity1 = respuesta;
            }
            return responseEntity1;
        }

        public Domain.DatosBoleto Passvalidation(Domain.DatosBoleto datosBoleto)
        {
            Agents.Data.DatosBoleto infoBoleto = new Agents.Data.DatosBoleto();
            infoBoleto.Stock_strBarcode = datosBoleto.Stock_strBarcode;
            DataClient dataCliente = new DataClient();
            var x = dataCliente.ConsultaDatosBoleto(infoBoleto);
            datosBoleto.BoletoNoValido = x.BoletoNoValido;
            datosBoleto.CodUtilizado = x.CodUtilizado;
            datosBoleto.dExpiryDate = x.dExpiryDate;
            datosBoleto.Error = x.Error;
            datosBoleto.Redeemed_dtmUpdated = x.Redeemed_dtmUpdated;
            datosBoleto.Redeemed_strDescription = x.Redeemed_strDescription;
            datosBoleto.Session_dtmRealShow = x.Session_dtmRealShow;
            datosBoleto.sName = x.sName;
            datosBoleto.token = null;
            datosBoleto.User_intUserNo = x.User_intUserNo;
            datosBoleto.Workstation_strCode = x.Workstation_strCode;
            datosBoleto.NumeroEntradas = x.NumeroEntradas;
            datosBoleto.EntradasRestantes = x.EntradasRestantes;
            return datosBoleto;
        }


        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties
    }
}