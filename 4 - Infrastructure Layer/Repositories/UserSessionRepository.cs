﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Premium;
using CinesUnidos.ESB.Infrastructure.Agents.Concessions;
using System;
using System.Collections.Generic;
using System.Linq;
using AGPayment = CinesUnidos.ESB.Infrastructure.Agents.Payment;
using System.Web.Hosting;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.ServiceRuntime;
using QRCoder;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Configuration;
using CinesUnidos.ESB.Domain.Contracts;
using System.Data;
using System.Data.SqlClient;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class UserSessionRepository : IUserSessionRepository
    {
        #region Attributes

        private ICache<UserSessionEntity> m_cache;
        private ICache<UserSessionMobileEntity> m_cacheMobile;
        private ITheaterBillboardRepository m_theaterBillboardRepository;
        private ICache<TicketEntity[]> m_cache_ticket;
        //private IConcessionsRepository m_ConcessionsRepo;

        #endregion Attributes

        #region Enumerator
        public enum QRCode
        {
            Alphanumeric, Concat
        }
        #endregion Enumerator

        #region Methods

        //public UserSessionRepository(ICache<UserSessionEntity> cache, ICache<UserSessionMobileEntity> cacheMobile, ITheaterBillboardRepository theaterBillboardRepository)
        //{
        //    m_cache = cache;
        //    m_cacheMobile = cacheMobile;
        //    m_theaterBillboardRepository = theaterBillboardRepository;
        //}

        public UserSessionRepository(ICache<UserSessionEntity> cache, ICache<UserSessionMobileEntity> cacheMobile, ITheaterBillboardRepository theaterBillboardRepository, ICache<TicketEntity[]> ticket)
        {
            m_cache = cache;
            m_cacheMobile = cacheMobile;
            m_theaterBillboardRepository = theaterBillboardRepository;
            m_cache_ticket = ticket;
        }

        public UserSessionEntity Get(Guid userSessionId)
        {
            UserSessionEntity userSession = m_cache.Get(userSessionId.ToString());

            return userSession;
        }

        public void Delete(Guid userSessionId)
        {
            m_cache.Delete(userSessionId.ToString());
        }

        public UserSessionEntity DeleteSession(string userSessionId)
        {
            UserSessionEntity _userSession = new UserSessionEntity();
            try
            {
                //Tomar valor de la sesión a borrar
                _userSession = m_cache.Get(userSessionId);
                //Cancelar orden en vista
                CancelOrder(userSessionId);
                //Borrar Id del Redis
                m_cache.Delete(userSessionId);

                return _userSession;
            }
            catch (Exception ex)
            {
                _userSession = null;
            }

            return _userSession;

        }

        public UserSessionResponseEntity Create(BasketEntity userSession)
        {
            UserSessionResponseEntity responce;
            userSession.Clear();

            #region Carga de Mapa

            MapInformation mapInformation;
            MapEntity mapEntity;
            string clientId = Configuration.Values["ClientID"];
            Guid id = Guid.NewGuid(); //Create a new Id order
            using (PremiumClient client = new PremiumClient())
            {
                client.Open();
                mapInformation = client.GetNewOrder(clientId, id.ToString().Replace("-", ""), userSession.TheaterId, userSession.SessionId, TicketType.GetTickets(userSession.Tickets), true);
                client.Close();
            }



            if (mapInformation.WithError)
            {
                responce = new UserSessionResponseEntity();
                responce.WithError = mapInformation.WithError;
                responce.ErrorMessage = mapInformation.ErrorMessage;
            }
            else
            {
                mapEntity = (MapEntity)mapInformation.Map;
                string[] seatsAvailable = mapInformation.Seats;
                string[] seatsSelected = mapInformation.SeatsSelected;
                //RM - reemplazar los tickets sin detalle por los tickets detallados
                var ticketes = mapInformation.Tickets.Select(t => new TicketEntity
                {
                    BarcodeRedemption = "",
                    BasePrice = t.BasePrice,
                    BookingFee = t.BookingFee,
                    BookingTax = t.BookingTax,
                    Code = t.Code,
                    FullPrice = t.FullPrice,
                    Name = t.Name,
                    Price = t.Price,
                    PromotionFee = t.PromotionFee,
                    PromotionFeeWithTax = t.TotalPromotionFee,
                    PromotionTax = t.PromotionTax,
                    Quantity = t.Quantity,
                    RedemptionOnly = false,
                    ServiceFee = t.ServiceFee,
                    ServiceFeeWithTax = t.TotalServiceFee,
                    ServiceTax = t.ServiceTax,
                    Tax = t.Tax,
                    TicketTax = t.TicketTax
                }).ToArray();
                userSession.Tickets = ticketes;
                var session = CreateUserSession(userSession, id);
                session.SetSeats(mapEntity, seatsSelected);
                session.ClientId = clientId;
                int timeOut = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Timeout"].ToString());
                session.TimeOut = DateTime.Now.AddMinutes(timeOut);
                timeOut = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PaymentTimeOut"].ToString());
                session.PaymentTimeOut = session.TimeOut.AddMinutes(timeOut);
                m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                responce = (UserSessionResponseEntity)session;
                mapEntity.Clear(!mapInformation.AreAvailable);

                if (mapInformation.AreAvailable)
                    UserSessionEntity.SetAvailable(mapEntity, seatsAvailable);
                else
                    UserSessionEntity.SetBooked(mapEntity, seatsAvailable);

                UserSessionEntity.SetSelected(mapEntity, seatsSelected);
                responce.Map = mapEntity;
            }


            #endregion Carga de Mapa

            return responce;
        }
        // metodo Create
        //public UserSessionResponseMobileEntity CreateMobile(BasketEntity userSession)
        //{
        //    UserSessionResponseMobileEntity responce;
        //    userSession.Clear();

        //    SeatMapResponse _SeatDataresponse;
        //    SeatMapResponseEntity _SeatData = new SeatMapResponseEntity();

        //    string clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"].ToString();//Configuration.Values["ClientID"];
        //    Guid id = Guid.NewGuid(); //Create a new Id order

        //    //Stopwatch watch = new Stopwatch();
        //    //watch.Start();
        //    using (PremiumClient client = new PremiumClient())
        //    {

        //        client.Open();
        //        //SeatData = client.GetNewOrderSeatData(clientId, id.ToString().Replace("-", ""), userSession.TheaterId, userSession.SessionId, TicketType.GetTickets(userSession.Tickets), true);
        //        _SeatDataresponse = client.GetNewOrderSeatData(clientId, id.ToString().Replace("-", ""), userSession.TheaterId, userSession.SessionId, TicketType.GetTickets(userSession.Tickets), true);
        //        client.Close();
        //    }
        //    //watch.Stop();
        //    //Console.WriteLine("Time elapsed: {0}",watch.Elapsed);

        //    if (_SeatDataresponse != null)
        //    {
        //        if (string.IsNullOrEmpty(_SeatDataresponse.SeatData))
        //        {
        //            responce = new UserSessionResponseMobileEntity();
        //            responce.WithError = true;
        //            responce.ErrorMessage = "sin resultado";
        //        }
        //        else
        //        {
        //            //Obtener el listado de tickets nuevamente
        //            _SeatData = (SeatMapResponseEntity)_SeatDataresponse;
        //            userSession.Tickets = _SeatData.Tickets;

        //            // watch.Start();
        //            var session = CreateUserSessionMobile(userSession, id);
        //            //watch.Stop();
        //            //Console.WriteLine("Time elapsed: {0}", watch.Elapsed);

        //            session.ClientId = clientId;
        //            int timeOut = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Timeout"].ToString());
        //            session.TimeOut = DateTime.Now.AddMinutes(timeOut);

        //            m_cacheMobile.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));

        //            responce = (UserSessionResponseMobileEntity)session;
        //            responce.Seats = _SeatDataresponse.SeatData;
        //        }
        //        return responce;
        //    }
        //    else
        //        return null;

        //}
        public UserSessionResponseEntity Read(Guid userSessionId, string readMap)
        {
            UserSessionResponseEntity responce = new UserSessionResponseEntity();
            UserSessionEntity userSession = m_cache.Get(userSessionId.ToString());
            if (userSession == null)
            {
                //responce = new UserSessionResponseEntity();
                responce.WithError = true;
                responce.ErrorMessage = "Lo sentimos, el tiempo de la sesión de compra ha expirado.";
                return responce;
            }
            responce = (UserSessionResponseEntity)userSession;

            //para reparar el problema del vencimiento de la sesion en el front y el backend
            TimeSpan sp = userSession.PaymentTimeOut - userSession.TimeOut;
            responce.TimeOut = DateTime.Now.AddMinutes(sp.TotalMinutes);
            bool getMap;
            if (bool.TryParse(readMap, out getMap))
            {
                if (getMap)
                {
                    string clientId = System.Configuration.ConfigurationManager.AppSettings["ClientID"].ToString(); //Configuration.Values["ClientID"];

                    MapInformation mapInformation;
                    MapEntity mapEntity;
                    string hallName = userSession.HallName;

                    using (PremiumClient client = new PremiumClient())
                    {
                        mapInformation = client.GetMap(clientId, userSession.Id.ToString().Replace("-", ""), userSession.SessionId, userSession.TheaterId, true);
                        client.Close();
                    }

                    if (mapInformation.WithError)
                    {
                        //responce = new UserSessionResponseEntity();
                        responce.WithError = mapInformation.WithError;
                        responce.ErrorMessage = mapInformation.ErrorMessage;
                        return responce;
                    }
                    else
                    {
                        mapEntity = (MapEntity)mapInformation.Map;

                        string[] seatsAvailable = mapInformation.Seats;
                        string[] seatsSelected = userSession.Seats.Select(s => s.Name).ToArray();
                        mapEntity.Clear(!mapInformation.AreAvailable);
                        if (mapInformation.AreAvailable)
                            UserSessionEntity.SetAvailable(mapEntity, seatsAvailable);
                        else
                            UserSessionEntity.SetBooked(mapEntity, seatsAvailable);
                        UserSessionEntity.SetSelected(mapEntity, seatsSelected);
                        responce.Map = mapEntity;
                        return responce;
                    }
                }
            }

            return responce;
        }

        public UserSessionResponseMobileEntity ReadMobile(Guid userSessionId)
        {
            UserSessionResponseMobileEntity responce = new UserSessionResponseMobileEntity();
            UserSessionMobileEntity userSession = new UserSessionMobileEntity();

            try
            {
                userSession = m_cacheMobile.Get(userSessionId.ToString());

                if (userSession == null)
                {
                    responce.WithError = true;
                    responce.ErrorMessage = "No existe el valor en los registros.";
                    return responce;
                }

                string clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"].ToString();//Configuration.Values["ClientId"];

                //if (String.IsNullOrEmpty(clientId))
                //{
                //    clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"].ToString();
                //}

                string hallName = userSession.HallName.ToString();
                string SeatData;

                #region MyRegion
                responce.BookingNumber = userSession.BookingNumber;
                responce.CodeResult = userSession.CodeResult;
                responce.Id = userSession.Id;
                responce.DescriptionResult = userSession.DescriptionResult;
                responce.ErrorMessage = String.Empty;
                responce.HallName = userSession.HallName;
                responce.Name = userSession.Name;
                responce.SessionId = userSession.SessionId;
                responce.ShowTime = userSession.ShowTime;
                responce.Theater = userSession.Theater;
                responce.TheaterId = userSession.TheaterId;
                responce.Tickets = userSession.Tickets;
                responce.TimeOut = userSession.TimeOut;
                responce.Title = userSession.Title;
                responce.Voucher = userSession.Voucher;
                responce.WithError = false;
                responce.Seats = String.Empty;
                #endregion

                using (PremiumClient client = new PremiumClient())
                {
                    SeatData = client.GetMapMobile(clientId, userSessionId.ToString().Replace("-", ""), userSession.SessionId, userSession.TheaterId);
                    client.Close();
                    responce.Seats = SeatData;
                }


            }
            catch (Exception ex)
            {
                responce.WithError = true;
                responce.ErrorMessage = ex.Message;
                return responce;
            }

            return responce;

        }

        public ConfirmationResponseEntity Confirmation(ConfirmationEntity userSession)
        {
            UserSessionEntity session = m_cache.Get(userSession.UserSessionId.ToString());
            ConfirmationResponseEntity responce = new ConfirmationResponseEntity();

            if (session != null)
            {
                responce = (ConfirmationResponseEntity)session;

                string clientId = Configuration.Values["ClientID"];
                TimeSpan sp = responce.PaymentTimeOut - responce.TimeOut;
                responce.TimeOut = DateTime.Now.AddMinutes(sp.TotalMinutes);

                if (session.ChageSeat(userSession.Seats))
                {

                    List<Seat> seats = new List<Seat>();
                    foreach (var seat in userSession.Seats)
                    {
                        seats.Add((Seat)seat);
                    }

                    bool result;
                    using (PremiumClient client = new PremiumClient())
                    {
                        result = client.ChangeSeats(clientId, session.Id.ToString().Replace("-", ""), session.TheaterId, session.SessionId, seats.ToArray());
                    }

                    if (!result)
                        responce.IsValid = false;
                    else
                    {
                        responce.IsValid = true;
                        responce.Seats = userSession.Seats;
                        session.Seats = userSession.Seats.ToList();
                        m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                    }
                }
                else
                {
                    responce.IsValid = true;
                }

                if (userSession.Concessions != null)
                {
                    List<ConcessionsEntity> concessions = new List<ConcessionsEntity>();
                    foreach (var concession in userSession.Concessions)
                    {
                        concessions.Add(concession);
                    }

                    responce.IsValid = true;
                    responce.Concessions = userSession.Concessions;

                    var concessions_client = new ConcessionsClient();

                    using (concessions_client = new ConcessionsClient())
                    {
                        var bookingFee = concessions_client.GetBookingFee(session.TheaterId);

                        session.PercentageTax = bookingFee.BookingTaxBookingFeeConcessions;
                        session.WebChargeConcessions = bookingFee.BookingFeeConcessions;
                        concessions_client.Close();
                    }
                    responce.WebChargeConcessions = session.WebChargeConcessions;
                    responce.PercentageTax = session.PercentageTax;

                    session.Concessions = userSession.Concessions.ToList();
                    if (userSession.BookingFee != null)
                    {
                        session.BookingFee = userSession.BookingFee;
                    }
                    m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                }

            }
            else
            {
                responce.IsValid = false;
            }

            return responce;
        }

        public ConfirmationResponseEntity ConfirmationMobile(ConfirmationEntity userSession)
        {
            UserSessionMobileEntity session = m_cacheMobile.Get(userSession.UserSessionId.ToString());
            ConfirmationResponseEntity responce = new ConfirmationResponseEntity();

            #region MyRegion
            responce.Id = session.Id;
            responce.SessionId = session.SessionId;
            responce.ShowTime = session.ShowTime;
            responce.Theater = session.Theater;
            responce.TheaterId = session.TheaterId;
            responce.Tickets = session.Tickets;
            responce.TimeOut = session.TimeOut;
            responce.Title = session.Title;
            #endregion

            string clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"].ToString();//Configuration.Values["ClientID"];

            PremiumClient client = new PremiumClient();
            List<Seat> seats = new List<Seat>();
            foreach (var seat in userSession.Seats)
            {
                seats.Add((Seat)seat);

            }
            bool result = client.ChangeSeats(clientId, session.Id.ToString().Replace("-", ""), session.TheaterId, session.SessionId, seats.ToArray());
            if (!result)
            {
                responce.IsValid = false;
                session.Seats = string.Join("|", (from r in seats select r.Name.ToString()).ToList());
                //Delete key
                m_cacheMobile.Delete(session.Id.ToString());
                //Create a key again
                m_cacheMobile.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(10));
            }
            else
            {
                responce.IsValid = true;
                responce.Seats = userSession.Seats;
                session.Seats = string.Join("|", (from r in responce.Seats select r.Name.ToString()).ToList());
                //Delete key
                m_cacheMobile.Delete(session.Id.ToString());
                //Create a key again
                m_cacheMobile.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(10));
            }

            return responce;
        }

        public Boolean CancelOrder(string UserSessionID)
        {
            try
            {
                string clientId = Configuration.Values["ClientID"];
                using (PremiumClient client = new PremiumClient())
                {
                    return client.CancelOrderRest(UserSessionID.ToString().Replace("-", ""));
                }
            }
            catch
            {
                return false;
            }
        }

        public PaymentResponseEntity Payment(PaymentEntity payment)
        {
            PaymentResponseEntity responce = new PaymentResponseEntity();
            UserSessionEntity session = m_cache.Get(payment.UserSessionId.ToString());

            if (session.Id.ToString() != null)
            {
                string userSessionId = payment.UserSessionId.ToString().Replace("-", "");
                ConfirmationResponseEntity confirmation = (ConfirmationResponseEntity)session;
                string clientId = Configuration.Values["ClientID"];
                AGPayment.PaymentResponse result;
                using (AGPayment.PaymentClient client = new AGPayment.PaymentClient())
                {
                    result = client.GetPaymentResponseByPaymentType(clientId, userSessionId, (AGPayment.PaymentInfo)payment);
                    client.Close();
                }
                responce = (PaymentResponseEntity)result;
                responce.Confirmation = confirmation;
                responce.Name = payment.Name;
                responce.TransactionNumber = result.TransactionNumber;
                //Actualizar UserSession y guardar información de pago al usuario
                #region Update UserSession
                //Localizador Alfanúmerico
                session.BookingId = responce.BookingId;
                session.BookingNumber = responce.BookingNumber;
                session.CodeResult = responce.CodeResult;
                session.DescriptionResult = responce.DescriptionResult;
                session.Voucher = responce.Voucher;
                session.Name = payment.Name;
                session.Email = payment.Email;

                ////Para uso de carameleria
                //session.TransIdTemp = result.TransactionNumber.ToString();
                //Si la transacción es satisfactoria CodeResult = 00, generar el código QR

                if (session.CodeResult.Equals("00") && session.Voucher != null)
                {
                    session.BookingByte = GenerateQRCoder(session, QRCode.Alphanumeric);
                    PurchasesEntity purchase = new PurchasesEntity();
                    purchase.Amount = session.Total;
                    purchase.CinemaTitle = session.Theater;
                    purchase.FilmTitle = session.Title;
                    purchase.CinemaCode = session.TheaterId;
                    purchase.Tickets = session.Seats.Count;
                    purchase.Date = DateTime.Now;
                    purchase.UserTypeId = payment.CertificateType;
                    purchase.UserId = payment.Certificate;
                    //purchase.FilmCode = session.CodeResult;
                    purchase.FilmCode = session.HO;
                    //LUIS RAMIREZ 12/07/2017, Agregar campos BookingNumber ó BookingId (Dependiendo del cine y SessionId
                    //string TheaterId = System.Configuration.ConfigurationManager.AppSettings["CinemaQR"].ToString();

                    //if (TheaterId.Contains(session.TheaterId))
                    //{
                    purchase.BookingNumber = session.BookingId;
                    //}
                    //else
                    //{
                    //    purchase.BookingNumber = session.BookingNumber.ToString();
                    //}
                    purchase.SessionId = session.SessionId;

                    PurchasesRepository p = new PurchasesRepository();
                    string res = p.RegisterComplete(purchase, session);
                    ////LUIS RAMIREZ 25/09/2017, Si la transacción tiene habilitada carameleria web
                    //if (session.Concessions.Count >= 0)
                    //{
                    //    var concessions_client = new ConcessionsClient();
                    //    using (concessions_client = new ConcessionsClient())
                    //    {                        
                    //        var bookingFee = concessions_client.GetBookingFee(session.TheaterId);

                    //        session.PercentageTax = bookingFee.BookingTaxBookingFeeConcessions;
                    //        session.WebChargeConcessions = bookingFee.BookingFeeConcessions;                                                    
                    //        concessions_client.Close();
                    //    }                    
                    //}

                    //por que esta comentada esta linea??? jchourio 2020
                    //Guardar en redis el resultado de la compra
                    //m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                }
                else
                {
                    CancelOrder(session.Id.ToString());
                    //m_cache.Delete(session.Id.ToString());
                    //responce.CodeResult = "999";
                }
                //Guardar en redis el resultado de la compra
                m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));

                #endregion Update UserSession
            }
            else
            {
                //Cuando es -1 significa que no se encontró la llave del redis del usuario
                responce.CodeResult = "999";
            }

            return responce;
        }

        public PaymentResponseEntity PaymentAuthMercantil(PaymentEntity payment)
        {
            PaymentResponseEntity responce = new PaymentResponseEntity();
            AuthResponse _authResponse = new AuthResponse();
            UserSessionEntity session = m_cache.Get(payment.UserSessionId.ToString());
            string userSessionId = payment.UserSessionId.ToString().Replace("-", "");
            ConfirmationResponseEntity confirmation = (ConfirmationResponseEntity)session;
            string clientId = Configuration.Values["ClientID"];

            AGPayment.PaymentResponse result;
            using (AGPayment.PaymentClient client = new AGPayment.PaymentClient())
            {
                result = client.GetAuthMercantil(clientId, userSessionId, (AGPayment.PaymentInfo)payment);
                client.Close();
            }

            responce = (PaymentResponseEntity)result;

            //if (session.Id.ToString() != null)
            //{
            //    string userSessionId = payment.UserSessionId.ToString().Replace("-", "");
            //    ConfirmationResponseEntity confirmation = (ConfirmationResponseEntity)session;
            //    string clientId = Configuration.Values["ClientID"];
            //    AGPayment.PaymentResponse result;
            //    using (AGPayment.PaymentClient client = new AGPayment.PaymentClient())
            //    {
            //        result = client.GetPaymentResponseByPaymentType(clientId, userSessionId, (AGPayment.PaymentInfo)payment);
            //        client.Close();
            //    }
            //    responce = (PaymentResponseEntity)result;
            //    responce.Confirmation = confirmation;
            //    responce.Name = payment.Name;
            //    responce.TransactionNumber = result.TransactionNumber;
            //    //Actualizar UserSession y guardar información de pago al usuario
            //    #region Update UserSession
            //    //Localizador Alfanúmerico
            //    session.BookingId = responce.BookingId;
            //    session.BookingNumber = responce.BookingNumber;
            //    session.CodeResult = responce.CodeResult;
            //    session.DescriptionResult = responce.DescriptionResult;
            //    session.Voucher = responce.Voucher;
            //    session.Name = payment.Name;
            //    session.Email = payment.Email;

            //    ////Para uso de carameleria
            //    //session.TransIdTemp = result.TransactionNumber.ToString();
            //    //Si la transacción es satisfactoria CodeResult = 00, generar el código QR

            //    if (session.CodeResult.Equals("00") && session.Voucher != null)
            //    {
            //        session.BookingByte = GenerateQRCoder(session, QRCode.Alphanumeric);
            //        PurchasesEntity purchase = new PurchasesEntity();
            //        purchase.Amount = session.Total;
            //        purchase.CinemaTitle = session.Theater;
            //        purchase.FilmTitle = session.Title;
            //        purchase.CinemaCode = session.TheaterId;
            //        purchase.Tickets = session.Seats.Count;
            //        purchase.Date = DateTime.Now;
            //        purchase.UserTypeId = payment.CertificateType;
            //        purchase.UserId = payment.Certificate;
            //        //purchase.FilmCode = session.CodeResult;
            //        purchase.FilmCode = session.HO;
            //        //LUIS RAMIREZ 12/07/2017, Agregar campos BookingNumber ó BookingId (Dependiendo del cine y SessionId
            //        //string TheaterId = System.Configuration.ConfigurationManager.AppSettings["CinemaQR"].ToString();

            //        //if (TheaterId.Contains(session.TheaterId))
            //        //{
            //        purchase.BookingNumber = session.BookingId;
            //        //}
            //        //else
            //        //{
            //        //    purchase.BookingNumber = session.BookingNumber.ToString();
            //        //}
            //        purchase.SessionId = session.SessionId;

            //        PurchasesRepository p = new PurchasesRepository();
            //        string res = p.RegisterComplete(purchase, session);
            //        ////LUIS RAMIREZ 25/09/2017, Si la transacción tiene habilitada carameleria web
            //        //if (session.Concessions.Count >= 0)
            //        //{
            //        //    var concessions_client = new ConcessionsClient();
            //        //    using (concessions_client = new ConcessionsClient())
            //        //    {                        
            //        //        var bookingFee = concessions_client.GetBookingFee(session.TheaterId);

            //        //        session.PercentageTax = bookingFee.BookingTaxBookingFeeConcessions;
            //        //        session.WebChargeConcessions = bookingFee.BookingFeeConcessions;                                                    
            //        //        concessions_client.Close();
            //        //    }                    
            //        //}

            //        //por que esta comentada esta linea??? jchourio 2020
            //        //Guardar en redis el resultado de la compra
            //        //m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
            //    }
            //    else
            //    {
            //        CancelOrder(session.Id.ToString());
            //        //m_cache.Delete(session.Id.ToString());
            //        //responce.CodeResult = "999";
            //    }
            //    //Guardar en redis el resultado de la compra
            //    m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));

            //    #endregion Update UserSession
            //}
            //else
            //{
            //    //Cuando es -1 significa que no se encontró la llave del redis del usuario
            //    responce.CodeResult = "999";
            //}

            return responce;
        }

        public PaymentResponseEntity PaymentCandies(PaymentEntity payment)
        {
            UserSessionEntity session = m_cache.Get(payment.UserSessionId.ToString());
            try
            {
                CandiesSingleton singleton = CandiesSingleton.Instance;
                string clientId = Configuration.Values["ClientID"];
                return singleton.MerchantPayment(payment, session, clientId, m_cache);
            }
            catch (Exception e)
            {
                PaymentResponseEntity responce = new PaymentResponseEntity();
                responce.CodeResult = "555";
                responce.DescriptionResult = "No hay disponibilidad para la cantidad de producto(s) seleccionado(s).";
                session.CodeResult = "555";
                session.DescriptionResult = "No hay disponibilidad para la cantidad de producto(s) seleccionado(s).";
                m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                return responce;

            }
        }

        public PaymentResponseEntity PaymentMobile(PaymentEntity payment)
        {
            PaymentResponseEntity responce = null;
            UserSessionMobileEntity session = m_cacheMobile.Get(payment.UserSessionId.ToString());
            if (session != null)
            {
                string userSessionId = payment.UserSessionId.ToString().Replace("-", "");
                ConfirmationResponseEntity confirmation = new ConfirmationResponseEntity();

                confirmation.Id = session.Id;
                confirmation.SessionId = session.SessionId;
                confirmation.ShowTime = session.ShowTime;
                confirmation.Theater = session.Theater;
                confirmation.TheaterId = session.TheaterId;
                confirmation.Tickets = session.Tickets;
                confirmation.TimeOut = session.TimeOut;
                confirmation.Title = session.Title;

                string clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"].ToString();// Configuration.Values["ClientID"];

                AGPayment.PaymentResponse result;
                using (AGPayment.PaymentClient client = new AGPayment.PaymentClient())
                {
                    result = client.GetPaymentResponseByPaymentType(clientId, userSessionId, (AGPayment.PaymentInfo)payment);
                    client.Close();
                }
                responce = (PaymentResponseEntity)result;
                responce.Confirmation = confirmation;
                responce.Name = payment.Name;

                #region Update UserSession

                session.BookingNumber = responce.BookingNumber;
                //localizador alfanúmerico
                session.BookingId = responce.BookingId;
                //
                session.CodeResult = responce.CodeResult;
                session.DescriptionResult = responce.DescriptionResult;
                session.Voucher = responce.Voucher;
                session.Name = payment.Name;
                session.Email = payment.Email;

                if (session.CodeResult.Equals("00"))
                {
                    PurchasesEntity purchase = new PurchasesEntity();
                    purchase.Amount = session.Total;
                    purchase.CinemaTitle = session.Theater;
                    purchase.FilmTitle = session.Title;
                    purchase.CinemaCode = session.TheaterId;
                    purchase.Tickets = session.Seats.Length;
                    purchase.Date = DateTime.Now;
                    purchase.UserTypeId = payment.CertificateType;
                    purchase.UserId = payment.Certificate;
                    purchase.FilmCode = session.CodeResult;

                    //LUIS RAMIREZ 12/07/2017, Agregar campos BookingNumber ó BookingId (Dependiendo del cine y SessionId)
                    //string TheaterId = Configuration.Values["CinemaQR"];

                    //if (TheaterId.Contains(session.TheaterId))
                    //{
                    purchase.BookingNumber = session.BookingId;
                    //}
                    //else
                    //{
                    //    purchase.BookingNumber = session.BookingNumber.ToString();
                    //}
                    purchase.SessionId = session.SessionId;

                    PurchasesRepository p = new PurchasesRepository();

                    string res = p.Register(purchase);
                }

                m_cacheMobile.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(5));

                #endregion Update UserSession
            }
            return responce;
        }

        public AVippoResponse GetVippoConfirmation(APaymentRequest Payment)
        {
            try
            {
                AGPayment.APaymentRequest PaymentRequest = new AGPayment.APaymentRequest()
                {
                    Amount = Payment.Amount,
                    Client_PasswordVippo = Payment.Client_PasswordVippo,
                    Client_UserVippo = Payment.Client_UserVippo,
                };
                AGPayment.PaymentClient Client = new AGPayment.PaymentClient();
                var AGPResponse = Client.GetVippoConfirmation(PaymentRequest);
                AVippoResponse Response = new AVippoResponse()
                {
                    APIKEY = AGPResponse.APIKEY,
                    Message = AGPResponse.Message,
                    RtnCde = AGPResponse.RtnCde,
                    SessionExpires = AGPResponse.SessionExpires,
                    SessionToken = AGPResponse.SessionToken,
                    Success = AGPResponse.Success,
                    VippoCommerce = AGPResponse.VippoCommerce,
                    Reference = AGPResponse.Reference,
                    Label = AGPResponse.Label

                };
                return Response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string BDVGeneratePayment(BDVUserInfo info)
        {
            AGPayment.PaymentClient Client = new AGPayment.PaymentClient();
            AGPayment.BDVUserInfo userinfo = new AGPayment.BDVUserInfo()
            {
                Cellphone = info.Cellphone,
                ClientId = info.ClientId,
                Email = info.Email,
                IdCardNumber = info.IdCardNumber,
                IdCardType = info.IdCardType
            };

            return Client.BDVGeneratePayment(userinfo);
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        //ConfigurationEntity Domain.Contracts.IConfiguration.Configuration
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }

        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        #endregion Properties

        #region Private Mehtods
        //manda aqui la informacion actualizada de los tickets para mostar los montos reales
        private UserSessionEntity CreateUserSession(BasketEntity userSession, Guid id)
        {
            UserSessionEntity session = new UserSessionEntity();
            session.Id = id;
            session.TheaterId = userSession.TheaterId;
            session.SessionId = userSession.SessionId;
            session.Tickets = userSession.Tickets;
            session.HO = userSession.MovieId;
            TheaterBillboardEntity theater =
                m_theaterBillboardRepository.Data.FirstOrDefault(b => b.Theater.Id.Equals(userSession.TheaterId));
            var movie = (from m in theater.Movies
                         where m.Movie.Id.Equals(userSession.MovieId)
                         select new { Movie = m.Movie, Sessions = m.Sessions }).FirstOrDefault();
            session.Title = movie.Movie.Title;
            session.ShowTime = movie.Sessions.Where(s => s.Id.Equals(userSession.SessionId)).FirstOrDefault().ShowTime;
            session.Theater = theater.Theater.Name;
            session.HallName = movie.Sessions.Where(s => s.Id.Equals(userSession.SessionId)).FirstOrDefault().HallName;
            session.ValidConcessions = ValidateCinemaModal(int.Parse(session.TheaterId));
            return session;
        }


        /// <summary>
        /// RM
        /// Muestra la modal de compra de carameleria por el cine 
        /// </summary>
        /// <param name="cinemaID"></param>
        /// <returns></returns>
        private bool ValidateCinemaModal(int cinemaID)
        {

            TimeZoneInfo timeZone = System.TimeZoneInfo.FindSystemTimeZoneById("Venezuela Standard Time");
            DateTime nowDateTime = DateTime.Now;
            DateTime newDateTime = TimeZoneInfo.ConvertTime(nowDateTime, timeZone);
            if (newDateTime.Hour < int.Parse(ConfigurationManager.AppSettings["MinHour"]) || newDateTime.Hour > int.Parse(ConfigurationManager.AppSettings["MaxHour"]))
            {
                return false;
            }

            string filePath = String.Empty;
            //Determinar ruta donde se encuentra el archivo (Local o LocalResource)
            //#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_Data/ModalConcessions.txt"));
            //#endif

            //#if (!DEBUG)
            //                        string cachePath = RoleEnvironment.GetLocalResource("CacheTemporal").RootPath;
            //                        filePath = Path.Combine(cachePath, "ModalConcessions.txt");
            //#endif

            try
            {
                if (File.Exists(filePath))
                {
                    using (StreamReader reader = new StreamReader(filePath))
                    {
                        var CinemaModals = JsonConvert.DeserializeObject<List<ModalConcesions>>(reader.ReadToEnd());
                        var cinemaModal = CinemaModals.Where(c => c.CinemaCode == cinemaID).SingleOrDefault();

                        if (cinemaModal != null && cinemaModal.ModalEnabled == true)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    //si el archivo no existe, llamo el metodo que lo crea...
                    ConcessionsRepository r = new ConcessionsRepository();
                    r.ModifyModalConcessions();
                    using (StreamReader reader = new StreamReader(filePath))
                    {
                        var CinemaModals = JsonConvert.DeserializeObject<List<ModalConcesions>>(reader.ReadToEnd());
                        var cinemaModal = CinemaModals.Where(c => c.CinemaCode == cinemaID).SingleOrDefault();

                        if (cinemaModal != null && cinemaModal.ModalEnabled == true)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private UserSessionMobileEntity CreateUserSessionMobile(BasketEntity userSession, Guid id)
        {
            UserSessionMobileEntity session = new UserSessionMobileEntity();

            session.Id = id;
            session.TheaterId = userSession.TheaterId;
            session.SessionId = userSession.SessionId;
            session.Tickets = userSession.Tickets;
            TheaterBillboardEntity theater = m_theaterBillboardRepository.Data.FirstOrDefault(b => b.Theater.Id.Equals(userSession.TheaterId));

            var movie = theater.Movies.FirstOrDefault(m => m.Movie.Id.Equals(userSession.MovieId));

            //var movie = (from m in theater.Movies
            //             where m.Movie.Id.Equals(userSession.MovieId)
            //             select new { Movie = m.Movie, Sessions = m.Sessions }).FirstOrDefault();
            session.Title = movie.Movie.Title;
            session.ShowTime = movie.Sessions.Where(s => s.Id.Equals(userSession.SessionId)).FirstOrDefault().ShowTime;
            session.Theater = theater.Theater.Name;
            session.HallName = movie.Sessions.Where(s => s.Id.Equals(userSession.SessionId)).FirstOrDefault().HallName;
            return session;

        }

        public UserSessionEntity GetMobile(Guid userSessionId)
        {
            UserSessionMobileEntity userSession = m_cacheMobile.Get(userSessionId.ToString());
            UserSessionEntity user = new UserSessionEntity();


            user.BookingNumber = userSession.BookingNumber;
            user.ClientId = userSession.ClientId;
            user.CodeResult = userSession.CodeResult;
            user.DescriptionResult = userSession.DescriptionResult;
            user.Email = userSession.Email;
            user.HallName = userSession.HallName;
            user.Id = userSession.Id;
            user.Name = userSession.Name;
            user.PaymentTimeOut = userSession.TimeOut;
            user.SessionId = userSession.SessionId;
            user.ShowTime = userSession.ShowTime;
            user.TheaterId = userSession.TheaterId;
            user.Theater = userSession.Theater;
            user.TimeOut = userSession.TimeOut;
            user.Title = userSession.Title;
            user.Tickets = userSession.Tickets;
            user.Voucher = userSession.Voucher;
            user.BookingId = userSession.BookingId;

            if (!string.IsNullOrEmpty(userSession.Seats))
            {
                foreach (string s in userSession.Seats.Split('|'))
                {
                    SeatEntity seat = new SeatEntity();
                    seat.Name = s.ToString();
                    user.Seats.Add(seat);
                }
            }

            if (user.CodeResult.Equals("00") && user.Voucher != null)
            {
                user.BookingByte = GenerateQRCoder(user, QRCode.Alphanumeric);
                //user.BookingConcat = GenerateQRCoder(user, QRCode.Concat);
            }
            else
                CancelOrder(user.Id.ToString());
            return user;
        }

        private string GenerateQRCoder(UserSessionEntity user, QRCode Code)
        {
            string FileStoragePath = string.Empty;
            //Generar listado de cines con lector QR
            //List<string> CinemaQRList = System.Configuration.ConfigurationManager.AppSettings["CinemaQR"].Split(';').ToList();
            //Validar que el cine se encuentre o no en la lista
            //var value = CinemaQRList.Where(c => c.Contains(user.TheaterId)).SingleOrDefault();
            //if (!string.IsNullOrEmpty(value))
            //{
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData;
            string fileName = string.Empty;

            qrCodeData = qrGenerator.CreateQrCode(user.BookingId, QRCodeGenerator.ECCLevel.Q);
            //FileName = CodCine-Localizador Alfanumerico-TokenUsuario.jpeg
            fileName = user.TheaterId + "-" + user.BookingId.ToString() + "-" + user.Id.ToString().Replace("-", "") + ".jpeg";

            QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            Bitmap bImage = new Bitmap(qrCodeImage, new Size(240, 240));
            string filePath = String.Empty;

            //Crear ruta local o storage para guardar el codigo qr
            //#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_Data/" + fileName));
            //#endif

            //#if (!DEBUG)
            //                string QRPath = RoleEnvironment.GetLocalResource("QRCode").RootPath;
            //                filePath = Path.Combine(QRPath, fileName);
            //#endif
            bImage.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            //Save Image to Storage as blob into container "qrcodes"                
            FileStoragePath = SaveImageToCloudStorage(fileName, filePath);

            //}
            //else
            //{
            //    FileStoragePath = "";
            //}

            return FileStoragePath;
        }

        //Guardar codigo QR dentro del contenedor del Storage (qrcodes)
        private string SaveImageToCloudStorage(string fileName, string filePath)
        {
            string BlobPath = string.Empty;

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("qrcodes");

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

            using (var fileStream = File.OpenRead(filePath))
            {
                blockBlob.UploadFromStream(fileStream);
            }

            //Borrar qr alojado temporalmente en App_Data (Local) ó Cloud (Resources)
            File.Delete(filePath);

            //System.IO.File.Delete(FileStoragePath);

            return BlobPath = fileName;

            //return BlobPath;
        }

        #endregion Private Methods

    }
}