﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Web.Hosting;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.IO;
using System.Net;
using System.Configuration;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class ContingencyRepository : DAOGeneral, IContingencyRepository
    {
        #region Constructor
        public ContingencyRepository()
        {

        }
        #endregion Constructor

        #region Methods
        /// <summary>
        /// Método para consultar si ya existe un mensaje con el código pasado en el parámetro
        /// </summary>
        /// <param name="code">Código del Mensaje a Consultar</param>
        /// <returns>Retorna un objeto con la respuesta de la consulta de la base de datos
        /// si el código ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        public ReplyEntity existCodeMessage(int code)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                var cmd = new SqlCommand("Access.nMessagesCode", con) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add("@code", SqlDbType.Int).Value = code;
                con.Open();
                var nroRows = (int)cmd.ExecuteScalar();

                if (nroRows < 1) return new ReplyEntity { isOk = true };
                var errors = new List<string> { ResourcesGeneralDAO.MessageErrorExistCode };
                con.Close();
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
            catch (SqlException)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                var errors = new List<string> { ResourcesGeneralDAO.MessageErrorDB };
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para verificar si ya existe en la base de datos un cine con el código pasado en el parámetro
        /// </summary>
        /// <param name="code">Código del cine</param>
        /// <returns>Retorna un objeto con la respuesta de la consulta de la base de datos
        /// si el código ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        public ReplyEntity existCodeTheater(int code)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.nTheaterCode", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@code", SqlDbType.Int).Value = code;
                con.Open();
                var nroRows = (int)cmd.ExecuteScalar();

                if (nroRows >= 1)
                {
                    var errors = new List<string>();
                    errors.Add(ResourcesGeneralDAO.MessageErrorExistCode);
                    con.Close();
                    return new ReplyEntity
                    {
                        isOk = false,
                        Messages = errors
                    };
                }
                con.Close();
                return new ReplyEntity { isOk = true };
            }
            catch (SqlException e)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                var errors = new List<string>();
                errors.Add(ResourcesGeneralDAO.MessageErrorDB);
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para verificar en la base de datos si existe un cine con el nombre pasado en el parámetro
        /// </summary>
        /// <param name="name">Nombre del Cine</param>
        /// <returns>Retorna un objeto con la respuesta de la consulta de la base de datos
        /// si el nombre ya existe retorna IsOk con el valor true
        /// si no existe IsOk se le asigna el valor false
        /// si se presenta algún error de conexión con la base de datos se retorna una lista de errores 
        /// además que el valor de IsOk será false
        /// </returns>
        public ReplyEntity existNameTheater(string name)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.nTheaterName", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                con.Open();
                var nroRows = (int)cmd.ExecuteScalar();

                if (nroRows >= 1)
                {
                    var errors = new List<string>();
                    errors.Add(ResourcesGeneralDAO.MessageErrorExistNameTheater);
                    con.Close();
                    return new ReplyEntity
                    {
                        isOk = false,
                        Messages = errors
                    };
                }
                con.Close();
                return new ReplyEntity { isOk = true };
            }
            catch (SqlException e)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                var errors = new List<string>();
                errors.Add(ResourcesGeneralDAO.MessageErrorDB);
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para crear en la Base de Datos un mensaje
        /// </summary>
        /// <param name="message">Objeto Mensaje</param>
        /// <returns>
        /// Retorna un objeto del tipo Reply si su atributo IsOk vale true es que se salvó correctamente
        /// en la base de datos, de lo contrario retornará con valor false y una lista de errores.
        /// </returns>
        public ReplyEntity createMessage(MessageEntity message)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.CreateMessage", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@code", SqlDbType.Int).Value = message.Code;
                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = message.Message;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return new ReplyEntity { isOk = true };
            }
            catch (SqlException e)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                var errors = new List<string>();
                errors.Add(ResourcesGeneralDAO.MessageErrorDB);
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para crear un Cine en la base de datos
        /// </summary>
        /// <param name="theater">Objeto del tipo Cine</param>
        /// <returns>
        /// Retorna un objeto del tipo Reply si su atributo IsOk tiene valor true es que se salvó
        /// correctamente en la base de datos, de lo contrario retornará con valor false y una lista de errores.
        /// </returns>
        public ReplyEntity createTheater(TheaterDBEntity theater)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.CreateTheater", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@code", SqlDbType.Int).Value = theater.Code;
                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = theater.Name;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return new ReplyEntity { isOk = true };
            }
            catch (SqlException e)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                var errors = new List<string>();
                errors.Add(ResourcesGeneralDAO.MessageErrorDB);
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para crear una eventualidad en la base de datos
        /// </summary>
        /// <param name="eventuality">Objeto del tipo Eventuality</param>
        /// <returns>Retorna un objeto del tipo Reply si su atributo IsOk tiene valor true es que se salvó
        /// correctamente en la base de datos, de lo contrario retornará con valor false y una lista de errores
        /// </returns>
        public ReplyEntity createEventuality(EventualityEntity eventuality)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.CreateEventuality", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@status", SqlDbType.Bit).Value = Convert.ToBoolean(eventuality.Status) ? 1 : 0;

                if (string.IsNullOrEmpty(eventuality.Creation))
                    cmd.Parameters.Add("@creation", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("@creation", SqlDbType.DateTime).Value = eventuality.Creation;

                if (string.IsNullOrEmpty(eventuality.Finish))
                    cmd.Parameters.Add("@finish", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("@finish", SqlDbType.DateTime).Value = eventuality.Finish;

                cmd.Parameters.Add("@theater_code", SqlDbType.Int).Value = eventuality.TheaterCode;
                cmd.Parameters.Add("@message_code", SqlDbType.Int).Value = eventuality.MessageCode;

                con.Open();
                var response = cmd.ExecuteScalar();
                var id = new Guid(response.ToString());
                con.Close();
                return new ReplyEntity { isOk = true, Id = id };
            }
            catch (SqlException e)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                var errors = new List<string>();
                errors.Add(ResourcesGeneralDAO.MessageErrorDB);
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para obtener las eventualidades activas en un cine
        /// </summary>
        /// <param name="theater">Objeto del tipo cine</param>
        /// <returns>
        /// Retorna una lista de objetos del tipo Mensaje, el atributo Id es el Id de la eventualidad
        /// los otros atributos si pertenecen al mensaje
        /// </returns>
        public List<MessageEventualityEntity> getEventualityMessage(TheaterDBEntity theater)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.GetEventualityMessages", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@theater_code", SqlDbType.Int).Value = theater.Code;

                con.Open();
                SqlDataReader data = cmd.ExecuteReader();
                var list = new List<MessageEventualityEntity>();
                while (data.Read())
                {
                    var message = new MessageEventualityEntity();
                    message.Id = new Guid(data["id"].ToString());
                    message.Code = int.Parse(data["code"].ToString());
                    message.Message = data["message"].ToString();
                    if (!String.IsNullOrEmpty(data["creation"].ToString()))
                        message.Creation = string.IsNullOrEmpty(data["creation"].ToString()) ? null : data["creation"].ToString();
                    if (!String.IsNullOrEmpty(data["finish"].ToString()))
                        message.Finish = string.IsNullOrEmpty(data["finish"].ToString()) ? null : data["finish"].ToString();
                    list.Add(message);
                }
                con.Close();
                return list;
            }
            catch (SqlException)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                throw;
            }

        }

        /// <summary>
        /// Método para desactivar una eventualidad
        /// </summary>
        /// <param name="eventuality">Objeto del tipo Eventuality</param>
        /// <returns>
        /// Retorna un objeto del tipo Reply, si la eventualidad se desactivó correctamente retornará 
        /// el atributo IsOk con valor false, de lo contrario retornará false y una lista de errores.
        /// </returns>
        public ReplyEntity deactivateEventuality(EventualityEntity eventuality)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.DeactivateEventuality", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = eventuality.Id;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return new ReplyEntity { isOk = true };
            }
            catch (SqlException e)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                var errors = new List<string>();
                errors.Add(ResourcesGeneralDAO.MessageErrorDB);
                return new ReplyEntity
                {
                    isOk = false,
                    Messages = errors
                };
            }
        }

        /// <summary>
        /// Método para obtener todos los mensajes
        /// </summary>
        /// <returns>Retorna una lista de Mensajes</returns>
        public List<MessageEntity> getAllMessages()
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.GetAllMessages", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader data = cmd.ExecuteReader();

                var list = new List<MessageEntity>();

                while (data.Read())
                {
                    var message = new MessageEntity();
                    message.Code = int.Parse(data["code"].ToString());
                    message.Message = data["message"].ToString();
                    message.IsValid = true;
                    list.Add(message);
                }
                con.Close();
                return list;
            }
            catch (SqlException)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                throw;
            }

        }

        /// <summary>
        /// Método para obtener todos los cines
        /// </summary>
        /// <returns>Retorna una lista de Cines</returns>
        public List<TheaterDBEntity> getAllTheaters()
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.GetAllTheaters", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader data = cmd.ExecuteReader();

                var list = new List<TheaterDBEntity>();

                while (data.Read())
                {
                    var theater = new TheaterDBEntity();
                    theater.Code = int.Parse(data["code"].ToString());
                    theater.Name = data["name"].ToString();
                    theater.IsValid = true;
                    list.Add(theater);
                }
                con.Close();
                return list;
            }
            catch (SqlException)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                throw;
            }

        }


        /// <summary>
        /// Método para obtener las eventualidades activas en un cine
        /// </summary>
        /// <param name="theater">Objeto del tipo cine</param>
        /// <returns>
        /// Retorna una lista de objetos del tipo Mensaje, el atributo Id es el Id de la eventualidad
        /// los otros atributos si pertenecen al mensaje
        /// </returns>
        public List<MessageEventualityEntity> getEventualityMessagesTheater(TheaterDBEntity theater)
        {
            SqlConnection con = null;
            try
            {
                con = Connect();
                SqlCommand cmd = new SqlCommand("Access.GetEventualityMessagesTheater", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@theater_code", SqlDbType.Int).Value = theater.Code;

                con.Open();
                SqlDataReader data = cmd.ExecuteReader();

                var list = new List<MessageEventualityEntity>();
                while (data.Read())
                {
                    var eventuality = new MessageEventualityEntity();

                    eventuality.Code = int.Parse(data["code"].ToString());
                    eventuality.Message = data["message"].ToString();
                    eventuality.Status = Convert.ToBoolean(data["status"]);
                    var prueba = data["id"];
                    if (!String.IsNullOrEmpty(data["id"].ToString()))
                        eventuality.Id = new Guid(data["id"].ToString());
                    if (!String.IsNullOrEmpty(data["creation"].ToString()))
                        eventuality.Creation = Convert.ToDateTime(data["creation"]).ToString("dd/MM/yyyy HH:mm");
                    if (!String.IsNullOrEmpty(data["finish"].ToString()))
                        eventuality.Finish = Convert.ToDateTime(data["finish"]).ToString("dd/MM/yyyy HH:mm");
                    list.Add(eventuality);
                }
                con.Close();
                return list;
            }
            catch (SqlException)
            {
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                throw;
            }

        }


        /// <summary>
        /// Este metodo recibe un string que contiene todos los mensajes genrados en el CMS en formato json. dicho string luego es guardado en un archivo XML
        /// para hacerle fileCaching
        /// </summary>
        /// <param name="ListMessages">este parametro es el string con los mensajes en formato json y un token que viene del cms para verificar la autenticidad de la peticion</param>
        /// <returns></returns>
        public ResultEntity PublishMessage(MessagePack ListMessages)
        {
            ResultEntity result = new ResultEntity();
            string filePath = String.Empty;
            try
            {
                //Determinar ruta donde se va a guardar el archivo (Local o LocalResource)
                //#if DEBUG
                filePath = (HostingEnvironment.MapPath("~/App_Data/Messages.xml"));
                //#endif

                //#if (!DEBUG)
                //                string cachePath = RoleEnvironment.GetLocalResource("CacheTemporal").RootPath;
                //                filePath = Path.Combine(cachePath, "Messages.xml");
                //#endif

                if (!File.Exists(filePath))
                    File.Create(filePath).Close();
                else
                {
                    File.Delete(filePath);
                    File.Create(filePath).Close();
                }

                //string json = JsonConvert.SerializeObject(ListMessages);
                //File.WriteAllText(filePath, ListMessages);
                using (StreamWriter Sw = new StreamWriter(filePath, false))
                {
                    Sw.WriteLine(ListMessages.MessageList);
                    Sw.Close();
                };

                result.number = 0;
                result.message = "Publicación realizada satisfactoriamente";

            }
            catch (Exception ex)
            {
                result.number = -1;
                result.message = ex.InnerException.Message;
            }
            return result;
        }

        /// <summary>
        /// metodo para leer el archivo xml de mensajes y devolverlo al MVC
        /// </summary>
        /// 
        public string MessagesForCinema()
        {
            string messages = string.Empty;
            string filePath = string.Empty;
            List<MessageForCinemaEntity> messageslist = new List<MessageForCinemaEntity>();
            try
            {
                //#if DEBUG
                filePath = (HostingEnvironment.MapPath("~/App_Data/Messages.xml"));
                //#endif

                //#if (!DEBUG)
                //                string cachePath = RoleEnvironment.GetLocalResource("cacheTemporal").RootPath;
                //                filePath = Path.Combine(cachePath, "Messages.xml");
                //#endif
                if (!File.Exists(filePath))
                {
                    //consultar DB directamente
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MessagesFromDB"].ToString());
                    SqlCommand cmd = new SqlCommand("select * from MessagesForCinemas", con);
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        MessageForCinemaEntity m = new MessageForCinemaEntity()
                        {
                            Activo = bool.Parse(reader[4].ToString()),
                            Category = (Category)Enum.Parse(typeof(Category), reader[5].ToString()),
                            Cinema = (Cinema)Enum.Parse(typeof(Cinema), reader[1].ToString()),
                            City = (city)Enum.Parse(typeof(city), reader[7].ToString()),
                            Message = reader[3].ToString()
                        };
                        messageslist.Add(m);
                    }
                    con.Close();
                    if (messageslist.Count > 0)
                    {
                        messages = JsonConvert.SerializeObject(messageslist);
                    }
                    //WebClient client = new WebClient();
                    //Stream data = client.OpenRead(ConfigurationManager.AppSettings["MesagesDBUrl"].ToString());
                    //StreamReader reader = new StreamReader(data);
                    //messages = reader.ReadToEnd();
                    //data.Close();
                    //reader.Close();
                    //client.Dispose();
                    //si el texto recibido es menor de 10 caracteres significa que no hay un mensaje real que mostrar, entonces hago la respuesta NA
                    if (messages.Length < 10)
                    {
                        messages = "N/A";
                    }
                    File.Create(filePath).Close();
                    using (StreamWriter Sw = new StreamWriter(filePath, false))
                    {
                        Sw.WriteLine(messages);
                        Sw.Close();
                    }
                }
                else
                {
                    using (StreamReader Sr = new StreamReader(filePath))
                    {
                        string a = Sr.ReadLine();
                        Sr.Close();
                        if (a.Length < 10)
                        {
                            messages = "N/A";
                        }
                        else
                        {
                            messages = a;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                messages = e.Message;
            }
            return messages;
        }

        #endregion Methods


        #region Properties
        public ConfigurationEntity Configuration { get; set; }
        #endregion Properties
    }
}
