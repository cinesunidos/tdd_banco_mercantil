﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Concessions;
using System.Collections.Generic;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Hosting;
using System.IO;
using Microsoft.WindowsAzure.ServiceRuntime;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 20-03-2017
    /// Descripcion: Clase que implementa los metodos de la clase ConcessionsRepository
    /// </summary>
    public class ConcessionsRepository : IConcessionsRepository
    {
        public ConcessionListEntity GetTheaterConcessions(string theaterId)
        {

            var response = new ConcessionListEntity();

            using (ConcessionsClient concessions_client = new ConcessionsClient())
            {
                List<ConcessionsEntity> listConcessions = new List<ConcessionsEntity>();
                var serviceResponse = concessions_client.GetTheaterConcessions(theaterId);

                foreach (ConcessionItem item in serviceResponse.Items)
                {
                    var concession = new ConcessionsEntity();

                    concession.ItemStrItemID = item.ItemStrItemID;
                    concession.ItemHOPK = item.ItemHOPK1;
                    concession.ItemStrItemDescription = item.ItemStrItemDescription;
                    concession.ItemPriceDCurPrice = item.ItemPriceDCurPrice;
                    concession.ItemQuantityPurchase = item.ItemQuantityPurchase;
                    concession.ItemStockCurAvailable = item.ItemStockCurAvailable;
                    concession.ItemType = item.ItemType;
                    concession.RecipeQuantity = item.RecipeQuantity;
                    concession.ClassStrCode = item.ClassStrCode;

                    listConcessions.Add(concession);
                }

                // filtrar productos cuyos ingredientes sean menores a lo que indica la receta
                //List<ConcessionsEntity> recipes = listConcessions.GroupBy(x => x.ItemStrItemID).Select(y => y.First()).ToList();
                List<ConcessionsEntity> recipes = listConcessions
                    .Where(x => x.ItemHOPK == x.ItemStrItemID)
                    .ToList();

                foreach (var item in serviceResponse.Items)
                {
                    // filtrar productos de 1er nivel (type = "") cuya cantidad en stock sea igual o menor que 0
                    if (item.ItemType == "" && item.ItemStockCurAvailable <= 0)
                    {
                        var entityToRemove = recipes
                            .Where(x => x.ItemStrItemID == item.ItemStrItemID)
                            .FirstOrDefault();
                        if (entityToRemove != null)
                        {
                            recipes.Remove(entityToRemove);
                        }
                    }

                    // filtrar ingredientes (type = "C") cuya cantidad en stock sea menor que la cantidad indicada en la receta
                    if (item.ItemType == "C" && item.ItemStockCurAvailable < item.RecipeQuantity)
                    {
                        var entityToRemove = recipes
                            .Where(x => x.ItemStrItemID == item.ItemStrItemID)
                            .FirstOrDefault();
                        if (entityToRemove != null)
                        {
                            recipes.Remove(entityToRemove);
                        }
                    }
                }

                response.Items = recipes;

                // Booking Fee
                var bookingFee = new ConcessionsEntity();
                bookingFee.ItemStrItemID = serviceResponse.BookingFee.ItemStrItemID;
                bookingFee.ItemStrItemDescription = serviceResponse.BookingFee.ItemStrItemDescription;
                bookingFee.ItemPriceDCurPrice = serviceResponse.BookingFee.ItemPriceDCurPrice;
                bookingFee.ItemType = serviceResponse.BookingFee.ItemType;

                response.BookingFee = bookingFee;

            }

            return response;
        }

        public ConcessionsRSEntity AddItemsToCart(ConcessionsRQEntity concessionsRq)
        {

            try
            {

                var concessions_client = new ConcessionsClient();

                var qty = concessionsRq.Concessions != null ? concessionsRq.Concessions.Count() : 0;
                ConcessionItem[] concessionArray = new ConcessionItem[qty];

                if (concessionsRq.Concessions != null)
                {

                    int x = 0;

                    foreach (var item in concessionsRq.Concessions)
                    {

                        var c = new ConcessionItem();

                        c.ItemStrItemID = item.ItemStrItemID;
                        c.ItemHOPK1 = item.ItemHOPK;
                        c.ItemStrItemDescription = item.ItemStrItemDescription;
                        c.ItemQuantityPurchase = item.ItemQuantityPurchase;
                        c.ItemStockCurAvailable = item.ItemStockCurAvailable;
                        c.ItemQuantityPurchase = item.ItemQuantityPurchase;
                        c.ItemPriceDCurPrice = item.ItemPriceDCurPrice;
                        c.ItemType = item.ItemType;
                        c.RecipeQuantity = item.RecipeQuantity;
                        c.ItemLocationStrCode = item.ItemLocationStrCode;
                        c.ItemLocationStrDescription = item.ItemLocationStrDescription;

                        concessionArray[x] = c;

                        x++;
                    }
                }

                var concessionOrder = concessions_client.AddItemsToCart(
                    concessionsRq.UserSessionId,
                    concessionsRq.TheaterId,
                    concessionsRq.SessionId.ToString(),
                    concessionsRq.TransIdTemp,
                    concessionArray
                );

                var response = new ConcessionsRSEntity();
                if (concessionOrder.Order != null)
                {
                    response.WithError = false;
                    response.Order = buildOrder(concessionOrder.Order);

                }
                else
                {
                    response.WithError = true;
                    response.ErrorMessage = concessionOrder.ErrorMessage;
                }

                return response;
            }
            catch (Exception ex)
            {
                var response = new ConcessionsRSEntity();
                response.WithError = true;
                response.ErrorMessage = ex.Message;
                return response;
            }

        }

        private ConcessionOrderEntity buildOrder(ConcessionOrder entity)
        {
            var o = new ConcessionOrderEntity();
            o.TransIdTemp = entity.TransIdTemp;

            // items
            o.Items = new List<ConcessionsEntity>();
            foreach (var item in entity.Items)
            {
                var orderItem = buildItem(item);
                o.Items.Add(orderItem);
            }

            // booking fee
            var bookingFee = new ConcessionsEntity();
            bookingFee.ItemStrItemID = entity.BookingFee.ItemStrItemID;
            bookingFee.ItemPriceDCurPrice = entity.BookingFee.ItemPriceDCurPrice;
            bookingFee.ItemStrItemDescription = entity.BookingFee.ItemStrItemDescription;
            o.BookingFee = bookingFee;





            return o;
        }

        private ConcessionsEntity buildItem(ConcessionItem entity)
        {
            var c = new ConcessionsEntity();

            c.ItemStrItemID = entity.ItemStrItemID;
            c.ItemHOPK = entity.ItemHOPK1;
            c.ItemStrItemDescription = entity.ItemStrItemDescription;
            c.ItemQuantityPurchase = entity.ItemQuantityPurchase;
            c.ItemStockCurAvailable = entity.ItemStockCurAvailable;
            c.ItemQuantityPurchase = entity.ItemQuantityPurchase;
            c.ItemPriceDCurPrice = entity.ItemPriceDCurPrice;
            c.ItemType = entity.ItemType;
            c.RecipeQuantity = entity.RecipeQuantity;
            c.ItemLocationStrCode = entity.ItemLocationStrCode;
            c.ItemLocationStrDescription = entity.ItemLocationStrDescription;

            return c;
        }

        public ConcessionsRSEntity PurchaseConcessions(ConcessionsRQEntity concessionsRq)
        {

            try
            {

                var concessions_client = new ConcessionsClient();
                var qty = concessionsRq.Concessions.Count();
                var concessionArray = new ConcessionItem[qty];
                var clientId = ConfigurationManager.AppSettings["POSClientId"];

                int x = 0;

                foreach (var item in concessionsRq.Concessions)
                {

                    var c = new ConcessionItem();

                    c.ItemStrItemID = item.ItemStrItemID;
                    c.ItemHOPK1 = item.ItemHOPK;
                    c.ItemStrItemDescription = item.ItemStrItemDescription;
                    c.ItemQuantityPurchase = item.ItemQuantityPurchase;
                    c.ItemStockCurAvailable = item.ItemStockCurAvailable;
                    c.ItemQuantityPurchase = item.ItemQuantityPurchase;
                    c.ItemPriceDCurPrice = item.ItemPriceDCurPrice;
                    c.ItemLocationStrCode = item.ItemLocationStrCode;
                    c.ItemLocationStrDescription = item.ItemLocationStrDescription;
                    c.RecipeQuantity = item.RecipeQuantity;
                    c.ItemType = item.ItemType;

                    concessionArray[x] = c;

                    x++;
                }

                var result = concessions_client.PurchaseConcessions(
                    concessionsRq.TransIdTemp,                  // Transaction Id (Temp)
                    concessionsRq.TransactionNumber,            // Transaction Number
                    concessionsRq.BookingNumber.ToString(),     // Booking Number
                    concessionsRq.UserSessionId,
                    concessionsRq.TheaterId,
                    concessionsRq.SessionId.ToString(),
                    concessionArray
                );

                var response = new ConcessionsRSEntity();
                response.WithError = !result;
                return response;
            }
            catch (Exception ex)
            {
                var response = new ConcessionsRSEntity();
                response.WithError = true;
                response.ErrorMessage = ex.Message;
                return response;
            }

        }

        public ConcessionsRSEntity CancelConcessionsOrder(ConcessionsRQEntity concessionsRq)
        {
            try
            {
                var concessions_client = new ConcessionsClient();
                bool result;
                if ((!string.IsNullOrEmpty(concessionsRq.TransactionNumber)) && (concessionsRq.TransactionNumber != "0"))
                {
                    //var result = concessions_client.CancelConcessionsOrder(concessionsRq.UserSessionId, concessionsRq.TheaterId, concessionsRq.TransIdTemp,concessionsRq.TransactionNumber);
                    result = concessions_client.CancelConcessionsOrder(concessionsRq.UserSessionId, concessionsRq.TheaterId, concessionsRq.TransIdTemp, concessionsRq.TransactionNumber);
                }
                else
                {
                    //var result = concessions_client.CancelConcessionsOrder(concessionsRq.UserSessionId, concessionsRq.TheaterId, concessionsRq.TransIdTemp, "");
                    result = concessions_client.CancelConcessionsOrder(concessionsRq.UserSessionId, concessionsRq.TheaterId, concessionsRq.TransIdTemp, "");
                }

                var response = new ConcessionsRSEntity();
                response.WithError = !result;
                return response;
            }
            catch (Exception ex)
            {
                var response = new ConcessionsRSEntity();
                response.WithError = true;
                response.ErrorMessage = ex.Message;
                return response;
            }
        }

        public ConcessionsRSEntity UpdateConcessionsOrder(UpdateConcessionsOrderRequest request)
        {
            try
            {
                var concessions_client = new ConcessionsClient();

                var result = concessions_client.UpdateConcessionsOrder(
                    request.TheaterId,
                    request.TransIdTemp,
                    request.TransactionNumber,
                    request.BookingNumber,
                    request.PaymentStart,
                    request.PaymentOK,
                    request.OrderComplete,
                    request.OrderCancel
                );

                var response = new ConcessionsRSEntity();
                response.WithError = false;
                //response.Order = result;
                //response.TransIdTemp = request.TransIdTemp;
                return response;
            }
            catch (Exception ex)
            {
                var response = new ConcessionsRSEntity();
                response.WithError = true;
                response.ErrorMessage = ex.Message;
                return response;
            }
        }

        /// <summary>
        /// Ramiro Marimon - Metodo para crear la caché de configuracion de ventanas modales para los cines con caramelería
        /// </summary>
        /// <returns></returns>
        public MessageEntity ModifyModalConcessions()
        {
            MessageEntity result = new MessageEntity();
            string filePath = String.Empty;
            try
            {
                //Determinar ruta donde se va a guardar el archivo (Local o LocalResource)
                //#if DEBUG
                filePath = (HostingEnvironment.MapPath("~/App_Data/ModalConcessions.txt"));
                //#endif

                //#if (!DEBUG)
                //                string cachePath = RoleEnvironment.GetLocalResource("CacheTemporal").RootPath;
                //                filePath = Path.Combine(cachePath, "ModalConcessions.txt");
                //#endif

                #region Obtener informacion de la BD
                List<ModalConcesions> l = new List<ModalConcesions>();
                using (SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
                {
                    string csql = "select * from ModalConcessions";
                    SqlCommand cmd = new SqlCommand(csql, c);
                    c.Open();
                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        ModalConcesions mc = new ModalConcesions
                        {
                            CinemaCode = int.Parse(r[0].ToString()),
                            CinemaName = r[1].ToString(),
                            DateChanged = DateTime.Parse(r[3].ToString()),
                            ModalEnabled = bool.Parse(r[2].ToString())
                        };
                        l.Add(mc);
                    }
                    c.Close();
                }
                #endregion

                if (!File.Exists(filePath))
                {
                    using (StreamWriter Sw = new StreamWriter(filePath, false))
                    {
                        //List<ModalConcesions> lista = new List<ModalConcesions>();
                        //ModalConcesions m = new ModalConcesions();
                        //m = JsonConvert.DeserializeObject<ModalConcesions>(ListModalConcessions.MessageList);
                        //lista.Add(m);
                        Sw.WriteLine(JsonConvert.SerializeObject(l));
                        Sw.Close();
                    }
                }
                else
                {
                    using (StreamWriter Sw = new StreamWriter(filePath, false))
                    {
                        //List<ModalConcesions> l = new List<ModalConcesions>();
                        //l = JsonConvert.DeserializeObject<List<ModalConcesions>>(text);
                        //ModalConcesions m = JsonConvert.DeserializeObject<ModalConcesions>(ListModalConcessions.MessageList);
                        //l = l.Where(i => i.CinemaCode != m.CinemaCode).ToList();
                        //l.Add(m);
                        Sw.WriteLine(JsonConvert.SerializeObject(l));
                        Sw.Close();
                    };
                }

                result.Code = 0;
                result.Message = "Publicación realizada satisfactoriamente";

            }
            catch (Exception ex)
            {
                result.Code = -1;
                result.Message = ex.InnerException.Message;
            }
            return result;
        }

        //public string PromoPepsi(string Codigo)
        //{
        //    //var response = new ConcessionListEntity();

        //    using (ConcessionsClient concessions_client = new ConcessionsClient())
        //    {
        //        var serviceResponse = concessions_client.PromoPepsi(Codigo);
        //        return serviceResponse;
        //    }

        //}

        public ConcessionListEntity GetCandiesWeb(string theaterId)
        {
            var response = new ConcessionListEntity();

            using (ConcessionsClient concessions_client = new ConcessionsClient())
            {
                List<ConcessionsEntity> listConcessions = new List<ConcessionsEntity>();
                var serviceResponse = concessions_client.GetCandiesWeb(theaterId);

                #region cambio a paralel
                //foreach (ConcessionItem item in serviceResponse.Items)
                //{
                //    var concession = new ConcessionsEntity();

                //    concession.ItemStrItemID = item.ItemStrItemID;
                //    concession.ItemHOPK = item.ItemHOPK1;
                //    concession.ItemStrItemDescription = item.ItemStrItemDescription;
                //    concession.ItemPriceDCurPrice = item.ItemPriceDCurPrice;
                //    concession.ItemQuantityPurchase = item.ItemQuantityPurchase;
                //    concession.ItemStockCurAvailable = item.ItemStockCurAvailable;
                //    concession.ItemType = item.ItemType;
                //    concession.RecipeQuantity = item.RecipeQuantity;
                //    concession.ClassStrCode = item.ClassStrCode;

                //    listConcessions.Add(concession);
                //}
                #endregion

                Parallel.ForEach(serviceResponse.Items, item =>
                {
                    var concession = new ConcessionsEntity();
                    concession.ItemStrItemID = item.ItemStrItemID;
                    concession.ItemHOPK = item.ItemHOPK1;
                    concession.ItemStrItemDescription = item.ItemStrItemDescription;
                    concession.ItemPriceDCurPrice = item.ItemPriceDCurPrice;
                    concession.ItemQuantityPurchase = item.ItemQuantityPurchase;
                    concession.ItemStockCurAvailable = item.ItemStockCurAvailable;
                    concession.ItemType = item.ItemType;
                    concession.RecipeQuantity = item.RecipeQuantity;
                    concession.ClassStrCode = item.ClassStrCode;
                    listConcessions.Add(concession);
                });

                List<ConcessionsEntity> recipes = listConcessions.Where(r => r != null).ToList();

                foreach (var item in serviceResponse.Items)
                {
                    if (item.ItemStockCurAvailable <= 0)
                    {
                        var entityToRemove = recipes
                            .Where(x => x.ItemStrItemID == item.ItemStrItemID)
                            .FirstOrDefault();
                        if (entityToRemove != null)
                        {
                            recipes.Remove(entityToRemove);
                        }
                    }
                }


                response.Items = recipes;

                // Booking Fee
                var bookingFee = new ConcessionsEntity();
                bookingFee.ItemStrItemID = serviceResponse.BookingFee.ItemStrItemID;
                bookingFee.ItemStrItemDescription = serviceResponse.BookingFee.ItemStrItemDescription;
                bookingFee.ItemPriceDCurPrice = serviceResponse.BookingFee.ItemPriceDCurPrice;
                bookingFee.ItemType = serviceResponse.BookingFee.ItemType;

                response.BookingFee = bookingFee;
            }

            return response;
        }

        public ConcessionsRSEntity AddItemsCandiesWeb(ConcessionsRQEntity concessionsRq)
        {
            try
            {
                var responseConcession = new ConcessionListEntity();

                var concessions_client = new ConcessionsClient();

                var qty = concessionsRq.Concessions != null ? concessionsRq.Concessions.Count() : 0;
                ConcessionItem[] concessionArray = new ConcessionItem[qty];

                if (concessionsRq.Concessions != null)
                {

                    int x = 0;

                    foreach (var item in concessionsRq.Concessions)
                    {
                        var c = new ConcessionItem();
                        c.ItemStrItemID = item.ItemStrItemID;
                        c.ItemHOPK1 = item.ItemHOPK;
                        c.ItemStrItemDescription = item.ItemStrItemDescription;
                        c.ItemQuantityPurchase = item.ItemQuantityPurchase;
                        c.ItemStockCurAvailable = item.ItemStockCurAvailable;
                        c.ItemQuantityPurchase = item.ItemQuantityPurchase;
                        c.ItemPriceDCurPrice = item.ItemPriceDCurPrice;
                        c.ItemType = item.ItemType;
                        c.RecipeQuantity = item.RecipeQuantity;
                        c.ItemLocationStrCode = item.ItemLocationStrCode;
                        c.ItemLocationStrDescription = item.ItemLocationStrDescription;

                        concessionArray[x] = c;

                        x++;
                    }
                }

                var concessionOrder = concessions_client.AddItemsToCartCandiesWeb(
                   concessionsRq.UserSessionId,
                   concessionsRq.TheaterId,
                   concessionsRq.SessionId.ToString(),
                   concessionsRq.TransIdTemp,
                   concessionArray
               );

                var response = new ConcessionsRSEntity();


                if (concessionOrder.Order != null)
                {
                    response.WithError = false;
                    response.Order = buildOrder(concessionOrder.Order);

                }
                else
                {
                    response.WithError = true;
                    response.ErrorMessage = concessionOrder.ErrorMessage;
                }

                return response;
            }
            catch (Exception ex)
            {
                var response = new ConcessionsRSEntity();
                response.WithError = true;
                response.ErrorMessage = ex.Message;
                return response;
            }


        }

        public TransferPack UpdateCandiesTable(TransferPack items)
        {
            List<TransferItem> lista = JsonConvert.DeserializeObject<List<TransferItem>>(items.MessageList);
            //hacer update la tabla de azure (para que juan no se moleste conmigo)
            //SqlConnection con = new SqlConnection();
            TransferPack m = new TransferPack();
            try
            {
                using (SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand("", c);
                    foreach (var i in lista)
                    {
                        string csql = string.Format("select * from tblCandiesWeb where Item_strItemId = '{0}' and Cinema_strCode = '{1}'", i.itemCode, items.idCine);
                        cmd.CommandText = csql;
                        c.Open();
                        SqlDataReader r = cmd.ExecuteReader();

                        if (r.HasRows)
                        {
                            c.Close();
                            csql = string.Format("update tblCandiesWeb set Item_decStockAvailable = Item_decStockAvailable + {0} where Item_strItemId = '{1}' and Cinema_strCode = '{2}'", i.Quantity, i.itemCode, items.idCine);
                            cmd.CommandText = "";
                            cmd.CommandText = csql;
                            c.Open();
                            cmd.ExecuteNonQuery();
                            c.Close();
                        }
                        else
                        {
                            c.Close();
                            csql = string.Format("insert into tblCandiesWeb (Cinema_strCode, Item_strItemId, Item_strItemDescription, Item_decStockAvailable, Item_dtmLastTransaction, Item_strItemType, Unit_strCode) values ('{0}','{1}','{2}','{3}', getdate(), '{4}', '{5}')", items.idCine, i.itemCode, i.itemDescription, i.Quantity, i.itemType, i.UnidadDeMedida);
                            cmd.CommandText = "";
                            cmd.CommandText = csql;
                            c.Open();
                            cmd.ExecuteNonQuery();
                            c.Close();
                        }
                    }
                }
                m.MessageList = "Tabla de Azure actualizada.";
                m.Token = "00";
            }
            catch (Exception e)
            {
                m.MessageList = e.Message;
                m.Token = "05";
            }

            return m;
        }

        public TransferPack DecreaseCandiesTable(TransferPack items)
        {
            List<TransferItem> lista = JsonConvert.DeserializeObject<List<TransferItem>>(items.MessageList);
            //hacer update la tabla de azure (para que juan no se moleste conmigo)
            //SqlConnection con = new SqlConnection();
            TransferPack m = new TransferPack();
            try
            {
                using (SqlConnection c = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand("", c);
                    c.Open();
                    foreach (var i in lista)
                    {
                        string csql = string.Format("update tblCandiesWeb set Item_decStockAvailable = Item_decStockAvailable - {0} where Item_strItemId = '{1}' and Cinema_strCode = '{2}'", i.Quantity, i.itemCode, items.idCine);
                        cmd.CommandText = "";
                        cmd.CommandText = csql;
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }

                m.MessageList = "Tabla de Azure actualizada.";
                m.Token = "00";
            }
            catch (Exception e)
            {
                m.MessageList = e.Message;
                m.Token = "05";
            }

            return m;
        }

        #region Promo Desorden  
        public Domain.PromoPack PromoDesorden(Domain.PromoPack Pack)
        {
            try
            {
                if (!PromoDesordenVerify(Pack))
                {
                    ConcessionsClient Client = new ConcessionsClient();
                    Agents.Concessions.PromoPack Promo = Client.PromoDesorden(Pack.TransactionId.ToString(), Pack.CinemaId.ToString());
                    Pack.Status = Promo.Status;

                    Pack.Workstation = Promo.Workstation;
                    Pack.Mensaje = Promo.Mensaje;
                    Pack.Token = string.Empty;
                    if (Pack.Status)
                    {
                        PromoDesordenRegister(Pack);
                    }
                    return Pack;
                }
                else
                {
                    Pack.Token = string.Empty;
                    Pack.Mensaje = "Usted ya Disfrutó de la Promoción";
                    return Pack;
                }
            }
            catch (Exception Ex)
            {
                Pack.Mensaje = Ex.Message;
                return Pack;
            }
        }

        public bool PromoDesordenVerify(Domain.PromoPack Pack)
        {
            try
            {
                using (SqlConnection Connect = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
                {
                    string Query = $"SELECT RedemptionCode FROM PromoDesordenPublico WHERE RedemptionCode = {Pack.TransactionId}";
                    SqlCommand Command = new SqlCommand(Query, Connect);
                    Connect.Open();
                    SqlDataReader DataReader = Command.ExecuteReader();
                    if (DataReader.HasRows)
                    {
                        DataReader.Close();
                        Connect.Close();
                        return true;
                    }
                    else
                    {
                        DataReader.Close();
                        Connect.Close();
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void PromoDesordenRegister(Domain.PromoPack Pack)
        {
            try
            {
                using (SqlConnection Connect = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
                {
                    string Query = $"INSERT INTO PromoDesordenPublico (RedemptionCode, CinemaId, Status, WorkStation, Date) values ('{Pack.TransactionId}','{Pack.CinemaId}','{Pack.Status.ToString()}','{Pack.Workstation}',GETDATE())";
                    SqlCommand Command = new SqlCommand(Query, Connect);
                    Connect.Open();
                    Command.ExecuteNonQuery();
                    Connect.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        #endregion Properties

    }

}