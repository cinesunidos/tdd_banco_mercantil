﻿using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CinesUnidos.Infrastructure.Repositories
{
    public class BlobsRepository : IBlobsRepository
    {
        #region Attributes

        private CloudStorageAccount m_storageAccount = null;
        private CloudBlobClient m_blobClient = null;
        private string m_name;
        private string m_key;
        private string m_connection;
        private string[] m_containers = new string[] { "poster", "slider", "files" };
        private Dictionary<string, CloudBlobContainer> m_cloudBlobContainer;

        #endregion Attributes

        #region Properties

        public string Name
        {
            get { return m_name; }
        }

        public string Key
        {
            get { return m_key; }
        }

        public string[] Containers
        {
            get
            {
                return m_containers.ToArray();
            }
        }

        #endregion Properties

        #region Constructor

        public BlobsRepository(string accountName, string accountKey)
        {
            m_key = accountKey;
            m_name = accountName;
            m_connection = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", m_name, m_key);
            m_storageAccount = CloudStorageAccount.Parse(m_connection);
            m_blobClient = m_storageAccount.CreateCloudBlobClient();
            LoadContainer();
        }

        #endregion Constructor

        #region Methods

        private void LoadContainer()
        {
            m_cloudBlobContainer = new Dictionary<string, CloudBlobContainer>();
            foreach (string container in m_containers)
            {
                m_cloudBlobContainer.Add(container, m_blobClient.GetContainerReference(container));
            }
        }

        private CloudBlobContainer GetCloudBlobContainer(string name)
        {
            return m_cloudBlobContainer[name];
        }

        public MemoryStream SearchFile(string fileName, string containerName)
        {
            CloudBlobContainer container = GetCloudBlobContainer(containerName);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            if (Exists(fileName, containerName))
            {
                blockBlob.FetchAttributes();
                long length = blockBlob.Properties.Length;
                if (length == 0)
                {
                    string message = string.Format("Name:{0} [Container:{1}]", fileName, containerName);
                    throw new ApplicationException(message);
                }
                MemoryStream stream = new MemoryStream();
                blockBlob.DownloadToStream(stream);
                return stream;
            }
            else
            {
                string message = string.Format("Name:{0} [Container:{1}]", fileName, containerName);
                throw new ApplicationException(message);
            }
        }

        public bool Exists(string fileName, string containerName)
        {
            CloudBlobContainer container = GetCloudBlobContainer(containerName);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            try
            {
                blockBlob.FetchAttributes();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion Methods
    }
}