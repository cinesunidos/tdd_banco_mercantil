﻿using CinesUnidos.ESB.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Management.Compute;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using System.Web.Hosting;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Net;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class CloudServicesObject
    {
        public String InstanceName { get; set; }
        public String IpInternal { get; set; }
        public String IpPublic { get; set; }
    }
    public class RedisCacheManagementRepository : IRedisCacheManagementRepository

    {
        private IConnectionFactory m_factory;

        private ICache<string> m_cache;
        public ConfigurationEntity Configuration { get; set; }

        public RedisCacheManagementRepository(ICache<string> cache)
        {
            m_cache = cache;
        }
        public RedisCacheManagementRepository()
        {
            m_factory = new ConnectionFactory("ELMAH");
        }

        private string DeleteKeys(string[] keys)
        {
            string message;
            try
            {
                //Eliminar archivos .xml
                foreach (var key in keys)
                {
                    //string cachePath = RoleEnvironment.GetLocalResource(key).RootPath;
                    string cachePath = HostingEnvironment.MapPath("~/App_Data/");
                    string filePath = Path.Combine(cachePath, key + ".xml");
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }
                }

                //Borra Slider desde el CMS
                DeleteHomeSlider();
                message = "ok";
            }
            catch (Exception ex)
            {
                message = "Error al borrar Llaves :" + ex.Message;
            }
            return message;
        }

        private void DownloadSliderHome()
        {
            //string sliderPath = RoleEnvironment.GetLocalResource("AllMoviesLocal").RootPath;
            string sliderPath = HostingEnvironment.MapPath("~/App_Data/");
            var sliderXML = Path.Combine(sliderPath, "Home.xml");

            WebClient client = new WebClient();
            if (File.Exists(sliderXML))
            {
                File.Delete(sliderXML);
            }
            client.DownloadFile("http://az693035.vo.msecnd.net/files/Home.xml", sliderXML);

        }

        private string DeleteHomeSlider()
        {
            string message;
            try
            {
                //Eliminar archivos .xml

                //string sliderPath = RoleEnvironment.GetLocalResource("AllMoviesLocal").RootPath;
                string sliderPath = HostingEnvironment.MapPath("~/App_Data/");
                var sliderXML = Path.Combine(sliderPath, "Home.xml");

                if (File.Exists(sliderXML))
                {
                    File.Delete(sliderXML);
                }

                //Descarga el slider en la ruta.
                DownloadSliderHome();

                message = "ok";
            }
            catch (Exception ex)
            {
                message = "Error al borrar Llaves :" + ex.Message;
            }
            return message;
        }


        public string DeleteCacheAll()
        {
            string msg = "";
            try
            {
                // Descomentar cuando se suba a Cloud Services
                //string[] keys = new string[] { "Theaters", "MovieBillboard", "TheaterBillboard", "Premiers", "DateTicketPrice", "CityTicketPrice","AllMoviesLocal" };
                string[] keys = new string[] { "MovieBillboard", "TheaterBillboard", "Premiers", "DateTicketPrice", "CityTicketPrice", "AllMoviesLocal" };

                if (DeleteKeys(keys).Equals("ok"))
                {
                    msg = "--Se borraron todas las llaves encontradas en caché";
                }
            }
            catch (Exception ex)
            {
                msg = string.Format("Error: {0}", ex.Message);
            }

            return msg;
            //m_cache.Delete("DateTicketPrice");
            //m_cache.Delete("CityTicketPrice");
            //m_cache.Delete("TheaterBillboard");
            //m_cache.Delete("MovieBillboard");
            //m_cache.Delete("Premiers");
            //m_cache.Delete("Theaters");
            //m_cache.Close();            
        }

        public string DeleteCacheNoTicketsPrice()
        {
            //m_cache.Delete("TheaterBillboard");
            //m_cache.Delete("MovieBillboard");
            //m_cache.Delete("Premiers");
            //m_cache.Delete("Theaters");
            //m_cache.Close();
            //return ("--Se borraron todas las llaves del caché, excepto las referentes a la lista de precio de los boletos");
            string msg = "";
            try
            {
                //string[] keys = new string[] { "Theaters","TheaterBillboard", "MovieBillboard", "Premiers", "Theaters", "AllMoviesLocal" };
                string[] keys = new string[] { "TheaterBillboard", "MovieBillboard", "Premiers", "AllMoviesLocal" };

                if (DeleteKeys(keys).Equals("ok"))
                {
                    msg = "--Se borraron todas las llaves del caché, excepto las referentes a la lista de precio de los boletos";
                }

                return msg;
            }
            catch (Exception ex)
            {
                msg = string.Format("Error: {0}", ex.Message);
            }

            return msg;
        }

        public string DeleteCacheTicketsPrice()
        {
            //m_cache.Delete("DateTicketPrice");
            //m_cache.Delete("CityTicketPrice");
            //m_cache.Close();
            string msg = "";

            try
            {
                string[] keys = new string[] { "DateTicketPrice", "CityTicketPrice" };


                if (DeleteKeys(keys).Equals("ok"))
                {
                    msg = "--Se borraron las llaves del caché referentes a la lista de precio de los boletos";
                }
            }
            catch (Exception ex)
            {
                msg = string.Format("Error: {0}", ex.Message);
            }

            //return ("--Se borraron las llaves del caché referentes a la lista de precio de los boletos");
            return msg;
        }

        public string ReadDateCacheTicketsPrice()
        {
            //string rs = m_cache.Get("DateTicketPrice");
            //m_cache.Close();
            //if (rs == null)
            //    msg = "No se encontró resultado.";
            //else
            //    msg = string.Format("Fecha cache Lista de Precios de los boletos: {0} ", rs.ToString());

            string msg = "";
            string result = "";
            //string cachePath = RoleEnvironment.GetLocalResource("DateTicketPrice").RootPath;
            string cachePath = HostingEnvironment.MapPath("~/App_Data/");
            string filePath = Path.Combine(cachePath, "DateTicketPrice.xml");

            if (File.Exists(filePath))
                result = File.ReadAllText(filePath);
            else
                result = "false";


            if (result.Equals("false"))
                msg = " Genere las llaves DateTicketPrice y CityTicketPrice";
            else
                msg = string.Format("Fecha cache Lista de Precios de los boletos: {0} ", result);

            return (msg);
        }

        public string DeletePremiers()
        {
            string[] keys = new string[] { "Premiers", };

            string msg = "";
            if (DeleteKeys(keys).Equals("ok"))
            {
                msg = "--Se borraron las llaves del caché referentes a Próximos Estrenos";
            }

            return msg;
            //m_cache.Delete("Premiers");
            //m_cache.Close();
            //return ("--Se borraron las llaves del caché referentes a Próximos Estrenos");
        }

        public string GetPathLocalStorage()
        {
            string Path = string.Empty;

            //#if DEBUG
            Path = (HostingEnvironment.MapPath("~/App_Data/"));
            //#endif

            //#if (!DEBUG)
            //             Path = RoleEnvironment.GetLocalResource("CacheTemporal").RootPath;


            //#endif

            return Path;
        }
    }
}
