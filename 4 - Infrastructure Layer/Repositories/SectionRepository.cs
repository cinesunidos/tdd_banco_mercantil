﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    internal class SectionRepository : Repository<SectionEntity>
    {
        #region Constructor

        public SectionRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion Constructor

        #region Methods

        public override void Create(Domain.SectionEntity section)
        {
            throw new NotImplementedException();
        }

        public override void Update(Domain.SectionEntity section)
        {
            throw new NotImplementedException();
        }             

        public override void Delete(Domain.SectionEntity section)
        {
            throw new NotImplementedException();
        }        

        public List<SectionEntity> Read(string platform)
        {
            var result = new List<SectionEntity>();
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [Id]
                              ,[Name]
                              ,[PlatformName]
                          FROM [dbo].[Mobile_VSection]
                          WHERE [PlatformName] = @P";

                command.CommandText = sql;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@P";
                idParameter.Value = platform;

                command.Parameters.Add(idParameter);

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var Section = new SectionEntity();

                        Section.Id = reader.GetGuid(0);
                        if (reader[1] != null && reader[1].ToString() != string.Empty) Section.Name = reader.GetString(1);
                        if (reader[2] != null && reader[2].ToString() != string.Empty) Section.PlatformName = reader.GetString(2);
                        result.Add(Section);
                    }
                }
            }
            return result;
        }               

        protected override void Map(IDataRecord record, SectionEntity entity)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}