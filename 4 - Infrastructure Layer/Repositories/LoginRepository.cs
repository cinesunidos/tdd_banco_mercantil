﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    internal class LoginRepository : Repository<UserEntity>
    {
        #region Constructor

        public LoginRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion Constructor

        #region Methods

        public override void Create(Domain.UserEntity user)
        {
            user.Active = false;
            user.CreationDate = DateTime.Now;
            user.Id = Guid.NewGuid();
            using (var command = this.Context.CreateCommand())
            {
                command.CommandText = "Access.RegisterUser";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                #region CreatingParameters

                IDbDataParameter activeParameter = command.CreateParameter();
                activeParameter.ParameterName = "@Active";
                activeParameter.Value = user.Active;

                IDbDataParameter addressParameter = command.CreateParameter();
                addressParameter.ParameterName = "@Address";
                addressParameter.Value = user.Address;

                IDbDataParameter birthDateParameter = command.CreateParameter();
                birthDateParameter.ParameterName = "@Birth";
                birthDateParameter.Value = user.BirthDate;

                IDbDataParameter cityParameter = command.CreateParameter();
                cityParameter.ParameterName = "@City";
                cityParameter.Value = user.City;

                IDbDataParameter creationDateParameter = command.CreateParameter();
                creationDateParameter.ParameterName = "@Creation";
                creationDateParameter.Value = user.CreationDate;

                IDbDataParameter emailParameter = command.CreateParameter();
                emailParameter.ParameterName = "@Email";
                emailParameter.Value = user.Email;

                IDbDataParameter facebookParameter = command.CreateParameter();
                facebookParameter.ParameterName = "@Facebook";
                facebookParameter.Value = user.Facebook;

                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.Value = user.Id;

                IDbDataParameter idCardParameter = command.CreateParameter();
                idCardParameter.ParameterName = "@IdCard";
                idCardParameter.Value = user.IdCard;

                IDbDataParameter instagramParameter = command.CreateParameter();
                instagramParameter.ParameterName = "@Instagram";
                instagramParameter.Value = user.Instagram;

                IDbDataParameter lastNameParameter = command.CreateParameter();
                lastNameParameter.ParameterName = "@LastName";
                lastNameParameter.Value = user.LastName;

                IDbDataParameter mobilePhoneParameter = command.CreateParameter();
                mobilePhoneParameter.ParameterName = "@MobilePhone";
                mobilePhoneParameter.Value = user.MobilePhone;

                IDbDataParameter nameParameter = command.CreateParameter();
                nameParameter.ParameterName = "@Name";
                nameParameter.Value = user.Name;

                IDbDataParameter passwordParameter = command.CreateParameter();
                passwordParameter.ParameterName = "@Password";
                passwordParameter.Value = user.Password;

                IDbDataParameter phoneParameter = command.CreateParameter();
                phoneParameter.ParameterName = "@Phone";
                phoneParameter.Value = user.Phone;

                IDbDataParameter receiveMassMailParameter = command.CreateParameter();
                receiveMassMailParameter.ParameterName = "@MassMailSubscription";
                receiveMassMailParameter.Value = user.ReceiveMassMail;

                IDbDataParameter secretAnswerParameter = command.CreateParameter();
                secretAnswerParameter.ParameterName = "@Answer";
                secretAnswerParameter.Value = user.SecretAnswer;

                IDbDataParameter secretQuestionParameter = command.CreateParameter();
                secretQuestionParameter.ParameterName = "@Question";
                secretQuestionParameter.Value = user.SecretQuestion;

                IDbDataParameter sexParameter = command.CreateParameter();
                sexParameter.ParameterName = "@Sex";
                sexParameter.Value = user.Sex;

                IDbDataParameter stateParameter = command.CreateParameter();
                stateParameter.ParameterName = "@State";
                stateParameter.Value = user.State;

                IDbDataParameter twitterParameter = command.CreateParameter();
                twitterParameter.ParameterName = "@Twitter";
                twitterParameter.Value = user.Twitter;

                IDbDataParameter zipcodeParameter = command.CreateParameter();
                zipcodeParameter.ParameterName = "@ZipCode";
                zipcodeParameter.Value = user.Zipcode;

                #endregion CreatingParameters

                #region AddingParameters

                command.Parameters.Add(idParameter);
                command.Parameters.Add(activeParameter);
                command.Parameters.Add(addressParameter);
                command.Parameters.Add(birthDateParameter);
                command.Parameters.Add(cityParameter);
                command.Parameters.Add(creationDateParameter);
                command.Parameters.Add(emailParameter);
                command.Parameters.Add(facebookParameter);
                command.Parameters.Add(idCardParameter);
                command.Parameters.Add(instagramParameter);
                command.Parameters.Add(lastNameParameter);
                command.Parameters.Add(mobilePhoneParameter);
                command.Parameters.Add(nameParameter);
                command.Parameters.Add(passwordParameter);
                command.Parameters.Add(phoneParameter);
                command.Parameters.Add(receiveMassMailParameter);
                command.Parameters.Add(secretAnswerParameter);
                command.Parameters.Add(secretQuestionParameter);
                command.Parameters.Add(sexParameter);
                command.Parameters.Add(stateParameter);
                command.Parameters.Add(twitterParameter);
                command.Parameters.Add(zipcodeParameter);

                #endregion AddingParameters

                command.ExecuteNonQuery();
            }
        }

        public override void Update(Domain.UserEntity user)
        {
            user.DateLastUpdate = DateTime.Now;
            using (var command = this.Context.CreateCommand())
            {
                command.CommandText = "Access.UpdateUser";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                #region CreatingParameters

                IDbDataParameter addressParameter = command.CreateParameter();
                addressParameter.ParameterName = "@Address";
                addressParameter.Value = user.Address;

                IDbDataParameter birthDateParameter = command.CreateParameter();
                birthDateParameter.ParameterName = "@Birth";
                birthDateParameter.Value = user.BirthDate;

                IDbDataParameter cityParameter = command.CreateParameter();
                cityParameter.ParameterName = "@City";
                cityParameter.Value = user.City;

                IDbDataParameter dateLastUpdateParameter = command.CreateParameter();
                dateLastUpdateParameter.ParameterName = "@DateLastUpdate";
                dateLastUpdateParameter.Value = user.DateLastUpdate;

                IDbDataParameter emailParameter = command.CreateParameter();
                emailParameter.ParameterName = "@Email";
                emailParameter.Value = user.Email;

                IDbDataParameter facebookParameter = command.CreateParameter();
                facebookParameter.ParameterName = "@Facebook";
                facebookParameter.Value = user.Facebook;

                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.Value = user.Id;

                IDbDataParameter idCardParameter = command.CreateParameter();
                idCardParameter.ParameterName = "@IdCard";
                idCardParameter.Value = user.IdCard;

                IDbDataParameter instagramParameter = command.CreateParameter();
                instagramParameter.ParameterName = "@Instagram";
                instagramParameter.Value = user.Instagram;

                IDbDataParameter lastNameParameter = command.CreateParameter();
                lastNameParameter.ParameterName = "@LastName";
                lastNameParameter.Value = user.LastName;

                IDbDataParameter mobilePhoneParameter = command.CreateParameter();
                mobilePhoneParameter.ParameterName = "@MobilePhone";
                mobilePhoneParameter.Value = user.MobilePhone;

                IDbDataParameter nameParameter = command.CreateParameter();
                nameParameter.ParameterName = "@Name";
                nameParameter.Value = user.Name;

                IDbDataParameter passwordParameter = command.CreateParameter();
                passwordParameter.ParameterName = "@Password";
                passwordParameter.Value = user.Password;

                IDbDataParameter phoneParameter = command.CreateParameter();
                phoneParameter.ParameterName = "@Phone";
                phoneParameter.Value = user.Phone;

                IDbDataParameter receiveMassMailParameter = command.CreateParameter();
                receiveMassMailParameter.ParameterName = "@MassMailSubscription";
                receiveMassMailParameter.Value = user.ReceiveMassMail;

                IDbDataParameter secretAnswerParameter = command.CreateParameter();
                secretAnswerParameter.ParameterName = "@Answer";
                secretAnswerParameter.Value = user.SecretAnswer;

                IDbDataParameter secretQuestionParameter = command.CreateParameter();
                secretQuestionParameter.ParameterName = "@Question";
                secretQuestionParameter.Value = user.SecretQuestion;

                IDbDataParameter sexParameter = command.CreateParameter();
                sexParameter.ParameterName = "@Sex";
                sexParameter.Value = user.Sex;

                IDbDataParameter stateParameter = command.CreateParameter();
                stateParameter.ParameterName = "@State";
                stateParameter.Value = user.State;

                IDbDataParameter twitterParameter = command.CreateParameter();
                twitterParameter.ParameterName = "@Twitter";
                twitterParameter.Value = user.Twitter;

                IDbDataParameter zipcodeParameter = command.CreateParameter();
                zipcodeParameter.ParameterName = "@ZipCode";
                zipcodeParameter.Value = user.Zipcode;

                #endregion CreatingParameters

                #region AddingParameters

                command.Parameters.Add(idParameter);
                command.Parameters.Add(addressParameter);
                command.Parameters.Add(birthDateParameter);
                command.Parameters.Add(cityParameter);
                command.Parameters.Add(dateLastUpdateParameter);
                command.Parameters.Add(emailParameter);
                command.Parameters.Add(facebookParameter);
                command.Parameters.Add(idCardParameter);
                command.Parameters.Add(instagramParameter);
                command.Parameters.Add(lastNameParameter);
                command.Parameters.Add(mobilePhoneParameter);
                command.Parameters.Add(nameParameter);
                command.Parameters.Add(passwordParameter);
                command.Parameters.Add(phoneParameter);
                command.Parameters.Add(receiveMassMailParameter);
                command.Parameters.Add(secretAnswerParameter);
                command.Parameters.Add(secretQuestionParameter);
                command.Parameters.Add(sexParameter);
                command.Parameters.Add(stateParameter);
                command.Parameters.Add(twitterParameter);
                command.Parameters.Add(zipcodeParameter);

                #endregion AddingParameters

                command.ExecuteNonQuery();
            }
        }

        public void UpdatePassword(Guid userId, string Password)
        {
            using (var command = this.Context.CreateCommand())
            {
                command.CommandText = "Access.UpdatePasswordUser";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                #region CreatingParameters

                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.Value = userId;
                IDbDataParameter passwordParameter = command.CreateParameter();
                passwordParameter.ParameterName = "@Password";
                passwordParameter.Value = Password;

                #endregion CreatingParameters

                #region AddingParameters

                command.Parameters.Add(idParameter);
                command.Parameters.Add(passwordParameter);

                #endregion AddingParameters

                command.ExecuteNonQuery();
            }
        }

        public void Activate(Guid userId)
        {
            using (var command = this.Context.CreateCommand())
            {
                command.CommandText = "Access.[ActivateUser]";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                #region CreatingParameters

                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.Value = userId;

                #endregion CreatingParameters

                #region AddingParameters

                command.Parameters.Add(idParameter);

                #endregion AddingParameters

                command.ExecuteNonQuery();
            }
        }

        public override void Delete(Domain.UserEntity user)
        {
            throw new NotImplementedException();
        }

        public Domain.UserEntity Read(Guid id)
        {
            Domain.UserEntity User = new UserEntity();
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [Id]
                              ,[Birth]
                              ,[Address]
                              ,[MassMailSubscription]
                              ,[Creation]
                              ,[Activation]
                              ,[Active]
                              ,[Sex]
                              ,[Email]
                              ,[Password]
                              ,[IdCard]
                              ,[Phone]
                              ,[Name]
                              ,[LastName]
                              ,[DateLastUpdate]
                              ,[DateOut]
                              ,[DateLastVisit]
                              ,[DateCurrentVisit]
                              ,[Question]
                              ,[Answer]
                              ,[MobilePhone]
                              ,[ZipCode]
                              ,[City]
                              ,[State]
                              ,[Twitter]
                              ,[Facebook]
                              ,[Instagram]
                          FROM [Access].[Login]
                          WHERE Id = @Id";

                command.CommandText = sql;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@Id";
                idParameter.Value = id;

                command.Parameters.Add(idParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        User.Id = reader.GetGuid(0);
                        if (reader[1] != null && reader[1].ToString() != string.Empty) User.BirthDate = reader.GetDateTime(1);
                        if (reader[2] != null && reader[2].ToString() != string.Empty) User.Address = reader.GetString(2);
                        User.ReceiveMassMail = reader.GetBoolean(3);
                        User.CreationDate = reader.GetDateTime(4);
                        if (reader[5] != null && reader[5].ToString() != string.Empty) User.ActivationDate = reader.GetDateTime(5);
                        User.Active = reader.GetBoolean(6);
                        if (reader[7] != null && reader[7].ToString() != string.Empty) User.Sex = reader.GetInt32(7);
                        User.Email = reader.GetString(8);
                        User.Password = reader.GetString(9);
                        if (reader[10] != null && reader[10].ToString() != string.Empty) User.IdCard = reader.GetString(10);
                        if (reader[11] != null && reader[11].ToString() != string.Empty) User.Phone = reader.GetString(11);
                        if (reader[12] != null && reader[12].ToString() != string.Empty) User.Name = reader.GetString(12);
                        if (reader[13] != null && reader[13].ToString() != string.Empty) User.LastName = reader.GetString(13);
                        if (reader[14] != null && reader[14].ToString() != string.Empty) User.DateLastUpdate = reader.GetDateTime(14);
                        if (reader[15] != null && reader[15].ToString() != string.Empty) User.DateOut = reader.GetDateTime(15);
                        if (reader[16] != null && reader[16].ToString() != string.Empty) User.DateLastVisit = reader.GetDateTime(16);
                        if (reader[17] != null && reader[17].ToString() != string.Empty) User.DateCurrentVisit = reader.GetDateTime(17);
                        if (reader[18] != null && reader[18].ToString() != string.Empty) User.SecretQuestion = reader.GetString(18);
                        if (reader[19] != null && reader[19].ToString() != string.Empty) User.SecretAnswer = reader.GetString(19);
                        if (reader[20] != null && reader[20].ToString() != string.Empty) User.MobilePhone = reader.GetString(20);
                        if (reader[21] != null && reader[21].ToString() != string.Empty) User.Zipcode = reader.GetInt32(21);
                        if (reader[22] != null && reader[22].ToString() != string.Empty) User.City = reader.GetString(22);
                        if (reader[23] != null && reader[23].ToString() != string.Empty) User.State = reader.GetString(23);
                        if (reader[24] != null && reader[24].ToString() != string.Empty) User.Twitter = reader.GetString(24);
                        if (reader[25] != null && reader[25].ToString() != string.Empty) User.Facebook = reader.GetString(25);
                        if (reader[26] != null && reader[26].ToString() != string.Empty) User.Instagram = reader.GetString(26);
                    }
                }
            }
            return User;
        }

        public Domain.UserEntity Read(string idCard, DateTime BirthDate)
        {
            Domain.UserEntity User = new UserEntity();
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [Id]
                              ,[Birth]
                              ,[Address]
                              ,[MassMailSubscription]
                              ,[Creation]
                              ,[Activation]
                              ,[Active]
                              ,[Sex]
                              ,[Email]
                              ,[Password]
                              ,[IdCard]
                              ,[Phone]
                              ,[Name]
                              ,[LastName]
                              ,[DateLastUpdate]
                              ,[DateOut]
                              ,[DateLastVisit]
                              ,[DateCurrentVisit]
                              ,[Question]
                              ,[Answer]
                              ,[MobilePhone]
                              ,[ZipCode]
                              ,[City]
                              ,[State]
                              ,[Twitter]
                              ,[Facebook]
                              ,[Instagram]
                          FROM [Access].[Login]
                          WHERE IdCard = @IdCard
                          AND Birth = @BirthDate";

                command.CommandText = sql;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@IdCard";
                idParameter.Value = idCard;

                IDbDataParameter idParameter2 = command.CreateParameter();
                idParameter2.ParameterName = "@BirthDate";
                idParameter2.Value = BirthDate;

                command.Parameters.Add(idParameter);
                command.Parameters.Add(idParameter2);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        User.Id = reader.GetGuid(0);
                        if (reader[1] != null && reader[1].ToString() != string.Empty) User.BirthDate = reader.GetDateTime(1);
                        if (reader[2] != null && reader[2].ToString() != string.Empty) User.Address = reader.GetString(2);
                        User.ReceiveMassMail = reader.GetBoolean(3);
                        User.CreationDate = reader.GetDateTime(4);
                        if (reader[5] != null && reader[5].ToString() != string.Empty) User.ActivationDate = reader.GetDateTime(5);
                        User.Active = reader.GetBoolean(6);
                        if (reader[7] != null && reader[7].ToString() != string.Empty) User.Sex = reader.GetInt32(7);
                        User.Email = reader.GetString(8);
                        User.Password = reader.GetString(9);
                        if (reader[10] != null && reader[10].ToString() != string.Empty) User.IdCard = reader.GetString(10);
                        if (reader[11] != null && reader[11].ToString() != string.Empty) User.Phone = reader.GetString(11);
                        if (reader[12] != null && reader[12].ToString() != string.Empty) User.Name = reader.GetString(12);
                        if (reader[13] != null && reader[13].ToString() != string.Empty) User.LastName = reader.GetString(13);
                        if (reader[14] != null && reader[14].ToString() != string.Empty) User.DateLastUpdate = reader.GetDateTime(14);
                        if (reader[15] != null && reader[15].ToString() != string.Empty) User.DateOut = reader.GetDateTime(15);
                        if (reader[16] != null && reader[16].ToString() != string.Empty) User.DateLastVisit = reader.GetDateTime(16);
                        if (reader[17] != null && reader[17].ToString() != string.Empty) User.DateCurrentVisit = reader.GetDateTime(17);
                        if (reader[18] != null && reader[18].ToString() != string.Empty) User.SecretQuestion = reader.GetString(18);
                        if (reader[19] != null && reader[19].ToString() != string.Empty) User.SecretAnswer = reader.GetString(19);
                        if (reader[20] != null && reader[20].ToString() != string.Empty) User.MobilePhone = reader.GetString(20);
                        if (reader[21] != null && reader[21].ToString() != string.Empty) User.Zipcode = reader.GetInt32(21);
                        if (reader[22] != null && reader[22].ToString() != string.Empty) User.City = reader.GetString(22);
                        if (reader[23] != null && reader[23].ToString() != string.Empty) User.State = reader.GetString(23);
                        if (reader[24] != null && reader[24].ToString() != string.Empty) User.Twitter = reader.GetString(24);
                        if (reader[25] != null && reader[25].ToString() != string.Empty) User.Facebook = reader.GetString(25);
                        if (reader[26] != null && reader[26].ToString() != string.Empty) User.Instagram = reader.GetString(26);
                    }
                }
            }
            return User;
        }

        public Domain.UserEntity Read(string email)
        {
            Domain.UserEntity User = new UserEntity();
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [Id]
                              ,[Birth]
                              ,[Address]
                              ,[MassMailSubscription]
                              ,[Creation]
                              ,[Activation]
                              ,[Active]
                              ,[Sex]
                              ,[Email]
                              ,[Password]
                              ,[IdCard]
                              ,[Phone]
                              ,[Name]
                              ,[LastName]
                              ,[DateLastUpdate]
                              ,[DateOut]
                              ,[DateLastVisit]
                              ,[DateCurrentVisit]
                              ,[Question]
                              ,[Answer]
                              ,[MobilePhone]
                              ,[ZipCode]
                              ,[City]
                              ,[State]
                              ,[Twitter]
                              ,[Facebook]
                              ,[Instagram]
                          FROM [Access].[Login]
                          WHERE Email = @Email";

                command.CommandText = sql;
                IDbDataParameter emailParameter = command.CreateParameter();
                emailParameter.ParameterName = "@Email";
                emailParameter.Value = email;

                command.Parameters.Add(emailParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        User.Id = reader.GetGuid(0);
                        if (reader[1] != null && reader[1].ToString() != string.Empty) User.BirthDate = reader.GetDateTime(1);
                        if (reader[2] != null && reader[2].ToString() != string.Empty) User.Address = reader.GetString(2);
                        User.ReceiveMassMail = reader.GetBoolean(3);
                        User.CreationDate = reader.GetDateTime(4);
                        if (reader[5] != null && reader[5].ToString() != string.Empty) User.ActivationDate = reader.GetDateTime(5);
                        User.Active = reader.GetBoolean(6);
                        if (reader[7] != null && reader[7].ToString() != string.Empty) User.Sex = reader.GetInt32(7);
                        User.Email = reader.GetString(8);
                        User.Password = reader.GetString(9);
                        if (reader[10] != null && reader[10].ToString() != string.Empty) User.IdCard = reader.GetString(10);
                        if (reader[11] != null && reader[11].ToString() != string.Empty) User.Phone = reader.GetString(11);
                        if (reader[12] != null && reader[12].ToString() != string.Empty) User.Name = reader.GetString(12);
                        if (reader[13] != null && reader[13].ToString() != string.Empty) User.LastName = reader.GetString(13);
                        if (reader[14] != null && reader[14].ToString() != string.Empty) User.DateLastUpdate = reader.GetDateTime(14);
                        if (reader[15] != null && reader[15].ToString() != string.Empty) User.DateOut = reader.GetDateTime(15);
                        if (reader[16] != null && reader[16].ToString() != string.Empty) User.DateLastVisit = reader.GetDateTime(16);
                        if (reader[17] != null && reader[17].ToString() != string.Empty) User.DateCurrentVisit = reader.GetDateTime(17);
                        if (reader[18] != null && reader[18].ToString() != string.Empty) User.SecretQuestion = reader.GetString(18);
                        if (reader[19] != null && reader[19].ToString() != string.Empty) User.SecretAnswer = reader.GetString(19);
                        if (reader[20] != null && reader[20].ToString() != string.Empty) User.MobilePhone = reader.GetString(20);
                        if (reader[21] != null && reader[21].ToString() != string.Empty) User.Zipcode = reader.GetInt32(21);
                        if (reader[22] != null && reader[22].ToString() != string.Empty) User.City = reader.GetString(22);
                        if (reader[23] != null && reader[23].ToString() != string.Empty) User.State = reader.GetString(23);
                        if (reader[24] != null && reader[24].ToString() != string.Empty) User.Twitter = reader.GetString(24);
                        if (reader[25] != null && reader[25].ToString() != string.Empty) User.Facebook = reader.GetString(25);
                        if (reader[26] != null && reader[26].ToString() != string.Empty) User.Instagram = reader.GetString(26);
                    }
                }
            }
            return User;
        }

        //metodo para leer usuario por cedula
        public Domain.UserEntity Read(string idCard, string option)
        {
            Domain.UserEntity User = new UserEntity();
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [Id]
                              ,[Birth]
                              ,[Address]
                              ,[MassMailSubscription]
                              ,[Creation]
                              ,[Activation]
                              ,[Active]
                              ,[Sex]
                              ,[Email]
                              ,[Password]
                              ,[IdCard]
                              ,[Phone]
                              ,[Name]
                              ,[LastName]
                              ,[DateLastUpdate]
                              ,[DateOut]
                              ,[DateLastVisit]
                              ,[DateCurrentVisit]
                              ,[Question]
                              ,[Answer]
                              ,[MobilePhone]
                              ,[ZipCode]
                              ,[City]
                              ,[State]
                              ,[Twitter]
                              ,[Facebook]
                              ,[Instagram]
                          FROM [Access].[Login]
                          WHERE IdCard = @IdCard";

                command.CommandText = sql;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@IdCard";
                idParameter.Value = idCard;


                command.Parameters.Add(idParameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        User.Id = reader.GetGuid(0);
                        if (reader[1] != null && reader[1].ToString() != string.Empty) User.BirthDate = reader.GetDateTime(1);
                        if (reader[2] != null && reader[2].ToString() != string.Empty) User.Address = reader.GetString(2);
                        User.ReceiveMassMail = reader.GetBoolean(3);
                        User.CreationDate = reader.GetDateTime(4);
                        if (reader[5] != null && reader[5].ToString() != string.Empty) User.ActivationDate = reader.GetDateTime(5);
                        User.Active = reader.GetBoolean(6);
                        if (reader[7] != null && reader[7].ToString() != string.Empty) User.Sex = reader.GetInt32(7);
                        User.Email = reader.GetString(8);
                        User.Password = reader.GetString(9);
                        if (reader[10] != null && reader[10].ToString() != string.Empty) User.IdCard = reader.GetString(10);
                        if (reader[11] != null && reader[11].ToString() != string.Empty) User.Phone = reader.GetString(11);
                        if (reader[12] != null && reader[12].ToString() != string.Empty) User.Name = reader.GetString(12);
                        if (reader[13] != null && reader[13].ToString() != string.Empty) User.LastName = reader.GetString(13);
                        if (reader[14] != null && reader[14].ToString() != string.Empty) User.DateLastUpdate = reader.GetDateTime(14);
                        if (reader[15] != null && reader[15].ToString() != string.Empty) User.DateOut = reader.GetDateTime(15);
                        if (reader[16] != null && reader[16].ToString() != string.Empty) User.DateLastVisit = reader.GetDateTime(16);
                        if (reader[17] != null && reader[17].ToString() != string.Empty) User.DateCurrentVisit = reader.GetDateTime(17);
                        if (reader[18] != null && reader[18].ToString() != string.Empty) User.SecretQuestion = reader.GetString(18);
                        if (reader[19] != null && reader[19].ToString() != string.Empty) User.SecretAnswer = reader.GetString(19);
                        if (reader[20] != null && reader[20].ToString() != string.Empty) User.MobilePhone = reader.GetString(20);
                        if (reader[21] != null && reader[21].ToString() != string.Empty) User.Zipcode = reader.GetInt32(21);
                        if (reader[22] != null && reader[22].ToString() != string.Empty) User.City = reader.GetString(22);
                        if (reader[23] != null && reader[23].ToString() != string.Empty) User.State = reader.GetString(23);
                        if (reader[24] != null && reader[24].ToString() != string.Empty) User.Twitter = reader.GetString(24);
                        if (reader[25] != null && reader[25].ToString() != string.Empty) User.Facebook = reader.GetString(25);
                        if (reader[26] != null && reader[26].ToString() != string.Empty) User.Instagram = reader.GetString(26);
                    }
                }
            }
            return User;
        }

        public string UpdateFromList(string token)
        {
            string str;
            string valor = string.Empty;
            using (IDbCommand command = base.Context.CreateCommand())
            {
                command.CommandText = "RemoveFromMassMail";
                command.CommandType = CommandType.StoredProcedure;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@token";
                idParameter.Value = token;
                IDbDataParameter idParameter2 = command.CreateParameter();
                idParameter2.Direction = ParameterDirection.ReturnValue;
                command.Parameters.Add(idParameter);
                command.Parameters.Add(idParameter2);
                command.ExecuteNonQuery();
                str = idParameter2.Value == null ? String.Empty : idParameter2.Value.ToString().ToString();
            }
            return str;
        }

        protected override void Map(IDataRecord record, UserEntity entity)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}