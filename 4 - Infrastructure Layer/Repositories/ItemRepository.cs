﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    internal class ItemRepository : Repository<ItemEntity>
    {
        #region Constructor

        public ItemRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion Constructor

        #region Methods

        public override void Create(Domain.ItemEntity item)
        {
            throw new NotImplementedException();
        }

        public override void Update(Domain.ItemEntity item)
        {
            throw new NotImplementedException();
        }            

        public override void Delete(Domain.ItemEntity user)
        {
            throw new NotImplementedException();
        }
        public List<ItemEntity> Read()
        {
            var items = new List<ItemEntity>();
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [Id]
                              ,[Type]
                              ,[ImageId]
                              ,[Movie_strID]
                              ,[Movie_HOFilmCode]
                              ,[Date]
                              ,[URL]
                              ,[SectionId]
                              ,[SectionName]
                          FROM [dbo].[Mobile_VItem]";

                command.CommandText = sql;
                                
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Domain.ItemEntity Item = new ItemEntity();
                        Item.Section = new SectionEntity();
                        
                        Item.Id = reader.GetGuid(0);
                        if (reader[1] != null && reader[1].ToString() != string.Empty) Item.Type = reader.GetString(1);
                        if (reader[2] != null && reader[2].ToString() != string.Empty) Item.ImageID = reader.GetString(2);
                        Item.MovieID = reader.GetString(3);
                        Item.MovieHO = reader.GetString(4);
                        if (reader[5] != null && reader[5].ToString() != string.Empty) Item.Date = reader.GetDateTime(5);
                        if (reader[6] != null && reader[6].ToString() != string.Empty) Item.URL = reader.GetString(6);
                        Item.Section.Id = reader.GetGuid(7);
                        Item.Section.Name = reader.GetString(8);
                        items.Add(Item);
                    }
                }
            }
            return items;
        }

        protected override void Map(IDataRecord record, ItemEntity entity)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}