﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class PromotionRepository : IPromotionRepository
    {
        public ResultEntity PublishPromotions(PromotionPackageEntity PromotionList)
        {
            ResultEntity result = new ResultEntity();
            string FilePath = string.Empty;

            try
            {
                //#if DEBUG
                FilePath = (HostingEnvironment.MapPath("~/App_data/Promotions.xml"));
                //#endif

                //#if (!DEBUG)
                //            string cachePath = RoleEnvironment.GetLocalResource("CacheTemporal").RootPath;
                //            FilePath = Path.Combine(cachePath, "Promotions.xml");
                //#endif
                if (PromotionList.ListPromotions.Length > 0)
                {
                    if (!File.Exists(FilePath))
                        File.Create(FilePath).Close();
                    else
                    {
                        File.Delete(FilePath);
                        File.Create(FilePath).Close();
                    }

                    string json = JsonConvert.SerializeObject(PromotionList.ListPromotions);
                    File.WriteAllText(FilePath, json);

                    result.number = 0;
                    result.message = "Publicación realizada satisfactoriamente";
                }
                else
                {
                    result.number = 1;
                    result.message = "No hay promociones activas para publicar";
                }
            }
            catch (Exception ex)
            {
                result.number = -1;
                result.message = ex.InnerException.Message.ToString();
            }
            return result;
        }

        public string GetPromotionsDB()
        {
            string FilePath;
            SqlConnection conexion = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            List<PromotionEntity> ListPromotions = new List<PromotionEntity>();
            string json_serialize1 = string.Empty;
            try
            {
                conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
                cmd = new SqlCommand("SELECT Title, Description, Image, StartDate, ExpirationDate FROM Promotions   where StartDate <= getdate() and  getdate() <= ExpirationDate order by StartDate desc", conexion);
                conexion.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PromotionEntity promotion = new PromotionEntity();
                    promotion.Title = reader[0].ToString();
                    promotion.Description = reader[1].ToString();
                    promotion.Image = reader[2].ToString();
                    promotion.StartDate = reader[3].ToString();
                    promotion.ExpirationDate = reader[4].ToString();
                    ListPromotions.Add(promotion);
                }
                //#if DEBUG
                FilePath = (HostingEnvironment.MapPath("~/App_data/Promotions.xml"));
                //#endif

                //#if (!DEBUG)
                //            string cachePath = RoleEnvironment.GetLocalResource("CacheTemporal").RootPath;
                //            FilePath = Path.Combine(cachePath, "Promotions.xml");
                //#endif
                if (!File.Exists(FilePath))
                    File.Create(FilePath).Close();
                else
                {
                    File.Delete(FilePath);
                    File.Create(FilePath).Close();
                }
                string json = JsonConvert.SerializeObject(ListPromotions);
                File.WriteAllText(FilePath, json);
            }
            catch (Exception ex)
            {
                json_serialize1 = "Error: " + ex.Message;
            }

            if (!(json_serialize1.Contains("Error: ")))
            {
                json_serialize1 = JsonConvert.SerializeObject(ListPromotions);
            }

            return json_serialize1;
        }


        public string GetPromotions()
        {
            string promotions = string.Empty;
            string FilePath = string.Empty;

            try
            {
                //#if DEBUG
                FilePath = (HostingEnvironment.MapPath("~/App_data/Promotions.xml"));
                //#endif

                //#if (!DEBUG)
                //            string cachePath = RoleEnvironment.GetLocalResource("CacheTemporal").RootPath;
                //            FilePath = Path.Combine(cachePath, "Promotions.xml");
                //#endif
                DateTime date = new DateTime(2010, 01, 01);

                if (!File.Exists(FilePath))
                {
                    File.Create(FilePath).Close();
                    promotions = "No_File";
                    //string json_format = "[{\"Id\":0,\"Title\":\"\",\"Description\":\"\",\"Image\":\"\",\"StartDate\":\"" + date + "\",\"ExpirationDate\":\"" + date + "\"}]";
                    //string json_serialize = JsonConvert.SerializeObject(json_format);
                    //promotions = JsonConvert.DeserializeObject<string>(json_serialize);
                }
                else
                {
                    //string json = File.ReadAllText(FilePath);
                    //if (string.IsNullOrEmpty(json) || string.IsNullOrWhiteSpace(json))
                    //{
                    //    string json_format = "[{\"Id\":0,\"Title\":\"\",\"Description\":\"\",\"Image\":\"\",\"StartDate\":\"" + date + "\",\"ExpirationDate\":\"" + date + "\"}]";
                    //    string json_serialize = JsonConvert.SerializeObject(json_format);
                    //    promotions = JsonConvert.DeserializeObject<string>(json_serialize);
                    //}
                    //else
                    //{
                    //    promotions = JsonConvert.DeserializeObject<string>(json);
                    //}
                    promotions = File.ReadAllText(FilePath);
                }
            }
            catch (Exception ex)
            {
                promotions = "Error: " + ex.Message;
            }

            return promotions;
        }
    }
}
