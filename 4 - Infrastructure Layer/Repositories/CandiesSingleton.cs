﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Premium;
using CinesUnidos.ESB.Infrastructure.Agents.Concessions;
using AGPayment = CinesUnidos.ESB.Infrastructure.Agents.Payment;
using QRCoder;
using System.Drawing;
using System.Web.Hosting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public sealed class CandiesSingleton
    {
        private CandiesSingleton()
        {
        }

        private ICache<UserSessionEntity> m_cache;
        private ICache<UserSessionMobileEntity> m_cacheMobile;
        private ITheaterBillboardRepository m_theaterBillboardRepository;
        private ICache<TicketEntity[]> m_cache_ticket;
        //private IConcessionsRepository m_ConcessionsRepo;

        public enum QRCode
        {
            Alphanumeric, Concat
        }

        //private PaymentResponseEntity RespuestaMerchant;
        public static CandiesSingleton Instance { get { return Nested.instance; } }

        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }
            internal static readonly CandiesSingleton instance = new CandiesSingleton();
        }

        /// <summary>
        /// RM-
        /// metodo para validar si hay suficiente carameleria en la web para el pedido que se quiere hacer
        /// </summary>
        /// <returns></returns>
        public bool ValidarCarameleriaEnLaWeb(UserSessionEntity session)
        {
            bool valido = true;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            try
            {

                if (session.Concessions.Count() > 0)
                {
                    SqlCommand command = new SqlCommand("");
                    command.Connection = conn;
                    conn.Open();

                    foreach (var item in session.Concessions)
                    {
                        command.CommandText = string.Format("SELECT Item_decStockAvailable FROM tblCandiesWeb WHERE Cinema_strCode = '{0}' AND Item_strItemId = '{1}' AND Item_decStockAvailable >= {2}", session.TheaterId, item.ItemStrItemID, (int)item.ItemQuantityPurchase);
                        using (var reader = command.ExecuteReader())
                        {
                            //si no trae filas es xq no tengo ese item o esa cantidad. retorno de inmediato para ahorrar tiempo de validacion.
                            if (!reader.HasRows)
                            {
                                valido = false;
                                return valido;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                valido = false;
                return valido;
            }
            conn.Close();
            return valido;
        }

        /// <summary>
        /// RM-
        /// metodo para descontar en la nube los items de carameleria que ya fueron pagados.
        /// </summary>
        /// <returns></returns>
        public bool DiscountCandiesWEb(UserSessionEntity session)
        {
            bool Descuento = true;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            SqlCommand command;
            try
            {
                if (session.Concessions.Count() > 0)
                {
                    conn.Open();
                    foreach (var item in session.Concessions)
                    {
                        string csql = string.Format("UPDATE tblCandiesWeb SET Item_decStockAvailable = Item_decStockAvailable - {0} WHERE Cinema_strCode = '{1}' and Item_strItemId = '{2}'", item.ItemQuantityPurchase, session.TheaterId, item.ItemStrItemID);
                        command = new SqlCommand(csql, conn);
                        command.ExecuteNonQuery();
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                Descuento = false;
                return Descuento;
            }
            return Descuento;
        }

        private string SaveImageToCloudStorage(string fileName, string filePath)
        {
            string BlobPath = string.Empty;

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("qrcodes");

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

            using (var fileStream = File.OpenRead(filePath))
            {
                blockBlob.UploadFromStream(fileStream);
            }

            //Borrar qr alojado temporalmente en App_Data (Local) ó Cloud (Resources)
            File.Delete(filePath);

            //System.IO.File.Delete(FileStoragePath);

            return BlobPath = fileName;

            //return BlobPath;
        }

        //valor referencial..
        public ConfigurationEntity Configuration { get; set; }

        private string GenerateQRCoder(UserSessionEntity user, QRCode Code)
        {
            string FileStoragePath = string.Empty;
            //Generar listado de cines con lector QR
            //List<string> CinemaQRList = System.Configuration.ConfigurationManager.AppSettings["CinemaQR"].Split(';').ToList();
            //Validar que el cine se encuentre o no en la lista
            //var value = CinemaQRList.Where(c => c.Contains(user.TheaterId)).SingleOrDefault();
            //if (!string.IsNullOrEmpty(value))
            //{
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData;
            string fileName = string.Empty;

            qrCodeData = qrGenerator.CreateQrCode(user.BookingId, QRCodeGenerator.ECCLevel.Q);
            //FileName = CodCine-Localizador Alfanumerico-TokenUsuario.jpeg
            fileName = user.TheaterId + "-" + user.BookingId.ToString() + "-" + user.Id.ToString().Replace("-", "") + ".jpeg";

            QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            Bitmap bImage = new Bitmap(qrCodeImage, new Size(240, 240));
            string filePath = String.Empty;

            //Crear ruta local o storage para guardar el codigo qr
//#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_Data/" + fileName));
//#endif

//#if (!DEBUG)
//                string QRPath = RoleEnvironment.GetLocalResource("QRCode").RootPath;
//                filePath = Path.Combine(QRPath, fileName);
//#endif
            bImage.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            //Save Image to Storage as blob into container "qrcodes"                
            FileStoragePath = SaveImageToCloudStorage(fileName, filePath);

            //}
            //else
            //{
            //    FileStoragePath = "";
            //}

            return FileStoragePath;
        }

        public Boolean CancelOrder(string UserSessionID)
        {
            try
            {
                string clientId = Configuration.Values["ClientID"];
                using (PremiumClient client = new PremiumClient())
                {
                    return client.CancelOrderRest(UserSessionID.ToString().Replace("-", ""));
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// RM - Metodo para cobrar desde el singleton, en sintesis, es el mimso metodo de pago pero esta vez se hara a traves de esta clase
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public PaymentResponseEntity MerchantPayment(PaymentEntity payment, UserSessionEntity session, string clientId, ICache<UserSessionEntity> m_cache)
        {
            lock (Instance)
            {
                PaymentResponseEntity responce = new PaymentResponseEntity();
                //UserSessionEntity session = m_cache.Get(payment.UserSessionId.ToString());

                if (session.Id.ToString() != null)
                {
                    string userSessionId = payment.UserSessionId.ToString().Replace("-", "");
                    ConfirmationResponseEntity confirmation = (ConfirmationResponseEntity)session;
                    //string clientId = Configuration.Values["ClientID"];
                    AGPayment.PaymentResponse result = new AGPayment.PaymentResponse();
                    if (ValidarCarameleriaEnLaWeb(session))
                    {
                        using (AGPayment.PaymentClient client = new AGPayment.PaymentClient())
                        {
                            result = client.GetPaymentResponseByPaymentType(clientId, userSessionId, (AGPayment.PaymentInfo)payment);
                            client.Close();
                        }

                        responce = (PaymentResponseEntity)result;
                        responce.Confirmation = confirmation;
                        responce.Name = payment.Name;
                        responce.TransactionNumber = result.TransactionNumber;
                        //Actualizar UserSession y guardar información de pago al usuario
                        #region Update UserSession
                        //Localizador Alfanúmerico
                        session.BookingId = responce.BookingId;
                        session.BookingNumber = responce.BookingNumber;
                        session.CodeResult = responce.CodeResult;
                        session.DescriptionResult = responce.DescriptionResult;
                        session.Voucher = responce.Voucher;
                        session.Name = payment.Name;
                        session.Email = payment.Email;

                        ////Para uso de carameleria
                        //session.TransIdTemp = result.TransactionNumber.ToString();
                        //Si la transacción es satisfactoria CodeResult = 00, generar el código QR

                        if (session.CodeResult.Equals("00") && session.Voucher != null)
                        {
                            DiscountCandiesWEb(session);
                            session.BookingByte = GenerateQRCoder(session, QRCode.Alphanumeric);
                            PurchasesEntity purchase = new PurchasesEntity();
                            purchase.Amount = session.Total;
                            purchase.CinemaTitle = session.Theater;
                            purchase.FilmTitle = session.Title;
                            purchase.CinemaCode = session.TheaterId;
                            purchase.Tickets = session.Seats.Count;
                            purchase.Date = DateTime.Now;
                            purchase.UserTypeId = payment.CertificateType;
                            purchase.UserId = payment.Certificate;
                            //purchase.FilmCode = session.CodeResult;
                            purchase.FilmCode = session.HO;
                            //LUIS RAMIREZ 12/07/2017, Agregar campos BookingNumber ó BookingId (Dependiendo del cine y SessionId
                            //string TheaterId = System.Configuration.ConfigurationManager.AppSettings["CinemaQR"].ToString();

                            //if (TheaterId.Contains(session.TheaterId))
                            //{
                            purchase.BookingNumber = session.BookingId;
                            //}
                            //else
                            //{
                            //    purchase.BookingNumber = session.BookingNumber.ToString();
                            //}
                            purchase.SessionId = session.SessionId;

                            PurchasesRepository p = new PurchasesRepository();
                            string res = p.RegisterComplete(purchase, session);

                            m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                        }
                        else
                        {
                            CancelOrder(session.Id.ToString());
                        }
                        //Guardar en redis el resultado de la compra
                        m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                        #endregion Update UserSession
                    }
                    else
                    {
                        //Cuando es -1 significa que no se encontró la llave del redis del usuario
                        responce.CodeResult = "555";
                        responce.DescriptionResult = "No hay disponibilidad para la cantidad de producto(s) seleccionado(s).";
                        session.CodeResult = "555";
                        session.DescriptionResult = "No hay disponibilidad para la cantidad de producto(s) seleccionado(s).";
                        m_cache.Set(session.Id.ToString(), session, TimeSpan.FromMinutes(15));
                        return responce;
                    }
                }
                else
                {
                    responce.CodeResult = "9999";
                }
                return responce;
            }
        }
    }
}