﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Web.Hosting;
using System.IO;
using Microsoft.WindowsAzure.ServiceRuntime;
using CinesUnidos.ESB.Infrastructure.Agents.Live;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class TicketPriceRepository : ITicketPriceRepository
    {
        #region Attributos

        private IConnectionFactory m_factory;

        private ICache<Domain.CityTicketPriceEntity[]> m_cache;

        private ICache<string> m_cache_str;


        public ConfigurationEntity Configuration { get; set; }

        #endregion Attributos

        #region Methods

        public TicketPriceRepository(ICache<Domain.CityTicketPriceEntity[]> cache, ICache<string> cache_str)
        {
            m_cache = cache;
            m_cache_str = cache_str;
        }

        public TicketPriceRepository()
        {
            m_factory = new ConnectionFactory("ELMAH");
        }

        public bool CompararHoras(string filename, double hours)
        {
            ////  var fecha = DateTime.Now.AddHours(-hours);
            ////return System.IO.File.GetLastWriteTime(filename).AddHours(-hours) <= fecha;
            bool value;
            DateTime DateSystem = DateTime.Now;
            DateTime DateFile = System.IO.File.GetLastWriteTime(filename);

            TimeSpan result = DateSystem - DateFile;

            if (result.TotalHours >= hours) value = true; else value = false;
            return value;
        }

        public CityTicketPriceEntity[] GetTicketPrice(string date)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("SA Western Standard Time");
            DateTime dateTimeNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            CityTicketPriceEntity[] cityTicketPrice = null;
            CityTicketPriceEntity[] TicketPrice = null;

            string[] filePath = new string[2]; // 0 -> DateTicketPrice  1-> CityTicketPrice
            FileInfo[] file = new FileInfo[2]; // 0 -> DateTicketPrice  1-> CityTicketPrice
            Boolean[] valido = new Boolean[2]; // 0 -> DateTicketPrice  1-> CityTicketPrice

            string dateCache_str = "";
            bool value = false;

            // Generar ruta de DateTicketPrice y CityTicketPrice

            // DateTicketPrice
            
            //#if DEBUG
            filePath[0] = (HostingEnvironment.MapPath("~/App_Data/DateTicketPrice.xml"));
            //#endif

            //#if (!DEBUG)
            //                filePath[0] = Path.Combine(RoleEnvironment.GetLocalResource("DateTicketPrice").RootPath, "DateTicketPrice.xml");
            //#endif

            // CityTicketPrice

            //#if DEBUG
            filePath[1] = (HostingEnvironment.MapPath("~/App_Data/CityTicketPrice.xml"));
            //#endif

            //#if (!DEBUG)                
            //            filePath[1] = Path.Combine(RoleEnvironment.GetLocalResource("CityTicketPrice").RootPath, "CityTicketPrice.xml");
            //#endif

            //Comprobar si existe el archivo DateTicketPrice y CityTicketPrice
            if (!File.Exists(filePath[0]))
                File.Create(filePath[0]).Close();

            if (!File.Exists(filePath[1]))
                File.Create(filePath[1]).Close();

            file[0] = new FileInfo(filePath[0]);
            file[1] = new FileInfo(filePath[1]);

            //Comprobar fechas de los archivos
            valido[0] = CompararHoras(filePath[0], 168);
            valido[1] = CompararHoras(filePath[1], 168);


            //Check DateTicketPrice
            if (valido[0] || file[0].Length > 20)
                dateCache_str = JsonConvert.DeserializeObject<string>(File.ReadAllText(filePath[0]));
            else
            {
                dateCache_str = Convert.ToString(dateTimeNow);
                value = true;
            }

            //Check TicketPrice File
            if (valido[1] || file[1].Length > 20)
                TicketPrice = JsonConvert.DeserializeObject<CityTicketPriceEntity[]>(File.ReadAllText(filePath[1]));
            else
            {
                dateCache_str = Convert.ToString(dateTimeNow);
                value = true;
            }
            DateTime dateCache = Convert.ToDateTime(dateCache_str);

            if (!value)
            {
                if (dateTimeNow.DayOfWeek.ToString().Equals("Friday"))
                {
                    if ((dateTimeNow.TimeOfDay.Hours >= 8) && (dateCache.Date != dateTimeNow.Date))
                    {
                        dateCache_str = Convert.ToString(dateTimeNow);
                        value = true;
                    }
                    else
                        cityTicketPrice = TicketPrice;
                }
                else
                    cityTicketPrice = TicketPrice;
            }

            if (value == true)
            {
                string json = "";
                //CityTicketPrice
                cityTicketPrice = Execute(date);
                json = JsonConvert.SerializeObject(cityTicketPrice);
                File.WriteAllText(filePath[1], json);
                json = ""; //Clean json var

                //DateTicketPrice
                json = JsonConvert.SerializeObject(dateCache_str);
                File.WriteAllText(filePath[0], json);
            }

            return cityTicketPrice;
        }

        #region MetodoAnteriorConRedis
        //public CityTicketPriceEntity[] GetTicketPrice(string date)
        //{
        //    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("SA Western Standard Time");
        //    DateTime dateTimeNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

        //    CityTicketPriceEntity[] cityTicketPrice = null;
        //    bool value = false; //LRAMIREZ, 28/11/2016. De acuerdo al valor, permite llenar el key del CityTicketPrice, haciendo referencia al último condicional
        //    //en el método
        //    string dateCache_str;
        //    CityTicketPriceEntity[] TicketPrice;

        //    dateCache_str = m_cache_str.Get("DateTicketPrice");
        //    TicketPrice = m_cache.Get("CityTicketPrice");

        //    if (dateCache_str == (null))
        //    {
        //        m_cache_str.Set("DateTicketPrice", Convert.ToString(dateTimeNow), TimeSpan.FromHours(168));
        //        m_cache.Delete("CityTicketPrice");
        //        value = true;
        //    }
        //    else
        //    {
        //        DateTime dateCache = Convert.ToDateTime(dateCache_str);
        //        if (dateTimeNow.DayOfWeek.ToString().Equals("Friday"))
        //        {
        //            if ((dateCache.DayOfWeek.Equals(dateTimeNow.DayOfWeek)) &&
        //                (dateCache.Day.Equals(dateTimeNow.Day)) &&
        //                (dateCache.Month.Equals(dateTimeNow.Month)) &&
        //                (dateCache.Year.Equals(dateTimeNow.Year)) &&
        //                (dateCache.Hour >= 8))
        //            {
        //                if (TicketPrice != null)
        //                {
        //                    cityTicketPrice = m_cache.Get("CityTicketPrice");
        //                }
        //                else
        //                {
        //                    value = true;
        //                }
        //            }
        //            else
        //            {
        //                if (dateTimeNow.Hour > 8)
        //                {
        //                    m_cache_str.Set("DateTicketPrice", Convert.ToString(dateTimeNow), TimeSpan.FromHours(168));
        //                    value = true;
        //                }
        //                else
        //                {
        //                    value = true;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (TicketPrice != null)
        //            {
        //                cityTicketPrice = m_cache.Get("CityTicketPrice");
        //            }
        //            else
        //            {
        //                value = true;
        //            }
        //        }
        //    }
        //    if (value == true)
        //    {
        //        cityTicketPrice = Execute(date);
        //        m_cache.Set("CityTicketPrice", cityTicketPrice, TimeSpan.FromHours(168));
        //    }
        //    return cityTicketPrice;
        //}
        #endregion

        private CityTicketPriceEntity[] Execute(string date)
        {
            CityTicketPrice[] result;

            using (DataClient client = new DataClient())
            {
                result = client.GetTicketPrice(date);
                client.Close();
            }

            List<CityTicketPriceEntity> listCityTicketPrice = new List<CityTicketPriceEntity>();
            foreach (var item in result)
            {
                CityTicketPriceEntity cityTicketPrice = new CityTicketPriceEntity { };

                cityTicketPrice.city = item.city;


                List<TheaterTicketPriceEntity> listTheaterTicketPriceEntity = new List<TheaterTicketPriceEntity>();
                foreach (var aitem in item.theaterTicketPrice)
                {
                    TheaterTicketPriceEntity theaterTicketPriceEntity = new TheaterTicketPriceEntity();

                    theaterTicketPriceEntity.theaterID = aitem.theaterID;

                    List<TicketPriceEntity> listTicketPriceEntity = new List<TicketPriceEntity>();
                    foreach (var bitem in aitem.listTicketPrice.OrderBy(o => o.typeHall))
                    {
                        cityTicketPrice.date = bitem.date;
                        theaterTicketPriceEntity.name = bitem.theaterName;
                        TicketPriceEntity ticketPriceEntity = new TicketPriceEntity();
                        ticketPriceEntity.formatMovie = bitem.formatMovie;
                        ticketPriceEntity.price = bitem.price;
                        ticketPriceEntity.ticketName = bitem.ticketName;
                        ticketPriceEntity.typeHall = bitem.typeHall;
                        ticketPriceEntity.fee = bitem.fee;
                        listTicketPriceEntity.Add(ticketPriceEntity);

                    }
                    theaterTicketPriceEntity.listTicketPrice = (listTicketPriceEntity);
                    listTheaterTicketPriceEntity.Add(theaterTicketPriceEntity);


                }
                cityTicketPrice.theaterTicketPrice = (listTheaterTicketPriceEntity);
                listCityTicketPrice.Add(cityTicketPrice);
            }
            return listCityTicketPrice.ToArray();
        }

        public Domain.InfoVoucher GetInfoVoucher(string cinemaid, string sessionid, string voucher)
        {
            LiveClient live = new LiveClient();
            var info = live.GetTicketTypesforBarcode(cinemaid, sessionid, voucher);
            Domain.InfoVoucher i = new Domain.InfoVoucher
            {
                CodigoBoletoParaCanje = info.CodigoBoletoParaCanje,
                Redimido = info.Redimido,
                TextoMostrar = info.TextoMostrar,
                ValidoParafuncion = info.ValidoParafuncion,
                ValorRedencion = info.ValorRedencion,
                VoucherCode = info.VoucherCode,
                IsMultiVoucher = info.IsMultiVoucher,
                EntradasRestantes = info.EntradasRestantes
            };
            return i;
        }
    }
    #endregion Methods
}