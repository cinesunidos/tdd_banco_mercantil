﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using System;
using System.Data;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class AdminRepository : IAdminRepository//Repository<UserEntity>
    {
        #region Constructor


        #endregion Constructor

        #region Methods
                
        public UserEntity Login(UserEntity usr)
        {
            try {
                ConnectionFactory m_factory = new ConnectionFactory("ELMAH");
                using (var uow = new UnitOfWork(m_factory))
                {
                    AdminRepositoryDB db = new AdminRepositoryDB(uow);
                    UserEntity usr2 = db.Read(usr.Email, usr.Password);
                    return usr2;
                }
            }
            catch(Exception ex)
            {
                //responseEntity.Dictionary.Add("ee", ex.Message);
                UserEntity usr2 = new UserEntity();
                usr2.Name = ex.Message;
                //responseEntity.isOk = true;
                return usr2;
            }
           
        }
        public ConfigurationEntity Configuration { get; set; }

        #endregion Methods
    }

    public class AdminRepositoryDB : Repository<UserEntity>
    {
        #region Constructor

        public AdminRepositoryDB(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion Constructor

        #region Methods

        public override void Create(Domain.UserEntity user)
        {
            throw new NotImplementedException();
        }

        public override void Update(Domain.UserEntity user)
        {
            throw new NotImplementedException();
        }

        public void UpdatePassword(Guid userId, string Password)
        {
            throw new NotImplementedException();
        }

        public void Activate(Guid userId)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Domain.UserEntity user)
        {
            throw new NotImplementedException();
        }

        public Domain.UserEntity Read(string user, string passw)
        {
            Domain.UserEntity User = new UserEntity();
            using (var command = this.Context.CreateCommand())
            {
                string sql = @"SELECT [User_intID]                              
                              ,[User_strName]
                              ,[User_strLastName]
                          FROM [dbo].[tblAdminUsers]
                          WHERE [User_strEmail] = @user
                          AND [User_strPassword] = @passw";

                command.CommandText = sql;
                IDbDataParameter idParameter = command.CreateParameter();
                idParameter.ParameterName = "@user";
                idParameter.Value = user;

                IDbDataParameter idParameter2 = command.CreateParameter();
                idParameter2.ParameterName = "@passw";
                idParameter2.Value = passw;

                command.Parameters.Add(idParameter);
                command.Parameters.Add(idParameter2);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        User.IdCard = reader["User_intID"].ToString();
                        User.Name = reader["User_strName"].ToString();
                        User.LastName = reader["User_strLastName"].ToString();
                    }
                }
            }
            return User;
        }

        protected override void Map(IDataRecord record, UserEntity entity)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}