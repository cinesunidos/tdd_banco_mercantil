﻿using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.DataAccess;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class PurchasesRepository : IPurchasesRepository
    {
        #region Constructor


        #endregion Constructor

        #region Methods

        public PurchasesEntity[] GetAll(string usr)
        {
            try
            {
                ConnectionFactory m_factory = new ConnectionFactory("ELMAH");
                using (var uow = new UnitOfWork(m_factory))
                {
                    PurchasesRepositoryDB db = new PurchasesRepositoryDB(uow);
                    PurchasesEntity[] usr2 = db.Read(usr);
                    return usr2;
                }
            }
            catch (Exception ex)
            {
                Domain.PurchasesEntity[] parr = new PurchasesEntity[1];
                PurchasesEntity p = new PurchasesEntity();
                p.CinemaCode = ex.Message;
                parr[0] = p;
                return parr;
            }
        }

        public string Register(PurchasesEntity data)
        {
            try
            {
                ConnectionFactory m_factory = new ConnectionFactory("ELMAH");
                using (var uow = new UnitOfWork(m_factory))
                {
                    PurchasesRepositoryDB db = new PurchasesRepositoryDB(uow);
                    db.Create(data);
                    uow.SaveChanges();
                    return "1";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string RegisterComplete(PurchasesEntity ent, UserSessionEntity UserSession)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString()))
                {
                    string consulta = @"insert into tblPurchases ([Film_strCode]
                  ,[Film_strTitle]
                  ,[Cinema_strID]
                  ,[Cinema_strName]
                  ,[Purchases_dtmDate]
                  ,[Purchases_intTicket]
                  ,[Purchases_decAmount], User_strIDCardType, User_strID
                  ,[SessionId]
                  ,[BookingNumber]
                  , [SessionOBJ])
                    values ('" + ent.FilmCode + "', '" + ent.FilmTitle + "', '" + ent.CinemaCode + "', '" + ent.CinemaTitle + "', '" + ent.Date.ToString("yyyy-MM-dd") + "', '" + ent.Tickets + "', " + ent.Amount.ToString().Replace(',', '.') + ",'" + ent.UserTypeId + "', '" + ent.UserId + "', '" + ent.SessionId + "', '" + ent.BookingNumber + "', '" + JsonConvert.SerializeObject(UserSession).Replace("'",".") + "')";
                    SqlCommand cmd = new SqlCommand(consulta, con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "1";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public ConfigurationEntity Configuration { get; set; }

        #endregion Methods
    }

    public class PurchasesRepositoryDB : Repository<PurchasesEntity>
    {
        #region Constructor

        public PurchasesRepositoryDB(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion Constructor

        #region Methods

        public override void Create(Domain.PurchasesEntity user)
        {

            try
            {
                using (var command = this.Context.CreateCommand())
                {
                    string sql = @"insert into tblPurchases ([Film_strCode]
                  ,[Film_strTitle]
                  ,[Cinema_strID]
                  ,[Cinema_strName]
                  ,[Purchases_dtmDate]
                  ,[Purchases_intTicket]
                  ,[Purchases_decAmount], User_strIDCardType, User_strID
                  ,[SessionId]
                  ,[BookingNumber])
                    values ('" + user.FilmCode + "', '" + user.FilmTitle + "', '" + user.CinemaCode + "', '" + user.CinemaTitle + "', '" + user.Date.ToString("yyyy-MM-dd") + "', '" + user.Tickets + "', " + user.Amount.ToString().Replace(',', '.') + ",'" + user.UserTypeId + "', '" + user.UserId + "', '" + user.SessionId + "', '" + user.BookingNumber + "')";

                    /*string sql = @"insert into tblPurchases ([Film_strCode]
                  ,[Film_strTitle]
                  ,[Cinema_strID]
                  ,[Cinema_strName]
                  ,[Purchases_dtmDate]
                  ,[Purchases_intTicket]
                  ,[Purchases_decAmount], User_strIDCardType, User_strID)
                    values (@strCode, @strTitle, @strID, @strName, @dtmDate, @intTicket, @amount, @usr, @usr2)
                   ";

                    IDbDataParameter codeParameter = command.CreateParameter();
                    codeParameter.ParameterName = "@strCode";
                    codeParameter.Value = user.FilmCode;
                    command.Parameters.Add(codeParameter);
                    IDbDataParameter titleParameter = command.CreateParameter();
                    titleParameter.ParameterName = "@strTitle";
                    titleParameter.Value = user.FilmTitle;
                    command.Parameters.Add(titleParameter);
                    IDbDataParameter idcParameter = command.CreateParameter();
                    idcParameter.ParameterName = "@strID";
                    idcParameter.Value = user.CinemaCode;
                    command.Parameters.Add(idcParameter);
                    IDbDataParameter nameParameter = command.CreateParameter();
                    nameParameter.ParameterName = "@strName";
                    nameParameter.Value = user.CinemaTitle;
                    command.Parameters.Add(nameParameter);
                    IDbDataParameter dateParameter = command.CreateParameter();
                    dateParameter.ParameterName = "@dtmDate";
                    dateParameter.Value = user.Date;
                    command.Parameters.Add(dateParameter);
                    IDbDataParameter tParameter = command.CreateParameter();
                    tParameter.ParameterName = "@intTicket";
                    tParameter.Value = user.Tickets;
                    command.Parameters.Add(tParameter);
                    IDbDataParameter amtParameter = command.CreateParameter();
                    amtParameter.ParameterName = " @amount";
                    amtParameter.Value = user.Amount;
                    command.Parameters.Add(amtParameter);
                    IDbDataParameter tidParameter = command.CreateParameter();
                    tidParameter.ParameterName = "@usr";
                    tidParameter.Value = user.UserTypeId;
                    command.Parameters.Add(tidParameter);
                    IDbDataParameter idParameter = command.CreateParameter();
                    idParameter.ParameterName = "@usr2";
                    idParameter.Value = user.UserId;
                    command.Parameters.Add(idParameter);*/
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Update(Domain.PurchasesEntity user)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Domain.PurchasesEntity user)
        {
            throw new NotImplementedException();
        }

        public Domain.PurchasesEntity[] Read(string user)
        {
            Domain.PurchasesEntity[] parr = new PurchasesEntity[10];
            String tid = user.Substring(0, 1);
            String id = user.Substring(1);
            try
            {
                using (var command = this.Context.CreateCommand())
                {
                    string sql = @"SELECT top 10 [Film_strCode]
                  ,[Film_strTitle]
                  ,[Cinema_strID]
                  ,[Cinema_strName]
                  ,[Purchases_dtmDate]
                  ,[Purchases_intTicket]
                  ,[Purchases_decAmount]
                  ,[BookingNumber]
                  ,[SessionId]
                  ,[SessionOBJ]
              FROM [dbo].[tblPurchases] where
                         [User_strIDCardType] = @usr AND [User_strID] = @usr2
               ORDER BY [Purchases_dtmDate] DESC";

                    command.CommandText = sql;
                    IDbDataParameter tidParameter = command.CreateParameter();
                    tidParameter.ParameterName = "@usr";
                    tidParameter.Value = tid;
                    command.Parameters.Add(tidParameter);
                    IDbDataParameter idParameter = command.CreateParameter();
                    idParameter.ParameterName = "@usr2";
                    idParameter.Value = id;
                    command.Parameters.Add(idParameter);

                    using (var reader = command.ExecuteReader())
                    {
                        int i = 0;
                        while (reader.Read())
                        {
                            PurchasesEntity p = new PurchasesEntity();
                            p.FilmCode = reader["Film_strCode"].ToString();
                            p.FilmTitle = reader["Film_strTitle"].ToString();
                            p.CinemaCode = reader["Cinema_strID"].ToString();
                            p.CinemaTitle = reader["Cinema_strName"].ToString();
                            p.Date = (DateTime)reader["Purchases_dtmDate"];
                            p.Tickets = (int)reader["Purchases_intTicket"];
                            p.Amount = (decimal)reader["Purchases_decAmount"];
                            p.BookingNumber = reader["BookingNumber"].ToString();
                            p.SessionId = (int)reader["SessionId"];
                            p.SessionOBJ = reader["SessionOBJ"].ToString();
                            parr[i] = p;
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PurchasesEntity p = new PurchasesEntity();
                p.CinemaTitle = ex.Message;
                parr[0] = p;
            }
            return parr;
        }

        protected override void Map(IDataRecord record, PurchasesEntity entity)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}