﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class PremiereRepository : IPremiereRepository
    {
        #region Attributos

        private ICache<PremierEntity[]> m_cache;

        #endregion Attributos

        #region Methods

        public PremiereRepository(ICache<PremierEntity[]> cache)
        {
            m_cache = cache;
        }

        //TODO: Refactorizar el llamado en el foreach del coming soon que lo puede hacer de manera infinita
        private PremierEntity[] Execute()
        {

            List<PremierEntity> premieres = new List<PremierEntity>();

            using (DataClient client = new DataClient())
            {
                foreach (Movie item in client.GetComingSoon().CoomingSoon)
                {
                    premieres.Add((PremierEntity)item);
                }
                client.Close();
            }
            return premieres.OrderBy(p => p.Date).ToArray();
        }

        public bool CompararHoras(string filename, double hours)
        {
            ////  var fecha = DateTime.Now.AddHours(-hours);
            ////return System.IO.File.GetLastWriteTime(filename).AddHours(-hours) <= fecha;

            bool value;

            DateTime DateSystem = DateTime.Now;
            DateTime DateFile = System.IO.File.GetLastWriteTime(filename);

            TimeSpan result = DateSystem - DateFile;

            if (result.TotalHours >= hours) value = true; else value = false;

            return value;
        }

        private PremierEntity[] Load()
        {
            PremierEntity[] premiers;
            string filePath = "";
            ///
            /// Para ser usado en ambiente local
            ///

            //Boolean valido = CompararHoras(HostingEnvironment.MapPath(@"~/App_Data/Premiers.xml"), 4);
            //string premiersFile = File.ReadAllText(HostingEnvironment.MapPath("~/App_Data/Premiers.xml"));

            //if (System.Configuration.ConfigurationManager.AppSettings["DirectoryKeys"].ToString().Equals("Cloud"))
            //{
            //    string cachePath = RoleEnvironment.GetLocalResource("Premiers").RootPath;
            //    filePath = Path.Combine(cachePath, "Premiers.xml");
            //}
            //else
            //{
            //    filePath = (HostingEnvironment.MapPath("~/App_data/Premiers.xml"));
            //}

            //#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_data/Premiers.xml"));
            //#endif

            //#if (!DEBUG)
            //            string cachePath = RoleEnvironment.GetLocalResource("Premiers").RootPath;
            //            filePath = Path.Combine(cachePath, "Premiers.xml");
            //#endif

            if (!File.Exists(filePath))
                File.Create(filePath).Close();

            Boolean invalido = CompararHoras(filePath, 24);
            FileInfo file = new FileInfo(filePath);

            if (invalido || file.Length == 0)
            {

                premiers = Execute();
                string json = JsonConvert.SerializeObject(premiers);
                File.WriteAllText(filePath, json);
            }
            else
            {
                premiers = JsonConvert.DeserializeObject<PremierEntity[]>(File.ReadAllText(filePath));
            }

            //premiers = m_cache.Get("Premiers");
            //if (premiers == null || premiers.Count() == 0)
            //{
            //    premiers = Execute();
            //    m_cache.Set("Premiers", premiers, TimeSpan.FromHours(24));
            //}
            //m_cache.Close();
            return premiers;
        }

        public void Clear()
        {
            m_cache.Delete("Premiers");
            m_cache.Close();
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        public PremierEntity[] Data
        {
            get
            {
                PremierEntity[] premiers = Load();
                return premiers;
            }
        }

        #endregion Properties
    }
}