﻿using CinesUnidos.ESB.Cache;
using CinesUnidos.ESB.Domain;
using CinesUnidos.ESB.Domain.Contracts.RepositoriesContracts;
using CinesUnidos.ESB.Infrastructure.Agents.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Web.Hosting;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CinesUnidos.ESB.Infrastructure.Repositories
{
    public class TheaterRepository : ITheaterRepository
    {
        #region Attributos

        private ICache<TheaterEntity[]> m_cache;

        #endregion Attributos

        #region Methods

        public TheaterRepository(ICache<TheaterEntity[]> cache)
        {
            m_cache = cache;
        }

        private TheaterEntity[] Execute()
        {
            string CompanyID = System.Configuration.ConfigurationManager.AppSettings["CompanyID"].ToString();
            Theater[] result = null;
            using (DataClient client = new DataClient())
            {
                client.Open();
                result = client.GetTheaters(CompanyID);
                client.Close();
            }

            List<TheaterEntity> theaters = new List<TheaterEntity>();
            foreach (var item in result)
            {
                TheaterEntity theater = (TheaterEntity)item;
                theaters.Add(theater);
            }
            return theaters.ToArray();
        }

        public bool CompararHoras(string filename, double hours)
        {
            ////  var fecha = DateTime.Now.AddHours(-hours);
            ////return System.IO.File.GetLastWriteTime(filename).AddHours(-hours) <= fecha;

            bool value;

            DateTime DateSystem = DateTime.Now;
            DateTime DateFile = System.IO.File.GetLastWriteTime(filename);

            TimeSpan result = DateSystem - DateFile;

            if (result.TotalHours >= hours) value = true; else value = false;

            return value;
        }

        private TheaterEntity[] Load()
        {
            TheaterEntity[] theaters;
            string filePath = String.Empty;
            //comparar las dos fechas y ver si ya tiene mas de 24 horas. alli entonces actualizar el
            //archivo.

            //#if DEBUG
            filePath = (HostingEnvironment.MapPath("~/App_Data/Theaters.xml")); ;
            //#endif

            //#if (!DEBUG)
            //                string cachePath = RoleEnvironment.GetLocalResource("Theaters").RootPath;
            //                filePath = Path.Combine(cachePath, "Theaters.xml");
            //#endif

            if (!File.Exists(filePath))
                File.Create(filePath).Close();

            //Boolean valido = CompararHoras(filePath,168);

            FileInfo file = new FileInfo(filePath);

            if (file.Length < 20)
            {
                theaters = Execute();
                string json = JsonConvert.SerializeObject(theaters);
                File.WriteAllText(filePath, json);
            }
            else
            {
                theaters = JsonConvert.DeserializeObject<TheaterEntity[]>(File.ReadAllText(filePath));
            }


            //theaters = m_cache.Get("Theaters");
            //if (theaters == null || theaters.Count() == 0)
            //{
            //    theaters = Execute();
            //    m_cache.Set("Theaters", theaters, TimeSpan.FromHours(24));
            //}
            //m_cache.Close();
            return theaters;
        }

        public void Clear()
        {
            m_cache.Delete("Theaters");
            m_cache.Close();
        }

        #endregion Methods

        #region Properties

        public ConfigurationEntity Configuration { get; set; }

        public TheaterEntity[] Data
        {
            get
            {
                TheaterEntity[] theaters = Load();
                return theaters;
            }
        }

        #endregion Properties
    }
}