﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Web;
using System.Net.Http;
using System.Web.Hosting;

namespace Payment_Services
{
    public class Payment
    {
        public Payment()
        {
            XMLObj = new System.Xml.XmlDocument();
            XMLObj.LoadXml("<?xml version='1.0' encoding='UTF-8'?><response>Null</response>");
        }
        #region Fields
        public System.Xml.XmlDocument XMLObj;
        public string URLPay;
        private string m_CodeResult;
        private string m_DescriptionResult;
        private string m_Voucher;
        private string m_SequenceNumber;
        public string Terminal { get; set; }
        public string AuthId { get; set; }
        public string Referencia { get; set; }
        #endregion
        #region Properties
        public string CodeResult
        {
            get { return m_CodeResult; }
            set { m_CodeResult = value; }
        }
        public string DescriptionResult
        {
            get { return m_DescriptionResult; }
            set { m_DescriptionResult = value; }
        }
        public string Voucher
        {
            get { return m_Voucher; }
            set { m_Voucher = value; }
        }
        public string SequenceNumber
        {
            get { return m_SequenceNumber; }
            set { m_SequenceNumber = value; }
        }
        #endregion
        #region Methods
        public bool ApproveFromParameters(PaymentRequest PaymentRequest)
        {
            //Fase 1: Construir URL					
            string Mode = ConfigurationManager.AppSettings["Mode"].ToString();
            string Transcode = ConfigurationManager.AppSettings["Transcode"].ToString();
            string CodAfiliacion = ConfigurationManager.AppSettings[string.Format("CodAfiliacion{0}", PaymentRequest.CompanyId)].ToString();
            string PaymentAddress = ConfigurationManager.AppSettings["PaymentAddress"].ToString();
            try
            {
                string URL = string.Format("{0}?cod_afiliacion={1}&transcode={2}&pan={3}&cvv2={4}&cid={5}&expdate={6}&amount={7}&client={8}&factura={9}&field1={10}&field2={11}&field3={12}&field4={13}&mode={14}",
                    PaymentAddress,
                    CodAfiliacion,
                    Transcode,
                    PaymentRequest.CardNumber,
                    PaymentRequest.CVV,
                    PaymentRequest.IDCard,
                    ExpDate(PaymentRequest.ExpMonth, PaymentRequest.ExpYear),
                    FixAmount(PaymentRequest.Amount),
                    PaymentRequest.Name,
                    PaymentRequest.Factura,
                    PaymentRequest.Cinema,
                    PaymentRequest.BookingNumber,
                    PaymentRequest.VistaOrderId,
                    PaymentRequest.ClientId,
                    Mode);
                URLPay = URL;

                //Fase 2: Llamar web service			

                ////Para ignorar errores de SSL
                ServicePointManager.CertificatePolicy = new AcceptAllCertificatePolicy();

                System.Net.WebClient ServerHTTP = new System.Net.WebClient();

                Byte[] Databuffer = ServerHTTP.DownloadData(URL);
                string ResponseText = System.Text.Encoding.UTF8.GetString(Databuffer);

                //Fase 3: Parsear respuesta

                XMLObj.LoadXml(ResponseText);
                CodeResult = XMLObj.SelectSingleNode("//codigo").InnerText;
                DescriptionResult = XMLObj.SelectSingleNode("//descripcion").InnerText;
                Voucher = XMLObj.SelectSingleNode("//voucher").InnerXml.Replace("_", " ");
                SequenceNumber = XMLObj.SelectSingleNode("//seqnum").InnerText;  //castear a int o string
                AuthId = XMLObj.SelectSingleNode("//authid").InnerText;
                Referencia = XMLObj.SelectSingleNode("//referencia").InnerText;
                Terminal = XMLObj.SelectSingleNode("//terminal").InnerText;
                if (CodeResult == "00") return true;
                else return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        /// <summary>
        /// RM - anulacion de pagos a traves de merchant.
        /// para pagos hechos con TDC
        /// </summary>
        /// <param name="PaymentRequest"></param>
        /// <param name="DataPayment"></param>
        /// <returns></returns>
        public bool AnularPagoEnMerchant(PaymentRequest PaymentRequest, Payment DataPayment)
        {
            //Fase 1: Construir URL                                                                   
            string Mode = ConfigurationManager.AppSettings["Mode"].ToString();
            string CodAfiliacion = ConfigurationManager.AppSettings[string.Format("CodAfiliacion{0}", PaymentRequest.CompanyId)].ToString();
            string PaymentAddress = ConfigurationManager.AppSettings["AnulationAddress"].ToString(); //cambiar por anulacionAddres
            try
            {
                string URL = string.Format("{0}?cod_afiliacion={1}&terminal={2}&seqnum={3}&monto={4}&factura={5}&referencia={6}&ult={7}&authid={8}",
                    PaymentAddress,
                    CodAfiliacion,
                    DataPayment.Terminal,
                    DataPayment.SequenceNumber,
                    FixAmount(PaymentRequest.Amount),
                    PaymentRequest.Factura,
                    DataPayment.Referencia,
                    PaymentRequest.CardNumber.Substring(PaymentRequest.CardNumber.Length - 4),
                    DataPayment.AuthId);
                URLPay = URL;

                //Fase 2: Llamar web service                                    

                ////Para ignorar errores de SSL
                ServicePointManager.CertificatePolicy = new AcceptAllCertificatePolicy();

                System.Net.WebClient ServerHTTP = new System.Net.WebClient();

                Byte[] Databuffer = ServerHTTP.DownloadData(URL);
                string ResponseText = System.Text.Encoding.UTF8.GetString(Databuffer);

                //Fase 3: Parsear respuesta

                XMLObj.LoadXml(ResponseText);
                CodeResult = XMLObj.SelectSingleNode("//codigo").InnerText;
                DescriptionResult = XMLObj.SelectSingleNode("//descripcion").InnerText;
                Voucher = XMLObj.SelectSingleNode("//voucher").InnerXml.Replace("_", " ");
                string ruta = Path.Combine(HostingEnvironment.MapPath("~/App_Data"), string.Format("{0}{1}{2}", PaymentRequest.IDCard, DateTime.Now.ToString().Replace(" ","").Replace(".","").Replace(":", "").Replace("/","-"), ".txt"));
                using (StreamWriter w = new StreamWriter(ruta))
                {
                    w.WriteLine(ResponseText);
                    w.Close();
                }
                if (CodeResult == "00") return true;
                else return false;
            }
            catch (Exception e)
            {
                string ruta = Path.Combine(HostingEnvironment.MapPath("~/App_Data"), string.Format("{0}{1}{2}", PaymentRequest.IDCard, DateTime.Now.ToString().Replace(" ", "").Replace(".", "").Replace(":", "").Replace("/", "-"), ".txt"));
                using (StreamWriter w = new StreamWriter(ruta))
                {
                    w.WriteLine("Mensaje: " + e.Message + "InnerExcepcion: " + e.InnerException);
                    w.Close();
                }
                return false;
            }
        }


        public int seqnum()
        {
            try
            {
                return Convert.ToInt32(XMLObj.SelectSingleNode("//seqnum").InnerText);
            }
            catch
            {
                return 0;
            }
        }
        private string OnlyNumbers(string s)
        {
            char next;
            string AuxS = "";
            System.CharEnumerator iter = s.GetEnumerator();
            while (iter.MoveNext())
            {
                next = iter.Current;
                if (Char.IsDigit(next))
                    AuxS += next;
            }
            return AuxS;
        }
        private string ExpDate(int Month, int Year)
        {
            int M, Y;
            M = Month + 100;
            Y = Year + 100;
            return M.ToString().Substring(M.ToString().Length - 2) + Y.ToString().Substring(Y.ToString().Length - 2);
        }
        private string FixAmount(decimal d)
        {
            return Convert.ToUInt32(d * 100).ToString();
        }
        #endregion

        internal class AcceptAllCertificatePolicy : ICertificatePolicy
        {
            //Esta clase y su método CheckValidationResult
            public AcceptAllCertificatePolicy() { }
            public bool CheckValidationResult(ServicePoint sPoint,
                X509Certificate cert, WebRequest wRequest, int certProb)
            {
                return true;
            }
        }
    }
    public class PaymentRequest
    {
        #region Fields
        private string m_CardNumber;
        private string m_CVV;
        private string m_IDCard;
        private int m_ExpMonth;
        private int m_ExpYear;
        private decimal m_Amount;
        private string m_Name;
        private int m_Factura;
        private string m_Cinema;
        private int m_BookingNumber;
        private string m_VistaOrderId;
        private string m_ClientId;
        private string m_CompanyId;
        #endregion
        #region Properties
        public string CardNumber
        {
            get { return m_CardNumber; }
            set { m_CardNumber = value; }
        }
        public string CVV
        {
            get { return m_CVV; }
            set { m_CVV = value; }
        }
        public string IDCard
        {
            get { return m_IDCard; }
            set { m_IDCard = value; }
        }
        public int ExpMonth
        {
            get { return m_ExpMonth; }
            set { m_ExpMonth = value; }
        }
        public int ExpYear
        {
            get { return m_ExpYear; }
            set { m_ExpYear = value; }
        }
        public decimal Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public int Factura
        {
            get { return m_Factura; }
            set { m_Factura = value; }
        }
        public string Cinema
        {
            get { return m_Cinema; }
            set { m_Cinema = value; }
        }
        public int BookingNumber
        {
            get { return m_BookingNumber; }
            set { m_BookingNumber = value; }
        }
        public string VistaOrderId
        {
            get { return m_VistaOrderId; }
            set { m_VistaOrderId = value; }
        }
        public string ClientId
        {
            get { return m_ClientId; }
            set { m_ClientId = value; }
        }
        public string CompanyId
        {
            get { return m_CompanyId; }
            set { m_CompanyId = value; }
        }
        #endregion
    }
}
