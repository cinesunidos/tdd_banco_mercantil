﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;

namespace Services.Vista.DAL
{
    public class Connection
    {
        SqlConnection conn;
        //RM-
        //string para setear el command type dependiendo de la naturaleza del query, el comandType por defecto es text; si este string se setea distinto de "", entonces el comand type se convierte a storeprocedure
        public string comandtype { get; set; }
        public Connection(string connectionString)
        {
            conn = new SqlConnection(connectionString);
            comandtype = "";
        }
        public void Open()
        {
            conn.Open();
        }
        public void Close()
        {
            if (conn != null) conn.Close();
        }
        public SqlDataReader ExecuteRead(string query, Dictionary<string, object> parameters)
        {
            SqlDataReader dataReader;
            SqlCommand sqlCommand;
            try
            {
                sqlCommand = conn.CreateCommand();
                if (comandtype == "")
                {
                    sqlCommand.CommandType = CommandType.Text;
                }
                else
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                }
                

                foreach (KeyValuePair<string, object> item in parameters)
                {
                    SqlParameter parameter = sqlCommand.CreateParameter();
                    parameter.ParameterName = string.Format("@{0}", item.Key);
                    parameter.Value = item.Value;
                    sqlCommand.Parameters.Add(parameter);
                }
                sqlCommand.CommandText = query;
                dataReader = sqlCommand.ExecuteReader();
                
                return dataReader;
            }
            catch (SqlException ex)
            {
                return null;
            }
        }
        public int ExectuteInsert(string query, Dictionary<string, object> parameters)
        {            
            SqlCommand sqlCommand;
            try
            {
                sqlCommand = conn.CreateCommand();
                sqlCommand.CommandType = CommandType.Text;

                foreach (KeyValuePair<string, object> item in parameters)
                {
                    SqlParameter parameter = sqlCommand.CreateParameter();
                    parameter.ParameterName = string.Format("@{0}", item.Key);
                    parameter.Value = item.Value;
                    sqlCommand.Parameters.Add(parameter);
                }
                sqlCommand.CommandText = query;
                int n = sqlCommand.ExecuteNonQuery();
                return n;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }
    }
}

