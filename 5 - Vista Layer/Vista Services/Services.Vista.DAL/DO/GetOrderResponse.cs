﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public partial class GetOrderResponse
    {
        public static GetOrderResponse GetOrder(string UserSessionID)
        {
            using (DO.TicketingService Ticketing = new DO.TicketingService(ConfigurationManager.AppSettings["url"] + "TicketingService.asmx"))
            {

                GetOrderRequest getOrderRequest = new GetOrderRequest();
                getOrderRequest.ProcessOrderValue = true;
                getOrderRequest.UserSessionId = UserSessionID;
                getOrderRequest.BookingMode = 0;
                getOrderRequest.OptionalClientId = "111.111.111.120";
                GetOrderResponse rp = Ticketing.GetOrder(getOrderRequest);

                return rp;
            }
        }
    }
}
