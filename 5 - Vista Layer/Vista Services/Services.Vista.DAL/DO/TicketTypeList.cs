﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Services.Vista.DAL.DORest;
using Newtonsoft.Json;

namespace Services.Vista.DAL.DO
{
    public class TicketTypeList
    {
        #region Fields
        private int isPaidField;
        private bool isPaidFieldSpecified;
        private int availableOnSalesChannelField;
        private bool availableOnSalesChannelFieldSpecified;
        private int isDisplayedStandardField;
        private bool isDisplayedStandardFieldSpecified;
        private int isLoyaltyOnlyField;
        private bool isLoyaltyOnlyFieldSpecified;
        private int orderPricesByField;
        private bool orderPricesByFieldSpecified;
        private string ticketHasBarcodeField;
        private string cinema_strIDField;
        private string session_strIDField;
        private string areaCat_strCodeField;
        private string areaCat_strSeatAllocationOnField;
        private string cinema_strID1Field;
        private string price_strTicket_Type_CodeField;
        private string price_strTicket_Type_DescriptionField;
        private string price_strGroup_CodeField;
        private int price_intTicket_PriceField;
        private bool price_intTicket_PriceFieldSpecified;
        private string price_strChild_TicketField;
        private string areaCat_strCode1Field;
        private int areaCat_intSeqField;
        private bool areaCat_intSeqFieldSpecified;
        private string price_strTicket_Type_Description_2Field;
        private string price_strPackageField;
        private string tType_strAvailLoyaltyOnlyField;
        private string tType_strHOCodeField;
        private string price_strRedemptionField;
        private string price_strCompField;
        private string tType_strSalesChannelsField;
        private string price_strATMAvailableField;
        private string tType_strShowOnPOSField;
        private int price_intSurchargeField;
        private bool price_intSurchargeFieldSpecified;
        private int tType_intBarcodeMaxRepeatsField;
        private bool tType_intBarcodeMaxRepeatsFieldSpecified;
        private string tType_strMaxRepeatsCycleField;
        private string tType_strLongDescriptionField;
        private string tType_strLongDescriptionAltField;
        private string tType_strMemberCardField;
        private string tType_strAvailRecognitionOnlyField;
        private string hOPKField;
        private string areaCat_strDescField;
        private string areaCat_strDescAltField;
        private string mMC_strNameField;
        private int price_intSequenceField;
        private bool price_intSequenceFieldSpecified;
        private string price_strIsTicketPackageField;
        private string loyaltyRewardTicketField;
        private string loyaltyTicketNameField;
        private string loyaltyTicketDescriptionField;
        private string loyaltyTicketMessageTextField;
        private string orderTicketsByField;
        private string ticketCategoryField;
        private string price_strLoyaltyRecognitionIDField;
        private string price_intLoyaltyDisplayPriceField;
        private string price_intLoyaltyPriceCentsField;
        private string price_intLoyaltyPointsCostField;
        private string price_intLoyaltyQtyAvailableField;
        #endregion
        #region Properties
        public int IsPaid
        {
            get
            {
                return this.isPaidField;
            }
            set
            {
                this.isPaidField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsPaidSpecified
        {
            get
            {
                return this.isPaidFieldSpecified;
            }
            set
            {
                this.isPaidFieldSpecified = value;
            }
        }
        public int AvailableOnSalesChannel
        {
            get
            {
                return this.availableOnSalesChannelField;
            }
            set
            {
                this.availableOnSalesChannelField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailableOnSalesChannelSpecified
        {
            get
            {
                return this.availableOnSalesChannelFieldSpecified;
            }
            set
            {
                this.availableOnSalesChannelFieldSpecified = value;
            }
        }
        public int IsDisplayedStandard
        {
            get
            {
                return this.isDisplayedStandardField;
            }
            set
            {
                this.isDisplayedStandardField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsDisplayedStandardSpecified
        {
            get
            {
                return this.isDisplayedStandardFieldSpecified;
            }
            set
            {
                this.isDisplayedStandardFieldSpecified = value;
            }
        }
        public int IsLoyaltyOnly
        {
            get
            {
                return this.isLoyaltyOnlyField;
            }
            set
            {
                this.isLoyaltyOnlyField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsLoyaltyOnlySpecified
        {
            get
            {
                return this.isLoyaltyOnlyFieldSpecified;
            }
            set
            {
                this.isLoyaltyOnlyFieldSpecified = value;
            }
        }
        public int OrderPricesBy
        {
            get
            {
                return this.orderPricesByField;
            }
            set
            {
                this.orderPricesByField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OrderPricesBySpecified
        {
            get
            {
                return this.orderPricesByFieldSpecified;
            }
            set
            {
                this.orderPricesByFieldSpecified = value;
            }
        }
        public string TicketHasBarcode
        {
            get
            {
                return this.ticketHasBarcodeField;
            }
            set
            {
                this.ticketHasBarcodeField = value;
            }
        }
        public string Cinema_strID
        {
            get
            {
                return this.cinema_strIDField;
            }
            set
            {
                this.cinema_strIDField = value;
            }
        }
        public string Session_strID
        {
            get
            {
                return this.session_strIDField;
            }
            set
            {
                this.session_strIDField = value;
            }
        }
        public string AreaCat_strCode
        {
            get
            {
                return this.areaCat_strCodeField;
            }
            set
            {
                this.areaCat_strCodeField = value;
            }
        }
        public string AreaCat_strSeatAllocationOn
        {
            get
            {
                return this.areaCat_strSeatAllocationOnField;
            }
            set
            {
                this.areaCat_strSeatAllocationOnField = value;
            }
        }
        public string Cinema_strID1
        {
            get
            {
                return this.cinema_strID1Field;
            }
            set
            {
                this.cinema_strID1Field = value;
            }
        }
        public string Price_strTicket_Type_Code
        {
            get
            {
                return this.price_strTicket_Type_CodeField;
            }
            set
            {
                this.price_strTicket_Type_CodeField = value;
            }
        }
        public string Price_strTicket_Type_Description
        {
            get
            {
                return this.price_strTicket_Type_DescriptionField;
            }
            set
            {
                this.price_strTicket_Type_DescriptionField = value;
            }
        }
        public string Price_strGroup_Code
        {
            get
            {
                return this.price_strGroup_CodeField;
            }
            set
            {
                this.price_strGroup_CodeField = value;
            }
        }
        public int Price_intTicket_Price
        {
            get
            {
                return this.price_intTicket_PriceField;
            }
            set
            {
                this.price_intTicket_PriceField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Price_intTicket_PriceSpecified
        {
            get
            {
                return this.price_intTicket_PriceFieldSpecified;
            }
            set
            {
                this.price_intTicket_PriceFieldSpecified = value;
            }
        }
        public string Price_strChild_Ticket
        {
            get
            {
                return this.price_strChild_TicketField;
            }
            set
            {
                this.price_strChild_TicketField = value;
            }
        }
        public string AreaCat_strCode1
        {
            get
            {
                return this.areaCat_strCode1Field;
            }
            set
            {
                this.areaCat_strCode1Field = value;
            }
        }
        public int AreaCat_intSeq
        {
            get
            {
                return this.areaCat_intSeqField;
            }
            set
            {
                this.areaCat_intSeqField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AreaCat_intSeqSpecified
        {
            get
            {
                return this.areaCat_intSeqFieldSpecified;
            }
            set
            {
                this.areaCat_intSeqFieldSpecified = value;
            }
        }
        public string Price_strTicket_Type_Description_2
        {
            get
            {
                return this.price_strTicket_Type_Description_2Field;
            }
            set
            {
                this.price_strTicket_Type_Description_2Field = value;
            }
        }
        public string Price_strPackage
        {
            get
            {
                return this.price_strPackageField;
            }
            set
            {
                this.price_strPackageField = value;
            }
        }
        public string TType_strAvailLoyaltyOnly
        {
            get
            {
                return this.tType_strAvailLoyaltyOnlyField;
            }
            set
            {
                this.tType_strAvailLoyaltyOnlyField = value;
            }
        }
        public string TType_strHOCode
        {
            get
            {
                return this.tType_strHOCodeField;
            }
            set
            {
                this.tType_strHOCodeField = value;
            }
        }
        public string Price_strRedemption
        {
            get
            {
                return this.price_strRedemptionField;
            }
            set
            {
                this.price_strRedemptionField = value;
            }
        }
        public string Price_strComp
        {
            get
            {
                return this.price_strCompField;
            }
            set
            {
                this.price_strCompField = value;
            }
        }
        public string TType_strSalesChannels
        {
            get
            {
                return this.tType_strSalesChannelsField;
            }
            set
            {
                this.tType_strSalesChannelsField = value;
            }
        }
        public string Price_strATMAvailable
        {
            get
            {
                return this.price_strATMAvailableField;
            }
            set
            {
                this.price_strATMAvailableField = value;
            }
        }
        public string TType_strShowOnPOS
        {
            get
            {
                return this.tType_strShowOnPOSField;
            }
            set
            {
                this.tType_strShowOnPOSField = value;
            }
        }
        public int Price_intSurcharge
        {
            get
            {
                return this.price_intSurchargeField;
            }
            set
            {
                this.price_intSurchargeField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Price_intSurchargeSpecified
        {
            get
            {
                return this.price_intSurchargeFieldSpecified;
            }
            set
            {
                this.price_intSurchargeFieldSpecified = value;
            }
        }
        public int TType_intBarcodeMaxRepeats
        {
            get
            {
                return this.tType_intBarcodeMaxRepeatsField;
            }
            set
            {
                this.tType_intBarcodeMaxRepeatsField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TType_intBarcodeMaxRepeatsSpecified
        {
            get
            {
                return this.tType_intBarcodeMaxRepeatsFieldSpecified;
            }
            set
            {
                this.tType_intBarcodeMaxRepeatsFieldSpecified = value;
            }
        }
        public string TType_strMaxRepeatsCycle
        {
            get
            {
                return this.tType_strMaxRepeatsCycleField;
            }
            set
            {
                this.tType_strMaxRepeatsCycleField = value;
            }
        }
        public string TType_strLongDescription
        {
            get
            {
                return this.tType_strLongDescriptionField;
            }
            set
            {
                this.tType_strLongDescriptionField = value;
            }
        }
        public string TType_strLongDescriptionAlt
        {
            get
            {
                return this.tType_strLongDescriptionAltField;
            }
            set
            {
                this.tType_strLongDescriptionAltField = value;
            }
        }
        public string TType_strMemberCard
        {
            get
            {
                return this.tType_strMemberCardField;
            }
            set
            {
                this.tType_strMemberCardField = value;
            }
        }
        public string TType_strAvailRecognitionOnly
        {
            get
            {
                return this.tType_strAvailRecognitionOnlyField;
            }
            set
            {
                this.tType_strAvailRecognitionOnlyField = value;
            }
        }
        public string HOPK
        {
            get
            {
                return this.hOPKField;
            }
            set
            {
                this.hOPKField = value;
            }
        }
        public string AreaCat_strDesc
        {
            get
            {
                return this.areaCat_strDescField;
            }
            set
            {
                this.areaCat_strDescField = value;
            }
        }
        public string AreaCat_strDescAlt
        {
            get
            {
                return this.areaCat_strDescAltField;
            }
            set
            {
                this.areaCat_strDescAltField = value;
            }
        }
        public string MMC_strName
        {
            get
            {
                return this.mMC_strNameField;
            }
            set
            {
                this.mMC_strNameField = value;
            }
        }
        public int Price_intSequence
        {
            get
            {
                return this.price_intSequenceField;
            }
            set
            {
                this.price_intSequenceField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Price_intSequenceSpecified
        {
            get
            {
                return this.price_intSequenceFieldSpecified;
            }
            set
            {
                this.price_intSequenceFieldSpecified = value;
            }
        }
        public string Price_strIsTicketPackage
        {
            get
            {
                return this.price_strIsTicketPackageField;
            }
            set
            {
                this.price_strIsTicketPackageField = value;
            }
        }
        public string LoyaltyRewardTicket
        {
            get
            {
                return this.loyaltyRewardTicketField;
            }
            set
            {
                this.loyaltyRewardTicketField = value;
            }
        }
        public string LoyaltyTicketName
        {
            get
            {
                return this.loyaltyTicketNameField;
            }
            set
            {
                this.loyaltyTicketNameField = value;
            }
        }
        public string LoyaltyTicketDescription
        {
            get
            {
                return this.loyaltyTicketDescriptionField;
            }
            set
            {
                this.loyaltyTicketDescriptionField = value;
            }
        }
        public string LoyaltyTicketMessageText
        {
            get
            {
                return this.loyaltyTicketMessageTextField;
            }
            set
            {
                this.loyaltyTicketMessageTextField = value;
            }
        }
        public string OrderTicketsBy
        {
            get
            {
                return this.orderTicketsByField;
            }
            set
            {
                this.orderTicketsByField = value;
            }
        }
        public string TicketCategory
        {
            get
            {
                return this.ticketCategoryField;
            }
            set
            {
                this.ticketCategoryField = value;
            }
        }
        public string Price_strLoyaltyRecognitionID
        {
            get
            {
                return this.price_strLoyaltyRecognitionIDField;
            }
            set
            {
                this.price_strLoyaltyRecognitionIDField = value;
            }
        }
        public string Price_intLoyaltyDisplayPrice
        {
            get
            {
                return this.price_intLoyaltyDisplayPriceField;
            }
            set
            {
                this.price_intLoyaltyDisplayPriceField = value;
            }
        }
        public string Price_intLoyaltyPriceCents
        {
            get
            {
                return this.price_intLoyaltyPriceCentsField;
            }
            set
            {
                this.price_intLoyaltyPriceCentsField = value;
            }
        }
        public string Price_intLoyaltyPointsCost
        {
            get
            {
                return this.price_intLoyaltyPointsCostField;
            }
            set
            {
                this.price_intLoyaltyPointsCostField = value;
            }
        }
        public string Price_intLoyaltyQtyAvailable
        {
            get
            {
                return this.price_intLoyaltyQtyAvailableField;
            }
            set
            {
                this.price_intLoyaltyQtyAvailableField = value;
            }
        }
        #endregion
        #region Methods

        //public static List<TicketTypeList> GetTicketTypeList(string CinemaID, int SessionID)
        //{
        //    //DataService.GetTicketTypeListRequest req = new DataService.GetTicketTypeListRequest();
        //    //req.CinemaId = CinemaID;
        //    //req.SessionId = SessionID.ToString();
        //    //req.OptionalReturnAllRedemptionAndCompTickets = true; //Type Tickets Redemption
        //    //Interpret<TicketTypeList, DataService.GetTicketTypeListRequest> interpret = new Interpret<TicketTypeList, DataService.GetTicketTypeListRequest>();
        //    //List<TicketTypeList> TicketTypes = interpret.Get("GetTicketTypeList", req);

        //    //return TicketTypes;
        //    using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
        //    {
        //        GetTicketTypeListRequest req = new GetTicketTypeListRequest();
        //        req.CinemaId = CinemaID;
        //        req.SessionId = SessionID.ToString();
        //        req.OptionalReturnAllRedemptionAndCompTickets = true; //Type Tickets Redemption
        //        var respuesta = servicio.GetTicketTypeList(req);
        //        XElement element = XElement.Parse(respuesta.DatasetXML.ToString());
        //        List<TicketTypeList> lista = element.Elements("Table")
        //            .Select(item => new TicketTypeList()
        //            {
        //                #region MyRegion
        //                AreaCat_intSeq = (item.Elements("AreaCat_intSeq").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("AreaCat_intSeq").Value)), //  Convert.ToInt32(item.Element("AreaCat_intSeq").Value),
        //                AreaCat_strSeatAllocationOn = (item.Elements("AreaCat_strSeatAllocationOn").FirstOrDefault() == null ? String.Empty : item.Element("AreaCat_strSeatAllocationOn").Value), //(item.Element("AreaCat_strSeatAllocationOn").Value),
        //                Price_intSurcharge = (item.Elements("Price_intSurcharge").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Price_intSurcharge").Value)), //Convert.ToInt32(item.Element("Price_intSurcharge").Value),
        //                Price_intTicket_Price = (item.Elements("Price_intTicket_Price").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Price_intTicket_Price").Value)), //Convert.ToInt32(item.Element("Price_intTicket_Price").Value),
        //                Price_strTicket_Type_Description = (item.Elements("Price_strTicket_Type_Description").FirstOrDefault() == null ? String.Empty : item.Element("Price_strTicket_Type_Description").Value), //Convert.ToString(item.Element("Price_strTicket_Type_Description").Value),
        //                Price_strTicket_Type_Description_2 = (item.Elements("Price_strTicket_Type_Description_2").FirstOrDefault() == null ? String.Empty : item.Element("Price_strTicket_Type_Description_2").Value), //Convert.ToString(item.Element("Price_strTicket_Type_Description_2").Value),
        //                TType_strAvailLoyaltyOnly = (item.Elements("TType_strAvailLoyaltyOnly").FirstOrDefault() == null ? String.Empty : item.Element("TType_strAvailLoyaltyOnly").Value), //Convert.ToString(item.Element("TType_strAvailLoyaltyOnly").Value),
        //                AreaCat_intSeqSpecified = (item.Elements("AreaCat_intSeqSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("AreaCat_intSeqSpecified").Value)), //Convert.ToBoolean(item.Element("AreaCat_intSeqSpecified").Value),
        //                AreaCat_strCode = (item.Elements("AreaCat_strCode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("AreaCat_strCode").Value)), //Convert.ToString(item.Element("AreaCat_strCode").Value),
        //                AreaCat_strCode1 = (item.Elements("AreaCat_strCode1").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("AreaCat_strCode1").Value)), //Convert.ToString(item.Element("AreaCat_strCode1").Value),
        //                AreaCat_strDesc = (item.Elements("AreaCat_strDesc").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("AreaCat_strDesc").Value)), //Convert.ToString(item.Element("AreaCat_strDesc").Value),
        //                AreaCat_strDescAlt = (item.Elements("AreaCat_strDescAlt").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("AreaCat_strDescAlt").Value)), //Convert.ToString(item.Element("AreaCat_strDescAlt").Value),
        //                AvailableOnSalesChannel = (item.Elements("AvailableOnSalesChannel").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("AvailableOnSalesChannel").Value)), //Convert.ToInt32(item.Element("AvailableOnSalesChannel").Value),
        //                AvailableOnSalesChannelSpecified = (item.Elements("AvailableOnSalesChannelSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("AvailableOnSalesChannelSpecified").Value)), //Convert.ToBoolean(item.Element("AvailableOnSalesChannelSpecified").Value),
        //                Cinema_strID = (item.Elements("Cinema_strID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Cinema_strID").Value)), //Convert.ToString(item.Element("Cinema_strID").Value),
        //                Cinema_strID1 = (item.Elements("Cinema_strID1").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Cinema_strID1").Value)), //Convert.ToString(item.Element("Cinema_strID1").Value),
        //                HOPK = (item.Elements("HOPK").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("HOPK").Value)), //Convert.ToString(item.Element("HOPK").Value),
        //                IsDisplayedStandard = (item.Elements("IsDisplayedStandard").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("IsDisplayedStandard").Value)), //Convert.ToInt32(item.Element("IsDisplayedStandard").Value),
        //                IsLoyaltyOnly = (item.Elements("IsLoyaltyOnly").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("IsLoyaltyOnly").Value)), //Convert.ToInt32(item.Element("IsLoyaltyOnly").Value),
        //                IsLoyaltyOnlySpecified = (item.Elements("IsLoyaltyOnlySpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("IsLoyaltyOnlySpecified").Value)), //Convert.ToBoolean(item.Element("IsLoyaltyOnlySpecified").Value),
        //                IsDisplayedStandardSpecified = (item.Elements("IsDisplayedStandardSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("IsDisplayedStandardSpecified").Value)), //Convert.ToBoolean(item.Element("IsDisplayedStandardSpecified").Value),
        //                IsPaid = (item.Elements("IsPaid").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("IsPaid").Value)), //Convert.ToInt32(item.Element("IsPaid").Value),
        //                IsPaidSpecified = (item.Elements("IsPaidSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("IsPaidSpecified").Value)), //Convert.ToBoolean(item.Element("IsPaidSpecified").Value),
        //                LoyaltyRewardTicket = (item.Elements("LoyaltyRewardTicket").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("LoyaltyRewardTicket").Value)), //Convert.ToString(item.Element("LoyaltyRewardTicket").Value),
        //                LoyaltyTicketDescription = (item.Elements("LoyaltyTicketDescription").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("LoyaltyTicketDescription").Value)), //Convert.ToString(item.Element("LoyaltyTicketDescription").Value),
        //                LoyaltyTicketMessageText = (item.Elements("LoyaltyTicketMessageText").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("LoyaltyTicketMessageText").Value)), //Convert.ToString(item.Element("LoyaltyTicketMessageText").Value),
        //                LoyaltyTicketName = (item.Elements("LoyaltyTicketName").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("LoyaltyTicketName").Value)), //Convert.ToString(item.Element("LoyaltyTicketName").Value),
        //                MMC_strName = (item.Elements("MMC_strName").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("MMC_strName").Value)), //Convert.ToString(item.Element("MMC_strName").Value),
        //                OrderPricesBy = (item.Elements("OrderPricesBy").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("OrderPricesBy").Value)), //Convert.ToInt32(item.Element("OrderPricesBy").Value),
        //                OrderPricesBySpecified = (item.Elements("OrderPricesBySpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("OrderPricesBySpecified").Value)), // Convert.ToBoolean(item.Element("OrderPricesBySpecified").Value),
        //                OrderTicketsBy = (item.Elements("OrderTicketsBy").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("OrderTicketsBy").Value)), //Convert.ToString(item.Element("OrderTicketsBy").Value),
        //                Price_intLoyaltyDisplayPrice = (item.Elements("Price_intLoyaltyDisplayPrice").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_intLoyaltyDisplayPrice").Value)), //Convert.ToString(item.Element("Price_intLoyaltyDisplayPrice").Value),
        //                Price_intLoyaltyPointsCost = (item.Elements("Price_intLoyaltyPointsCost").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_intLoyaltyPointsCost").Value)), //Convert.ToString(item.Element("Price_intLoyaltyPointsCost").Value),
        //                Price_intLoyaltyPriceCents = (item.Elements("Price_intLoyaltyPriceCents").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_intLoyaltyPriceCents").Value)), //Convert.ToString(item.Element("Price_intLoyaltyPriceCents").Value),
        //                Price_intLoyaltyQtyAvailable = (item.Elements("Price_intLoyaltyQtyAvailable").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_intLoyaltyQtyAvailable").Value)), //Convert.ToString(item.Element("Price_intLoyaltyQtyAvailable").Value),
        //                Price_intSequence = (item.Elements("Price_intSequence").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Price_intSequence").Value)), //Convert.ToInt32(item.Element("Price_intSequence").Value),
        //                Price_intSequenceSpecified = (item.Elements("Price_intSequenceSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Price_intSequenceSpecified").Value)), //Convert.ToBoolean(item.Element("Price_intSequenceSpecified").Value),
        //                Price_intSurchargeSpecified = (item.Elements("Price_intSurchargeSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Price_intSurchargeSpecified").Value)), //Convert.ToBoolean(item.Element("Price_intSurchargeSpecified").Value),
        //                Price_intTicket_PriceSpecified = (item.Elements("Price_intTicket_PriceSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Price_intTicket_PriceSpecified").Value)), //Convert.ToBoolean(item.Element("Price_intTicket_PriceSpecified").Value),
        //                Price_strATMAvailable = (item.Elements("Price_strATMAvailable").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strATMAvailable").Value)), //Convert.ToString(item.Element("Price_strATMAvailable").Value),
        //                Price_strChild_Ticket = (item.Elements("Price_strChild_Ticket").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strChild_Ticket").Value)), //Convert.ToString(item.Element("Price_strChild_Ticket").Value),
        //                Price_strComp = (item.Elements("Price_strComp").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strComp").Value)), //Convert.ToString(item.Element("Price_strComp").Value),
        //                Price_strGroup_Code = (item.Elements("Price_strGroup_Code").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strGroup_Code").Value)), //Convert.ToString(item.Element("Price_strGroup_Code").Value),
        //                Price_strIsTicketPackage = (item.Elements("Price_strIsTicketPackage").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strIsTicketPackage").Value)), //Convert.ToString(item.Element("Price_strIsTicketPackage").Value),
        //                Price_strLoyaltyRecognitionID = (item.Elements("Price_strLoyaltyRecognitionID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strLoyaltyRecognitionID").Value)), //Convert.ToString(item.Element("Price_strLoyaltyRecognitionID").Value),
        //                Price_strPackage = (item.Elements("Price_strPackage").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strPackage").Value)), //Convert.ToString(item.Element("Price_strPackage").Value),
        //                Price_strRedemption = (item.Elements("Price_strRedemption").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strRedemption").Value)), //Convert.ToString(item.Element("Price_strRedemption").Value),
        //                Price_strTicket_Type_Code = (item.Elements("Price_strTicket_Type_Code").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strTicket_Type_Code").Value)), //Convert.ToString(item.Element("Price_strTicket_Type_Code").Value),
        //                Session_strID = (item.Elements("Session_strID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strID").Value)), //Convert.ToString(item.Element("Session_strID").Value),
        //                TicketCategory = (item.Elements("TicketCategory").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TicketCategory").Value)), //Convert.ToString(item.Element("TicketCategory").Value),
        //                TicketHasBarcode = (item.Elements("TicketHasBarcode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TicketHasBarcode").Value)), //Convert.ToString(item.Element("TicketHasBarcode").Value),
        //                TType_intBarcodeMaxRepeats = (item.Elements("TType_intBarcodeMaxRepeats").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("TType_intBarcodeMaxRepeats").Value)), //Convert.ToInt32(item.Element("TType_intBarcodeMaxRepeats").Value),
        //                TType_intBarcodeMaxRepeatsSpecified = (item.Elements("TType_intBarcodeMaxRepeatsSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("TType_intBarcodeMaxRepeatsSpecified").Value)), //Convert.ToBoolean(item.Element("TType_intBarcodeMaxRepeatsSpecified").Value),
        //                TType_strAvailRecognitionOnly = (item.Elements("TType_strAvailRecognitionOnly").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strAvailRecognitionOnly").Value)), //Convert.ToString(item.Element("TType_strAvailRecognitionOnly").Value),
        //                TType_strHOCode = (item.Elements("TType_strHOCode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strHOCode").Value)), //Convert.ToString(item.Element("TType_strHOCode").Value),
        //                TType_strLongDescription = (item.Elements("TType_strLongDescription").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strLongDescription").Value)), //Convert.ToString(item.Element("TType_strLongDescription").Value),
        //                TType_strLongDescriptionAlt = (item.Elements("TType_strLongDescriptionAlt").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strLongDescriptionAlt").Value)), //Convert.ToString(item.Element("TType_strLongDescriptionAlt").Value),
        //                TType_strMaxRepeatsCycle = (item.Elements("TType_strMaxRepeatsCycle").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strMaxRepeatsCycle").Value)), //Convert.ToString(item.Element("TType_strMaxRepeatsCycle").Value),
        //                TType_strMemberCard = (item.Elements("TType_strMemberCard").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strMemberCard").Value)), //Convert.ToString(item.Element("TType_strMemberCard").Value),
        //                TType_strSalesChannels = (item.Elements("TType_strSalesChannels").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strSalesChannels").Value)), //Convert.ToString(item.Element("TType_strSalesChannels").Value),
        //                TType_strShowOnPOS = (item.Elements("TType_strShowOnPOS").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("TType_strShowOnPOS").Value)) //Convert.ToString(item.Element("TType_strShowOnPOS").Value)

        //                #endregion
        //            }).ToList();
        //        return lista;
        //    }

        //}

        public static List<TicketTypeList> RestGetTicketTypeList(string CinemaID, int SessionID)
        {
            string url = string.Format(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["RestTicketTypeList"].ToString(), CinemaID, SessionID);
            var ticketType = WebConsumer.Get<TicketTypeListResponse>(url);
            //RM  - este campo obliga a que ese tipo de boleto solo sea cambiado por un voucher, no admite venta
            //ticketType.Tickets = ticketType.Tickets.Where(t => t.EnforceUseOfBarcode == false).ToList(); 
            List<TicketTypeList> boletos = new List<TicketTypeList>();
            foreach (var i in ticketType.Tickets)
            {
                TicketTypeList t = new TicketTypeList();
                t.AreaCat_intSeq = 0;
                t.AreaCat_intSeqSpecified = false;
                t.AreaCat_strCode = i.AreaCategoryCode;
                t.AreaCat_strCode1 = i.AreaCategoryCode;
                t.AreaCat_strDesc = "";
                t.AreaCat_strDescAlt = "";
                t.AreaCat_strSeatAllocationOn = i.IsAllocatableSeating == true ? "Y" : "N";
                t.AvailableOnSalesChannel = i.SalesChannels.IndexOf("WWW") >= 0 ? 1 : 0;
                t.availableOnSalesChannelFieldSpecified = false;
                t.Cinema_strID = i.CinemaId;
                t.Cinema_strID1 = i.CinemaId;
                t.HOPK = i.HOPK;
                //ojo acá
                t.IsDisplayedStandard = 1;
                t.IsDisplayedStandardSpecified = false;
                t.IsLoyaltyOnlySpecified = i.IsAvailableForLoyaltyMembersOnly;
                //ojo acá
                t.IsPaid = 0;
                //ojo aca
                t.IsPaidSpecified = false;
                //ojo aca
                t.LoyaltyRewardTicket = "";
                //ojo aca
                t.LoyaltyTicketDescription = "";
                //ojo aca
                t.LoyaltyTicketMessageText = "";
                //ojo aca
                t.LoyaltyTicketName = "";
                //ojo aca
                t.MMC_strName = "";
                //ojo aca
                t.OrderPricesBy = 0;
                //ojo aca
                t.OrderPricesBySpecified = false;
                //ojo aca
                t.OrderTicketsBy = "0";
                t.Price_intLoyaltyDisplayPrice = "";
                t.Price_intLoyaltyPointsCost = "";
                t.Price_intLoyaltyPriceCents = "";
                t.Price_intLoyaltyQtyAvailable = "";
                //t.Price_intTicket_Price = i.PriceInCents;
                t.Price_intTicket_Price = i.EnforceUseOfBarcode ? i.SurchargeAmount :i.PriceInCents; //si el ticket es solo para redencion, entonces lo que el invitado debe pagar el es el surcharge
                //ojo aca
                t.Price_intSequence = 0;
                //ojo aca
                t.Price_intSequenceSpecified = false;
                t.Price_intSurcharge = 0;
                t.Price_intSurchargeSpecified = false;
                t.Price_intTicket_PriceSpecified = false;
                t.Price_strATMAvailable = "";

                //t.Price_strChild_Ticket = ""; SE COMENTO PARA PROBAR
                t.Price_strChild_Ticket = i.IsChildOnlyTicket == true ? "Y" : "N";

                t.Price_strComp = "";
                t.Price_strGroup_Code = i.PriceGroupCode;
                t.Price_strIsTicketPackage = i.IsPackageTicket == true ? "Y" : "N";
                t.Price_strLoyaltyRecognitionID = "";
                t.Price_strPackage = i.IsPackageTicket == true ? "Y" : "N";

                //t.Price_strRedemption = i.ProductCodeForVoucher; SE COMENTO PARA PROBAR
                //t.Price_strRedemption = i.IsRedemptionTicket == true ? "Y" : "N";
                t.Price_strRedemption = i.EnforceUseOfBarcode ? "Y" : "N";

                t.Price_strTicket_Type_Code = i.TicketTypeCode;
                t.Price_strTicket_Type_Description = i.Description;

                //t.Price_strTicket_Type_Description_2 = i.Description; comentado para probar
                t.Price_strTicket_Type_Description_2 = i.DescriptionAlt;

                t.Session_strID = SessionID.ToString();
                t.TicketCategory = "";
                t.TicketHasBarcode = "";
                t.TType_intBarcodeMaxRepeats = 0;
                t.TType_intBarcodeMaxRepeatsSpecified = false;
                t.TType_strAvailLoyaltyOnly = "";
                t.TType_strAvailRecognitionOnly = "";

                //t.TType_strHOCode = i.HOPK; comentado para probar
                t.TType_strHOCode = i.HeadOfficeGroupingCode;

                t.TType_strLongDescription = "";
                t.TType_strLongDescriptionAlt = "";
                t.TType_strMaxRepeatsCycle = "O";
                t.TType_strMemberCard = "N";
                t.TType_strSalesChannels = string.Join(";", i.SalesChannels);
                t.TType_strShowOnPOS = "Y";
                boletos.Add(t);
            }

            //var hh = JsonConvert.SerializeObject(boletos);
            return boletos;
        }

        //public static List<TicketTypeList> GetTicketTypeList(string CinemaID, int SessionID)
        //{
        //    DataService.GetTicketTypeListRequest req = new DataService.GetTicketTypeListRequest();
        //    req.CinemaId = CinemaID;
        //    req.SessionId = SessionID.ToString();
        //    req.OptionalReturnAllRedemptionAndCompTickets = true; //Type Tickets Redemption
        //    Interpret<TicketTypeList, DataService.GetTicketTypeListRequest> interpret = new Interpret<TicketTypeList, DataService.GetTicketTypeListRequest>();
        //    List<TicketTypeList> TicketTypes = interpret.Get("GetTicketTypeList", req);

        //    return TicketTypes;
        //}
        #endregion
    }
}
