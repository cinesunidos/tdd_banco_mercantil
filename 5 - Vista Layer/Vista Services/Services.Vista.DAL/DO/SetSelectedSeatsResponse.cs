﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public partial class SetSelectedSeatsResponse
    {

        public static SetSelectedSeatsResponse SetSelectedSeats(string ClientID, string UserSessionID, string CinemaID, string SessionID, SelectedSeat[] SelectedSeats)
        {
            using (TicketingService Ticketing = new TicketingService(ConfigurationManager.AppSettings["url"] + "TicketingService.asmx"))
            {

                SetSelectedSeatsRequest setSelectedSeatsRequest = new SetSelectedSeatsRequest();
                setSelectedSeatsRequest.CinemaId = CinemaID;
                setSelectedSeatsRequest.ReturnOrder = true;
                setSelectedSeatsRequest.SessionId = SessionID;
                setSelectedSeatsRequest.UserSessionId = UserSessionID;
                setSelectedSeatsRequest.OptionalClientId = ClientID;
                setSelectedSeatsRequest.SelectedSeats = SelectedSeats;

                SetSelectedSeatsResponse rp = Ticketing.SetSelectedSeats(setSelectedSeatsRequest);
                return rp;
            }
        }


        //public static SetSelectedSeatsResponse SetSelectedSeats(string ClientID, string UserSessionID, string CinemaID, string SessionID, SelectedSeat[] SelectedSeats)
        //{
        //    TicketingService Ticketing = new TicketingService();

        //    SetSelectedSeatsRequest setSelectedSeatsRequest = new SetSelectedSeatsRequest();
        //    setSelectedSeatsRequest.CinemaId = CinemaID;
        //    setSelectedSeatsRequest.ReturnOrder = true;
        //    setSelectedSeatsRequest.SessionId = SessionID;
        //    setSelectedSeatsRequest.UserSessionId = UserSessionID;
        //    setSelectedSeatsRequest.OptionalClientId = ClientID;
        //    setSelectedSeatsRequest.SelectedSeats = SelectedSeats;

        //    SetSelectedSeatsResponse rp = Ticketing.SetSelectedSeats(setSelectedSeatsRequest);
        //    return rp;
        //}
    }
}
