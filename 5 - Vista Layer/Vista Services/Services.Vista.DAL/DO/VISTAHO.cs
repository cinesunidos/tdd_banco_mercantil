﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace Services.Vista.DAL.DO
{
    public class VISTAHOFilm
    {
        #region Fields
        private string m_MovieID;
        private string m_NacionalFilm;
        private string m_Subtitle;
        private string m_Format;
        private string m_OriginCountry;
        #endregion
        #region Properties
        public string MovieID
        {
            get { return m_MovieID; }
            set { m_MovieID = value; }
        }
        public string NacionalFilm
        {
            get { return m_NacionalFilm; }
            set { m_NacionalFilm = value; }
        }
        public string Subtitle
        {
            get { return m_Subtitle; }
            set { m_Subtitle = value; }
        }
        public string Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public string OriginCountry
        {
            get { return m_OriginCountry; }
            set { m_OriginCountry = value; }
        }
        #endregion
        #region Methods

        #region queryEnCodigo
        //public static List<VISTAHOFilm> GetFilmList(List<string> MovieIDs)
        //{
        //    String Query;
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAHO"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    string StringMovieIDs = getListAsString(MovieIDs);
        //    connection.Open();
        //
        //    List<VISTAHOFilm> films = new List<VISTAHOFilm>();
        //    Query = string.Format(@"SELECT Film_strCode MovieID, Film_strLocal AS NacionalFilm,
        //                    (SELECT tblAttribute_1.Attrib_strShortName
        //                    FROM   VISTAHO.dbo.tblAttribute AS tblAttribute_1 INNER JOIN
        //                            VISTAHO.dbo.tblFilmAttribute AS tblFilmAttribute_1 ON tblAttribute_1.Attrib_strCode = tblFilmAttribute_1.Attrib_strCode
        //                    WHERE  (tblFilmAttribute_1.Film_strCode = pelicula.Film_strCode)) AS Subtitle,                  
        //                    (SELECT  top 1 tblFormat_1.Format_strShortName
        //                    FROM   VISTAHO.dbo.tblFilmFormat AS tblFilmFormat_1 INNER JOIN
        //                            VISTAHO.dbo.tblFormat AS tblFormat_1 ON tblFilmFormat_1.Format_strCode = tblFormat_1.Format_strCode
        //                    WHERE  (tblFilmFormat_1.Film_strCode = pelicula.Film_strCode)) AS Format,         
        //                    (SELECT  Country_strName
        //                    FROM   VISTAHO.dbo.tblCountry AS tblCountry_1
        //                    WHERE  (Country_strCode = pelicula.Country_strCode)) AS OriginCountry
        //            FROM VISTAHO.dbo.tblFilm AS pelicula
        //            WHERE Film_strCode in {0};", StringMovieIDs);
        //
        //    SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
        //
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            VISTAHOFilm film = new VISTAHOFilm();
        //            film.MovieID = dataReader["MovieID"].ToString();
        //            film.NacionalFilm = dataReader["NacionalFilm"].ToString();
        //            film.Subtitle = dataReader["Subtitle"].ToString();
        //            film.Format = dataReader["Format"].ToString();
        //            film.OriginCountry = dataReader["OriginCountry"].ToString();
        //
        //            films.Add(film);
        //        }
        //        dataReader.Close();
        //    }
        //    connection.Close();
        //    return films;
        //}
        #endregion
        #region StoreProcedureOK
        public static List<VISTAHOFilm> GetFilmList(List<string> MovieIDs)
        {
            string conectionstring = ConfigurationManager.ConnectionStrings["VISTAHO"].ConnectionString;
            SqlConnection cn = new SqlConnection(conectionstring);
            string StringMovieIDs = getListAsString(MovieIDs);
            List<VISTAHOFilm> films = new List<VISTAHOFilm>();
            SqlCommand cmd = new SqlCommand("GetFilmList", cn);
            SqlParameter par = cmd.CreateParameter();
            par.ParameterName = "@StringMovieIDs";
            par.Value = StringMovieIDs;
            cmd.Parameters.Add(par);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataReader reader;
            cn.Open();
            reader = cmd.ExecuteReader();
            if (reader != null)
            {
                while (reader.Read())
                {
                    VISTAHOFilm film = new VISTAHOFilm();
                    film.MovieID = reader["MovieID"].ToString();
                    film.NacionalFilm = reader["NacionalFilm"].ToString();
                    film.Subtitle = reader["Subtitle"].ToString();
                    film.Format = reader["Format"].ToString();
                    film.OriginCountry = reader["OriginCountry"].ToString();
                    films.Add(film);
                }
                reader.Close();
            }
            cn.Close();
            return films;
        }
        #endregion
        //metodo original para armar la cadena.
        private static string getListAsString(List<string> list)
        {
            string listInString = "(";
            if (list.Count > 0)
            {
                foreach (string item in list)
                {
                    listInString = listInString + string.Format("'{0}'", item);
                    if (item != list[list.Count - 1]) listInString = listInString + ",";
                }
            }
            else
            {
                listInString = listInString + "''";
            }
            listInString = listInString + ")";

            return listInString;
        }

        #endregion
    }
    public class VISTAHOComingSoon
    {
        #region Fields
        private string m_MovieID;
        private string m_OriginalName;
        private string m_SpanishName;
        private string m_Censor;
        private string m_Duration;
        private string m_OfficialSite;
        private string m_Trailer;
        private string m_FirstExhibit;
        private string m_Synopsis;
        private string m_NacionalFilm;
        private string m_Gender;
        private string m_Subtitle;
        private string m_Format;
        private string m_OriginCountry;
        #endregion
        #region Properties
        public string MovieID
        {
            get { return m_MovieID; }
            set { m_MovieID = value; }
        }
        public string OriginalName
        {
            get { return m_OriginalName; }
            set { m_OriginalName = value; }
        }
        public string SpanishName
        {
            get { return m_SpanishName; }
            set { m_SpanishName = value; }
        }
        public string Censor
        {
            get { return m_Censor; }
            set { m_Censor = value; }
        }
        public string Duration
        {
            get { return m_Duration; }
            set { m_Duration = value; }
        }
        public string OfficialSite
        {
            get { return m_OfficialSite; }
            set { m_OfficialSite = value; }
        }
        public string Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }
        public string Trailer
        {
            get { return m_Trailer; }
            set { m_Trailer = value; }
        }
        public string FirstExhibit
        {
            get { return m_FirstExhibit; }
            set { m_FirstExhibit = value; }
        }
        public string Synopsis
        {
            get { return m_Synopsis; }
            set { m_Synopsis = value; }
        }
        public string NacionalFilm
        {
            get { return m_NacionalFilm; }
            set { m_NacionalFilm = value; }
        }
        public string Subtitle
        {
            get { return m_Subtitle; }
            set { m_Subtitle = value; }
        }
        public string Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public string OriginCountry
        {
            get { return m_OriginCountry; }
            set { m_OriginCountry = value; }
        }
        #endregion
        #region Methods
        #region QueryEnCodigo
        //public static List<VISTAHOComingSoon> GetComingSoonList()
        //{
        //    String Query;
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAHO"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    connection.Open();

        //    List<VISTAHOComingSoon> films = new List<VISTAHOComingSoon>();
        //    Query = @"select Film_strCode MovieID
        //                  ,Film_strTitleAlt OriginalName
        //                  ,Film.[Film_strTitle] SpanishName
        //                  ,Film.[Film_strCensor] Censor
        //                  ,Film.[Film_intDuration] Duration
        //                  ,Film.[Film_strURL1] OfficialSite
        //                  ,Film.[Film_strURLforTrailer] Trailer
        //                  ,Film.[Film_dtmOpeningDate] FirstExhibit
        //                  ,Film.[Film_strDescriptionLong] Synopsis
        //                  ,Film_strLocal NacionalFilm
        //                  ,FilmCat_strName Gender
        //                  ,(SELECT tblAttribute_1.Attrib_strShortName
        //                    FROM   VISTAHO.dbo.tblAttribute AS tblAttribute_1 INNER JOIN
        //                           VISTAHO.dbo.tblFilmAttribute AS tblFilmAttribute_1 ON tblAttribute_1.Attrib_strCode = tblFilmAttribute_1.Attrib_strCode
        //                    WHERE  (tblFilmAttribute_1.Film_strCode = Film.Film_strCode)) AS Subtitle
        //                  ,(SELECT  top 1 tblFormat_1.Format_strShortName
        //                    FROM   VISTAHO.dbo.tblFilmFormat AS tblFilmFormat_1 INNER JOIN
        //                           VISTAHO.dbo.tblFormat AS tblFormat_1 ON tblFilmFormat_1.Format_strCode = tblFormat_1.Format_strCode
        //                    WHERE  (tblFilmFormat_1.Film_strCode = Film.Film_strCode)) AS Format
        //                   ,(SELECT  Country_strName
        //                    FROM   VISTAHO.dbo.tblCountry AS tblCountry_1
        //                    WHERE  (Country_strCode = Film.Country_strCode)) AS OriginCountry
        //            FROM VISTAHO.dbo.tblFilm Film
        //            INNER JOIN [VISTAHO].[dbo].[tblFilmCategory] Cat on Cat.FilmCat_strCode = Film.FilmCat_strCode
        //            WHERE Film_dtmOpeningDate > GETDATE()
        //            and Film_strStatus = 'A'
        //            and Film_strTitleAlt not like 'Partido%'";
        //    SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);

        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            VISTAHOComingSoon film = new VISTAHOComingSoon();
        //            film.MovieID = dataReader["MovieID"].ToString();
        //            film.OriginalName = dataReader["OriginalName"].ToString();
        //            film.SpanishName = dataReader["SpanishName"].ToString();
        //            film.Format = dataReader["Format"].ToString();
        //            film.Censor = dataReader["Censor"].ToString();
        //            film.Duration = dataReader["Duration"].ToString();
        //            film.OfficialSite = dataReader["OfficialSite"].ToString();
        //            film.Gender = dataReader["Gender"].ToString();
        //            film.Trailer = dataReader["Trailer"].ToString();
        //            film.FirstExhibit = dataReader["FirstExhibit"].ToString();
        //            film.Synopsis = dataReader["Synopsis"].ToString();
        //            film.NacionalFilm = dataReader["NacionalFilm"].ToString();
        //            film.Subtitle = dataReader["Subtitle"].ToString();
        //            film.Format = dataReader["Format"].ToString();
        //            film.OriginCountry = dataReader["OriginCountry"].ToString();

        //            films.Add(film);
        //        }
        //        dataReader.Close();
        //    }

        //    connection.Close();
        //    return films;
        //}
        #endregion

        #region StoredProcedureOK
        public static List<VISTAHOComingSoon> GetComingSoonList()
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAHO"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            //RM.
            //string que permite cambiar el tipo de commandtype de la conexion a sql. si se deja vacio este string, entonces el commandtype por defecto es Text. si este string obtiene cualquier valor, entonces el commandType se cambia a Stroredprocedure.
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            connection.Open();

            List<VISTAHOComingSoon> films = new List<VISTAHOComingSoon>();
            SqlDataReader dataReader = connection.ExecuteRead("VISTAHOComingSoon", parameters);

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    VISTAHOComingSoon film = new VISTAHOComingSoon();
                    film.MovieID = dataReader["MovieID"].ToString();
                    film.OriginalName = dataReader["OriginalName"].ToString();
                    film.SpanishName = dataReader["SpanishName"].ToString();
                    film.Format = dataReader["Format"].ToString();
                    film.Censor = dataReader["Censor"].ToString();
                    film.Duration = dataReader["Duration"].ToString();
                    film.OfficialSite = dataReader["OfficialSite"].ToString();
                    film.Gender = dataReader["Gender"].ToString();
                    film.Trailer = dataReader["Trailer"].ToString();
                    film.FirstExhibit = dataReader["FirstExhibit"].ToString();
                    film.Synopsis = dataReader["Synopsis"].ToString();
                    film.NacionalFilm = dataReader["NacionalFilm"].ToString();
                    film.Subtitle = dataReader["Subtitle"].ToString();
                    film.Format = dataReader["Format"].ToString();
                    film.OriginCountry = dataReader["OriginCountry"].ToString();

                    films.Add(film);
                }
                dataReader.Close();
            }

            connection.Close();
            return films;
        }
        #endregion
        #endregion
    }
    public class VISTAHOBookingFee
    {
        #region Fields
        private string m_Code;
        private string m_Description;
        private decimal m_Value;
        #endregion
        #region Properties
        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public decimal Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
        #endregion
        #region Methods
        public static VISTAHOBookingFee GetBookingFee()
        {
            VISTAHOBookingFee BookingFee = new VISTAHOBookingFee();
            BookingFee.Value = Decimal.Parse(ConfigurationManager.AppSettings["BookingFee"].ToString());
            return BookingFee;
        }
        #endregion
    }

    public class VISTAHOService
    {
        #region Properties
        public decimal Price { get; set; }

        public decimal Tax { get; set; }

        public decimal Total { get; set; }
        #endregion

        public VISTAHOService()
        {
            this.Price = 0;
            this.Tax = 0;
            this.Total = 0;
        }

        #region Methods
        public static VISTAHOService GetService(string TicketType, decimal Tax, int SessionId, string TheaterId)
        {
            VISTAHOService Service = new VISTAHOService();
            //List<Package> packages = Package.GetPackageList(TicketType, TheaterId, SessionId.ToString());
            List<Package> packages = Package.RestGetPackageList(TicketType, TheaterId, SessionId.ToString());

            foreach (var package in packages)
            {
                //ESTO SE COMENTO PARA PROBAR JUAN CHOURIO 16/05/2019
                if ((package.PPack_strType == "I") && (package.Item_strItemId.Equals("999999987")))
                {
                    Service.Total = Math.Round(((decimal)Int32.Parse(package.PPack_intPriceEach) / 100), 2) * (decimal)Int32.Parse(package.PPack_intQuantity);
                    Service.Tax = Service.Total - (Service.Total / (1 + (Tax / 100)));
                    Service.Price = Service.Total - Service.Tax;
                    break;
                }
                //ESTO SE COMENTO PARA PROBAR JUAN CHOURIO 16/05/2019
            }
            return Service;
        }
        public static VISTAHOService GetPromotion(string TicketType, decimal Tax, int SessionId, string TheaterId)
        {

            VISTAHOService Promotion = new VISTAHOService();
            List<Package> packages = Package.RestGetPackageList(TicketType, TheaterId, SessionId.ToString());
            foreach (var package in packages)
            {
                if ((package.PPack_strType == "I") && (package.Item_strItemId != ("999999987")))
                {

                    Promotion.Total = Promotion.Total + Math.Round((((decimal)Int32.Parse(package.PPack_intPriceEach) * (decimal)Int32.Parse(package.PPack_intQuantity)) / 100), 2);
                    Promotion.Tax = Promotion.Tax + Promotion.Total - (Promotion.Total / (1 + (Tax / 100)));
                }

            }
            Promotion.Price = Promotion.Price + Promotion.Total - Promotion.Tax;
            return Promotion;
        }
        #endregion
    }
}