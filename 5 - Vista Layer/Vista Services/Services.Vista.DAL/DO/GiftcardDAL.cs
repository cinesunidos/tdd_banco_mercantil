﻿using Services.Vista.BO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class GiftCardDAL
    {

        public string CardType { get; set; }

        public string CardBalance { get; set; }

        public string CardExpiry { get; set; }

        #region Methods
        /// <summary>
        /// Método que devuelve el objeto giftcard con el balance disponible
        /// En caso de no ser una giftcard válida la propiedad IsOk será false
        /// </summary>
        /// <param name="Giftcard">El objeto Giftcard que tendrá la info de la tarjeta</param>
        /// <returns>La Giftcard con su saldo y si es válida está válida</returns>
        /// 

        public static GiftCard GetGiftCardBalance(GiftCard giftCard)
        {

            GiftCardBalanceMethod method = new GiftCardBalanceMethod();
            //Escoger por cual tipo de método se hará la solicitud del servicio
            if (!String.IsNullOrEmpty(giftCard.CardExpiry))
            {
                method = GiftCardBalanceMethod.CardExpiry;
            }
            else if (!String.IsNullOrEmpty(giftCard.CardExpiryMonth) && !String.IsNullOrEmpty(giftCard.CardExpiryYear))
            {
                method = GiftCardBalanceMethod.CardExpiryMY;
            }
            else
            {
                method = GiftCardBalanceMethod.CardNum;
            }

            GiftCardBalanceRequest giftCardRequest = new GiftCardBalanceRequest
            {
                CardNo = giftCard.CardNumber,
                UseGiftCardBalanceMethod = method,
                CardExpiry = giftCard.CardExpiry,
                CardExpiryMonth = giftCard.CardExpiryMonth,
                CardExpiryYear = giftCard.CardExpiryYear,
            };
            List<string> errors = new List<string>();
            try
            {
                giftCard = new GiftCard();
                giftCard = WebConsumer.Get<GiftCard>("http://desaapl02/WSVistaWebClient/RESTData.svc/gift-cards/balance/" + giftCardRequest.CardNo);
                //using (DataService service = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
                //{
                //    //aqui hacver el cambio de la api. llamar al nuevo metodo rest.
                //    DataResponse GiftCardResponse = service.GiftCardBalanceRequest(giftCardRequest);
                //    if (GiftCardResponse.Result == ResultCode.OK)
                //    {
                //        XDocument document = new XDocument();
                //        document = XDocument.Parse(GiftCardResponse.DatasetXML);
                //        giftCard = new GiftCard
                //        {
                //            Balance = Decimal.Parse(document.Descendants("CardBalance").FirstOrDefault().Value) / 100,
                //            IsOk = true,
                //        };
                //        return giftCard;
                //    }
                //    else
                //    {
                //        errors.Add("Ha ocurrido un Error consultando la GiftCard");
                //        giftCard.IsOk = false;
                //    }

                //}

            }
            catch (NullReferenceException)
            {
                errors.Add("El número de GiftCard es inválido");
                giftCard.IsOk = false;
            }
            catch (Exception e)
            {
                errors.Add("Ha Ocurrido un error inesperado");
            }
            giftCard.Messages = errors;
            return giftCard;

        }



        //public static GiftCard GetGiftCardBalance(GiftCard giftCard)
        //{
        //    GiftCardBalanceMethod method = new GiftCardBalanceMethod();
        //    //Escoger por cual tipo de método se hará la solicitud del servicio
        //    if (!String.IsNullOrEmpty(giftCard.CardExpiry))
        //    {
        //        method = GiftCardBalanceMethod.CardExpiry;
        //    }
        //    else if (!String.IsNullOrEmpty(giftCard.CardExpiryMonth) && !String.IsNullOrEmpty(giftCard.CardExpiryYear))
        //    {
        //        method = GiftCardBalanceMethod.CardExpiryMY;
        //    }
        //    else
        //    {
        //        method = GiftCardBalanceMethod.CardNum;
        //    }

        //    GiftCardBalanceRequest giftCardRequest = new GiftCardBalanceRequest
        //    {
        //        CardNo = giftCard.CardNumber,
        //        UseGiftCardBalanceMethod = method,
        //        CardExpiry = giftCard.CardExpiry,
        //        CardExpiryMonth = giftCard.CardExpiryMonth,
        //        CardExpiryYear = giftCard.CardExpiryYear,
        //    };
        //    List<string> errors = new List<string>();
        //    try {
        //        giftCard = new GiftCard();
        //        DataService.DataService service = new DataService.DataService();
        //        DataResponse GiftCardResponse = service.GiftCardBalanceRequest(giftCardRequest);
        //        if (GiftCardResponse.Result == ResultCode.OK)
        //        {
        //            XDocument document = new XDocument();
        //            document = XDocument.Parse(GiftCardResponse.DatasetXML);
        //            giftCard = new GiftCard
        //            {
        //                Balance = Decimal.Parse(document.Descendants("CardBalance").FirstOrDefault().Value) / 100,
        //                IsOk = true,
        //            };
        //            return giftCard;
        //        }
        //        else
        //        {
        //            errors.Add("Ha ocurrido un Error consultando la GiftCard");
        //            giftCard.IsOk = false ;
        //        }
        //    } 
        //    catch (NullReferenceException)
        //    {
        //        errors.Add("El número de GiftCard es inválido");
        //        giftCard.IsOk = false;
        //    }
        //    catch (Exception)
        //    {
        //        errors.Add("Ha Ocurrido un error inesperado");
        //    }
        //    giftCard.Messages = errors;
        //    return giftCard;
        //}
        #endregion
    }

}
