﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public partial class GetSessionSeatDataResponse
    {
        public static GetSessionSeatDataResponse GetSessionSeatData(string ClientID, string UserSessionID, string SessionID, string TheaterID)
        {
            using (DO.TicketingService Ticketing = new DO.TicketingService(ConfigurationManager.AppSettings["url"] + "TicketingService.asmx"))
            {

                GetSessionSeatDataRequest getSessionSeatDataResquest = new GetSessionSeatDataRequest();
                getSessionSeatDataResquest.UserSessionId = UserSessionID;
                getSessionSeatDataResquest.SessionId = SessionID;
                getSessionSeatDataResquest.CinemaId = TheaterID;
                getSessionSeatDataResquest.OptionalClientId = ClientID;
                // getSessionSeatDataResquest.ExcludeAreasWithoutTickets = false;
                getSessionSeatDataResquest.IncludeSeatNumbers = true;
                // getSessionSeatDataResquest.ReturnOrder = true;

                GetSessionSeatDataResponse rp = Ticketing.GetSessionSeatData(getSessionSeatDataResquest);

                return rp;
            }
        }
    }
}
