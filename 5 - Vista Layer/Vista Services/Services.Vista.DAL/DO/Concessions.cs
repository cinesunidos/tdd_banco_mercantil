﻿using System;
using System.Collections.Generic;
using Services.Vista.DAL;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Services.Vista.BO;
using Newtonsoft.Json;

namespace Services.Vista.DAL.DO
{
    #region CU_Properties
    ///Propiedades para el objeto 
    ///
    public class OrderInvConcessions
    {
        public string ClientID { get; set; }
        public string Track_Id { get; set; }
        public int intSequence { get; set; }
        public string ItemCode { get; set; }
        public decimal Quantity { get; set; }
        public string IsBookingFee { get; set; }
    }
    #endregion CU_Properties

    /// <summary>
    /// Desarrollador: Bee Concept
    /// Fecha: 26-04-2017
    /// Descripcion: Clase que implementa los metodos del servicio de Concesiones
    /// </summary>

    public class Concessions
    {


        public class ConcessionItemReturn
        {
            public string ItemStrItemID { get; set; }
            public decimal ItemQuantityPurchase { get; set; }
            public bool ItemValidate { get; set; }
        }
        public static ConcessionOrderResponse AddItemsToCart(string userSessionId, string cinemaId, string sessionId, string transactionIdTemp, ConcessionItem[] concessions)
        {

            var response = new ConcessionOrderResponse();
            RestTicketingService _restTicketingService = new RestTicketingService();
            List<ConcessionItem> productCartItems = new List<ConcessionItem>();
            var locationStrCode = ConfigurationManager.AppSettings["POSLocationCode"]; // "0010"
            decimal TotalValueCents = 0;

            // consultar los productos agregados al carrito
            foreach (var item in concessions)
            {
                var realItem = GetConcessionItem(item.ItemStrItemID, cinemaId);
                if (realItem == null)
                {
                    continue;
                }
                productCartItems.Add(item);
            }

            // cargo web
            var realBookingFee = GetBookingFee(cinemaId);
            var bookingFee = new ConcessionItem();
            bookingFee.ItemStrItemID = realBookingFee.ItemStrItemID;
            bookingFee.ItemMasterItemCode = realBookingFee.ItemMasterItemCode;
            bookingFee.ItemQuantityPurchase = 1;
            productCartItems.Add(bookingFee);

            try
            {
                //var _ticketing = new TicketingService.TicketingService();

                var _ticketing = new TicketingService(ConfigurationManager.AppSettings["url"] + "TicketingService.asmx");
                string OptionalClientId = ConfigurationManager.AppSettings["POSClientId"];
                var UserSessionId = userSessionId.Replace("-", "");
                //RestTicketingService _restTicketingService = new RestTicketingService();

                //var orderRequest = new GetOrderRequest()
                //{
                //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                //    UserSessionId = userSessionId.Replace("-", "")
                //};

                //var orderResponse = _ticketing.GetOrder(orderRequest);

                var orderResponse = RestTicketingService.GetOrder(UserSessionId);

                if (orderResponse.Order.Concessions != null)
                {
                    // eliminar concesiones agregadas anteriormente
                    //var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                    //var removeConcessionsRequest = new RemoveConcessionsRequest
                    //{
                    //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                    //    UserSessionId = userSessionId.Replace("-", ""),
                    //    ConcessionRemovals = concessionsToRemove
                    //};
                    //var removeConcessionsResponse = _ticketing.RemoveConcessions(removeConcessionsRequest);


                    var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                    RemoveConcessionsResponse removeConcessionsResponse = _restTicketingService.RemoveConcessions(OptionalClientId, UserSessionId, concessionsToRemove);

                    if (transactionIdTemp != null)
                    {
                        spRollbackConcessionOrder(cinemaId, transactionIdTemp, "", userSessionId);
                    }
                }

                // agregar nuevas concesiones?
                if (concessions != null && concessions.Length > 0)
                {
                    //var addConcessionsRequest = new AddConcessionsRequest
                    //{
                    //    BookingMode = 0,
                    //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                    //    CinemaId = cinemaId,
                    //    Concessions = ToConcessionArray(productCartItems),
                    //    ProcessOrderValue = true,
                    //    ReturnOrder = true,
                    //    SessionId = sessionId,
                    //    UserSessionId = userSessionId.Replace("-", "")
                    //};


                    //AddConcessionsResponse addConcessionsResponse = _ticketing.AddConcessions(addConcessionsRequest);

                    Concession[] items = ToConcessionArray(productCartItems);

                    AddConcessionsResponse addConcessionsResponse = _restTicketingService.AddConcessions(0, OptionalClientId, cinemaId, items, true, true, sessionId, userSessionId.Replace("-", ""));

                    //addConcessionsResponse.Result = ResultCode.UnexpectedError;

                    if (addConcessionsResponse.Result == ResultCode.OK)
                    {
                        TotalValueCents = (decimal)(addConcessionsResponse.Order.TotalValueCents / 100m);
                    }
                    else if (addConcessionsResponse.Result == ResultCode.UnexpectedError)
                    {
                        throw new Exception("Lo sentimos, se presentó un problema al reservar su(s) producto(s), por favor reinicie la compra");
                    }
                    else
                    {
                        throw new Exception("Los productos seleccionados no están disponibles para la venta.");
                    }

                }

                try
                {
                    // Create Concessions Order
                    ConcessionOrder concessionOrder = CreateConcessionsOrder(cinemaId, concessions);


                    // Update Stock
                    bool updateStockResult = UpdateStock(cinemaId, concessionOrder);

                    if (!updateStockResult)
                    {
                        // remove concessions from Vista
                        //var concessionsToRemove = ToConcessionRemovals(ToConcessionArray(productCartItems));
                        //var removeConcessionsRequest = new RemoveConcessionsRequest
                        //{
                        //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                        //    UserSessionId = userSessionId.Replace("-", ""),
                        //    ConcessionRemovals = concessionsToRemove
                        //};
                        //var removeConcessionsResponse = _ticketing.RemoveConcessions(removeConcessionsRequest);

                        var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                        RemoveConcessionsResponse removeConcessionsResponse = _restTicketingService.RemoveConcessions(OptionalClientId, UserSessionId, concessionsToRemove);
                        //throw new Exception("Los productos seleccionados no están disponibles para la venta.");
                        throw new Exception("Lo sentimos, no hay disponibilidad para la cantidad de producto(s) seleccionado(s).");
                    }
                    else
                    {
                        //guardar en la tabla tblCUtblOrderTrackConcessions
                        bool rsp = CreateOrderTrackConcessions(userSessionId.Replace("-", ""), TotalValueCents, ConfigurationManager.AppSettings["POSClientId"].ToString(), "N", cinemaId);

                        if (rsp)
                        {
                            List<OrderInvConcessions> items = CreateOrderInv(userSessionId.Replace("-", ""), ConfigurationManager.AppSettings["POSClientId"].ToString(), concessionOrder);

                            bool rsponce = spCreateOrdenInvConcessions(userSessionId.Replace("-", ""), items, cinemaId);
                            if (!rsponce)
                                throw new Exception("Lo sentimos, hubo un error al procesar su pedido, por favor intente nuevamente");

                        }
                        else
                        {
                            throw new Exception("Lo sentimos, hubo un error al procesar su pedido, por favor intente nuevamente");
                        }
                    }

                    response.Order = concessionOrder;
                    response.Order.BookingFee = realBookingFee;

                    return response;
                }
                catch (Exception ex)
                {
                    // cancelar la orden en el webservice de VISTA
                    //var concessionsToRemove = ToConcessionRemovals(productCartItems);

                    //var removeConcessionsRequest = new RemoveConcessionsRequest
                    //{
                    //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                    //    ReturnOrder = false,
                    //    UserSessionId = userSessionId.Replace("-", ""),
                    //    ConcessionRemovals = concessionsToRemove
                    //};
                    //var removeConcessionsResponse = _ticketing.RemoveConcessions(removeConcessionsRequest);

                    var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                    RemoveConcessionsResponse removeConcessionsResponse = _restTicketingService.RemoveConcessions(OptionalClientId, UserSessionId, concessionsToRemove);
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                var errorResponse = new ConcessionOrderResponse();
                errorResponse.WithError = true;
                errorResponse.ErrorMessage = ex.Message;
                errorResponse.Order = null;
                return errorResponse;
            }

        }

        private static ConcessionOrder CreateConcessionsOrder(string cinemaId, ConcessionItem[] concessions)
        {
            var connectionString = "VISTA_" + cinemaId;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            connection.Open();

            SqlTransaction transaction = connection.BeginTransaction("CreateConcessionsOrder");

            ConcessionOrder order = new ConcessionOrder();
            order.PaymentOrderTotal = 0;
            order.ClientId = ConfigurationManager.AppSettings["POSClientId"]; // "111.111.111.113"
            order.LocationStrCode = ConfigurationManager.AppSettings["POSLocationCode"]; // "0030"
            order.Items = new List<ConcessionItem>();

            foreach (ConcessionItem item in concessions)
            {
                var realItem = GetConcessionItemFull(item.ItemStrItemID, cinemaId);
                realItem.ItemQuantityPurchase = item.ItemQuantityPurchase;
                order.PaymentOrderTotal += Decimal.Multiply(item.ItemPriceDCurPrice, item.ItemQuantityPurchase);
                realItem.Ingredients = new List<ConcessionItem>();
                var ingredients = GetProductRecipe(cinemaId, item.ItemStrItemID, order.LocationStrCode);
                foreach (var ingredient in ingredients)
                {
                    realItem.Ingredients.Add(ingredient);
                }
                order.Items.Add(realItem);
            }

            try
            {

                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Transaction = transaction;

                    command.CommandText = "spCU_CreateConcessionsOrder";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@ClientId", SqlDbType.VarChar).Value = order.ClientId;
                    command.Parameters.Add("@PaymentOrderTotal", SqlDbType.Decimal).Value = order.PaymentOrderTotal;
                    command.Parameters.Add("@OrderValueNet", SqlDbType.Decimal).Value = 0;
                    command.Parameters.Add("@Location_strCode", SqlDbType.VarChar).Value = order.LocationStrCode;
                    command.Parameters.Add("@TransIdTemp", SqlDbType.VarChar, 20);
                    command.Parameters["@TransIdTemp"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();
                    order.TransIdTemp = Convert.ToString(command.Parameters["@TransIdTemp"].Value.ToString());
                }

                transaction.Commit();
                connection.Close();
                return order;
            }
            catch (Exception ex)
            {
                try
                {
                    transaction.Rollback();
                }
                catch (Exception ex2)
                {

                }
            }

            return null;
        }

        private static bool UpdateStock(string cinemaId, ConcessionOrder order)
        {
            var connectionString = "VISTA_" + cinemaId;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            connection.Open();
            SqlTransaction transaction = connection.BeginTransaction("UpdateStock");

            try
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.Transaction = transaction;

                    command.CommandText = "spCU_ConcessionsUpdateStock";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@TransIdTemp", SqlDbType.VarChar).Value = order.TransIdTemp;
                    command.Parameters.Add("@ClientId", SqlDbType.VarChar).Value = order.ClientId;
                    command.Parameters.Add("@Location_strCode", SqlDbType.VarChar).Value = order.LocationStrCode;

                    // Concession Items
                    DataTable table = new DataTable();
                    table.Columns.Add("ItemStrItemID", typeof(string));
                    table.Columns.Add("ItemPriceDCurPrice", typeof(decimal));
                    table.Columns.Add("ItemQuantityPurchase", typeof(decimal));
                    table.Columns.Add("ItemRecipe", typeof(string));

                    foreach (ConcessionItem item in order.Items)
                    {
                        if (item.IsRecipe)
                        {
                            var row = table.NewRow();
                            row["ItemStrItemID"] = item.ItemStrItemID;
                            row["ItemPriceDCurPrice"] = item.ItemPriceDCurPrice;
                            row["ItemQuantityPurchase"] = item.ItemQuantityPurchase;
                            row["ItemRecipe"] = "N";
                            table.Rows.Add(row);
                        }
                        else
                        {
                            var row = table.NewRow();
                            row["ItemStrItemID"] = item.ItemStrItemID;
                            row["ItemPriceDCurPrice"] = item.ItemPriceDCurPrice;
                            row["ItemQuantityPurchase"] = item.ItemQuantityPurchase;
                            row["ItemRecipe"] = "Y";
                            table.Rows.Add(row);
                            foreach (ConcessionItem ingredient in item.Ingredients)
                            {
                                row = table.NewRow();
                                row["ItemStrItemID"] = ingredient.ItemStrItemID;
                                row["ItemPriceDCurPrice"] = ingredient.ItemPriceDCurPrice;
                                row["ItemQuantityPurchase"] = Decimal.Multiply(item.ItemQuantityPurchase, ingredient.RecipeQuantity);
                                row["ItemRecipe"] = "N";
                                table.Rows.Add(row);
                            }
                        }
                    }

                    command.Parameters.Add("@ConcessionItems", SqlDbType.Structured).Value = table;

                    command.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    transaction.Rollback();
                }
                catch (Exception ex2)
                {

                }
            }

            return false;
        }


        public static bool PurchaseConcessions(string transactionIdTemp, string transactionNumber, string bookingNumber,
            string userSessionId, string cinemaId, string sessionId, ConcessionItem[] concessions)
        {

            var connectionString = "VISTA_" + cinemaId;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            connection.Open();
            SqlTransaction transaction = connection.BeginTransaction("PlaceItemsTransaction");

            var locationStrCode = ConfigurationManager.AppSettings["POSLocationCode"]; // "0030"

            try
            {
                // find start sequence
                int sequence = spGetMaxSequence(connection, transaction, transactionNumber);
                int parentSequence = sequence;

                /*
                 * Este metodo esta comentado, debido a que el webservice de CompleteOrder (VISTA)
                 * ya inserta los items comprados (y los ingredientes usados) en la tabla `tblTrans_Inventory`.
                 * Por lo tanto, no hace falta que lo manejemos nosotros.
                foreach (var item in concessions)
                {
                    if( item.ItemType == "S" )
                    {
                        // services are already present...
                        continue;
                    }
                    parentSequence = sequence++;
                    spPurchaseRecipe(conn, transaction, transactionNumber, userSessionId, cinemaId, sessionId, item, parentSequence);
                    var ingredients = GetProductRecipe(cinemaId, item.ItemStrItemID, locationStrCode);
                    foreach (var ingredient in ingredients)
                    {
                        spPurchaseIngredient(conn, transaction, transactionNumber, userSessionId, cinemaId, sessionId, item, ingredient, sequence++, parentSequence);
                    }
                }
                */

                // Update Order Status
                UpdateConcessionsOrder(
                    cinemaId,           // Theather Id
                    transactionIdTemp,  // Temp Id
                    transactionNumber,  // Transaction Number
                    bookingNumber,      // Booking Number
                    true,               // Payment Started 
                    true,               // Payment OK
                    true,               // Order Completed
                    false               // Order Cancelled
                );

                transaction.Commit();
                connection.Close();
            }
            catch (Exception ex)
            {
                try
                {
                    transaction.Rollback();
                    connection.Close();
                }
                catch (Exception ex2)
                {

                }
            }
            return true;
        }

        private static int spGetMaxSequence(SqlConnection conn, SqlTransaction transaction, string transactionNumber)
        {
            var sql = @"SELECT (MAX(TransI_intSequence)+1) AS seq 
                FROM tblTrans_Inventory WITH (NOLOCK) 
                WHERE TransI_lgnNumber = @TransNo";
            using (var command = new SqlCommand(sql))
            {
                command.Connection = conn;
                command.Transaction = transaction;
                command.Parameters.Add("@TransNo", SqlDbType.Char).Value = Convert.ToInt32(transactionNumber);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            int result = Convert.ToInt32(reader["seq"]);
                            return result;
                        }
                    }
                }
            }
            return 1;
        }

        private static bool spPurchaseRecipe(SqlConnection conn, SqlTransaction transaction, string transactionId, string userSessionId, string cinemaId, string sessionId, ConcessionItem item, int sequence)
        {

            using (var command = new SqlCommand())
            {
                command.Connection = conn;
                command.Transaction = transaction;

                command.CommandText = "spCU_ConcessionsPurchaseRecipe";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@TransI_lgnNumber", SqlDbType.VarChar).Value = transactionId;
                command.Parameters.Add("@TransI_intSequence", SqlDbType.Int).Value = sequence;
                command.Parameters.Add("@TransI_strType", SqlDbType.Char).Value = "S";
                command.Parameters.Add("@TransI_strStatus", SqlDbType.VarChar).Value = "";
                command.Parameters.Add("@Item_strItemId", SqlDbType.VarChar).Value = item.ItemStrItemID;
                command.Parameters.Add("@TransI_decNoOfItems", SqlDbType.Decimal).Value = item.ItemQuantityPurchase;
                command.Parameters["@TransI_decNoOfItems"].Precision = 28;
                command.Parameters["@TransI_decNoOfItems"].Scale = 13;
                command.Parameters.Add("@TransI_decActualNoOfItems", SqlDbType.Decimal).Value = item.ItemQuantityPurchase;
                command.Parameters["@TransI_decActualNoOfItems"].Precision = 28;
                command.Parameters["@TransI_decActualNoOfItems"].Scale = 13;
                command.Parameters.Add("@TransI_curSTaxEach", SqlDbType.Decimal).Value = 0;
                command.Parameters.Add("@TransI_curValueEach", SqlDbType.Decimal).Value = item.ItemPriceDCurPrice;
                command.Parameters.Add("@TransI_curNetTotal", SqlDbType.Decimal).Value = item.ItemPriceDCurPrice;
                command.Parameters.Add("@TransI_curNetTotal", SqlDbType.Int).Value = 20;
                command.Parameters.Add("@TransI_strActualUOM", SqlDbType.VarChar).Value = item.ItemStockUOMCode != null ? item.ItemStockUOMCode : "";
                command.Parameters.Add("@TransI_curDiscount", SqlDbType.Decimal).Value = 0;
                command.Parameters.Add("@DiscountR_strRptCode", SqlDbType.VarChar).Value = "";
                command.Parameters.Add("@TransI_curFullPrice", SqlDbType.Decimal).Value = 0;
                command.Parameters.Add("@PriceH_strCode", SqlDbType.VarChar).Value = "";
                command.Parameters.Add("@TransI_strOriginalItemId", SqlDbType.VarChar).Value = "";

                // Configurable parameters -> Web.config
                command.Parameters.Add("@Location_strCode", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["POSLocationCode"]; // "0010";
                command.Parameters.Add("@User_intUserNo", SqlDbType.Int).Value = ConfigurationManager.AppSettings["POSUserId"];
                command.Parameters.Add("@ClientId", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["POSClientId"]; // "111.111.111.113"
                command.Parameters.Add("@TransI_lngPOSSessionID", SqlDbType.Int).Value = DBNull.Value;


                command.ExecuteNonQuery();

                return true;
            }
        }


        private static bool spPurchaseIngredient(SqlConnection conn, SqlTransaction transaction, string transactionId, string userSessionId, string cinemaId, string sessionId, ConcessionItem sellableItem, ConcessionItem item, int sequence, int parentSequence)
        {

            using (var command = new SqlCommand())
            {
                command.Connection = conn;
                command.Transaction = transaction;
                command.CommandText = "spCU_ConcessionsPurchaseIngredient";
                command.CommandType = CommandType.StoredProcedure;

                // id transaccion
                command.Parameters.Add("@TransI_lgnNumber", SqlDbType.VarChar).Value = transactionId;
                // num secuencia: ingrediente
                command.Parameters.Add("@TransI_intSequence", SqlDbType.Int).Value = sequence;
                // num secuencia: receta
                command.Parameters.Add("@TransI_intParentSeq", SqlDbType.Int).Value = parentSequence;
                // type: Componente Usado
                command.Parameters.Add("@TransI_strType", SqlDbType.Char).Value = "C";
                // Item Id
                command.Parameters.Add("@Item_strItemId", SqlDbType.VarChar).Value = item.ItemStrItemID;
                // Cantidad de Ingrediente Usado
                command.Parameters.Add("@TransI_decNoOfItems", SqlDbType.Decimal).Value = Decimal.Multiply(sellableItem.ItemQuantityPurchase, item.RecipeQuantity);
                command.Parameters["@TransI_decNoOfItems"].Precision = 28;
                command.Parameters["@TransI_decNoOfItems"].Scale = 13;
                command.Parameters.Add("@TransI_decActualNoOfItems", SqlDbType.Decimal).Value = Decimal.Multiply(sellableItem.ItemQuantityPurchase, item.RecipeQuantity);
                command.Parameters["@TransI_decActualNoOfItems"].Precision = 28;
                command.Parameters["@TransI_decActualNoOfItems"].Scale = 13;
                // Sellable Item Id
                command.Parameters.Add("@TransI_strSellableItemId", SqlDbType.VarChar).Value = sellableItem.ItemStrItemID;
                // Sellable Quantity
                command.Parameters.Add("@TransI_curSellableQty", SqlDbType.Decimal).Value = sellableItem.RecipeQuantity;
                // Sellable Price
                command.Parameters.Add("@TransI_curSellablePriceEach", SqlDbType.Decimal).Value = sellableItem.ItemPriceDCurPrice;
                // Sellable Taxes
                command.Parameters.Add("@TransI_curSellableSTax", SqlDbType.Decimal).Value = 0;

                command.Parameters.Add("@TransI_strStatus", SqlDbType.Char).Value = "";
                command.Parameters.Add("@TransI_strParentClassCode", SqlDbType.VarChar).Value = "20";
                command.Parameters.Add("@TransI_strActualUOM", SqlDbType.VarChar).Value = item.ItemStockUOMCode != null ? item.ItemStockUOMCode : "";
                command.Parameters.Add("@TransI_curDiscount", SqlDbType.Decimal).Value = 0;
                command.Parameters.Add("@DiscountR_strRptCode", SqlDbType.VarChar).Value = "";

                // Configurable parameters -> Web.config
                command.Parameters.Add("@Location_strCode", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["POSLocationCode"]; ;
                command.Parameters.Add("@ClientId", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["POSClientId"];
                command.Parameters.Add("@User_intUserNo", SqlDbType.SmallInt).Value = ConfigurationManager.AppSettings["POSUserId"];
                command.Parameters.Add("@TransI_lngPOSSessionID", SqlDbType.Int).Value = DBNull.Value;

                command.ExecuteNonQuery();

                return true;
            }

        }

        public static bool UpdateConcessionsOrder(string theaterId, string transactionId, string transactionNumber, string bookingNumber, bool paymentStart, bool paymentOk, bool orderComplete, bool orderCancel)
        {
            var connectionString = "VISTA_" + theaterId;
            bool rsp = false;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());
            try
            {
                conn.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "spCU_UpdateConcessionsOrder";
                    command.CommandType = CommandType.StoredProcedure;

                    var TransIdTemp = command.CreateParameter();
                    TransIdTemp.ParameterName = "@TransIdTemp";
                    TransIdTemp.Value = transactionId;
                    command.Parameters.Add(TransIdTemp);

                    command.Parameters.Add("@TransNo", SqlDbType.Char).Value = Convert.ToInt32(transactionNumber);
                    command.Parameters.Add("@BookingNo", SqlDbType.Char).Value = Convert.ToInt32(bookingNumber);
                    command.Parameters.Add("@PaymentStart", SqlDbType.Char).Value = paymentStart ? 'Y' : 'N';
                    command.Parameters.Add("@PaymentOK", SqlDbType.Char).Value = paymentOk ? 'Y' : 'N';
                    command.Parameters.Add("@OrderComplete", SqlDbType.Char).Value = orderComplete ? 'Y' : 'N';
                    command.Parameters.Add("@OrderCancel", SqlDbType.Char).Value = orderCancel ? 'Y' : 'N';

                    command.ExecuteNonQuery();

                    rsp = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Lo sentimos, actualmente hemos perdido la conexión con el cine.Por favor intente nuevamente.");
            }
            finally
            {
                conn.Close();
            }
            return rsp;
        }

        public static List<ConcessionItem> GetProductRecipe(string theaterId, string itemId, string locationCode)
        {

            var connectionString = "VISTA_" + theaterId;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());


            var listProducts = new List<ConcessionItem>();

            using (var command = new SqlCommand())
            {
                command.Connection = conn;

                command.CommandText = "spCU_GetConcessionsRecipeByProduct";
                command.CommandType = CommandType.StoredProcedure;

                var locationStrCode = command.CreateParameter();
                locationStrCode.ParameterName = "@Location_strCode";
                locationStrCode.Value = locationCode;
                command.Parameters.Add(locationStrCode);

                var Item_strItemId = command.CreateParameter();
                Item_strItemId.ParameterName = "@Item_strItemId";
                Item_strItemId.Value = itemId;
                command.Parameters.Add(Item_strItemId);

                conn.Open();

                using (var reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        // transaction
                        var product = new ConcessionItem();
                        product.ItemStrItemID = (string)(reader["HOPK3"]);
                        product.ItemStrItemDescription = (string)reader["Item_strItemDescription"];
                        product.ItemHOPK1 = (string)reader["HOPK1"];
                        product.ItemHOPK2 = (string)reader["HOPK2"];
                        product.ItemHOPK3 = (string)reader["HOPK3"];
                        product.ItemPriceDCurPrice = Convert.ToDecimal(reader["PriceD_curPrice"]);
                        product.RecipeQuantity = Convert.ToDecimal(reader["Quantity"]);
                        product.ItemStockUOMCode = (string)reader["Item_strStockUOMCode"];
                        product.ItemType = (string)reader["Item_strItemType"];
                        product.VendorStrCode = (string)reader["Vendor_strCode"];
                        product.ClassStrCode = (string)reader["Class_strCode"];

                        listProducts.Add(product);
                    }

                }

                conn.Close();
            }

            return listProducts;

        }

        public static ConcessionItem GetConcessionItem(string itemId, string theaterId)
        {
            var connectionString = "VISTA_" + theaterId;

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());
            try
            {
                connection.Open();

                var sql = @"SELECT Item.*, Stock.*
                FROM tblItem AS Item
                LEFT JOIN tblStock_Status AS Stock(NOLOCK) ON Item.Item_strItemId = Stock.Item_strItemId
                WHERE Item.Item_strItemId = @ItemId";

                using (var command = new SqlCommand(sql))
                {
                    command.Connection = connection;
                    command.Parameters.Add("@ItemId", SqlDbType.VarChar).Value = itemId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                var concession = new ConcessionItem();
                                concession.ItemStrItemID = (string)reader["Item_strItemId"];
                                concession.ItemHOPK1 = (string)reader["HOPK"];
                                concession.ItemMasterItemCode = (string)reader["Item_strMasterItemCode"];
                                concession.ItemStrItemDescription = ToTitleCaseString((string)reader["Item_strItemDescription"]);
                                concession.ItemStockCurAvailable = reader["Stock_curAvailable"] != DBNull.Value ? Convert.ToDecimal(reader["Stock_curAvailable"]) : 0;
                                concession.ItemStockUOMCode = (string)reader["Item_strStockUOMCode"];
                                concession.ItemType = (string)reader["Item_strItemType"];
                                concession.VendorStrCode = (string)reader["Vendor_strCode"];
                                concession.ClassStrCode = (string)reader["Class_strCode"];
                                concession.RecipeQuantity = 0;
                                return concession;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Lo sentimos, actualmente hemos perdido la conexión con el cine. Por favor intente nuevamente.");
            }

            return null;
        }

        public static ConcessionItem GetConcessionItemFull(string itemId, string theaterId)
        {
            var connectionString = "VISTA_" + theaterId;
            var locationStrCode = ConfigurationManager.AppSettings["POSLocationCode"];

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            connection.Open();

            var sql = @"SELECT Item.*, Stock.Stock_curAvailable, PriceDetails.PriceD_curPrice
                FROM tblItem AS Item
                LEFT JOIN tblStock_Status AS Stock(NOLOCK) ON Item.Item_strItemId = Stock.Item_strItemId
                LEFT JOIN tblPriceListDetails AS PriceDetails(NOLOCK) ON Item.Item_strItemId = PriceDetails.Item_strItemId
	            LEFT JOIN tblPriceListHeader AS PriceHeader (NOLOCK)  ON PriceHeader.PriceH_strCode = PriceDetails.PriceH_strCode
                WHERE Item.Item_strItemId = @ItemId
                    AND Stock.Location_strCode = @LocationStrCode
                    AND PriceDetails.PriceD_strStatus = 'A' 
		            AND PriceHeader.PriceH_strStatus = 'A'";
            using (var command = new SqlCommand(sql))
            {
                command.Connection = connection;
                command.Parameters.Add("@ItemId", SqlDbType.VarChar).Value = itemId;
                command.Parameters.Add("@LocationStrCode", SqlDbType.VarChar).Value = locationStrCode;
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            var concession = new ConcessionItem();
                            concession.ItemStrItemID = (string)reader["Item_strItemId"];
                            concession.ItemHOPK1 = (string)reader["HOPK"];
                            concession.ItemMasterItemCode = (string)reader["Item_strMasterItemCode"];
                            concession.ItemStrItemDescription = ToTitleCaseString((string)reader["Item_strItemDescription"]);
                            concession.ItemStockCurAvailable = reader["Stock_curAvailable"] != DBNull.Value ? Convert.ToDecimal(reader["Stock_curAvailable"]) : 0;
                            concession.ItemPriceDCurPrice = reader["PriceD_curPrice"] != DBNull.Value ? Convert.ToDecimal(reader["PriceD_curPrice"]) : 0;
                            concession.ItemStockUOMCode = (string)reader["Item_strStockUOMCode"];
                            concession.ItemType = (string)reader["Item_strItemType"];
                            concession.VendorStrCode = (string)reader["Vendor_strCode"];
                            concession.ClassStrCode = (string)reader["Class_strCode"];
                            concession.RecipeQuantity = 0;
                            return concession;
                        }
                    }
                }
            }
            return null;
        }

        public static ConcessionListResponse GetTheaterConcessions(string theaterId)
        {
            var response = new ConcessionListResponse();
            response.Items = spGetConcessionsByTheater(theaterId);
            response.BookingFee = GetBookingFee(theaterId);
            return response;
        }

        public static string PromoPepsi(string Codigo)
        {
            return Codigo;
        }

        private static List<ConcessionItem> spGetConcessionsByTheater(string theaterId)
        {
            var connectionString = "VISTA_" + theaterId;

            List<ConcessionItem> listConcessions = new List<ConcessionItem>();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            using (var command = new SqlCommand())
            {
                command.Connection = conn;
                command.CommandText = "spCU_GetConcessionsByTheater";
                command.CommandType = CommandType.StoredProcedure;

                var locationStrCode = command.CreateParameter();
                locationStrCode.ParameterName = "@Location_strCode";
                locationStrCode.Value = ConfigurationManager.AppSettings["POSLocationCode"];
                command.Parameters.Add(locationStrCode);

                conn.Open();

                using (var reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        var concession = new ConcessionItem();
                        concession.ItemStrItemID = (string)reader["Item_strItemId"];
                        concession.ItemHOPK1 = (string)reader["HOPK1"];
                        concession.ItemHOPK2 = (string)reader["HOPK2"];
                        concession.ItemHOPK3 = (string)reader["HOPK3"];
                        //concession.ItemStrItemDescription = ToTitleCaseString((string)reader["ItemDescriptionL1"]);
                        concession.ItemStrItemDescription = ToTitleCaseString((string)reader["Item_strItemDescriptionL1"]);
                        concession.ItemPriceDCurPrice = Convert.ToDecimal(reader["PriceD_curPrice"]);
                        concession.ItemStockCurAvailable = Convert.ToDecimal(reader["Stock_curAvailable"]);
                        concession.ItemStockUOMCode = (string)reader["Item_strStockUOMCode"];
                        concession.ItemType = (string)reader["Item_strItemType"];
                        concession.VendorStrCode = (string)reader["Vendor_strCode"];
                        concession.ClassStrCode = (string)reader["Class_strCode"];
                        concession.RecipeQuantity = Convert.ToDecimal(reader["Quantity"]);

                        listConcessions.Add(concession);
                    }

                }

                conn.Close();

            }

            return listConcessions;
        }

        private static ConcessionItem GetBookingFee(string theaterId)
        {
            string itemId = ConfigurationManager.AppSettings["WebBookingItemId"];
            decimal value = Convert.ToDecimal(ConfigurationManager.AppSettings["WebBookingPrice"].ToString());
            var bookingFee = GetConcessionItem(itemId, theaterId);
            bookingFee.ItemPriceDCurPrice = value;
            return bookingFee;
        }

        public static bool CancelConcessionsOrder(string userSessionId, string theaterId, string transactionId, string TransactionNumber)
        {

            // TODO: eliminar las concesiones agregadas en el Web Service de VISTA
            var _ticketing = new TicketingService(ConfigurationManager.AppSettings["url"] + "TicketingService.asmx");
            string OptionalClientId = ConfigurationManager.AppSettings["POSClientId"];
            var UserSessionId = userSessionId.Replace("-", "");
            RestTicketingService _restTicketingService = new RestTicketingService();


            //var orderRequest = new GetOrderRequest()
            //{
            //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"].ToString(),
            //    UserSessionId = userSessionId.Replace("-", "")
            //};

            //var orderResponse = _ticketing.GetOrder(orderRequest);

            var orderResponse = RestTicketingService.GetOrder(UserSessionId);

            if (orderResponse.Order.Concessions != null)
            {
                // eliminar concesiones agregadas anteriormente
                //var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                //var removeConcessionsRequest = new RemoveConcessionsRequest
                //{
                //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                //    UserSessionId = userSessionId.Replace("-", ""),
                //    ConcessionRemovals = concessionsToRemove
                //};
                //var removeConcessionsResponse = _ticketing.RemoveConcessions(removeConcessionsRequest);


                var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                RemoveConcessionsResponse removeConcessionsResponse = _restTicketingService.RemoveConcessions(OptionalClientId, UserSessionId, concessionsToRemove);
            }

            return spRollbackConcessionOrder(theaterId, transactionId, TransactionNumber, userSessionId);
        }

        private static bool spRollbackConcessionOrder(string theaterId, string transactionId, string TransactionNumber, string userSessionId)
        {
            bool value = false;
            var connectionString = "VISTA_" + theaterId;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            try
            {
                conn.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "spCU_RollbackConcessionsOrder";
                    command.CommandType = CommandType.StoredProcedure;

                    var TransIdTemp = command.CreateParameter();
                    TransIdTemp.ParameterName = "@TransIdTemp";
                    TransIdTemp.Value = transactionId;
                    command.Parameters.Add(TransIdTemp);

                    var locationStrCode = command.CreateParameter();
                    locationStrCode.ParameterName = "@Location_strCode";
                    locationStrCode.Value = ConfigurationManager.AppSettings["POSLocationCode"]; // "0010"
                    command.Parameters.Add(locationStrCode);

                    var userSession = command.CreateParameter();
                    userSession.ParameterName = "@UserSessionId";
                    userSession.Value = userSessionId.Replace("-", "");
                    command.Parameters.Add(userSession);

                    command.ExecuteNonQuery();


                    #region eliminar sesion de las tablas tblCUOrderTrackConcessions y tblCUOrdenInvConcessions 
                    command.Dispose();
                    // Se aplica este condicional para eliminar registros de las tablas tblCUOrderTrackConcessions y tblCUOrdenInvConcessions, esto se hace para que el job no 
                    // cargue nuevamente el inventario de los productos seleccionados
                    //Eliminar Items de la tabla tblCUOrderInvConcessions
                    command.Connection = conn;
                    command.CommandText = String.Format("DELETE FROM tblCUOrdenInvConcessions WHERE Track_Id='{0}'", userSessionId.Replace("-", ""));
                    command.CommandType = CommandType.Text;

                    command.ExecuteNonQuery();
                    command.Dispose();

                    //Eliminar track de la tabla tblCUOrderTrackConcessions
                    command.Connection = conn;
                    command.CommandText = String.Format("DELETE FROM tblCUOrderTrackConcessions WHERE Track_Id='{0}'", userSessionId.Replace("-", ""));
                    command.CommandType = CommandType.Text;

                    command.ExecuteNonQuery();
                    #endregion


                    if (!string.IsNullOrEmpty(TransactionNumber))
                    {
                        //Liberar recurso
                        command.Dispose();

                        var query = string.Format(@"UPDATE 
                                                        tblTrans_Inventory SET TransI_dtmDateCollected = '9999-01-01 00:00:00.000' 
                                                    WHERE 
                                                        TransI_lgnNumber = {0} 
                                                        AND Location_strCode='0010'  
                                                        --AND TransI_dtmDateCollected IS NULL
                                                        AND Item_strItemId NOT IN ('999999987','999999999','999999998')", TransactionNumber);
                        command.Connection = conn;
                        command.CommandText = query;
                        command.CommandType = CommandType.Text;

                        command.ExecuteNonQuery();

                    }

                    return true;
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }

            return false;

        }


        private static string ToTitleCaseString(string description)
        {
            return CultureInfo.InvariantCulture.TextInfo.ToTitleCase(description.ToLowerInvariant().Trim())
                    .Replace("_", "")
                    .Replace("-", "")
                    .Replace(".", "");
        }


        private static Concession[] ToConcessionArray(List<ConcessionItem> items)
        {
            var qty = items.Count();
            var array = new Concession[qty];
            int x = 0;
            foreach (var item in items)
            {
                var c = new Concession();
                c.ItemId = item.ItemStrItemID;
                c.HeadOfficeItemCode = item.ItemMasterItemCode;
                c.Quantity = Convert.ToInt32(item.ItemQuantityPurchase);
                array[x] = c;
                x++;
            }
            return array;
        }

        private static ConcessionRemoval[] ToConcessionRemovals(List<ConcessionItem> items)
        {
            var qty = items.Count();
            var array = new ConcessionRemoval[qty];
            int x = 0;
            foreach (var item in items)
            {
                var c = new ConcessionRemoval();
                c.ItemId = item.ItemStrItemID;
                c.Quantity = Convert.ToInt32(item.ItemQuantityPurchase);
                array[x] = c;
                x++;
            }
            return array;
        }

        private static ConcessionRemoval[] ToConcessionRemovals(Concession[] concessions)
        {
            var qty = concessions.Count();
            var array = new ConcessionRemoval[qty];
            int x = 0;
            foreach (var item in concessions)
            {
                var c = new ConcessionRemoval();
                c.ItemId = item.ItemId;
                c.Quantity = item.Quantity;
                array[x] = c;
                x++;
            }
            return array;
        }

        public static ConcessionListResponse GetCandiesWeb(string theaterId)
        {
            var response = new ConcessionListResponse();
            response.Items = GetConcessionsCandiesWeb(theaterId);
            response.BookingFee = GetBookingFee(theaterId);
            return response;
        }

        public static ConcessionOrderResponse AddItemsToCartCandiesWeb(string userSessionId, string cinemaId, string sessionId, string transactionIdTemp, ConcessionItem[] concessions)
        {
            var response = new ConcessionOrderResponse();

            List<ConcessionItem> productCartItems = new List<ConcessionItem>();
            var locationStrCode = ConfigurationManager.AppSettings["POSLocationCode"]; // "0010"
            decimal TotalValueCents = 0;
            string OptionalClientId = ConfigurationManager.AppSettings["POSClientId"];
            var UserSessionId = userSessionId.Replace("-", "");
            RestTicketingService _restTicketingService = new RestTicketingService();

            foreach (var item in concessions)
            {
                var realItem = GetConcessionItem(item.ItemStrItemID, cinemaId);
                if (realItem == null)
                {
                    continue;
                }
                productCartItems.Add(item);
            }

            List<ConcessionItem> listConcessions = new List<ConcessionItem>();

            var serviceResponse = GetCandiesWeb(cinemaId);

            //RECORRER LA LISTA PEQUEÑA EN LUGAR DE RECORRER LA GRANDE : tO dO
            foreach (var item in serviceResponse.Items)
            {
                foreach (var item2 in concessions)
                {
                    if (item.ItemStrItemID == item2.ItemStrItemID)
                    {
                        if ((item.ItemStockCurAvailable > 0) && (item2.ItemQuantityPurchase <= item.ItemStockCurAvailable))
                        {
                            item2.ItemStockCurAvailable = item.ItemStockCurAvailable;
                        }
                        else
                        {
                            throw new Exception("Lo sentimos, no hay disponibilidad para la cantidad de producto(s) seleccionado(s).");
                        }
                    }
                }
            }

            var realBookingFee = GetBookingFee(cinemaId);
            var bookingFee = new ConcessionItem();
            bookingFee.ItemStrItemID = realBookingFee.ItemStrItemID;
            bookingFee.ItemMasterItemCode = realBookingFee.ItemMasterItemCode;
            bookingFee.ItemQuantityPurchase = 1;
            bookingFee.ItemPriceDCurPrice = realBookingFee.ItemPriceDCurPrice;
            productCartItems.Add(bookingFee);

            try
            {
                //var _ticketing = new TicketingService.TicketingService();

                var _ticketing = new TicketingService(ConfigurationManager.AppSettings["url"] + "TicketingService.asmx");

                //var orderRequest = new GetOrderRequest()
                //{
                //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                //    UserSessionId = userSessionId.Replace("-", "")
                //};

                //var orderResponse = _ticketing.GetOrder(orderRequest);
                var orderResponse = RestTicketingService.GetOrder(UserSessionId);

                if (orderResponse.Order.Concessions != null)
                {
                    // eliminar concesiones agregadas anteriormente
                    //var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                    //var removeConcessionsRequest = new RemoveConcessionsRequest
                    //{
                    //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                    //    UserSessionId = userSessionId.Replace("-", ""),
                    //    ConcessionRemovals = concessionsToRemove
                    //};
                    //var removeConcessionsResponse = _ticketing.RemoveConcessions(removeConcessionsRequest);

                    
                    var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                    RemoveConcessionsResponse removeConcessionsResponse = _restTicketingService.RemoveConcessions(OptionalClientId,UserSessionId,concessionsToRemove);

                    if (transactionIdTemp != null)
                    {
                        spRollbackConcessionOrder(cinemaId, transactionIdTemp, "", userSessionId);
                    }
                }

                // agregar nuevas concesiones?
                if (concessions != null && concessions.Length > 0)
                {
                    //var addConcessionsRequest = new AddConcessionsRequest
                    //{
                    //    BookingMode = 0,
                    //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                    //    CinemaId = cinemaId,
                    //    Concessions = ToConcessionArray(productCartItems),
                    //    ProcessOrderValue = true,
                    //    ReturnOrder = true,
                    //    SessionId = sessionId,
                    //    UserSessionId = userSessionId.Replace("-", "")
                    //};


                    //AddConcessionsResponse addConcessionsResponse = _ticketing.AddConcessions(addConcessionsRequest);

                    Concession[] items = ToConcessionArray(productCartItems);

                    AddConcessionsResponse addConcessionsResponse = _restTicketingService.AddConcessions(0, OptionalClientId, cinemaId,items,true,true,sessionId, userSessionId.Replace("-", ""));

                    //addConcessionsResponse.Result = ResultCode.UnexpectedError;

                    if (addConcessionsResponse.Result == ResultCode.OK)
                    {
                        TotalValueCents = (decimal)(addConcessionsResponse.Order.TotalValueCents / 100m);
                    }
                    else if (addConcessionsResponse.Result == ResultCode.UnexpectedError)
                    {
                        throw new Exception("Lo sentimos, se presentó un problema al reservar su(s) producto(s), por favor reinicie la compra");
                    }
                    else
                    {
                        throw new Exception("Los productos seleccionados no están disponibles para la venta.");
                    }

                }

                try
                {
                    // Create Concessions Order
                    ConcessionOrder concessionOrder = CreateConcessionsOrder(cinemaId, concessions);

                    //guardar en la tabla tblCUtblOrderTrackConcessions
                    bool rsp = CreateOrderTrackConcessions(userSessionId.Replace("-", ""), TotalValueCents, ConfigurationManager.AppSettings["POSClientId"].ToString(), "N", cinemaId);

                    if (rsp)
                    {
                        List<OrderInvConcessions> items = CreateOrderInv(userSessionId.Replace("-", ""), ConfigurationManager.AppSettings["POSClientId"].ToString(), concessionOrder);

                        bool rsponce = spCreateOrdenInvConcessions(userSessionId.Replace("-", ""), items, cinemaId);
                        if (!rsponce)
                            throw new Exception("Lo sentimos, hubo un error al procesar su pedido, por favor intente nuevamente");

                    }
                    else
                    {
                        throw new Exception("Lo sentimos, hubo un error al procesar su pedido, por favor intente nuevamente");
                    }

                    response.Order = concessionOrder;
                    response.Order.BookingFee = realBookingFee;

                    return response;
                }
                catch (Exception ex)
                {
                    // cancelar la orden en el webservice de VISTA
                    //var concessionsToRemove = ToConcessionRemovals(productCartItems);

                    //var removeConcessionsRequest = new RemoveConcessionsRequest
                    //{
                    //    OptionalClientId = ConfigurationManager.AppSettings["POSClientId"],
                    //    ReturnOrder = false,
                    //    UserSessionId = userSessionId.Replace("-", ""),
                    //    ConcessionRemovals = concessionsToRemove
                    //};
                    //var removeConcessionsResponse = _ticketing.RemoveConcessions(removeConcessionsRequest);

                    var concessionsToRemove = ToConcessionRemovals(orderResponse.Order.Concessions);
                    RemoveConcessionsResponse removeConcessionsResponse = _restTicketingService.RemoveConcessions(OptionalClientId, UserSessionId, concessionsToRemove);
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                var errorResponse = new ConcessionOrderResponse();
                errorResponse.WithError = true;
                errorResponse.ErrorMessage = ex.Message;
                errorResponse.Order = null;
                return errorResponse;
            }



            //return response;


        }


        private static List<ConcessionItem> GetConcessionsCandiesWeb(string theaterId)
        {
            List<ConcessionItem> listConcessions = new List<ConcessionItem>();
            List<ConcessionItem> listConcessions3 = new List<ConcessionItem>();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ELMAH"].ToString());
            try
            {
                string sql = @"SELECT Cinema_strCode, Item_strItemId, Item_strItemDescription, Item_decStockAvailable, Item_dtmLastTransaction, Item_strItemType
                           FROM tblCandiesWeb
                           WHERE Cinema_strCode = '" + theaterId + "' AND NOT Item_decStockAvailable <= 0";

                using (var command = new SqlCommand(sql))
                {
                    command.Connection = conn;
                    conn.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var concession = new ConcessionItem();
                                concession.ItemStrItemID = (string)reader["Item_strItemId"];
                                concession.ItemStrItemDescription = ToTitleCaseString((string)reader["Item_strItemDescription"]);
                                concession.ItemStockCurAvailable = Convert.ToDecimal(reader["Item_decStockAvailable"]);
                                listConcessions.Add(concession);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                throw new Exception("Lo sentimos, se presentó un problema al reservar su(s) producto(s), por favor reinicie la compra");
            }

            conn.Close();


            List<ConcessionItem> listConcessions2 = new List<ConcessionItem>();

            var prueba = GetTheaterConcessions(theaterId);

            foreach (var item in prueba.Items)
            {
                var concession = new ConcessionItem();
                concession.ItemStrItemID = item.ItemStrItemID;
                concession.ItemStrItemDescription = item.ItemStrItemDescription;
                concession.ItemPriceDCurPrice = item.ItemPriceDCurPrice;
                concession.ItemQuantityPurchase = item.ItemQuantityPurchase;
                concession.ItemStockCurAvailable = item.ItemStockCurAvailable;
                concession.ItemType = item.ItemType;
                concession.RecipeQuantity = item.RecipeQuantity;
                concession.ClassStrCode = item.ClassStrCode;
                listConcessions2.Add(concession);
            }

            foreach (var item4 in listConcessions)
            {
                var prueba2 = listConcessions3.Where(ll => ll.ItemStrItemID == item4.ItemStrItemID);
                int prueba3 = prueba2.Count();
                if (prueba3 == 0)
                {
                    foreach (var item5 in listConcessions2)
                    {
                        if (item4.ItemStrItemID == item5.ItemStrItemID)
                        {
                            var prueba4 = listConcessions3.Where(ll => ll.ItemStrItemID == item4.ItemStrItemID);
                            int prueba5 = prueba4.Count();
                            if (prueba5 == 0)
                            {
                                var concession = new ConcessionItem();
                                concession.ItemStrItemID = item4.ItemStrItemID;
                                concession.ItemStrItemDescription = item4.ItemStrItemDescription;
                                concession.ItemPriceDCurPrice = item5.ItemPriceDCurPrice;
                                concession.ItemQuantityPurchase = item4.ItemQuantityPurchase;
                                //Esta validación es para que cuando el codigo del item sea igual a 999999901 (ESTACIONAMIENTO)
                                //se cambie el valor disponible por 1 y si es distinto entonces se queda con el valor que viene 
                                //de la base de datos
                                if (!(concession.ItemStrItemID == "999999901"))
                                {
                                    concession.ItemStockCurAvailable = item4.ItemStockCurAvailable;
                                }
                                else
                                {
                                    concession.ItemStockCurAvailable = (item4.ItemStockCurAvailable > 0) ? 1 : item4.ItemStockCurAvailable;
                                }
                                concession.ItemType = item4.ItemType;
                                concession.RecipeQuantity = item4.RecipeQuantity;
                                concession.ClassStrCode = item4.ClassStrCode;
                                listConcessions3.Add(concession);
                            }
                        }
                    }
                }
            }


            return listConcessions3;
        }

        /// <summary>
        /// Metodo que Realiza consulta en vista para la busqueda de compras relacionadas a la pelicula desorden publico,
        /// este metodo devuelve un objeto PromoPack el Cual se le Añade El Status que es de tipo Boolean
        /// </summary>
        /// <param name="TransactionID">Parametro que Podria ser BookingId o TransaccionId</param>
        /// <param name="CinemaID">Codigo del Cine al Cual Pertenece el BookingId o TransaccionId</param>
        /// <returns></returns>
        public static PromoPack PromoDesorden(string TransactionID, string CinemaID)
        {
            string Code = string.Empty;
            string Cine = "VISTA_" + CinemaID;
            string QueryBooking = $"SELECT TransC_lgnNumber  FROM tblBooking_Header  Where BookingH_intNextBookingNo = {TransactionID}";
            string QueryTransaction = $@"select top 1 TransT_lgnNumber, F.Film_strCode,W.Workstation_strName, S.Session_dtmRealShow FROM tblTrans_Ticket TT 
                                        LEFT JOIN tblWorkstation W ON W.Workstation_strCode = TT.Workstation_strCode 
                                        LEFT JOIN tblSession S ON S.Session_lngSessionId = TT.Session_lngSessionId
                                        LEFT JOIN tblFilm F ON F.Film_strCode = S.Film_strCode 
                                        LEFT JOIN tblAreaCategory A ON A.AreaCat_strCode = TT.AreaCat_strCode WHERE TransT_lgnNumber = {TransactionID}";

            PromoPack Pack = new PromoPack();
            SqlConnection Connect = new SqlConnection(ConfigurationManager.ConnectionStrings[Cine].ToString());
            try
            {
                SqlCommand Command = new SqlCommand(QueryBooking, Connect);
                Connect.Open();
                SqlDataReader DataReader = Command.ExecuteReader();
                if (DataReader.HasRows)
                {
                    DataReader.Read();
                    Code = DataReader["TransC_lgnNumber"].ToString();
                    string QueryCode = $@"select top 1 TransT_lgnNumber, F.Film_strCode, W.Workstation_strName, S.Session_dtmRealShow FROM tblTrans_Ticket TT 
                                        LEFT JOIN tblWorkstation W ON W.Workstation_strCode = TT.Workstation_strCode 
                                        LEFT JOIN tblSession S ON S.Session_lngSessionId = TT.Session_lngSessionId
                                        LEFT JOIN tblFilm F ON F.Film_strCode = S.Film_strCode 
                                        LEFT JOIN tblAreaCategory A ON A.AreaCat_strCode = TT.AreaCat_strCode WHERE TransT_lgnNumber = {Code}";
                    Command = new SqlCommand(QueryCode, Connect);
                    DataReader.Close();
                    DataReader = Command.ExecuteReader();
                    if (DataReader.HasRows)
                    {
                        DataReader.Read();
                        if (DataReader["Film_strCode"].ToString().Equals(ConfigurationManager.AppSettings["MoviePromoDesorden"].ToString()))
                        {
                            Pack.Workstation = DataReader["Workstation_strName"].ToString().Trim();
                            Pack.Status = true;
                            DataReader.Close();
                            Connect.Close();
                            return Pack;
                        }
                        else
                        {
                            DataReader.Close();
                            Connect.Close();
                            Pack.Mensaje = "Compra No Relacionada A la Promocion";
                            Pack.Status = false;
                            return Pack;
                        }
                    }
                    else
                    {
                        DataReader.Close();
                        Connect.Close();
                        Pack.Mensaje = "No Posee Compras Relacionadas A la Promocion";
                        Pack.Status = false;
                        return Pack;
                    }
                }
                else
                {
                    DataReader.Close();
                    Command = new SqlCommand(QueryTransaction, Connect);
                    DataReader = Command.ExecuteReader();
                    if (DataReader.HasRows)
                    {
                        DataReader.Read();
                        if (DataReader["Film_strCode"].ToString().Equals(ConfigurationManager.AppSettings["MoviePromoDesorden"].ToString()))
                        {
                            Pack.Workstation = DataReader["Workstation_strName"].ToString().Trim();
                            Pack.Status = true;
                            DataReader.Close();
                            Connect.Close();
                            return Pack;
                        }
                        else
                        {
                            DataReader.Close();
                            Connect.Close();
                            Pack.Mensaje = "Compra No Relacionada A la Promocion";
                            Pack.Status = false;
                            return Pack;
                        }
                    }
                    else
                    {
                        DataReader.Close();
                        Connect.Close();
                        Pack.Mensaje = "No Posee Compras Relacionadas A la Promocion";
                        Pack.Status = false;
                        return Pack;
                    }
                }
            }
            catch (Exception Ex)
            {
                if (Connect.State == ConnectionState.Open)
                {
                    Connect.Close();
                }
                Pack.Mensaje = Ex.Message;
                Pack.Status = false;
                return Pack;
            }
        }

        #region CU Methods Concessions

        public static bool CreateOrderTrackConcessions(string userSessionId, decimal TotalValueCents, string ClientId, string PaymentOK, string theaterId)
        {
            var connectionString = "VISTA_" + theaterId;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            using (var command = new SqlCommand())
            {
                command.Connection = conn;
                command.CommandText = "spCU_CreateOrderTrackConcessions";
                command.CommandType = CommandType.StoredProcedure;
                //userSession
                var parameter_userSessionId = command.CreateParameter();
                parameter_userSessionId.ParameterName = "@userSessionId";
                parameter_userSessionId.Value = userSessionId;
                command.Parameters.Add(parameter_userSessionId);
                //TotalValueCents
                var parameter_TotalValueCents = command.CreateParameter();
                parameter_TotalValueCents.ParameterName = "@TotalValueCents";
                parameter_TotalValueCents.Value = TotalValueCents;
                command.Parameters.Add(parameter_TotalValueCents);
                //ClientId
                var parameter_ClientId = command.CreateParameter();
                parameter_ClientId.ParameterName = "@ClientId";
                parameter_ClientId.Value = ClientId;
                command.Parameters.Add(parameter_ClientId);
                //PaymentOK
                var parameter_PaymentOK = command.CreateParameter();
                parameter_PaymentOK.ParameterName = "@PaymentOK";
                parameter_PaymentOK.Value = PaymentOK;
                command.Parameters.Add(parameter_PaymentOK);

                try
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    conn.Close();
                }

            }

        }

        //método que permite crear una lista de items que luego se pasa como objeto al storeProcedure "sp_CUCreateOrderInvConcessionsItems"
        public static List<OrderInvConcessions> CreateOrderInv(string userSessionId, string ClientId, ConcessionOrder ConcessionOrder)
        {
            List<OrderInvConcessions> list_items = new List<OrderInvConcessions>();
            int i = 1;
            //guardar items en la tabla tblCUOrderInvConcessions
            foreach (var items in ConcessionOrder.Items)
            {
                OrderInvConcessions order = new OrderInvConcessions();
                order.ClientID = ClientId;
                order.intSequence = i;
                order.ItemCode = items.ItemStrItemID;
                order.Track_Id = userSessionId.Replace("-", "");

                if ((items.Ingredients != null) && (items.Ingredients.Count() > 0))
                {
                    order.IsBookingFee = "M";
                    order.Quantity = 0;
                }
                else
                {
                    order.IsBookingFee = "N";
                    order.Quantity = items.ItemQuantityPurchase;
                }

                list_items.Add(order);
                i++;
                // Si el producto tiene carameleria

                if ((items.Ingredients != null) && (items.Ingredients.Count() > 0))
                {
                    foreach (var ingredients in items.Ingredients)
                    {
                        OrderInvConcessions orderingredients = new OrderInvConcessions();

                        orderingredients.ClientID = ClientId;
                        orderingredients.intSequence = i;
                        orderingredients.ItemCode = ingredients.ItemStrItemID;
                        orderingredients.Track_Id = userSessionId.Replace("-", "");
                        orderingredients.Quantity = ingredients.RecipeQuantity * items.ItemQuantityPurchase;
                        orderingredients.IsBookingFee = "N";
                        i++;
                        list_items.Add(orderingredients);
                    }
                }
            }

            return list_items;
        }
        //Permite agregar o actualizar items que estan relacionados a un determinado userSessionId
        public static bool spCreateOrdenInvConcessions(string userSessionId, List<OrderInvConcessions> list, string theaterId)
        {
            var connectionString = "VISTA_" + theaterId;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            using (var command = new SqlCommand())
            {
                command.Connection = conn;
                command.CommandText = "sp_CUCreateOrderInvConcessionsItems";
                command.CommandType = CommandType.StoredProcedure;

                //Parameters
                command.Parameters.Add("@userId", SqlDbType.VarChar).Value = userSessionId;

                //Items List
                //Crear datatable
                try
                {
                    DataTable table = new DataTable();

                    //Columnas
                    table.Columns.Add("Client_Id", typeof(string));
                    table.Columns.Add("Track_Id", typeof(string));
                    table.Columns.Add("SequenceInt", typeof(int));
                    table.Columns.Add("Item_Code", typeof(string));
                    table.Columns.Add("Quantity", typeof(decimal));
                    table.Columns.Add("BookingFee", typeof(string));
                    //llenar filas
                    foreach (var items in list)
                    {
                        var row = table.NewRow();
                        row["Client_Id"] = items.ClientID;
                        row["Track_Id"] = items.Track_Id;
                        row["SequenceInt"] = items.intSequence;
                        row["Item_Code"] = items.ItemCode;
                        row["Quantity"] = items.Quantity.ToString("N5");
                        row["BookingFee"] = items.IsBookingFee;

                        table.Rows.Add(row);
                    }
                    //pasar datatable como comando
                    command.Parameters.Add("@items", SqlDbType.Structured).Value = table;

                    //conexión a bd
                    conn.Open();
                    command.ExecuteNonQuery();

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    conn.Close();
                }
            }

        }

        //LUIS RAMIREZ, 30/10/2017 .Cambia de estatus a Y cuando se procesa la compra con concesiones, dicho cambio se hace en la tabla tblCUOrderTrackConcessions en el campo PaymentOK
        public static void ProcessOrderConcessions(string userSessionId, string theaterId)
        {
            var connectionString = "VISTA_" + theaterId;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ToString());

            using (var command = new SqlCommand())
            {
                var sql = @"UPDATE tblCUOrderTrackConcessions SET PaymentOK='Y' WHERE Track_Id = @TrackId";
                command.Connection = conn;
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                //Parameters
                command.Parameters.Add("@TrackId", SqlDbType.Char).Value = userSessionId;
                try
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }

            }
        }
        #endregion

    }
}