using Newtonsoft.Json;
using Services.Vista.DAL.DORest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DO
{
    public class RestTicketingService
    {
        public static RestAddTicketsResponse AddTicket(string ClientID, string UserSessionID, string CinemaID, string SessionID, TicketType[] TicketTypes)
        {
            List<DORest.TicketType> boletos = new List<DORest.TicketType>();
            foreach (var i in TicketTypes)
            {
                DORest.TicketType t = new DORest.TicketType();
                t.LoyaltyRecognitionId = i.LoyaltyRecognitionId;
                t.LoyaltyRecognitionSequence = i.LoyaltyRecognitionSequence;
                t.OptionalAreaCategoryCode = i.OptionalAreaCategoryCode;
                t.OptionalBarcode = i.OptionalBarcode;
                t.OptionalBarcodePin = i.OptionalBarcodePin;
                t.OptionalTicketTypeProvider = i.OptionalTicketTypeProvider;
                t.PriceCardPriceInCents = i.PriceInCents;
                t.PriceInCents = i.PriceInCents;
                t.PromotionInstanceGroupNumber = i.PromotionInstanceGroupNumber;
                t.PromotionTicketTypeId = i.PromotionTicketTypeId;
                t.Qty = i.Qty;
                t.ThirdPartyMemberScheme = null;
                t.TicketTypeCode = i.TicketTypeCode;
                boletos.Add(t);
            }
            TicketRequest tiketrequest = new TicketRequest();

            tiketrequest.UserSessionId = UserSessionID;
            tiketrequest.CinemaId = CinemaID;
            tiketrequest.SessionId = SessionID;
            tiketrequest.TicketTypes.AddRange(boletos);
            tiketrequest.UserSelectedSeatingSupported = true;
            tiketrequest.ReturnSeatData = true;
            tiketrequest.ExcludeAreasWithoutTickets = false;
            tiketrequest.IncludeSeatNumbers = true;
            tiketrequest.ProcessOrderValue = true;
            tiketrequest.ReturnOrder = true;
            tiketrequest.ReturnDiscountInfo = false;
            tiketrequest.BookingMode = 0;

            string json = JsonConvert.SerializeObject(tiketrequest);
            var addTicketsResponse = WebConsumer.Post<RestAddTicketsResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlAddTickets"].ToString(), json);
            return addTicketsResponse;
        }

        public AddConcessionsResponse AddConcessions(int bookingMode, string optionalClientId, string cinemaId, Concession[] concession, bool processOrderValue, bool returnOrder, string sessionId, string userSessionId)
        {

            List<DORest.Concession> concessionRest = new List<DORest.Concession>();


            foreach (var c in concession)
            {
                DORest.Concession concessionRestApi = new DORest.Concession();
                concessionRestApi.ItemId = c.ItemId;
                concessionRestApi.Quantity = c.Quantity;
                concessionRestApi.PromoCode = c.PromoCode;
                //concessionRestApi.RecognitionId = Convert. c.RecognitionId;
                concessionRestApi.RecognitionSequenceNumber = c.RecognitionSequenceNumber;
                concessionRestApi.IsLoyaltyMembershipActivation = c.IsLoyaltyMembershipActivation;
                concessionRestApi.HeadOfficeItemCode = c.HeadOfficeItemCode;
                concessionRestApi.GetBarcodeFromVGC = c.GetBarcodeFromVGC;
                concessionRestApi.VariablePriceInCents = null;
                concessionRest.Add(concessionRestApi);
            }
            var addConcessionsRequest = new DORest.AddConcessionsRequest
            {
                BookingMode = bookingMode,
                OptionalClientId = optionalClientId,
                CinemaId = cinemaId,
                Concessions = concessionRest.ToArray(),
                ProcessOrderValue = processOrderValue,
                ReturnOrder = returnOrder,
                SessionId = sessionId,
                UserSessionId = userSessionId
            };

            var jsonValue = JsonConvert.SerializeObject(addConcessionsRequest);
            var addConcessionsResponse = WebConsumer.Post<AddConcessionsResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlRestAddConcessions"].ToString(), jsonValue);
            //getter.Result = ResultCode.OK;
            return addConcessionsResponse;
        }

        public RemoveConcessionsResponse RemoveConcessions(string optionalClientId, string userSessionId, ConcessionRemoval[] concession)
        {
            List<DORest.ConcessionRemoval> concessionRemovalRest = new List<DORest.ConcessionRemoval>();

            foreach (var c in concession)
            {
                DORest.ConcessionRemoval concessionRemovalRestApi = new DORest.ConcessionRemoval();
                concessionRemovalRestApi.ItemId = c.ItemId;
                concessionRemovalRestApi.Quantity = c.Quantity;
                concessionRemovalRest.Add(concessionRemovalRestApi);
            }

            var removeConcessionsRequest = new DORest.RemoveConcessionsRequest
            {
                OptionalClientId = optionalClientId,
                UserSessionId = userSessionId,
                ConcessionRemovals = concessionRemovalRest.ToArray()
            };

            var jsonValue = JsonConvert.SerializeObject(removeConcessionsRequest);
            var removeConcessions = WebConsumer.Delete<RemoveConcessionsResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlRestRemoveConcessions"].ToString(), jsonValue);
            return removeConcessions;
        }

        public static GetOrderResponse GetOrder(string userSessionId)
        {

            var orderRequest = new DORest.GetOrderRequest()
            {
                OptionalClientId = ConfigurationManager.AppSettings["POSClientId"].ToString(),
                UserSessionId = userSessionId
            };

            var jsonValue = JsonConvert.SerializeObject(orderRequest);
            var orderResponse = WebConsumer.Post<GetOrderResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlRestGetOrder"].ToString(), jsonValue);
            return orderResponse;
        }

        public static SetSelectedSeatsResponse SetSelectedSeats(string ClientID, string UserSessionID, string CinemaID, string SessionID, SelectedSeat[] SelectedSeats)
        {
            List<DORest.SelectedSeat> selectedSeatRest = new List<DORest.SelectedSeat>();

            foreach (var s in SelectedSeats)
            {
                DORest.SelectedSeat selectedSeatRestApi = new DORest.SelectedSeat();
                selectedSeatRestApi.AreaCategoryCode = s.AreaCategoryCode;
                selectedSeatRestApi.AreaNumber = s.AreaNumber;
                selectedSeatRestApi.RowIndex = s.RowIndex;
                selectedSeatRestApi.ColumnIndex = s.ColumnIndex;
                selectedSeatRest.Add(selectedSeatRestApi);
            }

            SelectedSeatRequest selectedSeatRequest = new SelectedSeatRequest();
            selectedSeatRequest.CinemaId = CinemaID;
            selectedSeatRequest.SessionId = SessionID;
            selectedSeatRequest.UserSessionId = UserSessionID;
            selectedSeatRequest.ReturnOrder = true;
            selectedSeatRequest.SelectedSeats = selectedSeatRest.ToArray();
            selectedSeatRequest.OptionalClientId = ClientID;


            var jsonValue = JsonConvert.SerializeObject(selectedSeatRequest);
            var setSelectedSeats = WebConsumer.Post<SetSelectedSeatsResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlRestSetSelectedSeats"].ToString(), jsonValue);
            return setSelectedSeats;
        }

        public static GetSessionSeatDataResponse GetSessionSeatData(string ClientID, string UserSessionID, string SessionID, string TheaterID)
        {
            DORest.GetSessionSeatDataRequest getSessionSeatDataRequest = new DORest.GetSessionSeatDataRequest();
            getSessionSeatDataRequest.OptionalClientId = ClientID;
            getSessionSeatDataRequest.UserSessionId = UserSessionID;
            getSessionSeatDataRequest.SessionId = SessionID;
            getSessionSeatDataRequest.CinemaId = TheaterID;
            getSessionSeatDataRequest.includeSeatNumbers = true;

            string ruta = ConfigurationManager.AppSettings["urlBase"].ToString() +
                                ConfigurationManager.AppSettings["urlRestGetCinema"].ToString() + TheaterID +
                                ConfigurationManager.AppSettings["urlRestGetSeccion"].ToString() + SessionID +
                                ConfigurationManager.AppSettings["urlRestGetFinal"].ToString();


            var jsonValue = JsonConvert.SerializeObject(getSessionSeatDataRequest);
            var getSessionSeat = WebConsumer.Get<GetSessionSeatDataResponse>(ruta);
            return getSessionSeat;
        }

        public static AddTicketsResponse RestAddTicket(string ClientID, string UserSessionID, string CinemaID, string SessionID, TicketType[] TicketTypes)
        {
            List<DORest.TicketType> boletos = new List<DORest.TicketType>();
            foreach (var i in TicketTypes)
            {
                DORest.TicketType t = new DORest.TicketType();
                t.LoyaltyRecognitionId = i.LoyaltyRecognitionId;
                t.LoyaltyRecognitionSequence = i.LoyaltyRecognitionSequence;
                t.OptionalAreaCategoryCode = i.OptionalAreaCategoryCode;
                t.OptionalBarcode = i.OptionalBarcode;
                t.OptionalBarcodePin = i.OptionalBarcodePin;
                t.OptionalTicketTypeProvider = i.OptionalTicketTypeProvider;
                t.PriceCardPriceInCents = i.PriceInCents;
                t.PriceInCents = i.PriceInCents;
                t.PromotionInstanceGroupNumber = i.PromotionInstanceGroupNumber;
                t.PromotionTicketTypeId = i.PromotionTicketTypeId;
                t.Qty = i.Qty;
                t.ThirdPartyMemberScheme = null;
                t.TicketTypeCode = i.TicketTypeCode;
                boletos.Add(t);
            }
            TicketRequest tiketrequest = new TicketRequest();

            tiketrequest.UserSessionId = UserSessionID;
            tiketrequest.CinemaId = CinemaID;
            tiketrequest.SessionId = SessionID;
            tiketrequest.TicketTypes.AddRange(boletos);
            tiketrequest.UserSelectedSeatingSupported = true;
            tiketrequest.ReturnSeatData = true;
            tiketrequest.ExcludeAreasWithoutTickets = false;
            tiketrequest.IncludeSeatNumbers = true;
            tiketrequest.ProcessOrderValue = true;
            tiketrequest.ReturnOrder = true;
            tiketrequest.ReturnDiscountInfo = false;
            tiketrequest.BookingMode = 0;

            string json = JsonConvert.SerializeObject(tiketrequest);
            var addTickets = WebConsumer.Post<AddTicketsResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlAddTickets"].ToString(), json);
            return addTickets;
        }

        public static CompleteOrderResponse RestCompleteOrder(string ClientID, string UserSessionID, DORest.PaymentInfo payInfo, string Name, string Email, string Phone)
        {
            DORest.CompleteOrderRequest completeOrderRequest = new DORest.CompleteOrderRequest();
            completeOrderRequest.CustomerEmail = Email;
            completeOrderRequest.CustomerName = Name;
            completeOrderRequest.BookingMode = 0;
            completeOrderRequest.PaymentInfo = payInfo;
            completeOrderRequest.PerformPayment = false;
            if (payInfo.PaymentTenderCategory == "SVC")
            {
                completeOrderRequest.PerformPayment = true;
            }
            completeOrderRequest.UserSessionId = UserSessionID;
            //completeOrderRequest.OptionalMemberId = ClientID;

            string json = JsonConvert.SerializeObject(completeOrderRequest);
            var completeOrderResponse = WebConsumer.Post<CompleteOrderResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlCompleteOrder"].ToString(), json);
            return completeOrderResponse;
        }

        public static Boolean RestGetCancelOrder(string UserSessionID)
        {
            DORest.CancelOrderRequest cancelrequest = new DORest.CancelOrderRequest();
            cancelrequest.UserSessionId = UserSessionID;
            string json = JsonConvert.SerializeObject(cancelrequest);
            var CancelOrder = WebConsumer.Post<DORest.CancelOrderResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlRestCancelOrder"].ToString(), json);

            if (CancelOrder.OrderNotFound == false && CancelOrder.Result == 0)
            {
                return true;
            }
            return false;

        }

        public static Boolean RestGetRefundBooking(string TheaterID, int BookingNumber, int ValueInCents)
        {
            DORest.RefundBookingRequest RefundBookingResponseRequest = new DORest.RefundBookingRequest();
            RefundBookingResponseRequest.BookingNumber = BookingNumber;
            RefundBookingResponseRequest.CinemaId = TheaterID;
            RefundBookingResponseRequest.IsPriorDayRefund = false;
            RefundBookingResponseRequest.RefundAmount = ValueInCents;
            RefundBookingResponseRequest.RefundBookingFee = true;
            RefundBookingResponseRequest.RefundConcessions = true;
            RefundBookingResponseRequest.MarkAsRefundedOnly = true;
            string json = JsonConvert.SerializeObject(RefundBookingResponseRequest);
            var RefundResponse = WebConsumer.Post<RestRefundResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["urlRefundBooking"].ToString(), json);

            if (RefundResponse.ResultCode == 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// RM - Metodo para validar los tickets asociados a un VarCode
        /// </summary>
        /// <param name="cinemaid">el id del cine al que quiero comprobar el boleto</param>
        /// <param name="sessionid">la sesion que quiero consultar para ese boleto</param>
        /// <param name="barcode">el voucher que quiero validar para ese ciney esa session</param>
        /// <returns></returns>
        public static BO.InfoVoucher GetTicketTypesforBarcode(string cinemaid, string sessionid, string barcode)
        {
            BO.InfoVoucher info = new BO.InfoVoucher
            {
                VoucherCode = barcode,
                ValidoParafuncion = false,
                Redimido = false
            };
            var url = string.Format(ConfigurationManager.AppSettings["urlBase"].ToString() + "/RESTData.svc/cinemas/{0}/sessions/{1}/tickets-for-barcode?voucherBarcode={2}", cinemaid, sessionid, barcode);
            TicketTypesforBarcode tickets = WebConsumer.Get<TicketTypesforBarcode>(url);
            if (tickets.Tickets != null && tickets.Tickets.Count > 0)
            {
                var d = VISTAITFilm.ConsultaBoletos(new BO.DatosBoleto { Stock_strBarcode = barcode });
                if (d.CodUtilizado && !d.IsMultiVoucher)
                {
                    info.TextoMostrar = "Este Código Electrónico ya fue utilizado. Intente con otro.";
                    info.Redimido = true;
                }
                if (d.BoletoNoValido)
                {
                    info.TextoMostrar = "Este Código Electrónico no es válido.";
                    info.Redimido = d.CodUtilizado;
                }
                if (d.BoletoVencido)
                {
                    info.TextoMostrar = "La fecha de vigencia de este Código Electrónico ya expiró";
                }
                if (!d.BoletoNoValido && !d.BoletoVencido)
                {
                    info.ValidoParafuncion = true;
                    info.EntradasRestantes = d.EntradasRestantes;
                    info.IsMultiVoucher = d.IsMultiVoucher;
                    //tomo el primer boleto de la lista ya que el invitado no deberia poder seleccionar cual tipo de boleto quiere cambiar (en caso de que hayan dos o mas)
                    info.CodigoBoletoParaCanje = tickets.Tickets[0].TicketTypeCode;
                    //cobro solo el surcharge del ticket.
                    info.ValorRedencion = tickets.Tickets[0].SurchargeAmount /100; //xq viene con dos ceros de mas..
                    info.Redimido = false;
                    info.TextoMostrar = tickets.Tickets[0].Description;
                    if (!d.IsMultiVoucher && d.EntradasRestantes == 0 && d.NumeroEntradas == 0) //si no es un multivoucher, hago entrarasrestantes =1 para poder manejarlo en el front
                    {
                        info.EntradasRestantes = 1;
                    }
                    else
                    {
                        info.EntradasRestantes = d.EntradasRestantes;
                    }
                }
            }
            else
            {
                info.ValidoParafuncion = false;
                info.TextoMostrar = tickets.ErrorDescription;
            }

            return info;
        }
    }
}