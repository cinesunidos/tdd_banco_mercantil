﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public partial class CompleteOrderResponse
    {
        //public static CompleteOrderResponse CompleteOrder(string ClientID, string UserSessionID, PaymentInfo payInfo, string Name, string Email, string Phone)
        //{
        //    TicketingService Ticketing = new TicketingService();

        //    CompleteOrderRequest completeOrderRequest = new CompleteOrderRequest();
        //    completeOrderRequest.CustomerEmail = Email;
        //    completeOrderRequest.CustomerName = Name;
        //    completeOrderRequest.BookingMode = 0;
        //    //completeOrderRequest.CustomerPhone = Phone;
        //    //completeOrderRequest.GeneratePrintStream = false;
        //    completeOrderRequest.PaymentInfo = payInfo;
        //    completeOrderRequest.PerformPayment = false;
        //    if (payInfo.PaymentTenderCategory == "SVC")
        //    {
        //        completeOrderRequest.PerformPayment = true;
        //    }
        //    completeOrderRequest.UserSessionId = UserSessionID;
        //    //completeOrderRequest.CustomerZipCode = "1008";
        //    //completeOrderRequest.ReturnPrintStream = false;
        //    completeOrderRequest.OptionalClientId = ClientID;

        //    CompleteOrderResponse rp = Ticketing.CompleteOrder(completeOrderRequest);
        //    return rp;
        //}

        public static CompleteOrderResponse CompleteOrder(string ClientID, string UserSessionID, PaymentInfo payInfo, string Name, string Email, string Phone)
        {
            using (TicketingService Ticketing = new TicketingService(ConfigurationManager.AppSettings["url"] + "TicketingService.asmx"))
            {
                CompleteOrderRequest completeOrderRequest = new CompleteOrderRequest();
                completeOrderRequest.CustomerEmail = Email;
                completeOrderRequest.CustomerName = Name;
                completeOrderRequest.BookingMode = 0;
                //completeOrderRequest.CustomerPhone = Phone;
                //completeOrderRequest.GeneratePrintStream = false;
                completeOrderRequest.PaymentInfo = payInfo;
                completeOrderRequest.PerformPayment = false;
                if (payInfo.PaymentTenderCategory == "SVC")
                {
                    completeOrderRequest.PerformPayment = true;
                }
                completeOrderRequest.UserSessionId = UserSessionID;
                //completeOrderRequest.CustomerZipCode = "1008";
                //completeOrderRequest.ReturnPrintStream = false;
                completeOrderRequest.OptionalClientId = ClientID;

                CompleteOrderResponse rp = Ticketing.CompleteOrder(completeOrderRequest);
                return rp;
            }


        }

    }
}
