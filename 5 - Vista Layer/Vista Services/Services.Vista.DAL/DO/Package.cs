﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class Package
    {
        public string Cinema_strID {get; set;} 
        public string PGroup_strCode {get; set;}
        public string Price_strCode {get; set;}
        public string PPack_strPackageDesc {get; set;}
        public string PPack_strAllowRemoteSales {get; set;}
        public string PPack_strType {get; set;}
        public string TType_strCode {get; set;}
        public string Price_strDescription {get; set;}
        public string Price_strDescriptionAlt {get; set;}
        public string Item_strItemId {get; set;}
        public string Item_strItemDescription {get; set;}
        public string Item_strItemDescriptionAlt {get; set;}
        public string PPack_strCode {get; set;}
        public string PPack_intQuantity {get; set;}
        public string PPack_intPriceEach {get; set;}
        public string STax_strCode {get; set;}
        public string PPack_intSurcharge {get; set;}
        public string Price_strTicket_Type_Description_2 {  get; set;}

        #region Methods

        public static List<Package> GetPackageList(string TicketType, string CinemaID, string SessionID)
        {
            //DataService.GetTicketTypePackageRequest req = new DataService.GetTicketTypePackageRequest();
            //req.CinemaId = CinemaID;
            //req.SessionId = SessionID;
            //req.TicketTypeCode = TicketType;
            //Interpret<Package, DataService.GetTicketTypePackageRequest> interpret = new Interpret<Package, DataService.GetTicketTypePackageRequest>();
            //List<Package> Packages = interpret.Get("GetTicketTypePackage", req);
            //return Packages;
            using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
            {
                GetTicketTypePackageRequest req = new GetTicketTypePackageRequest();
                req.CinemaId = CinemaID;
                req.SessionId = SessionID;
                req.TicketTypeCode = TicketType;
                var respuesta = servicio.GetTicketTypePackage(req);
                XElement element = XElement.Parse(respuesta.DatasetXML.ToString());
                List<Package> lista = element.Elements("Table")
                    .Select(item => new Package()
                    {
                        Cinema_strID = item.Element("Cinema_strID").Value,
                        Item_strItemDescription = item.Element("Item_strItemDescription").Value,
                        Item_strItemDescriptionAlt = item.Element("Item_strItemDescriptionAlt").Value,
                        Item_strItemId = item.Element("Item_strItemId").Value,
                        PGroup_strCode = item.Element("PGroup_strCode").Value,
                        PPack_intPriceEach = item.Element("PPack_intPriceEach").Value,
                        PPack_intQuantity = item.Element("PPack_intQuantity").Value,
                        PPack_intSurcharge = item.Element("PPack_intSurcharge").Value,
                        PPack_strAllowRemoteSales = item.Element("PPack_strAllowRemoteSales").Value,
                        PPack_strCode = item.Element("PPack_strCode").Value,
                        PPack_strPackageDesc = item.Element("PPack_strPackageDesc").Value,
                        PPack_strType = item.Element("PPack_strType").Value,
                        Price_strCode = item.Element("Price_strCode").Value,
                        Price_strDescription = item.Element("Price_strDescription").Value,
                        Price_strDescriptionAlt = item.Element("Price_strDescriptionAlt").Value,
                        Price_strTicket_Type_Description_2 = item.Element("Price_strTicket_Type_Description_2").Value,
                        STax_strCode = item.Element("STax_strCode").Value,
                        TType_strCode = item.Element("TType_strCode").Value
                    }).ToList();
                return lista;

            }

        }

        public static List<Package> RestGetPackageList(string TicketType, string CinemaID, string SessionID)
        {
            string url = "http://desaapl02/WSVistaWebClient/RESTData.svc/cinemas/" + CinemaID + "/sessions/" + SessionID + "/tickets";
            var TicketTypePackage = WebConsumer.Get<Services.Vista.DAL.DORest.GetTicketTypePackage>(url);
            var filter = TicketTypePackage.Tickets.Where(t => t.TicketTypeCode.Equals(TicketType)).ToList();
            List<Package> lista = new List<Package>();
            Package package = new Package();
            foreach (var i in filter)
            {
                package.Cinema_strID = i.CinemaId;
                package.PGroup_strCode = i.PriceGroupCode;
                package.Price_strCode = i.TicketTypeCode;
                package.PPack_strPackageDesc = i.Description;
                package.PPack_strAllowRemoteSales = "";
                package.PPack_strType = "";
                package.TType_strCode = "";
                package.Price_strDescription = "";
                package.Price_strDescriptionAlt = "";
                package.Item_strItemId = "";
                package.Item_strItemDescription = "";
                package.Item_strItemDescriptionAlt = "";
                package.PPack_strCode = "";

                if ((i.PackageContent != null) && (i.PackageContent.Tickets != null) && (i.PackageContent.Tickets.Count > 0))
                {
                    foreach (var item in i.PackageContent.Tickets)
                    {
                        package.PPack_intQuantity = item.Quantity.ToString();
                    }
                }
                else
                {
                    package.PPack_intQuantity = "1";
                }

                package.PPack_intPriceEach = (i.PriceInCents / 2).ToString();
                package.STax_strCode = "";
                package.PPack_intSurcharge = "";
                package.Price_strTicket_Type_Description_2 = i.DescriptionAlt;
                lista.Add(package);
            }

            return lista;
        }


        //public static List<Package> GetPackageList(string TicketType, string CinemaID, string SessionID)
        //{
        //    DataService.GetTicketTypePackageRequest req = new DataService.GetTicketTypePackageRequest();
        //    req.CinemaId = CinemaID;
        //    req.SessionId = SessionID;
        //    req.TicketTypeCode = TicketType;
        //    Interpret<Package, DataService.GetTicketTypePackageRequest> interpret = new Interpret<Package, DataService.GetTicketTypePackageRequest>();
        //    List<Package> Packages = interpret.Get("GetTicketTypePackage", req);
        //    return Packages;
        //}
        #endregion
    }
}
