using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Services.Vista.DAL.DO
{
    public class WebConsumer
    {
        public static T Post<T>(string URL, string jsonValue)
        {
            var httpClient = new HttpClient();
            var request = new HttpRequestMessage(new HttpMethod("POST"), URL);
            request.Headers.TryAddWithoutValidation("Accept", "application/json");
            request.Headers.Add("connectapitoken", ConfigurationManager.AppSettings["connectapitoken"].ToString());
            request.Content = new StringContent(jsonValue, Encoding.UTF8, "application/json");
            var task = httpClient.SendAsync(request);
            task.Wait();
            var res = JsonConvert.DeserializeObject<T>(task.Result.Content.ReadAsStringAsync().Result);
            return res;
        }

        public static T Get<T>(string URL)
        {
            var http = (HttpWebRequest)WebRequest.Create(new Uri(URL));
            http.Headers.Add("connectapitoken", ConfigurationManager.AppSettings["connectapitoken"].ToString());
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";
            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            return JsonConvert.DeserializeObject<T>(content);
        }

        public static T Delete<T>(string URL, string jsonValue)
        {
            HttpClient httpClient = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(new HttpMethod("DELETE"), URL);

            request.Headers.TryAddWithoutValidation("Accept", "application/json");
            request.Headers.Add("connectapitoken", ConfigurationManager.AppSettings["connectapitoken"].ToString());
            request.Content = new StringContent(jsonValue, Encoding.UTF8, "application/json");
            var task = httpClient.SendAsync(request);
            task.Wait();
            var response = task.Result;
            return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
        }
    }
}
