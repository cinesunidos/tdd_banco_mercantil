﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class MovieList
    {
        #region Fields
        private string cinema_strIDField;

        private string movie_strIDField;

        private string movie_strNameField;

        private string movie_strRatingField;

        private string movie_strName_2Field;

        private string movie_strRating_2Field;

        private string movie_HOFilmCodeField;

        private int movie_intFCodeField;

        private bool movie_intFCodeFieldSpecified;

        private int cinOperator_strCodeField;

        private bool cinOperator_strCodeFieldSpecified;

        private int cinOperator_strNameField;

        private bool cinOperator_strNameFieldSpecified;

        private string event_strCodeField;

        private string event_strFilmsIndependentField;

        private string memberMovieField;

        private string hOPKField;

        private int movie_intList_PosField;

        private bool movie_intList_PosFieldSpecified;
        #endregion
        #region Properties
        public string Cinema_strID
        {
            get
            {
                return this.cinema_strIDField;
            }
            set
            {
                this.cinema_strIDField = value;
            }
        }

        public string Movie_strID
        {
            get
            {
                return this.movie_strIDField;
            }
            set
            {
                this.movie_strIDField = value;
            }
        }

        public string Movie_strName
        {
            get
            {
                return this.movie_strNameField;
            }
            set
            {
                this.movie_strNameField = value;
            }
        }

        public string Movie_strRating
        {
            get
            {
                return this.movie_strRatingField;
            }
            set
            {
                this.movie_strRatingField = value;
            }
        }

        public string Movie_strName_2
        {
            get
            {
                return this.movie_strName_2Field;
            }
            set
            {
                this.movie_strName_2Field = value;
            }
        }

        public string Movie_strRating_2
        {
            get
            {
                return this.movie_strRating_2Field;
            }
            set
            {
                this.movie_strRating_2Field = value;
            }
        }

        public string Movie_HOFilmCode
        {
            get
            {
                return this.movie_HOFilmCodeField;
            }
            set
            {
                this.movie_HOFilmCodeField = value;
            }
        }

        public int Movie_intFCode
        {
            get
            {
                return this.movie_intFCodeField;
            }
            set
            {
                this.movie_intFCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Movie_intFCodeSpecified
        {
            get
            {
                return this.movie_intFCodeFieldSpecified;
            }
            set
            {
                this.movie_intFCodeFieldSpecified = value;
            }
        }

        public int CinOperator_strCode
        {
            get
            {
                return this.cinOperator_strCodeField;
            }
            set
            {
                this.cinOperator_strCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CinOperator_strCodeSpecified
        {
            get
            {
                return this.cinOperator_strCodeFieldSpecified;
            }
            set
            {
                this.cinOperator_strCodeFieldSpecified = value;
            }
        }

        public int CinOperator_strName
        {
            get
            {
                return this.cinOperator_strNameField;
            }
            set
            {
                this.cinOperator_strNameField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CinOperator_strNameSpecified
        {
            get
            {
                return this.cinOperator_strNameFieldSpecified;
            }
            set
            {
                this.cinOperator_strNameFieldSpecified = value;
            }
        }

        public string Event_strCode
        {
            get
            {
                return this.event_strCodeField;
            }
            set
            {
                this.event_strCodeField = value;
            }
        }

        public string Event_strFilmsIndependent
        {
            get
            {
                return this.event_strFilmsIndependentField;
            }
            set
            {
                this.event_strFilmsIndependentField = value;
            }
        }

        public string MemberMovie
        {
            get
            {
                return this.memberMovieField;
            }
            set
            {
                this.memberMovieField = value;
            }
        }

        public string HOPK
        {
            get
            {
                return this.hOPKField;
            }
            set
            {
                this.hOPKField = value;
            }
        }

        public int Movie_intList_Pos
        {
            get
            {
                return this.movie_intList_PosField;
            }
            set
            {
                this.movie_intList_PosField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Movie_intList_PosSpecified
        {
            get
            {
                return this.movie_intList_PosFieldSpecified;
            }
            set
            {
                this.movie_intList_PosFieldSpecified = value;
            }
        }
        #endregion
        #region Methods
        //public static List<MovieList> GetMovieList(string CinemaID)
        //{
        //    DataService.GetMovieListRequest req = new DataService.GetMovieListRequest();
        //    req.OptionalCinemaId = CinemaID;
        //    Interpret<MovieList, DataService.GetMovieListRequest> interpret = new Interpret<MovieList, DataService.GetMovieListRequest>();
        //    List<MovieList> Movies = interpret.Get("GetMovieList", req);

        //    return Movies;
        //}
        public static List<MovieList> GetMovieList(string CinemaID)
        {
            //DataService.GetMovieListRequest req = new DataService.GetMovieListRequest();
            //req.OptionalCinemaId = CinemaID;
            //Interpret<MovieList, DataService.GetMovieListRequest> interpret = new Interpret<MovieList, DataService.GetMovieListRequest>();
            //List<MovieList> Movies = interpret.Get("GetMovieList", req);

            //return Movies;

            using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
            {
                GetMovieListRequest req = new GetMovieListRequest();
                req.OptionalCinemaId = CinemaID;
                var respuesta = servicio.GetMovieList(req);
                XElement element = XElement.Parse(respuesta.DatasetXML.ToString());
                List<MovieList> lista = element.Elements("Table")
                    .Select(item => new MovieList()
                    {
                        #region MyRegion
                        Cinema_strID = item.Element("Cinema_strID").Value,
                        CinOperator_strCode = Convert.ToInt32(item.Element("CinOperator_strCode").Value),
                        CinOperator_strCodeSpecified = Convert.ToBoolean(item.Element("CinOperator_strCodeSpecified").Value),
                        CinOperator_strName = Convert.ToInt32(item.Element("CinOperator_strName").Value),
                        CinOperator_strNameSpecified = Convert.ToBoolean(item.Element("CinOperator_strNameSpecified").Value),
                        Event_strCode = item.Element("Event_strCode").Value,
                        Event_strFilmsIndependent = item.Element("Event_strFilmsIndependent").Value,
                        HOPK = item.Element("HOPK").Value,
                        MemberMovie = item.Element("MemberMovie").Value,
                        Movie_HOFilmCode = item.Element("Movie_HOFilmCode").Value,
                        Movie_intFCode = Convert.ToInt32(item.Element("Movie_intFCode").Value),
                        Movie_intFCodeSpecified = Convert.ToBoolean(item.Element("Movie_intFCodeSpecified").Value),
                        Movie_intList_Pos = Convert.ToInt32(item.Element("Movie_intList_Pos").Value),
                        Movie_intList_PosSpecified = Convert.ToBoolean(item.Element("Movie_intList_PosSpecified").Value),
                        Movie_strID = item.Element("Movie_strID").Value,
                        Movie_strName = item.Element("Movie_strName").Value,
                        Movie_strName_2 = item.Element("Movie_strName_2").Value,
                        Movie_strRating = item.Element("Movie_strRating").Value,
                        Movie_strRating_2 = item.Element("Movie_strRating_2").Value
                        #endregion
                    }).ToList();
                return lista;
            }

        }

        #endregion
    }
}
