﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class CinemaListAll
    {
        #region Properties
        public string Cinema_strID { get; set; }
        public string Cinema_strName { get; set; }
        public string Cinema_strPrint_x0040_Home { get; set; }
        public string Cinema_strAllBookingsUnpaid { get; set; }
        public string Cinema_strDisplayName { get; set; }
        public string Cinema_strAllowLoyalty { get; set; }
        public string Cinema_strAllowOnlineVoucherValidation { get; set; }
        public string Cinema_strAllowOnlineVoucherRedemption { get; set; }
        public string Cinema_strIsGiftStore { get; set; }
        public int Cinema_intTimeOffsetInMins { get; set; }
        public string Cinema_strIsGiftStore1 { get; set; }
        #endregion
        #region Methods
        //public static List<CinemaListAll> GetCinemas()
        //{

        //    DataService.GetCinemaListAllRequest rqc = new DataService.GetCinemaListAllRequest();
        //    Interpret<CinemaListAll, DataService.GetCinemaListAllRequest> interpret = new Interpret<CinemaListAll, DataService.GetCinemaListAllRequest>();

        //    List<CinemaListAll> Cinemas = new List<CinemaListAll>();
        //    Cinemas = interpret.Get("GetCinemaListAll", rqc);

        //    return Cinemas;
        //}
        public static List<CinemaListAll> GetCinemas()
        {
            using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
            {
                DataResponse valor = servicio.GetCinemaListAll(new GetCinemaListAllRequest());
                XElement element = XElement.Parse(valor.DatasetXML.ToString());
                List<CinemaListAll> lista = element.Elements("Table")
                    .Select(item => new CinemaListAll()
                    {
                        Cinema_strID = item.Element("Cinema_strID").Value,
                        Cinema_strName = item.Element("Cinema_strName").Value,
                        Cinema_strPrint_x0040_Home = item.Element("Cinema_strPrint_x0040_Home").Value,
                        Cinema_strAllBookingsUnpaid = item.Element("Cinema_strAllBookingsUnpaid").Value,
                        Cinema_strDisplayName = item.Element("Cinema_strDisplayName").Value,
                        Cinema_strAllowLoyalty = item.Element("Cinema_strAllowLoyalty").Value,
                        Cinema_strAllowOnlineVoucherValidation = item.Element("Cinema_strAllowOnlineVoucherValidation").Value,
                        Cinema_strAllowOnlineVoucherRedemption = item.Element("Cinema_strAllowOnlineVoucherRedemption").Value,
                        Cinema_strIsGiftStore = item.Element("Cinema_strIsGiftStore").Value,
                        Cinema_intTimeOffsetInMins = Convert.ToInt32(item.Element("Cinema_intTimeOffsetInMins").Value),
                        Cinema_strIsGiftStore1 = item.Element("Cinema_strIsGiftStore1").Value
                    }).ToList();
                return lista;
            }

        }
        #endregion
    }
}
