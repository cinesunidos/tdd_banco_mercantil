﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class SessionInfo
    {
        #region Fields
        private string cinema_strIDField;

        private string movie_strIDField;

        private string session_strIDField;

        private System.DateTime session_dtmDate_TimeField;

        private bool session_dtmDate_TimeFieldSpecified;

        private int session_decDay_Of_WeekField;

        private bool session_decDay_Of_WeekFieldSpecified;

        private int session_decSeats_AvailableField;

        private bool session_decSeats_AvailableFieldSpecified;

        private string session_strSeatAllocation_OnField;

        private string price_strGroup_CodeField;

        private string session_strChild_AllowedField;

        private string session_strHOSessionIDField;

        private string cinOperator_strCodeField;

        private string session_strNoFreeListField;

        private int screen_bytNumField;

        private bool screen_bytNumFieldSpecified;

        private string screen_strNameField;

        private string sType_strSessionTypeCodeField;

        private string sType_strDescriptionField;

        private string session_strSalesChannelsField;

        private int screen_intRemoteSalesCutoffField;

        private bool screen_intRemoteSalesCutoffFieldSpecified;

        private string event_strCodeField;

        private string session_strAttributesField;

        private string format_strCodeField;

        private string cinema_strID1Field;

        private string movie_strID1Field;

        private string movie_strNameField;

        private string movie_strShortNameField;

        private string movie_strRatingField;

        private string movie_strRating_DescriptionField;

        private int movie_intList_PosField;

        private bool movie_intList_PosFieldSpecified;

        private int movie_intFCodeField;

        private bool movie_intFCodeFieldSpecified;

        private string movie_strChild_AllowedField;

        private string movie_strVoiceFileNameTitleField;

        private string movie_strVoiceFileNameSynopsisField;

        private string movie_HOFilmCodeField;

        private string movie_strRating_2Field;

        private string movie_strRating_Description_2Field;

        private string movie_strName_2Field;

        private string movie_strShortname_2Field;

        private string distrib_strNameField;

        private string hOPKField;

        private int session_intIDField;

        private bool session_intIDFieldSpecified;

        #endregion
        #region Properties

        public string Cinema_strID
        {
            get
            {
                return this.cinema_strIDField;
            }
            set
            {
                this.cinema_strIDField = value;
            }
        }

        public string Movie_strID
        {
            get
            {
                return this.movie_strIDField;
            }
            set
            {
                this.movie_strIDField = value;
            }
        }

        public string Session_strID
        {
            get
            {
                return this.session_strIDField;
            }
            set
            {
                this.session_strIDField = value;
            }
        }

        public System.DateTime Session_dtmDate_Time
        {
            get
            {
                return this.session_dtmDate_TimeField;
            }
            set
            {
                this.session_dtmDate_TimeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_dtmDate_TimeSpecified
        {
            get
            {
                return this.session_dtmDate_TimeFieldSpecified;
            }
            set
            {
                this.session_dtmDate_TimeFieldSpecified = value;
            }
        }

        public int Session_decDay_Of_Week
        {
            get
            {
                return this.session_decDay_Of_WeekField;
            }
            set
            {
                this.session_decDay_Of_WeekField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decDay_Of_WeekSpecified
        {
            get
            {
                return this.session_decDay_Of_WeekFieldSpecified;
            }
            set
            {
                this.session_decDay_Of_WeekFieldSpecified = value;
            }
        }

        public int Session_decSeats_Available
        {
            get
            {
                return this.session_decSeats_AvailableField;
            }
            set
            {
                this.session_decSeats_AvailableField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decSeats_AvailableSpecified
        {
            get
            {
                return this.session_decSeats_AvailableFieldSpecified;
            }
            set
            {
                this.session_decSeats_AvailableFieldSpecified = value;
            }
        }

        public string Session_strSeatAllocation_On
        {
            get
            {
                return this.session_strSeatAllocation_OnField;
            }
            set
            {
                this.session_strSeatAllocation_OnField = value;
            }
        }

        public string Price_strGroup_Code
        {
            get
            {
                return this.price_strGroup_CodeField;
            }
            set
            {
                this.price_strGroup_CodeField = value;
            }
        }

        public string Session_strChild_Allowed
        {
            get
            {
                return this.session_strChild_AllowedField;
            }
            set
            {
                this.session_strChild_AllowedField = value;
            }
        }

        public string Session_strHOSessionID
        {
            get
            {
                return this.session_strHOSessionIDField;
            }
            set
            {
                this.session_strHOSessionIDField = value;
            }
        }

        public string CinOperator_strCode
        {
            get
            {
                return this.cinOperator_strCodeField;
            }
            set
            {
                this.cinOperator_strCodeField = value;
            }
        }

        public string Session_strNoFreeList
        {
            get
            {
                return this.session_strNoFreeListField;
            }
            set
            {
                this.session_strNoFreeListField = value;
            }
        }

        public int Screen_bytNum
        {
            get
            {
                return this.screen_bytNumField;
            }
            set
            {
                this.screen_bytNumField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_bytNumSpecified
        {
            get
            {
                return this.screen_bytNumFieldSpecified;
            }
            set
            {
                this.screen_bytNumFieldSpecified = value;
            }
        }

        public string Screen_strName
        {
            get
            {
                return this.screen_strNameField;
            }
            set
            {
                this.screen_strNameField = value;
            }
        }

        public string SType_strSessionTypeCode
        {
            get
            {
                return this.sType_strSessionTypeCodeField;
            }
            set
            {
                this.sType_strSessionTypeCodeField = value;
            }
        }

        public string SType_strDescription
        {
            get
            {
                return this.sType_strDescriptionField;
            }
            set
            {
                this.sType_strDescriptionField = value;
            }
        }

        public string Session_strSalesChannels
        {
            get
            {
                return this.session_strSalesChannelsField;
            }
            set
            {
                this.session_strSalesChannelsField = value;
            }
        }

        public int Screen_intRemoteSalesCutoff
        {
            get
            {
                return this.screen_intRemoteSalesCutoffField;
            }
            set
            {
                this.screen_intRemoteSalesCutoffField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_intRemoteSalesCutoffSpecified
        {
            get
            {
                return this.screen_intRemoteSalesCutoffFieldSpecified;
            }
            set
            {
                this.screen_intRemoteSalesCutoffFieldSpecified = value;
            }
        }

        public string Event_strCode
        {
            get
            {
                return this.event_strCodeField;
            }
            set
            {
                this.event_strCodeField = value;
            }
        }

        public string Session_strAttributes
        {
            get
            {
                return this.session_strAttributesField;
            }
            set
            {
                this.session_strAttributesField = value;
            }
        }

        public string Format_strCode
        {
            get
            {
                return this.format_strCodeField;
            }
            set
            {
                this.format_strCodeField = value;
            }
        }

        public string Cinema_strID1
        {
            get
            {
                return this.cinema_strID1Field;
            }
            set
            {
                this.cinema_strID1Field = value;
            }
        }

        public string Movie_strID1
        {
            get
            {
                return this.movie_strID1Field;
            }
            set
            {
                this.movie_strID1Field = value;
            }
        }

        public string Movie_strName
        {
            get
            {
                return this.movie_strNameField;
            }
            set
            {
                this.movie_strNameField = value;
            }
        }

        public string Movie_strShortName
        {
            get
            {
                return this.movie_strShortNameField;
            }
            set
            {
                this.movie_strShortNameField = value;
            }
        }

        public string Movie_strRating
        {
            get
            {
                return this.movie_strRatingField;
            }
            set
            {
                this.movie_strRatingField = value;
            }
        }

        public string Movie_strRating_Description
        {
            get
            {
                return this.movie_strRating_DescriptionField;
            }
            set
            {
                this.movie_strRating_DescriptionField = value;
            }
        }

        public int Movie_intList_Pos
        {
            get
            {
                return this.movie_intList_PosField;
            }
            set
            {
                this.movie_intList_PosField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Movie_intList_PosSpecified
        {
            get
            {
                return this.movie_intList_PosFieldSpecified;
            }
            set
            {
                this.movie_intList_PosFieldSpecified = value;
            }
        }

        public int Movie_intFCode
        {
            get
            {
                return this.movie_intFCodeField;
            }
            set
            {
                this.movie_intFCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Movie_intFCodeSpecified
        {
            get
            {
                return this.movie_intFCodeFieldSpecified;
            }
            set
            {
                this.movie_intFCodeFieldSpecified = value;
            }
        }

        public string Movie_strChild_Allowed
        {
            get
            {
                return this.movie_strChild_AllowedField;
            }
            set
            {
                this.movie_strChild_AllowedField = value;
            }
        }

        public string Movie_strVoiceFileNameTitle
        {
            get
            {
                return this.movie_strVoiceFileNameTitleField;
            }
            set
            {
                this.movie_strVoiceFileNameTitleField = value;
            }
        }

        public string Movie_strVoiceFileNameSynopsis
        {
            get
            {
                return this.movie_strVoiceFileNameSynopsisField;
            }
            set
            {
                this.movie_strVoiceFileNameSynopsisField = value;
            }
        }

        public string Movie_HOFilmCode
        {
            get
            {
                return this.movie_HOFilmCodeField;
            }
            set
            {
                this.movie_HOFilmCodeField = value;
            }
        }

        public string Movie_strRating_2
        {
            get
            {
                return this.movie_strRating_2Field;
            }
            set
            {
                this.movie_strRating_2Field = value;
            }
        }

        public string Movie_strRating_Description_2
        {
            get
            {
                return this.movie_strRating_Description_2Field;
            }
            set
            {
                this.movie_strRating_Description_2Field = value;
            }
        }

        public string Movie_strName_2
        {
            get
            {
                return this.movie_strName_2Field;
            }
            set
            {
                this.movie_strName_2Field = value;
            }
        }

        public string Movie_strShortname_2
        {
            get
            {
                return this.movie_strShortname_2Field;
            }
            set
            {
                this.movie_strShortname_2Field = value;
            }
        }

        public string Distrib_strName
        {
            get
            {
                return this.distrib_strNameField;
            }
            set
            {
                this.distrib_strNameField = value;
            }
        }

        public string HOPK
        {
            get
            {
                return this.hOPKField;
            }
            set
            {
                this.hOPKField = value;
            }
        }

        public int Session_intID
        {
            get
            {
                return this.session_intIDField;
            }
            set
            {
                this.session_intIDField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_intIDSpecified
        {
            get
            {
                return this.session_intIDFieldSpecified;
            }
            set
            {
                this.session_intIDFieldSpecified = value;
            }
        }
        #endregion
        #region Methods

        public static SessionInfo GetSessionInfo(string CinemaID, int SessionID)
        {
            //DataService.GetSessionInfoRequest obj = new DataService.GetSessionInfoRequest();
            //obj.CinemaId = CinemaID;
            //obj.SessionId = SessionID.ToString();

            //Interpret<SessionInfo, DataService.GetSessionInfoRequest> interpret = new Interpret<SessionInfo, DataService.GetSessionInfoRequest>();
            //List<SessionInfo> SessionInfos = interpret.Get("GetSessionInfo", obj);

            //return SessionInfos.FirstOrDefault();

            using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
            {
                GetSessionInfoRequest obj = new GetSessionInfoRequest();
                obj.CinemaId = CinemaID;
                obj.SessionId = SessionID.ToString();
                var respuesta = servicio.GetSessionInfo(obj);
                XElement element = XElement.Parse(respuesta.DatasetXML.ToString());
                List<SessionInfo> lista = element.Elements("Table")
                    .Select(item => new SessionInfo()
                    {
                        #region MyRegion
                        Cinema_strID = (item.Elements("Cinema_strID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Cinema_strID").Value)), // item.Element("Cinema_strID").Value,
                        Cinema_strID1 = (item.Elements("Cinema_strID1").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Cinema_strID1").Value)), // item.Element("Cinema_strID1").Value,
                        SType_strSessionTypeCode = (item.Elements("SType_strSessionTypeCode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("SType_strSessionTypeCode").Value)), // item.Element("SType_strSessionTypeCode").Value,
                        SType_strDescription = (item.Elements("SType_strDescription").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("SType_strDescription").Value)), // item.Element("SType_strDescription").Value,
                        CinOperator_strCode = (item.Elements("CinOperator_strCode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("CinOperator_strCode").Value)), //  item.Element("CinOperator_strCode").Value,
                        Distrib_strName = (item.Elements("Distrib_strName").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Distrib_strName").Value)), // item.Element("Distrib_strName").Value,
                        Event_strCode = (item.Elements("Event_strCode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Event_strCode").Value)), // item.Element("Event_strCode").Value,
                        Format_strCode = (item.Elements("Format_strCode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Format_strCode").Value)), // item.Element("Format_strCode").Value,
                        HOPK = (item.Elements("HOPK").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("HOPK").Value)), // item.Element("HOPK").Value,
                        Movie_HOFilmCode = (item.Elements("Movie_HOFilmCode").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_HOFilmCode").Value)), // item.Element("Movie_HOFilmCode").Value,
                        Movie_intFCode = (item.Elements("Movie_intFCode").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Movie_intFCode").Value)), //  Convert.ToInt32(item.Element("Movie_intFCode").Value),
                        Movie_intFCodeSpecified = (item.Elements("Movie_intFCodeSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Movie_intFCodeSpecified").Value)), // Convert.ToBoolean(item.Element("Movie_intFCodeSpecified").Value),
                        Movie_intList_Pos = (item.Elements("Cinema_strID").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Cinema_strID").Value)), // Convert.ToInt32(item.Element("Movie_intList_Pos").Value),
                        Movie_intList_PosSpecified = (item.Elements("Movie_intList_PosSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Movie_intList_PosSpecified").Value)), // (item.Elements("Cinema_strID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Cinema_strID").Value)), // Convert.ToBoolean(item.Element("Movie_intList_PosSpecified").Value),
                        Movie_strChild_Allowed = (item.Elements("Movie_strChild_Allowed").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strChild_Allowed").Value)), // item.Element("Movie_strChild_Allowed").Value,
                        Movie_strID = (item.Elements("Movie_strID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strID").Value)), // item.Element("Movie_strID").Value,
                        Movie_strID1 = (item.Elements("Movie_strID1").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strID1").Value)), // item.Element("Movie_strID1").Value,
                        Movie_strName = (item.Elements("Movie_strName").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strName").Value)), // item.Element("Movie_strName").Value,
                        Movie_strName_2 = (item.Elements("Movie_strName_2").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strName_2").Value)), // item.Element("Movie_strName_2").Value,
                        Movie_strRating = (item.Elements("Movie_strRating").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strRating").Value)), // item.Element("Movie_strRating").Value,
                        Movie_strRating_2 = (item.Elements("Movie_strRating_2").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strRating_2").Value)), // item.Element("Movie_strRating_2").Value,
                        Movie_strRating_Description = (item.Elements("Movie_strRating_Description").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strRating_Description").Value)), // item.Element("Movie_strRating_Description").Value,
                        Movie_strRating_Description_2 = (item.Elements("Movie_strRating_Description_2").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strRating_Description_2").Value)), // item.Element("Movie_strRating_Description_2").Value,
                        Movie_strShortName = (item.Elements("Movie_strShortName").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strShortName").Value)), // item.Element("Movie_strShortName").Value,
                        Movie_strShortname_2 = (item.Elements("Movie_strShortname_2").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strShortname_2").Value)), // item.Element("Movie_strShortname_2").Value,
                        Movie_strVoiceFileNameSynopsis = (item.Elements("Movie_strVoiceFileNameSynopsis").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strVoiceFileNameTitle").Value)), // item.Element("Movie_strVoiceFileNameSynopsis").Value,
                        Movie_strVoiceFileNameTitle = (item.Elements("Movie_strVoiceFileNameTitle").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Movie_strVoiceFileNameTitle").Value)), // item.Element("Movie_strVoiceFileNameTitle").Value,
                        Price_strGroup_Code = (item.Elements("Price_strGroup_Code").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Price_strGroup_Code").Value)), //  item.Element("Price_strGroup_Code").Value,
                        Screen_bytNum = (item.Elements("Screen_bytNum").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Screen_bytNum").Value)), //  Convert.ToInt32(item.Element("Screen_bytNum").Value),
                        Screen_bytNumSpecified = (item.Elements("Screen_bytNumSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Screen_bytNumSpecified").Value)), // Convert.ToBoolean(item.Element("Screen_bytNumSpecified").Value),
                        Screen_intRemoteSalesCutoff = (item.Elements("Screen_intRemoteSalesCutoff").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Screen_intRemoteSalesCutoff").Value)), // Convert.ToInt32(item.Element("Screen_intRemoteSalesCutoff").Value),
                        Screen_intRemoteSalesCutoffSpecified = (item.Elements("Screen_intRemoteSalesCutoffSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Screen_intRemoteSalesCutoffSpecified").Value)), // Convert.ToBoolean(item.Element("Screen_intRemoteSalesCutoffSpecified").Value),
                        Screen_strName = (item.Elements("Screen_strName").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Screen_strName").Value)), // item.Element("Screen_strName").Value,
                        Session_decDay_Of_Week = (item.Elements("Session_decDay_Of_Week").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Session_decDay_Of_Week").Value)), // Convert.ToInt32(item.Element("Session_decDay_Of_Week").Value),
                        Session_decDay_Of_WeekSpecified = (item.Elements("Session_decDay_Of_WeekSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Session_decDay_Of_WeekSpecified").Value)), // Convert.ToBoolean(item.Element("Session_decDay_Of_WeekSpecified").Value),
                        Session_decSeats_Available = (item.Elements("Session_decSeats_Available").FirstOrDefault() == null ? 0 : Convert.ToInt32(item.Element("Session_decSeats_Available").Value)), // Convert.ToInt32(item.Element("Session_decSeats_Available").Value),
                        Session_decSeats_AvailableSpecified = (item.Elements("Session_decSeats_AvailableSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Session_decSeats_AvailableSpecified").Value)), // Convert.ToBoolean(item.Element("Session_decSeats_AvailableSpecified").Value),
                        Session_dtmDate_Time = (item.Elements("Session_dtmDate_Time").FirstOrDefault() == null ? DateTime.MinValue : Convert.ToDateTime(item.Element("Session_dtmDate_Time").Value)), // Convert.ToDateTime(item.Element("Session_dtmDate_Time").Value),
                        Session_dtmDate_TimeSpecified = (item.Elements("Session_dtmDate_TimeSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Session_dtmDate_TimeSpecified").Value)), //  Convert.ToBoolean(item.Element("Session_dtmDate_TimeSpecified").Value),
                        Session_intID = (item.Elements("Session_intID").FirstOrDefault() == null ? 0: Convert.ToInt32(item.Element("Session_intID").Value)), // Convert.ToInt32(item.Element("Session_intID").Value),
                        Session_intIDSpecified = (item.Elements("Session_intIDSpecified").FirstOrDefault() == null ? false : Convert.ToBoolean(item.Element("Session_intIDSpecified").Value)), // Convert.ToBoolean(item.Element("Session_intIDSpecified").Value),
                        Session_strAttributes = (item.Elements("Session_strAttributes").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strAttributes").Value)), // item.Element("Session_strAttributes").Value,
                        Session_strChild_Allowed = (item.Elements("Session_strChild_Allowed").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strChild_Allowed").Value)), // item.Element("Session_strChild_Allowed").Value,
                        Session_strHOSessionID = (item.Elements("Session_strHOSessionID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strHOSessionID").Value)), // item.Element("Session_strHOSessionID").Value,
                        Session_strID = (item.Elements("Session_strID").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strID").Value)), // item.Element("Session_strID").Value,
                        Session_strNoFreeList = (item.Elements("Session_strNoFreeList").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strNoFreeList").Value)), //  item.Element("Session_strNoFreeList").Value,
                        Session_strSalesChannels = (item.Elements("Session_strSalesChannels").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strSalesChannels").Value)), //  item.Element("Session_strSalesChannels").Value,
                        Session_strSeatAllocation_On = (item.Elements("Session_strSeatAllocation_On").FirstOrDefault() == null ? String.Empty : Convert.ToString(item.Element("Session_strSeatAllocation_On").Value)) //  item.Element("Session_strSeatAllocation_On").Value
                        #endregion
                    }).ToList();
                return lista[0];

            }

        }

        public static SessionInfo RestGetSessionInfo(string CinemaID, int SessionID)
        {
            //string url = "http://desaapl02/WSVistaWebClient/OData.svc/Sessions?$filter=CinemaId eq '" + CinemaID + "' and SessionId eq '" + SessionID + "'";
            string url = string.Format(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["RestSessionInfo"].ToString(), CinemaID, SessionID);
            var session = WebConsumer.Get<Services.Vista.DAL.DORest.SessionInfo>(url);

            SessionInfo sessioninfo = new SessionInfo();
            foreach (var i in session.value)
            {
                sessioninfo.Cinema_strID = i.CinemaId;
                sessioninfo.Movie_strID = i.ScheduledFilmId;
                sessioninfo.Session_intID = Convert.ToInt32(i.SessionId);
                sessioninfo.Session_dtmDate_TimeSpecified = false;
                sessioninfo.Session_decDay_Of_WeekSpecified = false;
                sessioninfo.Session_decSeats_AvailableSpecified = false;
                sessioninfo.Session_strSeatAllocation_On = i.IsAllocatedSeating == true ? "Y" : "N";
                sessioninfo.Price_strGroup_Code = i.PriceGroupCode;
                sessioninfo.Session_strChild_Allowed = i.AllowChildAdmits == true ? "Y" : "N";
                sessioninfo.Session_strHOSessionID = "";
                sessioninfo.CinOperator_strCode = i.CinemaOperatorCode;
                sessioninfo.Session_strNoFreeList = i.AllowTicketSales == true ? "Y" : "N";
                sessioninfo.Screen_bytNumSpecified = false;
                sessioninfo.Screen_strName = i.ScreenName;
                sessioninfo.SType_strSessionTypeCode = i.TypeCode;
                sessioninfo.SType_strDescription = "";
                sessioninfo.Session_strSalesChannels = string.Join(";", i.SalesChannels);
                sessioninfo.Screen_intRemoteSalesCutoffSpecified = false;
                sessioninfo.Event_strCode = i.EventId;
                sessioninfo.Session_strAttributes = "";
                sessioninfo.Format_strCode = i.FormatCode;
                sessioninfo.Cinema_strID1 = i.CinemaId;
                sessioninfo.Movie_strID1 = i.ScheduledFilmId;
                sessioninfo.Movie_strName = "";
                sessioninfo.Movie_strShortName = "";
                sessioninfo.Movie_strRating = "";
                sessioninfo.Movie_strRating_Description = "";
                sessioninfo.Movie_intList_PosSpecified = false;
                sessioninfo.Movie_intFCodeSpecified = false;
                sessioninfo.Movie_strChild_Allowed = "";
                sessioninfo.Movie_strVoiceFileNameTitle = "";
                sessioninfo.Movie_strVoiceFileNameSynopsis = "";
                sessioninfo.Movie_HOFilmCode = "";
                sessioninfo.Movie_strRating_2 = "";
                sessioninfo.Movie_strRating_Description_2 = "";
                sessioninfo.Movie_strName_2 = "";
                sessioninfo.Movie_strShortname_2 = "";
                sessioninfo.Distrib_strName = "";
                sessioninfo.HOPK = i.ScheduledFilmId;
                sessioninfo.Session_intIDSpecified = false;
                sessioninfo.Session_dtmDate_Time = i.Showtime;
                sessioninfo.Session_decSeats_Available = i.SeatsAvailable;
            }
            return sessioninfo;
        }


        //public static SessionInfo GetSessionInfo(string CinemaID, int SessionID)
        //{
        //    DataService.GetSessionInfoRequest obj = new DataService.GetSessionInfoRequest();
        //    obj.CinemaId = CinemaID;
        //    obj.SessionId = SessionID.ToString();

        //    Interpret<SessionInfo, DataService.GetSessionInfoRequest> interpret = new Interpret<SessionInfo, DataService.GetSessionInfoRequest>();
        //    List<SessionInfo> SessionInfos = interpret.Get("GetSessionInfo", obj);

        //    return SessionInfos.FirstOrDefault();
        //}
        #endregion
    }
}
