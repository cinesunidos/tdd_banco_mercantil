﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class TicketTypeFromBarcode
    {
        #region Fields
        private string m_TType_strCode;
        private string m_Price_strTicket_Type_Description;       
        private string m_Price_strTicket_Type_Description_2;       
        private int m_AreaCat_intSeq;       
        private string m_TType_strAvailLoyaltyOnly;       
        private string m_AvailableOnSalesChannel;       
        private int m_Price_intTicket_Price;       
        private int m_Price_intSurcharge;      
        private string m_AreaCat_strSeatAllocationOn;
        #endregion
        #region Properties
        public string TType_strCode
        {
            get { return m_TType_strCode; }
            set { m_TType_strCode = value; }
        }
        public string Price_strTicket_Type_Description
        {
            get { return m_Price_strTicket_Type_Description; }
            set { m_Price_strTicket_Type_Description = value; }
        }
        public string Price_strTicket_Type_Description_2
        {
            get { return m_Price_strTicket_Type_Description_2; }
            set { m_Price_strTicket_Type_Description_2 = value; }
        }
        public int AreaCat_intSeq
        {
            get { return m_AreaCat_intSeq; }
            set { m_AreaCat_intSeq = value; }
        }
        public string TType_strAvailLoyaltyOnly
        {
            get { return m_TType_strAvailLoyaltyOnly; }
            set { m_TType_strAvailLoyaltyOnly = value; }
        }
        public string AvailableOnSalesChannel
        {
            get { return m_AvailableOnSalesChannel; }
            set { m_AvailableOnSalesChannel = value; }
        }
        public int Price_intTicket_Price
        {
            get { return m_Price_intTicket_Price; }
            set { m_Price_intTicket_Price = value; }
        }
        public int Price_intSurcharge
        {
            get { return m_Price_intSurcharge; }
            set { m_Price_intSurcharge = value; }
        }
        public string AreaCat_strSeatAllocationOn
        {
            get { return m_AreaCat_strSeatAllocationOn; }
            set { m_AreaCat_strSeatAllocationOn = value; }
        }
        #endregion
        #region Methods

        public static List<TicketTypeFromBarcode> GetTicketTypeFromBarcode(string Barcode, string CinemaID, string SessionID)
        {
            //DataService.GetTicketTypeFromBarcodeRequest req = new DataService.GetTicketTypeFromBarcodeRequest();
            //req.CinemaId = CinemaID;
            //req.SessionId = SessionID;
            //req.Barcode = Barcode;
            //Interpret<TicketTypeFromBarcode, DataService.GetTicketTypeFromBarcodeRequest> interpret = new Interpret<TicketTypeFromBarcode, DataService.GetTicketTypeFromBarcodeRequest>();
            //List<TicketTypeFromBarcode> TicketTypeFromBarcodes = interpret.Get("GetTicketTypeFromBarcode", req);
            //return TicketTypeFromBarcodes;
            using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
            {
                GetTicketTypeFromBarcodeRequest req = new GetTicketTypeFromBarcodeRequest();
                req.CinemaId = CinemaID;
                req.SessionId = SessionID;
                req.Barcode = Barcode;
                var respuesta = servicio.GetTicketTypeFromBarcode(req);
                XElement element = XElement.Parse(respuesta.DatasetXML.ToString());
                List<TicketTypeFromBarcode> lista = element.Elements("Table")
                    .Select(item => new TicketTypeFromBarcode()
                    {
                        #region MyRegion
                        AreaCat_intSeq = Convert.ToInt32(item.Element("SessionDaysSpecified").Value),
                        AreaCat_strSeatAllocationOn = (item.Element("Cinema_strID").Value),
                        AvailableOnSalesChannel = (item.Element("Cinema_strID").Value),
                        Price_intSurcharge = Convert.ToInt32(item.Element("Cinema_strID").Value),
                        Price_intTicket_Price = Convert.ToInt32(item.Element("Cinema_strID").Value),
                        Price_strTicket_Type_Description = Convert.ToString(item.Element("Cinema_strID").Value),
                        Price_strTicket_Type_Description_2 = Convert.ToString(item.Element("Cinema_strID").Value),
                        TType_strAvailLoyaltyOnly = Convert.ToString(item.Element("Cinema_strID").Value),
                        TType_strCode = Convert.ToString(item.Element("Cinema_strID").Value),
                        #endregion
                    }).ToList();
                return lista;
            }
        }


        //public static List<TicketTypeFromBarcode> GetTicketTypeFromBarcode(string Barcode, string CinemaID, string SessionID)
        //{
        //    DataService.GetTicketTypeFromBarcodeRequest req = new DataService.GetTicketTypeFromBarcodeRequest();
        //    req.CinemaId = CinemaID;
        //    req.SessionId = SessionID;
        //    req.Barcode = Barcode;
        //    Interpret<TicketTypeFromBarcode, DataService.GetTicketTypeFromBarcodeRequest> interpret = new Interpret<TicketTypeFromBarcode, DataService.GetTicketTypeFromBarcodeRequest>();
        //    List<TicketTypeFromBarcode> TicketTypeFromBarcodes = interpret.Get("GetTicketTypeFromBarcode", req);
        //    return TicketTypeFromBarcodes;
        //}
        #endregion
    }
}
