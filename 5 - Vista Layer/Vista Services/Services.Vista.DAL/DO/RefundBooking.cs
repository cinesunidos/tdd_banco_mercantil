﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public partial class RefundBookingResponse
    {

        public static RefundBookingResponse GetRefundBooking(string TheaterID, int BookingNumber, int ValueInCents)
        {
            using (BookingService Booking = new BookingService(ConfigurationManager.AppSettings["url"] + "BookingService.asmx"))
            {

                RefundBookingRequest RefundBookingResponseRequest = new RefundBookingRequest();
                RefundBookingResponseRequest.BookingNumber = BookingNumber;
                RefundBookingResponseRequest.CinemaId = TheaterID;
                RefundBookingResponseRequest.IsPriorDayRefund = false;
                //RefundBookingResponseRequest.PaymentInfo = PaymentInfo;
                RefundBookingResponseRequest.RefundAmount = (long)ValueInCents;
                RefundBookingResponseRequest.RefundBookingFee = true;
                RefundBookingResponseRequest.RefundConcessions = true;
                // RefundBookingResponseRequest.RefundReason = "Pruebas";


                return Booking.RefundBooking(RefundBookingResponseRequest);
            }
        }


        //public static RefundBookingResponse GetRefundBooking(string TheaterID, int BookingNumber, int ValueInCents)
        //{            
        //    BookingService Booking = new BookingService();
                        
        //    RefundBookingRequest RefundBookingResponseRequest = new RefundBookingRequest();
        //    RefundBookingResponseRequest.BookingNumber = BookingNumber;
        //    RefundBookingResponseRequest.CinemaId = TheaterID;
        //    RefundBookingResponseRequest.IsPriorDayRefund = false;
        //    //RefundBookingResponseRequest.PaymentInfo = PaymentInfo;
        //    RefundBookingResponseRequest.RefundAmount = (long)ValueInCents;
        //    RefundBookingResponseRequest.RefundBookingFee = true;
        //    RefundBookingResponseRequest.RefundConcessions = true;
        //   // RefundBookingResponseRequest.RefundReason = "Pruebas";
            

        //    return Booking.RefundBooking(RefundBookingResponseRequest);
        //}
    }
}
