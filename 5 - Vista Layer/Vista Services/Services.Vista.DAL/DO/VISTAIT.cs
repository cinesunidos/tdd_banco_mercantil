﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using Services.Vista.BO;
using System.Data;

namespace Services.Vista.DAL.DO
{
    public class VISTAITFilm
    {
        #region Fields       
        private int m_CinemaID;
        private string m_ID;
        private string m_OriginalName;
        private string m_SpanishName;
        private string m_Censor;
        private string m_Duration;
        private string m_OfficialSite;
        private DateTime m_FirstExhibit;
        private string m_Trailer;
        private string m_Synopsis;
        private string m_Gender;
        #endregion
        #region Properties
        public int CinemaID
        {
            get { return m_CinemaID; }
            set { m_CinemaID = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string OriginalName
        {
            get { return m_OriginalName; }
            set { m_OriginalName = value; }
        }
        public string SpanishName
        {
            get { return m_SpanishName; }
            set { m_SpanishName = value; }
        }
        public string Censor
        {
            get { return m_Censor; }
            set { m_Censor = value; }
        }
        public string Duration
        {
            get { return m_Duration; }
            set { m_Duration = value; }
        }
        public string OfficialSite
        {
            get { return m_OfficialSite; }
            set { m_OfficialSite = value; }
        }
        public string Trailer
        {
            get { return m_Trailer; }
            set { m_Trailer = value; }
        }
        public DateTime FirstExhibit
        {
            get { return m_FirstExhibit; }
            set { m_FirstExhibit = value; }
        }
        public string Synopsis
        {
            get { return m_Synopsis; }
            set { m_Synopsis = value; }
        }
        public string Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }
        #endregion
        #region Methods

        #region QueryEnCodigo
        //public static List<VISTAITFilm> GetFilmList(string TheaterID)
        //{
        //    StringBuilder Query = new StringBuilder();
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    parameters.Add("TheaterID", TheaterID);
        //    connection.Open();

        //    List<VISTAITFilm> films = new List<VISTAITFilm>();
        //    SqlDataReader dataReader = connection.ExecuteRead(@"SELECT Mov.Cinema_strID TheaterID
        //                                                    ,Film.[Film_strCode] ID
        //                                                    ,Film.[Film_strTitleAlt] OriginalName
        //                                                    ,Film.[Film_strTitle] SpanishName
        //                                                    ,Mov.[Movie_strRating] Censor
        //                                                    ,Film.[Film_intDuration] Duration
        //                                                    ,Film.[Film_strURL1] OfficialSite
        //                                                    ,Film.[Film_strURLforTrailer] Trailer
        //                                                    ,Film.[Film_dtmOpeningDate] FirstExhibit
        //                                                    ,Film.[Film_strDescriptionLong] Synopsis 
        //                                                    ,FilmCat_strName Gender
        //                                                    FROM [dbo].[tblFilm] Film 
        //                                                    INNER JOIN [dbo].[tblMovies] Mov on  Mov.Movie_strID = Film.[Film_strCode] 
        //                                                    INNER JOIN [dbo].[tblFilmCategory] Cat on Cat.FilmCat_strCode = Film.FilmCat_strCode
        //                                                    WHERE Mov.Cinema_strID = @TheaterID", parameters);

        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            #region MyRegion
        //            VISTAITFilm film = new VISTAITFilm();
        //            film.CinemaID = Int32.Parse(dataReader["TheaterID"].ToString());
        //            film.ID = dataReader["ID"].ToString();
        //            film.OriginalName = dataReader["OriginalName"].ToString();
        //            film.SpanishName = dataReader["SpanishName"].ToString();
        //            film.Censor = dataReader["Censor"].ToString();
        //            film.Duration = dataReader["Duration"].ToString();
        //            film.OfficialSite = dataReader["OfficialSite"].ToString();
        //            film.FirstExhibit = dataReader["FirstExhibit"].ToString();
        //            film.Trailer = dataReader["Trailer"].ToString();
        //            film.Synopsis = dataReader["Synopsis"].ToString();
        //            film.Gender = dataReader["Gender"].ToString();
        //            films.Add(film); 
        //            #endregion
        //        }
        //        dataReader.Close();
        //    }            
        //    connection.Close();
        //    return films;
        //}
        #endregion

        #region StoredProcedureOK
        public static List<VISTAITFilm> GetFilmList(string TheaterID)
        {
            StringBuilder Query = new StringBuilder();
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            //RM
            //modificar el tuoi de sqlcommandType a SqlCommandtype.StoredProcedure
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("TheaterID", TheaterID);
            connection.Open();
            List<VISTAITFilm> films = new List<VISTAITFilm>();
            SqlDataReader dataReader = connection.ExecuteRead(" GetFilmList", parameters);
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    #region MyRegion
                    VISTAITFilm film = new VISTAITFilm();
                    film.CinemaID = Int32.Parse(dataReader["TheaterID"].ToString());
                    film.ID = dataReader["ID"].ToString();
                    film.OriginalName = dataReader["OriginalName"].ToString();
                    film.SpanishName = dataReader["SpanishName"].ToString();
                    film.Censor = dataReader["Censor"].ToString();
                    film.Duration = dataReader["Duration"].ToString();
                    film.OfficialSite = dataReader["OfficialSite"].ToString();
                    film.FirstExhibit = DateTime.Parse(dataReader["FirstExhibit"].ToString());
                    //film.FirstExhibit = dataReader["FirstExhibit"].ToString();
                    film.Trailer = dataReader["Trailer"].ToString();
                    film.Synopsis = dataReader["Synopsis"].ToString();
                    film.Gender = dataReader["Gender"].ToString();
                    films.Add(film);
                    #endregion
                }
                dataReader.Close();
            }
            connection.Close();
            return films;
        }

        public static DatosBoleto ConsultaBoletos(DatosBoleto datosboleto)
        {
            string ConnString = ConfigurationManager.ConnectionStrings["VISTAVM"].ConnectionString;
            SqlConnection con = new SqlConnection(ConnString);
            try
            {
                #region QuerySoloVouchers
                // string Query1 = "SELECT [Redeemed_strScannedBarcode],[sName],[Redeemed_dtmUpdated],[Redeemed_strDescription]," +
                //                "[Session_dtmRealShow],[Workstation_strCode],[User_intUserNo] FROM" +
                //                "[VISTAVM].[dbo].[vw_consultaTU_Boleto] WHERE [Redeemed_strScannedBarcode] = @tbx_CodigoDigital"; 
                #endregion

                #region QueryVouchers&Multivouchers
                string Query1 = @"SELECT[Redeemed_strScannedBarcode],[sName],[Redeemed_dtmUpdated],[Redeemed_strDescription],
                                [Session_dtmRealShow],[Workstation_strCode],[User_intUserNo],
                                (select v.VoucherBatch_intTicketRedemptionsFirstLoaded as numeroEntradas from VISTAVM.dbo.tblStock s
                                inner join VISTAVM.dbo.tblVoucherBatch v on s.lVoucherTypeID = v.lVoucherTypeID and s.lVoucherNumber = v.lVoucherNumber
                                where s.Stock_strBarcode = @CodigoDigital) as numeroEntradas,
                                (select v.VoucherBatch_intTicketRedemptionsRemaining from VISTAVM.dbo.tblStock s
                                inner join VISTAVM.dbo.tblVoucherBatch v on s.lVoucherTypeID = v.lVoucherTypeID and s.lVoucherNumber = v.lVoucherNumber
                                where s.Stock_strBarcode = @CodigoDigital) as entradasRestantes
                                FROM [VISTAVM].[dbo].[vw_consultaTU_Boleto] WHERE[Redeemed_strScannedBarcode] = @CodigoDigital";
                #endregion
                SqlParameter parameters = new SqlParameter();
                parameters.ParameterName = "@CodigoDigital";
                parameters.Value = datosboleto.Stock_strBarcode;
                parameters.DbType = DbType.String;
                SqlCommand cmd = new SqlCommand(Query1, con);
                cmd.Parameters.Add(parameters);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    datosboleto.CodUtilizado = true;
                    datosboleto.BoletoVencido = false;
                    datosboleto.Stock_strBarcode = dr["Redeemed_strScannedBarcode"].ToString();
                    datosboleto.sName = dr["sName"].ToString();
                    datosboleto.Redeemed_dtmUpdated = DateTime.Parse(dr["Redeemed_dtmUpdated"].ToString());
                    datosboleto.Redeemed_strDescription = dr["Redeemed_strDescription"].ToString();
                    datosboleto.Session_dtmRealShow = DateTime.Parse(dr["Session_dtmRealShow"].ToString());
                    datosboleto.Workstation_strCode = dr["Workstation_strCode"].ToString();
                    datosboleto.User_intUserNo = dr["User_intUserNo"].ToString();
                    datosboleto.NumeroEntradas = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
                    datosboleto.EntradasRestantes = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
                    datosboleto.BoletoNoValido = datosboleto.IsMultiVoucher && datosboleto.EntradasRestantes > 0 ? false : true;
                    con.Close();
                    cmd.Dispose();
                    datosboleto.Error = "boleto utilizado";
                    return datosboleto;
                }
                else
                {
                    con.Close();
                    //string Query2 = "SELECT [dExpiryDate],[Stock_strBarcode] FROM [VISTAVM].[dbo].[tblStock] WHERE [Stock_strBarcode] " +
                    //    "= @tbx_CodigoDigital";
                    string Query2 = @"SELECT [dExpiryDate],[Stock_strBarcode], (select v.VoucherBatch_intTicketRedemptionsFirstLoaded as numeroEntradas from VISTAVM.dbo.tblStock s
                                    inner join VISTAVM.dbo.tblVoucherBatch v on s.lVoucherTypeID = v.lVoucherTypeID and s.lVoucherNumber = v.lVoucherNumber
                                    where s.Stock_strBarcode = @tbx_CodigoDigital) as numeroEntradas,
                                    (select v.VoucherBatch_intTicketRedemptionsRemaining from VISTAVM.dbo.tblStock s
                                    inner join VISTAVM.dbo.tblVoucherBatch v on s.lVoucherTypeID = v.lVoucherTypeID and s.lVoucherNumber = v.lVoucherNumber
                                    where s.Stock_strBarcode = @tbx_CodigoDigital) as entradasRestantes
                                    FROM[VISTAVM].[dbo].[tblStock] WHERE[Stock_strBarcode] = @tbx_CodigoDigital";
                    SqlParameter parameters2 = new SqlParameter();
                    parameters2.ParameterName = "@tbx_CodigoDigital";
                    parameters2.Value = datosboleto.Stock_strBarcode;
                    parameters2.DbType = DbType.String;
                    SqlCommand cmd2 = new SqlCommand(Query2, con);
                    cmd2.Parameters.Add(parameters2);
                    con.Open();
                    SqlDataReader dr2 = cmd2.ExecuteReader();
                    if (dr2.HasRows == true)
                    {
                        dr2.Read();
                        datosboleto.BoletoNoValido = false;
                        datosboleto.CodUtilizado = false;
                        datosboleto.BoletoVencido = false;
                        //datosboleto.Stock_strBarcode = dr2["User_intUserNo"].ToString();
                        datosboleto.dExpiryDate = DateTime.Parse(dr2["dExpiryDate"].ToString());
                        bool multiVoucher = dr2.IsDBNull(2) ? false : true;
                        if (multiVoucher)
                        {
                            datosboleto.NumeroEntradas = dr2.GetInt32(2);
                            datosboleto.EntradasRestantes = dr2.GetInt32(3);
                        }
                        con.Close();
                        if (datosboleto.dExpiryDate < DateTime.Now)
                        {
                            datosboleto.BoletoVencido = true;
                            datosboleto.CodUtilizado = false;
                            datosboleto.BoletoNoValido = false;
                            datosboleto.Error = "Este código se encuentra vencido";
                            return datosboleto;
                        }
                        datosboleto.Error = "Este código se encuentra vigente";
                        return datosboleto;
                    }
                    else
                    {
                        datosboleto.BoletoVencido = false;
                        datosboleto.BoletoNoValido = true;
                        datosboleto.CodUtilizado = false;
                        datosboleto.Error = "Por favor, introduzca un código valido";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return datosboleto;
        }
        #endregion
        #endregion
    }

    public class VISTAITSession
    {
        #region Fields        
        private int m_CinemaID;
        private string m_MovieID;
        private string m_SessionID;
        private DateTime m_ShowTime;
        private string m_SeatsAvailable;
        private string m_SeatAllocation;
        private string m_GroupCode;
        private int m_HallNumber;
        private string m_HallName;
        private string m_Channels;
        #endregion
        #region Properties
        public int CinemaID
        {
            get { return m_CinemaID; }
            set { m_CinemaID = value; }
        }
        public string MovieID
        {
            get { return m_MovieID; }
            set { m_MovieID = value; }
        }
        public string SessionID
        {
            get { return m_SessionID; }
            set { m_SessionID = value; }
        }
        public DateTime ShowTime
        {
            get { return m_ShowTime; }
            set { m_ShowTime = value; }
        }
        public string SeatsAvailable
        {
            get { return m_SeatsAvailable; }
            set { m_SeatsAvailable = value; }
        }
        public string SeatAllocation
        {
            get { return m_SeatAllocation; }
            set { m_SeatAllocation = value; }
        }
        public string GroupCode
        {
            get { return m_GroupCode; }
            set { m_GroupCode = value; }
        }
        public int HallNumber
        {
            get { return m_HallNumber; }
            set { m_HallNumber = value; }
        }
        public string HallName
        {
            get { return m_HallName; }
            set { m_HallName = value; }
        }
        public string Channels
        {
            get { return m_Channels; }
            set { m_Channels = value; }
        }
        #endregion
        #region Methods

        #region QueryEnCodigo
        //public static List<VISTAITSession> GetSessionList(string TheaterID)
        //{
        //    StringBuilder Query = new StringBuilder();
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    parameters.Add("TheaterID", TheaterID);
        //    connection.Open();

        //    List<VISTAITSession> sessions = new List<VISTAITSession>();
        //    SqlDataReader dataReader = connection.ExecuteRead("SELECT mov.Cinema_strID CinemaID, mov.Movie_strID MovieID, ses.[Session_strID] SessionID, ses.[Session_dtmDate_Time] AS [DateTime], ses.[Session_decSeats_Available] SeatsAvailable, ses.[Session_strSeatAllocation_On] SeatAllocation, ses.[Price_strGroup_Code] GroupCode, ses.Screen_strName HallName, ses.Screen_bytNum HallNumber, ses.Session_strSalesChannels Channels FROM [dbo].[tblMovies] mov inner join [dbo].[tblSessions] ses on mov.Movie_strID = ses.Movie_strID and mov.Cinema_strID = ses.Cinema_strID WHERE mov.Cinema_strID = @TheaterID order by mov.Cinema_strID,mov.Movie_strID, ses.[Session_dtmDate_Time]", parameters);
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            VISTAITSession session = new VISTAITSession();
        //            session.CinemaID = Int32.Parse(dataReader["CinemaID"].ToString());
        //            session.MovieID = dataReader["MovieID"].ToString();
        //            session.SessionID = dataReader["SessionID"].ToString();
        //            session.ShowTime = dataReader.GetDateTime(3); 
        //            session.SeatsAvailable = dataReader["SeatsAvailable"].ToString();
        //            session.SeatAllocation = dataReader["SeatAllocation"].ToString();
        //            session.GroupCode = dataReader["GroupCode"].ToString();
        //            session.HallName = dataReader["HallName"].ToString();
        //            session.HallNumber = Int32.Parse(dataReader["HallNumber"].ToString());
        //            session.Channels = dataReader["Channels"].ToString();
        //            sessions.Add(session);
        //        }
        //        dataReader.Close();
        //    }
        //    connection.Close();            
        //    return sessions.Where(s => s.ShowTime >= new System.DateTime(System.DateTime.Now.Year,System.DateTime.Now.Month,System.DateTime.Now.Day)).ToList();
        //}
        #endregion

        #region StoredProcedureOK
        public static List<VISTAITSession> GetSessionList(string TheaterID)
        {
            StringBuilder Query = new StringBuilder();
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            //RM
            //modificar el tipo de comantype a SQLComandType.StoredProcedure
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("TheaterID", TheaterID);
            connection.Open();

            List<VISTAITSession> sessions = new List<VISTAITSession>();
            SqlDataReader dataReader = connection.ExecuteRead("GetSessionListV5", parameters);
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    VISTAITSession session = new VISTAITSession();
                    session.CinemaID = Int32.Parse(dataReader["CinemaID"].ToString());
                    session.MovieID = dataReader["MovieID"].ToString();
                    session.SessionID = dataReader["SessionID"].ToString();
                    session.ShowTime = dataReader.GetDateTime(3);
                    session.SeatsAvailable = dataReader["SeatsAvailable"].ToString();
                    session.SeatAllocation = dataReader["SeatAllocation"].ToString();
                    session.GroupCode = dataReader["GroupCode"].ToString();
                    session.HallName = dataReader["HallName"].ToString();
                    session.HallNumber = Int32.Parse(dataReader["HallNumber"].ToString());
                    session.Channels = dataReader["Channels"].ToString();
                    sessions.Add(session);
                }
                dataReader.Close();
            }
            connection.Close();

            var a = sessions.Where(s => s.ShowTime >= new System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day)).ToList();

            //foreach (var item in sessions)
            //{
            //    if (item.ShowTime == new System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day))
            //    {
            //        int sr = 0;
            //    }
            //}



            return a;
        }
        #endregion
        #endregion
    }

    public class VISTAITFilmPerson
    {
        #region Fields       
        private string m_MovieID;
        private string m_PersonType;
        private string m_FirstName;
        private string m_LastName;
        #endregion
        #region Properties
        public string MovieID
        {
            get { return m_MovieID; }
            set { m_MovieID = value; }
        }
        public string PersonType
        {
            get { return m_PersonType; }
            set { m_PersonType = value; }
        }
        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        #endregion
        #region Methods

        #region Queryencodigo
        //public static List<VISTAITFilmPerson> GetPersonList(string TheaterID, List<string> MovieIDs)
        //{
        //    StringBuilder Query = new StringBuilder();
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    string StringMovieIDs = getListAsString(MovieIDs);
        //    connection.Open();
        //
        //    List<VISTAITFilmPerson> filmPersons = new List<VISTAITFilmPerson>();
        //    SqlDataReader dataReader = connection.ExecuteRead(string.Format(@"SELECT FilmPerson.[Film_strCode] MovieID 
        //                                                    ,FilmPerson.[FPerson_strType] PersonType 
        //                                                    ,Person_strFirstName	FirstName 
        //                                                    ,Person_strLastName LastName 
        //                                                    FROM [VISTAIT].[dbo].[tblFilmPerson] FilmPerson 
        //                                                    INNER JOIN [VISTAIT].[dbo].[tblPerson] Person on FilmPerson.Person_strCode = Person.Person_strCode
        //                                                    WHERE FilmPerson.[Film_strCode] in {0}", StringMovieIDs), parameters);
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            VISTAITFilmPerson filmPerson = new VISTAITFilmPerson();
        //            filmPerson.MovieID = dataReader["MovieID"].ToString();
        //            filmPerson.PersonType = dataReader["PersonType"].ToString();
        //            filmPerson.FirstName = dataReader["FirstName"].ToString();
        //            filmPerson.LastName = dataReader["LastName"].ToString();
        //
        //            filmPersons.Add(filmPerson);
        //        }
        //        dataReader.Close();
        //    }
        //    connection.Close();
        //    return filmPersons;
        //}
        #endregion

        #region StoredProcedureOk
        public static List<VISTAITFilmPerson> GetPersonList(string TheaterID, List<string> MovieIDs)
        {
            //StringBuilder Query = new StringBuilder();
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            string StringMovieIDs = getListAsString(MovieIDs);
            connection.Open();
            parameters.Add("StringMovieIDs", StringMovieIDs);
            List<VISTAITFilmPerson> filmPersons = new List<VISTAITFilmPerson>();
            SqlDataReader dataReader = connection.ExecuteRead("GetPersonList", parameters);
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    VISTAITFilmPerson filmPerson = new VISTAITFilmPerson();
                    filmPerson.MovieID = dataReader["MovieID"].ToString();
                    filmPerson.PersonType = dataReader["PersonType"].ToString();
                    filmPerson.FirstName = dataReader["FirstName"].ToString();
                    filmPerson.LastName = dataReader["LastName"].ToString();

                    filmPersons.Add(filmPerson);
                }
                dataReader.Close();
            }
            connection.Close();
            return filmPersons;
        }
        #endregion

        private static string getListAsString(List<string> list)
        {
            string listInString = "(";
            if (list.Count > 0)
            {
                foreach (string item in list)
                {
                    listInString = listInString + string.Format("'{0}'", item);
                    if (item != list[list.Count - 1]) listInString = listInString + ",";
                }
            }
            else
            {
                listInString = listInString + "''";
            }
            listInString = listInString + ")";

            return listInString;
        }
        #endregion
    }

    public class VISTAITCinema
    {
        #region Fields
        private int m_IDTheather;
        private string m_DisplayName;
        private string m_FullName;
        private string m_Address;
        private double m_Latitude;
        private double m_Longitude;
        private string m_City;
        private string m_Phone;
        private string m_Company;
        #endregion

        #region Properties
        public int IDTheather
        {
            get { return m_IDTheather; }
            set { m_IDTheather = value; }
        }
        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }
        public double Latitude
        {
            get { return m_Latitude; }
            set { m_Latitude = value; }
        }
        public double Longitude
        {
            get { return m_Longitude; }
            set { m_Longitude = value; }
        }
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Company
        {
            get { return m_Company; }
            set { m_Company = value; }
        }
        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }
        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }
        #endregion
        #region Methods

        #region queryEnCodigo
        //public static List<VISTAITCinema> getInfoTheaters(string CompanyID)
        //{
        //    String Query;
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    connection.Open();
        //
        //    bool showVistaPIR = false; // IM: Por defecto no se muestra el cine de laboratorio, a menos que exista el key ShowVistaPIR y su valor sea true
        //    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowVistaPIR"]))
        //        showVistaPIR = bool.Parse(ConfigurationManager.AppSettings["ShowVistaPIR"]);
        //    string filtroVistaPIR = showVistaPIR ? "" : " AND  cin.[Cinema_strID] != '1028'";
        //    
        //    List<VISTAITCinema> cinemas = new List<VISTAITCinema>();
        //    Query = string.Format(@"SELECT cin.[Cinema_strID] IDTheater
        //                              ,cin.[Cinema_strName] DisplayName
        //                              ,cin.[Cinema_strName_2] FullName
        //                              ,cin.[Cinema_strAddress1] [Address]
        //                              ,cin.[Cinema_strCity] [City]
        //                              ,cin.[Cinema_decLatitude] [Latitude]
        //                              ,cin.[Cinema_decLongitude] [Longitude]
        //                              ,cin.[Cinema_strAddress2] [Phone]
        //                              ,gro.[CinSiteGroup_strName] [Company]
        //                          FROM [tblCinema] cin
        //                          INNER JOIN [tblCinemaSiteGroupLink] lin on cin.[Cinema_strID] = lin.Cinema_strID
        //                          INNER JOIN [tblCinemaSiteGroup] gro on lin.CinSiteGroup_strCode = gro.CinSiteGroup_strCode
        //                          WHERE gro.[CinSiteGroup_strName] = '{0}'{1};", CompanyID, filtroVistaPIR);
        //
        //    SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
        //
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            VISTAITCinema cinema = new VISTAITCinema();                    
        //            cinema.Address = dataReader["Address"].ToString();
        //            cinema.City = dataReader["City"].ToString();
        //            cinema.Company = dataReader["Company"].ToString();
        //            cinema.DisplayName = dataReader["DisplayName"].ToString();
        //            cinema.FullName = dataReader["FullName"].ToString();
        //            cinema.IDTheather = Int32.Parse(dataReader["IDTheater"].ToString());
        //            if (dataReader["Latitude"] != null && dataReader["Latitude"].ToString() != string.Empty) cinema.Latitude = Double.Parse(dataReader["Latitude"].ToString());
        //            if (dataReader["Longitude"] != null && dataReader["Longitude"].ToString() != string.Empty) cinema.Longitude = Double.Parse(dataReader["Longitude"].ToString());
        //            cinema.Phone = dataReader["Phone"].ToString();
        //
        //            cinemas.Add(cinema);
        //        }
        //        dataReader.Close();
        //    }
        //    connection.Close();
        //    return cinemas;
        //}
        #endregion

        #region StoredProcedureOK
        public static List<VISTAITCinema> getInfoTheaters(string CompanyID)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            connection.Open();

            bool showVistaPIR = false; // IM: Por defecto no se muestra el cine de laboratorio, a menos que exista el key ShowVistaPIR y su valor sea true

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowVistaPIR"]))
            {
                showVistaPIR = bool.Parse(ConfigurationManager.AppSettings["ShowVistaPIR"]);
            }
            string filtroVistaPIR = showVistaPIR ? "Y" : "N";
            parameters.Add("CompanyID", CompanyID);
            parameters.Add("filtroVistaPIR", filtroVistaPIR);
            List<VISTAITCinema> cinemas = new List<VISTAITCinema>();
            SqlDataReader dataReader = connection.ExecuteRead("getInfoTheatersV5", parameters);

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    VISTAITCinema cinema = new VISTAITCinema();
                    cinema.Address = dataReader["Address"].ToString();
                    cinema.City = dataReader["City"].ToString();
                    cinema.Company = dataReader["Company"].ToString();
                    cinema.DisplayName = dataReader["DisplayName"].ToString();
                    cinema.FullName = dataReader["FullName"].ToString();
                    cinema.IDTheather = Int32.Parse(dataReader["IDTheater"].ToString());
                    if (dataReader["Latitude"] != null && dataReader["Latitude"].ToString() != string.Empty) cinema.Latitude = Double.Parse(dataReader["Latitude"].ToString());
                    if (dataReader["Longitude"] != null && dataReader["Longitude"].ToString() != string.Empty) cinema.Longitude = Double.Parse(dataReader["Longitude"].ToString());
                    cinema.Phone = dataReader["Phone"].ToString();

                    cinemas.Add(cinema);
                }
                dataReader.Close();
            }
            connection.Close();
            return cinemas;
        }

        #endregion

        #region QueryEncodigo
        //public static VISTAITCinema getInfoTheater(string Code)
        //{
        //    String Query;
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    connection.Open();

        //    Query = string.Format(@"SELECT cin.[Cinema_strID] IDTheater
        //                              ,cin.[Cinema_strName] DisplayName
        //                              ,cin.[Cinema_strName_2] FullName
        //                              ,cin.[Cinema_strAddress1] [Address]
        //                              ,cin.[Cinema_strCity] [City]
        //                              ,cin.[Cinema_decLatitude] [Latitude]
        //                              ,cin.[Cinema_decLongitude] [Longitude]
        //                              ,cin.[Cinema_strAddress2] [Phone]
        //                              ,gro.[CinSiteGroup_strName] [Company]
        //                          FROM [tblCinema] cin
        //                          INNER JOIN [tblCinemaSiteGroupLink] lin on cin.[Cinema_strID] = lin.Cinema_strID
        //                          INNER JOIN [tblCinemaSiteGroup] gro on lin.CinSiteGroup_strCode = gro.CinSiteGroup_strCode
        //                          WHERE cin.[Cinema_strID] = '{0}';", Code);

        //    SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
        //    VISTAITCinema cinema = new VISTAITCinema();
        //    if (dataReader != null)
        //    {
        //        if (dataReader.Read())
        //        {
        //            cinema.Address = dataReader["Address"].ToString();
        //            cinema.City = dataReader["City"].ToString();
        //            cinema.Company = dataReader["Company"].ToString();
        //            cinema.DisplayName = dataReader["DisplayName"].ToString();
        //            cinema.FullName = dataReader["FullName"].ToString();
        //            cinema.IDTheather = Int32.Parse(dataReader["IDTheater"].ToString());
        //            cinema.Latitude = Double.Parse(dataReader["Latitude"].ToString());
        //            cinema.Longitude = Double.Parse(dataReader["Longitude"].ToString());
        //            cinema.Phone = dataReader["Phone"].ToString();
        //        }
        //        dataReader.Close();
        //    }
        //    connection.Close();
        //    return cinema;
        //}
        #endregion

        #region StoredProcedureOK
        public static VISTAITCinema getInfoTheater(string Code)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            //RM
            //Cambiar el tipo de SqlComandType a SqlCommandTyoe.StoredProcedure
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("code", Code);
            connection.Open();
            SqlDataReader dataReader = connection.ExecuteRead("getInfoTheater", parameters);
            VISTAITCinema cinema = new VISTAITCinema();
            if (dataReader != null)
            {
                if (dataReader.Read())
                {
                    cinema.Address = dataReader["Address"].ToString();
                    cinema.City = dataReader["City"].ToString();
                    cinema.Company = dataReader["Company"].ToString();
                    cinema.DisplayName = dataReader["DisplayName"].ToString();
                    cinema.FullName = dataReader["FullName"].ToString();
                    cinema.IDTheather = Int32.Parse(dataReader["IDTheater"].ToString());
                    cinema.Latitude = Double.Parse(dataReader["Latitude"].ToString());
                    cinema.Longitude = Double.Parse(dataReader["Longitude"].ToString());
                    cinema.Phone = dataReader["Phone"].ToString();
                }
                dataReader.Close();
            }
            connection.Close();
            return cinema;
        }
        #endregion

        /// <summary>
        /// Retorna una lista de todos los cines que estan en la base de datos
        /// para que pueda ser empleados en busquedas que requieran ID de cines
        /// por ejemplo en el metodo GetAllMovies, asi crear un archivo de cache
        /// que evite tener que hacer tantas consultas
        /// </summary>
        /// <returns></returns>
        /// 

        #region QueryEnCodigo
        //public static List<VISTAITCinema> GetAllCinemas()
        //{
        //    String Query;
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    connection.Open();

        //    Query = string.Format(@"SELECT [Cinema_strID] IDTheater
        //                                  ,[Cinema_strName] DisplayName    
        //                              FROM [dbo].[tblCinema];");

        //    SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
        //    VISTAITCinema cinema;
        //    List<VISTAITCinema> cinemas = new List<VISTAITCinema>();
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            cinema = new VISTAITCinema();
        //            cinema.IDTheather = Int32.Parse(dataReader["IDTheater"].ToString());
        //            cinema.DisplayName = dataReader["DisplayName"].ToString();
        //            cinemas.Add(cinema);
        //        }

        //        dataReader.Close();
        //    }
        //    connection.Close();
        //    return cinemas;
        //}
        #endregion

        #region StoredProcedureOK
        public static List<VISTAITCinema> GetAllCinemas()
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            //RM
            //cambiar el tipo de commdandtype
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            bool showVistaPIR = false; // IM: Por defecto no se muestra el cine de laboratorio, a menos que exista el key ShowVistaPIR y su valor sea true

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowVistaPIR"]))
            {
                showVistaPIR = bool.Parse(ConfigurationManager.AppSettings["ShowVistaPIR"]);
            }
            string filtroVistaPIR = showVistaPIR ? "Y" : "N";
            parameters.Add("filtroVistaPIR", filtroVistaPIR);
            connection.Open();
            SqlDataReader dataReader = connection.ExecuteRead("GetAllCinemasV5", parameters);
            VISTAITCinema cinema;
            List<VISTAITCinema> cinemas = new List<VISTAITCinema>();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    cinema = new VISTAITCinema();
                    cinema.IDTheather = Int32.Parse(dataReader["IDTheater"].ToString());
                    cinema.DisplayName = dataReader["DisplayName"].ToString();
                    cinemas.Add(cinema);
                }

                dataReader.Close();
            }
            connection.Close();
            return cinemas;
        }
        #endregion

        #endregion
    }

    public class VISTAITTheaterTicketPrice
    {
        #region QueryEncodigo
        //public static List<TicketPrice> GetTicketPriceListAll(String fecha)
        //{
        //    String Query;
        //    string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
        //    Connection connection = new Connection(ConnectionString);
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    connection.Open();

        //    bool showVistaPIR = false; // IM: Por defecto no se muestra el cine de laboratorio, a menos que exista el key ShowVistaPIR y su valor sea true
        //    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowVistaPIR"]))
        //        showVistaPIR = bool.Parse(ConfigurationManager.AppSettings["ShowVistaPIR"]);
        //    string bookingFee = (ConfigurationManager.AppSettings["BookingFee"]);

        //    string filtroVistaPIR = showVistaPIR ? "" : " AND  SUB_CONS.Cinema_strID != '1028'";

        //    List<TicketPrice> ticketPrice = new List<TicketPrice>();

        //    //Calcular que día de la semana es el jueves            
        //    int dayThursday = ((int)DayOfWeek.Thursday  - (int)Convert.ToDateTime(fecha).DayOfWeek + 7) % 7;
        //    DateTime Thursday = Convert.ToDateTime(fecha).AddDays(dayThursday);

        //    Query = string.Format(@" SELECT DISTINCT
        //                             SUB_CONS.Cinema_strCity as city
        //                            ,SUB_CONS.Cinema_strID as theaterID
        //                            ,SUB_CONS.Cinema_strName as theater
        //                            ,SUB_CONS.Price_strTicket_Type_Description as ticketName
        //                            ,SUB_CONS.Format_strName as format
        //                            ,SUB_CONS.Screen_strNameAlt as typeHall
        //                            ,SUB_CONS.Price_intTicket_Price as price 
        //                            --,CAST(SUB_CONS.Session_dtmDate_Time AS DATE)  AS date
        //                            FROM 
        //                            (
        //                            SELECT DISTINCT
        //                                  [vista_format].[Cinema_strID]
        //                                  ,[vista_format].[Format_strName]
        //                                  ,[Session_dtmDate_Time]
        //                                  ,CASE [Screen_strNameAlt] WHEN 'PREMIUM' THEN [Screen_strNameAlt] ELSE 'NO_APPLY' END AS [Screen_strNameAlt]
        //                                  ,SUBSTRING([Price_strTicket_Type_Description],CHARINDEX('-',[Price_strTicket_Type_Description])+2,LEN([Price_strTicket_Type_Description])) AS [Price_strTicket_Type_Description]                                          
        //                                  ,[Price_intTicket_Price]
        //                                  ,[vista_cinema].Cinema_strCity
        //                                  ,[vista_cinema].Cinema_strName

        //                                  FROM [VISTAIT_CCVISTA4].[dbo].[tblSessions] AS vista_sessions
        //                                  INNER JOIN [VISTAIT_CCVISTA4].[dbo].[tblFormat] AS vista_format 
        //                                  ON vista_format.Format_strCode = vista_sessions.Format_strCode
        //                                  INNER JOIN [VISTAIT_CCVISTA4].[dbo].[tblPrices] AS vista_prices 
        //                                  ON vista_prices.[Price_strGroup_Code] = [vista_sessions].[Price_strGroup_Code] 
        //                                  AND vista_prices.[Cinema_strID] = [vista_sessions].[Cinema_strID] 
        //                                  AND vista_prices.Cinema_strID = vista_sessions.Cinema_strID 
        //                                  INNER JOIN [VISTAIT_CCVISTA4].[dbo].[tblCinema] AS vista_cinema
        //                                  ON vista_sessions.Cinema_strID = vista_cinema.Cinema_strID 
        //                                  and vista_format.Cinema_strID=vista_prices.Cinema_strID
        //                            )SUB_CONS 
        //                            WHERE  
        //                                SUB_CONS.Cinema_strName != 'Parque Costa Azul'
        //                                AND  CAST(SUB_CONS.Session_dtmDate_Time AS DATE) >= '{0}'
        //                                AND CAST(SUB_CONS.Session_dtmDate_Time AS DATE) <= '{1}' {2}
        //                            ORDER BY city ASC, theaterID ASC, format DESC, ticketName ASC", fecha 
        //                                                                                          , Thursday.ToString("yyyy-MM-dd")
        //                                                                                          , filtroVistaPIR);
        //    SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
        //        if (dataReader != null)
        //        {
        //            while (dataReader.Read())
        //            {
        //                TicketPrice tp = new TicketPrice();
        //                tp.city = dataReader["city"].ToString();
        //                tp.theaterID = dataReader["theaterID"].ToString();
        //                tp.theaterName = dataReader["theater"].ToString();
        //                tp.ticketName = dataReader["ticketName"].ToString();
        //                tp.formatMovie = dataReader["format"].ToString();
        //                tp.typeHall = dataReader["typeHall"].ToString();
        //                tp.price = decimal.Parse(dataReader["price"].ToString());
        //            //Se comenta esta atributo para no repetir el listado de boletos por día en el objeto
        //            //tp.date = DateTime.Parse(dataReader["date"].ToString());                        
        //                ticketPrice.Add(tp);
        //        }
        //            dataReader.Close();
        //        }
        //    connection.Close();
        //    return ticketPrice;
        //}
        #endregion

        #region StoredProcedureOK
        public static List<TicketPrice> GetTicketPriceListAll(String fecha)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["VISTAIT"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            connection.comandtype = "SP";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            connection.Open();
            
            bool showVistaPIR = false; // IM: Por defecto no se muestra el cine de laboratorio, a menos que exista el key ShowVistaPIR y su valor sea true
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowVistaPIR"]))
                showVistaPIR = bool.Parse(ConfigurationManager.AppSettings["ShowVistaPIR"]);
            string filtroVistaPIR = showVistaPIR ? "" : "'1028','1030'";
            string bookingFee = (ConfigurationManager.AppSettings["BookingFee"]);
            List<TicketPrice> ticketPrice = new List<TicketPrice>();
            //Calcular que día de la semana es el jueves            
            int dayThursday = ((int)DayOfWeek.Thursday - (int)Convert.ToDateTime(fecha).DayOfWeek + 7) % 7;
            DateTime Thursday = Convert.ToDateTime(fecha).AddDays(dayThursday);
            //fecha = "'" + fecha + "'";
            parameters.Add("fecha", fecha);
            parameters.Add("Thursday", Thursday.ToString("yyyy-MM-dd"));
            parameters.Add("filtroVistaPIR", filtroVistaPIR);
            SqlDataReader dataReader = connection.ExecuteRead("GetTicketPriceListAllV5", parameters);
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    TicketPrice tp = new TicketPrice();
                    tp.city = dataReader["city"].ToString();
                    tp.theaterID = dataReader["theaterID"].ToString();
                    tp.theaterName = dataReader["theater"].ToString();
                    tp.ticketName = dataReader["ticketName"].ToString();
                    tp.formatMovie = dataReader["format"].ToString();
                    tp.typeHall = dataReader["typeHall"].ToString();
                    tp.price = decimal.Parse(dataReader["price"].ToString());
                    //Se comenta esta atributo para no repetir el listado de boletos por día en el objeto
                    //tp.date = DateTime.Parse(dataReader["date"].ToString());                        
                    ticketPrice.Add(tp);
                }
                dataReader.Close();
            }
            connection.Close();
            return ticketPrice;
        }
        #endregion
    }
}