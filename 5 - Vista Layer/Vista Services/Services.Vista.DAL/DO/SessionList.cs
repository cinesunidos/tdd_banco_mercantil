﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class SessionList
    {
        #region Fields
        private string cinema_strIDField;

        private string movie_strIDField;

        private string session_strIDField;

        private System.DateTime session_dtmDate_TimeField;

        private bool session_dtmDate_TimeFieldSpecified;

        private int session_decDay_Of_WeekField;

        private bool session_decDay_Of_WeekFieldSpecified;

        private int session_decSeats_AvailableField;

        private bool session_decSeats_AvailableFieldSpecified;

        private string session_strSeatAllocation_OnField;

        private string price_strGroup_CodeField;

        private string session_strChild_AllowedField;

        private string session_strHOSessionIDField;

        private string cinOperator_strCodeField;

        private string session_strNoFreeListField;

        private int screen_bytNumField;

        private bool screen_bytNumFieldSpecified;

        private string screen_strNameField;

        private string sType_strSessionTypeCodeField;

        private string sType_strDescriptionField;

        private string session_strSalesChannelsField;

        private int screen_intRemoteSalesCutoffField;

        private bool screen_intRemoteSalesCutoffFieldSpecified;

        private string event_strCodeField;

        private int session_intIDField;

        private bool session_intIDFieldSpecified;

        private System.DateTime session_dtmBizDateField;

        private bool session_dtmBizDateFieldSpecified;

        private string session_strAttributesField;

        private string movie_HOFilmCodeField;

        private string hOPKField;
        #endregion
        #region Properties
        public string Cinema_strID
        {
            get
            {
                return this.cinema_strIDField;
            }
            set
            {
                this.cinema_strIDField = value;
            }
        }

        public string Movie_strID
        {
            get
            {
                return this.movie_strIDField;
            }
            set
            {
                this.movie_strIDField = value;
            }
        }

        public string Session_strID
        {
            get
            {
                return this.session_strIDField;
            }
            set
            {
                this.session_strIDField = value;
            }
        }

        public System.DateTime Session_dtmDate_Time
        {
            get
            {
                return this.session_dtmDate_TimeField;
            }
            set
            {
                this.session_dtmDate_TimeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_dtmDate_TimeSpecified
        {
            get
            {
                return this.session_dtmDate_TimeFieldSpecified;
            }
            set
            {
                this.session_dtmDate_TimeFieldSpecified = value;
            }
        }

        public int Session_decDay_Of_Week
        {
            get
            {
                return this.session_decDay_Of_WeekField;
            }
            set
            {
                this.session_decDay_Of_WeekField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decDay_Of_WeekSpecified
        {
            get
            {
                return this.session_decDay_Of_WeekFieldSpecified;
            }
            set
            {
                this.session_decDay_Of_WeekFieldSpecified = value;
            }
        }

        public int Session_decSeats_Available
        {
            get
            {
                return this.session_decSeats_AvailableField;
            }
            set
            {
                this.session_decSeats_AvailableField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decSeats_AvailableSpecified
        {
            get
            {
                return this.session_decSeats_AvailableFieldSpecified;
            }
            set
            {
                this.session_decSeats_AvailableFieldSpecified = value;
            }
        }

        public string Session_strSeatAllocation_On
        {
            get
            {
                return this.session_strSeatAllocation_OnField;
            }
            set
            {
                this.session_strSeatAllocation_OnField = value;
            }
        }

        public string Price_strGroup_Code
        {
            get
            {
                return this.price_strGroup_CodeField;
            }
            set
            {
                this.price_strGroup_CodeField = value;
            }
        }

        public string Session_strChild_Allowed
        {
            get
            {
                return this.session_strChild_AllowedField;
            }
            set
            {
                this.session_strChild_AllowedField = value;
            }
        }

        public string Session_strHOSessionID
        {
            get
            {
                return this.session_strHOSessionIDField;
            }
            set
            {
                this.session_strHOSessionIDField = value;
            }
        }

        public string CinOperator_strCode
        {
            get
            {
                return this.cinOperator_strCodeField;
            }
            set
            {
                this.cinOperator_strCodeField = value;
            }
        }

        public string Session_strNoFreeList
        {
            get
            {
                return this.session_strNoFreeListField;
            }
            set
            {
                this.session_strNoFreeListField = value;
            }
        }

        public int Screen_bytNum
        {
            get
            {
                return this.screen_bytNumField;
            }
            set
            {
                this.screen_bytNumField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_bytNumSpecified
        {
            get
            {
                return this.screen_bytNumFieldSpecified;
            }
            set
            {
                this.screen_bytNumFieldSpecified = value;
            }
        }

        public string Screen_strName
        {
            get
            {
                return this.screen_strNameField;
            }
            set
            {
                this.screen_strNameField = value;
            }
        }

        public string SType_strSessionTypeCode
        {
            get
            {
                return this.sType_strSessionTypeCodeField;
            }
            set
            {
                this.sType_strSessionTypeCodeField = value;
            }
        }

        public string SType_strDescription
        {
            get
            {
                return this.sType_strDescriptionField;
            }
            set
            {
                this.sType_strDescriptionField = value;
            }
        }

        public string Session_strSalesChannels
        {
            get
            {
                return this.session_strSalesChannelsField;
            }
            set
            {
                this.session_strSalesChannelsField = value;
            }
        }

        public int Screen_intRemoteSalesCutoff
        {
            get
            {
                return this.screen_intRemoteSalesCutoffField;
            }
            set
            {
                this.screen_intRemoteSalesCutoffField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_intRemoteSalesCutoffSpecified
        {
            get
            {
                return this.screen_intRemoteSalesCutoffFieldSpecified;
            }
            set
            {
                this.screen_intRemoteSalesCutoffFieldSpecified = value;
            }
        }

        public string Event_strCode
        {
            get
            {
                return this.event_strCodeField;
            }
            set
            {
                this.event_strCodeField = value;
            }
        }

        public int Session_intID
        {
            get
            {
                return this.session_intIDField;
            }
            set
            {
                this.session_intIDField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_intIDSpecified
        {
            get
            {
                return this.session_intIDFieldSpecified;
            }
            set
            {
                this.session_intIDFieldSpecified = value;
            }
        }

        public System.DateTime Session_dtmBizDate
        {
            get
            {
                return this.session_dtmBizDateField;
            }
            set
            {
                this.session_dtmBizDateField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_dtmBizDateSpecified
        {
            get
            {
                return this.session_dtmBizDateFieldSpecified;
            }
            set
            {
                this.session_dtmBizDateFieldSpecified = value;
            }
        }

        public string Session_strAttributes
        {
            get
            {
                return this.session_strAttributesField;
            }
            set
            {
                this.session_strAttributesField = value;
            }
        }

        public string Movie_HOFilmCode
        {
            get
            {
                return this.movie_HOFilmCodeField;
            }
            set
            {
                this.movie_HOFilmCodeField = value;
            }
        }

        public string HOPK
        {
            get
            {
                return this.hOPKField;
            }
            set
            {
                this.hOPKField = value;
            }
        }
        #endregion
        #region Methods

        public static List<SessionList> GetSessionList(string CinemaID, string MovieID)
        {
            //DataService.GetSessionListRequest rq = new DataService.GetSessionListRequest();
            //rq.CinemaId = CinemaID;
            //rq.OptionalMovieId = MovieID;
            ////rq.OptionalBizDate = string.Format("{0}000000",DateTime.Now.ToString("yyyyMMdd")); //si se quiere filtrar por fecha
            //Interpret<SessionList, DataService.GetSessionListRequest> interpret = new Interpret<SessionList, DataService.GetSessionListRequest>();
            //List<SessionList> Sessions = interpret.Get("GetSessionList", rq);

            //return Sessions;

            using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
            {
                GetSessionListRequest rq = new GetSessionListRequest();
                rq.CinemaId = CinemaID;
                rq.OptionalMovieId = MovieID;
                var respuesta = servicio.GetSessionList(rq);
                XElement element = XElement.Parse(respuesta.DatasetXML.ToString());
                List<SessionList> lista = element.Elements("Table")
                    .Select(item => new SessionList()
                    {
                        #region MyRegion
                        Cinema_strID = item.Element("Cinema_strID").Value,
                        CinOperator_strCode = item.Element("Cinema_strID").Value,
                        Event_strCode = item.Element("Cinema_strID").Value,
                        HOPK = item.Element("Cinema_strID").Value,
                        Movie_HOFilmCode = item.Element("Cinema_strID").Value,
                        Movie_strID = item.Element("Cinema_strID").Value,
                        Price_strGroup_Code = item.Element("Cinema_strID").Value,
                        Screen_bytNum = Convert.ToInt32(item.Element("Cinema_strID").Value),
                        Screen_bytNumSpecified = Convert.ToBoolean(item.Element("Cinema_strID").Value),
                        Screen_intRemoteSalesCutoff = Convert.ToInt32(item.Element("Cinema_strID").Value),
                        Screen_intRemoteSalesCutoffSpecified = Convert.ToBoolean(item.Element("Cinema_strID").Value),
                        Screen_strName = item.Element("Cinema_strID").Value,
                        Session_decDay_Of_Week = Convert.ToInt32(item.Element("Cinema_strID").Value),
                        Session_decDay_Of_WeekSpecified = Convert.ToBoolean(item.Element("Cinema_strID").Value),
                        Session_decSeats_Available = Convert.ToInt32(item.Element("Cinema_strID").Value),
                        Session_decSeats_AvailableSpecified = Convert.ToBoolean(item.Element("Cinema_strID").Value),
                        Session_dtmBizDate = Convert.ToDateTime(item.Element("Cinema_strID").Value),
                        Session_dtmBizDateSpecified = Convert.ToBoolean(item.Element("Cinema_strID").Value),
                        Session_dtmDate_Time = Convert.ToDateTime(item.Element("Cinema_strID").Value),
                        Session_dtmDate_TimeSpecified = Convert.ToBoolean(item.Element("Cinema_strID").Value),
                        Session_intID = Convert.ToInt32(item.Element("Cinema_strID").Value),
                        Session_intIDSpecified = Convert.ToBoolean(item.Element("Cinema_strID").Value),
                        Session_strAttributes = item.Element("Cinema_strID").Value,
                        Session_strChild_Allowed = item.Element("Cinema_strID").Value,
                        Session_strHOSessionID = item.Element("Cinema_strID").Value,
                        Session_strID = item.Element("Cinema_strID").Value,
                        Session_strNoFreeList = item.Element("Cinema_strID").Value,
                        Session_strSalesChannels = item.Element("Cinema_strID").Value,
                        Session_strSeatAllocation_On = item.Element("Cinema_strID").Value,
                        SType_strDescription = item.Element("Cinema_strID").Value,
                        SType_strSessionTypeCode = item.Element("Cinema_strID").Value

                        #endregion
                    }).ToList();
                return lista;
            }


        }

//        public static List<SessionList> GetSessionsList()
//        {
//            string Response;
//            GetSessionList SessionList;
//            List<Session> Sessions;
//            WebClient Client = new WebClient();
//            Client.Headers.Add(HttpRequestHeader.Accept, "application/json");
//            //Client.Headers.Add("connectapitoken", "591179922C4E42CB9AAE06F3FC6AC13C");
//            Client.Headers.Add("connectapitoken", "0377744C-F716-4FE3-ACB2-7DA352B7E10D");
//            Client.Headers.Add("tenantId", "111.111.111.114");
//            Client.Headers.Add("correlationId", "Connect-VDT002-abcd1234-nbeb2453");
//#if DEBUG
//            Response = Client.DownloadString("http://desaapl02/WSVistaWebClient/OData.svc/Sessions");
//#else
//            Response = Client.DownloadString("http://api.vista.co.nz/WSVistaWebClient/OData.svc/Sessions");
//#endif
//            SessionList = JsonConvert.DeserializeObject<GetSessionList>(Response);
//            Sessions = SessionList.Sessions;
//            return Sessions;
//            return null;
//        }

        //public static List<SessionList> GetSessionList(string CinemaID, string MovieID)
        //{           
        //    DataService.GetSessionListRequest rq = new DataService.GetSessionListRequest();
        //    rq.CinemaId = CinemaID;
        //    rq.OptionalMovieId = MovieID;         
        //    //rq.OptionalBizDate = string.Format("{0}000000",DateTime.Now.ToString("yyyyMMdd")); //si se quiere filtrar por fecha
        //    Interpret<SessionList, DataService.GetSessionListRequest> interpret = new Interpret<SessionList, DataService.GetSessionListRequest>();
        //    List<SessionList> Sessions = interpret.Get("GetSessionList", rq);

        //    return Sessions;
        //}
        #endregion
    }
}
