﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Services.Vista.DAL.DO
{
    public class Interpret<Out, In>
    {
        public List<Out> Get(string MethodName, In request)
        {
            DataService.DataService client = new DataService.DataService();
            DataService.DataResponse rp = (DataService.DataResponse)client.GetType().GetMethod(MethodName).Invoke(client, new object[] { request });

            List<Out> objs = new List<Out>();
            if (rp.Result == DataService.ResultCode.OK)
            {
                XmlDocument document = new XmlDocument();
                document.InnerXml = rp.DatasetXML;

                int count = 1;
                while (count <= document.ChildNodes[0].ChildNodes.Count - 1)
                {
                    string className = typeof(Out).Name;
                    string value = string.Format("<{0}> {1} </{0}>", className, document.ChildNodes[0].ChildNodes[count].InnerXml);
                    Out obj = (Out)DogFramework.Xml.XmlAdapter.DeserializeFromMemory(value, typeof(Out));

                    objs.Add(obj);
                    count++;
                }
            }
            return objs;
        }
    }
}
