﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Services.Vista.DAL.DO
{
    public class ShowtimeDateList
    {
        #region Fields
        private System.DateTime sessionDaysField;
        private bool sessionDaysFieldSpecified;
        #endregion
        #region Properties
        public System.DateTime SessionDays
        {
            get
            {
                return this.sessionDaysField;
            }
            set
            {
                this.sessionDaysField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SessionDaysSpecified
        {
            get
            {
                return this.sessionDaysFieldSpecified;
            }
            set
            {
                this.sessionDaysFieldSpecified = value;
            }
        }
        #endregion

        public static List<ShowtimeDateList> GetShowtimeDateList()
        {
            //DataService.GetShowtimeDateListRequest req = new DataService.GetShowtimeDateListRequest();
            //req.OptionalBizStartHourOfDay = 0;

            //Interpret<ShowtimeDateList, DataService.GetShowtimeDateListRequest> interpret = new Interpret<ShowtimeDateList, DataService.GetShowtimeDateListRequest>();
            //List<ShowtimeDateList> ShowtimeDates = interpret.Get("GetShowtimeDateList", req);
            //return ShowtimeDates;
            using (DataService servicio = new DataService(ConfigurationManager.AppSettings["url"] + "DataService.asmx"))
            {
                GetShowtimeDateListRequest req = new GetShowtimeDateListRequest();
                req.OptionalBizStartHourOfDay = 0;
                var respuesta = servicio.GetShowtimeDateList(req);
                XElement element = XElement.Parse(respuesta.DatasetXML.ToString());
                List<ShowtimeDateList> lista = element.Elements("Table")
                    .Select(item => new ShowtimeDateList()
                    {
                        #region MyRegion
                        SessionDaysSpecified = Convert.ToBoolean(item.Element("SessionDaysSpecified").Value),
                        SessionDays = Convert.ToDateTime(item.Element("Cinema_strID").Value)
                        #endregion
                    }).ToList();
                return lista;

            }
        }


        //public static List<ShowtimeDateList> GetShowtimeDateList()
        //{
        //    DataService.GetShowtimeDateListRequest req = new DataService.GetShowtimeDateListRequest();
        //    req.OptionalBizStartHourOfDay = 0;

        //    Interpret<ShowtimeDateList, DataService.GetShowtimeDateListRequest> interpret = new Interpret<ShowtimeDateList, DataService.GetShowtimeDateListRequest>();
        //    List<ShowtimeDateList> ShowtimeDates = interpret.Get("GetShowtimeDateList", req);
        //    return ShowtimeDates;
        //}
    }
}
