﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class RefundBookingRequest
    {
        public int BookingNumber { get; set; }
        public string CinemaId { get; set; }
        public bool RefundBookingFee { get; set; }
        public bool RefundConcessions { get; set; }
        public string RefundReason { get; set; }
        public int RefundAmount { get; set; }
        public bool IsPriorDayRefund { get; set; }
        //public PaymentInfo PaymentInfo { get; set; }
        public bool MarkAsRefundedOnly { get; set; }
    }
}
