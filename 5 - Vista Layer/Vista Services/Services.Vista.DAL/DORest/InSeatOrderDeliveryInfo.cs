﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class InSeatOrderDeliveryInfo
    {
        public string Comment { get; set; }
        public string DeliveryWindowDescription { get; set; }
        public string DeliveryWindowValue { get; set; }
    }
}
