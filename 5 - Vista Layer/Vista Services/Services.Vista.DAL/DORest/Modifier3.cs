﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Modifier3
    {
        public string ModifierItemId { get; set; }
        public int Quantity { get; set; }
    }
}
