﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class TicketTypePackage
    {
        public string CinemaId { get; set; }
        public string TicketTypeCode { get; set; }
        public string AreaCategoryCode { get; set; }
        public string HeadOfficeGroupingCode { get; set; }
        public string Description { get; set; }
        public string DescriptionAlt { get; set; }
        public string LongDescription { get; set; }
        public string LongDescriptionAlt { get; set; }
        public bool IsChildOnlyTicket { get; set; }
        public bool IsPackageTicket { get; set; }
        public bool IsRedemptionTicket { get; set; }
        public bool IsComplimentaryTicket { get; set; }
        public string PriceGroupCode { get; set; }
        public int PriceInCents { get; set; }
        public int TaxInCents { get; set; }
        public List<string> SalesChannels { get; set; }
        public string ThirdPartyMembershipName { get; set; }
        public bool IsThirdPartyMemberTicket { get; set; }
        public int DisplaySequence { get; set; }
        public int SurchargeAmount { get; set; }
        public bool IsAvailableForLoyaltyMembersOnly { get; set; }
        public bool IsAvailableAsLoyaltyRecognitionOnly { get; set; }
        public string LoyaltyRecognitionId { get; set; }
        public int LoyaltyRecognitionSequence { get; set; }
        public string LoyaltyBalanceTypeId { get; set; }

        //public int LoyaltyQuantityAvailable { get; set; }
        public int? LoyaltyQuantityAvailable { get; set; }

        //public int LoyaltyPointsCost { get; set; }
        public int? LoyaltyPointsCost { get; set; }

        public string ProductCodeForVoucher { get; set; }
        public int QuantityAvailablePerOrder { get; set; }
        public bool IsDynamicallyPriced { get; set; }

        //public int TotalTicketFeeAmountInCents { get; set; }
        public int? TotalTicketFeeAmountInCents { get; set; }
        public List<DescriptionTranslation> DescriptionTranslations { get; set; }
        public List<LongDescriptionTranslation> LongDescriptionTranslations { get; set; }
        public PackageContent PackageContent { get; set; }
        public List<DiscountsAvailable> DiscountsAvailable { get; set; }
        public string HOPK { get; set; }

        //public int MaxServiceFeeInCents { get; set; }
        public int? MaxServiceFeeInCents { get; set; }

        //public int MinServiceFeeInCents { get; set; }
        public int? MinServiceFeeInCents { get; set; }

        public bool IsAllocatableSeating { get; set; }

        //public int ResalePriceInCents { get; set; }
        public int? ResalePriceInCents { get; set; }

        public bool IsCardPaymentPromotionTicket { get; set; }
        public List<CardPaymentPromotion> CardPaymentPromotions { get; set; }
        public bool EnforceUseOfBarcode { get; set; }
    }
}
