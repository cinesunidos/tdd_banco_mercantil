﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class DiscountsAvailable
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public int PriceToUseInCents { get; set; }
        public int LoyaltyPointsCost { get; set; }
        public int LoyaltyPointsCostDec { get; set; }
        public bool IsLoyaltyOnly { get; set; }
        public int MaxAvailable { get; set; }
    }
}
