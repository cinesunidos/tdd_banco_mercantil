﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class AddConcessionsRequest
    {
        public string UserSessionId { get; set; }
        public string CinemaId { get; set; }
        public Concession[] Concessions { get; set; }
        public bool ReturnOrder { get; set; }
        public bool ProcessOrderValue { get; set; }
        public int BookingMode { get; set; }
        public string SessionId { get; set; }
        public OrderDelivery OrderDelivery { get; set; }
        public bool GiftStoreOrder { get; set; }
        public InSeatOrderDeliveryInfo InSeatOrderDeliveryInfo { get; set; }
        public bool ReplaceExistingConcessions { get; set; }
        public string LinkedBookingId { get; set; }

        public string OptionalClientId { get; set; }

    }
}

