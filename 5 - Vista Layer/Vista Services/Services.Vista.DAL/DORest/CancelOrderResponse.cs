﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class CancelOrderResponse
    {
        public int ExtendedResultCode { get; set; }
        public bool OrderNotFound { get; set; }
        public int Result { get; set; }
        public object ErrorDescription { get; set; }   
    }
}
