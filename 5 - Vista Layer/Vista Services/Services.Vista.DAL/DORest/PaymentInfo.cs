﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class PaymentInfo
    {
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string CardExpiryMonth { get; set; }
        public string CardExpiryYear { get; set; }
        public string CardValidFromMonth { get; set; }
        public string CardValidFromYear { get; set; }
        public string CardIssueNumber { get; set; }
        public int PaymentValueCents { get; set; }
        public string PaymentSystemId { get; set; }
        public string CardCVC { get; set; }
        public string PaymentTenderCategory { get; set; }
        public PaymentVoucher PaymentVoucher { get; set; }
        public bool BillFullOutstandingAmount { get; set; }
        public bool UseAsBookingRef { get; set; }
        public string PaymentErrorCode { get; set; }
        public string PaymentErrorDescription { get; set; }
        public string PaymentStatus { get; set; }
        public int BillingValueCents { get; set; }
        public int CardBalance { get; set; }
        public string BankReference { get; set; }
        public string CardHash { get; set; }
        public string WalletAccessToken { get; set; }
        public bool SaveCardToWallet { get; set; }
        public string BankTransactionNumber { get; set; }
        public string CustomerTaxName { get; set; }
        public string CustomerTaxNumber { get; set; }
        public string MemberId { get; set; }
        public string PaymentToken { get; set; }
        public string PaymentTokenType { get; set; }
        public string BankAuthCode { get; set; }
        public string RefundPaymentMethod { get; set; }
        public string ReversalData { get; set; }
        public List<PaymentConnectorParameter> PaymentConnectorParameters { get; set; }
    }
}
