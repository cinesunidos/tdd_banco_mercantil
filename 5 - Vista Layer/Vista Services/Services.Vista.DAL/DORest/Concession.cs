﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Concession
    {
        public string Id { get; set; }
        public string ItemId { get; set; }
        public int Quantity { get; set; }
        public string PromoCode { get; set; }
        //public string RecognitionId { get; set; }
        public int RecognitionId { get; set; }

        public int RecognitionSequenceNumber { get; set; }
        public bool IsLoyaltyMembershipActivation { get; set; }
        public string HeadOfficeItemCode { get; set; }
        public bool GetBarcodeFromVGC { get; set; }
        public ParentSaleItem ParentSaleItem { get; set; }
        public List<PackageChildItem> PackageChildItems { get; set; }
        public bool IsParentSaleChildItem { get; set; }
        public DeliveryInfo2 DeliveryInfo { get; set; }
        public List<Modifier3> Modifiers { get; set; }
        public List<SmartModifier3> SmartModifiers { get; set; }
        public List<SmartModifierExtra3> SmartModifierExtras { get; set; }
        public string VoucherBarcode { get; set; }
        public int DeliveryOption { get; set; }
        public int? VariablePriceInCents { get; set; }
        public string GiftCardNumber { get; set; }
        public List<RecipeItem2> RecipeItems { get; set; }

    }
}
