﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Card
    {
        public string CardDefinitionId { get; set; }
        public string CardType { get; set; }
        public string BrandName { get; set; }
        public int RangeStart { get; set; }
        public int RangeEnd { get; set; }
        public int SubBrandId { get; set; }
        public string SubBrandName { get; set; }
        public string SubBrandDigits { get; set; }
    }
}
