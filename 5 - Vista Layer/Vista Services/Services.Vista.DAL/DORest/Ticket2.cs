﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Ticket2
    {
        public string Description { get; set; }
        public string DescriptionAlt { get; set; }
        public List<DescriptionTranslation3> DescriptionTranslations { get; set; }
        public string TicketTypeCode { get; set; }
        public int Quantity { get; set; }
    }
}
