﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class CardPaymentPromotion
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<Card> Cards { get; set; }
    }
}
