﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Seat2
    {
        public string RowId { get; set; }
        public string SeatNumber { get; set; }
        public int AreaNumber { get; set; }
        public int ColumnIndex { get; set; }
        public int RowIndex { get; set; }
    }
}
