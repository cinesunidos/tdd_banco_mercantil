﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class ConcessionPackage
    {
        public string Id { get; set; }
        public string HeadOfficeItemCode { get; set; }
        public string Description { get; set; }
        public string DescriptionAlt { get; set; }
        public string ExtendedDescription { get; set; }
        public string ExtendedDescriptionAlt { get; set; }
        public List<DescriptionTranslation2> DescriptionTranslations { get; set; }
        public List<ExtendedDescriptionTranslation> ExtendedDescriptionTranslations { get; set; }
        public int Quantity { get; set; }
    }
}
