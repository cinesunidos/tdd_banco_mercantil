﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class RemoveConcessionsRequest
    {
        public string UserSessionId { get; set; }
        public ConcessionRemoval[] ConcessionRemovals { get; set; }
        public bool ReturnOrder { get; set; }
        public bool RemoveAllConcessions { get; set; }
        public string OptionalClientId { get; set; }
    }
}
