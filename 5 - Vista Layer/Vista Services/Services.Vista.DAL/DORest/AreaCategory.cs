﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class AreaCategory
    {
        public string AreaCategoryCode { get; set; }
        public int SeatsToAllocate { get; set; }
        public int SeatsAllocatedCount { get; set; }
        public int SeatsNotAllocatedCount { get; set; }
        public List<GetSessionSeatDataRequest> SelectedSeats { get; set; }
        public bool IsInSeatDeliveryEnabled { get; set; }
    }
}
