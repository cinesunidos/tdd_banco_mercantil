﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class GetSessionSeatDataRequest
    {
        public string SessionId { get; set; }
        public string UserSessionId { get; set; }
        public string OptionalClientId { get; set; }
        public string CinemaId { get; set; }
        public string SeatData { get; set; }
        public bool excludeAreasWithoutTickets { get; set; }

        public bool includeBrokenSeats { get; set; }

        public bool includeHouseSpecialSeats { get; set; }

        public bool includeGreyAndSofaSeats { get; set; }

        public bool includeAllSeatPriorities { get; set; }

        public bool includeSeatNumbers { get; set; }

        public SeatLayoutData SeatLayoutData { get; set; }
        public int ResponseCode { get; set; }
        public object ErrorDescription { get; set; }
    }
}
