﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class RestRefundResponse
    {
        public int ResultCode { get; set; }
        public string ErrorDescription { get; set; }
        public decimal ExpiredPointsNotRefunded { get; set; }
    }
}
