﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class TicketType
    {
        public string TicketTypeCode { get; set; }
        public int Qty { get; set; }
        public int PriceInCents { get; set; }
        public string OptionalBarcode { get; set; }
        public string OptionalBarcodePin { get; set; }
        public string OptionalTicketTypeProvider { get; set; }
        //public int? BookingFeeOverride { get; set; }
        public ThirdPartyMemberScheme ThirdPartyMemberScheme { get; set; }
        public string LoyaltyRecognitionId { get; set; }
        public int LoyaltyRecognitionSequence { get; set; }
        public int? PromotionTicketTypeId { get; set; }
        public int? PromotionInstanceGroupNumber { get; set; }
        public string OptionalAreaCategoryCode { get; set; }
        public int PriceCardPriceInCents { get; set; }
    }
}
