﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Attribute
    {
        public string ID { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
        public string AltDescription { get; set; }
        public string AltShortName { get; set; }
        public string Message { get; set; }
        public string MessageAlt { get; set; }
        public string WarningMessage { get; set; }
        public string WarningMessageAlt { get; set; }
        public string SalesChannels { get; set; }
        public bool IsUsedForConcepts { get; set; }
        public bool IsUsedForSessionAdvertising { get; set; }
        public int DisplayPriority { get; set; }
        public List<DescriptionTranslation> DescriptionTranslations { get; set; }
        public List<MessageTranslation> MessageTranslations { get; set; }
        public List<string> SessionAttributeCinemaIDs { get; set; }
    }
}
