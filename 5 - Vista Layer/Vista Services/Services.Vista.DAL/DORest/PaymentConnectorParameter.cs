﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class PaymentConnectorParameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
