﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class OrderDelivery
    {
        public bool IsGift { get; set; }
        public bool IsGiftWrapped { get; set; }
        public string GiftMessage { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public DeliveryAddress DeliveryAddress { get; set; }
    }
}
