﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class TicketRequest
    {
        public string UserSessionId { get; set; }
        public string CinemaId { get; set; }
        public string SessionId { get; set; }
        public List<TicketType> TicketTypes { get; set; }
        public bool ReturnOrder { get; set; }
        public bool ReturnSeatData { get; set; }
        public bool ProcessOrderValue { get; set; }
        public bool UserSelectedSeatingSupported { get; set; }
        public bool SkipAutoAllocation { get; set; }
        public bool IncludeAllSeatPriorities { get; set; }
        public bool IncludeSeatNumbers { get; set; }
        public bool ExcludeAreasWithoutTickets { get; set; }
        public bool ReturnDiscountInfo { get; set; }
        //public int BookingFeeOverride { get; set; }
        public int BookingMode { get; set; }
        public bool ReorderSessionTickets { get; set; }
        public List<MultiSessionTicketOrder> MultiSessionTicketOrder { get; set; }
        public string CustomerLanguageTag { get; set; }
        public int ReturnSeatDataFormat { get; set; }
        public List<SelectedSeat> SelectedSeats { get; set; }
        public bool IncludeCompanionSeats { get; set; }

        public TicketRequest()
        {
            this.TicketTypes = new List<TicketType>();
            this.MultiSessionTicketOrder = new List<DORest.MultiSessionTicketOrder>();
            this.SelectedSeats = new List<SelectedSeat>();
        }
    }
}
