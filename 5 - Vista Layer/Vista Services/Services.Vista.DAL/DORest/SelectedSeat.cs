﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class SelectedSeat
    {
        public string AreaCategoryCode { get; set; }
        public int AreaNumber { get; set; }
        public int RowIndex { get; set; }
        public int ColumnIndex { get; set; }
    }
}
