﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class ConcessionRemoval
    {
        public string Id { get; set; }
        public string ItemId { get; set; }
        public int? Quantity { get; set; }
    }
}
