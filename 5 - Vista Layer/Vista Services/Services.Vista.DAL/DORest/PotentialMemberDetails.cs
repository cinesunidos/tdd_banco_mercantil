﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class PotentialMemberDetails
    {
        public bool AllowContactByThirdParty { get; set; }
        public bool AllowProfilingByThirdParty { get; set; }
    }
}
