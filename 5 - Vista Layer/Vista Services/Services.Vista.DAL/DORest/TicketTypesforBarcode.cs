﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class TicketTypesforBarcode
    {
        public int ResponseCode { get; set; }
        public int ExtendedResponseCode { get; set; }
        public string ErrorDescription { get; set; }
        public List<Ticket> Tickets { get; set; }

        public TicketTypesforBarcode()
        {
            Tickets = new List<Ticket>();
        }
    }
}
