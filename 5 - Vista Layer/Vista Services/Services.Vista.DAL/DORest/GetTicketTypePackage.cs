﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{

    public class GetTicketTypePackage
    {
        public int ResponseCode { get; set; }
        public string ErrorDescription { get; set; }
        public List<TicketTypePackage> Tickets { get; set; }
    }
}
