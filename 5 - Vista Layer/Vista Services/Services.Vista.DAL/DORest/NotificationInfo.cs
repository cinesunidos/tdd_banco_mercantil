﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class NotificationInfo
    {
        public string Type { get; set; }
        public string Platform { get; set; }
        public string ApplicationId { get; set; }
        public string DeviceToken { get; set; }
    }
}
