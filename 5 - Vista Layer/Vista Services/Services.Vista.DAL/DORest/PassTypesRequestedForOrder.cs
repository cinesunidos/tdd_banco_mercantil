﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class PassTypesRequestedForOrder
    {
        public bool IncludeApplePassBook { get; set; }
        public bool IncludeICal { get; set; }
        public List<string> AdditionalAttachmentTypes { get; set; }
    }
}
