﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Services.Vista.DAL.DO;
using System.Configuration;
namespace Services.Vista.DAL.DORest
{
    public class TransactionService
    {
        public class TransactionRequest
        {
            public string CinemaId { get; set; }
            public string FirstSixDigits { get; set; }
            public string LastFourDigits { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string TransactionNumber { get; set; }
            public bool GetVoucherStatus { get; set; }
        }

        public class Payment
        {
            public string PaymentId { get; set; }
            public string Type { get; set; }
            public DateTime DateTime { get; set; }
            public string CardNumber { get; set; }
            public string BankReference { get; set; }
            public string Workstation { get; set; }
            public int AmountInCents { get; set; }
        }

        public class Ticket
        {
            public string FilmTitle { get; set; }
            public string ScreenNumber { get; set; }
            public DateTime DateTime { get; set; }
            public string Type { get; set; }
            public int AmountInCents { get; set; }
            public string ReportCode { get; set; }
            public int TransactionSequence { get; set; }
            public int? DealSequence { get; set; }
            public int PackageGroupNumber { get; set; }
            public string PackageGroupDescription { get; set; }
            public string PackageGroupDescriptionShort { get; set; }
            public string FilmHOPK { get; set; }
            public string FilmHOFilmCode { get; set; }
            public string SeatRowId { get; set; }
            public string SeatNumber { get; set; }
            public bool IsRefunded { get; set; }
        }

        public class Concession
        {
            public int? DealSequence { get; set; }
            public int PackageGroupNumber { get; set; }
            public string PackageGroupDescription { get; set; }
            public string PackageGroupDescriptionShort { get; set; }
            public string Item { get; set; }
            public decimal Quantity { get; set; }
            public int AmountInCents { get; set; }
            public string ReportCode { get; set; }
            public string ItemId { get; set; }
            public bool IsBookingFee { get; set; }
            public string ItemClassDescription { get; set; }
            public string ItemClassCode { get; set; }
            public string ItemMasterCode { get; set; }
            public int TransactionSequence { get; set; }
            public bool IsRefunded { get; set; }
        }
        public class Transaction
        {
            public int Number { get; set; }
            public DateTime DateTime { get; set; }
            public int AmountInCents { get; set; }
            public int RemainingAmountInCents { get; set; }
            public string SalesChannel { get; set; }
            public string Workstation { get; set; }
            public List<Payment> Payments { get; set; }
            public List<Ticket> Tickets { get; set; }
            public List<Concession> Concessions { get; set; }
            public string BookingComments { get; set; }
            public string PickupName { get; set; }
        }

        public class TransactionResponse
        {
            public int ResultCode { get; set; }
            public List<Transaction> Transactions { get; set; }
        }

        public static  bool TransactionServiceFind(string CinemaId, string TransactionNumber)
        {
            var Request = new TransactionRequest()
            {
                CinemaId = CinemaId,
                TransactionNumber = TransactionNumber
            };

            var jsonValue = JsonConvert.SerializeObject(Request);
            var response = WebConsumer.Post<TransactionResponse>(ConfigurationManager.AppSettings["urlBase"].ToString() + ConfigurationManager.AppSettings["TransactionServiceFind"].ToString(), jsonValue);

            if ((response == null) || (response.Transactions == null) || (response.Transactions.Count() == 0))
            {
                return false;
            }

            return true;
        }
    }
}
