﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class PackageContent
    {
        public List<ConcessionPackage> Concessions { get; set; }
        public List<Ticket2> Tickets { get; set; }
    }
}
