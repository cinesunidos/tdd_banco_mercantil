﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class PaymentVoucher2
    {
        public string VoucherBarcode { get; set; }
        public string VoucherPin { get; set; }
    }
}
