﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class CompleteOrderRequest
    {
        public string UserSessionId { get; set; }
        public PaymentInfo PaymentInfo { get; set; }
        public List<PaymentInfoCollection> PaymentInfoCollection { get; set; }
        public bool PerformPayment { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string CustomerDateOfBirth { get; set; }
        public string CustomerGender { get; set; }
        public bool GeneratePrintStream { get; set; }
        public bool ReturnPrintStream { get; set; }
        public bool SendBookingConfirmationEmail { get; set; }
        public bool UnpaidBooking { get; set; }
        public string PrintTemplateName { get; set; }
        public string OptionalMemberId { get; set; }
        public bool OptionalReturnMemberBalances { get; set; }
        public string CustomerZipCode { get; set; }
        public int BookingMode { get; set; }
        public int PrintStreamType { get; set; }
        public bool GenerateConcessionVoucherPrintStream { get; set; }
        public PassTypesRequestedForOrder PassTypesRequestedForOrder { get; set; }
        public bool UseAlternateLanguage { get; set; }
        public string BookingNotes { get; set; }
        public string PickupName { get; set; }
        public string CustomerLanguageTag { get; set; }
        public NotificationInfo NotificationInfo { get; set; }
        public string LinkedBookingId { get; set; }
        public PotentialMemberDetails PotentialMemberDetails { get; set; }
        public List<string> Tags { get; set; }
    }
}

