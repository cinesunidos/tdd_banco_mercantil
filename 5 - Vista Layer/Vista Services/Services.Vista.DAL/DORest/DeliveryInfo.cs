﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class DeliveryInfo
    {
        public List<Seat> Seats { get; set; }
        public string Comment { get; set; }
    }
}
