﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class SeatLayoutData
    {
        public List<Area> Areas { get; set; }
        public List<AreaCategory> AreaCategories { get; set; }
        public int BoundaryRight { get; set; }
        public int BoundaryLeft { get; set; }
        public int BoundaryTop { get; set; }
        public int ScreenStart { get; set; }
        public int ScreenWidth { get; set; }
    }
}
