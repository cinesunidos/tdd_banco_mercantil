﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Ticket
    {
        public string CinemaId { get; set; }
        public string TicketTypeCode { get; set; }
        public string TicketCode { get; set; }
        public string AreaCategoryCode { get; set; }
        public string HeadOfficeGroupingCode { get; set; }
        public string Description { get; set; }
        public string DescriptionAlt { get; set; }
        public string LongDescription { get; set; }
        public string LongDescriptionAlt { get; set; }
        public bool IsChildOnlyTicket { get; set; }
        public bool IsPackageTicket { get; set; }
        public bool IsRedemptionTicket { get; set; }
        public bool IsComplimentaryTicket { get; set; }
        public string PriceGroupCode { get; set; }
        public int PriceInCents { get; set; }
        public int TaxInCents { get; set; }
        public List<string> SalesChannels { get; set; }
        public string ThirdPartyMembershipName { get; set; }
        public bool IsThirdPartyMemberTicket { get; set; }
        public int DisplaySequence { get; set; }
        public int SurchargeAmount { get; set; }
        public bool IsAvailableForLoyaltyMembersOnly { get; set; }
        public bool IsAvailableAsLoyaltyRecognitionOnly { get; set; }
        public object LoyaltyRecognitionId { get; set; }
        public int LoyaltyRecognitionSequence { get; set; }
        public object LoyaltyBalanceTypeId { get; set; }
        public object LoyaltyQuantityAvailable { get; set; }
        public object LoyaltyPointsCost { get; set; }
        public string ProductCodeForVoucher { get; set; }
        public int QuantityAvailablePerOrder { get; set; }
        public bool IsDynamicallyPriced { get; set; }
        public object TotalTicketFeeAmountInCents { get; set; }
        public List<string> DescriptionTranslations { get; set; }
        public List<string> LongDescriptionTranslations { get; set; }
        public PackageContent PackageContent { get; set; }
        public object DiscountsAvailable { get; set; }
        public string HOPK { get; set; }
        public object MaxServiceFeeInCents { get; set; }
        public object MinServiceFeeInCents { get; set; }
        public bool IsAllocatableSeating { get; set; }
        public object ResalePriceInCents { get; set; }
        public bool IsCardPaymentPromotionTicket { get; set; }
        public List<string> CardPaymentPromotions { get; set; }
        public bool EnforceUseOfBarcode { get; set; }
    }
}
