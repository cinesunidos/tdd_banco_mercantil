﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class CancelOrderRequest
    {
        public string UserSessionId { get; set; }
    }
}
