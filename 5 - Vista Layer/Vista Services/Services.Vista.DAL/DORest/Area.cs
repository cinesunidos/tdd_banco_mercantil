﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Area
    {
        public int Number { get; set; }
        public string AreaCategoryCode { get; set; }
        public string Description { get; set; }
        public string DescriptionAlt { get; set; }
        public int NumberOfSeats { get; set; }
        public bool IsAllocatedSeating { get; set; }
        public bool HasSofaSeatingEnabled { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public List<Row> Rows { get; set; }
        public int RowCount { get; set; }
        public int ColumnCount { get; set; }
    }
}
