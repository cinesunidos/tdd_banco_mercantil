﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class GetOrderRequest
    {
        public string UserSessionId { get; set; }
        public bool ProcessOrderValue { get; set; }
        public int BookingMode { get; set; }
        public string OptionalClientId { get; set; }
    }
}
