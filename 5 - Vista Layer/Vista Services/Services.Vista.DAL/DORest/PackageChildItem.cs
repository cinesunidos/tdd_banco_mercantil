﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class PackageChildItem
    {
        public string ItemId { get; set; }
        public ParentSaleItem2 ParentSaleItem { get; set; }
        public int Quantity { get; set; }
        public DeliveryInfo DeliveryInfo { get; set; }
        public List<Modifier> Modifiers { get; set; }
        public List<SmartModifier> SmartModifiers { get; set; }
        public List<SmartModifierExtra> SmartModifierExtras { get; set; }
        public List<RecipeItem> RecipeItems { get; set; }
        public bool IsParentSaleChildItem { get; set; }
    }
}
