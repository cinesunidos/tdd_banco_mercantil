﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class DescriptionTranslation2
    {
        public string LanguageTag { get; set; }
        public string Text { get; set; }
    }
}
