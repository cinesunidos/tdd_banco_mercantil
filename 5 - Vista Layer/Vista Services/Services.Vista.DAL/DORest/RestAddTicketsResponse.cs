﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class RestAddTicketsResponse
    {
        public int ExtendedResultCode { get; set; }
        public Order Order { get; set; }
        public string SeatData { get; set; }
        public string AreaSummaryData { get; set; }
        public int SeatDataLength { get; set; }
        public bool SeatsNotAllocated { get; set; }
        public int AvailableSeats { get; set; }
        public int MaxSeatsPerRow { get; set; }
        public Seatlayoutdata SeatLayoutData { get; set; }
        public Sessionstatus[] SessionStatuses { get; set; }
        public object[] RedemptionsRemainingForVouchers { get; set; }
        public int Result { get; set; }
        public object ErrorDescription { get; set; }
    }

    public class Order
    {
        public string UserSessionId { get; set; }
        public string CinemaId { get; set; }
        public int TotalValueCents { get; set; }
        public int TaxValueCents { get; set; }
        public int BookingFeeValueCents { get; set; }
        public int BookingFeeTaxValueCents { get; set; }
        public int TotalOrderCount { get; set; }
        public Session[] Sessions { get; set; }
        public object Concessions { get; set; }
        public Bookingfee[] BookingFees { get; set; }
        public Customer Customer { get; set; }
        public int VistaTransactionNumber { get; set; }
        public int VistaBookingNumber { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime LastUpdatedUtc { get; set; }
        public DateTime CreatedDateUtc { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime ExpiryDateUtc { get; set; }
        public object TotalTicketFeeValueInCents { get; set; }
        public object LoyaltyPointsCost { get; set; }
        public int LoyaltyPointsPayableValueInCents { get; set; }
        public object[] AppliedLoyaltyPointsPayments { get; set; }
        public object[] AppliedGiftCards { get; set; }
        public object[] AppliedPaymentVouchers { get; set; }
        public object[] SuggestedDeals { get; set; }
        public object[] DealVouchers { get; set; }
        public bool HasCardPaymentPromotionTickets { get; set; }
        public object[] PromotionCards { get; set; }
    }

    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public object ID { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public object MobilePhone { get; set; }
        public string Gender { get; set; }
        public object DateOfBirth { get; set; }
        public Address Address { get; set; }
    }

    public class Address
    {
        public object State { get; set; }
        public string ZipCode { get; set; }
        public object Suburb { get; set; }
        public object Address1 { get; set; }
        public object Address2 { get; set; }
        public object City { get; set; }
        public object ValidationReference { get; set; }
    }

    public class Session
    {
        public string CinemaId { get; set; }
        public int SessionId { get; set; }
        public bool AllocatedSeating { get; set; }
        public bool SeatsAllocated { get; set; }
        public TicketRest[] Tickets { get; set; }
        public string FilmTitle { get; set; }
        public string AltFilmTitle { get; set; }
        public object[] FilmTitleTranslations { get; set; }
        public string FilmClassification { get; set; }
        public string AltFilmClassification { get; set; }
        public DateTime ShowingRealDateTime { get; set; }
        public DateTime ShowingRealDateTimeOffset { get; set; }
    }

    public class TicketRest
    {
        public string Id { get; set; }
        public string TicketTypeCode { get; set; }
        public string TicketTypeHOPK { get; set; }
        public string TicketCode { get; set; }
        public int PriceCents { get; set; }
        public object Barcode { get; set; }
        public string SeatData { get; set; }
        public string Description { get; set; }
        public string AltDescription { get; set; }
        public object[] DescriptionTranslations { get; set; }
        public bool IsTicketPackage { get; set; }
        public object[] DiscountsAvailable { get; set; }
        public int? PackageId { get; set; }
        public object LoyaltyRecognitionId { get; set; }
        public int LoyaltyRecognitionSequence { get; set; }
        public Packageticket[] PackageTickets { get; set; }
        public Packageconcession[] PackageConcessions { get; set; }
        public int DiscountPriceCents { get; set; }
        public object PromotionTicketTypeId { get; set; }
        public object PromotionInstanceGroupNumber { get; set; }
        public string SeatNumber { get; set; }
        public string SeatRowId { get; set; }
        public object SeatIsDeliverable { get; set; }
        public string SeatAreaCatCode { get; set; }
        public string SeatAreaNumber { get; set; }
        public int SeatRowIndex { get; set; }
        public int SeatColumnIndex { get; set; }
        public int DealPriceCents { get; set; }
        public object DealDefinitionId { get; set; }
        public object DealDescription { get; set; }
        public int FinalPriceCents { get; set; }
        public int TaxCents { get; set; }
        public int BookingStatus { get; set; }
    }

    public class Packageticket
    {
        public string Id { get; set; }
        public string TicketTypeCode { get; set; }
        public string TicketTypeHOPK { get; set; }
        public string TicketCode { get; set; }
        public int PriceCents { get; set; }
        public object Barcode { get; set; }
        public string SeatData { get; set; }
        public string Description { get; set; }
        public string AltDescription { get; set; }
        public object[] DescriptionTranslations { get; set; }
        public bool IsTicketPackage { get; set; }
        public object[] DiscountsAvailable { get; set; }
        public int PackageId { get; set; }
        public object LoyaltyRecognitionId { get; set; }
        public int LoyaltyRecognitionSequence { get; set; }
        public object PackageTickets { get; set; }
        public object PackageConcessions { get; set; }
        public int DiscountPriceCents { get; set; }
        public object PromotionTicketTypeId { get; set; }
        public object PromotionInstanceGroupNumber { get; set; }
        public string SeatNumber { get; set; }
        public string SeatRowId { get; set; }
        public object SeatIsDeliverable { get; set; }
        public string SeatAreaCatCode { get; set; }
        public string SeatAreaNumber { get; set; }
        public int SeatRowIndex { get; set; }
        public int SeatColumnIndex { get; set; }
        public int DealPriceCents { get; set; }
        public object DealDefinitionId { get; set; }
        public object DealDescription { get; set; }
        public int FinalPriceCents { get; set; }
        public int TaxCents { get; set; }
        public int BookingStatus { get; set; }
    }

    public class Packageconcession
    {
        public string Id { get; set; }
        public string ItemId { get; set; }
        public int Quantity { get; set; }
        public string PromoCode { get; set; }
        public object RecognitionId { get; set; }
        public int RecognitionSequenceNumber { get; set; }
        public bool IsLoyaltyMembershipActivation { get; set; }
        public object HeadOfficeItemCode { get; set; }
        public bool GetBarcodeFromVGC { get; set; }
        public object ParentSaleItem { get; set; }
        public object PackageChildItems { get; set; }
        public bool IsParentSaleChildItem { get; set; }
        public object DeliveryInfo { get; set; }
        public object[] Modifiers { get; set; }
        public object[] SmartModifiers { get; set; }
        public object[] SmartModifierExtras { get; set; }
        public object[] RecipeItems { get; set; }
        public object VoucherBarcode { get; set; }
        public int DeliveryOption { get; set; }
        public object VariablePriceInCents { get; set; }
        public int DealPriceCents { get; set; }
        public object DealDefinitionId { get; set; }
        public object DealDescription { get; set; }
        public object GiftCardNumber { get; set; }
        public int FinalPriceCents { get; set; }
        public int TaxCents { get; set; }
        public object SessionId { get; set; }
        public int BookingStatus { get; set; }
    }

    public class Bookingfee
    {
        public int FinalPriceInCents { get; set; }
        public int TaxInCents { get; set; }
        public int BookingStatus { get; set; }
    }

    public class Seatlayoutdata
    {
        public AreaRest[] Areas { get; set; }
        public Areacategory[] AreaCategories { get; set; }
        public float BoundaryRight { get; set; }
        public float BoundaryLeft { get; set; }
        public float BoundaryTop { get; set; }
        public float ScreenStart { get; set; }
        public float ScreenWidth { get; set; }
    }

    public class AreaRest
    {
        public int Number { get; set; }
        public string AreaCategoryCode { get; set; }
        public string Description { get; set; }
        public string DescriptionAlt { get; set; }
        public int NumberOfSeats { get; set; }
        public bool IsAllocatedSeating { get; set; }
        public bool HasSofaSeatingEnabled { get; set; }
        public float Left { get; set; }
        public float Top { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public RowRest[] Rows { get; set; }
        public int RowCount { get; set; }
        public int ColumnCount { get; set; }
    }

    public class RowRest
    {
        public string PhysicalName { get; set; }
        public SeatRest[] Seats { get; set; }
    }

    public class SeatRest
    {
        public PositionRest Position { get; set; }
        public int Priority { get; set; }
        public string Id { get; set; }
        public int Status { get; set; }
        public int SeatStyle { get; set; }
        public object SeatsInGroup { get; set; }
        public int OriginalStatus { get; set; }
    }

    public class PositionRest
    {
        public int AreaNumber { get; set; }
        public int RowIndex { get; set; }
        public int ColumnIndex { get; set; }
    }

    public class Areacategory
    {
        public string AreaCategoryCode { get; set; }
        public int SeatsToAllocate { get; set; }
        public int SeatsAllocatedCount { get; set; }
        public int SeatsNotAllocatedCount { get; set; }
        public Selectedseat[] SelectedSeats { get; set; }
        public bool IsInSeatDeliveryEnabled { get; set; }
    }

    public class Selectedseat
    {
        public int AreaNumber { get; set; }
        public int RowIndex { get; set; }
        public int ColumnIndex { get; set; }
    }

    public class Sessionstatus
    {
        public int SessionId { get; set; }
        public int NumberOfSeatsAvailable { get; set; }
        public int MaximumNumberOfSeatsPerRow { get; set; }
    }




}
