﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class RecipeItem2
    {
        public string RecipeItemId { get; set; }
        public ParentSaleItem4 ParentSaleItem { get; set; }
        public List<Modifier4> Modifiers { get; set; }
        public List<SmartModifier4> SmartModifiers { get; set; }
        public List<SmartModifierExtra4> SmartModifierExtras { get; set; }
    }
}
