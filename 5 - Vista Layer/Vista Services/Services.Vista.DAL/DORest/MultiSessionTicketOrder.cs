﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class MultiSessionTicketOrder
    {
        public int SessionId { get; set; }
        public List<TicketType2> TicketTypes { get; set; }
    }
}
