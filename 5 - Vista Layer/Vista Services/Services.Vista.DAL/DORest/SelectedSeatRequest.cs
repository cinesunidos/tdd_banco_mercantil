﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class SelectedSeatRequest
    {
        public string CinemaId { get; set; }
        public string SessionId { get; set; }
        public string UserSessionId { get; set; }
        public string SeatData { get; set; }
        public bool ReturnOrder { get; set; }
        public SelectedSeat[] SelectedSeats { get; set; }
        public string OptionalClientId { get; set; }
    }
}
