﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class Seat
    {
        public Position Position { get; set; }
        public int Priority { get; set; }
        public string Id { get; set; }
        public int Status { get; set; }
        public int SeatStyle { get; set; }
        public object SeatsInGroup { get; set; }
        public int OriginalStatus { get; set; }
        public string RowId { get; set; }
        public string SeatNumber { get; set; }
        public int AreaNumber { get; set; }
        public int ColumnIndex { get; set; }
        public int RowIndex { get; set; }
    }
}
