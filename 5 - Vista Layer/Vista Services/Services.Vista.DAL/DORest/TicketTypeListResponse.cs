﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class TicketTypeListResponse
    {
        public int ResponseCode { get; set; }
        public int ExtendedResponseCode { get; set; }
        public object ErrorDescription { get; set; }
        public List<Ticket> Tickets { get; set; }
    }
}
