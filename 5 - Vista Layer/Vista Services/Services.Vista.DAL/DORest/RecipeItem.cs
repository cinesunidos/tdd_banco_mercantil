﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.DAL.DORest
{
    public class RecipeItem
    {
        public string RecipeItemId { get; set; }
        public ParentSaleItem3 ParentSaleItem { get; set; }
        public List<Modifier2> Modifiers { get; set; }
        public List<SmartModifier2> SmartModifiers { get; set; }
        public List<SmartModifierExtra2> SmartModifierExtras { get; set; }
    }
}
