﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class ConcessionItem
    {
        public string ItemStrItemID { get; set; }

        public string ItemHOPK1 { get; set; }

        public string ItemHOPK2 { get; set; }

        public string ItemHOPK3 { get; set; }

        public string ItemMasterItemCode { get; set; }

        public string ItemStrItemDescription { get; set; }

        public decimal ItemPriceDCurPrice { get; set; }

        public string ItemStrItemDescriptionRecipes { get; set; }

        public string ItemStrAbbriation { get; set; }

        public string ItemLocationStrCode { get; set; }

        public string ItemLocationStrDescription { get; set; }

        public decimal ItemStockCurAvailable { get; set; }

        public decimal ItemQuantityPurchase { get; set; }

        public string ItemStockUOMCode { get; set; }

        public string ItemType { get; set; }

        public string VendorStrCode { get; set; }

        public string ClassStrCode { get; set; }

        public decimal RecipeQuantity { get; set; }
        
        public List<ConcessionItem> Ingredients { get; set; }

        public bool IsRecipe
        {
            get
            {
                return ItemType.Trim().Equals("");
            }
        }

    }
}
