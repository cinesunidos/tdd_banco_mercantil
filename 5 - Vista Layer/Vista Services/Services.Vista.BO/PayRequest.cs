﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class PayRequest
    {
        public MerchantIdentify merchant_identify { get; set; }
        public ClientIdentify client_identify { get; set; }
        public Transaction transaction { get; set; }
    }
}
