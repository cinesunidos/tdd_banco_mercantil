﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class SIReduced
    {
        public int SeatsAvailable { get; set; }
        public Boolean OnSale { get; set; }
    }
}
