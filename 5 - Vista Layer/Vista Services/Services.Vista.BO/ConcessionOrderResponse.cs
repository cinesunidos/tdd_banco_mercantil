﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class ConcessionOrderResponse
    {

        public ConcessionOrder Order { get; set; }
        
        public bool WithError { get; set; }

        public string ErrorMessage { get; set; }
    }
}
