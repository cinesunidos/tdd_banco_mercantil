﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{

    public class PayDebitCardResponse
    {
        public bool respuesta { get; set; }
        public PayResponseError payResponseError { get; set; }
        public PayResponse payResponse { get; set; }
    }

    /// <summary>
    /// {"processing_date":"2020-01-14 06:45:32 VET",
    /// "status":{"error_code":"376",
    /// "description":"Saldo insuficiente"}}
    /// </summary>
   public class PayResponseError
    {
        public String processing_date { get; set; }
        public StatusResponse status { get; set; }
    }

    public class StatusResponse
    {
        public String error_code { get; set; }
        public String description { get; set; }
    }


}
