﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Movie
    {
        private List<Session> m_Sessions;
        public Movie()
        {
            m_Sessions = new List<Session>();
        }        

        public string ID { get; set; }
        public string OriginalName { get; set; }
        public string SpanishName { get; set; }
        public string Censor { get; set; }
        public string Gender { get; set; }
        public int Duration { get; set; }
        public string OfficialSite { get; set; }
        public string Trailer { get; set; }
        //public Boolean IsUpComing
        //{
        //    get { return m_IsUpComing; }
        //    set { m_IsUpComing = value; }
        //}
        public DateTime FirstExhibit { get; set; }
        public string Synopsis { get; set; }
        public string Format { get; set; }
        public List<Session> Sessions { get; set; }
        public string Actor { get; set; }
        public string Director { get; set; }
        public string Subtitle { get; set; }
        public string OriginCountry { get; set; }
        public string NacionalFilm { get; set; }

        //public string ShortTitle
        //{
        //    get { return m_ShortTitle; }
        //    set { m_ShortTitle = value; }
        //}       
        //public Boolean Is3D
        //{
        //    get { return m_Is3D; }
        //    set { m_Is3D = value; }
        //}
        //public Boolean IsForKids
        //{
        //    get { return m_IsForKids; }
        //    set { m_IsForKids = value; }
        //}
        //public string Awards
        //{
        //    get { return m_Awards; }
        //    set { m_Awards = value; }
        //}
        //public string Company
        //{
        //    get { return m_Company; }
        //    set { m_Company = value; }
        //}
        //public string Director
        //{
        //    get { return m_Director; }
        //    set { m_Director = value; }
        //}
        //public string Distributor
        //{
        //    get { return m_Distributor; }
        //    set { m_Distributor = value; }
        //}
        //public string Language
        //{
        //    get { return m_Language; }
        //    set { m_Language = value; }
        //}

        //public string Producer
        //{
        //    get { return m_Producer; }
        //    set { m_Producer = value; }
        //}

        //public string Starring
        //{
        //    get { return m_Starring; }
        //    set { m_Starring = value; }
        //}
        //public string Writer
        //{
        //    get { return m_Writer; }
        //    set { m_Writer = value; }
        //}        
      
        public override bool Equals(object obj)
        {
            if (obj is Movie)
            {
                if (obj.GetHashCode().Equals(this.GetHashCode()))
                    return true;
                else return false;
            }
            else return false;
        }
        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
    }
}
