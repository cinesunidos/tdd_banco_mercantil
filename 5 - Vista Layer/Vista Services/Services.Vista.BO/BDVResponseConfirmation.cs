﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class BDVResponseConfirmation
    {
        public int responseCode { get; set; }
        public string responseDescription { get; set; }
        public int result { get; set; }
        public BDVResponseDetail detail { get; set; }
    }
}
