﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class BDVTokenResponse
    {
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
    }
}
