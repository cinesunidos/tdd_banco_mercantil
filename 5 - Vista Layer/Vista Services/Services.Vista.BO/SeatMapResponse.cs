﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    /// <summary>
    /// Fecha:16/05/2017
    /// Luis Ramírez
    /// Clase que obtiene el mapa de las butacas en string (Para plataforma móvil e informacion de los tickets)
    /// </summary>
    public class SeatMapResponse
    {
        public string SeatData { get; set; }

        public Ticket[] Tickets { get; set; }
    }
}
