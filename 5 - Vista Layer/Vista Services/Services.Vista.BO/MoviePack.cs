﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class MoviePack
    {
        public List<Movie> Movies;
        public List<Movie> CoomingSoon;
        public String TheaterID { get; set; }
        public String CinemaName { get; set; }
    }
}
