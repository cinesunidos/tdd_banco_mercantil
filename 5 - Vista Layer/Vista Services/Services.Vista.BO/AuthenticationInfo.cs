﻿namespace Services.Vista.BO
{
    public class AuthenticationInfo
    {
        public string procesing_date { get; set; }
        public string trx_status { get; set; }
        public string trx_type { get; set; }
        public string payment_method { get; set; }
        public string twofactor_type { get; set; }
    }
}