﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Tier
    {
        public List<Seat> Seats { get; set; }
        public Char Name { get; set; }
    }
}
