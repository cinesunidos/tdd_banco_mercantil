﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class ConcessionListResponse
    {
        public List<ConcessionItem> Items { get; set; }
        public ConcessionItem BookingFee { get; set; }
        //public List<BookingFee> BookingFees { get; set; }
    }
}
