﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class BDVResponseDetail
    {
        public string token { get; set; }
        public int status { get; set; }
        public int currency { get; set; }
        public double amount { get; set; }
        public string reference { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string letter { get; set; }
        public string number { get; set; }
        public int transactionId { get; set; }
        public object paymentMethodCode { get; set; }
        public string paymentMethodDescription { get; set; }
        public string authorizationCode { get; set; }
        public string paymentMethodNumber { get; set; }
        public string paymentDate { get; set; }
    }
}
