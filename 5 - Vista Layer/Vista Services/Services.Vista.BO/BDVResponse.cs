﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class BDVResponse
    {
        public string responseCode { get; set; }
        public string paymentId { get; set; }
        public string urlPayment { get; set; }
    }
}
