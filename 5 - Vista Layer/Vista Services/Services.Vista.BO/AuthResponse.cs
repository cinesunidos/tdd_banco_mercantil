﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class AuthResponse
    {
        public MerchantIdentify merchant_identify { get; set; }
        public AuthenticationInfo authentication_info { get; set; }
    }
}
