﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class AVippoResponse
    {
        public bool Success { get; set; }
        public int RtnCde { get; set; }
        public string Message { get; set; }
        public string AuthenticationType { get; set; }
        public string SessionToken { get; set; }
        public string SessionExpires { get; set; }
        public string APIKEY { get; set; }
        public string VippoCommerce { get; set; }
        public string WebCommerce { get; set; }
        public string Payment_RRN { get; set; }
        public string Payment_Date { get; set; }
        public string Merchant_Vippo { get; set; }
        public string Client_Vippo { get; set; }
        public string Amount { get; set; }
        public string Reference { get; set; }
        public string Label { get; set; }

    }
}
