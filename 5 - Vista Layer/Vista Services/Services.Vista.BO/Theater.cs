﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Theater
    {
        private List<Facilities> l_facilities;
        public Theater()
        {
            l_facilities = new List<Facilities>();
        }

        #region Properties

        public String ID { get; set; }
        public String FullName { get; set; }
        public String DisplayName { get; set; }
        public String City { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public String Address { get; set; }
        public String Phone { get; set; }

        public List<Facilities> P_Facilities { get; set; }

        //public string Code
        //{
        //    get { return m_Code; }
        //    set { m_Code = value; }
        //}
        //public string ShortName
        //{
        //    get { return m_ShortName; }
        //    set { m_ShortName = value; }
        //}

        //public Boolean Premium
        //{
        //    get { return m_Premium; }
        //    set { m_Premium = value; }
        //}
        //public Boolean TheaterActive
        //{
        //    get { return m_TheaterActive; }
        //    set { m_TheaterActive = value; }
        //}
        #endregion
    }
}
