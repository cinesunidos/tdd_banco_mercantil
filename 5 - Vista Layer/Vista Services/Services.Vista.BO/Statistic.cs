﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Statistic
    {
        public Statistic() {}
        public Statistic(string Name, DateTime StartDate)
        {
            this.Name = Name;
            this.StartDate = StartDate;
        }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MilisecondsWaitTime 
        { 
            get { return (EndDate - StartDate).Milliseconds; } 
            set { } 
        }

        private int mili;

        public int Mili
        {
            get { return mili; }
            set { mili = value; }
        }
    }
    
}
