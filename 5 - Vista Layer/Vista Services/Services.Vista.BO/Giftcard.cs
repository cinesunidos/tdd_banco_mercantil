﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Services.Vista.BO
{
    public class GiftCard
    {
        [JsonProperty(PropertyName = "CardNumber")]
        public string CardNumber { get; set; }

        [JsonProperty(PropertyName = "CardExpiry")]
        public string CardExpiry { get; set; }

        public string CardExpiryMonth { get; set; }

        public string CardExpiryYear { get; set; }

        [JsonProperty(PropertyName = "BalanceInCents")]
        public decimal Balance { get; set; }

        public bool IsOk { get; set; }

        public List<string> Messages { get; set; }

    }
}
