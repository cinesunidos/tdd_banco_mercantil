﻿namespace Services.Vista.BO
{
    public class Mobile
    {
        public string manufacturer { get; set; }
        public string model { get; set; }
        public string os_version { get; set; }
        public Location location { get; set; }
    }
}