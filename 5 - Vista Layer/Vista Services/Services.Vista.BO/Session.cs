﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class Session
    {
        public Session()
        {
            //m_Theater = new Theater();
            //m_Movie = new Movie();
        }

        public int ID { get; set; }
        public DateTime ShowTime { get; set; }
        public string PriceGroup { get; set; }
        public Boolean SeatAllocated { get; set; }
        public int SeatsAvailable { get; set; }
        public int HallNumber { get; set; }

        public string HallName { get; set; }

        public Boolean OnSale { get; set; }

        /// <summary>
        /// Se creo para diferenciar la censura por cine y asi poder mostrar las diferentes censuras
        /// en una sola pelicula en un archivo que sera procesado por las app moviles.
        /// Se coloco aqui para poder agruparla en el formato que Pedro de mobilemedia
        /// pueda mostrar sin repetir la pelicula y por censura.
        /// La censura puede variar por municipio.
        /// </summary>
        public string Censor { get; set; }

        #region Fields
        //    private Movie m_Movie;
        //   private Theater m_Theater;        
        #endregion
        #region Properties
        //public Movie Movie
        //{
        //    get { return m_Movie; }
        //    set { m_Movie = value; }
        //}
        //public Theater Theater
        //{
        //    get { return m_Theater; }
        //    set { m_Theater = value; }
        //}
        #endregion
    }
}
