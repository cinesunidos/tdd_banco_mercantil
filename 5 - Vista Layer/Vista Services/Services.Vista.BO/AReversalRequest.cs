﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class AReversalRequest
    {
        public string APIKEY { get; set; }
        public string Merchant_Vippo { get; set; }
        public string Merchant_Reference { get; set; }
        public string Amount { get; set; }
        public string Client_UserVippo { get; set; }
        public string Client_PasswordVippo { get; set; }
        public string AuthenticationType { get; set; }
    }
}
