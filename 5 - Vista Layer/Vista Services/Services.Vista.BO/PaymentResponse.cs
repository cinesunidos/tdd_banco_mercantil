﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Services.Vista.BO
{
    public class PaymentResponse
    {
        //en caso de anulacion en falla de merchant
        public string SequenceNumber { get; set; }
        #region Fields
        private List<string> m_Voucher;
        #endregion
        //Localizador Alfanúmerico
        public string BookingId { get; set; }
        //Localizador númerico de seis digitos 123456
        public int BookingNumber { get; set; }
        public string CodeResult { get; set; }
        public string DescriptionResult { get; set; }
        public int TransactionNumber { get; set; }        
        public List<string> Voucher
        {
            get { return m_Voucher; }            
        }
        public string VoucherHTML
        {
            get
            {
                string VoucherHTML = "";
                foreach (string item in Voucher)
                {
                    VoucherHTML =  VoucherHTML + item;
                    VoucherHTML = VoucherHTML + "<br>";
                }
                return VoucherHTML; 
            }
        }
        public void SetVoucher(string str)
        {
            List<string> Lineas = new List<string>();
            string[] vect = Regex.Split(str, "<linea>");
            foreach (string l in vect)
            {
                string linea = l.Replace("</linea>", "").Replace("<UT>","").Replace("</UT>","");
                linea = linea.Trim();
                Lineas.Add(linea);
            }
            m_Voucher = Lineas;
        }
    }
}
