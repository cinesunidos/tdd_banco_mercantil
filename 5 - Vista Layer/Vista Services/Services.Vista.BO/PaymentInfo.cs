﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public enum PaymentTenderCategory
    {
        CREDIT = 0,
        DEBIT = 1,
        SVC = 2,
    }
    public enum PaymentCardType : int
    {
        VISA = 0,
        MASTERCARD = 1,
        AMERICANEXPRESS = 2,
        GIFTCARD = 3,
        PAGOVIPPO = 5,
        PAGOVZLATDD = 6,
        PAGOVZLATDC = 7,
        PAGOMERCTDD = 8,
        PAGOMERCTDC = 9
    }
    public class PaymentInfo
    {
        #region Properties

        public string CardCVV { get; set; }

        public int CardExpiryMonth { get; set; }

        public int CardExpiryYear { get; set; }
        public string CardNumber { get; set; }
        public PaymentCardType CardType { get; set; }
        public PaymentTenderCategory PaymentTenderCategory { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string IDCard { get; set; }
        public string Phone { get; set; }

        public TransactionInfo TransactionInfo { get; set; }

        public decimal TotalValue { get; set; }

        public string PaymentTenderCatStr
        {
            get { return PaymentTenderCategory.ToString(); }
        }
        public string CardTypeStr
        {
            get
            {
                switch (CardType)
                {
                    case PaymentCardType.MASTERCARD:
                        return CardType.ToString();
                    case PaymentCardType.VISA:
                        return CardType.ToString();
                    case PaymentCardType.AMERICANEXPRESS:
                        return "AMERICAN EXPRESS";
                    case PaymentCardType.GIFTCARD:
                        return CardType.ToString();
                    case PaymentCardType.PAGOVIPPO:
                        return CardType.ToString();
                    case PaymentCardType.PAGOVZLATDD:
                        return CardType.ToString();
                    case PaymentCardType.PAGOVZLATDC:
                        return CardType.ToString();
                    case PaymentCardType.PAGOMERCTDD:
                        return "PAGOMERCTDD";
                    default:
                        return "";
                }
            }
        }
        public string VippoSessionToken { get; set; }
        public string VippoToken { get; set; }
        public string VippoReference { get; set; }
        public string BDVReference { get; set; }
        public string BDVToken { get; set; }
        public string BDVAccountType { get; set; }

        //Este es un codigo que envia mercantil al usuario y el lo coloca desde el front 
        //este debe ser enviado por el usuario desde el front asi como lo hace vippo como el token 
        public string mrkcodeauth { get; set; }

        public string AccountType { get; set; }
        public string KeyAuth { get; set; }
        public string SessionToken { get; set; }
        public string OTPCode { get; set; }
        public string Reference { get; set; }
        public string Taccount { get; set; }
        public string Tcode { get; set; }
        #endregion
    }
    public class TransactionInfo
    {
        #region Fields
        private DateTime m_ShowTime;
        #endregion
        #region Properties

        public string HallName { get; set; }
        public string MovieName { get; set; }
        public string Seats { get; set; }
        public DateTime ShowTime { get; set; }
        public int Count { get; set; }
        public decimal Amount { get; set; }
        public decimal Charge { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }

        public string Date
        {
            get { return m_ShowTime.ToShortDateString(); }
        }
        public string Time
        {
            get { return m_ShowTime.ToShortTimeString(); }
        }


        #endregion
    }
}
