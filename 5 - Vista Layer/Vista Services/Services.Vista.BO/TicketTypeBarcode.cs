﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
   public class TicketTypeBarcode
    {
        public string TType_strCode { get; set; }
        public string Price_strTicket_Type_Description { get; set; }
        public string Price_strTicket_Type_Description_2 { get; set; }
        public int AreaCat_intSeq { get; set; }
        public string TType_strAvailLoyaltyOnly { get; set; }
        public string AvailableOnSalesChannel { get; set; }
        public decimal Price_decimalTicket_Price { get; set; }
        public decimal Price_decimalSurcharge { get; set; }
        public string AreaCat_strSeatAllocationOn { get; set; }
        
    }
}
