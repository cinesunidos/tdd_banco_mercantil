﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class Transaction : TransactionAuthInfo
    {
        public string invoice_number { get; set; }
        public string account_type { get; set; }
        public string twofactor_auth { get; set; }
        public string expiration_date { get; set; }
        public string cvv { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
    }
}
