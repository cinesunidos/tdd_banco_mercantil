﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class SessionInformation
    {
        public SessionInformation()
        {
            Statistics = new List<Statistic>();
        }


        public int SeatsAvailable { get; set; }
        public Boolean SeatAllocation { get; set; }
        public string MovieName { get; set; }
        public string MovieID { get; set; }
        public string TheaterName { get; set; }
        public string TheaterID { get; set; }
        public DateTime ShowTime { get; set; }
        public int MaxSeatsPerTransaction { get; set; }
        public List<Ticket> Tickets { get; set; }
        public string HallName { get; set; }
        public List<Statistic> Statistics { get; set; }
        public Boolean OnSale { get; set; }

    }
}
