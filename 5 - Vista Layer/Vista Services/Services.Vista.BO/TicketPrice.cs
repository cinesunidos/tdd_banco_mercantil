﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class TicketPrice
    {
        public string city { set; get; }
        public string theaterID { set; get; }
        public string theaterName { set; get; }
        public string ticketName { set; get; }
        public string formatMovie { set; get; }
        public string typeHall { set; get; }
        public decimal price { set; get; }
        public decimal fee { set; get; }
        public DateTime date { set; get; }
        
    }
    public class CityTicketPrice
    {
        public string city { set; get; }
        public List<TheaterTicketPrice> theaterTicketPrice { set; get; }
        public CityTicketPrice()
        {
            theaterTicketPrice = new List<TheaterTicketPrice>();
        }
     
    }
    public class TheaterTicketPrice
    {
        public string theaterID { set; get; }
        public List<TicketPrice> listTicketPrice { set; get; }
        public TheaterTicketPrice()
        {
            listTicketPrice = new List<TicketPrice>();
        }
     
    }
    
}
