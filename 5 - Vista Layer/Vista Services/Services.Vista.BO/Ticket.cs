﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Ticket
    {
        
        public string Code { get; set; }

        /// <summary>
        /// RM - indica si el boleto es solo para redenciones
        /// </summary>
        public bool RedemptionOnly { get; set; }

        public string DescriptionTicketCategory { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Ticket Total Price (Precio del Ticket)
        /// Incluye IVA cuando aplica
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Ticket Base Price (Precio base del Ticket)
        /// No Incluye el  IVA
        /// </summary>
        public decimal BasePrice { get; set; }

        /// <summary>
        /// Monto Reserva (sin IVA)
        /// </summary>
        public decimal BookingFee { get; set; }

        // IM: Faltaría TotalBookingFee (con IVA)

        /// <summary>
        /// Impuesto Total (incluye booking y service)
        /// </summary>
        public decimal Tax { get; set; }

        /// <summary>
        /// Cargos por Servicios (sin IVA)
        /// </summary>
        public decimal ServiceFee { get; set; }

        /// <summary>
        /// Cargos por Servicios con IVA incluido
        /// </summary>
        public decimal TotalServiceFee { get; set; }
        public decimal PromotionFee { get; set; }

        /// <summary>
        /// Cargos por Promocion con IVA incluido
        /// </summary>
        public decimal TotalPromotionFee { get; set; }

        /// <summary>
        /// Monto total incluidos impuestos
        /// </summary>
        public decimal FullPrice { get; set; }


        /// <summary>
        /// IVA sobre el precio del boleto (si el boleto no supera las 2 UT debe ser cero)
        /// </summary>
        public decimal TicketTax { get; set; }

        /// <summary>
        /// IVA sobre el cargo por reservación (Cargo Web)
        /// </summary>
        public decimal BookingTax { get; set; }

        /// <summary>
        /// IVA sobre el cargo por servicios.
        /// </summary>
        public decimal ServiceTax { get; set; }
        public decimal PromotionTax { get; set; }
        public int Quantity { get; set; }

    }
}
