﻿namespace Services.Vista.BO
{
    public class ClientIdentify
    {
        public string ipaddress { get; set; }
        public string browser_agent { get; set; }
        public Mobile mobile { get; set; }
    }
}