﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class TransactionResponse
    {
        public string processing_date { get; set; }
        public string trx_status { get; set; }
        public string trx_internal_status { get; set; }
        public string trx_type { get; set; }
        public string payment_method { get; set; }
        public string payment_reference { get; set; }
        public string invoice_number { get; set; }
        public double amount { get; set; }
        public string currency { get; set; }
    }
}
