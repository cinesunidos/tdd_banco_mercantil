﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class PayResponse
    {
        public MerchantIdentify merchant_identify { get; set; }
        public TransactionResponse transaction_response { get; set; }
    }
}
