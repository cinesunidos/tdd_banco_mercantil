﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public enum SeatType : int
    {
        Chair = 0,
        Aisle = 1
    }
    public enum SeatStatus : int
    {
        Booked = 0,
        House = 1,
        Selected = 2,
        Available = 3
    }
    public class Seat
    {

        #region Properties

        public string AreaCategoryCode { get; set; }
        public int AreaNumber { get; set; }

        public int ColumnIndex { get; set; }
        public int RowIndex { get; set; }
        public SeatType Type { get; set; }

        public string Name { get; set; }

        //public SeatStatus Status
        //{
        //    get { return m_Status; }
        //    set { m_Status = value; }
        //}
        #endregion
        #region Methods

        public static char GetASCII(int num)
        {
            return (char)(num + 65);
        }
        public string GetSeatName(int num)
        {
            return GetASCII(num).ToString() + (ColumnIndex + 1).ToString();
        }
        //public static int GetNumFromASCII(char l)
        //{
        //    return l - 65;
        //}
        //public static int GetColumn(string SeatName)
        //{
        //    char l = SeatName[0];
        //    return GetNumFromASCII(l) + 1;                     
        //}
        //public static int GetRow(string SeatName)
        //{
        //    int num = Int32.Parse(SeatName.Substring(1, SeatName.Length - 1));
        //    return num;   
        //}
        
        #endregion
    }
}
