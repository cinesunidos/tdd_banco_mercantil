﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class PromoPack
    {
        public string TransactionId { get; set; }
        public string CinemaId { get; set; }
        public string Cinema { get; set; }
        public string Token { get; set; }
        public bool Status { get; set; }
        public string Mensaje { get; set; }
        public string Workstation { get; set; }
        public DateTime DateRedemption { get; set; }
    }
}
