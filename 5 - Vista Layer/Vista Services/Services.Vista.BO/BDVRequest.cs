﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class BDVRequest
    {
        public string letter { get; set; }
        public string number { get; set; }
        /// <summary>
        /// Tipo de moneda: 1-Bolivar, 2-Dolar.
        /// </summary>
        public string currency = "1";
        public string amount { get; set; }
        public string reference = Guid.NewGuid().ToString();
        public string title = "Compra en Cines Unidos";
        public string description { get; set; }
        public string email { get; set; }
        public string cellphone { get; set; }
        public string urlToReturn { get; set; }

    }
}
