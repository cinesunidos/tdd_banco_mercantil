﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class ConcessionBookingFee
    {        
        public decimal BookingTaxBookingFeeConcessions { get; set; }
        public decimal BookingFeeConcessions { get; set; }
    }
}
