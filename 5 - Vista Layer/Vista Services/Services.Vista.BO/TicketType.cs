﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class TicketType
    {
        /// <summary>
        /// RM - indica si el boleto es solo para redenciones
        /// </summary>
        public bool RedemptionOnly { get; set; }
        public decimal Price { get; set; }
        public int PriceInCents { get; set; }
        public int Quantity { get; set; }
        public string TicketTypeCode { get; set; }
        public string TicketBarcode { get; set; }

    }
}
