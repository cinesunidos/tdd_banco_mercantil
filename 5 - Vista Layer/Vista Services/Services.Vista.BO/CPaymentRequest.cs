﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Vista.BO
{
    public class CPaymentRequest
    {
        public string APIKEY { get; set; }
        public string Merchant_Vippo { get; set; }
        public string Merchant_Reference { get; set; }
        public string Amount { get; set; }
        public string SessionToken { get; set; }
        public string OTPClient { get; set; }

    }
}
