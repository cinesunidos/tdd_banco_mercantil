﻿using Newtonsoft.Json;
using Services.Vista.BO;
using Services.Vista.DAL.DO;
using Services.Vista.DAL.DORest;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace Services.Vista.BL
{

    public class Payment : Contract.IPayment
    {
        public static int contFront = 0;
        public static AuthResponse GlobalAuthResponse;

        /// <summary>
        /// Metodo unificado para realizar pagos a traves de ditintas plataformas o tipos de tarjeta.
        /// se individualiza el pago por TDC, por Vippo, y las que vendran.
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="UserSessionID"></param>
        /// <param name="PaymentInfo"></param>
        /// <returns></returns>
        public PaymentResponse GetPaymentResponseByPaymentType(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        {
            PaymentResponse response = new PaymentResponse();
            try
            {
                switch (PaymentInfo.CardType)
                {
                    case PaymentCardType.GIFTCARD:
                        response = GetPagoGiftCard();
                        break;
                    case PaymentCardType.PAGOVIPPO:
                        response = GetPagoVippo(ClientID, UserSessionID, PaymentInfo);
                        break;
                    case PaymentCardType.PAGOMERCTDD:
                        response = GetPaymentMercantil(ClientID, UserSessionID, PaymentInfo);
                        break;
                    case PaymentCardType.PAGOVZLATDC:
                        response = GetPaymentVenezuela(ClientID, UserSessionID, PaymentInfo);
                        break;
                    case PaymentCardType.PAGOVZLATDD:
                        response = GetPaymentVenezuela(ClientID, UserSessionID, PaymentInfo);
                        break;
                    default:
                        response = GetPagoMerchant(ClientID, UserSessionID, PaymentInfo); //VISA, MASTERCARD, AMERICANEXPRESS
                        break;
                }

                return response;
            }
            catch (Exception e)
            {
                response.CodeResult = "E0005";
                return response;
            }
        }



        private PaymentResponse GetPaymentVenezuela(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        {
            Payment_Services.Payment payment = new Payment_Services.Payment();
            PaymentResponse paymentResponse = new PaymentResponse();

            try
            {
                WebClient webClient = new WebClient() { Encoding = Encoding.UTF8 };
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                webClient.Headers.Add(HttpRequestHeader.Authorization, "Basic NzI4NzYzOTM6enV6NVBkOFk=");
                string BDVUrl = string.Format("https://biodemo.ex-cle.com:4443/ipg/web/api/ProcessPayment?id={0}&paymentMethod={1}&smsCode={2}", 
                    PaymentInfo.BDVReference,
                    PaymentInfo.BDVAccountType,
                    PaymentInfo.BDVToken);
                string response = webClient.UploadString(BDVUrl, "{}");
                BDVResponseConfirmation responseConfirmation = JsonConvert.DeserializeObject<BDVResponseConfirmation>(response);
                if (responseConfirmation.responseCode == 0)
                {
                    payment.CodeResult = "00";
                    payment.Voucher = $@"<linea>Pago Realizado</linea><linea>Con</linea><linea>Cuenta</linea><linea>{responseConfirmation.detail.paymentMethodDescription}</linea><linea>Descipcion</linea><linea>{responseConfirmation.detail.description}</linea>";
                }
                else
                {
                    paymentResponse.CodeResult = responseConfirmation.responseCode.ToString();
                    paymentResponse.DescriptionResult = responseConfirmation.responseDescription;
                   
                }
            }
            catch (Exception)
            {
                paymentResponse.CodeResult = "E0002";
                paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
               // return paymentResponse;
            }

            return paymentResponse;
        }











        /// <summary>
        /// Metodo que Realiza el Pago en las Distintas Formas de Pagos
        /// -Merchant(Credit Card - Tarjetas de Creditos)
        /// -Vippo (Wallet - Billetera Virtual)
        /// -Mpandco (Wallet - Billetera Virtual)
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="UserSessionID"></param>
        /// <param name="PaymentInfo"></param>
        /// <returns></returns>
        #region MyRegion
        //public PaymentResponse GetPaymentPremiumUltimate(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        //{
        //    PaymentResponse paymentResponse = new PaymentResponse();
        //    GetOrderResponse OrderResponse = null;
        //    CompleteOrderResponse response = null;
        //    DAL.DORest.PaymentInfo payInfo;
        //    //int BookingNumber = 0;
        //    //int TransactionNumber;
        //    string BookingId = string.Empty;
        //    try
        //    {
        //        if (!PaymentInfo.CardTypeStr.Equals(PaymentCardType.PAGOVIPPO.ToString()))
        //        {
        //            if (!ValidatePaymentInfo(PaymentInfo)) { throw new ControlledException("E0007"); }
        //        }

        //        OrderResponse = GetOrder(UserSessionID);

        //        if (!OrderResponse.Result.Equals(ResultCode.OK)) { throw new ControlledException("E0005"); }

        //        if (OrderResponse.Order.Sessions[0].Tickets.Equals(null) || OrderResponse.Order.Sessions[0].Tickets.Length <= 0) { throw new ControlledException("E0006"); }

        //        Payment_Services.Payment payment = new Payment_Services.Payment();

        //        //Seccion Para Merchant
        //        string CompanyID = VISTAITCinema.getInfoTheater(OrderResponse.Order.CinemaId).Company;
        //        Payment_Services.PaymentRequest PayRequest = new Payment_Services.PaymentRequest();

        //        //Seccion Para Vippo
        //        AVippoResponse VippoResponse = new AVippoResponse();

        //        switch (PaymentInfo.CardType)
        //        {
        //            case PaymentCardType.PAGOVIPPO:
        //                try
        //                {
        //                    WebClient Client = new WebClient() { Encoding = System.Text.Encoding.UTF8 };
        //                    Client.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
        //                    CPaymentRequest PaymentRequest = new CPaymentRequest()
        //                    {
        //                        APIKEY = ConfigurationManager.AppSettings["APIKEY"],
        //                        Merchant_Vippo = ConfigurationManager.AppSettings["Merchant_Vippo"],
        //                        Merchant_Reference = PaymentInfo.VippoReference,
        //                        Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100),
        //                        SessionToken = PaymentInfo.VippoSessionToken,
        //                        OTPClient = PaymentInfo.VippoToken
        //                    };
        //                    string JsonVippoPayment = JsonConvert.SerializeObject(PaymentRequest);
        //                    string Response = Client.UploadString(ConfigurationManager.AppSettings["UrlClientPaymentConfirm"], "POST", JsonVippoPayment);
        //                    VippoResponse = JsonConvert.DeserializeObject<AVippoResponse>(Response);
        //                    if (VippoResponse.Success.Equals(true))
        //                    {
        //                        payment.CodeResult = "00";
        //                        payment.Voucher = "<linea>Pago Realizado</linea><linea>Con</linea><linea>Cuenta</linea><linea>ViPPO</linea>";
        //                    }
        //                    else
        //                    {
        //                        paymentResponse.CodeResult = "V" + VippoResponse.RtnCde.ToString();
        //                        paymentResponse.DescriptionResult = VippoResponse.Label;
        //                        return paymentResponse;
        //                    }
        //                }
        //                catch (Exception)
        //                {
        //                    paymentResponse.CodeResult = "V0001";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    return paymentResponse;
        //                }
        //                break;
        //            default:
        //                //Flujo para el Cobro con Tarjetas de Creditos pasando por Merchant
        //                try
        //                {
        //                    PayRequest.Amount = (decimal)OrderResponse.Order.TotalValueCents / 100;
        //                    //paymentResquest.BookingNumber = BookingNumber;
        //                    PayRequest.Cinema = OrderResponse.Order.CinemaId;
        //                    PayRequest.ClientId = ClientID; //TODO: llenar este campo            
        //                    PayRequest.CVV = PaymentInfo.CardCVV;
        //                    PayRequest.ExpMonth = PaymentInfo.CardExpiryMonth;
        //                    PayRequest.ExpYear = PaymentInfo.CardExpiryYear;
        //                    PayRequest.Factura = OrderResponse.Order.VistaTransactionNumber;
        //                    PayRequest.IDCard = PaymentInfo.IDCard;
        //                    PayRequest.Name = PaymentInfo.Name;
        //                    PayRequest.CardNumber = PaymentInfo.CardNumber;
        //                    PayRequest.VistaOrderId = UserSessionID; //TODO: llenar este campo     
        //                    PayRequest.CompanyId = CompanyID;
        //                    bool value = Convert.ToBoolean(ConfigurationManager.AppSettings["DisableMerchant"]);
        //                    if (value == false) { payment.ApproveFromParameters(PayRequest); }
        //                    else
        //                    {
        //                        payment.CodeResult = "00";
        //                        payment.Voucher = "<linea>Prueba</linea><linea>sin</linea><linea>pasar</linea>por</linea><linea>merchant</linea>";
        //                    }
        //                }
        //                catch (Exception)
        //                {
        //                    paymentResponse.CodeResult = "E0002";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    return paymentResponse;
        //                }
        //                break;
        //        }
        //        paymentResponse.SetVoucher(payment.Voucher);
        //        paymentResponse.CodeResult = payment.CodeResult;
        //        paymentResponse.DescriptionResult = payment.DescriptionResult;
        //        paymentResponse.SequenceNumber = payment.SequenceNumber; //Codigo en Caso de Falla en Merchant para Reembolso de la Transaccion

        //        //Se verifica que el Pago Procesado no es Fallido en Merchant
        //        if (!payment.CodeResult.Equals("00"))
        //        {
        //            paymentResponse.CodeResult = "E0002";
        //            paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //            return paymentResponse;
        //        }

        //        //Se verifica que el Pago Procesado no es Fallido en Vippo
        //        if (!VippoResponse.Success.Equals(true) && PaymentInfo.CardTypeStr.Equals(PaymentCardType.PAGOVIPPO))
        //        {
        //            paymentResponse.CodeResult = "V0004";
        //            paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //            return paymentResponse;
        //        }

        //        //Como el código es 00 la transaccion respondio de manera correcta entonces debemos ver el XML y ver si es aprobada.   

        //        //Se hace la inserción en vista de la operacion ya que fue aprobada.

        //        payInfo = new DAL.DORest.PaymentInfo();

        //        switch (PaymentInfo.CardType)
        //        {
        //            case PaymentCardType.GIFTCARD:
        //                payInfo.CardType = PaymentInfo.CardTypeStr;
        //                payInfo.CardNumber = PaymentInfo.CardNumber;
        //                payInfo.CardType = "SVS";
        //                payInfo.CardCVC = "111";
        //                payInfo.CardExpiryMonth = "12";
        //                payInfo.CardExpiryYear = "99";
        //                payInfo.PaymentTenderCategory = PaymentTenderCategory.SVC.ToString();
        //                break;
        //            case PaymentCardType.PAGOVIPPO:
        //                PaymentInfo.Name = PaymentInfo.IDCard;
        //                payInfo.CardType = PaymentInfo.CardTypeStr;
        //                payInfo.CardNumber = "6666777788889999";
        //                payInfo.CardCVC = "111";
        //                payInfo.CardExpiryMonth = "12";
        //                payInfo.CardExpiryYear = "99";
        //                payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
        //                break;
        //            default:
        //                payInfo.CardType = PaymentInfo.CardTypeStr;
        //                payInfo.CardNumber = PaymentInfo.CardNumber;
        //                payInfo.CardCVC = PaymentInfo.CardCVV;
        //                payInfo.CardExpiryMonth = PaymentInfo.CardExpiryMonth.ToString();
        //                payInfo.CardExpiryYear = PaymentInfo.CardExpiryYear.ToString();
        //                payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
        //                break;
        //        }

        //        payInfo.PaymentValueCents = OrderResponse.Order.TotalValueCents;

        //        //Realiza la orden en vista.
        //        //response = CompleteOrderResponse.CompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);
        //        response = RestTicketingService.RestCompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);

        //        //Se Marcar la Orden como Pagada en las tablas de Seguimiento de Concesiones de CinesUnidos

        //        //Luis Ramírez 31/10/2017, en la tabla tblCUOrdenInvConcessions, campo PaymentOK se marca en Y indicando que la compra fue procesada
        //        if ((OrderResponse.Order.Concessions != null) && (OrderResponse.Order.Concessions.Count() > 0))
        //        {
        //            Concessions.ProcessOrderConcessions(UserSessionID, OrderResponse.Order.CinemaId);
        //        }

        //        if (response == null || !response.Result.Equals(ResultCode.OK))
        //        {
        //            switch (PaymentInfo.CardType)
        //            {
        //                case PaymentCardType.PAGOVIPPO:
        //                    AReversalRequest Reversal = new AReversalRequest()
        //                    {
        //                        Merchant_Reference = PaymentInfo.VippoReference,
        //                        Client_UserVippo = string.Empty,
        //                        Client_PasswordVippo = string.Empty,
        //                        Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100)
        //                    };
        //                    AVippoResponse ReversalResponse = GetVippoReversal(Reversal, PaymentInfo.VippoSessionToken);
        //                    SaveReversal(PaymentInfo, Reversal.Amount, OrderResponse.Order.CinemaId);
        //                    paymentResponse.SetVoucher("");
        //                    paymentResponse.CodeResult = "V" + ReversalResponse.RtnCde.ToString();
        //                    paymentResponse.DescriptionResult = ReversalResponse.Label;
        //                    return paymentResponse;
        //                default:
        //                    // se hace la anulacion de la operacion en merchant empleando 
        //                    payment.AnularPagoEnMerchant(PayRequest, payment);
        //                    paymentResponse.CodeResult = "E0006";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    return paymentResponse;
        //            }
        //        }
        //        paymentResponse.BookingId = response.VistaBookingId;
        //        paymentResponse.BookingNumber = Int32.Parse(response.VistaBookingNumber);
        //        paymentResponse.TransactionNumber = Int32.Parse(response.VistaTransNumber);
        //        if (PaymentInfo.CardTypeStr.Equals(PaymentCardType.PAGOVIPPO.ToString()))
        //        {
        //            SavePurchase(PaymentInfo, response, OrderResponse);
        //        }
        //        return paymentResponse;

        //    }
        //    catch (Exception e)
        //    {
        //        if (PaymentInfo.CardTypeStr.Equals("PAGOVIPPO"))
        //        {
        //            paymentResponse.CodeResult = "V0003";
        //            paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //            return paymentResponse;
        //        }
        //        else
        //        {
        //            paymentResponse.CodeResult = "E0001";
        //            paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //            return paymentResponse;
        //        }
        //    }
        //}

        #endregion
        public Boolean RestRefundBooking(string TheaterID, int BookingNumber, int ValueInCents)
        {
            return RestTicketingService.RestGetRefundBooking(TheaterID, BookingNumber, ValueInCents);
        }

        #region Metodos Privados
        /// <summary>
        /// Metoo para realizar Pagos a traves de la plataforma Vippo
        /// </summary>
        /// <returns></returns>
        private PaymentResponse GetPagoVippo(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        {
            string BasePath = @"C:\Desarrollo\Logs\Payment\Vippo";
            if (!Directory.Exists(BasePath))
                Directory.CreateDirectory(BasePath);
            string FileName = "Payment_" + PaymentInfo.IDCard.ToString() + "_" + Guid.NewGuid().ToString() + ".txt";
            string LogPath = Path.Combine(BasePath, FileName);
            StreamWriter sw = new StreamWriter(LogPath);

            PaymentResponse paymentResponse = new PaymentResponse();
            GetOrderResponse OrderResponse = GetOrder(UserSessionID);
            //si la orden no viene OK, retorno una excepcion
            if (OrderResponse.Result != ResultCode.OK)
            {
                sw.WriteLine("L318 | No se encontro la orden con los datos: {0}", UserSessionID);
                sw.Dispose();
                sw.Close();
                throw new ControlledException("E0005");
            }
            //si la orden no trae los asientos, retorno una excepcion,
            if (OrderResponse.Order.Sessions[0].Tickets == null || OrderResponse.Order.Sessions[0].Tickets.Length <= 0)
            {
                sw.WriteLine("L326 | la orden no trajo los asientos: {0}", JsonConvert.SerializeObject(OrderResponse));
                sw.Dispose();
                sw.Close();
                throw new ControlledException("E0006");
            }

            try
            {
                AVippoResponse VippoResponse = new AVippoResponse();
                WebClient Client = new WebClient() { Encoding = System.Text.Encoding.UTF8 };
                Client.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
                CPaymentRequest PaymentRequest = new CPaymentRequest()
                {
                    Merchant_Vippo = ConfigurationManager.AppSettings["Merchant_Vippo"],
                    Merchant_Reference = PaymentInfo.VippoReference,
                    Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100),
                    SessionToken = PaymentInfo.VippoSessionToken,
                    OTPClient = PaymentInfo.VippoToken
                };
                string JsonVippoPayment = JsonConvert.SerializeObject(PaymentRequest);
                sw.WriteLine(string.Format("L340 | se realiza Pago en Vippo"));
                sw.WriteLine(string.Format("L341 | objeto que se envia a vippo {0}", JsonVippoPayment));
                PaymentRequest.APIKEY = ConfigurationManager.AppSettings["APIKEY"];
                JsonVippoPayment = JsonConvert.SerializeObject(PaymentRequest);
                string Response = Client.UploadString(ConfigurationManager.AppSettings["UrlClientPaymentConfirm"], "POST", JsonVippoPayment);
                sw.WriteLine(string.Format("L345 | respuesta de vippo: {0} ", Response));
                VippoResponse = JsonConvert.DeserializeObject<AVippoResponse>(Response);
                Payment_Services.Payment payment = new Payment_Services.Payment();
                //si el pago no fue positivo en vippo, retorno el meensaje con la respuesta de servicio.
                if (!VippoResponse.Success)
                {
                    paymentResponse.CodeResult = "V" + VippoResponse.RtnCde.ToString();
                    paymentResponse.DescriptionResult = VippoResponse.Label;
                    sw.WriteLine(string.Format("L353 |Pago con Vippo resulto fallido respuesta al cliente {0} | {1} ", paymentResponse.CodeResult, paymentResponse.DescriptionResult));
                    sw.Dispose();
                    sw.Close();
                    return paymentResponse;
                }

                //de lo contrario, continuo con el porceo de resgistrar la compra en vista.
                payment.CodeResult = "00";
                payment.Voucher = "<linea>Pago Realizado</linea><linea>Con</linea><linea>Cuenta</linea><linea>ViPPO</linea>";
                PaymentInfo.Name = PaymentInfo.IDCard;
                DAL.DORest.PaymentInfo payInfo = new DAL.DORest.PaymentInfo();
                payInfo.CardType = PaymentInfo.CardTypeStr;
                payInfo.CardNumber = "6666777788889999";
                payInfo.CardCVC = "111";
                payInfo.CardExpiryMonth = "12";
                payInfo.CardExpiryYear = "99";
                payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
                payInfo.PaymentValueCents = OrderResponse.Order.TotalValueCents;

                //datos para log
                DAL.DO.PaymentInfo info = new DAL.DO.PaymentInfo();
                info.CardType = payInfo.CardType;
                info.CardNumber = string.Format("{0}******{1}", payInfo.CardNumber.Substring(0, 6), payInfo.CardNumber.Substring(12, 4));
                info.CardCVC = "";
                info.CardExpiryMonth = payInfo.CardExpiryMonth;
                info.CardExpiryYear = payInfo.CardExpiryYear;
                info.PaymentTenderCategory = payInfo.PaymentTenderCategory;
                info.PaymentValueCents = payInfo.PaymentValueCents;
                string jsonpayinfo = JsonConvert.SerializeObject(info);
                sw.WriteLine(string.Format("L370 |se inicia guardado en vista CompleteOrder: {0} | {1} | {2} | {3}", ClientID, UserSessionID, jsonpayinfo, PaymentInfo.Name));
                //fin datos para Log
                
                
                CompleteOrderResponse VistaResponse = RestTicketingService.RestCompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);
               
                
                string jsonVistaResponse = VistaResponse != null ? JsonConvert.SerializeObject(VistaResponse) : "La orden en vista vino nula";
                sw.WriteLine(string.Format("L373 | respuesta del CompleteOrder: {0} ", jsonVistaResponse));
                //si no se puedo guardar la orden en vista, se devuelve el dinero en VIppo
                if (VistaResponse == null || (VistaResponse.Result != ResultCode.OK))
                {
                    sw.WriteLine("L389 | La orden de vista vino nula o distinta de ok. se procedera a revisar si se gurado la orden en el cine..");
                    bool ResponseTransaction = false;
                    sw.WriteLine("L391 | Datos que se enviaran al servicio TransactionServiceFind: {0} | {1}", VistaResponse.CinemaID, VistaResponse.VistaTransNumber);
                    ResponseTransaction = TransactionService.TransactionServiceFind(VistaResponse.CinemaID, VistaResponse.VistaTransNumber);
                    sw.WriteLine("L393 | respuesta del servicio TransactionServiceFind: {0}", ResponseTransaction);
                    if (ResponseTransaction == false)
                    {
                        sw.WriteLine("L396 | La orden no se guardo en en cine.");
                        sw.WriteLine("L377 | Se inicia el reverso en Vippo");
                        AReversalRequest Reversal = new AReversalRequest()
                        {
                            Merchant_Reference = PaymentInfo.VippoReference,
                            Client_UserVippo = string.Empty,
                            Client_PasswordVippo = string.Empty,
                            Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100)
                        };
                        sw.WriteLine("L385 | Objeto para el reverso en vippo: {0}", JsonConvert.SerializeObject(Reversal));
                        AVippoResponse ReversalResponse = GetVippoReversal(Reversal, PaymentInfo.VippoSessionToken);
                        sw.WriteLine("L387 | Respuesta del reverso en vippo: {0}", JsonConvert.SerializeObject(ReversalResponse));
                        SaveReversal(PaymentInfo, Reversal.Amount, OrderResponse.Order.CinemaId);
                        sw.WriteLine("L391 | Se guardo el reverso en azure...");
                        sw.Dispose();
                        sw.Close();
                        paymentResponse.SetVoucher("");
                        paymentResponse.CodeResult = "V" + ReversalResponse.RtnCde.ToString();
                        paymentResponse.DescriptionResult = ReversalResponse.Label;
                        return paymentResponse;
                    }
                    else
                    {
                        sw.WriteLine("L419 | La oden si se guardo en el cine..");
                    }
                }

                //Se Marcar la Orden como Pagada en las tablas de Seguimiento de Concesiones de CinesUnidos
                //Luis Ramírez 31/10/2017, en la tabla tblCUOrdenInvConcessions, campo PaymentOK se marca en Y indicando que la compra fue procesada
                //RM - No se si se use aun.
                if ((OrderResponse.Order.Concessions != null) && (OrderResponse.Order.Concessions.Count() > 0))
                {
                    Concessions.ProcessOrderConcessions(UserSessionID, OrderResponse.Order.CinemaId);
                }
                paymentResponse.SetVoucher(payment.Voucher);
                paymentResponse.CodeResult = payment.CodeResult;
                paymentResponse.DescriptionResult = payment.DescriptionResult;
                paymentResponse.SequenceNumber = payment.SequenceNumber; //Codigo en Caso de Falla en Merchant para Reembolso de la Transaccion
                paymentResponse.BookingId = VistaResponse.VistaBookingId;
                paymentResponse.BookingNumber = Int32.Parse(VistaResponse.VistaBookingNumber);
                paymentResponse.TransactionNumber = Int32.Parse(VistaResponse.VistaTransNumber);
                SavePurchase(PaymentInfo, VistaResponse, OrderResponse);
                sw.WriteLine("L411 | Se guardo la compra en azure..");
                sw.Dispose();
                sw.Close();
            }
            catch (Exception Ex)
            {
                string inner = (Ex.InnerException != null ? Ex.Message + ": " + Ex.InnerException.ToString() : Ex.Message);
                paymentResponse.CodeResult = "V0001";
                paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
                sw.WriteLine(string.Format("C112 |Fallo en el flujo de pago con vippo: {0}", inner));
                sw.Dispose();
                sw.Close();
                return paymentResponse;
            }
            return paymentResponse;
        }

        /// <summary>
        /// Metodo para realizar pagos tradicionales a traves del Merchant
        /// </summary>
        /// <returns></returns>
        private PaymentResponse GetPagoMerchant(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        {
            string BasePath = @"C:\Desarrollo\Logs\Payment\Merchant";
            if (!Directory.Exists(BasePath))
                Directory.CreateDirectory(BasePath);
            string FileName = "Payment_" + PaymentInfo.IDCard.ToString() + "_" + Guid.NewGuid().ToString() + ".txt";
            string LogPath = Path.Combine(BasePath, FileName);
            StreamWriter w = new StreamWriter(LogPath);
            PaymentResponse paymentResponse = new PaymentResponse();
            try
            {
                if (!ValidatePaymentInfo(PaymentInfo))
                {
                    w.WriteLine("L460 | Informacion de pago no valida: {0}", JsonConvert.SerializeObject(PaymentInfo));
                    w.Dispose();
                    w.Close();
                    throw new ControlledException("E0007");
                }
                GetOrderResponse OrderResponse = GetOrder(UserSessionID);
                if (!OrderResponse.Result.Equals(ResultCode.OK))
                {
                    w.WriteLine("L469 | No se encontro la orden con los datos: {0}", UserSessionID);
                    w.Dispose();
                    w.Close();
                    throw new ControlledException("E0005");
                }

                if (OrderResponse.Order.Sessions[0].Tickets.Equals(null) || OrderResponse.Order.Sessions[0].Tickets.Length <= 0)
                {
                    w.WriteLine("L480 | la orden no trajo los asientos: {0}", JsonConvert.SerializeObject(OrderResponse));
                    w.Dispose();
                    w.Close();
                    throw new ControlledException("E0006");
                }

                Payment_Services.Payment payment = new Payment_Services.Payment();
                string CompanyID = VISTAITCinema.getInfoTheater(OrderResponse.Order.CinemaId).Company;
                Payment_Services.PaymentRequest PayRequest = new Payment_Services.PaymentRequest();
                try
                {
                    PayRequest.Amount = (decimal)OrderResponse.Order.TotalValueCents / 100;
                    PayRequest.Cinema = OrderResponse.Order.CinemaId;
                    PayRequest.ClientId = ClientID;
                    PayRequest.ExpMonth = PaymentInfo.CardExpiryMonth;
                    PayRequest.ExpYear = PaymentInfo.CardExpiryYear;
                    PayRequest.Factura = OrderResponse.Order.VistaTransactionNumber;
                    PayRequest.IDCard = PaymentInfo.IDCard;
                    PayRequest.Name = PaymentInfo.Name;
                    PayRequest.VistaOrderId = UserSessionID;
                    PayRequest.CompanyId = CompanyID;
                    bool value = Convert.ToBoolean(ConfigurationManager.AppSettings["DisableMerchant"]);
                    //si el merchant está desabilitado en capa 3, no voy a merchant, de lo contrario, si voy.
                    if (value == false)
                    {
                        PayRequest.CardNumber = string.Format("{0}******{1}", PaymentInfo.CardNumber.Substring(0, 6), PaymentInfo.CardNumber.Substring(12, 4));
                        string jsonPayRequest = JsonConvert.SerializeObject(PayRequest);
                        w.WriteLine(string.Format("L507 | Datos enviados a merchant para el cobro: {0} ", jsonPayRequest));
                        //asigno los valores de la tarjeta luego de haber escrito el log, para evitar guardar datos de la TDC
                        PayRequest.CardNumber = PaymentInfo.CardNumber;
                        PayRequest.CVV = PaymentInfo.CardCVV;
                        payment.ApproveFromParameters(PayRequest);
                        w.WriteLine("L511 | Codigo Respuesta de merchant: {0}", payment.CodeResult);
                        payment.URLPay = ""; //PARA NO REVELAR DATOS ASOCIADOS AL PAGO
                        w.WriteLine("L512 | Objeto Respuesta de merchant: {0}", JsonConvert.SerializeObject(payment));
                    }
                    else
                    {
                        payment.CodeResult = "00";
                        payment.Voucher = "<linea>Prueba</linea><linea>sin</linea><linea>pasar</linea>por</linea><linea>merchant</linea>";
                    }

                    paymentResponse.SetVoucher(payment.Voucher);
                    paymentResponse.CodeResult = payment.CodeResult;
                    paymentResponse.DescriptionResult = payment.DescriptionResult;
                    paymentResponse.SequenceNumber = payment.SequenceNumber; //Codigo en Caso de Falla en Merchant para Reembolso de la Transaccion
                    //Se verifica que el Pago Procesado no es Fallido en Merchant
                    if (payment.CodeResult != "00")
                    {
                        w.WriteLine("L530 | Pago no exitoso en Merchant. CodeResult = {0}", payment.CodeResult);
                        w.Dispose();
                        w.Close();
                        //paymentResponse.CodeResult = "E0001";
                        //paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
                        string coderesult = payment.CodeResult.ToUpper().Contains("V") ? payment.CodeResult.ToUpper().Replace("V", "") : payment.CodeResult.ToUpper();
                        paymentResponse.CodeResult = coderesult;
                        paymentResponse.DescriptionResult = payment.DescriptionResult;
                        return paymentResponse;
                    }

                    DAL.DORest.PaymentInfo payInfo = new DAL.DORest.PaymentInfo();
                    payInfo.CardType = PaymentInfo.CardTypeStr;
                    payInfo.CardNumber = PaymentInfo.CardNumber;
                    payInfo.CardCVC = PaymentInfo.CardCVV;
                    payInfo.CardExpiryMonth = PaymentInfo.CardExpiryMonth.ToString();
                    payInfo.CardExpiryYear = PaymentInfo.CardExpiryYear.ToString();
                    payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
                    payInfo.PaymentValueCents = OrderResponse.Order.TotalValueCents;

                    DAL.DO.PaymentInfo info = new DAL.DO.PaymentInfo();
                    info.CardType = payInfo.CardType;
                    info.CardNumber = string.Format("{0}******{1}", payInfo.CardNumber.Substring(0, 6), payInfo.CardNumber.Substring(12, 4));
                    info.CardCVC = "";
                    info.CardExpiryMonth = payInfo.CardExpiryMonth;
                    info.CardExpiryYear = payInfo.CardExpiryYear;
                    info.PaymentTenderCategory = payInfo.PaymentTenderCategory;
                    info.PaymentValueCents = payInfo.PaymentValueCents;
                    string jsonpayinfo = JsonConvert.SerializeObject(info);
                    w.WriteLine("L556 |se inicia guardado en vista CompleteOrder: {0} | {1} | {2} | {3}", ClientID, UserSessionID, jsonpayinfo, PaymentInfo.Name);

                    CompleteOrderResponse VistaResponse = RestTicketingService.RestCompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);

                    string jsonVistaResponse = VistaResponse != null ? JsonConvert.SerializeObject(VistaResponse) : "La orden en vista vino nula";
                    w.WriteLine(string.Format("L562 | respuesta del CompleteOrder: {0} ", jsonVistaResponse));

                    if (VistaResponse == null || (VistaResponse.Result != ResultCode.OK)) //validar otro tipo de respuesta que tambien es satisfactoria
                    {
                        w.WriteLine("L570 | La orden de vista vino nula o distinta de ok. se procedera a revisar si se gurado la orden en el cine..");
                        bool ResponseTransaction = false;
                        w.WriteLine("L573 | Datos que se enviaran al servicio TransactionServiceFind: {0} | {1}", VistaResponse.CinemaID, VistaResponse.VistaTransNumber);
                        ResponseTransaction = TransactionService.TransactionServiceFind(VistaResponse.CinemaID, VistaResponse.VistaTransNumber);
                        w.WriteLine("L575| respuesta del servicio TransactionServiceFind: {0}", ResponseTransaction);
                        if (ResponseTransaction == false)
                        {
                            w.WriteLine("L577 | La orden no se guardo en en cine.");
                            PayRequest.CardNumber = string.Format("{0}******{1}", PayRequest.CardNumber.Substring(0, 6), PayRequest.CardNumber.Substring(12, 4));
                            PayRequest.CVV = "";
                            w.WriteLine("L564 | se hara reverso en merchant con los datos siguientes: payrequest = {0} payment = {1}", JsonConvert.SerializeObject(PayRequest), JsonConvert.SerializeObject(payment));
                            PayRequest.CardNumber = PaymentInfo.CardNumber;
                            PayRequest.CVV = PaymentInfo.CardCVV;
                            // se hace la anulacion de la operacion en merchant empleando 
                            var devuelto2 = payment.AnularPagoEnMerchant(PayRequest, payment);
                            w.WriteLine("L571 | respuesta de la devolucion em merchant : {0}", devuelto2);
                            w.Dispose();
                            w.Close();
                            paymentResponse.SetVoucher("");
                            paymentResponse.CodeResult = "E0006";
                            paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
                            return paymentResponse;
                        }
                        else
                        {
                            w.WriteLine("L594 | La oden si se guardo en el cine..");
                        }
                    }

                    //Se Marcar la Orden como Pagada en las tablas de Seguimiento de Concesiones de CinesUnidos
                    //Luis Ramírez 31/10/2017, en la tabla tblCUOrdenInvConcessions, campo PaymentOK se marca en Y indicando que la compra fue procesada
                    //RM - no se si se usa aun
                    if ((OrderResponse.Order.Concessions != null) && (OrderResponse.Order.Concessions.Count() > 0))
                    {
                        Concessions.ProcessOrderConcessions(UserSessionID, OrderResponse.Order.CinemaId);
                    }
                    paymentResponse.BookingId = VistaResponse.VistaBookingId;
                    paymentResponse.BookingNumber = Int32.Parse(VistaResponse.VistaBookingNumber);
                    paymentResponse.TransactionNumber = Int32.Parse(VistaResponse.VistaTransNumber);
                    w.Dispose();
                    w.Close();
                }
                catch (Exception e1)
                {
                    string inner = e1.InnerException != null ? e1.InnerException.ToString() : "";
                    w.WriteLine("L959 | hubo un problema en el flujo de pago con TDC, exepcion: {0} innerExcepcion: {}", e1.Message, inner);
                    w.Dispose();
                    w.Close();
                    paymentResponse.CodeResult = "E0002";
                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
                    return paymentResponse;
                }

            }
            catch (Exception e)
            {
                string inner = e.InnerException != null ? e.InnerException.ToString() : "";
                w.WriteLine("L601 | hubo un problema en el flujo de pago con TDC, exepcion: {0} innerExcepcion: {}", e.Message, inner);
                w.Dispose();
                w.Close();
                paymentResponse.CodeResult = "998";
                paymentResponse.DescriptionResult = e.Message;
            }
            return paymentResponse;
        }

        /// <summary>
        /// Metodo para hacer pagos con GiftCards de Vista
        /// </summary>
        /// <returns></returns>
        private PaymentResponse GetPagoGiftCard()
        {
            //metodo no implementado
            return null;
        }

        private GetOrderResponse GetOrder(string UserSessionID)
        {
            GetOrderResponse rp = RestTicketingService.GetOrder(UserSessionID);
            //if (rp.Result != DAL.DO.ResultCode.OK) { }//TODO: new exception
            return rp;
        }

        /// <summary>
        /// Verifica que el Modelo Contenga toda la Informacion de Pago Relacionado a Tarjetas de Creditos
        /// </summary>
        /// <param name="PaymentInfo"></param>
        /// <returns></returns>
        private bool ValidatePaymentInfo(BO.PaymentInfo PaymentInfo)
        {
            bool isOk = true;
            if (PaymentInfo != null)
            {
                if (PaymentInfo.CardNumber == null || PaymentInfo.CardNumber == string.Empty) isOk = false;
                //if (PaymentInfo.CardType == null) isOk = false;
                if (PaymentInfo.CardType != PaymentCardType.GIFTCARD)
                {
                    if (PaymentInfo.CardCVV == null || PaymentInfo.CardCVV == string.Empty) isOk = false;
                    if (PaymentInfo.CardExpiryMonth == 0) isOk = false;
                    if (PaymentInfo.CardExpiryYear == 0) isOk = false;
                    if (PaymentInfo.Email == null || PaymentInfo.Email == string.Empty) isOk = false;
                    if (PaymentInfo.IDCard == null || PaymentInfo.IDCard == string.Empty) isOk = false;
                    if (PaymentInfo.Name == null || PaymentInfo.Name == string.Empty) isOk = false;
                    //if (PaymentInfo.PaymentTenderCategory == null) isOk = false;
                    if (PaymentInfo.Phone == null || PaymentInfo.Phone == string.Empty) isOk = false;
                }
            }
            else
            {
                isOk = false;
            }
            return isOk;
        }

        private bool QueryBookingNumber(int CinemaID, string BookingNumber)
        {
            return true;
        }
        #endregion

        #region Metodos para el Flujo con Vippo

        /// <summary>
        /// Metodo para Auteticacion del Cliente Vippo para Envio del Codigo OTP
        /// </summary>
        /// <param name="VippoPayment">Modelo para la Autenticacion del Pago este recibe el UserSessionId en el parametro "Amount" para luego extraer el monto correcto desde VISTA con el metodo GetOrder</param>
        /// <returns></returns>
        public AVippoResponse GetVippoConfirmation(APaymentRequest VippoPayment)
        {
            AVippoResponse VippoResponse = new AVippoResponse();
            try
            {
                VippoPayment.APIKEY = ConfigurationManager.AppSettings["APIKEY"];//Esto se Encuentra en el Web.Config de la Capa3 Despues de la linea 97 <!-- VIPPO -->
                VippoPayment.Merchant_Vippo = ConfigurationManager.AppSettings["Merchant_Vippo"];//Esto se Encuentra en el Web.Config de la Capa3 Despues de la linea 97 <!-- VIPPO -->
                VippoPayment.Merchant_Reference = Guid.NewGuid().ToString().Replace("-", ""); //Se Genera el Merchant_Reference y Se Agrega en el Modelo
                GetOrderResponse OrderResponse = GetOrder(VippoPayment.Amount.ToString().Replace("-", "")); //VippoPayment en su Propiedad Amount Recibe el UserSessionId el Cual Utiliza para Obtener Monto Real de la Transaccion;
                VippoPayment.Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100).Replace(",", ".");
                VippoPayment.AuthenticationType = "Payment";
                WebClient Client = new WebClient() { Encoding = System.Text.Encoding.UTF8 };
                Client.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
                string JsonVippoPayment = JsonConvert.SerializeObject(VippoPayment);
                string Response = Client.UploadString(ConfigurationManager.AppSettings["UrlClientAuthentication"], "POST", JsonVippoPayment); //UrlClientAuthentication se Encuentra en el Web.Config de la Capa3 Despues de la linea 97 <!-- VIPPO -->
                VippoResponse = JsonConvert.DeserializeObject<AVippoResponse>(Response);
                VippoResponse.VippoCommerce = ConfigurationManager.AppSettings["Merchant_Vippo"].ToString(); //VippoCommerce y Merchant_Vippo son Sinonimos por lo cual se Coloca Nuevamente ya que El Servicio devuelve este parametro empty
                VippoResponse.Reference = VippoPayment.Merchant_Reference; // Se Coloca de Nuevo el Merchant_Reference para Ser Enviado al Flujo de Pago Natural
                return VippoResponse;
            }
            catch (Exception Ex)
            {
                VippoResponse.Label = "Error al conectar con ViPPO, por favor intente de nuevo";
                return VippoResponse;
            }
        }

        /// <summary>
        /// Metodo que se Utiliza para el Reverso de las Transaccion realizada con Vippo
        /// </summary>
        /// <param name="Reversal"></param>
        /// <returns></returns>
        private AVippoResponse GetVippoReversal(AReversalRequest Reversal, string SessionToken)
        {
            AVippoResponse VippoResponse = new AVippoResponse();
            try
            {
                Reversal.APIKEY = ConfigurationManager.AppSettings["APIKEY"];
                Reversal.Merchant_Vippo = ConfigurationManager.AppSettings["Merchant_Vippo"];
                Reversal.AuthenticationType = "Reversal";
                WebClient Client = new WebClient() { Encoding = System.Text.Encoding.UTF8 };
                Client.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
                string JsonVippoReversal = JsonConvert.SerializeObject(Reversal);
                string Response = Client.UploadString(ConfigurationManager.AppSettings["UrlClientAuthentication"], "POST", JsonVippoReversal);
                VippoResponse = JsonConvert.DeserializeObject<AVippoResponse>(Response);
                if (string.IsNullOrEmpty(VippoResponse.SessionToken))
                {
                    VippoResponse.SessionToken = SessionToken;
                }
                WebClient Client2 = new WebClient() { Encoding = Encoding.UTF8 };
                Client.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
                if (VippoResponse.Success && VippoResponse.RtnCde.Equals(0))
                {
                    RPaymentRequest ReversalRequest = new RPaymentRequest()
                    {
                        Amount = Reversal.Amount,
                        APIKEY = Reversal.APIKEY,
                        Merchant_Reference = Reversal.Merchant_Reference,
                        Merchant_Vippo = Reversal.Merchant_Vippo,
                        Payment_RRN = VippoResponse.Payment_RRN,
                        SessionToken = VippoResponse.SessionToken
                    };
                    string JsonReversal = JsonConvert.SerializeObject(ReversalRequest);
                    string ReversalResponse = Client.UploadString(ConfigurationManager.AppSettings["UrlClientReversalPayment"], "POST", JsonReversal);
                    VippoResponse = JsonConvert.DeserializeObject<AVippoResponse>(ReversalResponse);
                }
                return VippoResponse;
            }
            catch (Exception Ex)
            {
                VippoResponse.Label = "Error al conectar con ViPPO, por favor intente de nuevo";
                return VippoResponse;
            }
        }

        private void SavePurchase(BO.PaymentInfo PaymentInfo, CompleteOrderResponse VistaResponse, GetOrderResponse OrderResponse)
        {
            try
            {
                GuardarRegistro(PaymentInfo, VistaResponse, OrderResponse, "nube");
            }
            catch (Exception ex)
            {
                GuardarRegistro(PaymentInfo, VistaResponse, OrderResponse, "corp");
            }
        }

        private void GuardarRegistro(BO.PaymentInfo PaymentInfo, CompleteOrderResponse VistaResponse, GetOrderResponse OrderResponse, String origen)
        {
            string conexion = String.Empty;
            if (origen == "nube")
            {
                conexion = ConfigurationManager.ConnectionStrings["ELMAH"].ToString();
            }

            if (origen == "corp")
            {
                conexion = ConfigurationManager.ConnectionStrings["CORP"].ToString();
            }

            using (SqlConnection Connection = new SqlConnection(conexion))
            {
                string Query = $@"INSERT INTO [Mobile_Payment]([Platform],[IdTransaction],[Amount],[IDCardType],[IDCard],[Email],[Cinema_strID],[Cinema_strName],[Film_strTitle],[SessionId],[BookingNumber],[Date])
                                        VALUES('{PaymentInfo.CardTypeStr}','{PaymentInfo.VippoReference.ToString()}','{Convert.ToDecimal(OrderResponse.Order.TotalValueCents / 100)}','{PaymentInfo.IDCard.ToString().Substring(0, 1)}','{PaymentInfo.IDCard.ToString().Substring(1)}','{PaymentInfo.Email.ToString()}','{OrderResponse.Order.CinemaId}','{GetCinemaName(Convert.ToInt32(OrderResponse.Order.CinemaId))}','{OrderResponse.Order.Sessions[0].FilmTitle}',{OrderResponse.Order.Sessions[0].SessionId},'{VistaResponse.VistaBookingNumber.ToString()}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}')";
                using (SqlCommand Command = new SqlCommand(Query, Connection))
                {
                    Connection.Open();
                    var Response = Command.ExecuteNonQuery();
                    Command.Dispose();
                    Connection.Close();
                    Connection.Dispose();
                }
            }

        }

        private void SaveReversal(BO.PaymentInfo PaymentInfo, string Amount, string CinemaId)
        {
            try
            {
                GuardarReverso(PaymentInfo, Amount, CinemaId, "nube");
            }
            catch (Exception)
            {
                GuardarReverso(PaymentInfo, Amount, CinemaId, "corp");
            }
        }

        private void GuardarReverso(BO.PaymentInfo PaymentInfo, string Amount, string CinemaId, string origen)
        {
            string conexion = String.Empty;
            if (origen == "nube")
            {
                conexion = ConfigurationManager.ConnectionStrings["ELMAH"].ToString();
            }

            if (origen == "corp")
            {
                conexion = ConfigurationManager.ConnectionStrings["CORP"].ToString();
            }
            using (SqlConnection Connection = new SqlConnection(conexion))
            {
                string Query = $@"INSERT INTO [Mobile_Payment_Refund] ([Platform], [IdTransaction], [Amount], [IDCardType], [IDCard], [Email], [Date], [CinemaID])    
                                        VALUES ('{PaymentInfo.CardTypeStr}','{PaymentInfo.VippoReference}',{Amount},'{PaymentInfo.IDCard.Substring(0, 1)}','{PaymentInfo.IDCard.Substring(1)}','{PaymentInfo.Email}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{CinemaId}')";
                using (SqlCommand Command = new SqlCommand(Query, Connection))
                {
                    Connection.Open();
                    var Response = Command.ExecuteNonQuery();
                    Command.Dispose();
                    Connection.Close();
                    Connection.Dispose();
                }
            }
        }


        private string GetCinemaName(int id)
        {
            string nombre = string.Empty;
            switch (id)
            {
                case 1008:
                    nombre = "El Marqués";
                    break;
                case 1020:
                    nombre = "Galerias Avila";
                    break;
                case 1009:
                    nombre = "Galerias Paraiso";
                    break;
                case 1003:
                    nombre = "Guatire Plaza";
                    break;
                case 1027:
                    nombre = "Líder";
                    break;
                case 1001:
                    nombre = "Los Naranjos";
                    break;
                case 1002:
                    nombre = "Metro Center";
                    break;
                case 1026:
                    nombre = "Millennium";
                    break;
                case 1005:
                    nombre = "Sambil Caracas";
                    break;
                case 1015:
                    nombre = "Hiper Jumbo";
                    break;
                case 1013:
                    nombre = "La Granja";
                    break;
                case 1010:
                    nombre = "Las Americas";
                    break;
                case 1011:
                    nombre = "Metropolis";
                    break;
                case 1025:
                    nombre = "Sambil Barquisimeto";
                    break;
                case 1014:
                    nombre = "Sambil Valencia";
                    break;
                case 1006:
                    nombre = "Trinitarias";
                    break;
                case 1016:
                    nombre = "Metro Sol";
                    break;
                case 1022:
                    nombre = "costa Azul";
                    break;
                case 1017:
                    nombre = "Sambil Maracaibo";
                    break;
                case 1024:
                    nombre = "Sambil San Cristobal";
                    break;
                case 1023:
                    nombre = "Orinokia";
                    break;
                case 1007:
                    nombre = "Petroriente";
                    break;
                case 1019:
                    nombre = "Regina";
                    break;
                case 1021:
                    nombre = "Sambil Margarita";
                    break;
                case 1028:
                    nombre = "La Piramide";
                    break;
                case 1030:
                    nombre = "La Piramide V5";
                    break;
            }
            return nombre;
        }

        #endregion

        //#region Metodos para el Flujo con Mercantil

        //public AuthResponse GetAuth(AuthRequest request)
        //{
        //    string jsonreq = string.Empty;
        //    string jsonres = string.Empty;

        //    AuthResponse response = new AuthResponse();

        //    MerchantIdentify identify = new MerchantIdentify()
        //    {
        //        integratorId = ConfigurationManager.AppSettings["IntegratorId"],
        //        merchantId = ConfigurationManager.AppSettings["MerchantId"],
        //        terminalId = ConfigurationManager.AppSettings["TerminalId"]
        //    };

        //    WebClient client = new WebClient();
        //    client.Headers.Add("X-IBM-Client-Id", "9860e0f2-ed46-495e-a25f-ef377ea645f6");
        //    client.Headers.Add(HttpRequestHeader.Accept, "application/json");
        //    client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
        //    request.transaction_authInfo.payment_method = "tdd";
        //    request.transaction_authInfo.trx_type = "solaut";
        //    request.merchant_identify = identify;

        //    try
        //    {
        //        jsonreq = JsonConvert.SerializeObject(request);
        //        ServicePointManager.Expect100Continue = true;
        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //        jsonres = client.UploadString(ConfigurationManager.AppSettings["endPointTDDAuth"].ToString(), jsonreq);
        //        response = JsonConvert.DeserializeObject<AuthResponse>(jsonres);
        //        response.authentication_info.twofactor_type = Turing(response.authentication_info.twofactor_type);
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception(message: "Error al Procesar la Autenticación", innerException: e.InnerException);
        //    }

        //    return response;
        //}

        ////private PayResponse GetPay(string request)
        ////{
        ////    string jsonreq = string.Empty;
        ////    string jsonres = string.Empty;

        ////    PayResponse response = new PayResponse();

        ////    WebClient client = new WebClient();
        ////    client.Headers.Add("X-IBM-Client-Id", "9860e0f2-ed46-495e-a25f-ef377ea645f6");
        ////    client.Headers.Add(HttpRequestHeader.Accept, "application/json");

        ////    try
        ////    {
        ////        jsonreq = JsonConvert.SerializeObject(request);
        ////        ServicePointManager.Expect100Continue = true;
        ////        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        ////        jsonres = client.UploadString("https://apimbu.mercantilbanco.com:9443/mercantil-banco/desarrollo/v1/payment/pay", request);
        ////        response = JsonConvert.DeserializeObject<PayResponse>(jsonres);
        ////    }
        ////    catch (Exception e)
        ////    {
        ////        throw new Exception("Error al Procesar la Autenticación", e.InnerException);
        ////    }
        ////    return response;
        ////}

        ///// <summary>
        ///// Metodo para cifrar datos en AES con bloques de 128
        ///// </summary>
        ///// <param name="secretkey">Clave utilizada para cifrar los datos</param>
        ///// <param name="datatocypher">Datos para ser cifrados</param>
        ///// <returns></returns>
        //public string Enigma(String secretkey, string datatocypher)
        //{
        //    byte[] iv = new byte[16];
        //    byte[] array;

        //    using (Aes aes = Aes.Create())
        //    {
        //        aes.Key = ComputeSha256Hash(secretkey);
        //        aes.IV = iv;
        //        aes.Padding = PaddingMode.PKCS7;
        //        aes.Mode = CipherMode.ECB;
        //        ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
        //        using (MemoryStream memoryStream = new MemoryStream())
        //        {
        //            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
        //            {
        //                using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
        //                {
        //                    streamWriter.Write(datatocypher);
        //                }
        //                array = memoryStream.ToArray();
        //            }
        //        }
        //    }
        //    return Convert.ToBase64String(array);
        //}

        ///// <summary>
        ///// Metodo para descifrar datos en AES con bloques de 128
        ///// </summary>
        ///// <param name="secretkey">Clave utilizada para descifrar los datos</param>
        ///// <param name="datatodecrpyt">Datos para ser descifrados</param>
        ///// <returns></returns>
        //private string Turing(string datatodecrpyt)
        //{
        //    String secretkey = Turing(ConfigurationManager.AppSettings["Key"], ConfigurationManager.AppSettings["SecretKey"]);
        //    byte[] iv = new byte[16];
        //    byte[] buffer = Convert.FromBase64String(datatodecrpyt);

        //    using (Aes aes = Aes.Create())
        //    {
        //        aes.Key = ComputeSha256Hash(secretkey);
        //        aes.IV = iv;
        //        aes.Padding = PaddingMode.PKCS7;
        //        aes.Mode = CipherMode.ECB;
        //        ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

        //        using (MemoryStream memoryStream = new MemoryStream(buffer))
        //        {
        //            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
        //            {
        //                using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
        //                {
        //                    return streamReader.ReadToEnd();
        //                }
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// Metodo para descifrar datos en AES con bloques de 128
        ///// </summary>
        ///// <param name="secretkey">Clave utilizada para descifrar los datos</param>
        ///// <param name="datatodecrpyt">Datos para ser descifrados</param>
        ///// <returns></returns>
        //private string Turing(String secretkey, string datatodecrpyt)
        //{
        //    byte[] iv = new byte[16];
        //    byte[] buffer = Convert.FromBase64String(datatodecrpyt);

        //    using (Aes aes = Aes.Create())
        //    {
        //        aes.Key = ComputeSha256Hash(secretkey);
        //        aes.IV = iv;
        //        aes.Padding = PaddingMode.PKCS7;
        //        aes.Mode = CipherMode.ECB;
        //        ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

        //        using (MemoryStream memoryStream = new MemoryStream(buffer))
        //        {
        //            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
        //            {
        //                using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
        //                {
        //                    return streamReader.ReadToEnd();
        //                }
        //            }
        //        }
        //    }
        //}


        //private byte[] ComputeSha256Hash(string rawData)
        //{
        //    using (SHA256 sha256Hash = SHA256.Create())
        //    {
        //        byte[] key = new byte[16];
        //        byte[] hash = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
        //        Array.Copy(hash, key, 16);
        //        return key;
        //    }
        //}


        //#endregion

        #region Metodos para el Flujo con Banco de Venezuela

        public string BDVGeneratePayment(BDVUserInfo userinfo)
        {
            userinfo.ClientId = userinfo.ClientId.Replace("-", "");
            GetOrderResponse OrderResponse = GetOrder(userinfo.ClientId); //VippoPayment en su Propiedad Amount Recibe el UserSessionId el Cual Utiliza para Obtener Monto Real de la Transaccion;
            BDVRequest VenezuelaRequest = new BDVRequest()
            {
                amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100).Replace(",", "."),
                description = OrderResponse.Order.Sessions[0].Tickets.Length.ToString() + " Boletos para " + OrderResponse.Order.Sessions[0].FilmTitle,
                letter = userinfo.IdCardType,
                number = userinfo.IdCardNumber,
                email = userinfo.Email,
                cellphone = userinfo.Cellphone,
                urlToReturn = "http://localhost:61642/VenezuelaApi/GetResponce"
            };

            string request = JsonConvert.SerializeObject(VenezuelaRequest);
            WebClient webClient = new WebClient() { Encoding = Encoding.UTF8 };
            webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            webClient.Headers.Add(HttpRequestHeader.Authorization, "Basic NzI4NzYzOTM6enV6NVBkOFk=");

            string response = webClient.UploadString("https://biodemo.ex-cle.com:4443/ipg/web/api/Payment", request);
            BDVResponse VenezuelaResponse = JsonConvert.DeserializeObject<BDVResponse>(response);
            string SendTokenUrl = string.Format("https://biodemo.ex-cle.com:4443/ipg/web/api/SendToken?id={0}", VenezuelaResponse.paymentId);
            response = webClient.UploadString(SendTokenUrl, "{}");

            BDVTokenResponse tokenResponse = JsonConvert.DeserializeObject<BDVTokenResponse>(response);
            if (tokenResponse.responseCode != "0")
            {
                return tokenResponse.responseDescription;
            }
            return VenezuelaResponse.paymentId;
        }


        #endregion


        #region Metodos No Utilizados
        //private string GetCinemaPrefix(int id)
        //{
        //    string nombre = string.Empty;
        //    switch (id)
        //    {
        //        case 1008:
        //            nombre = "VISTAUNI";
        //            break;
        //        case 1020:
        //            nombre = "VISTAGAV";
        //            break;
        //        case 1009:
        //            nombre = "VISTAGPA";
        //            break;
        //        case 1003:
        //            nombre = "VISTAGUA";
        //            break;
        //        case 1027:
        //            nombre = "VISTALDR";
        //            break;
        //        case 1001:
        //            nombre = "VISTALNA";
        //            break;
        //        case 1002:
        //            nombre = "VISTAMCE";
        //            break;
        //        case 1026:
        //            nombre = "VISTAMLN";
        //            break;
        //        case 1005:
        //            nombre = "VISTASAM";
        //            break;
        //        case 1015:
        //            nombre = "VISTAHIP";
        //            break;
        //        case 1013:
        //            nombre = "VISTAGRA";
        //            break;
        //        case 1010:
        //            nombre = "VISTAAME";
        //            break;
        //        case 1011:
        //            nombre = "VISTAMTR";
        //            break;
        //        case 1025:
        //            nombre = "VISTASBQ";
        //            break;
        //        case 1014:
        //            nombre = "VISTASVA";
        //            break;
        //        case 1006:
        //            nombre = "VISTATRN";
        //            break;
        //        case 1016:
        //            nombre = "VISTACSU";
        //            break;
        //        case 1022:
        //            nombre = "VISTAPCA";
        //            break;
        //        case 1017:
        //            nombre = "VISTASMB";
        //            break;
        //        case 1024:
        //            nombre = "VISTASSC";
        //            break;
        //        case 1023:
        //            nombre = "VISTAORK";
        //            break;
        //        case 1007:
        //            nombre = "VISTACCP";
        //            break;
        //        case 1019:
        //            nombre = "VISTARGN";
        //            break;
        //        case 1021:
        //            nombre = "VISTASMA";
        //            break;
        //        case 1028:
        //            nombre = "VISTAPIR";
        //            break;
        //        case 1030:
        //            nombre = "VISTAPIR2012";
        //            break;
        //    }
        //    return nombre;
        //}

        //private void SendMail(Vista.BO.PaymentInfo PaymentInfo, PaymentResponse paymentResponse)
        //{
        //    try
        //    {
        //        MailService.MailClient MailClient = new MailService.MailClient();
        //        Dictionary<string, string> Dic = new Dictionary<string, string>();

        //        if (PaymentInfo != null)
        //        {
        //            if (PaymentInfo.TransactionInfo != null)
        //            {
        //                Dic.Add("HallName", PaymentInfo.TransactionInfo.HallName);
        //                Dic.Add("MovieName", PaymentInfo.TransactionInfo.MovieName);
        //                Dic.Add("Seats", PaymentInfo.TransactionInfo.Seats);
        //                Dic.Add("Date", PaymentInfo.TransactionInfo.Date);
        //                Dic.Add("Time", PaymentInfo.TransactionInfo.Time);
        //                Dic.Add("Count", PaymentInfo.TransactionInfo.Count.ToString());
        //                Dic.Add("Amount", PaymentInfo.TransactionInfo.Amount.ToString("N2"));
        //                Dic.Add("Charge", PaymentInfo.TransactionInfo.Charge.ToString("N2"));
        //                Dic.Add("Tax", PaymentInfo.TransactionInfo.Tax.ToString("N2"));
        //                Dic.Add("Total", PaymentInfo.TransactionInfo.Total.ToString("N2"));
        //                Dic.Add("BookingNumber", paymentResponse.BookingNumber.ToString());
        //                Dic.Add("Voucher", paymentResponse.VoucherHTML);
        //            }
        //        }

        //        MailClient.Send("PurchaseConfirmation", Dic, PaymentInfo.Email);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        //public Boolean RefundBooking(string TheaterID, int BookingNumber, int ValueInCents)
        //{
        //    RefundBookingResponse rp = RefundBookingResponse.GetRefundBooking(TheaterID, BookingNumber, ValueInCents);
        //    if (rp.ResultCode == 0)
        //        return true;
        //    else return false;
        //}
        //public PaymentResponse GetPaymentPremium(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        //{
        //    //CompanyID = CU: para Cines Unidos     PCA: para Parque Costa Azul
        //    #region VistaPayment
        //    GetOrderResponse OrderResponse = null;
        //    CompleteOrderResponse response = new CompleteOrderResponse();
        //    Services.Vista.DAL.DO.PaymentInfo payInfo;
        //    int BookingNumber;
        //    int TransactionNumber;
        //    string BookingId = string.Empty;
        //    try
        //    {
        //        if (!ValidatePaymentInfo(PaymentInfo))
        //        {
        //            throw new ControlledException("E0007");
        //        }

        //        OrderResponse = GetOrder(UserSessionID);
        //        if (OrderResponse.Result != DAL.DO.ResultCode.OK) throw new ControlledException("E0005");

        //        //validar si la orden tiene boletos para evitar ventas de carameleria sin boleto
        //        if (OrderResponse.Order.Sessions[0].Tickets == null || OrderResponse.Order.Sessions[0].Tickets.Length <= 0)
        //        {
        //            throw new ControlledException("E0006");
        //        }

        //        payInfo = new Services.Vista.DAL.DO.PaymentInfo();
        //        payInfo.CardNumber = PaymentInfo.CardNumber;
        //        payInfo.CardType = PaymentInfo.CardTypeStr;

        //        payInfo.PaymentValueCents = OrderResponse.Order.TotalValueCents;
        //        if (PaymentInfo.CardType != PaymentCardType.GIFTCARD)
        //        {
        //            payInfo.CardCVC = PaymentInfo.CardCVV;
        //            payInfo.CardExpiryMonth = PaymentInfo.CardExpiryMonth.ToString();
        //            payInfo.CardExpiryYear = PaymentInfo.CardExpiryYear.ToString();
        //            payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
        //        }
        //        else
        //        {
        //            payInfo.CardType = "SVS";
        //            payInfo.CardCVC = "111";
        //            payInfo.CardExpiryMonth = "12";
        //            payInfo.CardExpiryYear = "99";
        //            payInfo.PaymentTenderCategory = PaymentTenderCategory.SVC.ToString();
        //        }
        //        response = CompleteOrderResponse.CompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);
        //        // response.Result = DAL.DO.ResultCode.OK;

        //        if (response.Result != DAL.DO.ResultCode.OK)
        //        {
        //            throw new ControlledException("E0006");
        //        }

        //        BookingNumber = Int32.Parse(response.VistaBookingNumber);
        //        TransactionNumber = Int32.Parse(response.VistaTransNumber);
        //        BookingId = response.VistaBookingId;
        //    }
        //    catch (ControlledException ex)
        //    {
        //        PaymentResponse paymentResponse = new PaymentResponse();
        //        paymentResponse.CodeResult = ex.Code;
        //        paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //        return paymentResponse;
        //    }
        //    catch (Exception ex)
        //    {
        //        PaymentResponse paymentResponse = new PaymentResponse();
        //        paymentResponse.CodeResult = "E0001";
        //        paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //        return paymentResponse;
        //    }
        //    #endregion 

        //    //Compra tradicional
        //    if (PaymentInfo.CardType != PaymentCardType.GIFTCARD)
        //    {
        //        #region MerchantPayment
        //        try
        //        {
        //            string CompanyID = VISTAITCinema.getInfoTheater(OrderResponse.Order.CinemaId).Company;

        //            Payment_Services.PaymentRequest paymentResquest = new Payment_Services.PaymentRequest();
        //            paymentResquest.Amount = (decimal)OrderResponse.Order.TotalValueCents / 100;
        //            paymentResquest.BookingNumber = BookingNumber;
        //            paymentResquest.Cinema = OrderResponse.Order.CinemaId;
        //            paymentResquest.ClientId = ClientID; //TODO: llenar este campo            
        //            paymentResquest.CVV = PaymentInfo.CardCVV;
        //            paymentResquest.ExpMonth = PaymentInfo.CardExpiryMonth;
        //            paymentResquest.ExpYear = PaymentInfo.CardExpiryYear;
        //            paymentResquest.Factura = OrderResponse.Order.VistaTransactionNumber;
        //            paymentResquest.IDCard = PaymentInfo.IDCard;
        //            paymentResquest.Name = PaymentInfo.Name;
        //            paymentResquest.CardNumber = PaymentInfo.CardNumber;
        //            paymentResquest.VistaOrderId = UserSessionID; //TODO: llenar este campo     
        //            paymentResquest.CompanyId = CompanyID;
        //            Payment_Services.Payment payment = new Payment_Services.Payment();

        //            bool value = Convert.ToBoolean(ConfigurationManager.AppSettings["DisableMerchant"]);

        //            if (value == false)
        //                payment.ApproveFromParameters(paymentResquest);
        //            else
        //            {
        //                payment.CodeResult = "00";
        //                payment.Voucher = "<linea>Prueba</linea><linea>sin</linea><linea>pasar</linea>por</linea><linea>merchant</linea>";
        //            }

        //            PaymentResponse paymentResponse = new PaymentResponse();
        //            paymentResponse.SetVoucher(payment.Voucher);
        //            paymentResponse.BookingNumber = BookingNumber;
        //            paymentResponse.CodeResult = payment.CodeResult;
        //            paymentResponse.DescriptionResult = payment.DescriptionResult;
        //            paymentResponse.TransactionNumber = TransactionNumber;
        //            paymentResponse.BookingId = BookingId;


        //            if (payment.CodeResult != "00")
        //            {
        //                if (payment.CodeResult == string.Empty)
        //                {
        //                    GenericException gex = new GenericException();
        //                    gex.Code = "E0002";
        //                    throw new FaultException<GenericException>(gex, new FaultReason(gex.Code));
        //                }
        //                else
        //                {
        //                    Services.Vista.BL.Payment Payment = new Payment();
        //                    Payment.RestRefundBooking(OrderResponse.Order.CinemaId, BookingNumber, payInfo.PaymentValueCents);
        //                }
        //            }
        //            else
        //            {
        //                //Si la compra tiene concesiones
        //                if ((OrderResponse.Order.Concessions != null) && (OrderResponse.Order.Concessions.Count() > 0))
        //                {
        //                    //Luis Ramírez 31/10/2017, en la tabla tblCUOrdenInvConcessions, campo PaymentOK se marca en Y indicando que la compra fue procesada
        //                    Concessions.ProcessOrderConcessions(UserSessionID, OrderResponse.Order.CinemaId);
        //                }
        //                //---------------------------------------------Mail ------------------------------------------------------//
        //                // this.SendMail(PaymentInfo, paymentResponse);
        //                //--------------------------------------------------------------------------------------------------------//
        //            }
        //            return paymentResponse;
        //        }
        //        catch (FaultException fex)
        //        {
        //            Services.Vista.BL.Payment Payment = new Payment();
        //            Payment.RestRefundBooking(OrderResponse.Order.CinemaId, BookingNumber, payInfo.PaymentValueCents);

        //            PaymentResponse paymentResponse = new PaymentResponse();
        //            paymentResponse.CodeResult = fex.Reason.ToString();
        //            paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //            return paymentResponse;
        //        }
        //        catch (Exception ex)
        //        {
        //            Services.Vista.BL.Payment Payment = new Payment();
        //            Payment.RestRefundBooking(OrderResponse.Order.CinemaId, BookingNumber, payInfo.PaymentValueCents);
        //            PaymentResponse paymentResponse = new PaymentResponse();
        //            paymentResponse.CodeResult = "E0002";
        //            paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //            return paymentResponse;
        //        }
        //        #endregion
        //    }
        //    //Compra con GiftCard
        //    else
        //    {
        //        PaymentResponse paymentResponse = new PaymentResponse();
        //        //paymentResponse.SetVoucher(payment.Voucher);
        //        paymentResponse.BookingNumber = BookingNumber;
        //        paymentResponse.CodeResult = "S0001";
        //        paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //        return paymentResponse;
        //    }
        //}

        //public PaymentResponse GetPayment(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Vista.BO.PaymentInfo PaymentInfo)
        //{

        //    Live Live = new Live();

        //    Services.Vista.BO.SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);
        //    List<Services.Vista.DAL.DO.TicketType> tts = new List<DAL.DO.TicketType>();
        //    foreach (Services.Vista.BO.TicketType item in TicketTypes)
        //    {
        //        BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();
        //        Services.Vista.DAL.DO.TicketType tt = new DAL.DO.TicketType();
        //        tt.PriceInCents = (Int32)(Ticket.Price * 100);
        //        tt.Qty = item.Quantity;
        //        tt.TicketTypeCode = item.TicketTypeCode;
        //        tts.Add(tt);
        //    }
        //    AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());

        //    return this.GetPaymentPremiumUltimate(ClientID, UserSessionID, PaymentInfo);

        //} 
        #endregion

        #region Pago Mercantil

        #region MyRegion
        //        public PaymentResponse GetPaymentMercantil2(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        //        {
        //            PaymentResponse paymentResponse = new PaymentResponse();
        //#if DEBUG
        //            string BasePath = @"C:\PaymentLogs";
        //#else
        //            string BasePath = @"D:\PaymentLogs";
        //#endif
        //            string FileName = "Payment_" + PaymentInfo.IDCard.ToString() + "_" + Guid.NewGuid().ToString() + ".txt";
        //            string LogPath = Path.Combine(BasePath, FileName);
        //            StreamWriter streamWriter = new StreamWriter(LogPath);
        //            try
        //            {
        //                if (!ValidatePaymentInfo(PaymentInfo)) { throw new ControlledException("E0007"); } //pendiente por cambiar el metodo de validacíón

        //                GetOrderResponse OrderResponse = GetOrder(UserSessionID);

        //                if (OrderResponse.Result != ResultCode.OK) { throw new ControlledException("E0005"); }

        //                if (OrderResponse.Order.Sessions[0].Tickets == null || OrderResponse.Order.Sessions[0].Tickets.Length <= 0) { throw new ControlledException("E0006"); }

        //                Payment_Services.Payment payment = new Payment_Services.Payment();




        //                try
        //                {
        //                    WebClient Client = new WebClient() { Encoding = System.Text.Encoding.UTF8 };
        //                    Client.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
        //                    CPaymentRequest PaymentRequest = new CPaymentRequest()
        //                    {
        //                        APIKEY = ConfigurationManager.AppSettings["APIKEY"],
        //                        Merchant_Vippo = ConfigurationManager.AppSettings["Merchant_Vippo"],
        //                        Merchant_Reference = PaymentInfo.Reference,
        //                        Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100),
        //                        SessionToken = PaymentInfo.SessionToken,
        //                        OTPClient = PaymentInfo.OTPCode
        //                    };
        //                    string JsonVippoPayment = JsonConvert.SerializeObject(PaymentRequest);
        //                    streamWriter.WriteLine(string.Format("I85 |se realiza Pago en Vippo"));
        //                    string Response = Client.UploadString(ConfigurationManager.AppSettings["UrlClientPaymentConfirm"], "POST", JsonVippoPayment);
        //                    PaymentRequest.APIKEY = string.Empty;
        //                    string JsonPaymentRequest = JsonConvert.SerializeObject(PaymentRequest);
        //                    streamWriter.WriteLine(string.Format("S89 | objeto que se envia a vippo {0}", JsonPaymentRequest));
        //                    streamWriter.WriteLine(string.Format("F90 |respuesta de vippo: {0} ", Response));
        //                    VippoResponse = JsonConvert.DeserializeObject<AVippoResponse>(Response);
        //                    if (VippoResponse.Success.Equals(true))
        //                    {
        //                        payment.CodeResult = "00";
        //                        payment.Voucher = "<linea>Pago Realizado</linea><linea>Con</linea><linea>Cuenta</linea><linea>ViPPO</linea>";
        //                    }
        //                    else
        //                    {
        //                        paymentResponse.CodeResult = "V" + VippoResponse.RtnCde.ToString();
        //                        paymentResponse.DescriptionResult = VippoResponse.Label;
        //                        streamWriter.WriteLine(string.Format("S101 |Pago con Vippo resulto fallido respuesta al cliente {0} | {1} ", paymentResponse.CodeResult, paymentResponse.DescriptionResult));
        //                        streamWriter.Dispose();
        //                        streamWriter.Close();
        //                        return paymentResponse;
        //                    }
        //                }
        //                catch (Exception Ex)
        //                {
        //                    string inner = (Ex.InnerException != null ? Ex.Message + ": " + Ex.InnerException.ToString() : Ex.Message);
        //                    paymentResponse.CodeResult = "V0001";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    streamWriter.WriteLine(string.Format("C112 |Fallo en el flujo de pago con vippo: {0}", inner));
        //                    streamWriter.Dispose();
        //                    streamWriter.Close();
        //                    return paymentResponse;
        //                }

        //                paymentResponse.SetVoucher(payment.Voucher);
        //                paymentResponse.CodeResult = payment.CodeResult;
        //                paymentResponse.DescriptionResult = payment.DescriptionResult;
        //                paymentResponse.SequenceNumber = payment.SequenceNumber; //Codigo en Caso de Falla en Merchant para Reembolso de la Transaccion

        //                //Se verifica que el Pago Procesado no es Fallido en Merchant
        //                if (payment.CodeResult != "00")
        //                {
        //                    paymentResponse.CodeResult = "E0002";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    streamWriter.WriteLine(string.Format("S174 | payment code resul diferente de 0 informacion {0} | {1} ", paymentResponse.CodeResult, paymentResponse.DescriptionResult));
        //                    streamWriter.Dispose();
        //                    streamWriter.Close();
        //                    return paymentResponse;
        //                }

        //                //Se verifica que el Pago Procesado no es Fallido en Vippo
        //                if (VippoResponse.Success != true && PaymentInfo.CardTypeStr == PaymentCardType.PAGOVIPPO.ToString())
        //                {
        //                    paymentResponse.CodeResult = "V0004";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    streamWriter.WriteLine(string.Format("S185 | respuesta de vippo fallida, informacion:  {0} | {1} ", paymentResponse.CodeResult, paymentResponse.DescriptionResult));
        //                    streamWriter.Dispose();
        //                    streamWriter.Close();
        //                    return paymentResponse;
        //                }

        //                //Como el código es 00 la transaccion respondio de manera correcta entonces debemos ver el XML y ver si es aprobada.  
        //                //Se hace la inserción en vista de la operacion ya que fue aprobada.
        //                DAL.DO.PaymentInfo payInfo = new DAL.DO.PaymentInfo();

        //                switch (PaymentInfo.CardType)
        //                {
        //                    case PaymentCardType.GIFTCARD:
        //                        payInfo.CardType = PaymentInfo.CardTypeStr;
        //                        payInfo.CardNumber = PaymentInfo.CardNumber;
        //                        payInfo.CardType = "SVS";
        //                        payInfo.CardCVC = "111";
        //                        payInfo.CardExpiryMonth = "12";
        //                        payInfo.CardExpiryYear = "99";
        //                        payInfo.PaymentTenderCategory = PaymentTenderCategory.SVC.ToString();
        //                        break;
        //                    case PaymentCardType.PAGOVIPPO:
        //                        PaymentInfo.Name = PaymentInfo.IDCard;
        //                        payInfo.CardType = PaymentInfo.CardTypeStr;
        //                        payInfo.CardNumber = "6666777788889999";
        //                        payInfo.CardCVC = "111";
        //                        payInfo.CardExpiryMonth = "12";
        //                        payInfo.CardExpiryYear = "99";
        //                        payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
        //                        break;
        //                    default:
        //                        payInfo.CardType = PaymentInfo.CardTypeStr;
        //                        payInfo.CardNumber = PaymentInfo.CardNumber;
        //                        payInfo.CardCVC = PaymentInfo.CardCVV;
        //                        payInfo.CardExpiryMonth = PaymentInfo.CardExpiryMonth.ToString();
        //                        payInfo.CardExpiryYear = PaymentInfo.CardExpiryYear.ToString();
        //                        payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
        //                        break;
        //                }
        //                payInfo.PaymentValueCents = OrderResponse.Order.TotalValueCents;


        //                DAL.DO.PaymentInfo info = new DAL.DO.PaymentInfo();
        //                info.CardType = payInfo.CardType;
        //                info.CardNumber = string.Format("{0}******{1}", payInfo.CardNumber.Substring(0, 6), payInfo.CardNumber.Substring(12, 4));
        //                info.CardCVC = "";
        //                info.CardExpiryMonth = payInfo.CardExpiryMonth;
        //                info.CardExpiryYear = payInfo.CardExpiryYear;
        //                info.PaymentTenderCategory = payInfo.PaymentTenderCategory;
        //                info.PaymentValueCents = payInfo.PaymentValueCents;
        //                string jsonpayinfo = JsonConvert.SerializeObject(info);
        //                streamWriter.WriteLine(string.Format("I228 |se inicia guardado en vista CompleteOrder: {0} | {1} | {2} | {3}", ClientID, UserSessionID, jsonpayinfo, PaymentInfo.Name));

        //                CompleteOrderResponse VistaResponse = CompleteOrderResponse.CompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);


        //                string jsonVistaResponse = VistaResponse != null ? JsonConvert.SerializeObject(VistaResponse) : "La orden en vista vino nula";
        //                streamWriter.WriteLine(string.Format("F233 |respuesta del CompleteOrder: {0} ", jsonVistaResponse));

        //                //Se Marcar la Orden como Pagada en las tablas de Seguimiento de Concesiones de CinesUnidos
        //                //Luis Ramírez 31/10/2017, en la tabla tblCUOrdenInvConcessions, campo PaymentOK se marca en Y indicando que la compra fue procesada
        //                //George Embaid 12/08/2019 se anido el if para evitar un fallo de logica
        //                if (OrderResponse.Order.Concessions != null)
        //                {
        //                    if (OrderResponse.Order.Concessions.Count() > 0)
        //                    {
        //                        Concessions.ProcessOrderConcessions(UserSessionID, OrderResponse.Order.CinemaId);
        //                    }
        //                }

        //                streamWriter.WriteLine(string.Format("S247 |en caso de Reverso se validan estos datos : {0} | {1}  ", jsonVistaResponse, VistaResponse.Result.ToString()));
        //                if (VistaResponse == null || (VistaResponse.Result != ResultCode.OK && VistaResponse.Result != ResultCode.PaymentVoidSuccessPostCommit))
        //                {
        //                    switch (PaymentInfo.CardType)
        //                    {
        //                        case PaymentCardType.PAGOVIPPO:
        //                            AReversalRequest Reversal = new AReversalRequest()
        //                            {
        //                                Merchant_Reference = PaymentInfo.Reference,
        //                                Client_UserVippo = string.Empty,
        //                                Client_PasswordVippo = string.Empty,
        //                                Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100)
        //                            };
        //                            AVippoResponse ReversalResponse = GetVippoReversal(Reversal, PaymentInfo.SessionToken);
        //                            string jsonGetVippoReversal = JsonConvert.SerializeObject(ReversalResponse);
        //                            streamWriter.WriteLine(string.Format("S260 |respuesta del Reverso: {0}  ", jsonGetVippoReversal));
        //                            SaveReversal(PaymentInfo, Reversal.Amount, OrderResponse.Order.CinemaId);
        //                            paymentResponse.SetVoucher("");
        //                            paymentResponse.CodeResult = "V" + ReversalResponse.RtnCde.ToString();
        //                            paymentResponse.DescriptionResult = ReversalResponse.Label;
        //                            streamWriter.WriteLine(string.Format("F265 | finaliza el Reverso: {0} | {1}  ", paymentResponse.CodeResult, paymentResponse.DescriptionResult));
        //                            streamWriter.Dispose();
        //                            streamWriter.Close();
        //                            return paymentResponse;
        //                        default:
        //                            streamWriter.WriteLine(string.Format("I270 |la operacion se incia a reverzar: {0} | {1}  ", jsonVistaResponse, VistaResponse.Result.ToString()));
        //                            // se hace la anulacion de la operacion en merchant empleando
        //                            payment.AnularPagoEnMerchant(PayRequest, payment);
        //                            paymentResponse.CodeResult = "E0006";
        //                            paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                            streamWriter.WriteLine(string.Format("I275 |pago se a reversado: {0} | {1} | {2} | {3} ", jsonVistaResponse, VistaResponse.Result.ToString(), paymentResponse.CodeResult, paymentResponse.DescriptionResult));
        //                            streamWriter.Dispose();
        //                            streamWriter.Close();
        //                            return paymentResponse;
        //                    }
        //                }
        //                paymentResponse.BookingId = VistaResponse.VistaBookingId;
        //                paymentResponse.BookingNumber = Int32.Parse(VistaResponse.VistaBookingNumber);
        //                paymentResponse.TransactionNumber = Int32.Parse(VistaResponse.VistaTransNumber);
        //                if (PaymentInfo.CardTypeStr.Equals(PaymentCardType.PAGOVIPPO.ToString()))
        //                {
        //                    streamWriter.WriteLine(string.Format("S286 | inicio el guardado de la compra en la base de datos mobile_payment "));
        //                    SavePurchase(PaymentInfo, VistaResponse, OrderResponse);
        //                    streamWriter.WriteLine(string.Format("S288 | finalizo el guardado de la compra en la base de datos mobile_payment "));
        //                }
        //                streamWriter.Dispose();
        //                streamWriter.Close();
        //                return paymentResponse;
        //            }
        //            catch (Exception Ex)
        //            {
        //                streamWriter.WriteLine(string.Format("S294 |Exception Mesage: {0}  ", (Ex.InnerException != null ? string.Concat(Ex.Message, ":  ", Ex.InnerException.ToString()) : Ex.Message)));
        //                streamWriter.Dispose();
        //                streamWriter.Close();
        //                if (PaymentInfo.CardTypeStr.Equals("PAGOVIPPO"))
        //                {
        //                    paymentResponse.CodeResult = "V0003";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    return paymentResponse;
        //                }
        //                else
        //                {
        //                    paymentResponse.CodeResult = "E0001";
        //                    paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
        //                    return paymentResponse;
        //                }
        //            }
        //        } 
        #endregion

        public PaymentResponse GetAuthMercantil(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        {
//#if DEBUG
            string BasePath = @"C:\PaymentLogs\MERCANTIL";
            if (!Directory.Exists(BasePath))
                Directory.CreateDirectory(BasePath);
            //#else
            //            string BasePath = @"D:\PaymentLogs\MERCANTIL";
            //#endif
            AuthResponse _authResponse = null;
            PaymentResponse paymentResponse = new PaymentResponse();
            string FileName = "Payment_" + PaymentInfo.IDCard.ToString() + "_" + Guid.NewGuid().ToString() + ".txt";
            string LogPath = Path.Combine(BasePath, FileName);
            StreamWriter sw = new StreamWriter(LogPath);

            try
            {
                //Servicio Mercantil
                MerchantIdentify _merchantIdentify = new MerchantIdentify()
                {
                    integratorId = ConfigurationManager.AppSettings["IntegratorId"],
                    merchantId = ConfigurationManager.AppSettings["MerchantId"],
                    terminalId = ConfigurationManager.AppSettings["TerminalId"]
                };
                Location _location = new Location() { };
                Mobile _mobile = new Mobile()
                {
                    location = _location
                };
                ClientIdentify _clientIdentify = new ClientIdentify()
                {
                    mobile = _mobile,
                };

                TransactionAuthInfo _transactionAuthInfo = new TransactionAuthInfo()
                {
                    trx_type = "compra",
                    payment_method = "tdd",
                    card_number = PaymentInfo.CardNumber,
                    customer_id = PaymentInfo.IDCard
                };

                AuthRequest _authRequest = new AuthRequest()
                {
                    merchant_identify = _merchantIdentify,
                    client_identify = _clientIdentify,
                    transaction_authInfo = _transactionAuthInfo
                };

                AuthResponse auth = GetAuth(_authRequest);

                GlobalAuthResponse = auth;

                _authResponse = auth;

                paymentResponse.DescriptionResult = auth.authentication_info.twofactor_type;
            }
            catch(Exception Ex)
            {
                string inner = (Ex.InnerException != null ? Ex.Message + ": " + Ex.InnerException.ToString() : Ex.Message);
                paymentResponse.CodeResult = "M";
                paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
                sw.WriteLine(string.Format("C112 |Fallo en el flujo de pago con autenticación pago mercantil: {0}", inner));
                sw.Dispose();
                sw.Close();
                _authResponse = null;
            }

            return paymentResponse;
        }

        /// <summary>
        /// George Embaid
        /// Metodo de Pago Mercantil mediante el uso de Tarjetas de Debitos.
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="UserSessionID"></param>
        /// <param name="PaymentInfo"></param>
        /// <returns></returns>
        public PaymentResponse GetPaymentMercantil(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        {
            string typeTran = "";
            string trxType = "compra";
            string paymentMethod = "tdd";
            string money = "ves";
            long _random = 0;

            //if (contFront == 0)
            //{
            //    //AuthResponse auth = GetAuthMercantil(ClientID, UserSessionID, PaymentInfo);
            //    contFront += 1;
            //    PaymentResponse response = new PaymentResponse();
            //    response.CodeResult = "Z";
            //    response.SetVoucher("");
            //    return response;
            //}

            //Crear carpeta en la ruta para guardar los logs

            //#if DEBUG
            string BasePath = @"C:\PaymentLogs\MERCANTIL";
            if (!Directory.Exists(BasePath))
                Directory.CreateDirectory(BasePath);
            //#else
            //            string BasePath = @"D:\PaymentLogs\MERCANTIL";
            //#endif
            string FileName = "Payment_" + PaymentInfo.IDCard.ToString() + "_" + Guid.NewGuid().ToString() + ".txt";
            string LogPath = Path.Combine(BasePath, FileName);
            StreamWriter sw = new StreamWriter(LogPath);

            PaymentResponse paymentResponse = new PaymentResponse();

            //Se Busca toda la informacion de la orden en vista.
            GetOrderResponse OrderResponse = GetOrder(UserSessionID);

            //Se valida que la orden este en vista.
            if (OrderResponse.Result != ResultCode.OK)
            {
                sw.WriteLine("L1151 | No se encontro la Orden N° en VISTA | {0} |", UserSessionID);
                sw.Dispose();
                sw.Close();
                throw new ControlledException("E0005");
            }

            //Se valida que la orden contenga asientos asociados.
            if (OrderResponse.Order.Sessions[0].Tickets == null || OrderResponse.Order.Sessions[0].Tickets.Length <= 0)
            {
                string obj = JsonConvert.SerializeObject(OrderResponse);
                sw.WriteLine("L1159 | La Orden no contiene Asientos Asociados | {0} |", obj);
                sw.Dispose();
                sw.Close();
                throw new ControlledException("E0006");
            }

            try
            {

        
                #region Servicio Mercantil
                MerchantIdentify mi = new MerchantIdentify()
                {
                    integratorId = ConfigurationManager.AppSettings["IntegratorId"],
                    merchantId = ConfigurationManager.AppSettings["MerchantId"],
                    //terminalId = ConfigurationManager.AppSettings["TerminalId"]
                    terminalId = "0"
                };
                Location l = new Location() { };
                Mobile m = new Mobile()
                {
                    manufacturer = "Samsung",
                    model = "S9",
                    os_version = "Oreo 9.1",
                    location = l
                };
                ClientIdentify ci = new ClientIdentify()
                {
                    ipaddress = "10.0.0.1",
                    browser_agent = "Chrome",
                    mobile = m,
                };

                TransactionAuthInfo ta = new TransactionAuthInfo()
                {
                    trx_type = trxType,
                    payment_method = paymentMethod,
                    card_number = PaymentInfo.CardNumber,
                    customer_id = PaymentInfo.IDCard
                };

                AuthRequest aur = new AuthRequest()
                {
                    merchant_identify = mi,
                    client_identify = ci,
                    transaction_authInfo = ta
                };


                #endregion

                //AuthResponse auth = GetAuth(aur);

                switch (GlobalAuthResponse.authentication_info.twofactor_type)
                {
                    case "clavetelefonica":
                        typeTran = Encripta(PaymentInfo.KeyAuth);
                        break;
                    case "cvv":
                        typeTran = Encripta(PaymentInfo.CardCVV);
                        break;
                    case "token":
                        typeTran = Encripta(PaymentInfo.SessionToken);
                        break;
                    default:
                        typeTran = "";
                        break;
                }

                _random = LongRandom(1, 500000000000000, new Random());
                PaymentInfo.VippoReference = _random.ToString();

                //El monto viene con decimales, le quito los decimales dado que viene asi 6300000 
                //donde los dos ultimos digitos son los dos decimales.
                var montoCobrarMercantil = OrderResponse.Order.TotalValueCents/100;

                Transaction t = new Transaction()
                {
                    customer_id = PaymentInfo.IDCard,
                    invoice_number = _random.ToString(),
                    trx_type = trxType,
                    payment_method = paymentMethod,
                    twofactor_auth = typeTran,
                    account_type = PaymentInfo.AccountType,
                    card_number = PaymentInfo.CardNumber,
                    cvv = typeTran,
                    expiration_date = "20" + PaymentInfo.CardExpiryYear.ToString() + "/" + PaymentInfo.CardExpiryMonth.ToString(),
                    amount = double.Parse(montoCobrarMercantil.ToString()),
                    currency = money
                };

                PayRequest pr = new PayRequest()
                {
                    merchant_identify = mi,
                    client_identify = ci,
                    transaction = t
                };

                string req = JsonConvert.SerializeObject(pr);
                sw.WriteLine(string.Format("L340 | Se Inicia el Proceso de Pago para Mercantil | |"));
                sw.WriteLine(string.Format("L341 | Objeto que se envia al Servicio de Mercantil | {0} |", pr));
                //  AuthResponse token = GetAuth(req);

                // PayResponse res = GetPay(req);

                // SE ENVIA EL PAGO A MERCANTIL
                PayDebitCardResponse respuestaPago = GetPay(req);
                PayResponse res = new PayResponse();

                //se hace validacion para saber si es error o respuesta correcta.

                //la respuesta es diferente de 200 del servidor
                if (respuestaPago.respuesta == false)
                {

                    paymentResponse.DescriptionResult = respuestaPago.payResponseError.status.description;
                    paymentResponse.CodeResult = respuestaPago.payResponseError.status.error_code;

                    sw.WriteLine(string.Format("L353 | No se Pudo Realizar el Pago en Mercantil | {0} | {1} ", paymentResponse.CodeResult, paymentResponse.DescriptionResult));
                    sw.Dispose();
                    sw.Close();
                    return paymentResponse;
                }


                    res = respuestaPago.payResponse;
                        


                sw.WriteLine(string.Format("L345 | Respuesta del Servicio de Pago de Mercantil | {0} |", res));

                Payment_Services.Payment payment = new Payment_Services.Payment();
                        
                           
                            if (res.transaction_response.trx_status != "approved")
                            {
                                paymentResponse.CodeResult = "M";

                                if (res.transaction_response.trx_status == "rejected")
                                {
                                    paymentResponse.DescriptionResult = "RECHAZADA";
                                }
                                //else
                                //{
                                //    paymentResponse.DescriptionResult = "LA COMPRA NO FUE APROBADA";
                                //}              
                    
                                sw.WriteLine(string.Format("L353 | No se Pudo Realizar el Pago en Mercantil | {0} | {1} ", paymentResponse.CodeResult, paymentResponse.DescriptionResult));
                                sw.Dispose();
                                sw.Close();
                                return paymentResponse;
                            }


                //de lo contrario, continuo con el porceo de registrar la compra en vista.
                payment.CodeResult = "00";
                payment.Voucher = "<linea>Pago Realizado</linea><linea>Con</linea><linea>Cuenta</linea><linea>Mercantil</linea>";
                //PaymentInfo.Name = "";
                DAL.DO.PaymentInfo payInfo = new DAL.DO.PaymentInfo();
                payInfo.CardType = PaymentInfo.CardTypeStr;
                payInfo.CardNumber = PaymentInfo.CardNumber;
                payInfo.CardCVC = PaymentInfo.CardCVV;
                payInfo.CardExpiryMonth = PaymentInfo.CardExpiryMonth.ToString();
                payInfo.CardExpiryYear = PaymentInfo.CardExpiryYear.ToString();
                payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
                payInfo.PaymentValueCents = OrderResponse.Order.TotalValueCents;
                

                //Datos para Logs
                DAL.DORest.PaymentInfo info = new DAL.DORest.PaymentInfo();
                info.CardType = payInfo.CardType;
                //info.CardNumber = string.Format("{0}******{1}", payInfo.CardNumber.Substring(0, 6), payInfo.CardNumber.Substring(12, 4));                
                info.CardNumber = string.Format("{0}********{1}", payInfo.CardNumber.Substring(0, 6), payInfo.CardNumber.Substring(14, 4));
                info.CardCVC = payInfo.CardCVC;
                info.CardExpiryMonth = payInfo.CardExpiryMonth;
                info.CardExpiryYear = payInfo.CardExpiryYear;
                info.PaymentTenderCategory = payInfo.PaymentTenderCategory;
                info.PaymentValueCents = payInfo.PaymentValueCents;
                string jsonpayinfo = JsonConvert.SerializeObject(info);
                sw.WriteLine(string.Format("L370 |se inicia guardado en vista CompleteOrder: {0} | {1} | {2} | {3}", ClientID, UserSessionID, jsonpayinfo, PaymentInfo.Name));
                //fin datos para Log
                CompleteOrderResponse VistaResponse = RestTicketingService.RestCompleteOrder(ClientID, UserSessionID, info, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);

//                CompleteOrderResponse VistaResponse = CompleteOrderResponse.CompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);
                string jsonVistaResponse = VistaResponse != null ? JsonConvert.SerializeObject(VistaResponse) : "La orden en vista vino nula";
                sw.WriteLine(string.Format("L373 | respuesta del CompleteOrder: {0} ", jsonVistaResponse));
                //si no se puedo guardar la orden en vista, se devuelve el dinero en VIppo
                //VistaResponse.Result != ResultCode.PaymentVoidSuccessPostCommit

                if (VistaResponse == null || VistaResponse.Result != ResultCode.OK)
                {
                    //sw.WriteLine("L377 | Se inicia el reverso en Debito Mercantil");
                    //AReversalRequest Reversal = new AReversalRequest()
                    //{
                    //    Merchant_Reference = PaymentInfo.Reference,
                    //    Client_UserVippo = string.Empty,
                    //    Client_PasswordVippo = string.Empty,
                    //    Amount = Convert.ToString((decimal)OrderResponse.Order.TotalValueCents / 100)
                    //};
                    //sw.WriteLine("L385 | Objeto para el reverso en vippo: {0}", JsonConvert.SerializeObject(Reversal));
                    //AVippoResponse ReversalResponse = GetVippoReversal(Reversal, PaymentInfo.SessionToken);
                    //sw.WriteLine("L387 | Respuesta del reverso en vippo: {0}", JsonConvert.SerializeObject(ReversalResponse));
                    //SaveReversal(PaymentInfo, Reversal.Amount, OrderResponse.Order.CinemaId);
                    //sw.WriteLine("L391 | Se guardo el reverso en azure...");
                    //sw.Dispose();
                    //sw.Close();
                    paymentResponse.SetVoucher("");
                    paymentResponse.CodeResult = "2525";
                    paymentResponse.DescriptionResult = "Ocurrio un error al intentar de confirmar la compra en el cine.";
                     sw.WriteLine("2091 | Ocurrio un error al intentar de confirmar la compra en el cine. Vista devolvio diferente de OK o nulo");
                    return paymentResponse;
                }

                //Se Marcar la Orden como Pagada en las tablas de Seguimiento de Concesiones de CinesUnidos
                //Luis Ramírez 31/10/2017, en la tabla tblCUOrdenInvConcessions, campo PaymentOK se marca en Y indicando que la compra fue procesada
                //RM - No se si se use aun.
                if ((OrderResponse.Order.Concessions != null) && (OrderResponse.Order.Concessions.Count() > 0))
                {
                    Concessions.ProcessOrderConcessions(UserSessionID, OrderResponse.Order.CinemaId);
                }

                paymentResponse.SetVoucher(payment.Voucher);
                paymentResponse.CodeResult = payment.CodeResult;
                paymentResponse.DescriptionResult = payment.DescriptionResult;
                paymentResponse.SequenceNumber = payment.SequenceNumber; //Codigo en Caso de Falla en Merchant para Reembolso de la Transaccion
                paymentResponse.BookingId = VistaResponse.VistaBookingId;
                paymentResponse.BookingNumber = Int32.Parse(VistaResponse.VistaBookingNumber);
                paymentResponse.TransactionNumber = Int32.Parse(VistaResponse.VistaTransNumber);
                SavePurchase(PaymentInfo, VistaResponse, OrderResponse);
                sw.WriteLine("L411 | Se guardo la compra en azure..");
                sw.Dispose();
                sw.Close();
            }
            catch (Exception Ex)
            {
                string inner = (Ex.InnerException != null ? Ex.Message + ": " + Ex.InnerException.ToString() : Ex.Message);
                paymentResponse.CodeResult = "M";
                paymentResponse.DescriptionResult = inner;
                sw.WriteLine(string.Format("C112 |Fallo en el flujo de pago con Banco Mercantil: {0}", inner));
                sw.Dispose();
                sw.Close();
                return paymentResponse;
            }
            return paymentResponse;
        }

        public AuthResponse GetAuth(AuthRequest request)
        {
            string jsonreq = string.Empty;
            string jsonres = string.Empty;

            AuthResponse response = new AuthResponse();

            MerchantIdentify identify = new MerchantIdentify()
            {
                integratorId = ConfigurationManager.AppSettings["IntegratorId"],
                merchantId = ConfigurationManager.AppSettings["MerchantId"],
                terminalId = ConfigurationManager.AppSettings["TerminalId"]
            };

            WebClient client = new WebClient();           
            client.Headers.Add("X-IBM-Client-Id", ConfigurationManager.AppSettings["ClientID"]);
            client.Headers.Add(HttpRequestHeader.Accept, "application/json");
            client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            request.client_identify.ipaddress = "10.0.0.1";
            request.client_identify.browser_agent = "Chrome";
            request.transaction_authInfo.payment_method = "tdd";
            request.transaction_authInfo.trx_type = "solaut";
            request.merchant_identify = identify;

            try
            {
                jsonreq = JsonConvert.SerializeObject(request);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string ruta = ConfigurationManager.AppSettings["endPointTDDAuth"];
                jsonres = client.UploadString(ruta, jsonreq);
                //jsonres = client.UploadString("https://apimbu.mercantilbanco.com:9443/mercantil-banco/desarrollo/v1/payment/getauth", jsonreq);
              
                response = JsonConvert.DeserializeObject<AuthResponse>(jsonres);
                response.authentication_info.twofactor_type = Decrypt(response.authentication_info.twofactor_type);
            }
            catch (Exception e)
            {
                throw new Exception(message: "Error al Procesar la Autenticación", innerException: e.InnerException);
            }

            return response;
        }

        private PayDebitCardResponse GetPay(string request)
        {
           
              string jsonreq = string.Empty;
            string jsonres = string.Empty;
            PayDebitCardResponse responseDebit = new PayDebitCardResponse();

            PayResponse response = new PayResponse();
            PayResponseError responseError = new PayResponseError();

            WebClient client = new WebClient();                        
                               
            client.Headers.Add("X-IBM-Client-Id", ConfigurationManager.AppSettings["ClientID"]);
            client.Headers.Add(HttpRequestHeader.Accept, "application/json");
            client.Headers.Add("Content-Type", "application/json");
            // client.Headers.Add("X-Global-Transaction-ID", "62bc66755de8f2b1288e54c3");
            //client.Headers.Add("Access-Control-Allow-Methods", "POST");

            string ruta = String.Empty;
            String respuesta = String.Empty;

            try
            {
                //ya viene serializado a json
              //  jsonreq = JsonConvert.SerializeObject(request);
                ServicePointManager.Expect100Continue = true;                
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                ruta = ConfigurationManager.AppSettings["endPointTDDPay"];
             //   jsonres = client.UploadString(ruta, request);

             
                 jsonres = client.UploadString(ruta, request);
                 response = JsonConvert.DeserializeObject<PayResponse>(jsonres);
                responseDebit.respuesta = true;
                //jsonres = client.UploadString("https://apimbu.mercantilbanco.com:9443/mercantil-banco/desarrollo/v1/payment/pay", request);
                // response = JsonConvert.DeserializeObject<PayResponse>(respuesta);
                // response = JsonConvert.DeserializeObject<PayResponse>(jsonres);
            }
            catch (WebException e)
            {

                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    var code = ((HttpWebResponse)e.Response).StatusCode;
                    var descripcion = ((HttpWebResponse)e.Response).StatusDescription;
                    using (StreamReader r = new StreamReader(((HttpWebResponse)e.Response).GetResponseStream()))
                    {
                        respuesta = r.ReadToEnd();
                    }
                }

                responseDebit.respuesta = false;
                //se llena el objeto para la clase response
                responseError = JsonConvert.DeserializeObject<PayResponseError>(respuesta);

            }
            catch (Exception e)
            {
                responseDebit.respuesta = false;
                throw new Exception("Error al Procesar el Pago", e.InnerException);
            }

           
            responseDebit.payResponse = response;
            responseDebit.payResponseError = responseError;

            return responseDebit;
        }

        /// <summary>
        /// Metodo de Encriptado para Mercantil
        /// </summary>
        /// <param name="Texto">Texto a Encriptar</param>
        /// <returns></returns>
        /// 
        private string Encripta(string Texto)
        {
            string secretkey = ConfigurationManager.AppSettings["Key"].ToString(); //Turing(ConfigurationManager.AppSettings["SecretKey"].ToString(), ConfigurationManager.AppSettings["Key"].ToString());
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = ComputeSha256Hash(secretkey);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.ECB;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(Texto);
                        }
                        array = memoryStream.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(array);

        }

        public string Decrypt(string datatodecrpyt)
        {
            string keyValue = ConfigurationManager.AppSettings["Key"].ToString();

            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(datatodecrpyt);

            using (Aes aes = Aes.Create())
            {
                aes.Key = ComputeSha256Hash(keyValue);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.ECB;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        public static long LongRandom(long min, long max, Random rand)
        {
            long result = rand.Next((Int32)(min >> 32), (Int32)(max >> 32));
            result = (result << 32);
            result = result | (long)rand.Next((Int32)min, (Int32)max);
            return result;
        }

        //private string Enigma(string Text)
        //{
        //    string keyValue = Turing(ConfigurationManager.AppSettings["SecretKey"].ToString(), ConfigurationManager.AppSettings["Key"].ToString());
        //    byte[] encrypted;
        //    byte[] Key = new byte[16];
        //    Key = SetKey(keyValue);
        //    UTF8Encoding UTF8 = new UTF8Encoding();
        //    using (AesManaged aes = new AesManaged())
        //    {
        //        aes.Key = Key;
        //        aes.Mode = CipherMode.ECB;
        //        aes.Padding = PaddingMode.PKCS7;
        //        ICryptoTransform encryptor = aes.CreateEncryptor();
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
        //            {
        //                using (StreamWriter sw = new StreamWriter(cs))
        //                    sw.Write(Text);
        //                encrypted = ms.ToArray();
        //            }
        //        }
        //    }
        //    string encString = Convert.ToBase64String(encrypted);
        //    return encString;
        //}

        /// <summary>
        /// Metrodo de Desencriptado para Mercantil
        /// </summary>
        /// <param name="Text">Texto a Desencriptar</param>
        /// <returns></returns>
        //private string Turing(string Text)
        //{
        //    string keyValue = ConfigurationManager.AppSettings["Key"].ToString();
        //    byte[] encrypted = Convert.FromBase64String(Text);
        //    //byte[] SecretKey = new byte[16];
        //    byte[] SecretKey = SetKey(keyValue);
        //    string plaintext = null;
        //    using (AesManaged aes = new AesManaged())
        //    {
        //        aes.Key = SecretKey;
        //        aes.Mode = CipherMode.ECB;
        //        aes.Padding = PaddingMode.PKCS7;
        //        ICryptoTransform decryptor = aes.CreateDecryptor();
        //        using (MemoryStream ms = new MemoryStream(encrypted))
        //        {
        //            using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
        //            {
        //                using (StreamReader reader = new StreamReader(cs))
        //                    plaintext = reader.ReadToEnd();
        //            }
        //        }
        //    }
        //    return plaintext;
        //}

        //private string Decripta(string Text, string Key)
        //{
        //    byte[] encrypted = Convert.FromBase64String(Text);
        //    byte[] SecretKey = new byte[16];
        //    SecretKey = SetKey(Key);
        //    string plaintext = null;
        //    using (AesManaged aes = new AesManaged())
        //    {
        //        aes.Key = SecretKey;
        //        aes.Mode = CipherMode.ECB;
        //        aes.Padding = PaddingMode.PKCS7;
        //        ICryptoTransform decryptor = aes.CreateDecryptor();
        //        using (MemoryStream ms = new MemoryStream(encrypted))
        //        {
        //            using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
        //            {
        //                using (StreamReader reader = new StreamReader(cs))
        //                    plaintext = reader.ReadToEnd();
        //            }
        //        }
        //    }
        //    return plaintext;
        //}


        private static byte[] ComputeSha256Hash(string rawData)
        {

            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] key = new byte[16];
                byte[] hash = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                Array.Copy(hash, key, 16);
                return key;
            }
        }

        //private byte[] SetKey(string Key)
        //{
        //    byte[] SecretKey = new byte[16];
        //    byte[] bytes = Encoding.Default.GetBytes(Key);
        //    Key = Encoding.UTF8.GetString(bytes);

        //    byte[] result;
        //    SHA1 shaM = new SHA1Managed();
        //    result = shaM.ComputeHash(bytes);

        //    Array.Copy(result, SecretKey, 16);

        //    return SecretKey;
        //}

        #endregion
    }
}