﻿using System.Collections.Generic;
using System.ServiceModel;
using Services.Vista.BO;

namespace Services.Vista.BL.Contract
{

    [ServiceContract]
    public interface IConcessions
    {
        [OperationContract]
        [FaultContract(typeof(GenericException))]
        ConcessionListResponse GetTheaterConcessions(string theaterId);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        ConcessionOrderResponse AddItemsToCart(string userSessionId, string cinemaId, string sessionId, string transactionIdTemp, ConcessionItem[] concessions);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        bool PurchaseConcessions(string transactionIdTemp, string transactionNumber, string bookingNumber,
            string userSessionId, string cinemaId, string sessionId, ConcessionItem[] concessions);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        bool UpdateConcessionsOrder(string theaterId, string transactionIdTemp, string bookingNumber,
            string userSessionId, bool paymentStart, bool paymentOk, bool orderComplete, bool orderCancel);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        List<ConcessionItem> GetProductRecipe(string itemId, string cinemaId, string locationStrCode);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        //bool CancelConcessionsOrder(string userSessionId, string cinemaId, string transactionId);
        bool CancelConcessionsOrder(string userSessionId, string cinemaId, string transactionId, string TransactionNumber);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        ConcessionBookingFee GetBookingFee(string CinemaId);

        //[OperationContract]
        //[FaultContract(typeof(GenericException))]
        //string PromoPepsi(string Codigo);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        ConcessionListResponse GetCandiesWeb(string theaterId);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        ConcessionOrderResponse AddItemsToCartCandiesWeb(string userSessionId, string cinemaId, string sessionId, string transactionIdTemp, ConcessionItem[] concessions);
        //[OperationContract]
        //[FaultContract(typeof(GenericException))]
        //string GetPercentageWeb();

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        PromoPack PromoDesorden(string TransactionId, string CinemaId);

    }
}