﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.BO;
using System.ServiceModel;

namespace Services.Vista.BL.Contract
{     
    [ServiceContract]
    public interface IPremium
    {
        [OperationContract]
        MapInformation GetNewOrder(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Boolean WithMap);
        //[OperationContract]
        //Map GetNewOrderVoucher(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, List<string> Vouchers);

        //Luis Ramírez 16/05/2017 Old Contract method GetNewOrderSeatData() commented     
        //[OperationContract]
        //string GetNewOrderSeatData(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Boolean WithMap);
        //[OperationContract]
        //SeatMapResponse GetNewOrderSeatData(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Boolean WithMap);

        [OperationContract]
        Boolean ChangeSeats(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.Seat> Seats);
        [OperationContract]
        MapInformation GetMap(string ClientID, string UserSessionID, int SessionID, string TheaterID, Boolean WithMap);
        [OperationContract]
        string GetMapMobile(string ClientID, string UserSessionID, int SessionID, string TheaterID);

        //[OperationContract]
        //Boolean CancelOrder(string ClientID, string UserSessionID);
        //[OperationContract]
        //string GetMapSeatsAvailable(string ClientID, string UserSessionID, int SessionID, string TheaterID);
        [OperationContract]
        Boolean CancelOrderRest(string UserSessionID);
    }
}
