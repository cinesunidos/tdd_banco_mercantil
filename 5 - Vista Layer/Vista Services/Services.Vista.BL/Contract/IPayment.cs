﻿using System;
using Services.Vista.BO;
using System.ServiceModel;

namespace Services.Vista.BL.Contract
{
    [ServiceContract]
    public interface IPayment
    {
        [OperationContract]
        [FaultContract(typeof(PaymentException))]
        PaymentResponse GetPaymentResponseByPaymentType(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo);

        [OperationContract]
        [FaultContract(typeof(PaymentException))]
        PaymentResponse GetAuthMercantil(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo);

        [OperationContract]
        [FaultContract(typeof(PaymentException))]
        AVippoResponse GetVippoConfirmation(APaymentRequest VippoPayment);

        [OperationContract]
        Boolean RestRefundBooking(string TheaterID, int BookingNumber, int ValueInCents);

        [OperationContract]
        [FaultContract(typeof(PaymentException))]
        AuthResponse GetAuth(AuthRequest Request);

        [OperationContract]
        [FaultContract(typeof(PaymentException))]
        string BDVGeneratePayment(BDVUserInfo id);

        #region Metodos No Utilizados
        //[OperationContract]
        //[FaultContract(typeof(PaymentException))]
        //PaymentResponse GetPaymentPremiumUltimate(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo);

        //[OperationContract]
        //[FaultContract(typeof(PaymentException))]
        //PaymentResponse GetPaymentPremium(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo);

        //[OperationContract]
        //PaymentResponse GetPayment(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Vista.BO.PaymentInfo PaymentInfo); 

        //[OperationContract]
        //Boolean RefundBooking(string TheaterID, int BookingNumber, int ValueInCents);
        #endregion

    }
}
