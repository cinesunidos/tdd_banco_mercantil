﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.BO;
using System.ServiceModel;

namespace Services.Vista.BL.Contract
{
    [ServiceContract]
    public interface ILive
    {
        [OperationContract]      
        [FaultContract(typeof(GenericException))]
        SessionInformation GetSessionInformation(string TheaterID, int SessionID);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        SIReduced GetSeatsAvailables(string TheaterID, int SessionID);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        Voucher GetTicketTypeBarcode(string Barcode, string CinemaID, string SessionID);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        GiftCard GetGiftCardBalance(GiftCard Giftcard, string CinemaId, string SessionId);

        [OperationContract]
        InfoVoucher GetTicketTypesforBarcode(string cinemaid, string sessionid, string barcode);
    }
}