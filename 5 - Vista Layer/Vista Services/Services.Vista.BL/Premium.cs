﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.BO;
using Services.Vista.DAL.DO;
using Services.Vista.DAL.DORest;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace Services.Vista.BL
{
    public class Premium : Contract.IPremium
    {
        static string seatData = string.Empty;
        public MapInformation GetNewOrder(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Boolean WithMap)
        {
            string BasePath = @"C:\Desarrollo\Logs\Premium";
            string FileName = "Payment_" + UserSessionID + ".txt";
            string LogPath = Path.Combine(BasePath, FileName);
            if (!Directory.Exists(BasePath))
                Directory.CreateDirectory(BasePath);
            StreamWriter w = new StreamWriter(LogPath);
            List<DAL.DO.TicketType> tts = new List<DAL.DO.TicketType>();
            Live Live = new Live();
            w.WriteLine("L24| se busca la informacion de la session con los parametros  |TheaterID: {0} |SessionID: {1} |", TheaterID, SessionID);


            SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);

            var SessionInformationString = JsonConvert.SerializeObject(SessionInformation);
            w.WriteLine("L31| Respuesta de la Busqueda  | SessionInformationString: {0} | |", SessionInformationString);

            foreach (BO.TicketType item in TicketTypes)
            {
                BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();

                DAL.DO.TicketType tt = new DAL.DO.TicketType();
                tt.PriceInCents = (Int32)(Ticket.Price * 100);
                tt.Qty = item.Quantity;
                if (item.TicketBarcode != null)
                {
                    tt.OptionalBarcode = item.TicketBarcode;
                }
                tt.TicketTypeCode = item.TicketTypeCode;
                tts.Add(tt);
            }

            var ttsstring = JsonConvert.SerializeObject(tts);
            //AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserS  essionID, TheaterID, SessionID.ToString(), tts.ToArray());
            w.WriteLine("L50| Se llama el Servicio de add Ticket rest | ClientID: {0} | UserSessionID: {1} | TheaterID: {2} | SessionID: {3} | ttsstring: {4} |", ClientID, UserSessionID, TheaterID, SessionID.ToString(), ttsstring);
            RestAddTicketsResponse rp = RestTicketingService.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());
            var rpstring = JsonConvert.SerializeObject(rp);
            w.WriteLine("L53| finaliza la llamada del Servicio de add Ticket rest | respuesta del servicio {0}  |  |  |  |  |", rpstring);

            if (rp.Result != (int)ResultCode.OK) { return new MapInformation(true, rp.Result.ToString()); }
            seatData = rp.SeatData;
            List<BO.Ticket> tickets = new List<BO.Ticket>();
            decimal bookinFeeConIva = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["BookingFee"].ToString());
            //Esta linea devuelve el iva del cine 16 o 0 para los cines excentos
            decimal Iva = decimal.Parse(getValueFromListString("Tax", rp.Order.CinemaId));
            decimal factorIVA = 1 + (Iva / 100);
            foreach (var i in rp.Order.Sessions[0].Tickets)
            {
                var descripcion = i.Description;
                var montoBoletosConIva = i.PackageTickets != null ? i.PackageTickets.Sum(p => p.FinalPriceCents) / 100 : i.FinalPriceCents / 100;
                var MontoServiciosConIva = i.PackageConcessions != null ? i.PackageConcessions.Where(p => p.ItemId == "999999987").Sum(p1 => p1.FinalPriceCents) / 100 : 0;
                var MontoPrompcionesConIva = i.PackageConcessions != null ? i.PackageConcessions.Where(p => p.ItemId != "999999987").Sum(p1 => p1.FinalPriceCents) / 100 : 0;
                //var total = montoBoletosConIva + MontoServiciosConIva + MontoPrompcionesConIva;
                BO.Ticket t = new BO.Ticket
                {
                    BasePrice = montoBoletosConIva / factorIVA,
                    BookingFee = bookinFeeConIva / factorIVA,
                    BookingTax = bookinFeeConIva - (bookinFeeConIva / factorIVA),
                    Code = i.TicketTypeCode,
                    FullPrice = i.FinalPriceCents / 100 + bookinFeeConIva,
                    Price = i.FinalPriceCents / 100,
                    Name = i.Description,
                    ServiceFee = MontoServiciosConIva / factorIVA,
                    ServiceTax = MontoServiciosConIva - (MontoServiciosConIva / factorIVA),
                    PromotionFee = MontoPrompcionesConIva / factorIVA,
                    PromotionTax = MontoPrompcionesConIva - (MontoPrompcionesConIva / factorIVA),
                    Quantity = 1,
                    TicketTax = montoBoletosConIva - (montoBoletosConIva / factorIVA),
                    RedemptionOnly = false,
                    Tax = (bookinFeeConIva - (bookinFeeConIva / factorIVA)) + (MontoServiciosConIva - (MontoServiciosConIva / factorIVA)) + (MontoPrompcionesConIva - (MontoPrompcionesConIva / factorIVA)) + (montoBoletosConIva - (montoBoletosConIva / factorIVA)),
                    TotalPromotionFee = MontoPrompcionesConIva,
                    TotalServiceFee = MontoServiciosConIva
                };
                tickets.Add(t);
            }

            var ticketsstring = JsonConvert.SerializeObject(tickets);
            w.WriteLine("l93| se llena la lista de tickets | Listado de Tickets {0}  |  |  |  |  |", ticketsstring);

            var lista = tickets.GroupBy(t => t.Code).ToList();
            tickets = new List<BO.Ticket>();
            //ya que el servicio me devuelve uno a uno los tickets
            //los agrupo por tipo y seteo la cantidad que haya de cada uno.
            foreach (var item in lista)
            {
                var t1 = item.First();
                t1.Quantity = item.Count();
                tickets.Add(t1);
            }
            w.Dispose();
            w.Close();
            return MapInformation.GetTest(rp.SeatData, WithMap, tickets);
        }

        private string getValueFromListString(String key, String code)
        {
            List<String> items = System.Configuration.ConfigurationManager.AppSettings[key].Split(';').ToList();
            String value = "";
            value = items.Where(e => e.Contains(code)).SingleOrDefault();
            if (value != string.Empty) value = value.Split(':')[1];
            return value;
        }

        #region Metodos no usados
        //public string GetMapSeatsAvailable(string ClientID, string UserSessionID, int SessionID, string TheaterID)
        //{
        //    Services.Vista.DAL.TicketingService.GetSessionSeatDataResponse rp = GetSessionSeatDataResponse.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);

        //    Services.Vista.DAL.TicketingService.Theatre theatre = rp.SeatLayoutData;
        //    Map map = new Map(theatre.Areas[0].Rows.Count(), theatre.Areas[0].ColumnCount);
        //    map.Tiers = map.Tiers.OrderByDescending(ma => ma.Name).ToList();

        //    string MapSeatsAvailable =  Map.GetMapSeatsAvailable(rp.SeatData);            
        //    return MapSeatsAvailable;           
        //}

        //public SeatMapResponse GetNewOrderSeatData(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Boolean WithMap)
        //{

        //    List<DAL.DO.TicketType> tts = new List<DAL.DO.TicketType>();
        //    Live Live = new Live();

        //    //Stopwatch watch = new Stopwatch();
        //    //watch.Start();
        //    SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);
        //    //watch.Stop();

        //    SeatMapResponse response = new SeatMapResponse();
        //    List<BO.Ticket> TicketList = new List<BO.Ticket>();

        //    foreach (Services.Vista.BO.TicketType item in TicketTypes)
        //    {
        //        BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();
        //        Ticket.Quantity = item.Quantity;

        //        DAL.DO.TicketType tt = new DAL.DO.TicketType();
        //        tt.PriceInCents = (Int32)(Ticket.Price * 100);
        //        tt.Qty = item.Quantity;

        //        //item.TicketBarcode = null; //Quitar linea cuando se consume desde capa 2

        //        if (item.TicketBarcode != null)
        //        {
        //            tt.OptionalBarcode = item.TicketBarcode;
        //        }

        //        tt.TicketTypeCode = item.TicketTypeCode;
        //        tts.Add(tt);
        //        TicketList.Add(Ticket);
        //    }
        //    AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());
        //    response.Tickets = TicketList.ToArray();
        //    response.SeatData = rp.SeatData.ToString();

        //    return response;

        //}

        //public Map GetNewOrderVoucher(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, List<string> Vouchers)
        //{
        //    List<Services.Vista.DAL.TicketingService.TicketType> tts = new List<DAL.TicketingService.TicketType>();
        //    Live Live = new Live();
        //    Services.Vista.BO.SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);

        //    foreach (Services.Vista.BO.TicketType item in TicketTypes)
        //    {
        //        BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();

        //        Services.Vista.DAL.TicketingService.TicketType tt = new DAL.TicketingService.TicketType();
        //        tt.PriceInCents = (Int32)(Ticket.Price * 100);
        //        tt.Qty = item.Quantity;
        //        tt.TicketTypeCode = item.TicketTypeCode;
        //        tts.Add(tt);
        //    }

        //    foreach (string item in Vouchers)
        //    {
        ////        Services.Vista.DAL.TicketingService.TicketType tt = new DAL.TicketingService.TicketType();
        ////        List<TicketTypeFromBarcode> TicketTypeFromBarcodes = TicketTypeFromBarcode.GetTicketTypeFromBarcode(item.Substring(0,5),TheaterID,SessionID);

        //       // tt.OptionalBarcodePin = item.Substring(0, 5);
        //        tt.OptionalBarcode = item;
        //        tt.TicketTypeCode = TicketTypeFromBarcodes[0].TType_strCode;
        //        tt.PriceInCents = TicketTypeFromBarcodes[0].Price_intSurcharge;
        //        tt.Qty = 1;
        //        tts.Add(tt);
        //    }
        //    AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());
        //    return Map.Get(rp.SeatData);
        //} 
        #endregion
        public Boolean ChangeSeats(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.Seat> Seats)
        {
            List<Services.Vista.DAL.DO.SelectedSeat> SelectedSeats = new List<DAL.DO.SelectedSeat>();
            foreach (Services.Vista.BO.Seat item in Seats)
            {
                Services.Vista.DAL.DO.SelectedSeat selectedSeat = new DAL.DO.SelectedSeat();
                selectedSeat.AreaCategoryCode = item.AreaCategoryCode;
                selectedSeat.AreaNumber = item.AreaNumber;
                selectedSeat.ColumnIndex = item.ColumnIndex;
                selectedSeat.RowIndex = item.RowIndex;

                SelectedSeats.Add(selectedSeat);
            }

            //Services.Vista.DAL.DO.SetSelectedSeatsResponse rp = SetSelectedSeatsResponse.SetSelectedSeats(ClientID, UserSessionID, TheaterID, SessionID.ToString(), SelectedSeats.ToArray());
            Services.Vista.DAL.DO.SetSelectedSeatsResponse rp = RestTicketingService.SetSelectedSeats(ClientID, UserSessionID, TheaterID, SessionID.ToString(), SelectedSeats.ToArray());

            if (rp.Result == ResultCode.OK)
                return true;
            else return false;
        }
        public MapInformation GetMap(string ClientID, string UserSessionID, int SessionID, string TheaterID, Boolean WithMap)
        {
            //Services.Vista.DAL.DO.GetSessionSeatDataResponse rp = GetSessionSeatDataResponse.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);
            Services.Vista.DAL.DO.GetSessionSeatDataResponse rp = RestTicketingService.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);


            if (rp.Result != ResultCode.OK) { return new MapInformation(true, rp.Result.ToString()); }
            rp.SeatData = seatData;
            return MapInformation.GetTest(rp.SeatData, WithMap);
        }

        public string GetMapMobile(string ClientID, string UserSessionID, int SessionID, string TheaterID)
        {
            //Services.Vista.DAL.DO.GetSessionSeatDataResponse rp = GetSessionSeatDataResponse.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);
            Services.Vista.DAL.DO.GetSessionSeatDataResponse rp = RestTicketingService.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);
            rp.SeatData = seatData;
            return rp.SeatData.ToString();
        }


        public Boolean CancelOrder(string ClientID, string UserSessionID)
        {
            //return Services.Vista.DAL.DO.CancelOrder.GetCancelOrder(ClientID, UserSessionID);
            return RestTicketingService.RestGetCancelOrder(UserSessionID);
        }

        public Boolean CancelOrderRest(string UserSessionID)
        {
            return RestTicketingService.RestGetCancelOrder(UserSessionID);
        }
    }
}
