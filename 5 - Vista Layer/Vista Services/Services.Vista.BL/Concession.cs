﻿
using Services.Vista.BL.Contract;
using System.Collections.Generic;
using Services.Vista.BO;
using Services.Vista.DAL.DO;
using System;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;

namespace Services.Vista.BL
{
    public class Concession : IConcessions
    {
        public ConcessionListResponse GetTheaterConcessions(string theaterId)
        {
            return Concessions.GetTheaterConcessions(theaterId);
        }

        public ConcessionOrderResponse AddItemsToCart(string userSessionId, string theaterId, string sessionId, string transactionIdTemp, ConcessionItem[] concessions)
        {
            return Concessions.AddItemsToCart(userSessionId, theaterId, sessionId, transactionIdTemp, concessions);
        }

        public bool PurchaseConcessions(string transactionIdTemp, string transactionNumber, string bookingNumber,
            string userSessionId, string cinemaId, string sessionId, ConcessionItem[] concessions)
        {
            return Concessions.PurchaseConcessions(
                transactionIdTemp,
                transactionNumber,
                bookingNumber,
                userSessionId,
                cinemaId,
                sessionId,
                concessions
            );
        }

        public bool UpdateConcessionsOrder(string theaterId, string transactionIdTemp, string transactionNumber, string bookingNumber,
            bool paymentStart, bool paymentOk, bool orderComplete, bool orderCancel)
        {
            return Concessions.UpdateConcessionsOrder(
                theaterId,
                transactionIdTemp,
                transactionNumber,
                bookingNumber,
                paymentStart,
                paymentOk,
                orderComplete,
                orderCancel
            );
        }

        public List<ConcessionItem> GetProductRecipe(string theaterId, string itemId, string locationStrCode)
        {
            return Concessions.GetProductRecipe(theaterId, itemId, locationStrCode);
        }

        //public bool CancelConcessionsOrder(string userSessionId, string cinemaId, string transactionId)
        //{
        //    return Concessions.CancelConcessionsOrder(userSessionId, cinemaId, transactionId);
        //}
        public bool CancelConcessionsOrder(string userSessionId, string cinemaId, string transactionId, string TransactionNumber)
        {
            return Concessions.CancelConcessionsOrder(userSessionId, cinemaId, transactionId, TransactionNumber);
        }


        public ConcessionBookingFee GetBookingFee(string CinemaId)
        {
            ConcessionBookingFee result = new ConcessionBookingFee();
            result.BookingFeeConcessions = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["WebBookingPrice"].ToString());
            result.BookingTaxBookingFeeConcessions = decimal.Parse(getValueFromListString("Tax", CinemaId));

            return result;
        }

        public ConcessionListResponse GetCandiesWeb(string theaterId)
        {
            return Concessions.GetCandiesWeb(theaterId);
        }

        public ConcessionOrderResponse AddItemsToCartCandiesWeb(string userSessionId, string theaterId, string sessionId, string transactionIdTemp, ConcessionItem[] concessions)
        {
            return Concessions.AddItemsToCartCandiesWeb(userSessionId, theaterId, sessionId, transactionIdTemp, concessions);
        }

        public PromoPack PromoDesorden(string TransactionId, string CinemaId)
        {
            return Concessions.PromoDesorden(TransactionId, CinemaId);
        }

        #region Private method
        private string getValueFromListString(String key, String code)
        {
            List<String> items = System.Configuration.ConfigurationManager.AppSettings[key].Split(';').ToList();
            String value = "";
            value = items.Where(e => e.Contains(code)).SingleOrDefault();
            if (value != string.Empty) value = value.Split(':')[1];
            return value;
        }

        #endregion Private method
    }
}
