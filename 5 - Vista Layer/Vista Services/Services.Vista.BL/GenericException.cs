﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BL
{
    public class GenericException
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }        
      
    }
    public class PaymentException
    {       
        public List<string> Voucher { get; set; }
        public string CodeResult { get; set; }
        public string DescriptionResult { get; set; }
   
    }
    public class ControlledException:Exception
    {
        public ControlledException() { }
        public ControlledException(string Code)
        {
            this.Code = Code;
        }
        public ControlledException(string Code, string Message)
        {
            this.Code = Code;
            this.Message = Message;
        }
        
        public string Code { get; set; }
        public string Message { get; set; }
    
    }
}
