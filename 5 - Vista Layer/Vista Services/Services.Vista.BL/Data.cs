﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.BO;
using Services.Vista.DAL.DO;
using System.ServiceModel;
using System.Configuration;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;

namespace Services.Vista.BL
{
    // [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Data : Contract.IData
    {
        public List<Theater> GetTheaters(string CompanyID)
        {
            try
            {
                List<VISTAITCinema> VISTAITCinemas = VISTAITCinema.getInfoTheaters(CompanyID);
                List<Facility> Facilitys = Facility.getInfoFacilities();

                List<Theater> Theaters = new List<Theater>();

                foreach (VISTAITCinema item in VISTAITCinemas)
                {
                    Theater theater = new Theater();
                    theater.Address = item.Address;
                    theater.City = item.City;
                    theater.DisplayName = item.DisplayName;
                    theater.FullName = item.FullName;
                    theater.ID = item.IDTheather.ToString();
                    theater.Latitude = item.Latitude;
                    theater.Longitude = item.Longitude;
                    theater.Phone = item.Phone;
                    List<Facility> facilitys = Facilitys.Where(f => f.IDTheather == item.IDTheather).ToList();

                    List<Facilities> listfacilities = new List<Facilities>();
                    foreach (Facility fac in facilitys)
                    {
                        Facilities f = new Facilities();
                        f.ID = fac.IDFacility;
                        f.IDTheater = fac.IDTheather;
                        f.Name = fac.FacilityName;
                        listfacilities.Add(f);
                    }
                    theater.P_Facilities = listfacilities;
                    listfacilities = null;
                    Theaters.Add(theater);
                }
                return Theaters;
            }
            catch (Exception ex)
            {
                GenericException exp = new GenericException();
                exp.Code = "E0001";
                exp.Message = Services.Vista.BL.Message.ResourceManager.GetString(exp.Code);
                exp.Title = Services.Vista.BL.Title.ResourceManager.GetString(exp.Code);
                throw new FaultException<GenericException>(exp, new FaultReason(string.Format("{0}-{1}", exp.Code, exp.Message)));
            }
        }

        /// <summary>
        /// Obtiene todas las peliculas por cine en una sola consulta con el proposito de ser cacheadas 
        /// en la capa desde donde se llama, las consultas se hacen por cine directamente a la base de datos.
        /// Retorna un objeto tipo lista con todas las peliculas por cine.
        /// </summary>
        /// <returns></returns>
        public MoviePack[] GetAllMoviesCache()
        {
            List<MoviePack> movies = new List<MoviePack>();
            try
            {
                List<VISTAITCinema> cinema = VISTAITCinema.GetAllCinemas();
                foreach (var item in cinema)
                {
                    MoviePack mov = GetAllMovies(item.IDTheather.ToString());

                    movies.Add(mov);
                }

            }
            catch (Exception ex)
            {
                var mensaje = ex.Message;
            }
            return movies.ToArray();
        }

        public MoviePack GetAllMovies(string TheaterID)
        {
            try
            {
                // se hacen cuatro llamados innecesarios a base de datos.
                #region DataBaseCalled            
                List<VISTAITFilm> VistaITFilms = VISTAITFilm.GetFilmList(TheaterID);
                List<VISTAITSession> VistaITSessions = VISTAITSession.GetSessionList(TheaterID);
                //List<VISTAHOComingSoon> VistaHOComingSoons = VISTAHOComingSoon.GetComingSoonList();
                List<string> MovieIDs = VistaITFilms.Select(f => f.ID).ToList();
                List<VISTAHOFilm> VistaHOFilms = VISTAHOFilm.GetFilmList(MovieIDs);

                List<string> MovieIDsAll = MovieIDs;
                //MovieIDsAll.AddRange(VistaHOComingSoons.Select(f => f.MovieID).ToList());

                List<VISTAITFilmPerson> VistaITFilmPersons = VISTAITFilmPerson.GetPersonList(TheaterID, MovieIDsAll);

                #endregion
                #region Movies
                List<Movie> Movies = new List<Movie>();
                foreach (VISTAITFilm item in VistaITFilms)
                {
                    VISTAHOFilm VistaHOFilm = VistaHOFilms.Where(f => f.MovieID == item.ID).SingleOrDefault();
                    List<VISTAITSession> Sessions = VistaITSessions.Where(s => s.MovieID == item.ID).ToList();
                    List<VISTAITFilmPerson> MoviePeoples = VistaITFilmPersons.Where(p => p.MovieID == item.ID).ToList();

                    Movie movie = new Movie();
                    movie.ID = item.ID;
                    movie.OriginalName = item.OriginalName;
                    movie.SpanishName = item.SpanishName;
                    movie.Censor = item.Censor;
                    movie.Duration = Int32.Parse(item.Duration);
                    movie.OfficialSite = item.OfficialSite;
                    movie.Trailer = item.Trailer;
                    movie.FirstExhibit = item.FirstExhibit;
                    //movie.FirstExhibit = DateTime.Parse(item.FirstExhibit);
                    movie.Synopsis = item.Synopsis;
                    movie.Gender = item.Gender;
                    movie.Format = VistaHOFilm.Format;
                    movie.Subtitle = VistaHOFilm.Subtitle;
                    movie.OriginCountry = VistaHOFilm.OriginCountry;
                    movie.NacionalFilm = VistaHOFilm.NacionalFilm;


                    List<BO.Session> sessions = new List<BO.Session>();
                    foreach (VISTAITSession ses in Sessions)
                    {
                        BO.Session session = new BO.Session();
                        session.ID = Int32.Parse(ses.SessionID);
                        session.PriceGroup = ses.GroupCode;
                        session.SeatAllocated = ses.SeatAllocation == "Y" ? true : false;
                        session.SeatsAvailable = Int32.Parse(ses.SeatsAvailable);
                        session.ShowTime = ses.ShowTime;
                        session.HallName = ses.HallName;
                        session.HallNumber = ses.HallNumber;
                        session.OnSale = ses.Channels.Contains(";WWW;");
                        session.Censor = item.Censor;

                        sessions.Add(session);
                    }
                    movie.Sessions = sessions;

                    List<MoviePeople> Peoples = new List<MoviePeople>();
                    foreach (VISTAITFilmPerson per in MoviePeoples)
                    {
                        MoviePeople people = new MoviePeople();
                        people.Person_strFirstName = per.FirstName;
                        people.Person_strLastName = per.LastName;
                        people.FPerson_strType = per.PersonType;
                        Peoples.Add(people);
                    }
                    movie.Actor = MoviePeople.GetActors(Peoples);
                    movie.Director = MoviePeople.GetDirectors(Peoples);

                    Movies.Add(movie);
                }
                #endregion              

                MoviePack MoviePack = new MoviePack();
                MoviePack.Movies = Movies;
                MoviePack.TheaterID = TheaterID;
                return MoviePack;
            }
            catch (Exception ex)
            {
                GenericException exp = new GenericException();
                exp.Code = "E0001";
                exp.Message = Services.Vista.BL.Message.ResourceManager.GetString(exp.Code);
                exp.Title = Services.Vista.BL.Title.ResourceManager.GetString(exp.Code);
                throw new FaultException<GenericException>(exp, new FaultReason(string.Format("{0}-{1}", exp.Code, exp.Message)));
            }
        }

        public MoviePack GetComingSoon()
        {
            List<VISTAHOComingSoon> VistaHOComingSoons = VISTAHOComingSoon.GetComingSoonList();
            List<string> MovieIDs = VistaHOComingSoons.Select(f => f.MovieID).ToList();
            List<VISTAITFilmPerson> VistaITFilmPersons = VISTAITFilmPerson.GetPersonList("", MovieIDs);

            #region CommingSoon
            List<Movie> CommingSoons = new List<Movie>();
            foreach (VISTAHOComingSoon item in VistaHOComingSoons)
            {
                List<VISTAITFilmPerson> MoviePeoples = VistaITFilmPersons.Where(p => p.MovieID == item.MovieID).ToList();

                Movie movie = new Movie();
                movie.ID = item.MovieID;
                movie.OriginalName = item.OriginalName;
                movie.SpanishName = item.SpanishName;
                movie.Censor = item.Censor;
                movie.Duration = Int32.Parse(item.Duration);
                movie.OfficialSite = item.OfficialSite;
                movie.Trailer = item.Trailer;
                movie.FirstExhibit = DateTime.Parse(item.FirstExhibit);
                movie.Synopsis = item.Synopsis;
                movie.Gender = item.Gender;
                movie.Format = item.Format;
                movie.Subtitle = item.Subtitle;
                movie.OriginCountry = item.OriginCountry;
                movie.NacionalFilm = item.NacionalFilm;


                List<MoviePeople> Peoples = new List<MoviePeople>();
                foreach (VISTAITFilmPerson per in MoviePeoples)
                {
                    MoviePeople people = new MoviePeople();
                    people.Person_strFirstName = per.FirstName;
                    people.Person_strLastName = per.LastName;
                    people.FPerson_strType = per.PersonType;
                    Peoples.Add(people);
                }
                movie.Actor = MoviePeople.GetActors(Peoples);
                movie.Director = MoviePeople.GetDirectors(Peoples);

                CommingSoons.Add(movie);
            }
            #endregion
            MoviePack MoviePack = new MoviePack();
            MoviePack.CoomingSoon = CommingSoons.ToList();
            return MoviePack;
        }

        // [AspNetCacheProfile("1min")]
        public List<CityTicketPrice> GetTicketPrice(string date)
        {
            List<CityTicketPrice> pack = new List<CityTicketPrice>();
            List<TicketPrice> tpResult = VISTAITTheaterTicketPrice.GetTicketPriceListAll((date));
            List<string> ciudades = (tpResult.Select(t => t.city).Distinct()).ToList();
            List<string> theaters = (tpResult.Select(t => t.theaterID).Distinct()).ToList();

            foreach (string ciudad in ciudades)
            {
                CityTicketPrice cityTicketPrice = new CityTicketPrice();

                List<TheaterTicketPrice> ListTheaterTp = new List<TheaterTicketPrice>();
                foreach (string theater in theaters)
                {
                    List<TicketPrice> tp = tpResult.Where(c => c.theaterID.Equals(theater) && c.city.Equals(ciudad)).ToList();
                    if (tp.Count() > 0)
                    {
                        TheaterTicketPrice theaterTp = new TheaterTicketPrice();
                        foreach (TicketPrice ttp in tp)
                        {
                            ttp.fee = Convert.ToDecimal((ConfigurationManager.AppSettings["BookingFee"]));
                            theaterTp.theaterID = theater;
                            theaterTp.listTicketPrice.Add(ttp);
                        }
                        cityTicketPrice.city = ciudad;
                        cityTicketPrice.theaterTicketPrice.Add(theaterTp);
                    }
                }
                pack.Add(cityTicketPrice);
            }
            return pack;
        }

        public DatosBoleto ConsultaDatosBoleto(DatosBoleto datosboleto)
        {
            DatosBoleto infoBoleto = new DatosBoleto();
            infoBoleto = VISTAITFilm.ConsultaBoletos(datosboleto);
            return infoBoleto;
        }

    }
}
