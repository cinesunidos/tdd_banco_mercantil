﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Movie
    {
        public Movie()
        {
            m_Sessions = new List<Session>();
        }
        #region Fields
        private string m_ID;
        private string m_OriginalName;
        private string m_SpanishName;
        private string m_Censor;
        private string m_Gender;
        private int m_Duration;
        private string m_OfficialSite;
        private string m_Trailer;
       // private Boolean m_IsUpComing;
        private DateTime m_FirstExhibit;
        private string m_Synopsis;
        private string m_Format;
        private string m_Actor;
        private string m_Director;
        private string m_Subtitle;
        private string m_OriginCountry;
        private string m_NacionalFilm;
        private List<Session> m_Sessions;

        //private string m_ShortTitle;        
        //private Boolean m_Is3D;
        //private Boolean m_IsForKids;
        //private string m_Awards;
        //private string m_Company;
        //private string m_Director;
        //private string m_Distributor;
        //private string m_Language;
        //private string m_Producer;
        //private string m_Starring;
        //private string m_Writer;    
    
        #endregion        
        #region Properties
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string OriginalName
        {
            get { return m_OriginalName; }
            set { m_OriginalName = value; }
        }
        public string SpanishName
        {
            get { return m_SpanishName; }
            set { m_SpanishName = value; }
        }
        public string Censor
        {
            get { return m_Censor; }
            set { m_Censor = value; }
        }
        public string Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }
        public int Duration
        {
            get { return m_Duration; }
            set { m_Duration = value; }
        }
        public string OfficialSite
        {
            get { return m_OfficialSite; }
            set { m_OfficialSite = value; }
        }
        public string Trailer
        {
            get { return m_Trailer; }
            set { m_Trailer = value; }
        }
        //public Boolean IsUpComing
        //{
        //    get { return m_IsUpComing; }
        //    set { m_IsUpComing = value; }
        //}
        public DateTime FirstExhibit
        {
            get { return m_FirstExhibit; }
            set { m_FirstExhibit = value; }
        }
        public string Synopsis
        {
            get { return m_Synopsis; }
            set { m_Synopsis = value; }
        }
        public string Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public List<Session> Sessions
        {
            get { return m_Sessions; }
            set { m_Sessions = value; }
        }
        public string Actor
        {
            get { return m_Actor; }
            set { m_Actor = value; }
        }
        public string Director
        {
            get { return m_Director; }
            set { m_Director = value; }
        }
        public string Subtitle
        {
            get { return m_Subtitle; }
            set { m_Subtitle = value; }
        }
        public string OriginCountry
        {
            get { return m_OriginCountry; }
            set { m_OriginCountry = value; }
        }
        public string NacionalFilm
        {
            get { return m_NacionalFilm; }
            set { m_NacionalFilm = value; }
        }
      
        //public string ShortTitle
        //{
        //    get { return m_ShortTitle; }
        //    set { m_ShortTitle = value; }
        //}       
        //public Boolean Is3D
        //{
        //    get { return m_Is3D; }
        //    set { m_Is3D = value; }
        //}
        //public Boolean IsForKids
        //{
        //    get { return m_IsForKids; }
        //    set { m_IsForKids = value; }
        //}
        //public string Awards
        //{
        //    get { return m_Awards; }
        //    set { m_Awards = value; }
        //}
        //public string Company
        //{
        //    get { return m_Company; }
        //    set { m_Company = value; }
        //}
        //public string Director
        //{
        //    get { return m_Director; }
        //    set { m_Director = value; }
        //}
        //public string Distributor
        //{
        //    get { return m_Distributor; }
        //    set { m_Distributor = value; }
        //}
        //public string Language
        //{
        //    get { return m_Language; }
        //    set { m_Language = value; }
        //}
        
        //public string Producer
        //{
        //    get { return m_Producer; }
        //    set { m_Producer = value; }
        //}
        
        //public string Starring
        //{
        //    get { return m_Starring; }
        //    set { m_Starring = value; }
        //}
        //public string Writer
        //{
        //    get { return m_Writer; }
        //    set { m_Writer = value; }
        //}        
        #endregion
        public override bool Equals(object obj)
        {
            if (obj is Movie)
            {
                if (obj.GetHashCode().Equals(this.GetHashCode()))
                    return true;
                else return false;
            }
            else return false;
        }
        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
    }
}
