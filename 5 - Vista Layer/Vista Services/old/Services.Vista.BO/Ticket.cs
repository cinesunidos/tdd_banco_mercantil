﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Ticket
    {
        #region Fields
        private string m_Code;
        private string m_DescriptionTicketCategory;
        private string m_Name;
        private decimal m_Price;
        private decimal m_BasePrice;
        private decimal m_FullPrice;
        private decimal m_BookingFee;
        private decimal m_Tax;
        private decimal m_ServiceFee;
        private decimal m_TotalServiceFee;
        //CU Promociones en boleto
        private decimal m_PromotionFee;
        private decimal m_TotalPromotionFee;
        private decimal m_PromotionTax { get; set; }
        //Amagi cambio para desglose de IVA
        private decimal m_TicketTax;
        private decimal m_BookingTax;
        private decimal m_ServiceTax;


        #endregion
        #region Properties
        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }
        public string DescriptionTicketCategory
        {
            get { return m_DescriptionTicketCategory; }
            set { m_DescriptionTicketCategory = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        
        /// <summary>
        /// Ticket Total Price (Precio del Ticket)
        /// Incluye IVA cuando aplica
        /// </summary>
        public decimal Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }

        /// <summary>
        /// Ticket Base Price (Precio base del Ticket)
        /// No Incluye el  IVA
        /// </summary>
        public decimal BasePrice
        {
            get { return m_BasePrice; }
            set { m_BasePrice = value; }
        }

        /// <summary>
        /// Monto Reserva (sin IVA)
        /// </summary>
        public decimal BookingFee
        {
            get { return m_BookingFee; }
            set { m_BookingFee = value; }
        }

        // IM: Faltaría TotalBookingFee (con IVA)

        /// <summary>
        /// Impuesto Total (incluye booking y service)
        /// </summary>
        public decimal Tax
        {
            get { return m_Tax; }
            set { m_Tax = value; }
        }

        /// <summary>
        /// Cargos por Servicios (sin IVA)
        /// </summary>
        public decimal ServiceFee
        {
            get { return m_ServiceFee; }
            set { m_ServiceFee = value; }
        }

        /// <summary>
        /// Cargos por Servicios con IVA incluido
        /// </summary>
        public decimal TotalServiceFee 
        { 
            get { return m_TotalServiceFee; }
            set { m_TotalServiceFee = value; }
        }
        public decimal PromotionFee
        {
            get { return m_PromotionFee; }
            set { m_PromotionFee = value; }
        }

        /// <summary>
        /// Cargos por Promocion con IVA incluido
        /// </summary>
        public decimal TotalPromotionFee
        {
            get { return m_TotalPromotionFee; }
            set { m_TotalPromotionFee = value; }
        }
        /// <summary>
        /// Monto total incluidos impuestos
        /// </summary>
        public decimal FullPrice
        {
            get { return m_FullPrice; }
            set { m_FullPrice = value; }
            //get { return m_BasePrice + m_BookingFee + m_Tax + m_ServiceFee; }
        }

        /// <summary>
        /// IVA sobre el precio del boleto (si el boleto no supera las 2 UT debe ser cero)
        /// </summary>
        public decimal TicketTax
        {
            get
            {
                return m_TicketTax;
            }

            set
            {
                m_TicketTax = value;
            }
        }

        /// <summary>
        /// IVA sobre el cargo por reservación (Cargo Web)
        /// </summary>
        public decimal BookingTax
        {
            get
            {
                return m_BookingTax;
            }

            set
            {
                m_BookingTax = value;
            }
        }

        /// <summary>
        /// IVA sobre el cargo por servicios.
        /// </summary>
        public decimal ServiceTax
        {
            get
            {
                return m_ServiceTax;
            }

            set
            {
                m_ServiceTax = value;
            }
        }
        public decimal PromotionTax
        {
            get
            {
                return m_PromotionTax;
            }

            set
            {
                m_PromotionTax = value;
            }
        }
        #endregion
    }
}
