﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Facilities
    {
        #region Fields
            private int iFacilities;
            private int iTheater;
            private string strNameFacilities;
        #endregion
        #region Properties
            public int IDTheater
            {
                get { return iTheater; }
                set { iTheater = value; }
            }
            public int ID
            {
                get { return iFacilities; }
                set { iFacilities = value; }
            }
            public string Name
            {
                get { return strNameFacilities; }
                set { strNameFacilities = value; }
            }       
        #endregion
    }
}
