﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Services.Vista.BO
{
    public class GiftCard
    {
        public string CardNumber { get; set; }

        public string CardExpiry { get ; set; }

        public string CardExpiryMonth { get; set; }

        public string CardExpiryYear { get; set; }

        public decimal Balance { get; set; }

        public bool IsOk { get; set; }

        public List<string> Messages { get; set; }
    }
}
