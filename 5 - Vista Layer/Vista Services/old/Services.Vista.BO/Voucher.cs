﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
   public class Voucher
    {
       public Voucher()
       {
           this.ttbarcode = new List<TicketTypeBarcode>();
       }
        public Boolean IsValid { get; set; }
        public string CodeRes { get; set; }
        public string CodeResDesc { get; set; }
        public List<TicketTypeBarcode> ttbarcode { get;set; }
    }
}
