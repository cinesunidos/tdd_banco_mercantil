﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Services.Vista.BO
{
    public class MapInformation
    {
        public MapInformation()
        {
            Seats = new List<string>();
            SeatsSelected = new List<string>();            
        }
        public MapInformation(Boolean withError, string errorMessage)
        {
            this.WithError = withError;
            this.ErrorMessage = errorMessage;
        }
        #region Atributes
        public Map Map { get; set; }
        public List<string> Seats { get; set; }
        public List<string> SeatsSelected { get; set; }       
        public Boolean AreAvailable { get; set; }
        public Boolean WithError { get; set; }
        public string ErrorMessage { get; set; }
        #endregion
        //public static MapInformation Get(string SeatData, Boolean WithMap)
        //{
        //    MapInformation MapInformation = new BO.MapInformation();           
        //    MapInformation.SeatsAvailable = new List<string>();
        //    MapInformation.SeatsSelected = new List<string>();

        //    if (WithMap)
        //    {
        //        string mapa = SeatData;
        //        string[] vect = mapa.Split('|');
        //        int height = Int32.Parse(vect[1]) - 2;
        //        int width = Int32.Parse(vect[2]) - 2;
        //        int areaNumber = Int32.Parse(vect[4]);
        //        string areaCategoryCode = vect[5];

        //        Map map = new Map(height, width);

        //        int i = 12;
        //        int a = 0;
        //        while (a < height)
        //        {
        //            char row = Char.Parse(vect[i + 1]);
        //            string asientosString = vect[i + 2];

        //            int b = 0;
        //            while (b < asientosString.Length)
        //            {
        //                string asientos = asientosString.Substring(b, 12);
        //                int c = Int32.Parse(asientos.Substring(3, 2).Trim()) - 1;
        //                int status = Int32.Parse(asientos.Substring(asientos.Length - 1, 1));

        //                Tier tier = new Tier();
        //                tier = map.Tiers.Where(t => t.Name.Equals(row)).SingleOrDefault();

        //                tier.Seats[c].ColumnIndex = c;
        //                tier.Seats[c].RowIndex = height - a - 1;
        //                tier.Seats[c].Type = SeatType.Chair;
        //                tier.Seats[c].AreaCategoryCode = areaCategoryCode;
        //                tier.Seats[c].AreaNumber = areaNumber;
                                                
        //                if (status == 5)
        //                    MapInformation.SeatsSelected.Add(tier.Seats[c].GetSeatName(tier.Seats[c].RowIndex));
                        
        //                if (status == 0)                        
        //                    MapInformation.SeatsAvailable.Add(tier.Seats[c].GetSeatName(tier.Seats[c].RowIndex));
                        
        //                b = b + 12;
        //            }
        //            i = i + 3;
        //            a++;
        //        }
        //        MapInformation.Map = map;
        //    }
        //    else 
        //    {
        //        string mapa = SeatData;
        //        string[] vect = mapa.Split('|');
        //        int height = Int32.Parse(vect[1]) - 2;
        //      //  int width = Int32.Parse(vect[2]) - 2;
        //      //  int areaNumber = Int32.Parse(vect[4]);
        //      //  string areaCategoryCode = vect[5];
                
        //        int i = 12;
        //        int a = 0;
        //        while (a < height)
        //        {
        //            //char row = Char.Parse(vect[i + 1]);
        //            string asientosString = vect[i + 2];

        //            int b = 0;
        //            while (b < asientosString.Length)
        //            {
        //                string asientos = asientosString.Substring(b, 12);
        //                int c = Int32.Parse(asientos.Substring(3, 2).Trim()) - 1;
        //                int status = Int32.Parse(asientos.Substring(asientos.Length - 1, 1));

        //                Seat seat = new Seat();

        //                seat.ColumnIndex = c;
        //                seat.RowIndex = height - a - 1;
                        
        //                if (status == 5)
        //                    MapInformation.SeatsSelected.Add(seat.GetSeatName(seat.RowIndex));                        
        //                if (status == 0)
        //                    MapInformation.SeatsAvailable.Add(seat.GetSeatName(seat.RowIndex));
                        
        //                b = b + 12;
        //            }

        //            i = i + 3;
        //            a++;
        //        } 
        //    }
        //    return MapInformation;
        //}
        public static MapInformation GetTest(string SeatData, Boolean WithMap)
        {
            MapInformation MapInformation = new BO.MapInformation();
            List<string> SeatsAvailable = new List<string>();
            List<string> SeatsBooked = new List<string>();

            if (WithMap)
            {
                string mapa = SeatData;
                string[] vect = mapa.Split('|');
                int height = Int32.Parse(vect[1]) - 2;
                int width = Int32.Parse(vect[2]) - 2;
                int areaNumber = Int32.Parse(vect[4]);
                string areaCategoryCode = vect[5];
                //List<Tier> Tiers = new List<Tier>();

                Map map = new Map();
                int Rows = height;
                int Columns = width;

                int k = 12;
                int a = 0;
                for (int i = 0; i < Rows; i++)
                {
                    List<Seat> sts = new List<Seat>();
                    char row = Char.Parse(vect[k + 1]);
                    string asientosString = vect[k + 2];

                    int b = 0;
                    for (int j = 0; j < Columns; j++)
                    {    
                        Seat seat = new Seat();
                        seat.RowIndex = Rows - i - 1;
                        seat.ColumnIndex = j;
                        seat.Type = SeatType.Aisle;
                        seat.Name = seat.GetSeatName(i);
                        seat.AreaCategoryCode = areaCategoryCode;
                        seat.AreaNumber = areaNumber;
                        
                        if (b < asientosString.Count())
                        {
                            string asientos = asientosString.Substring(b, 12);
                            int c = Int32.Parse(asientos.Substring(3, 2).Trim()) - 1;
                            int status = Int32.Parse(asientos.Substring(asientos.Length - 1, 1));
                            if (c == j)
                            {
                                //Tier tier = new Tier();
                                //tier = map.Tiers.Where(t => t.Name.Equals(row)).SingleOrDefault();
                                
                                //seat.ColumnIndex = c;
                                //seat.RowIndex = height - a - 1;
                                seat.Type = SeatType.Chair;
                                //seat.Name = seat.GetSeatName(i);
                                //seat.AreaCategoryCode = areaCategoryCode;
                                //seat.AreaNumber = areaNumber;
                                if (status == 1)
                                    SeatsBooked.Add(seat.Name);

                                if (status == 5)
                                    MapInformation.SeatsSelected.Add(seat.Name);

                                if (status == 0)
                                    SeatsAvailable.Add(seat.Name);

                                b = b + 12;
                            }
                        }
                        sts.Add(seat);
                    }
                    Tier tier2 = new Tier();
                    tier2.Name = Seat.GetASCII(i);
                    tier2.Seats = sts;
                    map.Tiers.Add(tier2);

                    k = k + 3;
                    a++;
                }
                MapInformation.Map = map;
                
            }
            else
            {
                string mapa = SeatData;
                string[] vect = mapa.Split('|');
                int height = Int32.Parse(vect[1]) - 2;
                //  int width = Int32.Parse(vect[2]) - 2;
                //  int areaNumber = Int32.Parse(vect[4]);
                //  string areaCategoryCode = vect[5];

                int i = 12;
                int a = 0;
                while (a < height)
                {
                    //char row = Char.Parse(vect[i + 1]);
                    string asientosString = vect[i + 2];

                    int b = 0;
                    while (b < asientosString.Length)
                    {
                        string asientos = asientosString.Substring(b, 12);
                        int c = Int32.Parse(asientos.Substring(3, 2).Trim()) - 1;
                        int status = Int32.Parse(asientos.Substring(asientos.Length - 1, 1));

                        Seat seat = new Seat();

                        seat.ColumnIndex = c;
                        seat.RowIndex = height - a - 1;

                        if (status == 1)
                            SeatsBooked.Add(seat.GetSeatName(a));
                        if (status == 5)
                            MapInformation.SeatsSelected.Add(seat.GetSeatName(a));
                        if (status == 0)
                            SeatsAvailable.Add(seat.GetSeatName(a));

                        b = b + 12;
                    }

                    i = i + 3;
                    a++;
                }
            }
            if (SeatsAvailable.Count() < SeatsBooked.Count())
            {
                MapInformation.Seats = SeatsAvailable;
                MapInformation.AreAvailable = true;
            }
            else
            {
                MapInformation.Seats = SeatsBooked;
                MapInformation.AreAvailable = false;
            }
            if (MapInformation.Map != null) MapInformation.Map.Tiers = MapInformation.Map.Tiers.OrderByDescending(t => t.Name).ToList();
            return MapInformation;
        }
    }
}
