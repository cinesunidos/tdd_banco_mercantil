﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Session
    {
        public Session()
        {
            //m_Theater = new Theater();
            //m_Movie = new Movie();
        }
        #region Fields
        private int m_ID;
        private DateTime m_ShowTime;
        private string m_PriceGroup;
        private Boolean m_SeatAllocated;
        private int m_SeatsAvailable;
        private int m_HallNumber;     
        private string m_HallName;
        private Boolean m_OnSale;
    //    private Movie m_Movie;
     //   private Theater m_Theater;        
        #endregion
        #region Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public DateTime ShowTime
        {
            get { return m_ShowTime; }
            set { m_ShowTime = value; }
        }
        public string PriceGroup
        {
            get { return m_PriceGroup; }
            set { m_PriceGroup = value; }
        }
        public Boolean SeatAllocated
        {
            get { return m_SeatAllocated; }
            set { m_SeatAllocated = value; }
        }
        public int SeatsAvailable
        {
            get { return m_SeatsAvailable; }
            set { m_SeatsAvailable = value; }
        }
        public int HallNumber
        {
            get { return m_HallNumber; }
            set { m_HallNumber = value; }
        }
        public string HallName
        {
            get { return m_HallName; }
            set { m_HallName = value; }
        }
        public Boolean OnSale
        {
            get { return m_OnSale; }
            set { m_OnSale = value; }
        }
        //public Movie Movie
        //{
        //    get { return m_Movie; }
        //    set { m_Movie = value; }
        //}
        //public Theater Theater
        //{
        //    get { return m_Theater; }
        //    set { m_Theater = value; }
        //}
        #endregion
    }
}
