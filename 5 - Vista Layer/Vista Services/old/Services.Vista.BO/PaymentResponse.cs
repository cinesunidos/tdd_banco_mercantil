﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Services.Vista.BO
{
    public class PaymentResponse
    {
        #region Fields
        private List<string> m_Voucher;
        private int m_BookingNumber;
        private string m_CodeResult;
        private string m_DescriptionResult;
        #endregion
        #region Properties
        public List<string> Voucher
        {
            get { return m_Voucher; }            
        }
        public string VoucherHTML
        {
            get
            {
                string VoucherHTML = "";
                foreach (string item in Voucher)
                {
                    VoucherHTML =  VoucherHTML + item;
                    VoucherHTML = VoucherHTML + "<br>";
                }
                return VoucherHTML; 
            }
        }
        public int BookingNumber
        {
            get { return m_BookingNumber; }
            set { m_BookingNumber = value; }
        }
        public string CodeResult
        {
            get { return m_CodeResult; }
            set { m_CodeResult = value; }
        }
        public string DescriptionResult
        {
            get { return m_DescriptionResult; }
            set { m_DescriptionResult = value; }
        }

        #endregion

        public void SetVoucher(string str)
        {
            List<string> Lineas = new List<string>();
            string[] vect = Regex.Split(str, "<linea>");
            foreach (string l in vect)
            {
                string linea = l.Replace("</linea>", "").Replace("<UT>","").Replace("</UT>","");
                linea = linea.Trim();
                Lineas.Add(linea);
            }
            m_Voucher = Lineas;
        }
    }
}
