﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class SessionInformation
    {
        public SessionInformation()
        {
            Statistics = new List<Statistic>();
        }
        #region Fields
        private int m_SeatsAvailable;
        private Boolean m_SeatAllocation;
        private string m_MovieName;
        private string m_MovieID;
        private string m_TheaterName;
        private string m_TheaterID;
        private DateTime m_ShowTime;
        private int m_MaxSeatsPerTransaction;
        private string m_HallName;
        private List<Ticket> m_Tickets;
        private List<Statistic> m_Statistics;
        private Boolean m_OnSale;

       
        #endregion
        #region Properties
        public int SeatsAvailable
        {
            get { return m_SeatsAvailable; }
            set { m_SeatsAvailable = value; }
        }
        public Boolean SeatAllocation
        {
            get { return m_SeatAllocation; }
            set { m_SeatAllocation = value; }
        }
        public string MovieName
        {
            get { return m_MovieName; }
            set { m_MovieName = value; }
        }
        public string MovieID
        {
            get { return m_MovieID; }
            set { m_MovieID = value; }
        }
        public string TheaterName
        {
            get { return m_TheaterName; }
            set { m_TheaterName = value; }
        }
        public string TheaterID
        {
            get { return m_TheaterID; }
            set { m_TheaterID = value; }
        }
        public DateTime ShowTime
        {
            get { return m_ShowTime; }
            set { m_ShowTime = value; }
        }
        public int MaxSeatsPerTransaction
        {
            get { return m_MaxSeatsPerTransaction; }
            set { m_MaxSeatsPerTransaction = value; }
        }
        public List<Ticket> Tickets
        {
            get { return m_Tickets; }
            set { m_Tickets = value; }
        }
        public string HallName
        {
            get { return m_HallName; }
            set { m_HallName = value; }
        }
        public List<Statistic> Statistics
        {
            get { return m_Statistics; }
            set { m_Statistics = value; }
        }
        public Boolean OnSale
        {
            get { return m_OnSale; }
            set { m_OnSale = value; }
        }
        #endregion
    }
}
