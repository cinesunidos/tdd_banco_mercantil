﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Tier
    {
        private List<Seat> m_Seats = new List<Seat>();
        private Char m_Name;

        public List<Seat> Seats
        {
            get { return m_Seats; }
            set { m_Seats = value; }
        }
        public Char Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

    }
}
