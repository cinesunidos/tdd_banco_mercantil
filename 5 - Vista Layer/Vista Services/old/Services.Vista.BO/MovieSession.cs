﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class MovieSession
    {
        private string m_ID;
        private string m_SpanishName;
        
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }        
        public string SpanishName
        {
            get { return m_SpanishName; }
            set { m_SpanishName = value; }
        }
    }
}
