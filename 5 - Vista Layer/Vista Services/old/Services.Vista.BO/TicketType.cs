﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class TicketType
    {
        #region Fields
        private decimal m_Price;
        private int m_Quantity;
        private string m_TicketTypeCode;
        private string m_TicketBarcode;
        #endregion 
        #region Properties
        public decimal Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }
        public int PriceInCents
        {
            get { return (int) (m_Price*(decimal)100); }            
        }
        public int Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }
        public string TicketTypeCode
        {
            get { return m_TicketTypeCode; }
            set { m_TicketTypeCode = value; }
        }

        public string TicketBarcode
        {
            get { return m_TicketBarcode; }
            set { m_TicketBarcode = value; }
        }
        #endregion
    }
}
