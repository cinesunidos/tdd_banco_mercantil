﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public class Theater
    {
        public Theater()
        {
            l_facilities = new List<Facilities>();
        }
        #region Fields
        private string m_ID;                     
        private string m_FullName;
        private string m_DisplayName;
        // private string m_Code; 
        //private string m_ShortName;        
        private string m_Address;       
        private string m_Phone;
        private string m_City;
        private double d_Latitude;
        private double d_Longitude;
        private List<Facilities> l_facilities;
        //private Boolean m_Premium;
        //private Boolean m_TheaterActive;
        #endregion
        #region Properties
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }        
        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }
        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        public double Longitude
        {
            get { return d_Longitude; }
            set { d_Longitude = value; }
        }
        public double Latitude
        {
            get { return d_Latitude; }
            set { d_Latitude = value; }
        }
        //public string Code
        //{
        //    get { return m_Code; }
        //    set { m_Code = value; }
        //}
        //public string ShortName
        //{
        //    get { return m_ShortName; }
        //    set { m_ShortName = value; }
        //}
        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public List<Facilities> P_Facilities
        {
            get { return l_facilities; }
            set { l_facilities = value; }
        }
        //public Boolean Premium
        //{
        //    get { return m_Premium; }
        //    set { m_Premium = value; }
        //}
        //public Boolean TheaterActive
        //{
        //    get { return m_TheaterActive; }
        //    set { m_TheaterActive = value; }
        //}
        #endregion
    }
}
