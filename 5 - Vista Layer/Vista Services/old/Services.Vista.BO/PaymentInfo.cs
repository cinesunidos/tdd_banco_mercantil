﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
    public enum PaymentTenderCategory
    {
        CREDIT = 0,
        DEBIT = 1,
        SVC = 2
    }
    public enum PaymentCardType:int
    {
        VISA=0,
        MASTERCARD=1,
        AMERICANEXPRESS=2,
        GIFTCARD=3,
    }
    public class PaymentInfo
    {
        #region Fields
        private string m_CardCVV;
        private int m_CardExpiryMonth;
        private int m_CardExpiryYear;        
        private string m_CardNumber;
        private PaymentCardType m_CardType;
        private PaymentTenderCategory m_PaymentTenderCategory;
        private string m_Email;
        private string m_Name;
        private string m_IDCard;        
        private string m_Phone;
        private TransactionInfo m_TransactionInfo;
        #endregion 
        #region Properties
        public string CardCVV
        {
            get { return m_CardCVV; }
            set { m_CardCVV = value; }
        }
        public int CardExpiryMonth
        {
            get { return m_CardExpiryMonth; }
            set { m_CardExpiryMonth = value; }
        }
        public int CardExpiryYear
        {
            get { return m_CardExpiryYear; }
            set { m_CardExpiryYear = value; }
        }
        public string CardNumber
        {
            get { return m_CardNumber; }
            set { m_CardNumber = value; }
        }
        public PaymentCardType CardType
        {
            get { return m_CardType; }
            set { m_CardType = value; }
        }
        public PaymentTenderCategory PaymentTenderCategory
        {
            get { return m_PaymentTenderCategory; }
            set { m_PaymentTenderCategory = value; }
        }
        public string PaymentTenderCatStr
        {
            get{return PaymentTenderCategory.ToString();} 
        }
        public string CardTypeStr
        {   
            get 
            {                
                switch (CardType)
	            {
                    case PaymentCardType.MASTERCARD:
                        return CardType.ToString();                        
                    case PaymentCardType.VISA:
                        return CardType.ToString();
                    case PaymentCardType.AMERICANEXPRESS:
                        return "AMERICAN EXPRESS";
                    case PaymentCardType.GIFTCARD:
                        return CardType.ToString();		     
                    default:
                        return "";
	            }
            }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string IDCard
        {
            get { return m_IDCard; }
            set { m_IDCard = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public TransactionInfo TransactionInfo
        {
            get { return m_TransactionInfo; }
            set { m_TransactionInfo = value; }
        }
        #endregion
    }
    public class TransactionInfo
    {
        #region Fields
        private string m_HallName;
        private string m_MovieName;
        private string m_Seats;
        private DateTime m_ShowTime;
        private int m_Count;
        private decimal m_Amount;
        private decimal m_Charge;
        private decimal m_Tax;
        private decimal m_Total;
        #endregion
        #region Properties
        public string HallName
        {
          get { return m_HallName; }
          set { m_HallName = value; }
        }
        public string MovieName
        {
          get { return m_MovieName; }
          set { m_MovieName = value; }
        }
        public string Seats
        {
            get { return m_Seats; }
            set { m_Seats = value; }
        }
        public DateTime ShowTime
        {
            get { return m_ShowTime; }
            set { m_ShowTime = value; }
        }
        public string Date
        {
            get { return m_ShowTime.ToShortDateString(); }            
        }
        public string Time
        {
            get { return m_ShowTime.ToShortTimeString(); }
        }
        public int Count
        {
            get { return m_Count; }
            set { m_Count = value; }
        }
        public decimal Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }
        public decimal Charge
        {
            get { return m_Charge; }
            set { m_Charge = value; }
        }
        public decimal Tax
        {
            get { return m_Tax; }
            set { m_Tax = value; }
        }
        public decimal Total
        {
            get { return m_Total; }
            set { m_Total = value; }
        }
        #endregion
    }
}
