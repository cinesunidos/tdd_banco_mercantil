﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BO
{
   public class TicketTypeBarcode
    {
        #region Fields
        private string m_TType_strCode;
        private string m_Price_strTicket_Type_Description;
        private string m_Price_strTicket_Type_Description_2;
        private int m_AreaCat_intSeq;
        private string m_TType_strAvailLoyaltyOnly;
        private string m_AvailableOnSalesChannel;
        private decimal m_Price_decimalTicket_Price;
        private decimal m_Price_decimalSurcharge;
        private string m_AreaCat_strSeatAllocationOn;
        #endregion
        #region Properties
        public string TType_strCode
        {
            get { return m_TType_strCode; }
            set { m_TType_strCode = value; }
        }
        public string Price_strTicket_Type_Description
        {
            get { return m_Price_strTicket_Type_Description; }
            set { m_Price_strTicket_Type_Description = value; }
        }
        public string Price_strTicket_Type_Description_2
        {
            get { return m_Price_strTicket_Type_Description_2; }
            set { m_Price_strTicket_Type_Description_2 = value; }
        }
        public int AreaCat_intSeq
        {
            get { return m_AreaCat_intSeq; }
            set { m_AreaCat_intSeq = value; }
        }
        public string TType_strAvailLoyaltyOnly
        {
            get { return m_TType_strAvailLoyaltyOnly; }
            set { m_TType_strAvailLoyaltyOnly = value; }
        }
        public string AvailableOnSalesChannel
        {
            get { return m_AvailableOnSalesChannel; }
            set { m_AvailableOnSalesChannel = value; }
        }
        public decimal Price_decimalTicket_Price
        {
            get { return m_Price_decimalTicket_Price; }
            set { m_Price_decimalTicket_Price = value; }
        }
        public decimal Price_decimalSurcharge
        {
            get { return m_Price_decimalSurcharge; }
            set { m_Price_decimalSurcharge = value; }
        }
        public string AreaCat_strSeatAllocationOn
        {
            get { return m_AreaCat_strSeatAllocationOn; }
            set { m_AreaCat_strSeatAllocationOn = value; }
        }
        #endregion
    }
}
