﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Services.Vista.BO
{
    public class Map
    {
        public Map()
        {
        }
        public Map(int Rows, int Columns)
        {
            for (int i = 0; i < Rows; i++)
            {
                List<Seat> sts = new List<Seat>();
                for (int j = 0; j < Columns; j++)
                {
                    Seat seat = new Seat();
                    seat.RowIndex = Rows - i - 1;
                    seat.ColumnIndex = j;
                    //seat.Status = SeatStatus.House;
                    seat.Type = SeatType.Aisle;
                    seat.Name = seat.GetSeatName(i);
                    sts.Add(seat);
                }
                Tier tier = new Tier();
                tier.Name = Seat.GetASCII(i);
                tier.Seats = sts;
                Tiers.Add(tier);
            }
        }
        #region Fields        
        private List<Tier> m_Tiers = new List<Tier>();
        //private string m_AreaCategoryCode;
        //private int m_AreaNumber;
        #endregion
        #region Properties     
        public List<Tier> Tiers
        {
            get { return m_Tiers; }
            set { m_Tiers = value; }
        }        
        #endregion
        #region Methods
        //public static Map Get(string SeatData)
        //{
        //    string mapa = SeatData;
        //    string[] vect = mapa.Split('|');
        //    int height = Int32.Parse(vect[1]) - 2;
        //    int width = Int32.Parse(vect[2]) - 2;
        //    int areaNumber = Int32.Parse(vect[4]);
        //    string areaCategoryCode = vect[5];

        //    Map map = new Map(height, width);            

        //    int i = 12;
        //    int a = 0;
        //    while (a < height)
        //    {
        //        char row = Char.Parse(vect[i + 1]);
        //        string asientosString = vect[i + 2];

        //        int b = 0;
        //        while (b < asientosString.Length)
        //        {
        //            string asientos = asientosString.Substring(b, 12);
        //            int c = Int32.Parse(asientos.Substring(3, 2).Trim()) - 1;
        //            int status = Int32.Parse(asientos.Substring(asientos.Length - 1, 1));

        //            Tier tier = new Tier();
        //            tier = map.Tiers.Where(t => t.Name.Equals(row)).SingleOrDefault();
                    
        //            tier.Seats[c].ColumnIndex = c;
        //            tier.Seats[c].RowIndex = height - a - 1;
        //            tier.Seats[c].Type = SeatType.Chair;
        //            tier.Seats[c].AreaCategoryCode = areaCategoryCode;
        //            tier.Seats[c].AreaNumber = areaNumber;
                    
        //            if (status == 1)
        //                tier.Seats[c].Status = SeatStatus.Booked;
        //            if (status == 3)
        //                tier.Seats[c].Status = SeatStatus.House;
        //            if (status == 5)
        //                tier.Seats[c].Status = SeatStatus.Selected;
        //            if (status == 0)
        //                tier.Seats[c].Status = SeatStatus.Available;


        //            b = b + 12;
        //        }

        //        i = i + 3;
        //        a++;
        //    }

        //    map.Tiers = map.Tiers.OrderByDescending(t => t.Name).ToList();
        //    return map;
        //}
        //public static string GetMapSeatsAvailable(string SeatData)
        //{
        //    string mapa = SeatData;
        //    string[] vect = mapa.Split('|');
        //    int height = Int32.Parse(vect[1]) - 2;
        //    int width = Int32.Parse(vect[2]) - 2;
        //    int areaNumber = Int32.Parse(vect[4]);
        //    string areaCategoryCode = vect[5];
        //    string stringSeats="";            

        //    int i = 12;
        //    int a = 0;
        //    while (a < height)
        //    {
        //        char row = Char.Parse(vect[i + 1]);
        //        string asientosString = vect[i + 2];

        //        int b = 0;
        //        while (b < asientosString.Length)
        //        {
        //            string asientos = asientosString.Substring(b, 12);
        //            int c = Int32.Parse(asientos.Substring(3, 2).Trim()) - 1;
        //            int status = Int32.Parse(asientos.Substring(asientos.Length - 1, 1));

        //            Seat seat = new Seat();
        //            seat.ColumnIndex = c;
        //            seat.RowIndex = height - a - 1;

        //            if (status == 0)
        //               stringSeats = string.Format("{0},{1}",stringSeats, seat.GetSeatName(seat.RowIndex));

        //            b = b + 12;
        //        }

        //        i = i + 3;
        //        a++;
        //    }
        //    stringSeats = stringSeats.Substring(1);
        //    return stringSeats;
        //}
        #endregion
    }
}
