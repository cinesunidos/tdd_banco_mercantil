﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Services.Vista.DAL.DO
{    
    public class Facility
    {
        #region Fields
        private int m_IDFacility;
        private int m_IDTheather;
        private string m_FacilityName;
        #endregion
        #region Properties
        public int IDTheather
        {
            get { return m_IDTheather; }
            set { m_IDTheather = value; }
        }
        public int IDFacility
        {
            get { return m_IDFacility; }
            set { m_IDFacility = value; }
        }
        public string FacilityName
        {
            get { return m_FacilityName; }
            set { m_FacilityName = value; }
        }

        #endregion

        public static List<Facility> getInfoFacilities()
        {
            String Query;
            string ConnectionString = ConfigurationManager.ConnectionStrings["CU"].ConnectionString;
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            connection.Open();

            List<Facility> Facilitys = new List<Facility>();
            Query = string.Format(@"SELECT TF.IDFACILITY,  TF.IDTHEATER,th.FACILITYNAME 
                                    FROM dbo.TBL_THEATERS_FACILITIES AS TF INNER JOIN TBLFACILITIES as TH ON TF.IDFACILITY=TH.IDFACILITY");

            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    Facility facility = new Facility();

                    facility.IDTheather = Int32.Parse(dataReader["IDTHEATER"].ToString());
                    facility.IDFacility = Int32.Parse(dataReader["IDFACILITY"].ToString());
                    facility.m_FacilityName = dataReader["FACILITYNAME"].ToString();
                    Facilitys.Add(facility);
                }
                dataReader.Close();
            }
            connection.Close();
            return Facilitys;
        }
    }


    //    #region Fields
    //    private int m_IDTheather;        
    //    private string m_Address;        
    //    private double d_Latitude;
    //    private double d_Longitude;
    //    private string m_City;
    //    private string m_Phone;
    //    static string connection_BD;
    //    #endregion        
    //    #region Properties
    //    public int IDTheather
    //    {
    //        get { return m_IDTheather; }
    //        set { m_IDTheather = value; }
    //    }       
    //    public string City
    //    {
    //        get { return m_City; }
    //        set { m_City = value; }
    //    }
    //    public double Longitude
    //    {
    //        get { return d_Longitude; }
    //        set { d_Longitude = value; }
    //    }
    //    public double Latitude
    //    {
    //        get { return d_Latitude; }
    //        set { d_Latitude = value; }
    //    }        
    //    public string Address
    //    {
    //        get { return m_Address; }
    //        set { m_Address = value; }
    //    }
    //    public string Phone
    //    {
    //        get { return m_Phone; }
    //        set { m_Phone = value; }
    //    }
    //    #endregion
    //    #region Methods
    //    public  CU()
    //    {
    //         //connection_BD = ConfigurationManager.ConnectionStrings["CU"].ConnectionString;
    //    }
    //    public static List<CU> getInfoTheaters()
    //    {
    //        try
    //        {
    //            connection_BD = ConfigurationManager.ConnectionStrings["CU"].ConnectionString;
    //            SqlConnection connection = new SqlConnection(connection_BD);
    //            //Dictionary<string, object> parameters = new Dictionary<string, object>();
    //            connection.Open();
    //            SqlCommand cmd = new SqlCommand("getInfoTheathers", connection);
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            SqlDataReader r = cmd.ExecuteReader();
    //            List<CU> cu_Object = new List<CU>();
    //            if (r != null)
    //            {
    //                while (r.Read())
    //                {
    //                    CU objCU = new CU();
    //                    objCU.m_IDTheather = Int32.Parse(r["IDTHEATER"].ToString());
    //                    objCU.m_Address = r["ADDRESS"].ToString();
    //                    objCU.m_City = r["CITYNAME"].ToString();
    //                    objCU.m_Phone = r["BOOKINGPHONE"].ToString();
    //                    objCU.d_Latitude = double.Parse(r["LATITUDE"].ToString());
    //                    objCU.d_Longitude = double.Parse(r["LONGITUDE"].ToString());
    //                    cu_Object.Add(objCU);
    //                }
    //            }
    //            connection.Close();
    //            return cu_Object;
    //        }
    //        catch (SqlException ex)
    //        {                
    //            return null;
    //        }           
    //    }        
    //    #endregion 
    
}
