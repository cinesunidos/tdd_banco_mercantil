﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class SessionInfo
    {
        #region Fields
        private string cinema_strIDField;

        private string movie_strIDField;

        private string session_strIDField;

        private System.DateTime session_dtmDate_TimeField;

        private bool session_dtmDate_TimeFieldSpecified;

        private int session_decDay_Of_WeekField;

        private bool session_decDay_Of_WeekFieldSpecified;

        private int session_decSeats_AvailableField;

        private bool session_decSeats_AvailableFieldSpecified;

        private string session_strSeatAllocation_OnField;

        private string price_strGroup_CodeField;

        private string session_strChild_AllowedField;

        private string session_strHOSessionIDField;

        private string cinOperator_strCodeField;

        private string session_strNoFreeListField;

        private int screen_bytNumField;

        private bool screen_bytNumFieldSpecified;

        private string screen_strNameField;

        private string sType_strSessionTypeCodeField;

        private string sType_strDescriptionField;

        private string session_strSalesChannelsField;

        private int screen_intRemoteSalesCutoffField;

        private bool screen_intRemoteSalesCutoffFieldSpecified;

        private string event_strCodeField;

        private string session_strAttributesField;

        private string format_strCodeField;

        private string cinema_strID1Field;

        private string movie_strID1Field;

        private string movie_strNameField;

        private string movie_strShortNameField;

        private string movie_strRatingField;

        private string movie_strRating_DescriptionField;

        private int movie_intList_PosField;

        private bool movie_intList_PosFieldSpecified;

        private int movie_intFCodeField;

        private bool movie_intFCodeFieldSpecified;

        private string movie_strChild_AllowedField;

        private string movie_strVoiceFileNameTitleField;

        private string movie_strVoiceFileNameSynopsisField;

        private string movie_HOFilmCodeField;

        private string movie_strRating_2Field;

        private string movie_strRating_Description_2Field;

        private string movie_strName_2Field;

        private string movie_strShortname_2Field;

        private string distrib_strNameField;

        private string hOPKField;

        private int session_intIDField;

        private bool session_intIDFieldSpecified;

        #endregion
        #region Properties

        public string Cinema_strID
        {
            get
            {
                return this.cinema_strIDField;
            }
            set
            {
                this.cinema_strIDField = value;
            }
        }

        public string Movie_strID
        {
            get
            {
                return this.movie_strIDField;
            }
            set
            {
                this.movie_strIDField = value;
            }
        }

        public string Session_strID
        {
            get
            {
                return this.session_strIDField;
            }
            set
            {
                this.session_strIDField = value;
            }
        }

        public System.DateTime Session_dtmDate_Time
        {
            get
            {
                return this.session_dtmDate_TimeField;
            }
            set
            {
                this.session_dtmDate_TimeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_dtmDate_TimeSpecified
        {
            get
            {
                return this.session_dtmDate_TimeFieldSpecified;
            }
            set
            {
                this.session_dtmDate_TimeFieldSpecified = value;
            }
        }

        public int Session_decDay_Of_Week
        {
            get
            {
                return this.session_decDay_Of_WeekField;
            }
            set
            {
                this.session_decDay_Of_WeekField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decDay_Of_WeekSpecified
        {
            get
            {
                return this.session_decDay_Of_WeekFieldSpecified;
            }
            set
            {
                this.session_decDay_Of_WeekFieldSpecified = value;
            }
        }

        public int Session_decSeats_Available
        {
            get
            {
                return this.session_decSeats_AvailableField;
            }
            set
            {
                this.session_decSeats_AvailableField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decSeats_AvailableSpecified
        {
            get
            {
                return this.session_decSeats_AvailableFieldSpecified;
            }
            set
            {
                this.session_decSeats_AvailableFieldSpecified = value;
            }
        }

        public string Session_strSeatAllocation_On
        {
            get
            {
                return this.session_strSeatAllocation_OnField;
            }
            set
            {
                this.session_strSeatAllocation_OnField = value;
            }
        }

        public string Price_strGroup_Code
        {
            get
            {
                return this.price_strGroup_CodeField;
            }
            set
            {
                this.price_strGroup_CodeField = value;
            }
        }

        public string Session_strChild_Allowed
        {
            get
            {
                return this.session_strChild_AllowedField;
            }
            set
            {
                this.session_strChild_AllowedField = value;
            }
        }

        public string Session_strHOSessionID
        {
            get
            {
                return this.session_strHOSessionIDField;
            }
            set
            {
                this.session_strHOSessionIDField = value;
            }
        }

        public string CinOperator_strCode
        {
            get
            {
                return this.cinOperator_strCodeField;
            }
            set
            {
                this.cinOperator_strCodeField = value;
            }
        }

        public string Session_strNoFreeList
        {
            get
            {
                return this.session_strNoFreeListField;
            }
            set
            {
                this.session_strNoFreeListField = value;
            }
        }

        public int Screen_bytNum
        {
            get
            {
                return this.screen_bytNumField;
            }
            set
            {
                this.screen_bytNumField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_bytNumSpecified
        {
            get
            {
                return this.screen_bytNumFieldSpecified;
            }
            set
            {
                this.screen_bytNumFieldSpecified = value;
            }
        }

        public string Screen_strName
        {
            get
            {
                return this.screen_strNameField;
            }
            set
            {
                this.screen_strNameField = value;
            }
        }

        public string SType_strSessionTypeCode
        {
            get
            {
                return this.sType_strSessionTypeCodeField;
            }
            set
            {
                this.sType_strSessionTypeCodeField = value;
            }
        }

        public string SType_strDescription
        {
            get
            {
                return this.sType_strDescriptionField;
            }
            set
            {
                this.sType_strDescriptionField = value;
            }
        }

        public string Session_strSalesChannels
        {
            get
            {
                return this.session_strSalesChannelsField;
            }
            set
            {
                this.session_strSalesChannelsField = value;
            }
        }

        public int Screen_intRemoteSalesCutoff
        {
            get
            {
                return this.screen_intRemoteSalesCutoffField;
            }
            set
            {
                this.screen_intRemoteSalesCutoffField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_intRemoteSalesCutoffSpecified
        {
            get
            {
                return this.screen_intRemoteSalesCutoffFieldSpecified;
            }
            set
            {
                this.screen_intRemoteSalesCutoffFieldSpecified = value;
            }
        }

        public string Event_strCode
        {
            get
            {
                return this.event_strCodeField;
            }
            set
            {
                this.event_strCodeField = value;
            }
        }

        public string Session_strAttributes
        {
            get
            {
                return this.session_strAttributesField;
            }
            set
            {
                this.session_strAttributesField = value;
            }
        }

        public string Format_strCode
        {
            get
            {
                return this.format_strCodeField;
            }
            set
            {
                this.format_strCodeField = value;
            }
        }

        public string Cinema_strID1
        {
            get
            {
                return this.cinema_strID1Field;
            }
            set
            {
                this.cinema_strID1Field = value;
            }
        }

        public string Movie_strID1
        {
            get
            {
                return this.movie_strID1Field;
            }
            set
            {
                this.movie_strID1Field = value;
            }
        }

        public string Movie_strName
        {
            get
            {
                return this.movie_strNameField;
            }
            set
            {
                this.movie_strNameField = value;
            }
        }

        public string Movie_strShortName
        {
            get
            {
                return this.movie_strShortNameField;
            }
            set
            {
                this.movie_strShortNameField = value;
            }
        }

        public string Movie_strRating
        {
            get
            {
                return this.movie_strRatingField;
            }
            set
            {
                this.movie_strRatingField = value;
            }
        }

        public string Movie_strRating_Description
        {
            get
            {
                return this.movie_strRating_DescriptionField;
            }
            set
            {
                this.movie_strRating_DescriptionField = value;
            }
        }

        public int Movie_intList_Pos
        {
            get
            {
                return this.movie_intList_PosField;
            }
            set
            {
                this.movie_intList_PosField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Movie_intList_PosSpecified
        {
            get
            {
                return this.movie_intList_PosFieldSpecified;
            }
            set
            {
                this.movie_intList_PosFieldSpecified = value;
            }
        }

        public int Movie_intFCode
        {
            get
            {
                return this.movie_intFCodeField;
            }
            set
            {
                this.movie_intFCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Movie_intFCodeSpecified
        {
            get
            {
                return this.movie_intFCodeFieldSpecified;
            }
            set
            {
                this.movie_intFCodeFieldSpecified = value;
            }
        }

        public string Movie_strChild_Allowed
        {
            get
            {
                return this.movie_strChild_AllowedField;
            }
            set
            {
                this.movie_strChild_AllowedField = value;
            }
        }

        public string Movie_strVoiceFileNameTitle
        {
            get
            {
                return this.movie_strVoiceFileNameTitleField;
            }
            set
            {
                this.movie_strVoiceFileNameTitleField = value;
            }
        }

        public string Movie_strVoiceFileNameSynopsis
        {
            get
            {
                return this.movie_strVoiceFileNameSynopsisField;
            }
            set
            {
                this.movie_strVoiceFileNameSynopsisField = value;
            }
        }

        public string Movie_HOFilmCode
        {
            get
            {
                return this.movie_HOFilmCodeField;
            }
            set
            {
                this.movie_HOFilmCodeField = value;
            }
        }

        public string Movie_strRating_2
        {
            get
            {
                return this.movie_strRating_2Field;
            }
            set
            {
                this.movie_strRating_2Field = value;
            }
        }

        public string Movie_strRating_Description_2
        {
            get
            {
                return this.movie_strRating_Description_2Field;
            }
            set
            {
                this.movie_strRating_Description_2Field = value;
            }
        }

        public string Movie_strName_2
        {
            get
            {
                return this.movie_strName_2Field;
            }
            set
            {
                this.movie_strName_2Field = value;
            }
        }

        public string Movie_strShortname_2
        {
            get
            {
                return this.movie_strShortname_2Field;
            }
            set
            {
                this.movie_strShortname_2Field = value;
            }
        }

        public string Distrib_strName
        {
            get
            {
                return this.distrib_strNameField;
            }
            set
            {
                this.distrib_strNameField = value;
            }
        }

        public string HOPK
        {
            get
            {
                return this.hOPKField;
            }
            set
            {
                this.hOPKField = value;
            }
        }

        public int Session_intID
        {
            get
            {
                return this.session_intIDField;
            }
            set
            {
                this.session_intIDField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_intIDSpecified
        {
            get
            {
                return this.session_intIDFieldSpecified;
            }
            set
            {
                this.session_intIDFieldSpecified = value;
            }
        }
        #endregion
        #region Methods
        public static SessionInfo GetSessionInfo(string CinemaID, int SessionID)
        {
            DataService.GetSessionInfoRequest obj = new DataService.GetSessionInfoRequest();
            obj.CinemaId = CinemaID;
            obj.SessionId = SessionID.ToString();

            Interpret<SessionInfo, DataService.GetSessionInfoRequest> interpret = new Interpret<SessionInfo, DataService.GetSessionInfoRequest>();
            List<SessionInfo> SessionInfos = interpret.Get("GetSessionInfo", obj);

            return SessionInfos.FirstOrDefault();
        }
        #endregion
    }
}
