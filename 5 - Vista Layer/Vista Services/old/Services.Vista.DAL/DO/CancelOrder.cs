﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class CancelOrder
    {
        public static Boolean GetCancelOrder(string ClientID, string UserSessionID)
        {
            TicketingService.TicketingService Ticketing = new TicketingService.TicketingService();

            TicketingService.CancelOrderRequest CancelOrderRequest = new TicketingService.CancelOrderRequest();
            CancelOrderRequest.UserSessionId = UserSessionID;
            CancelOrderRequest.OptionalClientId = ClientID;
            

            TicketingService.Response response = Ticketing.CancelOrder(CancelOrderRequest);
            if (response.Result == TicketingService.ResultCode.OK)
                return true;
            else
                return false;            
        }
    }
}
