﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.TicketingService
{
    public partial class AddTicketsResponse
    {
        public static AddTicketsResponse AddTicket(string ClientID,string UserSessionID, string CinemaID, string SessionID, TicketType[] TicketTypes)
        {
            TicketingService Ticketing = new TicketingService();
            AddTicketsRequest TicketRequest = new AddTicketsRequest();
            TicketRequest.CinemaId = CinemaID;
            TicketRequest.SessionId = SessionID;
            TicketRequest.TicketTypes = TicketTypes;
            TicketRequest.UserSessionId = UserSessionID;
            //TicketRequest.OptionalClientId = ClientID;
            TicketRequest.UserSelectedSeatingSupported = true;
            TicketRequest.ReturnSeatData = true;
            TicketRequest.ExcludeAreasWithoutTickets = false;
            TicketRequest.IncludeSeatNumbers = true;
            TicketRequest.ProcessOrderValue = true;
            TicketRequest.ReturnOrder = true;
            TicketRequest.ReturnDiscountInfo = false;
            TicketRequest.BookingMode = 0;
            TicketRequest.OptionalClientId = ClientID;

            AddTicketsResponse rp = Ticketing.AddTickets(TicketRequest);
            return rp;
        }        
    }
}
