﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class Package
    {
        public string Cinema_strID {get; set;} 
        public string PGroup_strCode {get; set;}
        public string Price_strCode {get; set;}
        public string PPack_strPackageDesc {get; set;}
        public string PPack_strAllowRemoteSales {get; set;}
        public string PPack_strType {get; set;}
        public string TType_strCode {get; set;}
        public string Price_strDescription {get; set;}
        public string Price_strDescriptionAlt {get; set;}
        public string Item_strItemId {get; set;}
        public string Item_strItemDescription {get; set;}
        public string Item_strItemDescriptionAlt {get; set;}
        public string PPack_strCode {get; set;}
        public string PPack_intQuantity {get; set;}
        public string PPack_intPriceEach {get; set;}
        public string STax_strCode {get; set;}
        public string PPack_intSurcharge {get; set;}
        public string Price_strTicket_Type_Description_2 {  get; set;}

        #region Methods
        public static List<Package> GetPackageList(string TicketType, string CinemaID, string SessionID)
        {
            DataService.GetTicketTypePackageRequest req = new DataService.GetTicketTypePackageRequest();
            req.CinemaId = CinemaID;
            req.SessionId = SessionID;
            req.TicketTypeCode = TicketType;
            Interpret<Package, DataService.GetTicketTypePackageRequest> interpret = new Interpret<Package, DataService.GetTicketTypePackageRequest>();
            List<Package> Packages = interpret.Get("GetTicketTypePackage", req);
            return Packages;
        }
        #endregion
    }
}
