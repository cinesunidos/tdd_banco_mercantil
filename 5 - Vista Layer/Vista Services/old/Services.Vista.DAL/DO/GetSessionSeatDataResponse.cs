﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.TicketingService
{
    public partial class GetSessionSeatDataResponse
    {
        public static GetSessionSeatDataResponse GetSessionSeatData(string ClientID, string UserSessionID, string SessionID, string TheaterID)
        {
            TicketingService Ticketing = new TicketingService();

            GetSessionSeatDataRequest getSessionSeatDataResquest = new GetSessionSeatDataRequest();
            getSessionSeatDataResquest.UserSessionId = UserSessionID;
            getSessionSeatDataResquest.SessionId = SessionID;
            getSessionSeatDataResquest.CinemaId = TheaterID;
            getSessionSeatDataResquest.OptionalClientId = ClientID;
           // getSessionSeatDataResquest.ExcludeAreasWithoutTickets = false;
            getSessionSeatDataResquest.IncludeSeatNumbers = true;
           // getSessionSeatDataResquest.ReturnOrder = true;
     
            GetSessionSeatDataResponse rp = Ticketing.GetSessionSeatData(getSessionSeatDataResquest);

            return rp;
        }
    }
}
