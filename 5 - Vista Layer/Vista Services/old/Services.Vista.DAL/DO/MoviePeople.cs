﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class MoviePeople
    {
        #region Fields
        private string fPerson_strTypeField;
        private string person_strFirstNameField;
        private string person_strLastNameField;
        private string person_strURLToDetailsField;
        private string person_strURLToPictureField;
        #endregion
        #region Properties
        public string FPerson_strType
        {
            get
            {
                return this.fPerson_strTypeField;
            }
            set
            {
                this.fPerson_strTypeField = value;
            }
        }

        public string Person_strFirstName
        {
            get
            {
                return this.person_strFirstNameField;
            }
            set
            {
                this.person_strFirstNameField = value;
            }
        }

        public string Person_strLastName
        {
            get
            {
                return this.person_strLastNameField;
            }
            set
            {
                this.person_strLastNameField = value;
            }
        }

        public string Person_strURLToDetails
        {
            get
            {
                return this.person_strURLToDetailsField;
            }
            set
            {
                this.person_strURLToDetailsField = value;
            }
        }

        public string Person_strURLToPicture
        {
            get
            {
                return this.person_strURLToPictureField;
            }
            set
            {
                this.person_strURLToPictureField = value;
            }
        }
        #endregion 
        public static List<MoviePeople> GetMoviePeople(string MovieID)
        {
            DataService.GetMoviePeopleRequest req = new DataService.GetMoviePeopleRequest();
            req.MovieId = MovieID;
            Interpret<MoviePeople, DataService.GetMoviePeopleRequest> interpret = new Interpret<MoviePeople, DataService.GetMoviePeopleRequest>();
            List<MoviePeople> MoviePeoples = interpret.Get("GetMoviePeople", req);

            return MoviePeoples;
        }
        public static string GetActors(List<MoviePeople> MoviePeoples)
        {
            string actorsString="";
            foreach (MoviePeople item in MoviePeoples)
            {
                if (item.FPerson_strType.ToUpper().Equals("A") == true)
                {
                    if(actorsString != string.Empty) actorsString = actorsString + ",";
                    actorsString = string.Format("{0} {1} {2}", actorsString, item.Person_strFirstName, item.Person_strLastName); 
                }
            }
            return actorsString.Trim();
        }
        public static string GetDirectors(List<MoviePeople> MoviePeoples)
        {
            string directorString = "";
            foreach (MoviePeople item in MoviePeoples)
            {
                if (item.FPerson_strType.ToUpper().Equals("D") == true)
                {
                    if (directorString != string.Empty) directorString = directorString + ",";
                    directorString = string.Format("{0} {1} {2}", directorString, item.Person_strFirstName, item.Person_strLastName);
                }
            }
            return directorString.Trim();
        }
    }
}
