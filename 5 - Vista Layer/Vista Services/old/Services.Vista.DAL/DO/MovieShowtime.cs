﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class MovieShowtime
    {
        #region Fields
        private string cinema_strIDField;

        private string session_strIDField;

        private System.DateTime session_dtmDate_TimeField;

        private bool session_dtmDate_TimeFieldSpecified;

        private int session_decDay_Of_WeekField;

        private bool session_decDay_Of_WeekFieldSpecified;

        private int session_decSeats_AvailableField;

        private bool session_decSeats_AvailableFieldSpecified;

        private string session_strSeatAllocation_OnField;

        private string price_strGroup_CodeField;

        private string session_strChild_AllowedField;

        private string session_strNoFreeListField;

        private int screen_bytNumField;

        private bool screen_bytNumFieldSpecified;

        private string screen_strNameField;

        private int screen_intRemoteSalesCutoffField;

        private bool screen_intRemoteSalesCutoffFieldSpecified;

        private string session_strAttributesField;

        private int session_intIDField;

        private bool session_intIDFieldSpecified;

        private string movie_strIDField;

        private string movie_strNameField;

        private string movie_strRatingField;

        private string movie_strName_2Field;

        private string movie_strRating_2Field;

        private string movie_strRating_DescriptionField;

        private string movie_strRating_Description_2Field;

        private string cinema_strNameField;

        private string cinema_strName_2Field;

        private string filmCat_strNameField;

        private string film_strCodeField;

        private string film_strCensorField;

        private string film_strContentField;

        private string film_strTitleField;

        private short film_intDurationField;

        private bool film_intDurationFieldSpecified;

        private string film_strURL1Field;

        private string film_strURL2Field;

        private string film_strTitleAltField;

        private string film_strCensorAltField;

        private string film_strContentAltField;

        private string film_strDescriptionAltField;

        private string film_strURL1DescriptionField;

        private string film_strURL2DescriptionField;

        private string film_strURLforGraphicField;

        private string film_strURLforFilmNameField;

        private string film_strURLforTrailerField;

        private string film_strMovieFormatField;

        private string film_strSoundFormatField;

        private string film_strUpcomingFeatureFlagField;

        private string film_strFeatureFlagField;

        private string film_strNowShowingFlagField;

        private System.DateTime film_dtmOpeningDateField;

        private bool film_dtmOpeningDateFieldSpecified;

        private short film_intOpeningYearField;

        private bool film_intOpeningYearFieldSpecified;

        private short film_intOpeningWeekField;

        private bool film_intOpeningWeekFieldSpecified;

        private string film_strDescriptionLongField;

        private string event_strFilmsIndependentField;
        #endregion
        #region Properties
        public string Cinema_strID
        {
            get
            {
                return this.cinema_strIDField;
            }
            set
            {
                this.cinema_strIDField = value;
            }
        }

        public string Session_strID
        {
            get
            {
                return this.session_strIDField;
            }
            set
            {
                this.session_strIDField = value;
            }
        }

        public System.DateTime Session_dtmDate_Time
        {
            get
            {
                return this.session_dtmDate_TimeField;
            }
            set
            {
                this.session_dtmDate_TimeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_dtmDate_TimeSpecified
        {
            get
            {
                return this.session_dtmDate_TimeFieldSpecified;
            }
            set
            {
                this.session_dtmDate_TimeFieldSpecified = value;
            }
        }

        public int Session_decDay_Of_Week
        {
            get
            {
                return this.session_decDay_Of_WeekField;
            }
            set
            {
                this.session_decDay_Of_WeekField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decDay_Of_WeekSpecified
        {
            get
            {
                return this.session_decDay_Of_WeekFieldSpecified;
            }
            set
            {
                this.session_decDay_Of_WeekFieldSpecified = value;
            }
        }

        public int Session_decSeats_Available
        {
            get
            {
                return this.session_decSeats_AvailableField;
            }
            set
            {
                this.session_decSeats_AvailableField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_decSeats_AvailableSpecified
        {
            get
            {
                return this.session_decSeats_AvailableFieldSpecified;
            }
            set
            {
                this.session_decSeats_AvailableFieldSpecified = value;
            }
        }

        public string Session_strSeatAllocation_On
        {
            get
            {
                return this.session_strSeatAllocation_OnField;
            }
            set
            {
                this.session_strSeatAllocation_OnField = value;
            }
        }

        public string Price_strGroup_Code
        {
            get
            {
                return this.price_strGroup_CodeField;
            }
            set
            {
                this.price_strGroup_CodeField = value;
            }
        }

        public string Session_strChild_Allowed
        {
            get
            {
                return this.session_strChild_AllowedField;
            }
            set
            {
                this.session_strChild_AllowedField = value;
            }
        }

        public string Session_strNoFreeList
        {
            get
            {
                return this.session_strNoFreeListField;
            }
            set
            {
                this.session_strNoFreeListField = value;
            }
        }

        public int Screen_bytNum
        {
            get
            {
                return this.screen_bytNumField;
            }
            set
            {
                this.screen_bytNumField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_bytNumSpecified
        {
            get
            {
                return this.screen_bytNumFieldSpecified;
            }
            set
            {
                this.screen_bytNumFieldSpecified = value;
            }
        }

        public string Screen_strName
        {
            get
            {
                return this.screen_strNameField;
            }
            set
            {
                this.screen_strNameField = value;
            }
        }

        public int Screen_intRemoteSalesCutoff
        {
            get
            {
                return this.screen_intRemoteSalesCutoffField;
            }
            set
            {
                this.screen_intRemoteSalesCutoffField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Screen_intRemoteSalesCutoffSpecified
        {
            get
            {
                return this.screen_intRemoteSalesCutoffFieldSpecified;
            }
            set
            {
                this.screen_intRemoteSalesCutoffFieldSpecified = value;
            }
        }

        public string Session_strAttributes
        {
            get
            {
                return this.session_strAttributesField;
            }
            set
            {
                this.session_strAttributesField = value;
            }
        }

        public int Session_intID
        {
            get
            {
                return this.session_intIDField;
            }
            set
            {
                this.session_intIDField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Session_intIDSpecified
        {
            get
            {
                return this.session_intIDFieldSpecified;
            }
            set
            {
                this.session_intIDFieldSpecified = value;
            }
        }

        public string Movie_strID
        {
            get
            {
                return this.movie_strIDField;
            }
            set
            {
                this.movie_strIDField = value;
            }
        }

        public string Movie_strName
        {
            get
            {
                return this.movie_strNameField;
            }
            set
            {
                this.movie_strNameField = value;
            }
        }

        public string Movie_strRating
        {
            get
            {
                return this.movie_strRatingField;
            }
            set
            {
                this.movie_strRatingField = value;
            }
        }

        public string Movie_strName_2
        {
            get
            {
                return this.movie_strName_2Field;
            }
            set
            {
                this.movie_strName_2Field = value;
            }
        }

        public string Movie_strRating_2
        {
            get
            {
                return this.movie_strRating_2Field;
            }
            set
            {
                this.movie_strRating_2Field = value;
            }
        }

        public string Movie_strRating_Description
        {
            get
            {
                return this.movie_strRating_DescriptionField;
            }
            set
            {
                this.movie_strRating_DescriptionField = value;
            }
        }

        public string Movie_strRating_Description_2
        {
            get
            {
                return this.movie_strRating_Description_2Field;
            }
            set
            {
                this.movie_strRating_Description_2Field = value;
            }
        }

        public string Cinema_strName
        {
            get
            {
                return this.cinema_strNameField;
            }
            set
            {
                this.cinema_strNameField = value;
            }
        }

        public string Cinema_strName_2
        {
            get
            {
                return this.cinema_strName_2Field;
            }
            set
            {
                this.cinema_strName_2Field = value;
            }
        }

        public string FilmCat_strName
        {
            get
            {
                return this.filmCat_strNameField;
            }
            set
            {
                this.filmCat_strNameField = value;
            }
        }

        public string Film_strCode
        {
            get
            {
                return this.film_strCodeField;
            }
            set
            {
                this.film_strCodeField = value;
            }
        }

        public string Film_strCensor
        {
            get
            {
                return this.film_strCensorField;
            }
            set
            {
                this.film_strCensorField = value;
            }
        }

        public string Film_strContent
        {
            get
            {
                return this.film_strContentField;
            }
            set
            {
                this.film_strContentField = value;
            }
        }

        public string Film_strTitle
        {
            get
            {
                return this.film_strTitleField;
            }
            set
            {
                this.film_strTitleField = value;
            }
        }

        public short Film_intDuration
        {
            get
            {
                return this.film_intDurationField;
            }
            set
            {
                this.film_intDurationField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Film_intDurationSpecified
        {
            get
            {
                return this.film_intDurationFieldSpecified;
            }
            set
            {
                this.film_intDurationFieldSpecified = value;
            }
        }

        public string Film_strURL1
        {
            get
            {
                return this.film_strURL1Field;
            }
            set
            {
                this.film_strURL1Field = value;
            }
        }

        public string Film_strURL2
        {
            get
            {
                return this.film_strURL2Field;
            }
            set
            {
                this.film_strURL2Field = value;
            }
        }

        public string Film_strTitleAlt
        {
            get
            {
                return this.film_strTitleAltField;
            }
            set
            {
                this.film_strTitleAltField = value;
            }
        }

        public string Film_strCensorAlt
        {
            get
            {
                return this.film_strCensorAltField;
            }
            set
            {
                this.film_strCensorAltField = value;
            }
        }

        public string Film_strContentAlt
        {
            get
            {
                return this.film_strContentAltField;
            }
            set
            {
                this.film_strContentAltField = value;
            }
        }

        public string Film_strDescriptionAlt
        {
            get
            {
                return this.film_strDescriptionAltField;
            }
            set
            {
                this.film_strDescriptionAltField = value;
            }
        }

        public string Film_strURL1Description
        {
            get
            {
                return this.film_strURL1DescriptionField;
            }
            set
            {
                this.film_strURL1DescriptionField = value;
            }
        }

        public string Film_strURL2Description
        {
            get
            {
                return this.film_strURL2DescriptionField;
            }
            set
            {
                this.film_strURL2DescriptionField = value;
            }
        }

        public string Film_strURLforGraphic
        {
            get
            {
                return this.film_strURLforGraphicField;
            }
            set
            {
                this.film_strURLforGraphicField = value;
            }
        }

        public string Film_strURLforFilmName
        {
            get
            {
                return this.film_strURLforFilmNameField;
            }
            set
            {
                this.film_strURLforFilmNameField = value;
            }
        }

        public string Film_strURLforTrailer
        {
            get
            {
                return this.film_strURLforTrailerField;
            }
            set
            {
                this.film_strURLforTrailerField = value;
            }
        }

        public string Film_strMovieFormat
        {
            get
            {
                return this.film_strMovieFormatField;
            }
            set
            {
                this.film_strMovieFormatField = value;
            }
        }

        public string Film_strSoundFormat
        {
            get
            {
                return this.film_strSoundFormatField;
            }
            set
            {
                this.film_strSoundFormatField = value;
            }
        }

        public string Film_strUpcomingFeatureFlag
        {
            get
            {
                return this.film_strUpcomingFeatureFlagField;
            }
            set
            {
                this.film_strUpcomingFeatureFlagField = value;
            }
        }

        public string Film_strFeatureFlag
        {
            get
            {
                return this.film_strFeatureFlagField;
            }
            set
            {
                this.film_strFeatureFlagField = value;
            }
        }

        public string Film_strNowShowingFlag
        {
            get
            {
                return this.film_strNowShowingFlagField;
            }
            set
            {
                this.film_strNowShowingFlagField = value;
            }
        }

        public System.DateTime Film_dtmOpeningDate
        {
            get
            {
                return this.film_dtmOpeningDateField;
            }
            set
            {
                this.film_dtmOpeningDateField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Film_dtmOpeningDateSpecified
        {
            get
            {
                return this.film_dtmOpeningDateFieldSpecified;
            }
            set
            {
                this.film_dtmOpeningDateFieldSpecified = value;
            }
        }

        public short Film_intOpeningYear
        {
            get
            {
                return this.film_intOpeningYearField;
            }
            set
            {
                this.film_intOpeningYearField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Film_intOpeningYearSpecified
        {
            get
            {
                return this.film_intOpeningYearFieldSpecified;
            }
            set
            {
                this.film_intOpeningYearFieldSpecified = value;
            }
        }

        public short Film_intOpeningWeek
        {
            get
            {
                return this.film_intOpeningWeekField;
            }
            set
            {
                this.film_intOpeningWeekField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Film_intOpeningWeekSpecified
        {
            get
            {
                return this.film_intOpeningWeekFieldSpecified;
            }
            set
            {
                this.film_intOpeningWeekFieldSpecified = value;
            }
        }

        public string Film_strDescriptionLong
        {
            get
            {
                return this.film_strDescriptionLongField;
            }
            set
            {
                this.film_strDescriptionLongField = value;
            }
        }

        public string Event_strFilmsIndependent
        {
            get
            {
                return this.event_strFilmsIndependentField;
            }
            set
            {
                this.event_strFilmsIndependentField = value;
            }
        }
        #endregion
        #region Methods
        public static List<MovieShowtime> GetMovieShowtimes(string TheaterID, string BizDate)
        {
            DataService.GetMovieShowtimesRequest req = new DataService.GetMovieShowtimesRequest();
            req.CinemaId = TheaterID;
            req.BizStartTimeOfDay = 0;
            req.BizDate = BizDate;
            req.OrderMode = "CINEMA";
            

            Interpret<MovieShowtime, DataService.GetMovieShowtimesRequest> interpret = new Interpret<MovieShowtime, DataService.GetMovieShowtimesRequest>();
            List<MovieShowtime> Movies = interpret.Get("GetMovieShowtimes", req);

            return Movies;
        }
        #endregion 
    }
}
