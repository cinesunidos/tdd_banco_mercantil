﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.TicketingService
{
    public partial class SetSelectedSeatsResponse
    {
        public static SetSelectedSeatsResponse SetSelectedSeats(string ClientID, string UserSessionID, string CinemaID, string SessionID, SelectedSeat[] SelectedSeats)
        {
            TicketingService Ticketing = new TicketingService();

            SetSelectedSeatsRequest setSelectedSeatsRequest = new SetSelectedSeatsRequest();
            setSelectedSeatsRequest.CinemaId = CinemaID;
            setSelectedSeatsRequest.ReturnOrder = true;
            setSelectedSeatsRequest.SessionId = SessionID;
            setSelectedSeatsRequest.UserSessionId = UserSessionID;
            setSelectedSeatsRequest.OptionalClientId = ClientID;
            setSelectedSeatsRequest.SelectedSeats = SelectedSeats;

            SetSelectedSeatsResponse rp = Ticketing.SetSelectedSeats(setSelectedSeatsRequest);
            return rp;
        }
    }
}
