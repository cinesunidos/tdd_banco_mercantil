﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class TicketTypeList
    {
        #region Fields
        private int isPaidField;
        private bool isPaidFieldSpecified;
        private int availableOnSalesChannelField;
        private bool availableOnSalesChannelFieldSpecified;
        private int isDisplayedStandardField;
        private bool isDisplayedStandardFieldSpecified;
        private int isLoyaltyOnlyField;
        private bool isLoyaltyOnlyFieldSpecified;
        private int orderPricesByField;
        private bool orderPricesByFieldSpecified;
        private string ticketHasBarcodeField;
        private string cinema_strIDField;
        private string session_strIDField;
        private string areaCat_strCodeField;
        private string areaCat_strSeatAllocationOnField;
        private string cinema_strID1Field;
        private string price_strTicket_Type_CodeField;
        private string price_strTicket_Type_DescriptionField;
        private string price_strGroup_CodeField;
        private int price_intTicket_PriceField;
        private bool price_intTicket_PriceFieldSpecified;
        private string price_strChild_TicketField;
        private string areaCat_strCode1Field;
        private int areaCat_intSeqField;
        private bool areaCat_intSeqFieldSpecified;
        private string price_strTicket_Type_Description_2Field;
        private string price_strPackageField;
        private string tType_strAvailLoyaltyOnlyField;
        private string tType_strHOCodeField;
        private string price_strRedemptionField;
        private string price_strCompField;
        private string tType_strSalesChannelsField;
        private string price_strATMAvailableField;
        private string tType_strShowOnPOSField;
        private int price_intSurchargeField;
        private bool price_intSurchargeFieldSpecified;
        private int tType_intBarcodeMaxRepeatsField;
        private bool tType_intBarcodeMaxRepeatsFieldSpecified;
        private string tType_strMaxRepeatsCycleField;
        private string tType_strLongDescriptionField;
        private string tType_strLongDescriptionAltField;
        private string tType_strMemberCardField;
        private string tType_strAvailRecognitionOnlyField;
        private string hOPKField;
        private string areaCat_strDescField;
        private string areaCat_strDescAltField;
        private string mMC_strNameField;
        private int price_intSequenceField;
        private bool price_intSequenceFieldSpecified;
        private string price_strIsTicketPackageField;
        private string loyaltyRewardTicketField;
        private string loyaltyTicketNameField;
        private string loyaltyTicketDescriptionField;
        private string loyaltyTicketMessageTextField;
        private string orderTicketsByField;
        private string ticketCategoryField;
        private string price_strLoyaltyRecognitionIDField;
        private string price_intLoyaltyDisplayPriceField;
        private string price_intLoyaltyPriceCentsField;
        private string price_intLoyaltyPointsCostField;
        private string price_intLoyaltyQtyAvailableField;
        #endregion
        #region Properties
        public int IsPaid
        {
            get
            {
                return this.isPaidField;
            }
            set
            {
                this.isPaidField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsPaidSpecified
        {
            get
            {
                return this.isPaidFieldSpecified;
            }
            set
            {
                this.isPaidFieldSpecified = value;
            }
        }
        public int AvailableOnSalesChannel
        {
            get
            {
                return this.availableOnSalesChannelField;
            }
            set
            {
                this.availableOnSalesChannelField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailableOnSalesChannelSpecified
        {
            get
            {
                return this.availableOnSalesChannelFieldSpecified;
            }
            set
            {
                this.availableOnSalesChannelFieldSpecified = value;
            }
        }
        public int IsDisplayedStandard
        {
            get
            {
                return this.isDisplayedStandardField;
            }
            set
            {
                this.isDisplayedStandardField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsDisplayedStandardSpecified
        {
            get
            {
                return this.isDisplayedStandardFieldSpecified;
            }
            set
            {
                this.isDisplayedStandardFieldSpecified = value;
            }
        }
        public int IsLoyaltyOnly
        {
            get
            {
                return this.isLoyaltyOnlyField;
            }
            set
            {
                this.isLoyaltyOnlyField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsLoyaltyOnlySpecified
        {
            get
            {
                return this.isLoyaltyOnlyFieldSpecified;
            }
            set
            {
                this.isLoyaltyOnlyFieldSpecified = value;
            }
        }
        public int OrderPricesBy
        {
            get
            {
                return this.orderPricesByField;
            }
            set
            {
                this.orderPricesByField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OrderPricesBySpecified
        {
            get
            {
                return this.orderPricesByFieldSpecified;
            }
            set
            {
                this.orderPricesByFieldSpecified = value;
            }
        }
        public string TicketHasBarcode
        {
            get
            {
                return this.ticketHasBarcodeField;
            }
            set
            {
                this.ticketHasBarcodeField = value;
            }
        }
        public string Cinema_strID
        {
            get
            {
                return this.cinema_strIDField;
            }
            set
            {
                this.cinema_strIDField = value;
            }
        }
        public string Session_strID
        {
            get
            {
                return this.session_strIDField;
            }
            set
            {
                this.session_strIDField = value;
            }
        }
        public string AreaCat_strCode
        {
            get
            {
                return this.areaCat_strCodeField;
            }
            set
            {
                this.areaCat_strCodeField = value;
            }
        }
        public string AreaCat_strSeatAllocationOn
        {
            get
            {
                return this.areaCat_strSeatAllocationOnField;
            }
            set
            {
                this.areaCat_strSeatAllocationOnField = value;
            }
        }
        public string Cinema_strID1
        {
            get
            {
                return this.cinema_strID1Field;
            }
            set
            {
                this.cinema_strID1Field = value;
            }
        }
        public string Price_strTicket_Type_Code
        {
            get
            {
                return this.price_strTicket_Type_CodeField;
            }
            set
            {
                this.price_strTicket_Type_CodeField = value;
            }
        }
        public string Price_strTicket_Type_Description
        {
            get
            {
                return this.price_strTicket_Type_DescriptionField;
            }
            set
            {
                this.price_strTicket_Type_DescriptionField = value;
            }
        }
        public string Price_strGroup_Code
        {
            get
            {
                return this.price_strGroup_CodeField;
            }
            set
            {
                this.price_strGroup_CodeField = value;
            }
        }
        public int Price_intTicket_Price
        {
            get
            {
                return this.price_intTicket_PriceField;
            }
            set
            {
                this.price_intTicket_PriceField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Price_intTicket_PriceSpecified
        {
            get
            {
                return this.price_intTicket_PriceFieldSpecified;
            }
            set
            {
                this.price_intTicket_PriceFieldSpecified = value;
            }
        }
        public string Price_strChild_Ticket
        {
            get
            {
                return this.price_strChild_TicketField;
            }
            set
            {
                this.price_strChild_TicketField = value;
            }
        }
        public string AreaCat_strCode1
        {
            get
            {
                return this.areaCat_strCode1Field;
            }
            set
            {
                this.areaCat_strCode1Field = value;
            }
        }
        public int AreaCat_intSeq
        {
            get
            {
                return this.areaCat_intSeqField;
            }
            set
            {
                this.areaCat_intSeqField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AreaCat_intSeqSpecified
        {
            get
            {
                return this.areaCat_intSeqFieldSpecified;
            }
            set
            {
                this.areaCat_intSeqFieldSpecified = value;
            }
        }
        public string Price_strTicket_Type_Description_2
        {
            get
            {
                return this.price_strTicket_Type_Description_2Field;
            }
            set
            {
                this.price_strTicket_Type_Description_2Field = value;
            }
        }
        public string Price_strPackage
        {
            get
            {
                return this.price_strPackageField;
            }
            set
            {
                this.price_strPackageField = value;
            }
        }
        public string TType_strAvailLoyaltyOnly
        {
            get
            {
                return this.tType_strAvailLoyaltyOnlyField;
            }
            set
            {
                this.tType_strAvailLoyaltyOnlyField = value;
            }
        }
        public string TType_strHOCode
        {
            get
            {
                return this.tType_strHOCodeField;
            }
            set
            {
                this.tType_strHOCodeField = value;
            }
        }
        public string Price_strRedemption
        {
            get
            {
                return this.price_strRedemptionField;
            }
            set
            {
                this.price_strRedemptionField = value;
            }
        }
        public string Price_strComp
        {
            get
            {
                return this.price_strCompField;
            }
            set
            {
                this.price_strCompField = value;
            }
        }
        public string TType_strSalesChannels
        {
            get
            {
                return this.tType_strSalesChannelsField;
            }
            set
            {
                this.tType_strSalesChannelsField = value;
            }
        }
        public string Price_strATMAvailable
        {
            get
            {
                return this.price_strATMAvailableField;
            }
            set
            {
                this.price_strATMAvailableField = value;
            }
        }
        public string TType_strShowOnPOS
        {
            get
            {
                return this.tType_strShowOnPOSField;
            }
            set
            {
                this.tType_strShowOnPOSField = value;
            }
        }
        public int Price_intSurcharge
        {
            get
            {
                return this.price_intSurchargeField;
            }
            set
            {
                this.price_intSurchargeField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Price_intSurchargeSpecified
        {
            get
            {
                return this.price_intSurchargeFieldSpecified;
            }
            set
            {
                this.price_intSurchargeFieldSpecified = value;
            }
        }
        public int TType_intBarcodeMaxRepeats
        {
            get
            {
                return this.tType_intBarcodeMaxRepeatsField;
            }
            set
            {
                this.tType_intBarcodeMaxRepeatsField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TType_intBarcodeMaxRepeatsSpecified
        {
            get
            {
                return this.tType_intBarcodeMaxRepeatsFieldSpecified;
            }
            set
            {
                this.tType_intBarcodeMaxRepeatsFieldSpecified = value;
            }
        }
        public string TType_strMaxRepeatsCycle
        {
            get
            {
                return this.tType_strMaxRepeatsCycleField;
            }
            set
            {
                this.tType_strMaxRepeatsCycleField = value;
            }
        }
        public string TType_strLongDescription
        {
            get
            {
                return this.tType_strLongDescriptionField;
            }
            set
            {
                this.tType_strLongDescriptionField = value;
            }
        }
        public string TType_strLongDescriptionAlt
        {
            get
            {
                return this.tType_strLongDescriptionAltField;
            }
            set
            {
                this.tType_strLongDescriptionAltField = value;
            }
        }
        public string TType_strMemberCard
        {
            get
            {
                return this.tType_strMemberCardField;
            }
            set
            {
                this.tType_strMemberCardField = value;
            }
        }
        public string TType_strAvailRecognitionOnly
        {
            get
            {
                return this.tType_strAvailRecognitionOnlyField;
            }
            set
            {
                this.tType_strAvailRecognitionOnlyField = value;
            }
        }
        public string HOPK
        {
            get
            {
                return this.hOPKField;
            }
            set
            {
                this.hOPKField = value;
            }
        }
        public string AreaCat_strDesc
        {
            get
            {
                return this.areaCat_strDescField;
            }
            set
            {
                this.areaCat_strDescField = value;
            }
        }
        public string AreaCat_strDescAlt
        {
            get
            {
                return this.areaCat_strDescAltField;
            }
            set
            {
                this.areaCat_strDescAltField = value;
            }
        }
        public string MMC_strName
        {
            get
            {
                return this.mMC_strNameField;
            }
            set
            {
                this.mMC_strNameField = value;
            }
        }
        public int Price_intSequence
        {
            get
            {
                return this.price_intSequenceField;
            }
            set
            {
                this.price_intSequenceField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Price_intSequenceSpecified
        {
            get
            {
                return this.price_intSequenceFieldSpecified;
            }
            set
            {
                this.price_intSequenceFieldSpecified = value;
            }
        }
        public string Price_strIsTicketPackage
        {
            get
            {
                return this.price_strIsTicketPackageField;
            }
            set
            {
                this.price_strIsTicketPackageField = value;
            }
        }
        public string LoyaltyRewardTicket
        {
            get
            {
                return this.loyaltyRewardTicketField;
            }
            set
            {
                this.loyaltyRewardTicketField = value;
            }
        }
        public string LoyaltyTicketName
        {
            get
            {
                return this.loyaltyTicketNameField;
            }
            set
            {
                this.loyaltyTicketNameField = value;
            }
        }
        public string LoyaltyTicketDescription
        {
            get
            {
                return this.loyaltyTicketDescriptionField;
            }
            set
            {
                this.loyaltyTicketDescriptionField = value;
            }
        }
        public string LoyaltyTicketMessageText
        {
            get
            {
                return this.loyaltyTicketMessageTextField;
            }
            set
            {
                this.loyaltyTicketMessageTextField = value;
            }
        }
        public string OrderTicketsBy
        {
            get
            {
                return this.orderTicketsByField;
            }
            set
            {
                this.orderTicketsByField = value;
            }
        }
        public string TicketCategory
        {
            get
            {
                return this.ticketCategoryField;
            }
            set
            {
                this.ticketCategoryField = value;
            }
        }
        public string Price_strLoyaltyRecognitionID
        {
            get
            {
                return this.price_strLoyaltyRecognitionIDField;
            }
            set
            {
                this.price_strLoyaltyRecognitionIDField = value;
            }
        }
        public string Price_intLoyaltyDisplayPrice
        {
            get
            {
                return this.price_intLoyaltyDisplayPriceField;
            }
            set
            {
                this.price_intLoyaltyDisplayPriceField = value;
            }
        }
        public string Price_intLoyaltyPriceCents
        {
            get
            {
                return this.price_intLoyaltyPriceCentsField;
            }
            set
            {
                this.price_intLoyaltyPriceCentsField = value;
            }
        }
        public string Price_intLoyaltyPointsCost
        {
            get
            {
                return this.price_intLoyaltyPointsCostField;
            }
            set
            {
                this.price_intLoyaltyPointsCostField = value;
            }
        }
        public string Price_intLoyaltyQtyAvailable
        {
            get
            {
                return this.price_intLoyaltyQtyAvailableField;
            }
            set
            {
                this.price_intLoyaltyQtyAvailableField = value;
            }
        }
        #endregion
        #region Methods
        public static List<TicketTypeList> GetTicketTypeList(string CinemaID, int SessionID)
        {
            DataService.GetTicketTypeListRequest req = new DataService.GetTicketTypeListRequest();
            req.CinemaId = CinemaID;
            req.SessionId = SessionID.ToString();
            req.OptionalReturnAllRedemptionAndCompTickets = true; //Type Tickets Redemption
            Interpret<TicketTypeList, DataService.GetTicketTypeListRequest> interpret = new Interpret<TicketTypeList, DataService.GetTicketTypeListRequest>();
            List<TicketTypeList> TicketTypes = interpret.Get("GetTicketTypeList", req);

            return TicketTypes;
        }
        #endregion
    }
}
