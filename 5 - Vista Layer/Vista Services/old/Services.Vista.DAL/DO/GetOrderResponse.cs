﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.TicketingService
{
    public partial class GetOrderResponse
    {
        public static GetOrderResponse GetOrder(string UserSessionID)
        {
            TicketingService Ticketing = new TicketingService();

            GetOrderRequest getOrderRequest = new GetOrderRequest();
            getOrderRequest.ProcessOrderValue = true;
            getOrderRequest.UserSessionId = UserSessionID;
            getOrderRequest.BookingMode = 0;
            getOrderRequest.OptionalClientId = "111.111.111.120";
            GetOrderResponse rp = Ticketing.GetOrder(getOrderRequest);

            return rp;
        }
    }
}
