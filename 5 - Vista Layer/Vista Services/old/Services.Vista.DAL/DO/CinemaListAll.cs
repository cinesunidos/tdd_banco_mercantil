﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class CinemaListAll
    {
        #region Properties
        public string Cinema_strID { get; set; }
        public string Cinema_strName { get; set; }
        public string Cinema_strPrint_x0040_Home { get; set; }
        public string Cinema_strAllBookingsUnpaid { get; set; }
        public string Cinema_strDisplayName { get; set; }
        public string Cinema_strAllowLoyalty { get; set; }
        public string Cinema_strAllowOnlineVoucherValidation { get; set; }
        public string Cinema_strAllowOnlineVoucherRedemption { get; set; }
        public string Cinema_strIsGiftStore { get; set; }
        public int Cinema_intTimeOffsetInMins { get; set; }
        public string Cinema_strIsGiftStore1 { get; set; }
        #endregion
        #region Methods
        public static List<CinemaListAll> GetCinemas()
        {

            DataService.GetCinemaListAllRequest rqc = new DataService.GetCinemaListAllRequest();
            Interpret<CinemaListAll, DataService.GetCinemaListAllRequest> interpret = new Interpret<CinemaListAll, DataService.GetCinemaListAllRequest>();

            List<CinemaListAll> Cinemas = new List<CinemaListAll>();
            Cinemas = interpret.Get("GetCinemaListAll", rqc);

            return Cinemas;
        }
        #endregion
    }
}
