﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.DAL.DO
{
    public class ShowtimeDateList
    {
        #region Fields
        private System.DateTime sessionDaysField;
        private bool sessionDaysFieldSpecified;
        #endregion
        #region Properties
        public System.DateTime SessionDays
        {
            get
            {
                return this.sessionDaysField;
            }
            set
            {
                this.sessionDaysField = value;
            }
        }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SessionDaysSpecified
        {
            get
            {
                return this.sessionDaysFieldSpecified;
            }
            set
            {
                this.sessionDaysFieldSpecified = value;
            }
        }
        #endregion

        public static List<ShowtimeDateList> GetShowtimeDateList()
        {
            DataService.GetShowtimeDateListRequest req = new DataService.GetShowtimeDateListRequest();
            req.OptionalBizStartHourOfDay = 0;

            Interpret<ShowtimeDateList, DataService.GetShowtimeDateListRequest> interpret = new Interpret<ShowtimeDateList, DataService.GetShowtimeDateListRequest>();
            List<ShowtimeDateList> ShowtimeDates = interpret.Get("GetShowtimeDateList", req);
            return ShowtimeDates;
        }
    }
}
