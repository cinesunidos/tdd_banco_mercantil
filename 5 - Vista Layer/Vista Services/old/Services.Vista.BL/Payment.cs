﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.DAL.TicketingService;
using Services.Vista.BO;
using System.ServiceModel;
using Services.Vista.DAL.BookingService;
using Services.Vista.DAL.DO;

namespace Services.Vista.BL
{
    public class Payment : Contract.IPayment
    {
        public PaymentResponse GetPaymentPremium(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo)
        {
            //CompanyID = CU: para Cines Unidos     PCA: para Parque Costa Azul
            #region VistaPayment
            GetOrderResponse OrderResponse = null;
            CompleteOrderResponse response = new CompleteOrderResponse();
            Services.Vista.DAL.TicketingService.PaymentInfo payInfo;
            int BookingNumber;
            try
            {
                if (!ValidatePaymentInfo(PaymentInfo)) throw new ControlledException("E0007");

                OrderResponse = GetOrder(UserSessionID);
                if (OrderResponse.Result != DAL.TicketingService.ResultCode.OK) throw new ControlledException("E0005");               

                payInfo = new Services.Vista.DAL.TicketingService.PaymentInfo();
                payInfo.CardNumber = PaymentInfo.CardNumber;
                payInfo.CardType = PaymentInfo.CardTypeStr;

                payInfo.PaymentValueCents = OrderResponse.Order.TotalValueCents;
                if (PaymentInfo.CardType != PaymentCardType.GIFTCARD)
                {
                    payInfo.CardCVC = PaymentInfo.CardCVV;
                    payInfo.CardExpiryMonth = PaymentInfo.CardExpiryMonth.ToString();
                    payInfo.CardExpiryYear = PaymentInfo.CardExpiryYear.ToString();
                    payInfo.PaymentTenderCategory = PaymentInfo.PaymentTenderCatStr;
                }
                else
                {
                    payInfo.CardType = "SVS";
                    payInfo.CardCVC = "111";
                    payInfo.CardExpiryMonth = "12";
                    payInfo.CardExpiryYear = "99";
                    payInfo.PaymentTenderCategory = PaymentTenderCategory.SVC.ToString();
                }
                response = CompleteOrderResponse.CompleteOrder(ClientID, UserSessionID, payInfo, PaymentInfo.Name, PaymentInfo.Email, PaymentInfo.Phone);
                if (response.Result != DAL.TicketingService.ResultCode.OK)
                {
                    throw new ControlledException("E0006");
                }

                BookingNumber = Int32.Parse(response.VistaBookingNumber);
            }
            catch (ControlledException ex)
            {                
                PaymentResponse paymentResponse = new PaymentResponse();
                paymentResponse.CodeResult = ex.Code;
                paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
                return paymentResponse;
            }
            catch (Exception ex)
            {
                PaymentResponse paymentResponse = new PaymentResponse();
                paymentResponse.CodeResult = "E0001";
                paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
                return paymentResponse;
            }
            #endregion 

            //Compra tradicional
            if (PaymentInfo.CardType != PaymentCardType.GIFTCARD)
            {
                #region MerchantPayment
                try
                {
                    string CompanyID = VISTAITCinema.getInfoTheater(OrderResponse.Order.CinemaId).Company;

                    Payment_Services.PaymentRequest paymentResquest = new Payment_Services.PaymentRequest();
                    paymentResquest.Amount = (decimal)OrderResponse.Order.TotalValueCents / 100;
                    paymentResquest.BookingNumber = BookingNumber;
                    paymentResquest.Cinema = OrderResponse.Order.CinemaId;
                    paymentResquest.ClientId = ClientID; //TODO: llenar este campo            
                    paymentResquest.CVV = PaymentInfo.CardCVV;
                    paymentResquest.ExpMonth = PaymentInfo.CardExpiryMonth;
                    paymentResquest.ExpYear = PaymentInfo.CardExpiryYear;
                    paymentResquest.Factura = OrderResponse.Order.VistaTransactionNumber;
                    paymentResquest.IDCard = PaymentInfo.IDCard;
                    paymentResquest.Name = PaymentInfo.Name;
                    paymentResquest.CardNumber = PaymentInfo.CardNumber;
                    paymentResquest.VistaOrderId = UserSessionID; //TODO: llenar este campo     
                    paymentResquest.CompanyId = CompanyID;
                    Payment_Services.Payment payment = new Payment_Services.Payment();
                    payment.ApproveFromParameters(paymentResquest);

                    PaymentResponse paymentResponse = new PaymentResponse();
                    paymentResponse.SetVoucher(payment.Voucher);
                    paymentResponse.BookingNumber = BookingNumber;
                    paymentResponse.CodeResult = payment.CodeResult;
                    paymentResponse.DescriptionResult = payment.DescriptionResult;

                    // Modificar mensajes
                    switch (payment.CodeResult)
                    {
                        case "05":
                            paymentResponse.DescriptionResult = "Rechazada";
                            break;
                        case "00":// = APROBADA 
                            paymentResponse.DescriptionResult = "Aprobada";
                            break;
                        case "01":// = LLAME AL EMISOR  
                            paymentResponse.DescriptionResult = "Llamar al banco";
                            break;
                        case "02":// = CEDULA INVALIDA 
                            paymentResponse.DescriptionResult = "Cédula invalida";
                            break;
                        case "03":// = COMERCIO INVALIDO 
                            paymentResponse.DescriptionResult = "Comercio Inválido";
                            break;
                        case "04":// = RETENGA Y LLAME  
                            paymentResponse.DescriptionResult = "Negada";
                            break;
                        case "08":// = HONOUR WITH IDENTIFICATION   
                            paymentResponse.DescriptionResult = "Emisor no responde";
                            break;
                        case "12":// = TRANS.INVALIDA   
                            paymentResponse.DescriptionResult = "Transacción Inválida";
                            break;
                        case "13":// = MONTO INVALIDO 
                            paymentResponse.DescriptionResult = "Monto Inválido";
                            break;
                        case "14":// = TARJETA INVALIDA 
                            paymentResponse.DescriptionResult = "Tarjeta inválida";
                            break;
                        case "19":// = REINTENTE TRANS 
                            paymentResponse.DescriptionResult = "Intente nuevamente";
                            break;
                        case "41":// = TARJETA EXTRAVIADA 
                            paymentResponse.DescriptionResult = "Tarjeta extraviada";
                            break;
                        case "43":// = TARJETA ROBADA 
                            paymentResponse.DescriptionResult = "Tarjeta extraviada";
                            break;
                        case "48":// = RESERVADO PARA ISO USA 
                            paymentResponse.DescriptionResult = "Imposible procesar";
                            break;
                        case "51":// = FONDO INSUFICIENTE 
                            paymentResponse.DescriptionResult = "Saldo insuficiente";
                            break;
                        case "54":// = TARJETA VENCIDA 
                            paymentResponse.DescriptionResult = "Tarjeta vencida";
                            break;
                        case "55":// = CLAVE INVALIDA 
                            paymentResponse.DescriptionResult = "Clave inválida";
                            break;
                        case "58":// = TRANS.NO PERMITIDA 
                            paymentResponse.DescriptionResult = "Transacción no permitida";
                            break;
                        case "61":// = EXC.MONTO RETIROS 
                            paymentResponse.DescriptionResult = "Excede límite diario";
                            break;
                        case "62":// = TARJ.RESTRINGIDA 
                            paymentResponse.DescriptionResult = "Tarjeta restringida";
                            break;
                        case "67":// = HARD CAPTURE(REQUIRES THAT CARD BE PICKED UP AT ATM)    
                            paymentResponse.DescriptionResult = "Negada";
                            break;
                        case "68":// = SIN RESP.DEL HOST   
                            paymentResponse.DescriptionResult = "Emisor no responde";
                            break;
                        case "75":// = EXC.CLAVE INVAL 
                            paymentResponse.DescriptionResult = "Tarjeta bloqueada";
                            break;
                        case "76":// = CUENTA INVALIDA* 
                            paymentResponse.DescriptionResult = "Cuenta inválida";
                            break;
                        case "77":// = CUENTA INVALIDA** 
                            paymentResponse.DescriptionResult = "Cuenta inválida";
                            break;
                        case "78":// = CUENTA INVALIDA 
                            paymentResponse.DescriptionResult = "Cuenta inválida";
                            break;
                        case "82":// = TARJETA INVALIDA* 
                            paymentResponse.DescriptionResult = "Tarjeta inválida";
                            break;
                        case "85":// = NO RECHAZADA 
                            paymentResponse.DescriptionResult = "Negada";
                            break;
                        case "86":// = CLAVE NO VERIF   
                            paymentResponse.DescriptionResult = "Negada";
                            break;
                        case "87":// = CONCIL.DETENIDA  
                            paymentResponse.DescriptionResult = "Negada";
                            break;
                        case "91":// = EMISOR INACTIVO 
                            paymentResponse.DescriptionResult = "Emisor no activo";
                            break;
                        case "92":// = SERVICIO NO DISP 
                            paymentResponse.DescriptionResult = "Emisor inválido";
                            break;
                        case "94":// = TRANS.DUPLICADA  
                            paymentResponse.DescriptionResult = "Negada";
                            break;
                        case "96":// = SISTEMA INACTIVO 
                            paymentResponse.DescriptionResult = "Emisor no activo";
                            break;
                    }

                    // Si hay error
                    if (payment.CodeResult != "00")
                    {
                        if (payment.CodeResult == string.Empty)
                        {
                            GenericException gex = new GenericException();
                            gex.Code = "E0002";
                            throw new FaultException<GenericException>(gex, new FaultReason(gex.Code));
                        }
                        else
                        {
                            Services.Vista.BL.Payment Payment = new Payment();
                            Payment.RefundBooking(OrderResponse.Order.CinemaId, BookingNumber, payInfo.PaymentValueCents);
                        }
                    }
                    else
                    {
                        //---------------------------------------------Mail ------------------------------------------------------//
                        // this.SendMail(PaymentInfo, paymentResponse);
                        //--------------------------------------------------------------------------------------------------------//
                    }
                    return paymentResponse;
                }
                catch (FaultException fex)
                {
                    Services.Vista.BL.Payment Payment = new Payment();
                    Payment.RefundBooking(OrderResponse.Order.CinemaId, BookingNumber, payInfo.PaymentValueCents);

                    PaymentResponse paymentResponse = new PaymentResponse();
                    paymentResponse.CodeResult = fex.Reason.ToString();
                    paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
                    return paymentResponse;
                }
                catch (Exception ex)
                {
                    Services.Vista.BL.Payment Payment = new Payment();
                    Payment.RefundBooking(OrderResponse.Order.CinemaId, BookingNumber, payInfo.PaymentValueCents);
                    PaymentResponse paymentResponse = new PaymentResponse();
                    paymentResponse.CodeResult = "E0002";
                    paymentResponse.DescriptionResult = Services.Vista.BL.Message.ResourceManager.GetString(paymentResponse.CodeResult);
                    return paymentResponse;
                }
                #endregion
            }
            //Compra con GiftCard
            else
            {
                PaymentResponse paymentResponse = new PaymentResponse();
                //paymentResponse.SetVoucher(payment.Voucher);
                paymentResponse.BookingNumber = BookingNumber;
                paymentResponse.CodeResult = "S0001";
                paymentResponse.DescriptionResult = Message.ResourceManager.GetString(paymentResponse.CodeResult);
                return paymentResponse;
            }
        }
        public PaymentResponse GetPayment(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Vista.BO.PaymentInfo PaymentInfo)
        {
            List<Services.Vista.DAL.TicketingService.TicketType> tts = new List<DAL.TicketingService.TicketType>();
            Live Live = new Live();
            Services.Vista.BO.SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);

            foreach (Services.Vista.BO.TicketType item in TicketTypes)
            {
                BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();
                Services.Vista.DAL.TicketingService.TicketType tt = new DAL.TicketingService.TicketType();
                tt.PriceInCents = (Int32)(Ticket.Price * 100);
                tt.Qty = item.Quantity;
                tt.TicketTypeCode = item.TicketTypeCode;
                tts.Add(tt);
            }
            AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());
            return this.GetPaymentPremium(ClientID, UserSessionID, PaymentInfo);
        }        
        public Boolean RefundBooking(string TheaterID, int BookingNumber, int ValueInCents)
        {
            RefundBookingResponse rp = RefundBookingResponse.GetRefundBooking(TheaterID, BookingNumber, ValueInCents);
            if (rp.ResultCode == 0)
                return true;
            else return false;
        }
        private GetOrderResponse GetOrder(string UserSessionID)
        {
            GetOrderResponse rp = GetOrderResponse.GetOrder(UserSessionID);
            if (rp.Result != DAL.TicketingService.ResultCode.OK) { }//TODO: new exception

            return rp;
        }
        private void SendMail(Vista.BO.PaymentInfo PaymentInfo, PaymentResponse paymentResponse)
        {
             try
                {
                    MailService.MailClient MailClient = new MailService.MailClient();
                    Dictionary<string, string> Dic = new Dictionary<string, string>();

                    if (PaymentInfo != null)
                    {
                        if (PaymentInfo.TransactionInfo != null)
                        {
                            Dic.Add("HallName", PaymentInfo.TransactionInfo.HallName);
                            Dic.Add("MovieName", PaymentInfo.TransactionInfo.MovieName);
                            Dic.Add("Seats", PaymentInfo.TransactionInfo.Seats);
                            Dic.Add("Date", PaymentInfo.TransactionInfo.Date);
                            Dic.Add("Time", PaymentInfo.TransactionInfo.Time);
                            Dic.Add("Count", PaymentInfo.TransactionInfo.Count.ToString());
                            Dic.Add("Amount", PaymentInfo.TransactionInfo.Amount.ToString("N2"));
                            Dic.Add("Charge", PaymentInfo.TransactionInfo.Charge.ToString("N2"));
                            Dic.Add("Tax", PaymentInfo.TransactionInfo.Tax.ToString("N2"));                            
                            Dic.Add("Total", PaymentInfo.TransactionInfo.Total.ToString("N2"));
                            Dic.Add("BookingNumber", paymentResponse.BookingNumber.ToString());
                            Dic.Add("Voucher", paymentResponse.VoucherHTML);
                        }                        
                    }

                    MailClient.Send("PurchaseConfirmation", Dic, PaymentInfo.Email);
                }
                catch (Exception ex) 
                {
                }
        }
        private Boolean ValidatePaymentInfo(Vista.BO.PaymentInfo PaymentInfo)
        {
            bool isOk = true;
            if (PaymentInfo != null)
            {
                if (PaymentInfo.CardNumber == null || PaymentInfo.CardNumber == string.Empty) isOk = false;
                if (PaymentInfo.CardType == null) isOk = false;
                if (PaymentInfo.CardType != PaymentCardType.GIFTCARD)
                {
                    if (PaymentInfo.CardCVV == null || PaymentInfo.CardCVV == string.Empty) isOk = false;
                    if (PaymentInfo.CardExpiryMonth == null || PaymentInfo.CardExpiryMonth == 0) isOk = false;
                    if (PaymentInfo.CardExpiryYear == null || PaymentInfo.CardExpiryYear == 0) isOk = false;
                    if (PaymentInfo.Email == null || PaymentInfo.Email == string.Empty) isOk = false;
                    if (PaymentInfo.IDCard == null || PaymentInfo.IDCard == string.Empty) isOk = false;
                    if (PaymentInfo.Name == null || PaymentInfo.Name == string.Empty) isOk = false;
                    if (PaymentInfo.PaymentTenderCategory == null) isOk = false;
                    if (PaymentInfo.Phone == null || PaymentInfo.Phone == string.Empty) isOk = false;
                }
            }
            else
            {
                isOk = false;
            }

            return isOk;
        }   
    }      
}
