﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.BO;
using System.ServiceModel;

namespace Services.Vista.BL.Contract
{
    [ServiceContract]
    public interface IData
    {
        [OperationContract]
        [FaultContract(typeof(GenericException))]
        List<Theater> GetTheaters(string CompanyID);   
             
        [OperationContract]
        [FaultContract(typeof(GenericException))]
        MoviePack GetAllMovies(string TheaterID);

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        MoviePack GetComingSoon();

        [OperationContract]
        [FaultContract(typeof(GenericException))]
        List<CityTicketPrice> GetTicketPrice(string date);
    }
}
