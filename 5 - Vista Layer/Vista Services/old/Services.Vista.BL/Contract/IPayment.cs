﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.BO;
using System.ServiceModel;

namespace Services.Vista.BL.Contract
{
    [ServiceContract]
    public interface IPayment
    {
        [OperationContract]
        [FaultContract(typeof(GenericException))]
        [FaultContract(typeof(PaymentException))]
        PaymentResponse GetPaymentPremium(string ClientID, string UserSessionID, Vista.BO.PaymentInfo PaymentInfo);
        [OperationContract]
        PaymentResponse GetPayment(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Vista.BO.PaymentInfo PaymentInfo);
        [OperationContract]
        Boolean RefundBooking(string TheaterID, int BookingNumber, int ValueInCents);
    }
}
