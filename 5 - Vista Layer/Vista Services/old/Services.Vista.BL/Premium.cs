﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Vista.BO;
using Services.Vista.DAL.DO;
using Services.Vista.DAL.TicketingService;

namespace Services.Vista.BL
{
    public class Premium : Contract.IPremium
    {
        public MapInformation GetNewOrder(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Boolean WithMap)
        {
            List<Services.Vista.DAL.TicketingService.TicketType> tts = new List<DAL.TicketingService.TicketType>();
            Live Live = new Live();
            Services.Vista.BO.SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);
            
            foreach (Services.Vista.BO.TicketType item in TicketTypes)
            {
                BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();
   
                Services.Vista.DAL.TicketingService.TicketType tt = new DAL.TicketingService.TicketType();
                tt.PriceInCents = (Int32)(Ticket.Price*100);
                tt.Qty = item.Quantity;
                if (item.TicketBarcode != null)
                {
                    tt.OptionalBarcode = item.TicketBarcode;
                }
                tt.TicketTypeCode = item.TicketTypeCode;
                tts.Add(tt);
            }
            AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());
            if (rp.Result != ResultCode.OK) { return new MapInformation(true, rp.Result.ToString()); }
            return MapInformation.GetTest(rp.SeatData,WithMap);
        }
        
        public string GetNewOrderSeatData(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, Boolean WithMap)
        {
            List<Services.Vista.DAL.TicketingService.TicketType> tts = new List<DAL.TicketingService.TicketType>();
            Live Live = new Live();
            Services.Vista.BO.SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);

            foreach (Services.Vista.BO.TicketType item in TicketTypes)
            {
                BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();

                Services.Vista.DAL.TicketingService.TicketType tt = new DAL.TicketingService.TicketType();
                tt.PriceInCents = (Int32)(Ticket.Price * 100);
                tt.Qty = item.Quantity;
                if (item.TicketBarcode != null)
                {
                    tt.OptionalBarcode = item.TicketBarcode;
                }
                tt.TicketTypeCode = item.TicketTypeCode;
                tts.Add(tt);
            }
            AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());
           
            return rp.SeatData.ToString();
        }

        //public Map GetNewOrderVoucher(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.TicketType> TicketTypes, List<string> Vouchers)
        //{
        //    List<Services.Vista.DAL.TicketingService.TicketType> tts = new List<DAL.TicketingService.TicketType>();
        //    Live Live = new Live();
        //    Services.Vista.BO.SessionInformation SessionInformation = Live.GetSessionInformation(TheaterID, SessionID);

        //    foreach (Services.Vista.BO.TicketType item in TicketTypes)
        //    {
        //        BO.Ticket Ticket = SessionInformation.Tickets.Where(t => t.Code == item.TicketTypeCode).SingleOrDefault();

        //        Services.Vista.DAL.TicketingService.TicketType tt = new DAL.TicketingService.TicketType();
        //        tt.PriceInCents = (Int32)(Ticket.Price * 100);
        //        tt.Qty = item.Quantity;
        //        tt.TicketTypeCode = item.TicketTypeCode;
        //        tts.Add(tt);
        //    }

        //    foreach (string item in Vouchers)
        //    {
        ////        Services.Vista.DAL.TicketingService.TicketType tt = new DAL.TicketingService.TicketType();
        ////        List<TicketTypeFromBarcode> TicketTypeFromBarcodes = TicketTypeFromBarcode.GetTicketTypeFromBarcode(item.Substring(0,5),TheaterID,SessionID);

        //       // tt.OptionalBarcodePin = item.Substring(0, 5);
        //        tt.OptionalBarcode = item;
        //        tt.TicketTypeCode = TicketTypeFromBarcodes[0].TType_strCode;
        //        tt.PriceInCents = TicketTypeFromBarcodes[0].Price_intSurcharge;
        //        tt.Qty = 1;
        //        tts.Add(tt);
        //    }
        //    AddTicketsResponse rp = AddTicketsResponse.AddTicket(ClientID, UserSessionID, TheaterID, SessionID.ToString(), tts.ToArray());
        //    return Map.Get(rp.SeatData);
        //}
        public Boolean ChangeSeats(string ClientID, string UserSessionID, string TheaterID, int SessionID, List<Services.Vista.BO.Seat> Seats)
        {
            List<Services.Vista.DAL.TicketingService.SelectedSeat> SelectedSeats = new List<SelectedSeat>();
            foreach (Services.Vista.BO.Seat item in Seats)
            {
                Services.Vista.DAL.TicketingService.SelectedSeat selectedSeat = new SelectedSeat();
                selectedSeat.AreaCategoryCode = item.AreaCategoryCode;
                selectedSeat.AreaNumber = item.AreaNumber;
                selectedSeat.ColumnIndex = item.ColumnIndex;
                selectedSeat.RowIndex = item.RowIndex;
               
                SelectedSeats.Add(selectedSeat);
            }

            Services.Vista.DAL.TicketingService.SetSelectedSeatsResponse rp = SetSelectedSeatsResponse.SetSelectedSeats(ClientID, UserSessionID, TheaterID, SessionID.ToString(), SelectedSeats.ToArray());

            if (rp.Result == ResultCode.OK)
                return true;
            else return false;
        }
        public MapInformation GetMap(string ClientID, string UserSessionID, int SessionID, string TheaterID, Boolean WithMap)
        {
            Services.Vista.DAL.TicketingService.GetSessionSeatDataResponse rp = GetSessionSeatDataResponse.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);
            if (rp.Result != ResultCode.OK) { return new MapInformation(true, rp.Result.ToString()); }
            return MapInformation.GetTest(rp.SeatData, WithMap);                           
        }

        public string GetMapMobile(string ClientID, string UserSessionID, int SessionID, string TheaterID)
        {
            Services.Vista.DAL.TicketingService.GetSessionSeatDataResponse rp = GetSessionSeatDataResponse.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);
            return rp.SeatData.ToString();
        }

        //public string GetMapSeatsAvailable(string ClientID, string UserSessionID, int SessionID, string TheaterID)
        //{
        //    Services.Vista.DAL.TicketingService.GetSessionSeatDataResponse rp = GetSessionSeatDataResponse.GetSessionSeatData(ClientID, UserSessionID, SessionID.ToString(), TheaterID);

        //    Services.Vista.DAL.TicketingService.Theatre theatre = rp.SeatLayoutData;
        //    Map map = new Map(theatre.Areas[0].Rows.Count(), theatre.Areas[0].ColumnCount);
        //    map.Tiers = map.Tiers.OrderByDescending(ma => ma.Name).ToList();

        //    string MapSeatsAvailable =  Map.GetMapSeatsAvailable(rp.SeatData);            
        //    return MapSeatsAvailable;           
        //}
        public Boolean CancelOrder(string ClientID, string UserSessionID)
        {
            return Services.Vista.DAL.DO.CancelOrder.GetCancelOrder(ClientID, UserSessionID);
        }
    }
}
