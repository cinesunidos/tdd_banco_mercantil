﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Vista.BO;
using Services.Vista.DAL.DO;
using Services.Vista.DAL;
using System.ServiceModel;
using Services.Vista.BL.Contract;
using Services.Vista.DAL.DataService;
using System.Xml.Linq;
using System.Configuration;

namespace Services.Vista.BL
{

    public class Live : ILive
    {
        public SessionInformation GetSessionInformation(string TheaterID, int SessionID)
        {
            try
            {
               // Statistic statistic = new Statistic("GetTicketTypeList CALL", DateTime.Now);               //todo: comentar
                List<TicketTypeList> ticketTypeLists = TicketTypeList.GetTicketTypeList(TheaterID, SessionID);
                //statistic.EndDate = DateTime.Now;                                                           //todo: comentar
                //SessionInformation.Statistics.Add(statistic);                                               //todo: comentar

                //statistic = new Statistic("GetSessionInfo CALL", DateTime.Now);                             //todo: comentar
                SessionInfo SessionInfo = SessionInfo.GetSessionInfo(TheaterID, SessionID);
                //statistic.EndDate = DateTime.Now;                                                           //todo: comentar
                //SessionInformation.Statistics.Add(statistic);                                               //todo: comentar

                //statistic = new Statistic("ParseTax CALL", DateTime.Now);                                   //todo: comentar
                decimal Tax = decimal.Parse(getValueFromListString("Tax", TheaterID));
                //statistic.EndDate = DateTime.Now;                                                           //todo: comentar
                //SessionInformation.Statistics.Add(statistic);                                               //todo: comentar

                //statistic = new Statistic("Rest of CALL", DateTime.Now);                                    //todo: comentar
                
                List<Ticket> Tickets = new List<Ticket>();
                foreach (TicketTypeList item in ticketTypeLists)
                {
                    if (item.AvailableOnSalesChannel == 1 
                        && item.TType_strSalesChannels.Contains("WWW")
                        && ValidateChildrenWithSession(SessionInfo.Session_strChild_Allowed, item.Price_strChild_Ticket))
                    {

                        VISTAHOBookingFee BookingFee = VISTAHOBookingFee.GetBookingFee(); // Viene del web.config incluye IVA
                        VISTAHOService Service = VISTAHOService.GetService(item.Price_strTicket_Type_Code, Tax, SessionID, TheaterID);

                        VISTAHOService promotion = VISTAHOService.GetPromotion(item.Price_strTicket_Type_Code, Tax, SessionID, TheaterID);

                        Ticket ticket = CalculateTicketInfo(item, BookingFee, Service, Tax, promotion);

                        Tickets.Add(ticket);
                    }
                }

                SessionInformation sessionInformation = new SessionInformation();
                sessionInformation.TheaterID = SessionInfo.Cinema_strID;
                sessionInformation.MovieName = SessionInfo.Movie_strName;
                sessionInformation.MovieID = SessionInfo.HOPK;
                sessionInformation.SeatAllocation = SessionInfo.Session_strSeatAllocation_On == "Y" ? true : false;
                sessionInformation.SeatsAvailable = SessionInfo.Session_decSeats_Available;
                sessionInformation.ShowTime = SessionInfo.Session_dtmDate_Time;
                sessionInformation.Tickets = Tickets;
                sessionInformation.MaxSeatsPerTransaction = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["MaxSeatsPerTransaction"][0].ToString());
                sessionInformation.HallName = SessionInfo.Screen_strName;
                sessionInformation.OnSale = SessionInfo.Session_strSalesChannels.Contains(";WWW;");
                //statistic.EndDate = DateTime.Now;                                                           //todo: comentar
                //SessionInformation.Statistics.Add(statistic);                                               //todo: comentar       
                return sessionInformation;  
            }
            catch (Exception ex)
            {
                GenericException gex = new GenericException();
                gex.Code = "E0001";
                gex.Message = Services.Vista.BL.Message.ResourceManager.GetString(gex.Code);
                gex.Title = Services.Vista.BL.Title.ResourceManager.GetString(gex.Code);
                throw new FaultException<GenericException>(gex, new FaultReason(string.Format("{0}-{1}", gex.Code, gex.Message)));
            }
        }

        /// <summary>
        /// Contiene la lógica para calcular los montos del ticket
        /// </summary>
        /// <param name="ticketType">Tipo de Ticket</param>
        /// <param name="bookingFee">Información del Booking Fee</param>
        /// <param name="service">Información del Service Fee desde Vista</param>
        /// <param name="theaterTaxRate">Tasa de Impuesto del Cine</param>
        /// <returns></returns>
        private Ticket CalculateTicketInfo(TicketTypeList ticketType, VISTAHOBookingFee bookingFee, VISTAHOService service, decimal theaterTaxRate, VISTAHOService promotion)
        {
            Ticket ticket = new Ticket();

            ticket.Code = ticketType.Price_strTicket_Type_Code;
            ticket.Name = ticketType.Price_strTicket_Type_Description;
            ticket.DescriptionTicketCategory = ticketType.TicketCategory.ToString();

            ticket.Price = ((decimal)ticketType.Price_intTicket_Price / 100 - service.Total - promotion.Total); // Se le restan los cargos por servicios

            ticket.ServiceFee = service.Price; // Sin IVA
            ticket.TotalServiceFee = service.Total; // Con IVA

            ticket.PromotionFee = promotion.Price; // Sin IVA
            ticket.TotalPromotionFee = promotion.Total; // Con IVA
            ticket.PromotionTax = promotion.Tax; // IVA

            ticket.BookingFee = bookingFee.Value / (1 + (theaterTaxRate / 100)); // Viene con IVA desde Vista, se resta el IVA

            //IM: Añadido/Modificado 2016-06-28
            decimal basePriceTax = GetCalculatedTaxTicket(ticket.Price, theaterTaxRate);
            ticket.BasePrice = ticket.Price - basePriceTax;

            // CalculateTax ya toma en cuenta las unidades tributarias
            // GP: Modificado 2016-11-02
            ticket.BookingTax = (bookingFee.Value - ticket.BookingFee);
            ticket.ServiceTax = service.Tax;

            ticket.Tax = CalculateTax(ticket.BookingTax, ticket.ServiceTax, basePriceTax, promotion.Tax);
            ticket.FullPrice = ticket.BasePrice + ticket.BookingFee + ticket.ServiceFee + ticket.Tax + ticket.PromotionFee; 

            //GP: Añadido 2016-11-01
            ticket.TicketTax = basePriceTax;
            return ticket;
        }

        public SIReduced GetSeatsAvailables(string TheaterID, int SessionID)
        {
            try
            {
                SessionInfo SessionInfo = SessionInfo.GetSessionInfo(TheaterID, SessionID);
                if (SessionInfo == null) throw new ControlledException("E0008");
                SIReduced SIReduced = new SIReduced();
                SIReduced.SeatsAvailable = SessionInfo.Session_decSeats_Available;
                SIReduced.OnSale = SessionInfo.Session_strSalesChannels.Contains(";WWW;");
                return SIReduced;
            }
            catch (ControlledException ex)
            {   
                GenericException gex = new GenericException();
                gex.Code = ex.Code;
                gex.Message = Services.Vista.BL.Message.ResourceManager.GetString(gex.Code);
                gex.Title = Services.Vista.BL.Title.ResourceManager.GetString(gex.Code);
                throw new FaultException<GenericException>(gex, new FaultReason(string.Format("{0}-{1}", gex.Code, gex.Message)));
            }
            catch (Exception ex)
            {            
                GenericException gex = new GenericException();
                gex.Code = "E0001";
                gex.Message = Services.Vista.BL.Message.ResourceManager.GetString(gex.Code);
                gex.Title = Services.Vista.BL.Title.ResourceManager.GetString(gex.Code);
                throw new FaultException<GenericException>(gex, new FaultReason(string.Format("{0}-{1}", gex.Code, gex.Message)));
            }
        }

        //Summary: Busca si el barcode del voucher esta configurado en el cine retornando descrpcion, cargos, entre otros
        public Voucher GetTicketTypeBarcode(string Barcode, string CinemaID, string SessionID)
        {
            //todo: por hacer Validar el negocio 
            //todo: Se tiene que tomar la decision si se quiere validar el voucher una vez ingresados si es asi entonces
            // se tiene que usar el addtickets que se usan en la clase Premium donde busque el GetTicketTypeFromBarcode
            // y se manda a agregar el ticket
            try
            {
                List<TicketTypeFromBarcode> ttbarcode = TicketTypeFromBarcode.GetTicketTypeFromBarcode(Barcode, CinemaID, SessionID);
                Voucher voucher = new Voucher();
                
                if (ttbarcode.Count == 0)
                {
                    voucher.IsValid = false;
                    voucher.CodeRes = "01";
                    voucher.CodeResDesc = "El cine no tiene Vouchers Configurados";
                }
                else 
                {
                    voucher.IsValid = true;
                    voucher.CodeRes = "00";
                    voucher.CodeResDesc = "Exitoso";
                    foreach (TicketTypeFromBarcode item in ttbarcode)
                    {
                        TicketTypeBarcode ttbar = new TicketTypeBarcode();
                        ttbar.AreaCat_intSeq = item.AreaCat_intSeq;
                        ttbar.AreaCat_strSeatAllocationOn = item.AreaCat_strSeatAllocationOn;
                        ttbar.AvailableOnSalesChannel = item.AvailableOnSalesChannel;
                        ttbar.Price_decimalSurcharge = (decimal)item.Price_intSurcharge / (decimal)100;
                        ttbar.Price_decimalTicket_Price = (decimal)item.Price_intTicket_Price/ (decimal) 100;
                        ttbar.Price_strTicket_Type_Description = item.Price_strTicket_Type_Description;
                        ttbar.Price_strTicket_Type_Description_2 = item.Price_strTicket_Type_Description_2;
                        ttbar.TType_strAvailLoyaltyOnly = item.TType_strAvailLoyaltyOnly;
                        ttbar.TType_strCode = item.TType_strCode;
                        voucher.ttbarcode.Add(ttbar);
                    }
                }
                return voucher;
               
            }
            catch (Exception ex)
            {
                GenericException gex = new GenericException();
                gex.Code = "E0001";
                gex.Message = Services.Vista.BL.Message.ResourceManager.GetString(gex.Code);
                gex.Title = Services.Vista.BL.Title.ResourceManager.GetString(gex.Code);
                throw new FaultException<GenericException>(gex, new FaultReason(string.Format("{0}-{1}", gex.Code, gex.Message)));
            }           
        }

        /// <summary>
        /// Método que devuelve el objeto giftcard con el balance disponible
        /// En caso de no ser una giftcard válida la propiedad IsOk será false
        /// </summary>
        /// <param name="Giftcard">El objeto Giftcard que tendrá la info de la tarjeta</param>
        /// <param name="CinemaId">el código del cine</param>
        /// <param name="SessionId">Session id del invitado</param>
        /// <returns></returns>
        public GiftCard GetGiftCardBalance(GiftCard GiftCard, string CinemaId, string SessionId)
        {
            GiftCard = GiftCardDAL.GetGiftCardBalance(GiftCard);
            //Validación de Saldo
            if (GiftCard.Balance <= 0 && GiftCard.IsOk)
            {
                GiftCard.IsOk = false;
                GiftCard.Messages = new List<string> { "La GiftCard no tiene saldo disponible" };
            }
            return GiftCard;
        }

        private string getValueFromListString(String key, String code)
        {
           List<String> items = System.Configuration.ConfigurationManager.AppSettings[key].Split(';').ToList();
           String value = "";
           value = items.Where(e=> e.Contains(code)).SingleOrDefault();
           if (value != string.Empty) value = value.Split(':')[1];
           //foreach (String item in items)
           //{
           //    if (item.Contains(code))
           //    {                   
           //        value = item.Split(':')[1];
           //        break;
           //    }
           //}
           return value;
        }

        /// <summary>
        /// Método que cálcula el impuesto del iva sí y solo si, supera las 2 unidades tributaria
        /// </summary>
        /// <param name="price">el precio completo de la entrada</param>
        /// <param name="tax">la tasa de impuesto del teatro</param>
        /// <returns></returns>
        private decimal GetCalculatedTaxTicket(decimal price, decimal tax)
        {
            decimal taxUnit = Decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["TaxUnit"].ToString());
            if (price > taxUnit)
            {
                return price - (price / (1 + (tax / 100)));
            }
            return 0;
        }

        /// <summary>
        /// Método que calcula el impuesto total del boleto
        /// </summary>
        /// <param name="BookingFeeTax"></param>
        /// <param name="ServiceTax"></param>
        /// <param name="TicketTax"></param>
        /// <returns></returns>
        private decimal CalculateTax(decimal BookingFeeTax, decimal ServiceTax, decimal TicketTax, decimal PromotionTax)
        {
            decimal TotalTax = TicketTax + BookingFeeTax + ServiceTax + PromotionTax;
            return TotalTax;
        }

        /// <summary>
        /// Método que índica si la película puede tener un ticket válido para niños
        /// </summary>
        /// <param name="ChildAllowedSession">Parámetro que dice que si es o no una función que permite niños: "Y" / "N"</param>
        /// <param name="IsTicketChild">Parámetro que dice que si el ticket es de niño: "Y" / "N"</param>
        /// <returns></returns>
        private bool ValidateChildrenWithSession(string ChildAllowedSession, string IsTicketChild)
        {
            if (ChildAllowedSession == "N" && IsTicketChild == "Y")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}