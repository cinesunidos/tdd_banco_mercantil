﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Vista.BL
{
    public class GenericException
    {
        #region Fields
        private string m_Code;        
        private string m_Message;        
        private string m_Title;
        #endregion
        #region Properties
        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }
        public string Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }
        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }
        #endregion        
    }
    public class PaymentException
    {
        #region Fields
        private List<string> m_Voucher;
        private string m_CodeResult;
        private string m_DescriptionResult;
        #endregion
        #region Properties
        public List<string> Voucher
        {
            get { return m_Voucher; }
            set { m_Voucher = value; }
        }
        public string CodeResult
        {
            get { return m_CodeResult; }
            set { m_CodeResult = value; }
        }
        public string DescriptionResult
        {
            get { return m_DescriptionResult; }
            set { m_DescriptionResult = value; }
        }
        #endregion
    }
    public class ControlledException:Exception
    {
        public ControlledException() { }
        public ControlledException(string Code)
        {
            this.Code = Code;
        }
        public ControlledException(string Code, string Message)
        {
            this.Code = Code;
            this.Message = Message;
        }
        #region Fields
        private string m_Code;
        private string m_Message;
        #endregion
        #region Properties
        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }
        public string Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }
        #endregion
    }
}
