﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Services.Mail.Worker
{
    public class Log
    {
        public string sSource { get; set; }
        public string sLog { get; set; }        
                
        public Log()
        {
            sSource = "Windows Service Mail Worker";
            sLog = "Application";
        }
        public Log(string Source, string Log, string Event)
        {
            sSource = Source;
            sLog = Log;            
        }
        public void Write(string Message)
        {
            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);
            EventLog.WriteEntry(sSource, Message);            
        }
    }
}
