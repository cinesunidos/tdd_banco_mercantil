﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Services.Mail.BO;
using System.Timers;

namespace Services.Mail.Worker
{
    public partial class Service : ServiceBase
    {
        Timer Timer;
        Boolean Semaphore;

        public Service()
        {
            InitializeComponent();
            Timer = new Timer();
            Timer.Elapsed += new ElapsedEventHandler(ti_Elapsed);
            Timer.Enabled = true;
            Timer.Interval = 10000;
            Semaphore = false;
        }

        void ti_Elapsed(object sender, ElapsedEventArgs e)
        {   
            if (Semaphore == false)
            {
                Timer.Enabled = false;
                Semaphore = true;
                try
                {
                    Outbox Outbox = new Outbox();
                    Outbox.Read();
                    foreach (Outbox item in Outbox.Collection)
                    {
                        Boolean isSent = Mail.SendMail(item.Body, item.ToEmail, item.Subject);

                        if (isSent == false && item.Attempts > 0)
                        {
                            item.Attempts = item.Attempts - 1;
                            item.CreationDate = DateTime.Now;
                            item.Update();
                        }
                        else
                        {
                            item.Delete();
                        }
                    }
                }
                catch (Exception ex) { };
                Semaphore = false;
                Timer.Enabled = true;
            }
                      
        }
        protected override void OnStart(string[] args)
        {
            Timer.Start();            
        }
        protected override void OnStop()
        {
            Timer.Stop();
        }
        protected override void OnPause()
        {
            Timer.Stop();
        }
        protected override void OnContinue()
        {
            Timer.Start();
        }
      
    }
}
