﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Services.Mail.BO;

namespace Services.Mail.DAL
{
    public class ParameterDAL:EntityDAL<Parameter>
    {
         public ParameterDAL()
            : base() { }
         public ParameterDAL(string ConnectionName)
            : base(ConnectionName) { }

        #region Methods
        public void Read()
        {
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            connection.Open();
            string Query = @"SELECT  [ID]
                                    ,[Name]
                                    ,[Format]
                                    ,[Mandatory]
                                    ,[TypeID]
                                FROM [Parameter]";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);

            List<Parameter> Parameters = new List<Parameter>();
            
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    Parameter Parameter = new Parameter();
                    Parameter.ID = dataReader.GetGuid(0);
                    Parameter.Name = dataReader.GetString(1);
                    Parameter.Format = dataReader.GetString(2);
                    Parameter.Mandatory = dataReader.GetBoolean(3);
                    Parameter.MailType.ID = dataReader.GetGuid(4);                    
                    Parameters.Add(Parameter);
                }
                dataReader.Close();
            }
            connection.Close();
            Collection = Parameters;
        }
        #endregion
    }
}
