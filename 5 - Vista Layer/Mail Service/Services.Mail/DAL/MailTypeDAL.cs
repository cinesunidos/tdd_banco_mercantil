﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Services.Mail.BO;


namespace Services.Mail.DAL
{
    public class MailTypeDAL : EntityDAL<MailType>
    {
        public MailTypeDAL()
            : base() { }
        public MailTypeDAL(string ConnectionName)
            : base(ConnectionName) { }

        #region Methods
        public void Create(MailType MailType)
        {
            String Query;
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("ID", MailType.ID);
            parameters.Add("Name", MailType.Name);
            parameters.Add("Template", MailType.Template);
            parameters.Add("Subject", MailType.Subject);
            parameters.Add("Attempts", MailType.Attempts);
            

            connection.Open();

            Query = @"INSERT INTO [Type]
                           ([ID]
                           ,[Name]
                           ,[Template]
                           ,[Subject]
                           ,[Attempts])
                     VALUES
                           (@ID
                           ,@Name
                           ,@Template
                           ,@Subject
                           ,@Attempts)";

            int rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected == -1) { }; //TODO: Lanzar excepción


            foreach (Parameter par in MailType.Parameters)
            {
                parameters = new Dictionary<string, object>();

                parameters.Add("ID", par.ID);
                parameters.Add("Name", par.Name);
                parameters.Add("Format", par.Format);
                parameters.Add("Mandatory", par.Mandatory);
                parameters.Add("TypeID", MailType.ID);

                Query = @"INSERT INTO [Parameter]
                                       ([ID]
                                       ,[Name]
                                       ,[Format]
                                       ,[Mandatory]
                                       ,[TypeID])
                                 VALUES
                                       (@ID
                                       ,@Name
                                       ,@Format
                                       ,@Mandatory
		                               ,@TypeID)";

                rowsAffected = connection.Execute(Query, parameters);
                if (rowsAffected == -1) { }; //TODO: Lanzar excepción
            }
            
                      
            connection.Close();
        }
        public void Read()
        {
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();            

            connection.Open();
            string Query = @"SELECT [ID], [Name], [Template], [Subject] ,[Attempts]
                                    FROM [Type]";
            SqlDataReader dataReader = connection.ExecuteRead(Query,parameters);           
            List<MailType> MailTypes = new List<MailType>();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    MailType MailType = new MailType();
                    MailType.ID = dataReader.GetGuid(0);
                    MailType.Name = dataReader.GetString(1);
                    MailType.Template = dataReader.GetString(2);
                    MailType.Subject = dataReader.GetString(3);
                    MailType.Attempts = dataReader.GetInt32(4);                    
                    MailTypes.Add(MailType);
                }
                dataReader.Close();
            }
            this.Collection = MailTypes;
        }
        public void Read(string Name)
        {            
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("Name", Name);

            connection.Open();
            string Query = @"SELECT [ID], [Name], [Template], [Subject] ,[Attempts]
                                    FROM [Type]
                                    WHERE Name = @Name";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);           
             List<MailType> MailTypes = new List<MailType>();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    MailType MailType = new MailType();
                    MailType.ID = dataReader.GetGuid(0);
                    MailType.Name = dataReader.GetString(1);
                    MailType.Template = dataReader.GetString(2);
                    MailType.Subject = dataReader.GetString(3);                    
                    MailType.Attempts = dataReader.GetInt32(4);         
                    MailTypes.Add(MailType);
                }
                dataReader.Close();
            }
            connection.Close();

           this.Collection = MailTypes;
        }        
        #endregion
    }
}
