﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Net.Mail;
using Services.Mail.BL.Contract;
using Services.Mail.BO;

namespace Services.Mail.BL
{    
    public class Mail : IMail
    {
        public void Send(string MailTypeName, Dictionary<string, string> Dic, string UserMail)
        {
            try
            {
                MailType MailType = new MailType();
                MailType.Read(MailTypeName);
                if (CheckParameters(MailType.Parameters,Dic) == true)
                {
                    string MailString = MailType.Template;
                    string SubjectString = MailType.Subject;
                    foreach (KeyValuePair<string, string> item in Dic)
                    {
                        MailString = MailString.Replace(string.Format("@{0}", item.Key), item.Value.ToString());
                        SubjectString = SubjectString.Replace(string.Format("@{0}", item.Key), item.Value.ToString());
                    }

                    Outbox Outbox = new Outbox();
                    Outbox.Attempts = MailType.Attempts;
                    Outbox.Body = MailString;
                    Outbox.CreationDate = DateTime.Now;
                    Outbox.ID = Guid.NewGuid();
                    Outbox.Subject = SubjectString;
                    Outbox.ToEmail = UserMail;
                    Outbox.Create();
                }
                else 
                {
                    Log log = new Log();
                    log.Write("Error en comprobación de parametros. Método Send() del servicio Mail");
                } 
            }
            catch (Exception ex)
            {
                Log log = new Log();
                log.Write(ex.Message);
            }
            
        }
        public void CreateMailType(string Name, string Subject, string Template, List<Parameter> Parameters)
        {
            MailType mailType = new MailType();
            mailType.ID = Guid.NewGuid();
            mailType.Name = Name;
            mailType.Parameters = Parameters;
            mailType.Subject = Subject;
            mailType.Template = Template;
            mailType.Create();
        }

        private Boolean CheckParameters(List<Parameter> parameters, Dictionary<string,string> dic)
        {
            Boolean isGood = true;
            if (parameters.Count > 0)
            {
                foreach (Parameter par in parameters)
                {
                    if (par.Mandatory == true)
                    {
                        int cant = dic.Where(d => d.Key.Equals(par.Name)).Count();
                        if (cant <= 0) isGood = false;
                    }
                }
            }
            return isGood;
        }     
    }
}
