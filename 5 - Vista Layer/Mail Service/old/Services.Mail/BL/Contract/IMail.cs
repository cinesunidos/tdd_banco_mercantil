﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Services.Mail.BO;

namespace Services.Mail.BL.Contract
{
    [ServiceContract]
    public interface IMail 
    {
        [OperationContract]
        void Send(string MailType,Dictionary<string, string> Dic, string UserMail);
        [OperationContract]
        void CreateMailType(string Name, string Subject, string Template, List<Parameter> Parameters);       
    }
}
