﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Mail.BO;
using System.Data.SqlClient;

namespace Services.Mail.DAL
{
    public class OutboxDAL : EntityDAL<Outbox>
    {
        public OutboxDAL()
            : base() { }
        public OutboxDAL(string ConnectionName)
            : base(ConnectionName) { }

        #region Methods
        public void Create(Outbox Outbox)
        {
            String Query;
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();                       
            parameters.Add("ID", Outbox.ID);
            parameters.Add("ToEmail", Outbox.ToEmail);
            parameters.Add("Body", Outbox.Body);
            parameters.Add("Subject", Outbox.Subject);
            parameters.Add("Attempts", Outbox.Attempts);
            parameters.Add("CreationDate", Outbox.CreationDate);
            
            connection.Open();
            Query = @"INSERT INTO [Outbox]([ID], [ToEmail], [Body], [Subject],[Attempts],[CreationDate])
                             VALUES (@ID, @ToEmail, @Body, @Subject,@Attempts,@CreationDate)";

            int rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected == 0) { };//TODO: Lanzar excepción
            connection.Close();
        }
        public void Update(Outbox Outbox)
        {            
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("ID", Outbox.ID);
            parameters.Add("ToEmail", Outbox.ToEmail);
            parameters.Add("Body", Outbox.Body);
            parameters.Add("Subject", Outbox.Subject);
            parameters.Add("Attempts", Outbox.Attempts);
            parameters.Add("CreationDate", Outbox.CreationDate);

            connection.Open();
            
            string Query = @"UPDATE [Outbox]
                           SET [ToEmail] = @ToEmail
                              ,[Body] = @Body
                              ,[Subject] = @Subject     
                              ,[Attempts] = @Attempts
                              ,[CreationDate] = @CreationDate                         
                         WHERE [ID] = @ID";
            int rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected == 0) { }; //TODO: Lanzar Excepción
            connection.Close();
        }
        public void Delete(Outbox Outbox)
        {
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("ID", Outbox.ID);

            connection.Open();

            string Query = @"DELETE FROM [Outbox]                         
                             WHERE [ID] = @ID";
            int rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected == 0) { }; //TODO: Lanzar Excepción
            connection.Close();
        }
        public void Read()
        {            
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            
            connection.Open();
            string Query = @"SELECT [ID], [ToEmail], [Body], [Subject], [Attempts] , [CreationDate]
                               FROM [Outbox]
                               ORDER BY [CreationDate]";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);

            List<Outbox> Outboxs = new List<Outbox>();            
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    Outbox Outbox = new Outbox();
                    Outbox.ID = dataReader.GetGuid(0);
                    Outbox.ToEmail = dataReader.GetString(1);
                    Outbox.Body = dataReader.GetString(2);
                    Outbox.Subject = dataReader.GetString(3);
                    Outbox.Attempts = dataReader.GetInt32(4);
                    Outbox.CreationDate = dataReader.GetDateTime(5);
                    Outboxs.Add(Outbox);
                }
                dataReader.Close();
            }
            connection.Close();
            this.Collection = Outboxs;
        }
        public void Read(Guid ID)
        {            
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("ID", ID);

            connection.Open();
            string Query = @"SELECT [ID], [ToEmail], [Body], [Subject],[Attempts],[CreationDate]
                               FROM [Outbox]
                              WHERE [ID] = @ID";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);

            List<Outbox> Outboxs = new List<Outbox>();
           
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    Outbox Outbox = new Outbox();
                    Outbox.ID = dataReader.GetGuid(0);
                    Outbox.ToEmail = dataReader.GetString(1);
                    Outbox.Body = dataReader.GetString(2);
                    Outbox.Subject = dataReader.GetString(3); 
                    Outbox.Attempts = dataReader.GetInt32(4);
                    Outbox.CreationDate = dataReader.GetDateTime(5);
                    Outboxs.Add(Outbox);
                }
                dataReader.Close();
            }
            this.Collection = Outboxs;
        }
        #endregion
    }
}
