﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Services.Mail.DAL
{
    public class EntityDAL<T>
    {
        public EntityDAL()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        }
        public EntityDAL(string ConnectionName)
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;
        }
        #region Fields
        private string m_ConnectionString;
        private List<T> m_Collection;        
        #endregion
        #region Properties
        public string ConnectionString
        {
            get { return m_ConnectionString; }
            set { m_ConnectionString = value; }
        }
        public List<T> Collection
        {
            get { return m_Collection; }
            set { m_Collection = value; }
        }
        #endregion
        
    }
}
