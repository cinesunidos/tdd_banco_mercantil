﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Mail.BO
{
    public class Parameter:Entity<Parameter>
    {
        public Parameter()
        {
            MailType = new MailType();
        }
        #region Fields
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Format { get; set; }
        public Boolean Mandatory { get; set; }
        public MailType MailType { get; set; }
        #endregion
    }
}
