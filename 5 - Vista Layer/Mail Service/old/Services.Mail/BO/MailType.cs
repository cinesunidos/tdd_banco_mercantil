﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Mail.DAL;

namespace Services.Mail.BO
{
    public class MailType:Entity<MailType>
    {           
        #region Fields        
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
        public string Subject { get; set; }
        public int Attempts { get; set; }        
        public List<Parameter> Parameters { get; set; }
        #endregion
        #region Methods
        public void Create()
        {
            MailTypeDAL dal = new MailTypeDAL();
            dal.Create(this);
        }
        public void Read()
        {
            MailTypeDAL dal = new MailTypeDAL();
            dal.Read();
            this.Collection = dal.Collection;
        }
        public void Read(string Name)
        {
            MailTypeDAL dal = new MailTypeDAL();            
            dal.Read(Name);
            this.Collection = dal.Collection;
            if (this.Collection.Count > 0)
            {
                this.ID = this.Collection[0].ID;
                this.Name = this.Collection[0].Name;
                this.Template = this.Collection[0].Template;
                this.Subject = this.Collection[0].Subject;
                this.Attempts = this.Collection[0].Attempts;
                this.Parameters = this.Collection[0].Parameters;                
            }
            ParameterDAL parameterDAL = new ParameterDAL();
            parameterDAL.Read();
            List<Parameter> allParameters = parameterDAL.Collection;
            this.Parameters = allParameters.Where(p => p.MailType.ID == this.ID).ToList();
        }
        #endregion
    }
}
