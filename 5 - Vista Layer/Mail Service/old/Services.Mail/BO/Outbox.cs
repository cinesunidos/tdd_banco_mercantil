﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Mail.DAL;

namespace Services.Mail.BO
{
    public class Outbox : Entity<Outbox>
    {
        #region Fields
        public Guid ID { get; set; }
        public string ToEmail { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public int Attempts { get; set; }
        public DateTime CreationDate { get; set; }
        #endregion
        #region Methods
        public void Create()
        {
            OutboxDAL dal = new OutboxDAL();
            dal.Create(this);
        }
        public void Update()
        {
            OutboxDAL dal = new OutboxDAL();
            dal.Update(this);
        }
        public void Delete()
        {
            OutboxDAL dal = new OutboxDAL();
            dal.Delete(this);
        }
        public void Read()
        {
            OutboxDAL dal = new OutboxDAL();
            dal.Read();
            Collection = dal.Collection;
        }
        public void Read(Guid ID)
        {
            OutboxDAL dal = new OutboxDAL();
            dal.Read(ID);
            Collection = dal.Collection;
        }
        #endregion
    }
}
