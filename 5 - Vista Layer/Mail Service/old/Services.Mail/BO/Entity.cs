﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Mail.BO
{
    public class Entity<T>
    {
        public List<T> Collection { get; set; }
    }
}
