﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Services.Common.BL.Contract
{
    [ServiceContract]
    public interface ICommon
    {
        [OperationContract]
        void SendContactMail(string MailTypeName, Dictionary<string, string> Dic, string UserMail);        
    }
}
