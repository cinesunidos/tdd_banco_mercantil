﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Common.BL.Contract;

namespace Services.Common.BL
{
    public class Common : ICommon
    {
        public void SendContactMail(string MailTypeName, Dictionary<string, string> Dic, string UserMail)
        {
            MailService.MailClient client = new MailService.MailClient();
            client.Send(MailTypeName, Dic, UserMail);
        }
    }
}
