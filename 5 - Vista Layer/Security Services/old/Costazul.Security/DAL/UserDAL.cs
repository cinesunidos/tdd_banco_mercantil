﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using Services.Security.BO;

namespace Services.Security.DAL
{
    public class UserDAL:EntityDAL<User>
    {        
       public UserDAL()
            : base() { }
       public UserDAL(string ConnectionName)
            : base(ConnectionName) { }
        #region Methods
        public void Create(Guid CompanyID, User User)
        {
            String Query;            
            Connection connection = new Connection(ConnectionString);
            
            connection.Open();
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            
            parameters.Add("ID", User.ID);
            parameters.Add("IDCard", User.IDCard);
            parameters.Add("BirthDate", User.BirthDate);
            parameters.Add("Country", User.Country);
            parameters.Add("Phone", User.Phone);
            parameters.Add("State", User.State);
            parameters.Add("Sex", User.Sex);
            parameters.Add("City", User.City);
            parameters.Add("Address", User.Address);            
            parameters.Add("Phone2", User.Phone2);
            parameters.Add("MassMailSubscription", User.MassMailSubscription);
            
            

            Query = @"INSERT INTO [User]([ID],[IDCard],[BirthDate],[Country],[Phone],[State],[Sex],[City],[Address],[Phone2],[MassMailSubscription])
                             VALUES (@ID,@IDCard,@BirthDate,@Country,@Phone,@State,@Sex,@City,@Address,@Phone2,@MassMailSubscription)";

            int rowsAffected = connection.Execute(Query, parameters);           

            
            parameters = new Dictionary<string, object>();
            parameters.Add("UserID", User.ID);
            parameters.Add("Name", User.Name);
            parameters.Add("Email", User.Email);
            parameters.Add("Password", User.Password);
            parameters.Add("SecretQuestion", User.SecretQuestion);
            parameters.Add("SecretAnswer", User.SecretAnswer);
            parameters.Add("CreationDate", DateTime.Now);
            parameters.Add("ActivationDate", DateTime.Now.AddDays(2));
            parameters.Add("IsActivated", false);
            parameters.Add("CompanyID", CompanyID);

            Query = @"INSERT INTO [Login]
                                   ([UserID]
                                   ,[Name]
                                   ,[Email]
                                   ,[Password]
                                   ,[SecretQuestion]
                                   ,[SecretAnswer]
                                   ,[CreationDate]
                                   ,[ActivationDate]
                                   ,[IsActivated]
                                   ,[CompanyID])
                                   VALUES
                                   (@UserID,@Name,@Email,@Password,@SecretQuestion,@SecretAnswer,@CreationDate,@ActivationDate,@IsActivated,@CompanyID)";

            rowsAffected = connection.Execute(Query, parameters);
            connection.Close();
            if (!connection.isGood)  throw new Exception(); 
        }
        public void Update(User User)
        {
            String Query;            
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("ID", User.ID);
            parameters.Add("IDCard", User.IDCard);
            parameters.Add("BirthDate", User.BirthDate);
            parameters.Add("Country", User.Country);
            parameters.Add("Phone", User.Phone);
            parameters.Add("State", User.State);
            parameters.Add("Sex", User.Sex);
            parameters.Add("City", User.City);
            parameters.Add("Address", User.Address);            
            parameters.Add("Phone2", User.Phone2);
            parameters.Add("MassMailSubscription", User.MassMailSubscription);
            parameters.Add("SecretQuestion", User.SecretQuestion);
            parameters.Add("SecretAnswer", User.SecretAnswer);
            
            connection.Open();
                       
            Query = @"UPDATE [User]
                           SET [IDCard] = @IDCard
                              ,[BirthDate] = @BirthDate
                              ,[Country] = @Country
                              ,[Phone] = @Phone
                              ,[State] = @State
                              ,[Sex] = @Sex
                              ,[City] = @City
                              ,[Address] = @Address
                              ,[Phone2] = @Phone2
                              ,[MassMailSubscription] = @MassMailSubscription
                         WHERE [ID] = @ID";

            int rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected <= 0) { }; //TODO: Lanzar excepción

            parameters = new Dictionary<string, object>();
            parameters.Add("UserID", User.ID);
            parameters.Add("Name", User.Name);         
            parameters.Add("SecretQuestion", User.SecretQuestion);
            parameters.Add("SecretAnswer", User.SecretAnswer);
            
                        
            Query = @"UPDATE [Login]
                        SET  [Name] = @Name
                            ,[SecretQuestion] = @SecretQuestion
                            ,[SecretAnswer] = @SecretAnswer
                        WHERE  [UserID] = @UserID";

            rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected <= 0) { }; //TODO: Lanzar excepción

            if (User.Password != null && User.Password != string.Empty)
            {
                parameters = new Dictionary<string, object>();
                parameters.Add("UserID", User.ID);
                parameters.Add("Password", User.Password);
                Query = @"UPDATE [Login]
                        SET [Password] = @Password                            
                        WHERE  [UserID] = @UserID";
                rowsAffected = connection.Execute(Query, parameters);
                if (rowsAffected <= 0) { }; //TODO: Lanzar excepción
            }            
            connection.Close();
        }
        public void Read(Guid CompanyID, string Email)
        {            
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("Email", Email);
            parameters.Add("CompanyID", CompanyID);
            

            connection.Open();
            List<User> Users = new List<User>();
            User User = new User();
            String Query = @"SELECT [UserID],[Name],[Email],[Password],[SecretQuestion],[SecretAnswer],[CompanyID],[IsActivated]
                                                                            FROM [Login]
                                                                            WHERE Email = @Email 
                                                                            AND CompanyID = @CompanyID";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    User.ID = dataReader.GetGuid(0);
                    User.Name = dataReader.GetString(1);
                    User.Email = dataReader.GetString(2);
                    User.Password = dataReader.GetString(3);
                    User.SecretQuestion = dataReader.GetString(4);
                    User.SecretAnswer = dataReader.GetString(5);
                    //User.Company.ID = dataReader.GetGuid(6);
                    User.IsActivated = dataReader.GetBoolean(7);
                }
                dataReader.Close();
            }
            

            if (User.ID != Guid.Empty)
            {
                parameters.Add("ID", @User.ID.ToString());
                Query = @"SELECT [ID],[IDCard],[BirthDate],[Country],[Phone],[State],[Sex],[City],[Address],[Phone2],[MassMailSubscription]
                                                                                    FROM [User]
                                                                                    WHERE ID = @ID";
                dataReader = connection.ExecuteRead(Query, parameters);

                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        User.ID = dataReader.GetGuid(0);
                        User.IDCard = dataReader.GetString(1);
                        User.BirthDate = dataReader.GetDateTime(2);
                        User.Country = dataReader.GetString(3);
                        User.Phone = dataReader.GetString(4);
                        if (dataReader.GetValue(5).ToString() != string.Empty) User.State = dataReader.GetString(5);
                        if (dataReader.GetValue(6).ToString() != string.Empty) User.Sex = dataReader.GetString(6);
                        if (dataReader.GetValue(7).ToString() != string.Empty) User.City = dataReader.GetString(7);
                        User.Address = dataReader.GetString(8);
                        if (dataReader.GetValue(9).ToString() != string.Empty) User.Phone2 = dataReader.GetString(9);
                        User.MassMailSubscription = dataReader.GetBoolean(10);
                    }
                    dataReader.Close();
                }
                Users.Add(User);
            }
            connection.Close();
            this.Collection = Users;
        }
        public void Read(Guid UserID)
        {
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("UserID", UserID);


            connection.Open();
            List<User> Users = new List<User>();
            User User = new User();
            String Query = @"SELECT [UserID],[Name],[Email],[Password],[SecretQuestion],[SecretAnswer],[CompanyID],[IsActivated]
                                                                            FROM [Login]
                                                                            WHERE UserID = @UserID";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    User.ID = dataReader.GetGuid(0);
                    User.Name = dataReader.GetString(1);
                    User.Email = dataReader.GetString(2);
                    User.Password = dataReader.GetString(3);
                    User.SecretQuestion = dataReader.GetString(4);
                    User.SecretAnswer = dataReader.GetString(5);
                    //User.Company.ID = dataReader.GetGuid(6);
                    User.IsActivated = dataReader.GetBoolean(7);
                }
                dataReader.Close();
            }


            if (User.ID != Guid.Empty)
            {
                parameters.Add("ID", @User.ID.ToString());
                Query = @"SELECT [ID],[IDCard],[BirthDate],[Country],[Phone],[State],[Sex],[City],[Address],[Phone2],[MassMailSubscription]
                                                                                    FROM [User]
                                                                                    WHERE ID = @ID";
                dataReader = connection.ExecuteRead(Query, parameters);

                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        User.ID = dataReader.GetGuid(0);
                        User.IDCard = dataReader.GetString(1);
                        User.BirthDate = dataReader.GetDateTime(2);
                        User.Country = dataReader.GetString(3);
                        User.Phone = dataReader.GetString(4);
                        if (dataReader.GetValue(5).ToString() != string.Empty) User.State = dataReader.GetString(5);
                        if (dataReader.GetValue(6).ToString() != string.Empty) User.Sex = dataReader.GetString(6);
                        if (dataReader.GetValue(7).ToString() != string.Empty) User.City = dataReader.GetString(7);
                        User.Address = dataReader.GetString(8);
                        if (dataReader.GetValue(9).ToString() != string.Empty) User.Phone2 = dataReader.GetString(9);
                        User.MassMailSubscription = dataReader.GetBoolean(10);
                    }
                    dataReader.Close();
                }
                Users.Add(User);
            }
            connection.Close();
            this.Collection = Users;
        }
        public void Read(string IDCard)
        {
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("IDCard", IDCard);
            
            connection.Open();
            List<User> Users = new List<User>();
            User User = new User();

                       
            String Query = @"SELECT [ID],[IDCard],[BirthDate],[Country],[Phone],[State],[Sex],[City],[Address],[Phone2],[MassMailSubscription]
                             FROM [User]
                             WHERE IDCard = @IDCard";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    User.ID = dataReader.GetGuid(0);
                    User.IDCard = dataReader.GetString(1);
                    User.BirthDate = dataReader.GetDateTime(2);
                    User.Country = dataReader.GetString(3);
                    User.Phone = dataReader.GetString(4);
                    if (dataReader.GetValue(5).ToString() != string.Empty) User.State = dataReader.GetString(5);
                    if (dataReader.GetValue(6).ToString() != string.Empty) User.Sex = dataReader.GetString(6);
                    if (dataReader.GetValue(7).ToString() != string.Empty) User.City = dataReader.GetString(7);
                    User.Address = dataReader.GetString(8);
                    if (dataReader.GetValue(9).ToString() != string.Empty) User.Phone2 = dataReader.GetString(9);
                    User.MassMailSubscription = dataReader.GetBoolean(10);
                }
                dataReader.Close();
            }           

            if (User.ID != Guid.Empty)
            {
                parameters.Add("UserID", @User.ID.ToString());
                Query = @"SELECT [UserID],[Name],[Email],[Password],[SecretQuestion],[SecretAnswer],[CompanyID],[IsActivated]
                       FROM [Login]
                       WHERE UserID = @UserID";
                dataReader = connection.ExecuteRead(Query, parameters);
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        User.ID = dataReader.GetGuid(0);
                        User.Name = dataReader.GetString(1);
                        User.Email = dataReader.GetString(2);
                        User.Password = dataReader.GetString(3);
                        User.SecretQuestion = dataReader.GetString(4);
                        User.SecretAnswer = dataReader.GetString(5);
                        //User.Company.ID = dataReader.GetGuid(6);
                        User.IsActivated = dataReader.GetBoolean(7);
                    }
                    dataReader.Close();
                }
                Users.Add(User);
            }
            connection.Close();
            this.Collection = Users;
        }
        public void Log(Guid CompanyID, string Email, string Password)
        {  
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("CompanyID", CompanyID);
            parameters.Add("Email", Email);
            parameters.Add("Password", Password);
         //   parameters.Add("IsActivated",true);

            connection.Open();
            String Query = @"SELECT [UserID],[Name],[Email],[Password],[SecretQuestion],[SecretAnswer], [CompanyID], [IsActivated]
                                                                            FROM [Login]
                                                                            WHERE [CompanyID] = @CompanyID
                                                                            and [Email] = @Email 
                                                                            and [Password] = @Password";
                                                                            //and [IsActivated] = @IsActivated";

            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
            List<User> Users = new List<User>();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    User User = new User();
                    User.ID = dataReader.GetGuid(0);
                    User.Name = dataReader.GetString(1);
                    User.Email = dataReader.GetString(2);                   
                    //User.Company.ID = dataReader.GetGuid(6);
                    User.IsActivated = dataReader.GetBoolean(7);
                    Users.Add(User);
                }
                dataReader.Close();
            }
            connection.Close();

            this.Collection = Users;
        }
        public void Activate(Guid UserID)
        {
            String Query;
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();            
            parameters.Add("UserID", UserID);
            parameters.Add("IsActivated", true);
            
            connection.Open();

            Query = @"UPDATE [Login]
                        SET  [IsActivated] = @IsActivated
                        WHERE  [UserID] = @UserID";

            int rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected == -1) { throw new Exception(); } //TODO: Lanzar excepción

            connection.Close();
        }
        public string RememberEmail(Guid CompanyID, string IDCard, DateTime BirthDate)
        {            
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("CompanyID", CompanyID);
            parameters.Add("IDCard", IDCard);
            parameters.Add("Year", BirthDate.Year);
            parameters.Add("Month", BirthDate.Month);
            parameters.Add("Day", BirthDate.Day);
            

            connection.Open();
            string Query = @"SELECT ID, Email
                            FROM [User] u
                            INNER JOIN [Login] l on u.ID = l.UserID
                            WHERE l.CompanyID = @CompanyID
                            AND u.IDCard = @IDCard 
                            AND Year(u.BirthDate) = @Year
                            AND Month(u.BirthDate) = @Month
                            AND DAY(u.BirthDate) = @Day";
            SqlDataReader dataReader = connection.ExecuteRead(Query, parameters);
            
            User User = new User();
            if (dataReader != null)
            {
                if (dataReader.Read())
                {   
                    User.ID = dataReader.GetGuid(0);
                    User.Email = dataReader.GetString(1);
                }
                dataReader.Close();
            }
            connection.Close();
            return User.Email;
        }
        public void ChangePassword(Guid CompanyID, string Email, string Password)
        {             
            String Query;
            Connection connection = new Connection(ConnectionString);
            Dictionary<string, object> parameters = new Dictionary<string, object>();
           
            connection.Open();

            parameters = new Dictionary<string, object>();

            parameters.Add("CompanyID", CompanyID);
            parameters.Add("Email", Email);
            parameters.Add("Password", Password);         
           

            Query = @"UPDATE [Login]
                        SET  [Password] = @Password                                             
                        WHERE [CompanyID] =  @CompanyID 
                        AND [Email] = @Email";

            int rowsAffected = connection.Execute(Query, parameters);
            if (rowsAffected == 0) { }; //TODO: Lanzar excepción

            connection.Close();
        }
        #endregion
    }
}
