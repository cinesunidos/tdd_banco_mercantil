﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Services.Security.BL
{
    public class User : Contract.IUser
    {
        public void Create(Guid CompanyID, BO.User User)
        {
            try
            {
                BO.User user = new BO.User();
                user.Read(CompanyID, User.Email);

                BO.User user2 = new BO.User();
                user2.Read(User.IDCard);

                if (user.Collection.Count <= 0 && user2.Collection.Count <= 0)
                {
                    User.Create(CompanyID);
                    this.SendActivateEmail(User.ID);
                }
                else
                {
                    if(user.Collection.Count > 0)
                    {
                        GenericException exp = new GenericException();
                        exp.Code = "S0002";
                        exp.Message = "Usuario ya existente";
                        exp.Title = "Usuario ya existente";
                        throw new FaultException<GenericException>(exp, new FaultReason(string.Format("{0}",exp.Message)));
                    }
                    if (user2.Collection.Count > 0)
                    {
                        GenericException exp = new GenericException();
                        exp.Code = "S0004";
                        exp.Message = "La cédula ya se encuentra registrada";
                        exp.Title = "La cédula ya se encuentra registrada";
                        throw new FaultException<GenericException>(exp, new FaultReason(string.Format("{0}", exp.Message)));
                    }
                }
            }
            catch (FaultException fex)
            {
                throw fex;
            }
            catch (Exception ex)
            {
                GenericException exp = new GenericException();
                exp.Code = "S0003";
                exp.Message = "No se puede crear el usuario";
                exp.Title = "No se puede crear el usuario";
                throw new FaultException<GenericException>(exp, new FaultReason(string.Format("{0}", exp.Message)));
            }
        }
        public void Update(BO.User User)
        {
            User.Update();
        }
        public BO.User Read(Guid CompanyID, string Email)
        {
            BO.User User = new BO.User();
            User.Read(CompanyID, Email);
            return User;
        }
        public BO.User Log(Guid CompanyID, string Email, string Password)
        {
            BO.User User = new BO.User();
            User.Log(CompanyID, Email, Password);
            return User;
        }
        public Boolean Activate(Guid UserID)
        {
            try
            {
                BO.User User = new BO.User();
                User.Activate(UserID);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string RememberEmail(Guid CompanyID, string IDCard, DateTime BirthDate)
        {
            BO.User User = new BO.User();
            return User.RememberEmail(CompanyID, IDCard, BirthDate);
        }
        public void RememberPassword(Guid CompanyID, string Email)
        {
            try
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                BO.User user = new BO.User();
                user.Read(CompanyID, Email);
                if (user.ID != Guid.Empty)
                {
                    MailService.MailClient Client = new MailService.MailClient();
                    dic.Add("Name", user.Name);
                    dic.Add("Email", Email);
                    dic.Add("Password", user.Password);
                    Client.Send("RememberPassword", dic, Email);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception Ex)
            {
                GenericException exp = new GenericException();
                exp.Code = "S0001";
                exp.Message = Ex.Message;
                exp.Title = Ex.Message;
                throw new FaultException<GenericException>(exp, new FaultReason(string.Format("{0}-{1}", exp.Code, exp.Message)));            
            }
        }        
        public Boolean SendActivateEmail(Guid UserID)
        {
            try
            {
                BO.User User = new BO.User();
                User.Read(UserID);
                if (User.IsActivated == false)
                {
                    MailService.MailClient Client = new MailService.MailClient();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("UserID", User.ID.ToString());
                    dic.Add("Name", User.Name);
                    Client.Send("ActivateAccount", dic, User.Email);
                }
                else
                {
                    throw new Exception();
                }

                return true;
            }
            catch (Exception Ex)
            {
                return false;
            }
        }
        //public void ChangePassword(Guid CompanyID, string Email, string Password)
        //{
        //    BO.User user = new BO.User();
        //    user.ChangePassword(CompanyID, Email, Password);
        //}
    }
}
