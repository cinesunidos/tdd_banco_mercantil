﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;

namespace Services.Security
{
    internal class Connection
    {
        SqlConnection conn;
        private SqlTransaction SqlTransaction { get;  set; }
        public bool isGood { get; private set; }
        public Connection(string connectionString)
        {
            isGood = true;
            conn = new SqlConnection(connectionString);
        }
        public void Open()
        {
            isGood = true;
            conn.Open();
            SqlTransaction = conn.BeginTransaction();
        }
        private void Save()
        {
            SqlTransaction.Commit(); 
        }
        private void RollBack()
        {
            SqlTransaction.Rollback(); 
        }
        public void Close()
        {
            if (!isGood) RollBack();
            else Save();
            if (conn != null) conn.Close();
        }
        public SqlDataReader ExecuteRead(string query, Dictionary<string, object> parameters)
        {
            SqlDataReader dataReader;
            SqlCommand sqlCommand;
            try
            {
                sqlCommand = conn.CreateCommand();
                sqlCommand.Transaction = SqlTransaction;
                sqlCommand.CommandType = CommandType.Text;

                foreach (KeyValuePair<string, object> item in parameters)
                {
                    SqlParameter parameter = sqlCommand.CreateParameter();
                    parameter.ParameterName = string.Format("@{0}", item.Key);
                    parameter.Value = item.Value;
                    sqlCommand.Parameters.Add(parameter);
                }
                sqlCommand.CommandText = query;
                dataReader = sqlCommand.ExecuteReader();
                
                return dataReader;
            }
            catch (SqlException ex)
            {
                return null;
            }
        }
        public int Execute(string query, Dictionary<string, object> parameters)
        {            
            SqlCommand sqlCommand;
            try
            {
                sqlCommand = conn.CreateCommand();
                sqlCommand.Transaction = SqlTransaction;
                sqlCommand.CommandType = CommandType.Text;

                foreach (KeyValuePair<string, object> item in parameters)
                {
                    SqlParameter parameter = sqlCommand.CreateParameter();
                    parameter.IsNullable = true;
                    parameter.ParameterName = string.Format("@{0}", item.Key);
                    parameter.Value = (object)item.Value??DBNull.Value;
                    sqlCommand.Parameters.Add(parameter);
                }
                sqlCommand.CommandText = query;
                int n = sqlCommand.ExecuteNonQuery();
                return n;
            }
            catch (SqlException ex)
            {
                isGood = false;
                return -1;
            }
        }
    }
}

