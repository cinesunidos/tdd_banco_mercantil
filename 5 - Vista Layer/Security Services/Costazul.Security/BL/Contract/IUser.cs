﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using Services.Security.BO;

namespace Services.Security.BL.Contract
{
    [ServiceContract]
    public interface IUser
    {
        [OperationContract]
        void Create(Guid CompanyID, BO.User User);
        [OperationContract]
        void Update(BO.User User);
        [OperationContract]
        BO.User Read(Guid Company, string Email);
        [OperationContract]
        BO.User Log(Guid Company, string Email, string Password);
        [OperationContract]
        Boolean Activate(Guid UserID);
        [OperationContract]
        string RememberEmail(Guid CompanyID, string IDCard, DateTime BirthDate);
        [OperationContract]
        [FaultContract(typeof(GenericException))]
        void RememberPassword(Guid CompanyID, string Email);
        [OperationContract]
        Boolean SendActivateEmail(Guid UserID);
    }
}
