﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Security.BL
{
    public class GenericException
    {
        #region Fields
        private string m_Code;        
        private string m_Message;        
        private string m_Title;
        #endregion
        #region Properties
        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }
        public string Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }
        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }
        #endregion        
    }
}
