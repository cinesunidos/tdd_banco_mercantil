﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Security.DAL;

namespace Services.Security.BO
{   
    public class User:Entity<User>        
    {
        public User()            
        {
           // Company = new Company();
        }
        #region Fields       
        private Guid m_ID;      
        private string m_IDCard;        
        private DateTime m_BirthDate;        
        private string m_Country;        
        private string m_Phone;        
        private string m_State;      
        private string m_Sex;        
        private string m_City;       
        private string m_Address;        
        private string m_Phone2;
        private Boolean m_MassMailSubscription;        
        private string m_Name;
        private string m_Email;
        private string m_Password;
        private string m_SecretQuestion;
        private string m_SecretAnswer;
        private Boolean m_IsActivated;
                
      //  private Company m_Company;
        #endregion
        #region Properties
        public Guid ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }       
        public string IDCard
        {
            get { return m_IDCard; }
            set { m_IDCard = value; }
        }
        public DateTime BirthDate
        {
            get { return m_BirthDate; }
            set { m_BirthDate = value; }
        }
        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }
        public string Sex
        {
            get { return m_Sex; }
            set { m_Sex = value; }
        }
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }
        public string Phone2
        {
            get { return m_Phone2; }
            set { m_Phone2 = value; }
        }
        public Boolean MassMailSubscription
        {
            get { return m_MassMailSubscription; }
            set { m_MassMailSubscription = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }
        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }
        public string SecretQuestion
        {
            get { return m_SecretQuestion; }
            set { m_SecretQuestion = value; }
        }
        public string SecretAnswer
        {
            get { return m_SecretAnswer; }
            set { m_SecretAnswer = value; }
        }
        public Boolean IsActivated
        {
            get { return m_IsActivated; }
            set { m_IsActivated = value; }
        }
        //public Company Company
        //{
        //    get { return m_Company; }
        //    set { m_Company = value; }
        //}
        #endregion             
        #region Methods
        public void Create(Guid CompanyID)
        {
            UserDAL UserDAL = new UserDAL();
            UserDAL.Create(CompanyID,this);
        }
        public void Update()
        {
            UserDAL UserDAL = new UserDAL();
            UserDAL.Update(this);
        }
        public void Read(Guid Company, string Email)
        {
            UserDAL UserDAL = new UserDAL();
            UserDAL.Read(Company, Email);
            this.Collection = UserDAL.Collection;
            if(Collection.Count>0)
            {
                this.ID = Collection[0].ID;
                this.Address = Collection[0].Address;
                this.BirthDate = Collection[0].BirthDate;
                this.City = Collection[0].City;
               // this.Company.ID = Collection[0].Company.ID;
                this.Country = Collection[0].Country;
                this.Email = Collection[0].Email;
                this.IDCard = Collection[0].IDCard;
                this.Name = Collection[0].Name;
                this.Password = Collection[0].Password;
                this.Phone = Collection[0].Phone;
                this.Phone2 = Collection[0].Phone2;
                this.Sex = Collection[0].Sex;
                this.State = Collection[0].State;
                this.IsActivated = Collection[0].IsActivated;
                this.MassMailSubscription = Collection[0].MassMailSubscription;
                this.SecretQuestion = Collection[0].SecretQuestion;
                this.SecretAnswer = Collection[0].SecretAnswer;
            }
        }
        public void Read(Guid ID)
        {
            UserDAL UserDAL = new UserDAL();
            UserDAL.Read(ID);
            this.Collection = UserDAL.Collection;
            if (Collection.Count > 0)
            {
                this.ID = Collection[0].ID;
                this.Address = Collection[0].Address;
                this.BirthDate = Collection[0].BirthDate;
                this.City = Collection[0].City;
               // this.Company.ID = Collection[0].Company.ID;
                this.Country = Collection[0].Country;
                this.Email = Collection[0].Email;
                this.IDCard = Collection[0].IDCard;
                this.Name = Collection[0].Name;
                this.Password = Collection[0].Password;
                this.Phone = Collection[0].Phone;
                this.Phone2 = Collection[0].Phone2;
                this.Sex = Collection[0].Sex;
                this.State = Collection[0].State;
                this.IsActivated = Collection[0].IsActivated;
                this.MassMailSubscription = Collection[0].MassMailSubscription;
                this.SecretQuestion = Collection[0].SecretQuestion;
                this.SecretAnswer = Collection[0].SecretAnswer;
            }
        }
        public void Read(string IDCard)
        {
            UserDAL UserDAL = new UserDAL();
            UserDAL.Read(IDCard);
            this.Collection = UserDAL.Collection;
            if (Collection.Count > 0)
            {
                this.ID = Collection[0].ID;
                this.Address = Collection[0].Address;
                this.BirthDate = Collection[0].BirthDate;
                this.City = Collection[0].City;
                // this.Company.ID = Collection[0].Company.ID;
                this.Country = Collection[0].Country;
                this.Email = Collection[0].Email;
                this.IDCard = Collection[0].IDCard;
                this.Name = Collection[0].Name;
                this.Password = Collection[0].Password;
                this.Phone = Collection[0].Phone;
                this.Phone2 = Collection[0].Phone2;
                this.Sex = Collection[0].Sex;
                this.State = Collection[0].State;
                this.IsActivated = Collection[0].IsActivated;
                this.MassMailSubscription = Collection[0].MassMailSubscription;
                this.SecretQuestion = Collection[0].SecretQuestion;
                this.SecretAnswer = Collection[0].SecretAnswer;
            }   
        }
        public void Log(Guid Company, string Email, string Password)
        {
            UserDAL UserDAL = new UserDAL();
            UserDAL.Log(Company, Email, Password);
            this.Collection = UserDAL.Collection;
            if (Collection.Count > 0)
            {
                this.ID = Collection[0].ID;
                this.Address = Collection[0].Address;
                this.BirthDate = Collection[0].BirthDate;
                this.City = Collection[0].City;
                //this.Company.ID = Collection[0].Company.ID;
                this.Country = Collection[0].Country;
                this.Email = Collection[0].Email;
                this.IDCard = Collection[0].IDCard;
                this.Name = Collection[0].Name;
                this.Phone = Collection[0].Phone;
                this.Phone2 = Collection[0].Phone2;
                this.Sex = Collection[0].Sex;
                this.State = Collection[0].State;
                this.IsActivated = Collection[0].IsActivated;
            }
        }
        public void Activate(Guid UserID)
        {
            UserDAL UserDAL = new UserDAL();
            UserDAL.Activate(UserID);
        }
        public string RememberEmail(Guid CompanyID, string IDCard, DateTime BirthDate)
        {
            UserDAL UserDAL = new UserDAL();
            string Email = UserDAL.RememberEmail(CompanyID, IDCard, BirthDate);
            return Email;
        }
        public void ChangePassword(Guid CompanyID, string Email, string Password)
        {
            UserDAL dal = new UserDAL();
            dal.ChangePassword(CompanyID, Email, Password);
        }
        #endregion
    }
}