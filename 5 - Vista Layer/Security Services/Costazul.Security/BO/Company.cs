﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Security.BO
{
    public class Company
    {
        #region Fields
        private Guid m_ID;
        private string m_Name;
        #endregion
        #region Properties
        public Guid ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        #endregion
    }
}
