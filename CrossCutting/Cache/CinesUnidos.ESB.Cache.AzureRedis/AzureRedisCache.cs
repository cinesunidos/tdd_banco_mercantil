﻿using CinesUnidos.ESB.Cache.Configuration;
using StackExchange.Redis;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CinesUnidos.ESB.Cache.AzureRedis
{
    public class AzureRedisCache<T> : ICache<T>
        where T : class
    {
        #region Attributes

        protected readonly ICacheConfiguration iConfiguration;

        protected ConnectionMultiplexer connection;

        #endregion Attributes

        #region Properties

        protected IDatabase Cache { get; set; }

        #endregion Properties

        #region Methods

        public AzureRedisCache(ICacheConfiguration iConfiguration)
        {
            if (iConfiguration == null)
                throw new ArgumentNullException("ICacheConfiguration");
            this.iConfiguration = iConfiguration;
            this.SetupConnection();
            this.Cache = connection.GetDatabase();
        }

        private void SetupConnection()
        {
            var config = new ConfigurationOptions();
            config.EndPoints.Add(this.iConfiguration.ConnectionString, 6380);
            config.Password = this.iConfiguration.Password;
            config.Ssl = Convert.ToBoolean(this.iConfiguration.sslRedisCache);
            config.AllowAdmin = true;
            config.KeepAlive = 30;
            config.ConnectRetry = 3;
            config.ConnectTimeout = 15000;
            config.SyncTimeout = 15000;
            config.AbortOnConnectFail = false;
            connection = ConnectionMultiplexer.Connect(config);
        }

        public T Get(string key)
        {
            T result = null;
            var cachePeding = Cache.StringGetAsync(key);
            RedisValue cacheValue = Cache.Wait(cachePeding);//Cache.StringGet(key);
            if (cacheValue.HasValue)
                result = Deserialize(cacheValue);
            return result;
        }

        public void Set(string key, T value, TimeSpan expire)
        {
            var setvalue = Serialize(value);
            Cache.StringSet(key, setvalue, expire);
            //Cache.StringSet(key, setvalue, expire, When.Always, CommandFlags.FireAndForget);
        }

        public void Close()
        {
            connection.Close();
        }
        public void Delete(string key)
        {
            Cache.KeyDelete(key);
        }

        private byte[] Serialize(object o)
        {
            if (o == null)
            {
                return null;
            }
            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, o);
                byte[] objectDataAsStream = memoryStream.ToArray();
                return objectDataAsStream;
            }
        }

        private T Deserialize(byte[] stream)
        {
            var binaryFormatter = new BinaryFormatter();
            if (stream == null)
                return default(T);

            using (var memoryStream = new MemoryStream(stream))
            {
                T result = (T)binaryFormatter.Deserialize(memoryStream);
                return result;
            }
        }

       /* public void FlushAll()
        {
            var endpoints = connection.GetEndPoints(true);
            foreach (var endpoint in endpoints)
            {
                var server = connection.GetServer(endpoint);
                server.FlushAllDatabases();
            }
        }*/

        #endregion Methods
    }
}