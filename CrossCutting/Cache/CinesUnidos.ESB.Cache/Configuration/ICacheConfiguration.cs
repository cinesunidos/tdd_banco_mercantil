﻿namespace CinesUnidos.ESB.Cache.Configuration
{
    public interface ICacheConfiguration
    {
        string ConnectionString { get; }

        string Password { get; }

        string sslRedisCache { get; }

    }
}