﻿using System;

namespace CinesUnidos.ESB.Cache.Configuration
{
    public class CacheConfiguration : ICacheConfiguration
    {
        private string connectionString;
        private string password;
        private string sslRedis;

        public CacheConfiguration(string connectionString, string password, string sslRedisCache)
        {
            this.connectionString = connectionString;
            this.password = password;
            this.sslRedis = sslRedisCache;
        }

        public string ConnectionString
        {
            get { return connectionString; }
        }

        public string Password
        {
            get { return password; }
        }

        public string sslRedisCache
        {
            get { return sslRedis; }
        }
    }
}