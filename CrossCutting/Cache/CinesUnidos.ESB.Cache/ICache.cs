﻿using System;

namespace CinesUnidos.ESB.Cache
{
    public interface ICache<T>
        where T : class
    {
        #region Methods

        T Get(string key);

        void Set(string key, T value, TimeSpan expire);

        void Delete(string key);

        void Close();

        #endregion Methods
    }
}