﻿using CinesUnidos.ESB.Queue.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace CinesUnidos.ESB.Queue.AzureQueue
{
    public class AzureQueue : IQueue
    {
        #region Attributes

        private CloudStorageAccount m_storageAccount = null;
        private CloudQueueClient m_queueClient = null;
        private string m_accountName;
        private string m_accountKey;
        private string m_connection;

        private string[] m_queues = new string[] { "purchases", "cancelled", "services" };
        private string[] m_queueKeys = new string[] { "purchases", "cancelled", "services" };
        private Dictionary<string, string> m_queueNameKeysDictionary = new Dictionary<string, string>();

        private Dictionary<string, CloudQueue> m_cloudQueue;

        #endregion Attributes

        #region Properties

        public string AccountName
        {
            get { return m_accountName; }
        }

        public string AccountKey
        {
            get { return m_accountKey; }
        }

        public string[] Containers
        {
            get
            {
                return m_queues.ToArray();
            }
        }

        #endregion Properties

        #region Constructor

        public AzureQueue(IQueueConfiguration queueConfiguration)
        {
            m_queues = queueConfiguration.Queues;
            m_queueKeys = queueConfiguration.Keys;
            m_queueNameKeysDictionary = queueConfiguration.QueueNameFromKey;

            m_accountKey = queueConfiguration.AccountKey;
            m_accountName = queueConfiguration.AccountName;

            //LUIS RAMIREZ 10/05/2017, Si las colas apuntan a ambiente local, habilitar cadena de conexion con la url http://127.0.0.1/devstorageaccount1
            string AltConnectionString = string.Empty;
            if (m_queues.Contains("purchases-local") || m_queues.Contains("cancelled-local") || m_queues.Contains("services-local"))
                AltConnectionString = "UseDevelopmentStorage=true";
            else
                AltConnectionString = "";
            //if (String.IsNullOrEmpty(queueConfiguration.AltConnectionString))
            if (String.IsNullOrEmpty(AltConnectionString))
                m_connection = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", m_accountName, m_accountKey);
            else
                m_connection = string.Format(AltConnectionString, m_accountName, m_accountKey);
            //m_connection = string.Format(queueConfiguration.AltConnectionString, m_accountName, m_accountKey);            
            m_storageAccount = CloudStorageAccount.Parse(m_connection);
            m_queueClient = m_storageAccount.CreateCloudQueueClient();
            LoadQueue();
        }

        #endregion Constructor

        #region Methods

        private void LoadQueue()
        {
            m_cloudQueue = new Dictionary<string, CloudQueue>();
            foreach (string queue in m_queues)
            {
                CloudQueue cloudQueue = m_queueClient.GetQueueReference(queue);
                cloudQueue.CreateIfNotExists();
                m_cloudQueue.Add(queue, cloudQueue);
            }
        }

        private CloudQueue GetCloudQueueByKey(string key)
        {
            return m_cloudQueue[m_queueNameKeysDictionary[key]];
        }

        public void InsertMessage<T>(string queueKey, T value)
            where T : class
        {
            Serializer s = new Serializer();
            CloudQueue cloudQueue = GetCloudQueueByKey(queueKey);
            //string xml = DogFramework.Xml.XmlAdapter.Serialize<T>(value);
            string xml = s.Serialize<T>(value);
            string xmlCompress = Compress(xml);
            CloudQueueMessage message = new CloudQueueMessage(xmlCompress);
            cloudQueue.AddMessage(message);
        }

        public T PeekMessage<T>(string queueKey)
            where T : class
        {
            Serializer s = new Serializer();
            CloudQueue cloudQueue = GetCloudQueueByKey(queueKey);
            CloudQueueMessage peekedMessage = cloudQueue.PeekMessage();
            string xmlDecompress = Decompress(peekedMessage.AsString);
            //T value = (T)DogFramework.Xml.XmlAdapter.DeserializeFromMemory(xmlDecompress, typeof(T));
            T value = (T)s.DeserializeFromMemory(xmlDecompress, typeof(T));
            return value;
        }

        public T DequeueMessage<T>(string queueKey)
             where T : class
        {
            CloudQueue cloudQueue = GetCloudQueueByKey(queueKey);
            CloudQueueMessage message = cloudQueue.GetMessage();
            Serializer s = new Serializer();
            if (message != null)
            {
                if (!string.IsNullOrEmpty(message.AsString))
                {
                    string xmlDecompress = Decompress(message.AsString);
                    T value;
                    //value = (T)DogFramework.Xml.XmlAdapter.DeserializeFromMemory(xmlDecompress, typeof(T));
                    value = (T)s.DeserializeFromMemory(xmlDecompress, typeof(T));
                    cloudQueue.DeleteMessage(message);
                    return value;
                }
                else
                    return null;
            }
            else
                return null;
        }

        public IEnumerable<T> DequeueMessages<T>(string queueKey, int countMessage, TimeSpan time)
             where T : class
        {
            List<T> messages = new List<T>();
            Serializer s = new Serializer();
            CloudQueue cloudQueue = GetCloudQueueByKey(queueKey);
            foreach (CloudQueueMessage message in cloudQueue.GetMessages(countMessage, time))
            {
                string xmlDecompress = Decompress(message.AsString);
                //  T value = (T)DogFramework.Xml.XmlAdapter.DeserializeFromMemory(xmlDecompress, typeof(T));
                T value = (T)s.DeserializeFromMemory(xmlDecompress, typeof(T));
                messages.Add(value);
            }
            return messages;
        }

        public int? ApproximateMessageCount(string queueKey)
        {
            CloudQueue cloudQueue = GetCloudQueueByKey(queueKey);
            cloudQueue.FetchAttributes();
            int? cachedMessageCount = cloudQueue.ApproximateMessageCount;
            return cachedMessageCount;
        }

        private string Compress(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        private string Decompress(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        #endregion Methods
    }
}