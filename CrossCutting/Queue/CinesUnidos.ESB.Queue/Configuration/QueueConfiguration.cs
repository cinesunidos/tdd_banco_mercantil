﻿using System.Collections.Generic;
using System.Linq;

namespace CinesUnidos.ESB.Queue.Configuration
{
    public class QueueConfiguration : IQueueConfiguration
    {
        private string accountName;
        private string accountKey;

        private Dictionary<string, string> queueNameFromKey;

        private string altConnectionString;

        private void GetQueues(string queues)
        {
            this.queueNameFromKey = new Dictionary<string, string>();

            string[] keyValues = queues.Split(',');
            foreach (var keyValue in keyValues)
            {
                var parts = keyValue.Split(':');
                if (parts.Length == 2)
                {
                    this.queueNameFromKey.Add(parts[0], parts[1]);
                }
                else if (parts.Length == 1) // Usa el mismo nombre como key
                {
                    this.queueNameFromKey.Add(parts[0], parts[0]);
                }
                else
                    throw new System.Exception("Error en formato del parametro queues, debe ser key1:name1,key2:name2...");
            }
        }

        public QueueConfiguration(string accountName, string accountKey, string queues, string altConnectionString = null)
        {
            this.altConnectionString = altConnectionString;
            this.accountName = accountName;
            this.accountKey = accountKey;
            GetQueues(queues);
        }

        public string AccountName
        {
            get { return accountName; }
        }

        public string AccountKey
        {
            get { return accountKey; }
        }

        /// <summary>
        /// Arreglo con los nombres reales de las colas
        /// </summary>
        public string[] Queues
        {
            get { return queueNameFromKey.Values.ToArray(); }
        }

        /// <summary>
        /// Arreglo con el key para referirse a las colas
        /// </summary>
        public string[] Keys
        {
            get { return queueNameFromKey.Keys.ToArray(); }
        }

        /// <summary>
        /// Arreglo con el key para referirse a las colas
        /// </summary>
        public Dictionary<string, string> QueueNameFromKey
        {
            get { return queueNameFromKey; }
        }

        /// <summary>
        /// Connection String personalizable.
        /// </summary>
        public string AltConnectionString
        {
            get { return altConnectionString; }
        }
    }
}