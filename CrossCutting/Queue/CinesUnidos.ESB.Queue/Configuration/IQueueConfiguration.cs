﻿using System.Collections.Generic;

namespace CinesUnidos.ESB.Queue.Configuration
{
    public interface IQueueConfiguration
    {
        string AccountName { get; }

        string AccountKey { get; }

        /// <summary>
        /// Arreglo con los nombres reales de las colas
        /// </summary>
        string[] Queues { get; }
        /// <summary>
        /// Arreglo con los Key para referirse a las colas
        /// </summary>
        string[] Keys { get; }

        /// <summary>
        /// Diccionario de Nombres a partir de los key
        /// </summary>
        Dictionary<string, string> QueueNameFromKey { get; }

        /// <summary>
        /// Connection String personalizable.
        /// </summary>
        string AltConnectionString { get; }
    }
}