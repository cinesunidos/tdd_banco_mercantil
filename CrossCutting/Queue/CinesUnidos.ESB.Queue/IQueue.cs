﻿using System;
using System.Collections.Generic;

namespace CinesUnidos.ESB.Queue
{
    public interface IQueue
    {
        #region Properties

        string[] Containers { get; }

        string AccountKey { get; }

        string AccountName { get; }

        #endregion Properties

        #region Methods

        int? ApproximateMessageCount(string queueKey);

        T DequeueMessage<T>(string queueKey)
            where T : class;

        IEnumerable<T> DequeueMessages<T>(string queueKey, int countMessage, TimeSpan time)
            where T : class;

        void InsertMessage<T>(string queueKey, T value)
            where T : class;

        T PeekMessage<T>(string queueKey)
            where T : class;

        #endregion Methods
    }
}